/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <context.h>

namespace sa::tl::bind {

template<typename T, typename... Args>
inline sa::tl::Value CreateInstance(sa::tl::Context& ctx, const sa::tl::MetaTable& meta, Args&&... Arguments)
{
    auto* result = ctx.GetGC().CreateObject<T>(std::forward<Args>(Arguments)...);
    return sa::tl::MakeObject(meta, result);
};

template<typename T, typename... Args>
inline sa::tl::ValuePtr CreateInstanceValuePtr(sa::tl::Context& ctx, const sa::tl::MetaTable& meta, Args&&... Arguments)
{
    auto* result = ctx.GetGC().CreateObject<T>(std::forward<Args>(Arguments)...);
    return sa::tl::MakeValue(sa::tl::MakeObject(meta, result));
};

template<typename T, typename... Args>
inline T* CreateInstancePtr(sa::tl::Context& ctx, Args&&... Arguments)
{
    return ctx.GetGC().CreateObject<T>(std::forward<Args>(Arguments)...);
};

}
