project (bind CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(bind STATIC ${SOURCES})
set_property(TARGET bind PROPERTY POSITION_INDEPENDENT_CODE ON)
target_include_directories(bind PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(bind satl)
