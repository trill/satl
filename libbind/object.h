/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <context.h>
#include <non_copyable.h>
#include <metatable.h>

namespace sa::tl::bind {

class Object
{
    SATL_NON_COPYABLE(Object);
    SATL_NON_MOVEABLE(Object);
public:
    explicit Object(sa::tl::Context& ctx);
    virtual ~Object();
    [[nodiscard]] virtual constexpr std::string_view GetTypeName() const { return sa::tl::TypeName<Object>::Get(); }
    virtual void Destroy();

    operator sa::tl::WeakObject() const // NOLINT(hicpp-explicit-conversions)
    {
        return ctx_.GetGC().GetWeak(this);
    }
    operator sa::tl::StrongObject() // NOLINT(hicpp-explicit-conversions)
    {
        return ctx_.GetGC().GetStrong(this);
    }
    bool HasProperty(const std::string& name) const;
    Value GetProperty(const std::string& name) const;
    void SetProperty(const std::string& name, ValuePtr value);
    bool HasFunction(const std::string& name) const;
    Value CallFunction(const std::string& name, const Table& arguments);
    bool HasOperator(const std::string& name) const;
    Value CallOperator(const std::string& name, const Table& arguments);
protected:
    // Returns a pointer to self of the most concrete type.
    sa::tl::ValuePtr GetSelfPtr()
    {
        const auto* meta = ctx_.GetMetatable(GetTypeName());
        // Add a metatable for this type to the context!
        SATL_ASSERT(meta);
        return sa::tl::MakeValue(sa::tl::MakeObject(*meta, meta->GetClassName(), this));
    }
    sa::tl::Context& ctx_;
};

#define TYPED_OBJECT(T) \
public:                 \
    [[nodiscard]] constexpr std::string_view GetTypeName() const override { return sa::tl::TypeName<T>::Get(); }

class [[nodiscard]] ObjectDestroyer
{
    SATL_NON_COPYABLE(ObjectDestroyer);
    SATL_NON_MOVEABLE(ObjectDestroyer);
private:
    Object& object_;
public:
    explicit ObjectDestroyer(Object& object) :
        object_(object)
    { }
    ~ObjectDestroyer()
    {
        object_.Destroy();
    }
};

template<typename T>
inline sa::tl::MetaTable& Register(sa::tl::Context&)
{
    SATL_ASSERT_FALSE();
}

template<> sa::tl::MetaTable& Register<Object>(sa::tl::Context& ctx);

}
