# libbind

A static library to make it easier to bind C++ classes, especially memory
management, lifetime and casting.

## Usage

Using this library is optional, but if you do, your class must derive from
`sa::tl::bind::Object` and use the `TYPED_OBJECT` macro. `TYPED_OBJECT` is
used to make `Object::GetTypeName()` working.

~~~cpp
#include <object.h>

class MyClass : public sa::tl::bind::Object
{
    TYPED_OBJECT(MyClass)
public:
    explicit MyClass(sa::tl::Context& ctx) :
        Object(ctx)
    { }
}
~~~

Subclasses will be GC'd, they get a `Destroy()` function to remove them from the GC.
They should be instantiated with one of the `CreateInstance...` factory functions.

## Register

You need to create a metatable for each class that should be useable from scripts.
The `Object` base class isnt't registered automatically, you must do it in your code:
~~~cpp
[[maybe_unused]] auto& objectMeta = sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
~~~

Register a custom class:
~~~cpp
namespace sa::tl::bind {

// Create a template specialization of the Register function for MyClass
template<>
inline sa::tl::MetaTable& Register<MyClass>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<MyClass, Object>();
    // Add properties, functions and events
    return result;
}

}
~~~

Call it as follows:
~~~cpp
[[maybe_unused]] auto& myClassMeta = sa::tl::bind::Register<MyClass>(ctx);
~~~

## Examples

See `http_lib` and `ui_lib` which use it heavily.

