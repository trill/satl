/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "object.h"

namespace sa::tl::bind {

Object::Object(sa::tl::Context& ctx) :
    ctx_(ctx)
{
}

Object::~Object()
{
    ctx_.ResetObject(this);
}

void Object::Destroy()
{
    // Destroy, deallocate this Object and set all Values pointing to this instance to null. It does it by:
    // 1. Remove from GC, this  causes
    // 2. the deletion of the object
    // 3. which calls the destructor calling Context::ResetObject().
    ctx_.GetGC().Remove(this);
}

bool Object::HasProperty(const std::string& name) const
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
        return false;
    auto prop = meta->GetPropertyGetter(name);
    return !!prop;
}

Value Object::GetProperty(const std::string& name) const
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Metatable for {} not found", GetTypeName()));
        return {};
    }
    auto prop = meta->GetPropertyGetter(name);
    if (!prop)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Property {} not found", name));
        return {};
    }
    sa::tl::ObjectPtr object = MakeObject(*meta, const_cast<Object*>(this));
    return *(*prop)(ctx_, object, {});
}

void Object::SetProperty(const std::string& name, ValuePtr value)
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Metatable for {} not found", GetTypeName()));
        return;
    }
    auto prop = meta->GetPropertySetter(name);
    if (!prop)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Property {} not found", name));
        return;
    }
    sa::tl::ObjectPtr object = MakeObject(*meta, this);
    (*prop)(ctx_, object, { std::move(value) });
}

bool Object::HasFunction(const std::string& name) const
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Metatable for {} not found", GetTypeName()));
        return false;
    }
    return !!meta->GetFunction(name);
}

Value Object::CallFunction(const std::string& name, const Table& arguments)
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Metatable for {} not found", GetTypeName()));
        return {};
    }
    auto func = meta->GetFunction(name);
    if (!func)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Function {} not found", name));
        return {};
    }
    sa::tl::ObjectPtr object = MakeObject(*meta, this);
    FunctionArguments args;
    for (const auto& a : (arguments))
        args.push_back(a.second);

    return *(*func)(ctx_, object, args);
}

bool Object::HasOperator(const std::string& name) const
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Metatable for {} not found", GetTypeName()));
        return false;
    }
    return !!meta->GetOperator(name);
}

Value Object::CallOperator(const std::string& name, const Table& arguments)
{
    const auto* meta = ctx_.GetMetatable(GetTypeName());
    if (!meta)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Metatable for {} not found", GetTypeName()));
        return {};
    }
    auto func = meta->GetOperator(name);
    if (!func)
    {
        ctx_.RuntimeError(Error::Code::InvalidArgument, std::format("Function {} not found", name));
        return {};
    }
    sa::tl::ObjectPtr object = MakeObject(*meta, this);
    FunctionArguments args;
    for (const auto& a : (arguments))
        args.push_back(a.second);

    return *(*func)(ctx_, object, args);
}

template<>
sa::tl::MetaTable& Register<Object>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<Object>();
    result
        .AddFunction<Object, void>("destroy", &Object::Destroy)
        .AddFunction<Object, bool, const std::string&>("has_property", &Object::HasProperty)
        .AddFunction<Object, Value, const std::string&>("get_property", &Object::GetProperty)
        .AddFunction<Object, void, const std::string&, ValuePtr>("set_property", &Object::SetProperty)
        .AddFunction<Object, bool, const std::string&>("has_function", &Object::HasFunction)
        .AddFunction<Object, Value, const std::string&, const Table&>("call_function", &Object::CallFunction)
        .AddFunction<Object, bool, const std::string&>("has_operator", &Object::HasOperator)
        .AddFunction<Object, Value, const std::string&, const Table&>("call_operator", &Object::CallOperator);
    return result;
}

}
