# Samples

Some testing, sample scripts. Most of these scripts require that the standard and debug libraries are loaded. You can do that with:

~~~
load_lib("std");
load_lib("dbg");
~~~

e.g. in the `~/.config/sly/init.tl` script.

Run them with:

~~~sh
$ sly <filename>
~~~
