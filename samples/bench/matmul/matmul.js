'use strict';

const matrix = {
    new: function (n) {
        const a = new Array(n);
        for (let i = 0; i < n; i++) a[i] = new Float64Array(n);
        return a;
    },
    T: function (a, n) {
        const y = matrix.new(n);
        for (let i = 0; i < n; i++) {
            for (let j = 0; j < n; j++) {
                y[i][j] = a[j][i];
            }
        }
        return y;
    },
    mul: function (a, b, n) {
        const y = matrix.new(n);
        const c = matrix.T(b, n);
//        print(c);
        let counter = 0;
        for (let i = 0; i < n; i++) {
            for (let j = 0; j < n; j++) {
                let sum = 0;
                for (let k = 0; k < n; k++) {
                    sum = sum + a[i][k] * c[j][k];
//                    print(++counter, "i", i, "j", j, "k", k, "sum", sum, a[i][k], c[j][k]);
                }
                y[i][j] = sum;
            }
        }
        return y;
    }
};

function matgen(n, seed) {
    const y = matrix.new(n);
    const tmp = seed / n / n;
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            y[i][j] = tmp * (i - j) * (i + j);
        }
    }
    return y;
}

function calc(n) {
    n = n >> 1 << 1;
    const a = matgen(n, 1.0);
//    print(a);
    const b = matgen(n, 2.0);
//    print(b);
    const c = matrix.mul(a, b, n);
//    print(c);
    return c[n / 2][n / 2];
}

const n = 100;
const results = calc(n);
print(results);
