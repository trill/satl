#include <iostream>

// Compile with `g++ -O3 fib.cpp -o fib`

static int fib(int n)
{
	if (n < 2)
		return n;
	return fib(n - 1) + fib(n - 2);
}

int main()
{
	for (int i = 0; i < 100; ++i)
	{
		if (fib(20) != 6765)
		{
			std::cerr << "Error" << std::endl;
			break;
		}
	}
	return 0;
}

