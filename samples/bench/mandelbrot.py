def mandelbrot(N):
    bits  = 0
    nbits = 0
    delta = 2.0 / N
    for y in range(N - 1):
        Ci = y*delta - 1.0
        for x in range(N - 1):
            Cr = x*delta - 1.5

            bit = 0x1
            Zr  = 0.0
            Zi  = 0.0
            Zr2 = 0.0
            Zi2 = 0.0
            for _ in range(50 - 1):
                Zi = 2.0 * Zr * Zi + Ci
                Zr = Zr2 - Zi2 + Cr
                Zi2 = Zi * Zi
                Zr2 = Zr * Zr
                if (Zi2 + Zr2 > 4.0):
                    bit = 0x0
                    break

            bits = (bits << 1) | bit
            nbits = nbits + 1

            if (nbits == 8):
                print("0" if bits == 0 else "1", end = "")
                bits  = 0
                nbits = 0

        if (nbits > 0):
            bits  = bits << (8 - nbits)
            print("0" if bits == 0 else "1", end = "")
            bits  = 0
            nbits = 0


N = 200
print("P4\n", N, N)
mandelbrot(N)
