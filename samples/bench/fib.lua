local function fib(n)
    if n < 2 then
        return n
    end

    return fib(n-1) + fib(n-2)
end

for _ = 0, 100, 1 do
    assert(fib(20) == 6765)
end
