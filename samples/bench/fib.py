def fib(n):
    if n < 2:
        return n

    return fib(n-1) + fib(n-2)

for i in range(100):
    assert fib(20) == 6765
