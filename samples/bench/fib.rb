def fib(n)
  return n if n < 2
  fib(n - 1) + fib(n - 2)
end

for i in 0..100
    if (fib(20) != 6765)
        raise "Error"
    end
end
