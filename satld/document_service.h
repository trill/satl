/**
 * Copyright 2022-2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "service.h"
#include <satl.h>
#include <map>
#include <ast.h>
#include "protocol.h"

using ScopeNode = std::pair<sa::tl::ScopeID, sa::tl::NodePtr>;
using NodeList = std::vector<ScopeNode>;

struct Document
{
    sa::tl::NodePtr root;
    NodeList nodes;
    std::vector<sa::tl::Token> comments;
    std::vector<sa::tl::Error> errors;
    std::vector<std::string> includes;
};

class DocumentService final : public Service
{
public:
    DocumentService();
    ~DocumentService() override = default;
    bool HandleRequest(const lsp::RequestMessage& request) override;
private:
    static const sa::tl::Node* GetPreviousNode(const Document& doc, const sa::tl::Node* closest, const sa::tl::ScopeID& scope, const std::function<bool(const sa::tl::Node&)>& matcher);
    static const sa::tl::Node* FindDefinition(const Document& doc, const sa::tl::Node* closest, const sa::tl::ScopeID& scope, sa::tl::ScopeID* defScope);
    static std::vector<const sa::tl::Node*> GetReferences(const Document& doc, const sa::tl::Node& definition, const sa::tl::ScopeID& defScope);
    static const sa::tl::Node* GetNodeAt(const Document& doc, const sa::tl::Location& loc, sa::tl::ScopeID* scope);
    static const sa::tl::Node* GetClosestNode(const Document& doc, const sa::tl::Location& loc, const std::function<bool(const sa::tl::Node&)>& matcher, sa::tl::ScopeID* scope);
    static Document ParseDocument(const std::string& uri, const std::string& contents);

    void HandleDocumentDidChange(const lsp::RequestMessage& request);
    void HandleDocumentDidOpen(const lsp::RequestMessage& request);
    void HandleDocumentDidDSave(const lsp::RequestMessage& request);
    void HandleSignatureHelp(const lsp::RequestMessage& request);
    void HandleDocumentSymbol(const lsp::RequestMessage& request);
    void HandleDocumentDefinition(const lsp::RequestMessage& request);
    void HandleDocumentReferences(const lsp::RequestMessage& request);
    void HandleDocumentHighlight(const lsp::RequestMessage& request);
    void HandleDocumentHover(const lsp::RequestMessage& request);

    Document GetDocument(const std::string& uri);
    void AddDocument(const std::string& uri, const std::string& text);
    void ChangeDocument(const std::string& uri, const std::string& text);
    lsp::PublishDiagnosticsParams GetDiagnostics(const std::string& uri);
    void DiagnoseDocument(const std::string& uri, bool withIncludes = true, bool forceResult = true);
    void DiagnoseDependants(const std::string& uri);

    std::map<std::string, Document> documents_;
};
