/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "message_buffer.h"
#include <cstdlib>

MessageBuffer::MessageBuffer() = default;

void MessageBuffer::Append(char c)
{
    raw_ += c;
    Process();
}

void MessageBuffer::Append(const std::string& s)
{
    raw_.append(s);
    Process();
}

void MessageBuffer::Process()
{
    auto header = ParseHeader();
    if (!header.first.empty())
    {
        headers_[header.first] = header.second;
        raw_.clear();
    }

    if (raw_ == "\r\n")
    {
        raw_.clear();
        state_ = State::Body;
    }

    if (state_ == State::Body)
    {
        const auto h = headers_.find("Content-Length");
        if (h != headers_.end())
        {
            auto length = std::atoi(h->second.c_str());
            if (length == (int)raw_.length())
            {
                body_ = sa::json::Parse(raw_);
            }
        }
    }
}

bool MessageBuffer::IsComplete() const
{
    return (state_ == State::Body && !body_.Empty());
}

std::pair<std::string, std::string> MessageBuffer::ParseHeader()
{
    auto eol = raw_.find("\r\n");
    if (eol != std::string::npos)
    {
        std::string header = raw_.substr(0, eol);
        auto delim = header.find(':');
        if (delim != std::string::npos)
        {
            std::string name = header.substr(0, delim);
            std::string value = header.substr(delim + 1);
            return std::make_pair(name, value);
        }
    }
    return std::make_pair("", "");
}

void MessageBuffer::Clear()
{
    state_ = State::Header;
    body_ = sa::json::JSON();
    raw_.clear();
}
