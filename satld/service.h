/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <json.h>
#include <non_copyable.h>
#include "protocol.h"

class Service
{
    SATL_NON_COPYABLE(Service);
    SATL_NON_MOVEABLE(Service);
public:
    Service();
    virtual ~Service() = default;
    virtual bool HandleRequest(const lsp::RequestMessage& request) = 0;
protected:
    static void WriteResponse(const sa::json::JSON& json);
};
