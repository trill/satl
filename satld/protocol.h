/**
 * Copyright 2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <location.h>
#include <json_adapter.h>
#include <json.h>
#include <json_serialize.h>
#include <vector>

namespace lsp {

enum class ErrorCode
{
    // Defined by JSON-RPC
    ParseError = -32700,
    InvalidRequest = -32600,
    MethodNotFound = -32601,
    InvalidParams = -32602,
    InternalError = -32603,

    /**
     * This is the start range of JSON-RPC reserved error codes.
     * It doesn't denote a real error code. No LSP error codes should
     * be defined between the start and end range. For backwards
     * compatibility the `ServerNotInitialized` and the `UnknownErrorCode`
     * are left in the range.
     *  * @since 3.16.0
     */
    jsonrpcReservedErrorRangeStart = -32099,

    /**
     * Error code indicating that a server received a notification or
     * request before the server has received the `initialize` request.
     */
    ServerNotInitialized = -32002,
    UnknownErrorCode = -32001,

    /**
     * This is the end range of JSON-RPC reserved error codes.
     * It doesn't denote a real error code.
     *  * @since 3.16.0
     */
    jsonrpcReservedErro = -32000,

    /**
     * This is the start range of LSP reserved error codes.
     * It doesn't denote a real error code.
     *  * @since 3.16.0
     */
    lspReservedErrorRangeStart = -32899,

    /**
     * A request failed but it was syntactically correct, e.g the
     * method name was known and the parameters were valid. The error
     * message should contain human readable information about why
     * the request failed.
     *  * @since 3.17.0
     */
    RequestFailed = -32803,

    /**
     * The server cancelled the request. This error code should
     * only be used for requests that explicitly support being
     * server cancellable.
     *  * @since 3.17.0
     */
    ServerCancelled = -32802,

    /**
     * The server detected that the content of a document got
     * modified outside normal conditions. A server should
     * NOT send this error code if it detects a content change
     * in it unprocessed messages. The result even computed
     * on an older state might still be useful for the client.
     *  * If a client decides that a result is not of any use anymore
     * the client should cancel the request.
     */
    ContentModified = -32801,

    /**
     * The client has canceled a request and a server has detected
     * the cancel.
     */
    RequestCancelled = -32800,

    /**
     * This is the end range of LSP reserved error codes.
     * It doesn't denote a real error code.
     *  * @since 3.16.0
     */
    lspReservedErrorRangeEnd = -32800,
};

#define _STRINGIFY(x) #x
#define STRINGIFY(x) _STRINGIFY(x)
#define VALUE(v) ar.value(STRINGIFY(v), v);
#define VALUE_OPT(v) ar.value(STRINGIFY(v), v, true);

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#requestMessage
// We get this from the client
struct RequestMessage
{
    std::string jsonrpc = "2.0";
    sa::json::JSON id;
    std::string method;
    sa::json::JSON params;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(jsonrpc);
        // When id is missing it's a NotificationMessage, but we don't know beforehand.
        VALUE_OPT(id);
        VALUE(method);
        VALUE(params);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#notificationMessage
struct NotificationMessage
{
    std::string jsonrpc = "2.0";
    std::string method;
    sa::json::JSON params;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(jsonrpc);
        VALUE(method);
        VALUE(params);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#responseError
struct ResponseError
{
    ErrorCode code;
    std::string message;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(code);
        VALUE(message);
    }
};

inline sa::json::JSON GetErrorJSON(ErrorCode code, std::string message)
{
    lsp::ResponseError error {
        .code = code,
        .message = std::move(message)
    };
    return sa::json::Serialize(error);
}

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#responseMessage
struct ResponseMessage
{
    std::string jsonrpc = "2.0";
    // integer | string
    sa::json::JSON id;
    sa::json::JSON result;
    sa::json::JSON error;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(jsonrpc);
        VALUE(id);
        VALUE_OPT(result);
        VALUE_OPT(error);
    }
};

enum class TextDocumentSyncKind
{
    None = 0,
    Full = 1,
    Incremental = 2
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentSyncOptions
struct TextDocumentSyncOptions
{
    bool openClose;
    TextDocumentSyncKind change;

    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(openClose);
        VALUE(change);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#completionOptions
struct CompletionOptions
{
    std::vector<std::string> triggerCharacters;
    std::vector<std::string> allCommitCharacters;
    bool resolveProvider;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE_OPT(triggerCharacters);
        VALUE_OPT(allCommitCharacters);
        VALUE(resolveProvider);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#signatureHelpOptions
struct SignatureHelpOptions
{
    std::vector<std::string> triggerCharacters;
    std::vector<std::string> retriggerCharacters;
    bool Empty() const
    {
        return triggerCharacters.empty() && retriggerCharacters.empty();
    }
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE_OPT(triggerCharacters);
        VALUE_OPT(retriggerCharacters);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#codeLensOptions
struct CodeLensOptions
{
    bool resolveProvider;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(resolveProvider);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#documentOnTypeFormattingOptions
struct DocumentOnTypeFormattingOptions
{
    std::string firstTriggerCharacter;
    std::string moreTriggerCharacter;
    bool Empty() const
    {
        return firstTriggerCharacter.empty() && moreTriggerCharacter.empty();
    }
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(firstTriggerCharacter);
        VALUE_OPT(moreTriggerCharacter);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#documentLinkOptions
struct DocumentLinkOptions
{
    bool resolveProvider;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(resolveProvider);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#executeCommandOptions
struct ExecuteCommandOptions
{
    std::vector<std::string> commands;
    bool Empty() const
    {
        return commands.empty();
    }
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(commands);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#serverCapabilities
struct ServerCapabilities
{
    std::string positionEncoding;
    TextDocumentSyncOptions textDocumentSync;
    bool hoverProvider;
    CompletionOptions completionProvider;
    SignatureHelpOptions signatureHelpProvider;
    bool definitionProvider;
    bool declarationProvider;
    bool referencesProvider;
    bool documentHighlightProvider;
    bool documentSymbolProvider;
    bool workspaceSymbolProvider;
    bool codeActionProvider;
    CodeLensOptions codeLensProvider;
    bool documentFormattingProvider;
    bool documentRangeFormattingProvider;
    DocumentOnTypeFormattingOptions documentOnTypeFormattingProvider;
    bool renameProvider;
    DocumentLinkOptions documentLinkProvider;
    ExecuteCommandOptions executeCommandProvider;

    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE_OPT(positionEncoding);
        VALUE_OPT(textDocumentSync);
        VALUE(hoverProvider);
        VALUE_OPT(completionProvider);
        VALUE_OPT(signatureHelpProvider);
        VALUE(definitionProvider);
        VALUE(declarationProvider);
        VALUE(referencesProvider);
        VALUE(documentHighlightProvider);
        VALUE(documentSymbolProvider);
        VALUE(workspaceSymbolProvider);
        VALUE(codeActionProvider);
        VALUE_OPT(codeLensProvider);
        VALUE(documentFormattingProvider);
        VALUE(documentRangeFormattingProvider);
        VALUE_OPT(documentOnTypeFormattingProvider);
        VALUE(renameProvider);
        VALUE(documentLinkProvider);
        VALUE(executeCommandProvider);
    }
};

struct ServerInfo
{
    std::string name;
    std::string version;
    bool Empty() const
    {
        return name.empty() && version.empty();
    }
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(name);
        VALUE_OPT(version);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#initializeResult
struct InitializeResult
{
    ServerCapabilities capabilities;
    ServerInfo serverInfo;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(capabilities);
        VALUE_OPT(serverInfo);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#position
// Zero based
using Position = sa::tl::Location;

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#range
struct Range
{
    Position start;
    Position end;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(start);
        VALUE(end);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#location
struct Location
{
    Location() = default;
    Location(const Position& start, const Position& end) :
        uri(start.file),
        range({ start, end })
    { }
    std::string uri;
    Range range;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(uri);
        VALUE(range);
    }
};

using Locations = std::vector<Location>;

enum class SymbolKind
{
    File = 1,
    Module = 2,
    Namespace = 3,
    Package = 4,
    Class = 5,
    Method = 6,
    Property = 7,
    Field = 8,
    Constructor = 9,
    Enum = 10,
    Interface = 11,
    Function = 12,
    Variable = 13,
    Constant = 14,
    String = 15,
    Number = 16,
    Boolean = 17,
    Array = 18,
    Object = 19,
    Key = 20,
    Null = 21,
    EnumMember = 22,
    Struct = 23,
    Event = 24,
    Operator = 25,
    TypeParameter = 26,
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#documentSymbol
struct DocumentSymbol
{
    std::string name;
    std::string detail;
    SymbolKind kind;
    Range range;
    Range selectionRange;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(name);
        VALUE_OPT(detail);
        VALUE(kind);
        VALUE(range);
        VALUE(selectionRange);
    }
};

using DocumentSymbols = std::vector<DocumentSymbol>;

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#symbolInformation
struct SymbolInformation
{
    std::string name;
    SymbolKind kind;
    Location location;
    std::string containerName;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(name);
        VALUE(kind);
        VALUE(location);
        VALUE_OPT(containerName);
    }
};

enum class DocumenHightlightKind
{
    Text = 1,
    Read = 2,
    Write = 3
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#documentHighlight
struct DocumentHighlight
{
    Range range;
    DocumenHightlightKind kind;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(range);
        VALUE(kind);
    }
};

using DocumentHighlights = std::vector<DocumentHighlight>;

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#parameterInformation
struct ParameterInformation
{
    std::string label;
    std::string documentation;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(label);
        VALUE_OPT(documentation);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#signatureInformation
struct SignatureInformation
{
    std::string label;
    std::string documentation;
    std::vector<ParameterInformation> parameters;
    int activeParameter = 0;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(label);
        VALUE_OPT(documentation);
        VALUE_OPT(parameters);
        VALUE(activeParameter);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#signatureHelp
struct SignatureHelp
{
    std::vector<SignatureInformation> signatures;
    int activeSignature = 0;
    int activeParameter = 0;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(signatures);
        VALUE(activeSignature);
        VALUE(activeParameter);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#markupContentInnerDefinition
struct MarkupContent
{
    // 'plaintext' | 'markdown'
    std::string kind;
    std::string value;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(kind);
        VALUE(value);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#hover
struct Hover
{
    MarkupContent contents;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(contents);
    }
};

enum class DiagnosticSeverity
{
    Error = 1,
    Warning = 2,
    Information = 3,
    Hint = 4
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#diagnostic
struct Diagnostic
{
    Range range;
    DiagnosticSeverity severity;
    int code;
    std::string source;
    std::string message;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(range);
        VALUE(severity);
        VALUE(code);
        VALUE_OPT(source);
        VALUE(message);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#publishDiagnosticsParams
struct PublishDiagnosticsParams
{
    std::string uri;
    std::vector<Diagnostic> diagnostics;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(uri);
        VALUE(diagnostics);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentIdentifier
struct TextDocumentIdentifier
{
    std::string uri;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(uri);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentPositionParams
struct TextDocumentPositionParams
{
    TextDocumentIdentifier textDocument;
    Position position;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(textDocument);
        VALUE(position);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#documentSymbolParams
struct DocumentSymbolParams
{
    TextDocumentIdentifier textDocument;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(textDocument);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentItem
struct TextDocumentItem
{
    std::string uri;
    std::string languageId;
    int version;
    std::string text;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(uri);
        VALUE(languageId);
        VALUE(version);
        VALUE(text);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#didOpenTextDocumentParams
struct DidOpenTextDocumentParams
{
    TextDocumentItem textDocument;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(textDocument);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#versionedTextDocumentIdentifier
struct VersionedTextDocumentIdentifier : public TextDocumentIdentifier
{
    int version;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        TextDocumentIdentifier::Serialize(ar);
        VALUE(version);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentContentChangeEvent
struct TextDocumentContentChangeEvent
{
    Range range;
    int rangeLength;
    std::string text;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE_OPT(range);
        VALUE_OPT(rangeLength);
        VALUE(text);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#didChangeTextDocumentParams
struct DidChangeTextDocumentParams
{
    VersionedTextDocumentIdentifier textDocument;
    std::vector<TextDocumentContentChangeEvent> contentChanges;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(textDocument);
        VALUE(contentChanges);
    }
};

// https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#didSaveTextDocumentParams
struct DidSaveTextDocumentParams
{
    TextDocumentIdentifier textDocument;
    std::string text;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        VALUE(textDocument);
        VALUE_OPT(text);
    }
};

} // namespace lsp

template<>
struct sa::json::Adapter<lsp::Position>
{
    template<typename _Ar>
    void Serialize(lsp::Position& value, _Ar& ar)
    {
        ar.value("line", value.line);
        ar.value("character", value.col);
    }
};
