/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "json.h"
#include <string>
#include <map>

struct CILess
{
    bool operator()(const std::string& s1, const std::string& s2) const
    {
        std::string str1(s1.length(), ' ');
        std::string str2(s2.length(), ' ');
        std::transform(s1.begin(), s1.end(), str1.begin(), ::tolower);
        std::transform(s2.begin(), s2.end(), str2.begin(), ::tolower);
        return str1 < str2;
    }
};

class MessageBuffer
{
public:
    MessageBuffer();
    void Append(char c);
    void Append(const std::string& s);
    bool IsComplete() const;
    const sa::json::JSON& GetBody() const { return body_; }
    const std::map<std::string, std::string, CILess>& GetHeaders() const { return headers_; }
    void Clear();
private:
    std::pair<std::string, std::string> ParseHeader();
    void Process();
    enum class State
    {
        Header,
        Body
    } state_ { State::Header };
    sa::json::JSON body_;
    std::map<std::string, std::string, CILess> headers_;
    std::string raw_;
};
