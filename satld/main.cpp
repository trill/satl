/**
 * Copyright (c) 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <getopt.h>
#include <iostream>
#include <version.h>
#include "application.h"

static void ShowInfo()
{
    std::cout << "satld - sa::tl Language Server" << std::endl;
    std::cout << "Copyright (C) 2022-2025 Stefan Ascher" << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
}

static void ShowHelp(const char* arg0)
{
    std::cout << "Usage: " << arg0 << " [<options>]" << std::endl << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "  -l, --log <filename>   Log to file" << std::endl;
    std::cout << "  -h, --help             Show help and exit" << std::endl;
    std::cout << "  -v, --version          Show program version and exit" << std::endl << std::endl;
}

int main(int argc, char** argv)
{
    int ac;
    std::string logFile;

    static struct option longOptions[] = {
        { "log", required_argument, NULL, 'l' },
        { "help", no_argument, NULL, 'h' },
        { "version", no_argument, NULL, 'v' },
        { NULL, 0, NULL, 0 } };

    int optionIndex = 0;

    while ((ac = getopt_long(argc, argv, "l:hv", longOptions, &optionIndex)) != -1)
    {
        switch (ac)
        {
        case 'l':
            logFile = optarg;
            break;
        case 'h':
            ShowInfo();
            std::cout << std::endl;
            ShowHelp(argv[0]);
            exit(0);
        case 'v':
            std::cout << "satld " << SATL_VERSION_STRING << std::endl;
            exit(0);
        default:
            break;
        }
    }

    Application app(logFile);
    return app.Run();
}
