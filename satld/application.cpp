/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "application.h"
#include <iostream>
#include <json_serialize.h>
#include <json_deserialize.h>
#include <format>
#include <version.h>

Application* Application::Instance = nullptr;

Application::Application(const std::string& logFile)
{
    Application::Instance = this;
    documentsService_ = std::make_unique<DocumentService>();
    if (!logFile.empty())
    {
        logStream_.open(logFile);
        if (!logStream_.is_open())
            std::cerr << "Unable to open log file " << logFile << std::endl;
    }
}

int Application::Run()
{
    try
    {
        MessageBuffer buffer;
        char c = 0;
        while (std::cin.get(c))
        {
            buffer.Append(c);
            if (buffer.IsComplete())
            {
                HandleMessage(buffer);
                if (logStream_.is_open())
                    logStream_.flush();
                buffer.Clear();
            }
        }
    }
    catch (const std::exception& ex)
    {
        *this << "EXCEPTION: " << ex.what() << std::endl;
        return 6;
    }
    return 0;
}

void Application::HandleMessage(const MessageBuffer& buffer)
{
    const sa::json::JSON& body = buffer.GetBody();
    *this << "================== REQUEST ==================" << std::endl;
    *this << body << std::endl;

    auto request = sa::json::DeserializeSafe<lsp::RequestMessage>(body);
    if (!request.has_value())
    {
        lsp::ResponseMessage message;
        message.id = body["id"];
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidRequest, std::format("Couldn't parse message. {}", request.error()));
        WriteResponse(sa::json::Serialize(message));
        return;
    }

    HandleRequest(*request);
}

bool Application::HandleRequest(const lsp::RequestMessage& request)
{
    if (request.method != "initialize" && !initialized_)
    {
        lsp::ResponseMessage message;
        message.id = request.id;
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::ServerNotInitialized,
            std::format("Unexpected method \"{}\". Server not yet initialized.", request.method));
        WriteResponse(sa::json::Serialize(message));
        return false;
    }

    if (request.method.starts_with("textDocument/"))
        return documentsService_->HandleRequest(request);

    if (request.method == "initialized")
        return true;

    if (request.method == "$/cancelRequest")
        return true;

    if (request.method == "initialize")
        return HandleInitialize(request);
    if (request.method == "shutdown")
        return HandleShutdown(request);
    if (request.method == "exit")
        return HandleExit(request);

    lsp::ResponseMessage message;
    message.id = request.id;
    message.error = lsp::GetErrorJSON(lsp::ErrorCode::MethodNotFound, std::format("Unsupported method \"{}\"", request.method));
    WriteResponse(sa::json::Serialize(message));
    return false;
}

bool Application::HandleInitialize(const lsp::RequestMessage& request)
{
    initialized_ = true;

    lsp::InitializeResult result;
    result.serverInfo.name = "satld";
    result.serverInfo.version = SATL_VERSION_STRING;
    result.capabilities.positionEncoding = "utf-8";
    result.capabilities.textDocumentSync.change = lsp::TextDocumentSyncKind::Full;
    result.capabilities.textDocumentSync.openClose = true;
    result.capabilities.hoverProvider = true;
    result.capabilities.completionProvider.resolveProvider = false;
    result.capabilities.signatureHelpProvider.triggerCharacters = { "(", "," };
    result.capabilities.definitionProvider = true;
    result.capabilities.declarationProvider = true;
    result.capabilities.referencesProvider = true;
    result.capabilities.documentHighlightProvider = true;
    result.capabilities.documentSymbolProvider = true;
    result.capabilities.workspaceSymbolProvider = false;
    result.capabilities.codeActionProvider = false;
    result.capabilities.codeLensProvider.resolveProvider = false;
    result.capabilities.documentFormattingProvider = false;
    result.capabilities.renameProvider = false;
    result.capabilities.documentLinkProvider.resolveProvider = false;

    lsp::ResponseMessage message;
    message.id = request.id;
    message.result = sa::json::Serialize(result);
    WriteResponse(sa::json::Serialize(message));
    return true;
}

bool Application::HandleShutdown(const lsp::RequestMessage&)
{
    shutdown_ = true;
    if (logStream_.is_open())
        logStream_.close();
    return true;
}

bool Application::HandleExit(const lsp::RequestMessage&)
{
    exit(shutdown_ ? 0 : 1);
}
