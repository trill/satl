/**
 * Copyright 2022-2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "service.h"
#include <iostream>
#include "application.h"

Service::Service() = default;

void Service::WriteResponse(const sa::json::JSON& json)
{
    std::string jsonString = json.Dump(true);
    std::string header;
    header.append(std::format("Content-Length: {}\r\n", jsonString.size()));
    header.append("Content-Type: application/vscode-jsonrpc;charset=utf-8\r\n");
    header.append("\r\n");
    *Application::Instance << "================== RESPONSE ==================" << std::endl;
    *Application::Instance << json << std::endl;
    std::cout << header;
    std::cout << jsonString;
}
