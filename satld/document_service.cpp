/**
 * Copyright 2022-2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "document_service.h"
#include <json_deserialize.h>
#include <json_serialize.h>
#include <scope_guard.h>
#include <type_hint.h>
#include <utils.h>
#include <set>
#include "application.h"

DocumentService::DocumentService() = default;

static std::string GetCommentFor(const Document& doc, const sa::tl::Location& location)
{
    for (const auto& c : doc.comments)
    {
        if (c.loc.file != location.file)
            continue;
        auto lines = sa::tl::Split(c.value, "\n", true, true);
        if (c.loc.line + lines.size() == location.line)
            return c.value;
        if (c.loc.line == location.line && c.loc.col > location.col)
            return c.value;
    }
    return "";
}

static std::string GetNodeIdentifier(const sa::tl::Node& nd)
{
    switch (nd.GetClass())
    {
    case sa::tl::Node::Class::CallExpr:
        return sa::tl::To<sa::tl::CallExpr>(nd).GetName();
    case sa::tl::Node::Class::VariableExpr:
        return sa::tl::To<sa::tl::VariableExpr>(nd).GetName();
    case sa::tl::Node::Class::VariableDefinition:
        return sa::tl::To<sa::tl::VariableDefinition>(nd).GetName();
    case sa::tl::Node::Class::Function:
        return sa::tl::To<sa::tl::Function>(nd).GetName();
    case sa::tl::Node::Class::ValueExpr:
        return sa::tl::To<sa::tl::ValueExpr>(nd).GetValue()->ToString();
    default:
        return "";
    }
}

static sa::tl::Location GetNodeEndLocation(const sa::tl::Node& node)
{
    auto ident = GetNodeIdentifier(node);
    if (ident.empty())
        return node.loc_;
    sa::tl::Location result = node.loc_;
    result.col += ident.length();
    return result;
}

static lsp::Range GetLspNodeRange(const sa::tl::Node& node)
{
    return { node.loc_, GetNodeEndLocation(node) };
}

static lsp::Location GetLspNodeLocation(const sa::tl::Node& node)
{
    return { node.loc_, GetNodeEndLocation(node) };
}

static bool LocationInsideNode(const sa::tl::Location& loc, const sa::tl::Node& nd)
{
    if (loc.file != nd.loc_.file)
        return false;
    if (loc.line != nd.loc_.line)
        return false;
    if (loc.col < nd.loc_.col)
        return false;
    std::string name = GetNodeIdentifier(nd);
    if (name.empty())
        return false;
    if (loc.col > nd.loc_.col + name.length())
        return false;
    return true;
}

const sa::tl::Node* DocumentService::GetClosestNode(const Document& doc,
    const sa::tl::Location& loc,
    const std::function<bool(const sa::tl::Node&)>& matcher,
    sa::tl::ScopeID* scope)
{
    for (const auto& nd : doc.nodes)
    {
        if (LocationInsideNode(loc, *nd.second))
        {
            if (matcher(*nd.second))
            {
                if (scope)
                    *scope = nd.first;
                return nd.second.get();
            }
        }
    }
    *Application::Instance << "Nothing found at " << loc << std::endl;
    return nullptr;
}

const sa::tl::Node* DocumentService::GetNodeAt(const Document& doc, const sa::tl::Location& loc, sa::tl::ScopeID* scope)
{
    int mindist = std::numeric_limits<int>::max();
    const sa::tl::Node* result = nullptr;
    for (const auto& nd : doc.nodes)
    {
        auto d = nd.second->loc_ - loc;
        if (d > 0)
            break;
        if (abs(d) < mindist)
        {
            mindist = abs(d);
            if (scope)
                *scope = nd.first;
            result = nd.second.get();
        }
    }
    return result;
}

const sa::tl::Node* DocumentService::GetPreviousNode(const Document& doc,
    const sa::tl::Node* closest,
    const sa::tl::ScopeID& scope,
    const std::function<bool(const sa::tl::Node&)>& matcher)
{
    if (!closest)
        return nullptr;

           // Find the node in the list
    auto it = std::find_if(doc.nodes.rbegin(),
        doc.nodes.rend(),
        [&](const ScopeNode& current)
        {
            return current.second.get() == closest;
        });
    if (it == doc.nodes.rend())
        return nullptr;

    int currentScope = scope.Level();
    // And reverse iterate to find something
    for (auto nd = it + 1; nd != doc.nodes.rend(); ++nd)
    {
        // Walk up the tree to find the definition
        if (nd->first.Level() < currentScope)
            currentScope = nd->first.Level();
        if (nd->first.Level() > currentScope)
            continue;
        if (matcher(*nd->second))
            return nd->second.get();
    }
    return nullptr;
}

const sa::tl::Node* DocumentService::FindDefinition(const Document& doc,
    const sa::tl::Node* closest,
    const sa::tl::ScopeID& scope,
    sa::tl::ScopeID* defScope)
{
    if (!closest)
        return nullptr;

    // Find the node in the list
    auto it = std::find_if(doc.nodes.rbegin(),
        doc.nodes.rend(),
        [&](const ScopeNode& current)
        {
            return current.second.get() == closest;
        });
    if (it == doc.nodes.rend())
        return nullptr;

    auto closestClass = closest->GetClass();
    std::string closestName = GetNodeIdentifier(*closest);

    const sa::tl::Node* result = nullptr;
    if (closestClass == sa::tl::Node::Class::VariableDefinition ||
        closestClass == sa::tl::Node::Class::Function)
        // This may be already the definition
        result = closest;

    int currentScope = scope.Level();
    // And reverse iterate to find something
    for (auto nd = it + 1; nd != doc.nodes.rend(); ++nd)
    {
        // Walk up the tree to find the definition
        if (nd->first.Level() < currentScope)
            currentScope = nd->first.Level();
        if (nd->first.Level() > currentScope)
            continue;

        const sa::tl::Node& current = *nd->second;
        std::string nodeName = GetNodeIdentifier(current);
        if (nodeName.empty())
            continue;

        switch (current.GetClass())
        {
        case sa::tl::Node::Class::Function:
        {
            if (closestClass == sa::tl::Node::Class::CallExpr
                || closestClass == sa::tl::Node::Class::VariableExpr)
            {
                if (nodeName == closestName)
                {
                    result = &current;
                    if (defScope)
                        *defScope = nd->first;
                }
            }
            break;
        }
        case sa::tl::Node::Class::VariableDefinition:
            if (closestClass == sa::tl::Node::Class::VariableExpr
                || closestClass == sa::tl::Node::Class::VariableDefinition
                || closestClass == sa::tl::Node::Class::CallExpr)
            {
                if (nodeName == closestName)
                {
                    result = &current;
                    if (defScope)
                        *defScope = nd->first;
                }
            }
            break;
        default:
            break;
        }
    }

    return result;
}

std::vector<const sa::tl::Node*> DocumentService::GetReferences(const Document& doc, const sa::tl::Node& definition,
    const sa::tl::ScopeID& defScope)
{
    std::vector<const sa::tl::Node*> result;
    std::set<sa::tl::Location> refs;
    // Find the node in the list
    auto it = std::find_if(doc.nodes.begin(),
        doc.nodes.end(),
        [&](const ScopeNode& current)
        {
            return current.second.get() == &definition;
        });
    if (it == doc.nodes.end())
        return result;

    std::string defName = GetNodeIdentifier(definition);
    if (defName.empty())
        return result;
    // Walk down the tree from the defintion
    for (auto nd = it + 1; nd != doc.nodes.end(); ++nd)
    {
        if (nd->first.Level() < defScope.Level())
            continue;
        if (refs.contains(nd->second->loc_))
            continue;
        refs.emplace(nd->second->loc_);

        switch (nd->second->GetClass())
        {
        case sa::tl::Node::Class::Function:
        case sa::tl::Node::Class::CallExpr:
        case sa::tl::Node::Class::VariableExpr:
        case sa::tl::Node::Class::VariableDefinition:
            if (GetNodeIdentifier(*nd->second) == defName)
                result.push_back(nd->second.get());
            break;
        default:
            break;
        }
    }
    return result;
}

Document DocumentService::ParseDocument(const std::string& uri, const std::string& contents)
{
    Document doc;
    sa::tl::Parser parser(contents, uri);
    parser.onInclude_ = [&uri, &parser, &doc](std::string& filename, const sa::tl::Location& loc) -> std::string
    {
        filename = sa::tl::FindRelatedFile(uri, filename);
        std::string source = sa::tl::ReadScriptFile(filename);
        if (source.empty())
            parser.SyntaxError(loc, std::format("File {} not found", filename));
        else
            doc.includes.push_back(filename);
        return source;
    };
    doc.root = parser.Parse();
    if (doc.root)
    {
        sa::tl::ScopeID scope;
        // A flat array is easier to search. This list is sorted by the order of appearance.
        doc.root->ForEachChild(
            [&doc](const sa::tl::Node& nd, const sa::tl::ScopeID& scope)
            {
                doc.nodes.emplace_back(scope, const_cast<sa::tl::Node&>(nd).shared_from_this());
            }, scope);
        doc.comments = parser.GetComments();
    }
    doc.errors = parser.GetErrors();
    return doc;
}

bool DocumentService::HandleRequest(const lsp::RequestMessage& request)
{
    if (!request.method.starts_with("textDocument/"))
        return false;

    if (request.method == "textDocument/didChange")
        HandleDocumentDidChange(request);
    else if (request.method == "textDocument/didOpen")
        HandleDocumentDidOpen(request);
    else if (request.method == "textDocument/didSave")
        HandleDocumentDidDSave(request);
    else if (request.method == "textDocument/documentSymbol")
        HandleDocumentSymbol(request);
    else if (request.method == "textDocument/definition" || request.method == "textDocument/declaration")
        HandleDocumentDefinition(request);
    else if (request.method == "textDocument/references")
        HandleDocumentReferences(request);
    else if (request.method == "textDocument/documentHighlight")
        HandleDocumentHighlight(request);
    else if (request.method == "textDocument/signatureHelp")
        HandleSignatureHelp(request);
    else if (request.method == "textDocument/hover")
        HandleDocumentHover(request);

    return true;
}

void DocumentService::HandleDocumentDidOpen(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_didOpen
    auto params = sa::json::DeserializeSafe<lsp::DidOpenTextDocumentParams>(request.params);
    if (!params.has_value())
    {
        lsp::ResponseMessage message;
        message.id = request.id;
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        WriteResponse(sa::json::Serialize(message));
        return;
    }

    AddDocument(params->textDocument.uri, params->textDocument.text);
    DiagnoseDocument(params->textDocument.uri);
}

void DocumentService::HandleDocumentDidChange(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_didChange
    auto params = sa::json::DeserializeSafe<lsp::DidChangeTextDocumentParams>(request.params);
    if (!params.has_value())
    {
        lsp::ResponseMessage message;
        message.id = request.id;
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        WriteResponse(sa::json::Serialize(message));
        return;
    }

    std::string text;
    if (!params->contentChanges.empty())
    {
        // Just assume the client sent the whole file in one change event
        text = params->contentChanges[0].text;
    }

    ChangeDocument(params->textDocument.uri, text);
    DiagnoseDocument(params->textDocument.uri);
}

void DocumentService::HandleDocumentDidDSave(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_didSave

    auto params = sa::json::DeserializeSafe<lsp::DidSaveTextDocumentParams>(request.params);
    if (!params.has_value())
    {
        lsp::ResponseMessage message;
        message.id = request.id;
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        WriteResponse(sa::json::Serialize(message));
        return;
    }
    DiagnoseDependants(params->textDocument.uri);
}

void DocumentService::DiagnoseDependants(const std::string& uri)
{
    for (const auto& doc : documents_)
    {
        if (std::find(doc.second.includes.begin(), doc.second.includes.end(), uri) != doc.second.includes.end())
        {
            // If this document is found in their includes we must revalidate this as well
            documents_.erase(doc.first);
            DiagnoseDocument(doc.first, false, true);
        }
    }
}

void DocumentService::HandleDocumentSymbol(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_documentSymbol
    // Get all symbols in tree

    lsp::ResponseMessage message;
    sa::tl::ScopeGuard sg([&]()
    {
        message.id = request.id;
        WriteResponse(sa::json::Serialize(message));
    });

    auto params = sa::json::DeserializeSafe<lsp::DocumentSymbolParams>(request.params);
    if (!params.has_value())
    {
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        return;
    }

    Document doc = GetDocument(params->textDocument.uri);

    lsp::DocumentSymbols symbols;

    std::vector<const sa::tl::Node*> nodeList;

    bool isConst = false;
    bool isGlobal = false;
    for (const auto& nd : doc.nodes)
    {
        if (nd.second->loc_.file != params->textDocument.uri)
            continue;

        if (nd.second->GetClass() == sa::tl::Node::Class::Assign)
        {
            isConst = sa::tl::To<sa::tl::Assign>(*nd.second).IsConst();
            isGlobal = sa::tl::To<sa::tl::Assign>(*nd.second).IsGlobal();
        }
        else if (nd.second->GetClass() != sa::tl::Node::Class::VariableDefinition)
        {
            isConst = false;
            isGlobal = false;
        }
        if (!(nd.first.Level() == 0 || isGlobal))
            // Return only smybols in global scope
            continue;

        std::string ndName = GetNodeIdentifier(*nd.second);
        if (ndName.empty())
            continue;
        sa::tl::ScopeID defScope;
        const auto* targetNode = FindDefinition(doc, nd.second.get(), nd.first, &defScope);
        if (std::find(nodeList.begin(), nodeList.end(), targetNode) != nodeList.end())
            continue;

        switch (nd.second->GetClass())
        {
        case sa::tl::Node::Class::Function:
        {
            lsp::DocumentSymbol symbol;
            symbol.name = ndName;
            symbol.kind = lsp::SymbolKind::Function;
            symbol.range = GetLspNodeRange(*nd.second);
            symbol.selectionRange = symbol.range;
            symbols.push_back(std::move(symbol));
            nodeList.push_back(nd.second.get());
            break;
        }
        case sa::tl::Node::Class::VariableDefinition:
        {
            lsp::DocumentSymbol symbol;
            symbol.name = ndName;
            const auto& varDef = sa::tl::To<sa::tl::VariableDefinition>(*nd.second);
            switch (varDef.typeHint_)
            {
            case sa::tl::TypeHint::Bool:
                symbol.kind = lsp::SymbolKind::Boolean;
                break;
            case sa::tl::TypeHint::Int:
            case sa::tl::TypeHint::Float:
                symbol.kind = lsp::SymbolKind::Number;
                break;
            case sa::tl::TypeHint::Function:
                symbol.kind = lsp::SymbolKind::Function;
                break;
            case sa::tl::TypeHint::Object:
                symbol.kind = lsp::SymbolKind::Object;
                break;
            case sa::tl::TypeHint::String:
                symbol.kind = lsp::SymbolKind::String;
                break;
            case sa::tl::TypeHint::Table:
                symbol.kind = lsp::SymbolKind::Array;
                break;
            default:
                // A symbol can be just one kind, that's unfortunate
                if (isConst)
                    symbol.kind = lsp::SymbolKind::Constant;
                else
                    symbol.kind = lsp::SymbolKind::Variable;
                break;
            }

            symbol.range = GetLspNodeRange(*nd.second);
            symbol.selectionRange = symbol.range;
            symbols.push_back(std::move(symbol));
            nodeList.push_back(nd.second.get());
            break;
        }
        default:
            break;
        }
    }

    message.result = sa::json::Serialize(symbols);
}

void DocumentService::HandleDocumentDefinition(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_definition

    lsp::ResponseMessage message;
    sa::tl::ScopeGuard sg([&]()
    {
        message.id = request.id;
        WriteResponse(sa::json::Serialize(message));
    });

    auto params = sa::json::DeserializeSafe<lsp::TextDocumentPositionParams>(request.params);
    if (!params.has_value())
    {
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        return;
    }

    params->position.file = params->textDocument.uri;

    Document doc = GetDocument(params->textDocument.uri);
    sa::tl::ScopeID scope;
    const auto* closest = GetClosestNode(
        doc,
        params->position,
        [](const sa::tl::Node& nd) -> bool {
            return nd.GetClass() == sa::tl::Node::Class::CallExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableExpr;
        },
        &scope);

    sa::tl::ScopeID defScope;
    const auto* targetNode = FindDefinition(doc, closest, scope, &defScope);

    if (targetNode)
    {
        auto location = GetLspNodeLocation(*targetNode);
        message.result = sa::json::Serialize(location);
    }
}

void DocumentService::HandleDocumentReferences(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_references
    // Find the definition at location and return all references to it

    lsp::ResponseMessage message;
    sa::tl::ScopeGuard sg([&]()
    {
        message.id = request.id;
        WriteResponse(sa::json::Serialize(message));
    });

    auto params = sa::json::DeserializeSafe<lsp::TextDocumentPositionParams>(request.params);
    if (!params.has_value())
    {
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        return;
    }

    params->position.file = params->textDocument.uri;

    lsp::Locations locations;

    Document doc = GetDocument(params->textDocument.uri);
    sa::tl::ScopeID closestScope;
    const auto* closest = GetClosestNode(
        doc,
        params->position,
        [](const sa::tl::Node& nd) -> bool
        {
            return nd.GetClass() == sa::tl::Node::Class::CallExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableDefinition
                || nd.GetClass() == sa::tl::Node::Class::Function;
        },
        &closestScope);

    if (!closest)
        return;

    sa::tl::ScopeID defScope;
    const sa::tl::Node* definition = nullptr;
    if (closest && closest->GetClass() == sa::tl::Node::Class::Function)
        // It is already the definition
        definition = closest;
    else
        definition = FindDefinition(doc, closest, closestScope, &defScope);

    if (!definition)
        return;

    // Add definition itself
    locations.push_back(GetLspNodeLocation(*definition));

    auto refs = GetReferences(doc, *definition, defScope);
    for (const auto& ref : refs)
    {
        locations.push_back(GetLspNodeLocation(*ref));
    }

    message.result = sa::json::Serialize(locations);
}

void DocumentService::HandleDocumentHighlight(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_documentHighlight

    lsp::ResponseMessage message;
    sa::tl::ScopeGuard sg([&]()
    {
        message.id = request.id;
        WriteResponse(sa::json::Serialize(message));
    });

    auto params = sa::json::DeserializeSafe<lsp::TextDocumentPositionParams>(request.params);
    if (!params.has_value())
    {
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        return;
    }
    params->position.file = params->textDocument.uri;

    Document doc = GetDocument(params->textDocument.uri);

    sa::tl::ScopeID closestScope;
    const auto* closest = GetClosestNode(
        doc,
        params->position,
        [](const sa::tl::Node& nd) -> bool
        {
            return nd.GetClass() == sa::tl::Node::Class::CallExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableDefinition
                || nd.GetClass() == sa::tl::Node::Class::Function;
        },
        &closestScope);

    if (!closest)
        return;

    sa::tl::ScopeID defScope;
    const sa::tl::Node* definition = nullptr;
    if (closest && closest->GetClass() == sa::tl::Node::Class::Function)
        // It is already the definition
        definition = closest;
    else
        definition = FindDefinition(doc, closest, closestScope, &defScope);

    if (!definition)
        return;

    lsp::DocumentHighlights highlights;
    highlights.push_back({ GetLspNodeRange(*definition), lsp::DocumenHightlightKind::Write });

    auto refs = GetReferences(doc, *definition, defScope);
    for (const auto& ref : refs)
    {
        lsp::DocumenHightlightKind kind = lsp::DocumenHightlightKind::Text;
        if (sa::tl::Is<sa::tl::VariableDefinition>(*ref))
            kind = lsp::DocumenHightlightKind::Write;
        else if (sa::tl::Is<sa::tl::VariableExpr>(*ref))
            kind = lsp::DocumenHightlightKind::Read;

        highlights.push_back({ GetLspNodeRange(*ref), kind });
    }

    message.result = sa::json::Serialize(highlights);
}

void DocumentService::HandleSignatureHelp(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_signatureHelp

    lsp::ResponseMessage message;
    sa::tl::ScopeGuard sg([&]()
    {
        message.id = request.id;
        WriteResponse(sa::json::Serialize(message));
    });

    auto params = sa::json::DeserializeSafe<lsp::TextDocumentPositionParams>(request.params);
    if (!params.has_value())
    {
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        return;
    }
    params->position.file = params->textDocument.uri;


    Document doc = GetDocument(params->textDocument.uri);
    sa::tl::ScopeID level;
    const auto* closest = GetNodeAt(doc, params->position, &level);
    if (!closest)
        return;

    const auto* previous = GetPreviousNode(doc, closest, level, [](const sa::tl::Node& current) -> bool
    {
        return current.GetClass() == sa::tl::Node::Class::CallExpr;
    });
    if (!previous)
        return;

    int activeParameterIndex = -1;

    const auto& callExprArgs = sa::tl::To<sa::tl::CallExpr>(*previous).GetArguments();
    for (int i = 0; i < (int)callExprArgs.size(); ++i)
    {
        if (callExprArgs[i].get() == closest)
        {
            activeParameterIndex = i + 1;
            break;
        }
    }

    sa::tl::ScopeID defScope;
    const auto* definition = FindDefinition(doc, previous, level, &defScope);

    lsp::SignatureHelp signatureHelp;
    if (definition && definition->GetClass() == sa::tl::Node::Class::Function)
    {
        lsp::SignatureInformation signature;
        const auto& func = sa::tl::To<sa::tl::Function>(*definition);
        if (!func.GetName().empty())
            signature.label = func.GetName() + "(";
        else
            signature.label = "(anonymous)(";
        if (activeParameterIndex >= func.ParamCount())
            activeParameterIndex = -1;


        for (int i = 0; i < func.ParamCount(); ++i)
        {
            lsp::ParameterInformation parameter;
            parameter.label = func.GetParameterName(i);
            signature.label += parameter.label;
            signature.label += ": " + sa::tl::TypeHintToString(func.GetParameterType(i));
            if (i < func.ParamCount() - 1)
                signature.label += ", ";
            signature.parameters.push_back(std::move(parameter));
        }

        signature.label += ")";
        if (func.typeHint_ != sa::tl::TypeHint::Any)
            signature.label += ": " + sa::tl::TypeHintToString(func.typeHint_);

        if (activeParameterIndex != -1)
            signatureHelp.activeParameter = activeParameterIndex;
        signatureHelp.signatures.push_back(std::move(signature));
    }

    message.result = sa::json::Serialize(signatureHelp);
}

void DocumentService::HandleDocumentHover(const lsp::RequestMessage& request)
{
    // https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_hover
    // The hover request is sent from the client to the server to request hover information at a given text document
    // position.

    lsp::ResponseMessage message;
    sa::tl::ScopeGuard sg([&]()
    {
        message.id = request.id;
        WriteResponse(sa::json::Serialize(message));
    });

    auto params = sa::json::DeserializeSafe<lsp::TextDocumentPositionParams>(request.params);
    if (!params.has_value())
    {
        message.error = lsp::GetErrorJSON(lsp::ErrorCode::InvalidParams, params.error());
        return;
    }
    params->position.file = params->textDocument.uri;

    auto getFunctionSignature = [](const sa::tl::Function& function) -> std::string
    {
        std::string result;
        if (!function.GetName().empty())
            result = function.GetName() + "(";
        else
            result = "(anonymous)(";
        for (int i = 0; i < function.ParamCount(); ++i)
        {
            result += function.GetParameterName(i);
            result += ": " + sa::tl::TypeHintToString(function.GetParameterType(i));
            if (i < function.ParamCount() - 1)
                result += ", ";
        }

        result += ")";
        if (function.typeHint_ != sa::tl::TypeHint::Any)
            result += ": " + sa::tl::TypeHintToString(function.typeHint_);

        return result;
    };

    Document doc = GetDocument(params->textDocument.uri);
    sa::tl::ScopeID level;
    const auto* closest = GetClosestNode(
        doc,
        params->position,
        [](const sa::tl::Node& nd) -> bool
        {
            return nd.GetClass() == sa::tl::Node::Class::CallExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableExpr
                || nd.GetClass() == sa::tl::Node::Class::VariableDefinition
                || nd.GetClass() == sa::tl::Node::Class::Function;
        },
        &level);

    if (!closest)
        return;

    sa::tl::ScopeID defScope;
    const sa::tl::Node* definition = nullptr;
    if (closest && closest->GetClass() == sa::tl::Node::Class::Function)
        // It is already the definition
        definition = closest;
    else
        definition = FindDefinition(doc, closest, level, &defScope);

    lsp::Hover hover;
    std::stringstream content;
    bool haveIdent = false;

    switch (closest->GetClass())
    {
    case sa::tl::Node::Class::CallExpr:
        content << "Function call";
        if (const auto* funcDef = sa::tl::To<sa::tl::Function>(definition))
        {
            content << "\n" << getFunctionSignature(*funcDef);
            haveIdent = true;
        }
        break;
    case sa::tl::Node::Class::VariableExpr:
        content << "Variable";
        break;
    case sa::tl::Node::Class::VariableDefinition:
        if (definition == closest)
            content << "Variable definition";
        else
            content << "Variable";
        break;
    case sa::tl::Node::Class::Function:
        content << "Function definition";
        break;
    default:
        break;
    }
    if (!haveIdent)
    {
        if (const auto* varDef = sa::tl::To<sa::tl::VariableDefinition>(definition))
        {
            content << "\n";
            const auto& name = varDef->GetName();
            if (!name.empty())
                content << " " << name;
            content << ": " << sa::tl::TypeHintToString(varDef->typeHint_);
        }
        else
        {
            std::string name = GetNodeIdentifier(*closest);
            if (!name.empty())
                content << " " << name;
        }
    }

    if (definition)
    {
        auto comment = GetCommentFor(doc, definition->loc_);
        if (!comment.empty())
            content << "\n" << comment;
    }

    if (definition && definition != closest)
        content << "\n\nDefined in:\n" << definition->loc_;

    hover.contents.kind = "markdown";
    hover.contents.value = content.str();
    message.result = sa::json::Serialize(hover);
}

void DocumentService::DiagnoseDocument(const std::string& uri, bool withIncludes, bool forceResult)
{
    auto diag = GetDiagnostics(uri);
    if (!diag.diagnostics.empty() || forceResult)
    {
        lsp::NotificationMessage body {
            .method = "textDocument/publishDiagnostics",
            .params = sa::json::Serialize(diag)
        };
        WriteResponse(sa::json::Serialize(body));
    }

    Document doc = GetDocument(uri);
    if (withIncludes)
    {
        for (const auto& e : doc.includes)
            DiagnoseDocument(e, true, false);
    }
}

lsp::PublishDiagnosticsParams DocumentService::GetDiagnostics(const std::string& uri)
{
    lsp::PublishDiagnosticsParams result;
    result.uri = uri;
    Document doc = GetDocument(uri);

    for (const auto& e : doc.errors)
    {
        lsp::Diagnostic diag;

        diag.range.start = (e.loc.file == uri) ? e.loc : e.included;
        diag.range.end = diag.range.start;
        diag.message = e.message;

        if (e.loc.file != uri)
        {
            std::stringstream ss;
            ss << " (in " << e.loc << ")";
            diag.message += ss.str();
        }

        diag.severity = lsp::DiagnosticSeverity::Error;
        diag.code = e.code;
        diag.source = "satl";

        result.diagnostics.push_back(std::move(diag));
    }

    return result;
}

Document DocumentService::GetDocument(const std::string& uri)
{
    auto it = documents_.find(uri);
    if (it != documents_.end())
        return it->second;

    std::string fileContents = sa::tl::ReadScriptFile(uri);
    if (fileContents.empty())
        return {};

    documents_[uri] = ParseDocument(uri, fileContents);
    return documents_[uri];
}

void DocumentService::AddDocument(const std::string& uri, const std::string& text)
{
    documents_[uri] = ParseDocument(uri, text);
}

void DocumentService::ChangeDocument(const std::string& uri, const std::string& text)
{
    documents_.erase(uri);
    documents_[uri] = ParseDocument(uri, text);
}
