/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "service.h"
#include "message_buffer.h"
#include <fstream>
#include "document_service.h"

class Application : public Service
{
public:
    static Application* Instance;
    Application(const std::string& logFile);
    ~Application() override = default;
    int Run();
    bool HandleRequest(const lsp::RequestMessage& request) override;
    template <typename T>
    std::ofstream& operator << (const T& data)
    {
        if (logStream_.is_open())
            logStream_ << data;
        return logStream_;
    }

private:
    void HandleMessage(const MessageBuffer& buffer);
    bool HandleInitialize(const lsp::RequestMessage& request);
    bool HandleShutdown(const lsp::RequestMessage& request);
    bool HandleExit(const lsp::RequestMessage& request);

    bool initialized_{ false };
    bool shutdown_{ false };
    std::unique_ptr<DocumentService> documentsService_;
    std::ofstream logStream_;
};
