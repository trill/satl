project (unicode CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(unicode ${SOURCES})
target_include_directories(unicode PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
set_property(TARGET unicode PROPERTY POSITION_INDEPENDENT_CODE True)
if (BUILD_TESTING)
    add_subdirectory(tests)
endif()
