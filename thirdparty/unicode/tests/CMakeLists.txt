project (unicode_tests CXX)

file(GLOB TEST_SOURCES *.cpp)

add_executable(unicode_tests ${TEST_SOURCES})

set_property(TARGET unicode_tests PROPERTY POSITION_INDEPENDENT_CODE True)
target_link_libraries(unicode_tests unicode)
add_test(UnicodeTestRuns unicode_tests)
