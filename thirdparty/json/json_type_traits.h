/**
 * Copyright (c) 2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <type_traits>
#include <concepts>
#include "json_adapter.h"

namespace sa::json::details {

template<typename T>
concept Enum = std::is_enum_v<T>;

template<typename T>
concept CArray = std::is_array_v<T>;

template <typename T>
struct IsVector
{
    static const bool value = false;
};
template <typename T, typename Alloc>
struct IsVector<std::vector<T, Alloc>>
{
    static const bool value = true;
};
template<typename T>
concept StdVector = IsVector<T>::value;

template <typename T>
struct IsArray
{
    static const bool value = false;
};
template <class T, size_t Size>
struct IsArray<std::array<T, Size>>
{
    static const bool value = true;
};
template<typename T>
concept StdArray = IsArray<T>::value;

template <typename T>
struct IsMap
{
    static const bool value = false;
};
template <typename Key, typename T, typename Compare, typename Alloc>
struct IsMap<std::map<Key, T, Compare, Alloc>>
{
    static const bool value = true;
};
template <typename Key, typename T, typename Compare, typename Alloc>
struct IsMap<std::unordered_map<Key, T, Compare, Alloc>>
{
    static const bool value = true;
};
template<typename T>
concept StdMap = IsMap<T>::value;

template<typename T>
concept ArrayLike = std::is_array_v<T> || IsArray<T>::value || IsVector<T>::value;
template<typename T>
concept ObjectLike = std::is_class_v<T> || IsMap<T>::value;

template<typename T>
concept HasEmpty = requires(T& t)
{
    { std::empty<T>(t) } -> std::same_as<bool>;
};
template<typename T>
concept THasEmpty = requires(const T& t)
{
    { t.Empty() } -> std::same_as<bool>;
};
template<typename T>
concept AdapterHasEmpty = requires(Adapter<T> a, const T& t)
{
    { a.Empty(t) } -> std::same_as<bool>;
};
template<typename T>
concept TypeAdapterHasEmpty = requires(TypeAdapter<T> a, const T& t)
{
    { a.Empty(t) } -> std::same_as<bool>;
};

// T has a suitable Serialize() function
template<typename T, typename S>
concept Serializable = requires(T& t, S& s)
{
    { t.Serialize(s) } -> std::same_as<void>;
};

// An Adapter exists for T
template<typename T, typename S>
concept Adaptable = requires(Adapter<T> a, T& t, S& s)
{
    { a.Serialize(t, s) } -> std::same_as<void>;
};

template<typename T, typename J>
concept TypeSettable = requires(TypeAdapter<T> v, const J& j, T& t)
{
    { v(j, t) } -> std::same_as<void>;
};
template<typename T, typename J>
concept TypeGettable = requires(TypeAdapter<T> v, const T& t)
{
    { v(t) } -> std::same_as<J>;
};

// Can directly call Reader|Writer.value() with these
template<typename T>
concept Valueable = ArrayLike<T> || StdMap<T>;

}
