# sa::json

Single header JSON parser. Not the fastest, but maybe one of the simpler ones.

## License

AGPL 3.0

## Requirements

* C++ 23
* [unicode](../unicode) library

## Parsing

~~~cpp
static constexpr const char* Src = R"json(
{
  "id" : 1,
  "jsonrpc" : "2.0",
  "method" : "initialize",
  "params" : {
    "capabilities" : {
      "textDocument" : {
        "codeAction" : {
          "codeActionLiteralSupport" : {
            "codeActionKind" : {
              "valueSet" : []
            }
          }
        },
        "documentSymbol" : {
          "hierarchicalDocumentSymbolSupport" : true
        },
        "publishDiagnostics" : {
          "relatedInformation" : true
        },
        "semanticTokens" : {
          "formats" : ["relative"],
          "requests" : {
            "full" : {
              "delta" : true
            },
            "range" : true
          },
          "tokenModifiers" : [],
          "tokenTypes" : ["namespace", "type", "class", "enum", "interface", "struct", "typeParameter", "parameter", "variable", "property", "enumMember", "event", "function", "method", "macro", "keyword", "modifier", "comment", "string", "number", "regexp", "operator"]
        }
      },
      "window" : {
        "workDoneProgress" : true
      },
      "workspace" : {
        "workspaceFolders" : true
      }
    },
    "processId" : 266263,
    "rootPath" : "/home/sa",
    "rootUri" : "file:///home/sa",
    "workspaceFolders" : []
  }
}
)json";

sa::json::JSON result = sa::json::Parse(Src, [](size_t offset, const std::string& error) {
    std::cerr << offset << ": " << error << std::endl;
});
std::cout << result << std::endl;
~~~

## Constructing

### Using `initializer_list`

~~~cpp
sa::json::JSON docSyncSave = { { "includeText", false } };
std::cout << docSyncSave << std::endl;
sa::json::JSON textDocumentSync{ { "openClose", true },
    { "change", 1 },
    { "willSave", false },
    { "willSaveWaitUntil", false },
    { "save", {{ "includeText", false }} } };
std::cout << textDocumentSync << std::endl;

sa::json::JSON range{
    { "start", {
        { "line", 1},
        { "col", 2 }
    }},
    { "end", {
         { "line", 1},
         { "col", 2}
     }}
};
std::cout << range << std::endl;

sa::json::JSON arr { 1, 2, true, 1.0};
std::cout << arr << std::endl;
~~~

### As a literal

~~~cpp
using namespace sa::json::literals;
sa::json::JSON ex2 = R"(
{
    "pi": 3.141,
    "happy": true
}
)"_json;
std::cout << ex2 << std::endl;
~~~

### "By hand"

~~~cpp
sa::json::JSON textDocumentSync = sa::json::Object();
textDocumentSync["openClose"] = true;
textDocumentSync["change"] = 1;
textDocumentSync["willSave"] = false;
textDocumentSync["willSaveWaitUntil"] = false;
sa::json::JSON docSyncSave = sa::json::Object();
docSyncSave["includeText"] = false;
textDocumentSync["save"] = docSyncSave;
std::cout << textDocumentSync << std::endl;
~~~


## Iterate

For objects/arrays we iterate over all elements. For single values we iterate once. For null we don't iterate, i.e. `begin() == end()`.

~~~cpp
for (auto it = result.begin(); it != result.end(); ++it)
{
    // ...
}
~~~

~~~cpp
for (const auto& j : result)
{
    // ...
}
~~~

## Serialize/Deserialize

For serializing/deserializing arbitrary structs/classes/arrays see [here](tests/json_serialize_tests.cpp).

Essentially:

~~~cpp
#include <json_serialize.h>
#include <json_deserialize.h>

struct Something
{
    bool a = true;
    int b = 42;
    float c = 3.14;
    std::string d = "Hello";

    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value("a", a);
        ar.value("b", b);
        ar.value("c", c);
        ar.value("d", d);
    }
};

Something test;

// Something -> JSON
auto json = sa::json::Serialize(test);

// JSON -> Something
auto test2 = sa::json::Deserialize<Something>(json);

// test == test2 (in theory, but floats are silly)
~~~

You could also make macros like:

~~~cpp
#define _STRINGIFY(x) #x
#define STRINGIFY(x) _STRINGIFY(x)
#define VALUE(v) ar.value(STRINGIFY(v), v);
// Optional value
#define VALUE_OPT(v) ar.value(STRINGIFY(v), v, true);

// Then you end up with
template<typename _Ar>
void Serialize(_Ar& ar)
{
    VALUE(a);
    VALUE(b);
// ...
~~~

## Options

* `SA_JSON_FLOATING_POINT_TYPE` default `double`
* `SA_JSON_INTEGER_TYPE` default `long long`
* `SA_JSON_CASE_INSENSITIVE_KEYS` for case insensitive object keys
* `SA_JSON_STRICT` JSON must be an array or object
