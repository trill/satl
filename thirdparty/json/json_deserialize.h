/**
 * Copyright (c) 2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <expected>
#include <format>
#include "json.h"
#include "json_type_traits.h"

namespace sa::json {

namespace details {

class Reader
{
private:
    const JSON& source_;
    template<Serializable<Reader> T>
    static void SetValue(const JSON& json, T& value)
    {
        Reader reader(json);
        value.Serialize(reader);
    }
    template<Adaptable<Reader> T>
    static void SetValue(const JSON& json, T& value)
    {
        Reader reader(json);
        Adapter<T> adapter;
        adapter.Serialize(value, reader);
    }
    template<TypeSettable<JSON> T>
    static void SetValue(const JSON& json, T& value)
    {
        TypeAdapter<T> v;
        v(json, value);
    }
    template<Valueable T>
    static void SetValue(const JSON& json, T& value)
    {
        Reader reader(json);
        reader.value(value);
    }
    template<Enum T>
    static void SetValue(const JSON& json, T& value)
    {
        value = static_cast<T>(static_cast<std::underlying_type_t<T>>(json));
    }
    template<typename T>
    static void SetValue(const JSON& json, T& value)
    {
        if constexpr (std::is_same_v<T, JSON>)
        {
            value = json;
        }
        else
        {
            if (!json.IsCompatible<T>())
                throw std::runtime_error("Incompatible types");
            value = static_cast<T>(json);
        }
    }
public:
    Reader(const JSON& source) :
        source_(source)
    { }
    template<Serializable<Reader> T>
    void value(T& value)
    {
        value.Serialize(*this);
    }
    template<Adaptable<Reader> T>
    void value(T& value)
    {
        Adapter<T> adapter;
        adapter.Serialize(value, *this);
    }
    template<TypeSettable<JSON> T>
    void value(T& value)
    {
        SetValue(source_, value);
    }
    template<StdVector T>
    void value(T& value)
    {
        value.clear();
        size_t size = source_.Size();
        value.resize(size);
        for (size_t i = 0; i < size; ++i)
        {
            const auto& v = source_[i];
            auto& elem = value[i];
            SetValue(v, elem);
        }
    }
    template<StdArray T>
    void value(T& value)
    {
        if (value.size() != source_.Size())
            throw std::runtime_error("Array size mismatch");
        for (size_t i = 0; i < source_.Size(); ++i)
        {
            SetValue(source_[i], value[i]);
        }
    }
    template<CArray T>
    void value(T& value)
    {
        if (sizeof(value) / sizeof(*value) != source_.Size())
            throw std::runtime_error("Array size mismatch");
        for (size_t i = 0; i < source_.Size(); ++i)
        {
            SetValue(source_[i], value[i]);
        }
    }
    template<StdMap T>
    void value(T& value)
    {
        value.clear();
        for (auto it = source_.begin(); it != source_.end(); ++it)
        {
            auto& elem = value[source_.KeyAt(it)];
            SetValue(*it, elem);
        }
    }
    template<typename T>
    void value(std::string_view name, T& value, bool optional = false)
    {
        auto sname = std::string(name);
        if (!source_.HasKey(sname))
        {
            if (optional)
                return;
            throw std::runtime_error(std::format("No member with name '{}'", name));
        }

        SetValue<T>(source_[sname], value);
    }
};

}

template <typename T>
inline T Deserialize(const JSON& json)
{
    static_assert(std::is_default_constructible_v<T>);
    T value;
    details::Reader reader(json);
    if constexpr (details::Serializable<T, details::Reader>)
        value.Serialize(reader);
    else if constexpr (details::Adaptable<T, details::Reader>)
    {
        Adapter<T> adapter;
        adapter.Serialize(value, reader);
    }
    else if constexpr (details::Valueable<T> || details::TypeSettable<T, JSON>)
        reader.value(value);
    else
        static_assert(false, "Array/class/struct/adapter required");

    return value;
}

template <typename T>
inline std::expected<T, std::string> DeserializeSafe(const JSON& json) noexcept
{
    try
    {
        return Deserialize<T>(json);
    }
    catch (const std::exception& e)
    {
        return std::unexpected(e.what());
    }
    catch (...)
    {
        return std::unexpected("Unknown Exception");
    }
}

}
