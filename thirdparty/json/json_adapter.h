/**
 * Copyright (c) 2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace sa::json {

// This is for structures which can not be modified for adding a Serialize() function
template<typename T>
struct Adapter { };

// A way to convert a single value of a certain type to JSON and vice versa
template<typename T>
struct TypeAdapter { };

/*
 * Example for std::string
 * Not necessary because we can just cast it

template<>
struct TypeAdapter<std::string>
{
    bool Empty(const std::string& value) const
    {
        return value.empty();
    }
    void operator ()(const JSON& json, std::string& value)
    {
        value = static_cast<std::string>(json);
    }
    JSON operator ()(const std::string& value)
    {
        return value;
    }
};
 */

}
