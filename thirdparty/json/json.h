/**
 * Copyright (c) 2022-2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#if __has_include (<utf8.h>)
#include <utf8.h>
#define SA_JSON_HAVS_UTF8_H
#endif
#include <cmath>
#include <format>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <optional>
#include <sstream>
#include <string_view>
#include <type_traits>
#include <variant>
#include <vector>
#if !defined(SA_JSON_FLOATING_POINT_TYPE)
#define SA_JSON_FLOATING_POINT_TYPE double
#endif
#if !defined(SA_JSON_INTEGER_TYPE)
#define SA_JSON_INTEGER_TYPE long long
#endif

namespace sa::json {

inline std::string EscapeString(std::string_view str)
{
    std::string result;
    for (auto c : str)
    {
        switch (c)
        {
        case '\"':
            result.append("\\\"");
            break;
        case '\\':
            result.append("\\\\");
            break;
        case '\b':
            result.append("\\b");
            break;
        case '\f':
            result.append("\\f");
            break;
        case '\n':
            result.append("\\n");
            break;
        case '\r':
            result.append("\\r");
            break;
        case '\t':
            result.append("\\t");
            break;
        default:
            result.push_back(c);
            break;
        }
    }
    return result;
}

namespace {

template<typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs && lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

}

class JSON;

#ifdef SA_JSON_CASE_INSENSITIVE_KEYS
namespace details {
struct CILess
{
    bool operator()(const std::string& s1, const std::string& s2) const
    {
        std::string str1(s1.length(), ' ');
        std::string str2(s2.length(), ' ');
        std::transform(s1.begin(), s1.end(), str1.begin(), ::tolower);
        std::transform(s2.begin(), s2.end(), str2.begin(), ::tolower);
        return str1 < str2;
    }
};
}

using ObjectKeyComparator = details::CILess;
#else
using ObjectKeyComparator = std::less<std::string>;
#endif
using ObjectType = std::map<std::string, JSON, ObjectKeyComparator>;
using ArrayType = std::vector<JSON>;
using Float = SA_JSON_FLOATING_POINT_TYPE;
using Integer = SA_JSON_INTEGER_TYPE;

template<typename T>
concept IntegerLike = std::is_integral_v<T> && !std::is_same_v<T, bool>;

class JSON
{
private:
    using ObjectPtr = std::unique_ptr<ObjectType>;
    using ArrayPtr = std::unique_ptr<ArrayType>;
    using DataType = std::variant<std::nullptr_t, bool, Integer, Float, std::string, ObjectPtr, ArrayPtr>;

    template<bool IsConst>
    class It
    {
    private:
        using Type = std::conditional<IsConst, const JSON, JSON>::type;
        friend class JSON;
        Type& json_;
        size_t index_;
        It(Type& json, size_t index) :
            json_(json),
            index_(index)
        { }
    public:
        bool operator != (const It& other) noexcept { return index_ != other.index_; }
        It& operator++()
        {
            switch (json_.GetType())
            {
            case JSON::Type::Array:
            case JSON::Type::Object:
                // For objects/arrays we iterate over all elements
                if (index_ < json_.Size())
                    ++index_;
                break;
            case JSON::Type::Bool:
            case JSON::Type::Float:
            case JSON::Type::Int:
            case JSON::Type::String:
                // For single values we iterate once
                if (index_ == 0)
                    ++index_;
                break;
            default:
                // For null we don't iterate
                break;
            }
            return *this;
        }
        Type& operator*()
        {
            switch (json_.GetType())
            {
            case JSON::Type::Array:
                return json_[index_];
            case JSON::Type::Object:
            {
                using ObjType = std::conditional<IsConst, const ObjectType, ObjectType>::type;
                ObjType& obj = *std::get<ObjectPtr>(json_.data_);
                auto it = obj.begin();
                std::advance(it, index_);
                return it->second;
            }
            default:
                return json_;
            }
        }
    };

    DataType data_{ nullptr };
public:
    enum class Type
    {
        Null,
        Bool,
        Int,
        Float,
        String,
        Array,
        Object
    };

    using Iterator = It<false>;
    using ConstIterator = It<true>;

    static JSON Make(Type type)
    {
        switch (type)
        {
        case Type::Null:
            return nullptr;
        case Type::Bool:
            return false;
        case Type::Int:
            return static_cast<Integer>(0);
        case Type::Float:
            return static_cast<Float>(0.0);
        case Type::String:
            return std::string("");
        case Type::Array:
            return std::make_unique<ArrayType>();
        case Type::Object:
            return std::make_unique<ObjectType>();
        }
        return {};
    }
    constexpr JSON() noexcept = default;
    constexpr JSON(JSON&& value) noexcept
    {
        // Somehow GCC doesn't like the default move ctor.
        std::swap(data_, value.data_);
    }
    JSON(const JSON& other)
    {
        switch (other.GetType())
        {
        case Type::Null:
            break;
        case Type::Bool:
            data_ = std::get<bool>(other.data_);
            break;
        case Type::Int:
            data_ = std::get<Integer>(other.data_);
            break;
        case Type::Float:
            data_ = std::get<Float>(other.data_);
            break;
        case Type::String:
            data_ = std::get<std::string>(other.data_);
            break;
        case Type::Array:
        {
            auto obj = std::make_unique<ArrayType>();
            const auto& otherArr = *std::get<ArrayPtr>(other.data_);
            obj->reserve(otherArr.size());
            for (const auto& ti : otherArr)
                obj->push_back(ti);
            data_ = std::move(obj);
            break;
        }
        case Type::Object:
        {
            auto obj = std::make_unique<ObjectType>();
            const auto& otherObj = *std::get<ObjectPtr>(other.data_);
            for (const auto& ti : otherObj)
                obj->emplace(ti.first, ti.second);
            data_ = std::move(obj);
            break;
        }
        }
    }

    template<typename T>
    constexpr JSON(T) // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, std::nullptr_t>
    { }
    template<typename T>
    constexpr JSON(T value) // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, bool> :
        data_(value)
    { }
    template<IntegerLike T>
    constexpr JSON(T value) // NOLINT(hicpp-explicit-conversions)
        : data_(static_cast<Integer>(value))
    { }
    template<typename T>
    constexpr JSON(T value) // NOLINT(hicpp-explicit-conversions)
        requires std::is_floating_point_v<T> :
        data_(static_cast<Float>(value))
    { }
    template<typename T>
    constexpr JSON(T value) // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, std::string> :
        data_(std::move(value))
    { }
    template<typename T>
    constexpr JSON(T&& value) // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, ArrayPtr> :
        data_(std::move(value))
    { }
    template<typename T>
    constexpr JSON(T&& value) // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, ObjectPtr> :
        data_(std::move(value))
    { }

    JSON(std::initializer_list<std::tuple<std::string, JSON>> init)
    {
        data_ = std::make_unique<ObjectType>();
        std::for_each(init.begin(), init.end(),
            [this](const auto& current)
            { std::get<ObjectPtr>(data_)->emplace(std::move(std::get<0>(current)), std::move(std::get<1>(current))); });
    }
    JSON(std::initializer_list<JSON> init)
    {
        data_ = std::make_unique<ArrayType>();
        std::for_each(init.begin(), init.end(),
            [this](const auto& current)
            { std::get<ArrayPtr>(data_)->push_back(std::move(current)); });
    }

    ~JSON() = default;

    JSON& operator=(JSON&&) = default;
    JSON& operator=(const JSON& other)
    {
        *this = JSON(other);
        return *this;
    }

    template<IntegerLike T>
    JSON& operator[](T index)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        if (std::get<ArrayPtr>(data_)->size() <= static_cast<size_t>(index))
            std::get<ArrayPtr>(data_)->resize(static_cast<size_t>(index) + 1);
        return std::get<ArrayPtr>(data_)->operator[](static_cast<size_t>(index));
    }
    JSON& operator[](const std::string& key)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);

    }
    JSON& operator[](const char* key)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }
    template<IntegerLike T>
    const JSON& operator[](T index) const
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        return std::get<ArrayPtr>(data_)->operator[](static_cast<size_t>(index));
    }
    const JSON& operator[](const std::string& key) const
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }
    const JSON& operator[](const char* key) const
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return std::get<ObjectPtr>(data_)->operator[](key);
    }

    template<typename T>
    JSON& operator=(T value) requires std::is_same_v<T, bool>
    {
        data_ = value;
        return *this;
    }
    template<IntegerLike T>
    JSON& operator=(T value)
    {
        data_ = static_cast<Integer>(value);
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_floating_point_v<T>
    {
        data_ = (Float)value;
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_convertible_v<T, std::string>
    {
        data_ = std::string(value);
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_same_v<T, ArrayPtr>
    {
        data_ = std::move(value);
        return *this;
    }
    template<typename T>
    JSON& operator=(T value) requires std::is_same_v<T, ObjectPtr>
    {
        data_ = std::move(value);
        return *this;
    }

    auto operator<=>(const JSON& rhs) const
    {
        if (auto cmp = GetType() <=> rhs.GetType(); cmp != 0)
            return cmp;

        switch (GetType())
        {
        case Type::Null:
            return (rhs.IsNull() ? std::strong_ordering::equivalent : std::strong_ordering::less);
        case Type::Bool:
            return static_cast<bool>(*this) <=> static_cast<bool>(rhs);
        case Type::Int:
            return static_cast<Integer>(*this) <=> static_cast<Integer>(rhs);
        case Type::Float:
            // Floats are silly!
            if (Equals(static_cast<Float>(*this), static_cast<Float>(rhs)))
                return std::strong_ordering::equivalent;
            if (IsNaN() && rhs.IsNaN())
                return std::strong_ordering::equivalent;
            if (!IsNaN() && rhs.IsNaN())
                return std::strong_ordering::greater;
            if (IsNaN() && !rhs.IsNaN())
                return std::strong_ordering::less;
            if (static_cast<Float>(*this) < static_cast<Float>(rhs))
                return std::strong_ordering::less;
            if (static_cast<Float>(*this) > static_cast<Float>(rhs))
                return std::strong_ordering::greater;
            return std::strong_ordering::equivalent;
        case Type::String:
            return static_cast<std::string>(*this) <=> static_cast<std::string>(rhs);
        case Type::Array:
        {
            if (auto cmp = Size() <=> rhs.Size(); cmp != 0)
                return cmp;

            for (size_t i = 0; i < Size(); ++i)
            {
                if (auto cmp = (*this)[i] <=> rhs[i]; cmp != 0)
                    return cmp;
            }
            return std::strong_ordering::equivalent;
        }
        case Type::Object:
        {
            if (auto cmp = Size() <=> rhs.Size(); cmp != 0)
                return cmp;

            const auto& obj = *std::get<ObjectPtr>(data_);
            for (const auto& o : obj)
            {
                if (auto cmp = rhs[o.first] <=> o.second; cmp != 0)
                    return cmp;
            }
            return std::strong_ordering::equivalent;
        }
        }
        // Shouldn't get here
        return std::strong_ordering::equivalent;
    }

    bool operator<(const JSON& rhs) const
    {
        return (*this <=> rhs) != std::strong_ordering::less;
    }
    bool operator>(const JSON& rhs) const
    {
        return (*this <=> rhs) != std::strong_ordering::greater;
    }
    bool operator!=(const JSON& rhs) const
    {
        return (*this <=> rhs) != std::strong_ordering::equivalent;
    }
    bool operator==(const JSON& rhs) const
    {
        return (*this <=> rhs) == std::strong_ordering::equivalent;
    }

    template<typename T>
    [[nodiscard]] constexpr bool IsCompatible() const
    {
        Type type = GetType();
        if (type == Type::Null)
            return std::is_same_v<T, std::nullptr_t>;
        if (type == Type::Bool)
            return std::is_same_v<T, bool>;
        if (type == Type::Int)
            return std::is_convertible_v<T, Integer>;
        if (type == Type::Float)
            return std::is_convertible_v<T, Float>;
        if (type == Type::String)
            return std::is_convertible_v<T, std::string>;
        if (type == Type::Array)
            return std::is_same_v<T, ArrayPtr>;
        if (type == Type::Object)
            return std::is_same_v<T, ObjectPtr>;
        return false;
    }
    [[nodiscard]] constexpr Type GetType() const
    {
        if (std::holds_alternative<bool>(data_))
            return Type::Bool;
        if (std::holds_alternative<Integer>(data_))
            return Type::Int;
        if (std::holds_alternative<Float>(data_))
            return Type::Float;
        if (std::holds_alternative<std::string>(data_))
            return Type::String;
        if (std::holds_alternative<ArrayPtr>(data_))
            return Type::Array;
        if (std::holds_alternative<ObjectPtr>(data_))
            return Type::Object;
        return Type::Null;
    }

    [[nodiscard]] constexpr bool IsNull() const
    {
        return GetType() == Type::Null;
    }
    [[nodiscard]] constexpr bool IsNaN() const
    {
        if (GetType() == Type::Float)
            return std::isnan(std::get<Float>(data_));
        return false;
    }
    [[nodiscard]] constexpr bool IsInf() const
    {
        if (GetType() == Type::Float)
            return Equals(std::get<Float>(data_), std::numeric_limits<Float>::infinity());
        return false;
    }
    [[nodiscard]] constexpr bool IsNegInf() const
    {
        if (GetType() == Type::Float)
        {
            if constexpr (std::numeric_limits<Float>::is_iec559)
                return Equals(std::get<Float>(data_), -std::numeric_limits<Float>::infinity());
            else
                return false;
        }
        return false;
    }

    template<typename T>
    operator T() const // NOLINT(hicpp-explicit-conversions)
        requires std::is_same_v<T, bool>
    {
        if (GetType() != Type::Bool)
            throw std::runtime_error("Not a bool");
        return std::get<bool>(data_);
    }
    template<IntegerLike T>
    operator T() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Int)
            throw std::runtime_error("Not an int");
        return static_cast<T>(std::get<Integer>(data_));
    }
    template<typename T>
    operator T() const // NOLINT(hicpp-explicit-conversions)
        requires std::is_floating_point_v<T>
    {
        if (GetType() != Type::Float)
            throw std::runtime_error("Not a float");
        return static_cast<T>(std::get<Float>(data_));
    }
    operator const std::string&() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::String)
            throw std::runtime_error("Not a string");
        return std::get<std::string>(data_);
    }
    operator const ArrayType&() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        return *std::get<ArrayPtr>(data_);
    }
    operator const ObjectType&() const // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return *std::get<ObjectPtr>(data_);
    }
    operator ArrayType&() // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        return *std::get<ArrayPtr>(data_);
    }
    operator ObjectType&() // NOLINT(hicpp-explicit-conversions)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        return *std::get<ObjectPtr>(data_);
    }

    [[nodiscard]] size_t Size() const
    {
        switch (GetType())
        {
        case Type::Array:
            return std::get<ArrayPtr>(data_)->size();
        case Type::Object:
            return std::get<ObjectPtr>(data_)->size();
        default:
            return 0;
        }
    }

    [[nodiscard]] bool Empty() const
    {
        switch (GetType())
        {
        case Type::Null:
            return true;
        case Type::Array:
            return std::get<ArrayPtr>(data_)->empty();
        case Type::Object:
            return std::get<ObjectPtr>(data_)->empty();
        default:
            return true;
        }
    }

    [[nodiscard]] bool HasKey(const std::string& key) const
    {
        if (GetType() == Type::Object)
            return std::get<ObjectPtr>(data_)->find(key) != std::get<ObjectPtr>(data_)->end();
        return false;
    }

    template<typename T>
    void Append(T value)
    {
        if (GetType() != Type::Array)
            throw std::runtime_error("Not an array");
        std::get<ArrayPtr>(data_)->push_back(value);
    }

    template<typename T>
    void Emplace(std::string key, T value)
    {
        if (GetType() != Type::Object)
            throw std::runtime_error("Not an object");
        std::get<ObjectPtr>(data_)->emplace(std::move(key), std::move(value));
    }

    [[nodiscard]] std::string Dump(bool minify = false) const
    {
        return InternalDump(minify, 1, 0);
    }

    // Since the iterators drop the keys of objects.
    template<typename T>
        requires std::is_same_v<T, ConstIterator> || std::is_same_v<T, Iterator>
    [[nodiscard]] std::string KeyAt(const T& it) const
    {
        if (&it.json_ != this)
            throw std::runtime_error("Invalid iterator");
        return KeyAt(it.index_);
    }
    template<IntegerLike T>
    [[nodiscard]] std::string KeyAt(T it) const
    {
        if (GetType() == Type::Object)
        {
            const auto& obj = *std::get<ObjectPtr>(data_);
            auto i = obj.begin();
            std::advance(i, it);
            return i->first;
        }
        throw std::runtime_error("Not an object");
    }

    ConstIterator begin() const { return { *this, 0 }; }
    ConstIterator end() const
    {
        switch (GetType())
        {
        case Type::Bool:
        case Type::Int:
        case Type::Float:
        case Type::String:
            return { *this, 1 };
        case Type::Array:
        case Type::Object:
            return { *this, Size() };
        default:
            return { *this, 0 };
        }
    }
    Iterator begin() { return { *this, 0 }; }
    Iterator end()
    {
        switch (GetType())
        {
        case Type::Bool:
        case Type::Int:
        case Type::Float:
        case Type::String:
            return { *this, 1 };
        case Type::Array:
        case Type::Object:
            return { *this, Size() };
        default:
            return { *this, 0 };
        }
    }

    friend std::ostream& operator<<(std::ostream&, const JSON&);
private:
    [[nodiscard]] std::string InternalDump(bool minify = false, int depth = 1, int indent = 0) const
    {
        switch (GetType())
        {
        case Type::Null:
            return "null";
        case Type::Bool:
            return std::get<bool>(data_) ? "true" : "false";
        case JSON::Type::Int:
            return std::to_string(std::get<Integer>(data_));
        case JSON::Type::Float:
        {
            if (IsInf())
                return "\"Infinity\"";
            if (IsNegInf())
                return "\"-Infinity\"";
            if (IsNaN())
                return "\"NaN\"";

            std::stringstream ss;
            ss.imbue(std::locale::classic());
            ss << std::get<Float>(data_);
            std::string result = ss.str();
            if (result.find('.') == std::string::npos)
                return result + ".0";
            return result;
        }
        case Type::String:
            return std::format("\"{}\"", EscapeString(std::get<std::string>(data_)));
        case Type::Array:
        {
            std::stringstream ss;
            ss << "[";
            bool skip = true;
            for (const auto& i : *std::get<ArrayPtr>(data_))
            {
                if (!skip)
                {
                    ss << ",";
                    if (!minify)
                        ss << " ";
                }
                ss << i.InternalDump(minify, depth + 1, indent + 2);
                skip = false;
            }
            ss << "]";
            return ss.str();
        }
        case Type::Object:
        {
            std::stringstream ss;
            ss << "{";
            if (!minify)
                ss << "\n";
            bool skip = true;
            for (const auto& i : *std::get<ObjectPtr>(data_))
            {
                if (!skip)
                {
                    ss << ",";
                    if (!minify)
                        ss << "\n";
                }
                if (!minify)
                    ss << std::string(indent + 2, ' ');
                ss << "\"" << i.first << "\"";
                if (!minify)
                    ss << " ";
                ss << ":";
                if (!minify)
                    ss << " ";
                ss << i.second.InternalDump(minify, depth + 1, indent + 2);
                skip = false;
            }
            if (!minify)
                ss << "\n" << std::string(indent, ' ');
            ss << "}";
            return ss.str();
        }
        }

        return {};
    }

};

using ErrorFunc = std::function<void(size_t, const std::string&)>;

namespace {

template<typename T>
inline std::optional<T> ToNumber(std::string_view number)
{
    T result;
    auto [ptr, error] = std::from_chars(number.data(), number.data() + number.size(), result);
    if (error != std::errc{})
    {
        return {};
    }
    return result;
}

inline void SyntaxError(const ErrorFunc& error, size_t offset, const std::string& message)
{
    if (error)
        error(offset, message);
}

inline std::optional<JSON> ParseNext(std::string_view, size_t&, const ErrorFunc&);

inline bool IsSpace(char c)
{
    // https://www.rfc-editor.org/rfc/rfc4627
    /*
   ws = *(
             %x20 /              ; Space
             %x09 /              ; Horizontal tab
             %x0A /              ; Line feed or New line
             %x0D                ; Carriage return
         )
     */
    return c == 0x20 || c == 0x09 || c == 0x0a || c == 0x0d;
}

inline void SkipWhite(std::string_view str, size_t& offset)
{
    while (IsSpace(str[offset]))
        ++offset;
}

inline std::optional<JSON> ParseObject(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    JSON result = JSON::Make(JSON::Type::Object);

    SkipWhite(str, ++offset);

    if (str[offset] == '}')
    {
        ++offset;
        return result;
    }

    while (true)
    {
        auto key = ParseNext(str, offset, error);
        if (!key.has_value())
            return {};
        if (key.value().GetType() != JSON::Type::String)
        {
            SyntaxError(error, offset, "Non string object key");
            return {};
        }

        SkipWhite(str, offset);
        if (str[offset] != ':')
        {
            SyntaxError(error, offset, ": expected");
            return {};
        }

        SkipWhite(str, ++offset);
        auto value = ParseNext(str, offset, error);
        if (!value.has_value())
            return {};
        result[(std::string)key.value()] = std::move(value.value());

        SkipWhite(str, offset);
        if (str[offset] == ',')
        {
            ++offset;
            continue;
        }
        if (str[offset] == '}')
        {
            ++offset;
            break;
        }
        SyntaxError(error, offset, ", or } expected");
        return {};
    }

    return result;
}

inline std::optional<JSON> ParseArray(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    JSON result = JSON::Make(JSON::Type::Array);
    size_t index = 0;
    SkipWhite(str, ++offset);
    if (str[offset] == ']')
    {
        ++offset;
        return result;
    }

    while (true)
    {
        auto value = ParseNext(str, offset, error);
        if (!value.has_value())
            return {};
        result[index++] = std::move(value.value());
        SkipWhite(str, offset);
        if (str[offset] == ',')
        {
            ++offset;
            continue;
        }
        if (str[offset] == ']')
        {
            ++offset;
            break;
        }
        SyntaxError(error, offset, ", or ] expected");
        return {};
    }
    return result;
}

inline std::optional<JSON> ParseString(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    std::stringstream result;

#ifdef SA_JSON_HAVS_UTF8_H
    auto isHexDigit = [](char c) -> bool
    {
        return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
    };
    auto peek = [&](size_t off) -> char
    {
        if (offset + off >= str.length())
            return '\0';
        return str[offset + off];
    };
#endif

    bool escaped = false;
    for (char c = str[++offset]; (c != '\"' || escaped); c = str[offset])
    {
        if (escaped)
        {
            switch (c)
            {
            case '\"':
                result << '\"';
                break;
            case '\\':
                result << '\\';
                break;
            case '/':
                // Interesting https://stackoverflow.com/questions/1580647/json-why-are-forward-slashes-escaped
                result << '/';
                break;
            case 'b':
                result << '\b';
                break;
            case 'f':
                result << '\f';
                break;
            case 'n':
                result << '\n';
                break;
            case 'r':
                result << '\r';
                break;
            case 't':
                result << '\t';
                break;
#ifdef SA_JSON_HAVS_UTF8_H
            case 'u':
            {
                if (isHexDigit(peek(1)) && isHexDigit(peek(2)) && isHexDigit(peek(3)) && isHexDigit(peek(4)))
                {
                    char hex[5] = {};
                    for (int i = 0; i < 4; ++i)
                    {
                        hex[i] = str[++offset];
                    }
                    uint32_t value = std::stoi(hex, nullptr, 16);
                    result << utf8::Encode({ value });
                    break;
                }
                SyntaxError(error, offset, "Malformed escape sequence, expecting two bytes");
                return {};
            }
#endif
            default:
                SyntaxError(error, offset, std::format("Invalid escape sequence {}", c));
                return {};
            }
            escaped = false;
        }
        else if (c == '\\')
            escaped = true;
        else if (c == '\n')
        {
            // No multiline strings
            SyntaxError(error, offset, "Unescaped new line");
            return {};
        }
        else if (c == '\b' || c == '\f' || c == '\r' || c == '\t' || c == '\0')
        {
            // Literal tab character is not allowed inside a JSON string. You need to escape it as \t
            SyntaxError(error, offset, "Unexpected control character");
            return {};
        }
        else
            result << c;
        ++offset;
        if (offset >= str.length())
        {
            SyntaxError(error, offset, "Unterminated string");
            return {};
        }
    }
    ++offset;
    std::string value = result.str();
    if (value == "Infinity")
        return std::numeric_limits<Float>::infinity();
    if (value == "-Infinity")
        return -std::numeric_limits<Float>::infinity();
    if (value == "NaN")
        return std::numeric_limits<Float>::quiet_NaN();
    return value;
}

inline std::optional<JSON> ParseBool(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    if (str.substr(offset, 4) == "true")
    {
        offset += 4;
        return true;
    }
    if (str.substr(offset, 5) == "false")
    {
        offset += 5;
        return false;
    }
    SyntaxError(error, offset, "true or false expected");
    return {};
}

inline std::optional<JSON> ParseNull(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    if (str.substr(offset, 4) != "null")
    {
        SyntaxError(error, offset, "null expected");
        return {};
    }
    offset += 4;
    return nullptr;
}

inline std::optional<JSON> ParseNumber(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    // https://i.sstatic.net/1mXlt.gif
    size_t start = offset;
    char current = str[offset];
    bool hasFraction = false;

    if (current == '-')
    {
        current = str[++offset];
        if (!isdigit(current))
        {
            SyntaxError(error, offset, "Malformed number");
            return {};
        }
    }
    if (!isdigit(current))
    {
        SyntaxError(error, offset, "Malformed number");
        return {};
    }

    if (current == '0')
    {
        // Zero must be followed by a dot or exponent
        current = str[++offset];
        if (current == 'e' || current == 'E')
            goto exponent;

        if (current == '.')
        {
            hasFraction = true;
            current = str[++offset];
            if (!isdigit(current))
            {
                SyntaxError(error, offset, "Malformed number");
                return {};
            }
            goto digit;
        }

        // (Negative) zero is valid
        if (!isdigit(current))
        {
            goto done;
        }
        // A number with leading zero is not
        SyntaxError(error, offset, "Malformed number");
        return {};
    }

digit:
    while (isdigit(current))
    {
        current = str[++offset];
    }
    if (current == '.')
    {
        if (hasFraction)
        {
            SyntaxError(error, offset, "Malformed number");
            return {};
        }
        hasFraction = true;
        current = str[++offset];
        if (!isdigit(current))
        {
            SyntaxError(error, offset, "Malformed number");
            return {};
        }
        while (isdigit(current))
        {
            current = str[++offset];
        }
    }

exponent:
    if ((current == 'e' || current == 'E'))
    {
        current = str[++offset];

        if (current == '+' || current == '-')
        {
            if (current == '-')
                hasFraction = true;
            current = str[++offset];
        }

        if (!isdigit(current))
        {
            SyntaxError(error, offset, "Expected exponent");
            return {};
        }

        while (isdigit(current))
        {
            current = str[++offset];
        }
    }

done:
    auto value = std::string_view(&str[start], offset - start);
    auto v = ToNumber<Float>(value);
    if (!v.has_value())
    {
        SyntaxError(error, offset, std::format("Malformed number \"{}\"", value));
        return {};
    }
    if (!hasFraction)
        // When it doesn't have a fractional part make it an integer
        return static_cast<Integer>(v.value());
    return static_cast<Float>(v.value());
}

inline std::optional<JSON> ParseNext(std::string_view str, size_t& offset, const ErrorFunc& error)
{
    SkipWhite(str, offset);
    char value = str[offset];
    switch (value)
    {
    case '[':
        return ParseArray(str, offset, error);
    case '{':
        return ParseObject(str, offset, error);
    case '\"':
        return ParseString(str, offset, error);
    case 't':
    case 'f':
        return ParseBool(str, offset, error);
    case 'n':
        return ParseNull(str, offset, error);
    default:
        if ((value >= '0' && value <= '9') || value == '-')
            return ParseNumber(str, offset, error);
    }
    SyntaxError(error, offset, std::format("Unexpected character '{}'", value));
    return {};
}

}

inline JSON Parse(std::string_view str, const ErrorFunc& error = {})
{
    if (str.empty())
    {
        SyntaxError(error, 0, "Empty value");
        return nullptr;
    }
    size_t offset = 0;
    auto result = ParseNext(str, offset, error);
    SkipWhite(str, offset);
    if (offset < str.length())
    {
        SyntaxError(error, 0, std::format("Extra '{}'", str[offset]));
        return nullptr;
    }
#ifdef SA_JSON_STRICT
    if (result.has_value())
    {
        if (result->GetType() != JSON::Type::Array && result->GetType() != JSON::Type::Object)
        {
            SyntaxError(error, 0, "Not an object or array");
            return nullptr;
        }
    }
#endif
    return result.value_or(JSON{});
}

inline std::ostream& operator<<(std::ostream& os, const JSON& json)
{
    os << json.Dump();
    return os;
}

inline JSON Array()
{
    return JSON::Make(JSON::Type::Array);
}

inline JSON Object()
{
    return JSON::Make(JSON::Type::Object);
}

namespace literals
{
inline JSON operator "" _json(const char* s, size_t n)
{
    return Parse(std::string_view(s, n));
}
}

}
