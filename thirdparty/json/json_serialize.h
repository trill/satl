/**
 * Copyright (c) 2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <utility>
#include "json.h"
#include "json_type_traits.h"

namespace sa::json {

template <typename T>
inline JSON Serialize(const T& value);

namespace details {

template<typename T>
inline JSON GetJSONForType()
{
    if constexpr (ArrayLike<T>)
        return Array();
    else if constexpr (ObjectLike<T>)
        return Object();
    else
        static_assert(false, "Array or class/struct required");
}

class Writer
{
private:
    JSON& target_;
    template<Serializable<Writer> T>
    static JSON GetValue(T& value)
    {
        JSON result = GetJSONForType<T>();
        Writer writer(result);
        value.Serialize(writer);
        return result;
    }
    template<Adaptable<Writer> T>
    static JSON GetValue(T& value)
    {
        JSON result = GetJSONForType<T>();
        Writer writer(result);
        Adapter<T> adapter;
        adapter.Serialize(value, writer);
        return result;
    }
    template<TypeGettable<JSON> T>
    static JSON GetValue(T& value)
    {
        TypeAdapter<T> v;
        return v(value);
    }
    template<ArrayLike T>
    static JSON GetValue(T& value)
    {
        JSON result = Array();
        for (auto& v : value)
            result.Append(GetValue(v));
        return result;
    }
    template<StdMap T>
    static JSON GetValue(T& value)
    {
        JSON result = GetJSONForType<T>();
        for (auto& v : value)
            result[v.first] = GetValue(v.second);
        return result;
    }
    template<Enum T>
    static JSON GetValue(T& value)
    {
        return static_cast<Integer>(std::to_underlying(value));
    }
    template<typename T>
    static JSON GetValue(T& value)
    {
        return value;
    }

    template<typename T>
    static bool IsEmpty(const T& value)
    {
        if constexpr (std::is_same_v<T, std::nullptr_t>)
            // Don't write null
            return true;
        else if constexpr (details::THasEmpty<T>)
            return value.Empty();
        else if constexpr (details::AdapterHasEmpty<T>)
        {
            // There is an Adapter with an Empty() function
            Adapter<T> adapter;
            return adapter.Empty(value);
        }
        else if constexpr (details::TypeAdapterHasEmpty<T>)
        {
            // There is an TypeAdapter with an Empty() function
            TypeAdapter<T> adapter;
            return adapter.Empty(value);
        }
        else if constexpr (details::HasEmpty<T>)
            // also skip when empty
            return std::empty(value);
        else if constexpr (std::is_same_v<T, JSON>)
            return value.Empty();
        else
            return false;
    }
public:
    Writer(JSON& target) :
        target_(target)
    { }
    template<TypeGettable<JSON> T>
    void value(T& value)
    {
        target_ = GetValue(value);
    }
    template<ArrayLike T>
    void value(T& value)
    {
        for (auto& v : value)
            target_.Append(GetValue(v));
    }
    template<StdMap T>
    void value(T& value)
    {
        for (auto& v : value)
            target_.Emplace(v.first, GetValue(v.second));
    }
    template<typename T>
    void value(std::string_view name, T& value, bool optional = false)
    {
        if (optional && IsEmpty(value))
            return;
        target_.Emplace(std::string(name), GetValue<T>(value));
    }
};

}

template <typename T>
inline JSON Serialize(const T& value)
{
    JSON json = details::GetJSONForType<T>();
    details::Writer writer(json);
    if constexpr (details::Serializable<T, details::Writer>)
        const_cast<T&>(value).Serialize(writer);
    else if constexpr (details::Adaptable<T, details::Writer>)
    {
        Adapter<T> adapter;
        adapter.Serialize(const_cast<T&>(value), writer);
    }
    else if constexpr (details::Valueable<T> || details::TypeGettable<T, JSON>)
        writer.value(const_cast<T&>(value));
    else
        static_assert(false, "Array/class/struct/adapter required");

    return json;
}

}
