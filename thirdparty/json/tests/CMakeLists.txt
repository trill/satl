project (json_tests CXX)

file(GLOB TEST_SOURCES *.cpp)

add_executable(json_tests ${TEST_SOURCES})

set_property(TARGET json_tests PROPERTY POSITION_INDEPENDENT_CODE True)
target_link_libraries(json_tests json)
add_test(JSONTestRuns json_tests)
