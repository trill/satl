/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <json.h>

static bool TestParseSuccess(std::string_view src)
{
    bool result = true;
    [[maybe_unused]] sa::json::JSON json = sa::json::Parse(src, [&](size_t, const std::string&) {
        result = false;
    });
    return result;
}

TEST_CASE("JSON construct initialize_list", "[json]")
{
    sa::json::JSON docSyncSave = { { "includeText", false } };
    REQUIRE(docSyncSave.Size() == 1);
    REQUIRE(docSyncSave.HasKey("includeText"));
    REQUIRE((bool)docSyncSave["includeText"] == false);
}

TEST_CASE("JSON construct", "[json]")
{
    sa::json::JSON docSyncSave = sa::json::Object();
    docSyncSave["includeText"] = false;
    REQUIRE(docSyncSave.Size() == 1);
    REQUIRE(docSyncSave.HasKey("includeText"));
    REQUIRE((bool)docSyncSave["includeText"] == false);
}

TEST_CASE("JSON parse", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "id" : 1,
  "params" : {
    "processId" : 266263,
    "workspaceFolders" : []
  }
}
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(result.Size() == 2);
}

TEST_CASE("JSON parse Unicode", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "key16" : "\u03B2"
}
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(result.Size() == 1);
    REQUIRE(result.HasKey("key16"));
    REQUIRE((std::string)result["key16"] == "\u03B2");
}

TEST_CASE("JSON parse number literals", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "keyint"   : 12345,
  "keynegint": -12345,
  "keyfloat" : 3.1415,
  "keysci"   : 3e2,
  "keysci1"  : -3e2,
  "keysci2"  : 3e-2,
  "keysci3"  : -3e-2
}
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string& msg) {
        (void)msg;
        REQUIRE(false);
    });

    REQUIRE((int)result["keyint"] == 12345);
    REQUIRE((int)result["keynegint"] == -12345);
    REQUIRE((float)result["keyfloat"] == Approx(3.1415));
    REQUIRE((int)result["keysci"] == 3e2);
    REQUIRE((int)result["keysci1"] == -3e2);
    REQUIRE((float)result["keysci2"] == Approx(3e-2));
    REQUIRE((float)result["keysci3"] == Approx(-3e-2));
}

TEST_CASE("Unterminated string", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "keyhex  : 42
}
)json";
    bool error = false;
    sa::json::JSON result = sa::json::Parse(Src, [&error](size_t, const std::string& msg) {
        if (!error)
        {
            REQUIRE(msg == "Unescaped new line");
            error = true;
        }
    });
    REQUIRE(error);
}

TEST_CASE("Empty key", "[json]")
{
    static constexpr const char* Src = R"json(
{
  ""  : 42
}
)json";
    // Empty key is ok
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
}

TEST_CASE("Duplicate empty key", "[json]")
{
    static constexpr const char* Src = R"json(
{
  ""  : 42,
  ""  : 43
}
)json";
    // Duplicate keys are allowed, just don't make sense
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
}

TEST_CASE("Unterminated string 2", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "string"  : "value
}
)json";
    bool error = false;
    sa::json::JSON result = sa::json::Parse(Src, [&error](size_t, const std::string& msg) {
        if (!error)
        {
            REQUIRE(msg == "Unescaped new line");
            error = true;
        }
    });
    REQUIRE(error);
}

TEST_CASE("Iterator object", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "key1"  : "value1",
  "key2"  : "value2",
  "key3"  : "value3",
  "key4"  : "value4"
}
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    int c = 0;
    for (auto it = result.begin(); it != result.end(); ++it)
    {
        ++c;
        std::string exp = "value" + std::to_string(c);
        std::string expk = "key" + std::to_string(c);
        REQUIRE(((std::string)*it) == exp);
        std::string key = result.KeyAt(it);
        REQUIRE(result.KeyAt(it) == expk);
    }
    REQUIRE(c == 4);
}

TEST_CASE("Iterator array", "[json]")
{
    static constexpr const char* Src = R"json(
[
  "value1",
  "value2",
  "value3",
  "value4"
]
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    int c = 0;
    for (auto it = result.begin(); it != result.end(); ++it) // NOLINT(modernize-loop-convert)
    {
        ++c;
        std::string exp = "value" + std::to_string(c);
        REQUIRE(((std::string)*it) == exp);
    }
    REQUIRE(c == 4);
}

TEST_CASE("Iterator object foreach", "[json]")
{
    static constexpr const char* Src = R"json(
{
  "key1"  : "value1",
  "key2"  : "value2",
  "key3"  : "value3",
  "key4"  : "value4"
}
)json";
    const sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    int c = 0;
    for (const auto& j : result)
    {
        ++c;
        std::string exp = "value" + std::to_string(c);
        REQUIRE(((std::string)j) == exp);
    }
    REQUIRE(c == 4);
}

TEST_CASE("Iterator array foreach", "[json]")
{
    static constexpr const char* Src = R"json(
[
  "value1",
  "value2",
  "value3",
  "value4"
]
)json";
    sa::json::JSON result = sa::json::Parse(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    int c = 0;
    for (const auto& j : result)
    {
        ++c;
        std::string exp = "value" + std::to_string(c);
        REQUIRE(((std::string)j) == exp);
    }
    REQUIRE(c == 4);
}

TEST_CASE("Literal", "[json]")
{
    using namespace sa::json::literals;
    sa::json::JSON ex2 = R"(
  {
    "pi": 3.141,
    "happy": true
  }
)"_json;
    REQUIRE(ex2["pi"] == Approx(3.141));
    REQUIRE(ex2["happy"]);
}

TEST_CASE("Infinity, -Infinity, NaN", "[json]")
{
    // These must be strings, because there is no support for these values in JSON.
    using namespace sa::json::literals;
    sa::json::JSON t = R"(
  {
    "inf": "Infinity",
    "ninf": "-Infinity",
    "nan": "NaN"
  }
)"_json;
    // Returns null if parsing fails
    REQUIRE(!t.IsNull());
    REQUIRE(t["inf"].IsInf());
    REQUIRE(t["inf"] == Approx(std::numeric_limits<sa::json::Float>::infinity()));
    REQUIRE(t["ninf"].IsNegInf());
    REQUIRE(t["ninf"] == Approx(-std::numeric_limits<sa::json::Float>::infinity()));
    REQUIRE(t["nan"].IsNaN());
}

TEST_CASE("empty", "[json]")
{
    static constexpr const char* Src = R"json()json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("invalid", "[json]")
{
    static constexpr const char* Src = R"json(abc)json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("n_string_invalid-utf-8-in-escape.json", "[json]")
{
    static constexpr const char* Src = R"json(["\uå"])json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("n_number_+Inf.json", "[json]")
{
    static constexpr const char* Src = R"json([+Inf])json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("n_number_neg_int_starting_with_zero.json", "[json]")
{
    static constexpr const char* Src = R"json([-012])json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("n_number_0.1.2.json", "[json]")
{
    static constexpr const char* Src = R"json([0.1.2])json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("n_object_non_string_key.json", "[json]")
{
    static constexpr const char* Src = R"json({1:1})json";
    REQUIRE(!TestParseSuccess(Src));
}

TEST_CASE("y_number_0e+1.json", "[json]")
{
    static constexpr const char* Src = R"json([0e+1])json";
    REQUIRE(TestParseSuccess(Src));
}

TEST_CASE("y_string_allowed_escapes.json", "[json]")
{
    static constexpr const char* Src = R"json(["\"\\\/\b\f\n\r\t"])json";
    REQUIRE(TestParseSuccess(Src));
}

/*
TEST_CASE("y_number.json", "[json]")
{
    // Thats just too big for a float
    static constexpr const char* Src = R"json([123e65])json";
    REQUIRE(TestParseSuccess(Src));
}
*/

TEST_CASE("y_number_negative_zero.json", "[json]")
{
    static constexpr const char* Src = R"json([-0])json";
    REQUIRE(TestParseSuccess(Src));
}

TEST_CASE("y_structure_lonely_negative_real.json", "[json]")
{
    static constexpr const char* Src = R"json(-0.1)json";
    REQUIRE(TestParseSuccess(Src));
}

TEST_CASE("n_string_unescaped_ctrl_char.json", "[json]")
{
    static constexpr const char* Src = R"json(["a a"])json";
    REQUIRE(!TestParseSuccess(Src));
}

/*
TEST_CASE("y_number_real_fraction_exponent.json", "[json]")
{
    // Too large
    static constexpr const char* Src = R"json([123.456e78])json";
    REQUIRE(TestParseSuccess(Src));
}
*/

TEST_CASE("y_number_double_close_to_zero.json", "[json]")
{
    if constexpr (sizeof(sa::json::Float) == 8)
    {
        // 	std::errc::result_out_of_range
        static constexpr const char* Src = R"json([-0.000000000000000000000000000000000000000000000000000000000000000000000000000001])json";
        REQUIRE(TestParseSuccess(Src));
    }
}

TEST_CASE("number_1e-999.json", "[json]")
{
    if constexpr (sizeof(sa::json::Float) == 8)
    {
        // 	std::errc::result_out_of_range
        static constexpr const char* Src = R"json([1E-999])json";
        REQUIRE(TestParseSuccess(Src));
    }
}
