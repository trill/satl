/**
 * Copyright (c) 2025, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include <json_serialize.h>
#include <json_deserialize.h>
#include <cstring>

using namespace sa::json::literals;

namespace {

struct TestBool
{
    bool b = true;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value("b", b);
    }
};

struct Test1
{
    int a;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value("a", a);
    }
};

struct Test2 : Test1
{
    int b;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        Test1::Serialize(ar);
        ar.value("b", b);
    }
    bool operator==(const Test2& rhs) const
    {
        return a == rhs.a && b == rhs.b;
    }
};

struct Test3
{
    Test2 t;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value("t", t);
    }
};

struct Test4
{
    int t[5] = { 1, 2, 3, 4, 5 };
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value("t", t);
    }
    bool operator==(const Test4& rhs) const
    {
        int cmp = std::memcmp(t, rhs.t, sizeof(t));
        return cmp == 0;
    }
};

struct Test6
{
    int a;
    int b;
    template<typename _Ar>
    void Serialize(_Ar& ar)
    {
        ar.value("a", a, true);
        ar.value("b", b, true);
    }
};

// Compound type
struct Test7
{
    std::array<Test1, 5> test1s = {};
    Test3 test3;
    Test4 test4;
    template<typename _Ar>
    void Serialize(_Ar ar)
    {
        ar.value("test5s", test1s);
        ar.value("test3", test3);
        ar.value("test4", test4);
    }
    bool operator==(const Test7& rhs) const
    {
        for (size_t i = 0; i < test1s.size(); ++i)
        {
            if (test1s[i].a != rhs.test1s[i].a)
                return false;
        }
        return test3.t == rhs.test3.t && test4 == rhs.test4;
    }
};

// Using an Adapter instead of a member function.
// Useful for structures you can not change.
struct TestAdapter
{
    int t[5] = { 1, 2, 3, 4, 5 };
    std::string thing = "the thing is...";
    std::map<std::string, std::string> map = {
        {"key1", "value1"},
        {"key2", "value2"},
        {"key3", "value3"},
        {"key4", "value4"},
    };
    bool happy = true;
    int answer = 42;

    bool operator==(const TestAdapter& rhs) const
    {
        int cmp = std::memcmp(t, rhs.t, sizeof(t));
        return cmp == 0 &&
            thing == rhs.thing &&
            map == rhs.map &&
            happy == rhs.happy &&
            answer == rhs.answer;
    }
};

struct NestedTest
{
    // Will automagically use sa::json::Adapter<TestAdapter>
    TestAdapter test1;
    std::string teststring = "test";
    bool operator==(const NestedTest& rhs) const
    {
        return test1 == rhs.test1 && teststring == rhs.teststring;
    }
};

struct ValueGetterSetterTester
{
    int i = 42;
};

struct EnumTester
{
    enum class Test { A, B, C };
    Test test = Test::B;
    enum Test2 { Test2A, Test2B, Test2C };
    Test2 test2 = Test2C;
    template<typename _Ar>
    void Serialize(_Ar ar)
    {
        ar.value("test", test);
        ar.value("test2", test2);
    }
};

struct JsonTester
{
    std::string test = "teststring";
    sa::json::JSON jsontest;
    bool operator==(const JsonTester& rhs) const
    {
        return test == rhs.test && jsontest == rhs.jsontest;
    }
    bool Empty() const
    {
        return test.empty() && jsontest.Empty();
    }
    template<typename _Ar>
    void Serialize(_Ar ar)
    {
        ar.value("test", test);
        ar.value("jsontest", jsontest);
    }
};

}

// Creating an Adapter specialization with a Serialize() function
template<>
struct sa::json::Adapter<TestAdapter>
{
    template<typename _Ar>
    void Serialize(TestAdapter& value, _Ar& ar)
    {
        ar.value("t", value.t);
        ar.value("thing", value.thing);
        ar.value("map", value.map);
        ar.value("happy", value.happy);
        ar.value("answer", value.answer);
    }
};

template<>
struct sa::json::Adapter<NestedTest>
{
    bool Empty(const NestedTest&) const
    {
        return false;
    }
    template<typename _Ar>
    void Serialize(NestedTest& value, _Ar& ar)
    {
        ar.value("test1", value.test1);
        ar.value("teststring", value.teststring);
    }
};

template<>
struct sa::json::TypeAdapter<ValueGetterSetterTester>
{
    bool Empty(const ValueGetterSetterTester& value) const
    {
        return value.i == 0;
    }
    void operator ()(const JSON& json, ValueGetterSetterTester& value)
    {
        value.i = static_cast<int>(json["i"]);
    }
    JSON operator ()(const ValueGetterSetterTester& value)
    {
        JSON result = Object();
        result["i"] = value.i;
        return result;
    }
};

TEST_CASE("json_serialize concepts", "[json_serialize]")
{
    static_assert(sa::json::details::HasEmpty<std::vector<int>>);
    static_assert(sa::json::details::THasEmpty<JsonTester>);
    static_assert(!sa::json::details::HasEmpty<EnumTester>);
    static_assert(sa::json::details::AdapterHasEmpty<NestedTest>);
    static_assert(sa::json::details::TypeAdapterHasEmpty<ValueGetterSetterTester>);
}


TEST_CASE("json_serialize bool", "[json_serialize]")
{
    TestBool test;
    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);
    REQUIRE(result.Size() == 1);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);

    REQUIRE((bool)result["b"] == test.b);

    TestBool test2 = sa::json::Deserialize<TestBool>(result);
    REQUIRE(test.b == test2.b);
}

TEST_CASE("json_serialize struct 1", "[json_serialize]")
{
    Test1 test{ .a = 1 };
    auto result = sa::json::Serialize(test);
    REQUIRE(result.Size() == 1);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);
    REQUIRE((int)result["a"] == test.a);
}

TEST_CASE("json_serialize struct inheritance", "[json_serialize]")
{
    Test2 test{};
    test.a = 1;
    test.b = 2;
    auto result = sa::json::Serialize(test);
    auto json = result.Dump();
    REQUIRE(result.Size() == 2);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);
    REQUIRE((int)result["a"] == test.a);
    REQUIRE((int)result["b"] == test.b);
}

TEST_CASE("json_serialize struct member", "[json_serialize]")
{
    Test3 test{};
    test.t.a = 1;
    test.t.b = 2;
    auto result = sa::json::Serialize(test);
    auto json = result.Dump();
    REQUIRE(result.Size() == 1);
    REQUIRE(result["t"].GetType() == sa::json::JSON::Type::Object);
    const sa::json::JSON& t = result["t"];
    REQUIRE((int)t["a"] == test.t.a);
    REQUIRE((int)t["b"] == test.t.b);
}

TEST_CASE("json_serialize array 1", "[json_serialize]")
{
    int test[] = { 1, 2, 3, 4, 5 };
    auto result = sa::json::Serialize(test);
    auto json = result.Dump();
    REQUIRE(result.GetType() == sa::json::JSON::Type::Array);
    REQUIRE(result.Size() == 5);
    REQUIRE((int)result[0u] == test[0]);
    REQUIRE((int)result[1u] == test[1]);
    REQUIRE((int)result[2u] == test[2]);
    REQUIRE((int)result[3u] == test[3]);
    REQUIRE((int)result[4u] == test[4]);
}

TEST_CASE("json_serialize std::array 1", "[json_serialize]")
{
    std::array<int, 5> test = { 1, 2, 3, 4, 5 };
    auto result = sa::json::Serialize(test);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Array);
    REQUIRE(result.Size() == 5);
    REQUIRE((int)result[0u] == test[0]);
    REQUIRE((int)result[1u] == test[1]);
    REQUIRE((int)result[2u] == test[2]);
    REQUIRE((int)result[3u] == test[3]);
    REQUIRE((int)result[4u] == test[4]);
}

TEST_CASE("json_serialize std::vector 1", "[json_serialize]")
{
    std::vector<int> test;
    for (int i = 1; i <= 5; ++i)
        test.push_back(i);
    auto result = sa::json::Serialize(test);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Array);
    REQUIRE(result.Size() == 5);
    REQUIRE((int)result[0u] == test[0]);
    REQUIRE((int)result[1u] == test[1]);
    REQUIRE((int)result[2u] == test[2]);
    REQUIRE((int)result[3u] == test[3]);
    REQUIRE((int)result[4u] == test[4]);
}

TEST_CASE("json_serialize struct array member", "[json_serialize]")
{
    Test4 test{};
    auto result = sa::json::Serialize(test);
    auto json = result.Dump();
    REQUIRE(result.Size() == 1);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);
    const auto& t = result["t"];
    REQUIRE(t.Size() == 5);
    REQUIRE((int)t[0u] == test.t[0]);
    REQUIRE((int)t[1u] == test.t[1]);
    REQUIRE((int)t[2u] == test.t[2]);
    REQUIRE((int)t[3u] == test.t[3]);
    REQUIRE((int)t[4u] == test.t[4]);
}

TEST_CASE("json_deserialize struct 1", "[json_deserialize]")
{
    sa::json::JSON json = R"({"a" : 1})"_json;
    Test1 result = sa::json::Deserialize<Test1>(json);
    REQUIRE(result.a == 1);
}

TEST_CASE("json_deserialize struct inheritance", "[json_deserialize]")
{
    sa::json::JSON json = R"({"a" : 1, "b" : 2})"_json;
    Test2 result = sa::json::Deserialize<Test2>(json);
    REQUIRE(result.a == 1);
    REQUIRE(result.b == 2);
}

TEST_CASE("json_deserialize struct member", "[json_serialize]")
{
    sa::json::JSON json = R"({"t" : {"a" : 1, "b" : 2}})"_json;
    Test3 result = sa::json::Deserialize<Test3>(json);
    REQUIRE(result.t.a == 1);
    REQUIRE(result.t.b == 2);
}

TEST_CASE("json_deserialize array 1", "[json_serialize]")
{
    sa::json::JSON json = "[1, 2, 3, 4, 5]"_json;
    std::array<int, 5> result = sa::json::Deserialize<std::array<int, 5>>(json);
    REQUIRE(result[0u] == 1);
    REQUIRE(result[1u] == 2);
    REQUIRE(result[2u] == 3);
    REQUIRE(result[3u] == 4);
    REQUIRE(result[4u] == 5);
}

TEST_CASE("json_deserialize std::vector 1", "[json_serialize]")
{
    sa::json::JSON json = "[1, 2, 3, 4, 5]"_json;
    std::vector<int> result = sa::json::Deserialize<std::vector<int>>(json);
    REQUIRE(result.size() == 5);
    REQUIRE(result[0u] == 1);
    REQUIRE(result[1u] == 2);
    REQUIRE(result[2u] == 3);
    REQUIRE(result[3u] == 4);
    REQUIRE(result[4u] == 5);
}

TEST_CASE("json_deserialize struct array member", "[json_serialize]")
{
    sa::json::JSON json = R"({"t" : [1, 2, 3, 4, 5]})"_json;
    Test4 result = sa::json::Deserialize<Test4>(json);
    REQUIRE(result.t[0u] == 1);
    REQUIRE(result.t[1u] == 2);
    REQUIRE(result.t[2u] == 3);
    REQUIRE(result.t[3u] == 4);
    REQUIRE(result.t[4u] == 5);
}

TEST_CASE("json_serialize std::map", "[json_serialize]")
{
    std::map<std::string, int> test;
    for (int i = 1; i <= 5; ++i)
        test.emplace(std::to_string(i), i);
    auto result = sa::json::Serialize(test);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);
    REQUIRE(result.Size() == 5);
    REQUIRE((int)result["1"] == 1);
    REQUIRE((int)result["2"] == 2);
    REQUIRE((int)result["3"] == 3);
    REQUIRE((int)result["4"] == 4);
    REQUIRE((int)result["5"] == 5);

    std::map<std::string, int> test2 = sa::json::Deserialize<std::map<std::string, int>>(result);
    REQUIRE(test == test2);
}

TEST_CASE("json_serialize std::unordered_map", "[json_serialize]")
{
    std::unordered_map<std::string, int> test;
    for (int i = 1; i <= 5; ++i)
        test.emplace(std::to_string(i), i);
    auto result = sa::json::Serialize(test);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);
    REQUIRE(result.Size() == 5);
    REQUIRE((int)result["1"] == 1);
    REQUIRE((int)result["2"] == 2);
    REQUIRE((int)result["3"] == 3);
    REQUIRE((int)result["4"] == 4);
    REQUIRE((int)result["5"] == 5);

    std::unordered_map<std::string, int> test2 = sa::json::Deserialize<std::unordered_map<std::string, int>>(result);
    REQUIRE(test == test2);
}

TEST_CASE("json_serialize Adapter", "[json_serialize]")
{
    TestAdapter test;

    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);
    const sa::json::JSON& t = result["t"];
    REQUIRE(t.Size() == 5);
    REQUIRE((int)t[0] == test.t[0]);
    REQUIRE((int)t[1] == test.t[1]);
    REQUIRE((int)t[2] == test.t[2]);
    REQUIRE((int)t[3] == test.t[3]);
    REQUIRE((int)t[4] == test.t[4]);

    TestAdapter test2 = sa::json::Deserialize<TestAdapter>(result);
    REQUIRE(test == test2);
}

TEST_CASE("json_serialize nested Adapter", "[json_serialize]")
{
    NestedTest test;

    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);

    NestedTest test2 = sa::json::Deserialize<NestedTest>(result);
    REQUIRE(test == test2);
}

TEST_CASE("json_serialize compound objects", "[json_serialize]")
{
    Test7 test;

    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);

    Test7 test2 = sa::json::Deserialize<Test7>(result);
    REQUIRE(test == test2);
}

TEST_CASE("json_serialize ValueSetter/ValueGetter", "[json_serialize]")
{
    ValueGetterSetterTester test;

    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);
    REQUIRE(result.GetType() == sa::json::JSON::Type::Object);

    auto test2 = sa::json::Deserialize<ValueGetterSetterTester>(result);
    REQUIRE(test.i == test2.i);
}

TEST_CASE("json_serialize EnumTester", "[json_serialize]")
{
    EnumTester test;

    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);

    EnumTester test2 = sa::json::Deserialize<EnumTester>(result);
    REQUIRE(test.test == test2.test);
}

TEST_CASE("json_serialize JsonTester", "[json_serialize]")
{
    JsonTester test;
    test.jsontest = R"({"t" : [1, 2, 3, 4, 5]})"_json;;

    auto result = sa::json::Serialize(test);
    auto json = result.Dump(true);

    JsonTester test2 = sa::json::Deserialize<JsonTester>(result);
    REQUIRE(test == test2);
}





TEST_CASE("json_serialize DeserializeSafe success", "[json_serialize]")
{
    sa::json::JSON json = R"({"t" : {"a" : 1, "b" : 2}})"_json;
    auto result = sa::json::DeserializeSafe<Test3>(json);
    REQUIRE(result.has_value());
    REQUIRE(result->t.a == 1);
    REQUIRE(result->t.b == 2);
}

TEST_CASE("json_serialize DeserializeSafe fail", "[json_serialize]")
{
    sa::json::JSON json = R"({"t" : {"x" : 1, "y" : 2}})"_json;
    auto result = sa::json::DeserializeSafe<Test3>(json);
    REQUIRE(!result.has_value());
    // Will fail with "No member with name..."
    REQUIRE(result.error().starts_with("No member with name"));
}

TEST_CASE("json_serialize DeserializeSafe ignoreNull", "[json_serialize]")
{
    sa::json::JSON json = R"({"a" : 1, "y" : 2})"_json;
    auto result = sa::json::DeserializeSafe<Test6>(json);
    REQUIRE(result.has_value());
    REQUIRE(result->a == 1);
}

TEST_CASE("json_serialize Serialize ignoreNull", "[json_serialize]")
{
    sa::json::JSON json = R"({"a" : 1, "b" : 2})"_json;
    Test6 test { .a = 1, .b = 2 };
    auto result = sa::json::Serialize(test);
    REQUIRE(result == json);
}
