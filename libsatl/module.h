/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "native_function.h"
#include "value.h"
#include "metatable.h"

namespace sa::tl {

// This is just a Table which can be used to put functions and
// constants into some sort of namespace.
class Module final : public Table
{
public:
    Module();
    template<typename T>
    void AddConst(std::string name, T value)
    {
        emplace(std::move(name), MakeConstValue(std::move(value)));
    }
    template<typename T>
    void AddValue(std::string name, T value)
    {
        emplace(std::move(name), MakeValue<T>(std::move(value)));
    }
    void AddFunction(std::string name, std::function<NativeFunctionSignature>&& func, bool safe = true);
    template<typename R, typename... Args>
    void AddFunction(std::string name, auto&& func, bool safe = true)
    {
        emplace(std::move(name), MakeConstValue<CallablePtr>(std::make_shared<NativeFunctionGeneric<R, Args...>>(name, std::move(func), safe)));
    }
    // Const objects are always const
    template<typename Class>
    void SetInstance(const MetaTable& meta, std::string name, const Class& inst)
    {
        SATL_ASSERT(meta.GetClassName() == TypeName<Class>::Get());
        AddConst(std::forward<std::string>(name), MakeObject(meta, &const_cast<Class&>(inst), true));
    }
    // But non-const objects can also be made const to scripts
    template<typename Class>
    void SetInstance(const MetaTable& meta, std::string name, Class& inst, bool _const = false) requires (!std::is_const<Class>::value)
    {
        SATL_ASSERT(meta.GetClassName() == TypeName<Class>::Get());
        AddConst(std::forward<std::string>(name), MakeObject(meta, &inst, _const));
    }

    [[nodiscard]] constexpr bool IsModule() const override { return true; }
};

using ModulePtr = std::shared_ptr<Module>;

inline ModulePtr MakeModule()
{
    return std::make_shared<Module>();
}

}
