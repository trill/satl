/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "node.h"
#include <sstream>
#include "value.h"
#include "type_hint.h"

namespace sa::tl {

class VariableExpr;

// Statements

class Assign final : public Node
{
public:
    Assign(NodePtr lhs, NodePtr rhs) : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}
    constexpr Class GetClass() const override { return Class::Assign; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Assign" << std::endl;
        lhs_->Dump(os, indent + 2);
        rhs_->Dump(os, indent + 2);
    }
    void SetConst(bool value) { const_ = value; }
    bool IsConst() const { return const_; }
    void SetGlobal(bool value) { global_ = value; }
    bool IsGlobal() const { return global_; }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        callback(*lhs_, scope);
        if (recursive)
            lhs_->ForEachChild(callback, scope);
        callback(*rhs_, scope);
        if (recursive)
            rhs_->ForEachChild(callback, scope);
    }

private:
    NodePtr lhs_;
    NodePtr rhs_;
    bool const_{ false };
    bool global_{ false };
};

class CompoundAssign final : public Node
{
public:
    enum class Op
    {
        Add,
        And,
        Div,
        Mod,
        Mul,
        Or,
        Shl,
        Shr,
        Sub,
        Xor,
        Pow,
        IDiv
    };
    CompoundAssign(Op op, NodePtr lhs, NodePtr rhs) :
        op_(op),
        lhs_(std::move(lhs)),
        rhs_(std::move(rhs))
    {
    }
    constexpr Class GetClass() const override { return Class::CompoundAssign; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "CompoundAssign" << std::endl;
        lhs_->Dump(os, indent + 2);
        rhs_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        callback(*lhs_, scope);
        if (recursive)
            lhs_->ForEachChild(callback, scope);
        callback(*rhs_, scope);
        if (recursive)
            rhs_->ForEachChild(callback, scope);
    }

private:
    Op op_;
    NodePtr lhs_;
    NodePtr rhs_;
};

class TableInitialization final : public Node
{
public:
    TableInitialization() = default;
    constexpr Class GetClass() const override { return Class::TableInitialization; };
    void Add(ValuePtr key, NodePtr value, TypeHint typeHint);
    bool HasKey(const Value& value) const;
    size_t GetCount() const { return items_.size(); }
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "TableInit" << std::endl;
        for (const auto& v : items_)
        {
            os << std::string(indent + 2, ' ');
            v.key->Dump(os);
            os << " = " << std::endl;
            v.value->Dump(os, indent + 4);
        }
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        for (const auto& v : items_)
        {
            callback(*v.value, scope);
            if (recursive)
                v.value->ForEachChild(callback, scope);
        }
    }

private:
    struct Item
    {
        ValuePtr key;
        NodePtr value;
        TypeHint type;
    };
    std::vector<Item> items_;
};

class Block final : public Node
{
public:
    Block() = default;
    constexpr Class GetClass() const override { return Class::Block; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Add(NodePtr node) { children_.push_back(std::move(node)); }
    bool IsEmpty() const { return children_.empty(); }
    const std::vector<NodePtr>& GetChildren() const { return children_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Block" << std::endl;
        for (const auto& c : children_)
            c->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        ScopeNew s(scope);
        for (const auto& child : children_)
        {
            callback(*child, scope);
            if (recursive)
                child->ForEachChild(callback, scope);
        }
    }

private:
    std::vector<NodePtr> children_;
};

class Root final : public Node
{
public:
    Root() = default;
    constexpr Class GetClass() const override { return Class::Root; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Add(NodePtr node) { children_.push_back(std::move(node)); }
    const std::vector<NodePtr>& GetChildren() const { return children_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Root" << std::endl;
        for (const auto& c : children_)
            c->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        for (const auto& child : children_)
        {
            callback(*child, scope);
            if (recursive)
                child->ForEachChild(callback, scope);
        }
    }

private:
    std::vector<NodePtr> children_;
};

class Try final : public Node
{
public:
    struct CatchBlock
    {
        std::string ident;
        NodePtr block;
    };
    explicit Try(NodePtr body) : body_(std::move(body)) {}
    constexpr Class GetClass() const override { return Class::Try; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetFinally(NodePtr nd) { finally_ = std::move(nd); }
    bool AddCatch(int code, CatchBlock&& block)
    {
        if (catchBlocks_.contains(code))
            // Can not have more blocks that catch the same
            return false;
        catchBlocks_.emplace(code, std::move(block));
        return true;
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Try" << std::endl;
        if (body_)
            body_->Dump(os, indent + 2);
        for (const auto& b : catchBlocks_)
        {
            os << std::string(indent, ' ');
            os << "Catch " << std::to_string(b.first) << std::endl;
            b.second.block->Dump(os, indent + 2);
        }
        if (finally_)
        {
            os << std::string(indent, ' ');
            os << "Finally" << std::endl;
            finally_->Dump(os, indent + 2);
        }
    }
    bool IsValid() const { return !catchBlocks_.empty() || !!finally_; }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (body_)
        {
            callback(*body_, scope);
            if (recursive)
                body_->ForEachChild(callback, scope);
        }
        for (const auto& b : catchBlocks_)
        {
            callback(*b.second.block, scope);
            if (recursive)
                b.second.block->ForEachChild(callback, scope);
        }
        if (finally_)
        {
            callback(*finally_, scope);
            if (recursive)
                finally_->ForEachChild(callback, scope);
        }
    }

private:
    const CatchBlock* GetCatch(int code) const
    {
        {
            const auto it = catchBlocks_.find(code);
            if (it != catchBlocks_.end())
                return &it->second;
        }
        {
            const auto it = catchBlocks_.find(0);
            if (it != catchBlocks_.end())
                return &it->second;
        }

        return nullptr;
    }
    NodePtr body_;
    NodePtr finally_;
    std::map<int, CatchBlock> catchBlocks_;
};

class Break final : public Node
{
public:
    Break() = default;
    constexpr Class GetClass() const override { return Class::Break; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Break" << std::endl;
    }
};

class Fallthrough final : public Node
{
public:
    Fallthrough() = default;
    constexpr Class GetClass() const override { return Class::Fallthrough; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Fallthrough" << std::endl;
    }
};

class Continue final : public Node
{
public:
    Continue() = default;
    constexpr Class GetClass() const override { return Class::Continue; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Continue" << std::endl;
    }
};

class Do final : public Node
{
public:
    Do() = default;
    constexpr Class GetClass() const override { return Class::Do; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetCondition(NodePtr value) { condition_ = std::move(value); }
    void SetBlock(NodePtr value) { block_ = std::move(value); }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Do" << std::endl;
        if (condition_)
            condition_->Dump(os, indent + 2);
        if (block_)
            block_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (condition_)
        {
            callback(*condition_, scope);
            if (recursive)
                condition_->ForEachChild(callback, scope);
        }
        if (block_)
        {
            callback(*block_, scope);
            if (recursive)
                block_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr condition_;
    NodePtr block_;
};

class For final : public Node
{
public:
    For() = default;
    constexpr Class GetClass() const override { return Class::For; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetInit(NodePtr value) { init_ = std::move(value); }
    void SetCondition(NodePtr value) { condition_ = std::move(value); }
    void SetIncrement(NodePtr value) { increment_ = std::move(value); }
    void SetBlock(NodePtr value) { block_ = std::move(value); }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "For" << std::endl;
        if (init_)
            init_->Dump(os, indent + 2);
        if (condition_)
            condition_->Dump(os, indent + 2);
        if (increment_)
            increment_->Dump(os, indent + 2);
        if (block_)
            block_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        ScopeNew s(scope);
        if (init_)
        {
            callback(*init_, scope);
            if (recursive)
                init_->ForEachChild(callback, scope);
        }
        if (condition_)
        {
            callback(*condition_, scope);
            if (recursive)
                condition_->ForEachChild(callback, scope);
        }
        if (increment_)
        {
            callback(*increment_, scope);
            if (recursive)
                increment_->ForEachChild(callback, scope);
        }
        if (block_)
        {
            callback(*block_, scope);
            if (recursive)
                block_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr init_;
    NodePtr condition_;
    NodePtr increment_;
    NodePtr block_;
};

class ForEach final : public Node
{
public:
    ForEach() = default;
    constexpr Class GetClass() const override { return Class::ForEach; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetKey(NodePtr value) { key_ = std::move(value); }
    void SetValue(NodePtr value) { value_ = std::move(value); }
    void SetContainer(NodePtr value) { container_ = std::move(value); }
    void SetBlock(NodePtr value) { block_ = std::move(value); }
    void SetReverse() { reverse_ = true; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "ForEach" << std::endl;
        if (key_)
            key_->Dump(os, indent + 2);
        if (value_)
            value_->Dump(os, indent + 2);
        if (container_)
            container_->Dump(os, indent + 2);
        if (block_)
            block_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        ScopeNew s(scope);
        if (key_)
        {
            callback(*key_, scope);
            if (recursive)
                key_->ForEachChild(callback, scope);
        }
        if (value_)
        {
            callback(*value_, scope);
            if (recursive)
                value_->ForEachChild(callback, scope);
        }
        if (container_)
        {
            callback(*container_, scope);
            if (recursive)
                container_->ForEachChild(callback, scope);
        }
        if (block_)
        {
            callback(*block_, scope);
            if (recursive)
                block_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr key_;
    NodePtr value_;
    NodePtr container_;
    NodePtr block_;
    bool reverse_{ false };
};

class Using final : public Node
{
public:
    Using() = default;
    constexpr Class GetClass() const override { return Class::Using; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetIdent(NodePtr value);
    void SetEpxr(NodePtr value);
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Using" << std::endl;
        if (ident_)
            ident_->Dump(os, indent + 2);
        if (expr_)
            expr_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (expr_)
        {
            callback(*expr_, scope);
            if (recursive)
                expr_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr ident_;
    NodePtr expr_;
};

class VariableDefinition final : public Node
{
public:
    explicit VariableDefinition(std::string name) :
        name_(MakeString(std::forward<std::string>(name)))
    {}
    constexpr Class GetClass() const override { return Class::VariableDefinition; };
    ValuePtr Evaluate(Context&) const override { return MakeValue(name_); }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "VariableDefinition " << *name_ << std::endl;
    }
    const std::string& GetName() const { return *name_; }

    TypeHint typeHint_{ TypeHint::Any };
private:
    StringPtr name_;
};

class Function final : public Callable
{
public:
    Function() = default;
    constexpr Class GetClass() const override { return Class::Function; };
    ValuePtr Evaluate(Context& ctx) const override;
    ValuePtr operator()(Context& ctx, const FunctionArguments& args) const override;
    ValuePtr operator()(Context& ctx, const Value& thisValue, const FunctionArguments& args) const override;
    const std::string& GetParameterName(size_t index) const;
    TypeHint GetParameterType(size_t index) const;
    void SetName(std::string value)
    {
        name_ = std::move(value);
    }
    bool HasName() const { return !name_.empty(); }
    const std::string& GetName() const final { return name_; }
    void AddParameter(std::shared_ptr<VariableDefinition> value) { parameters_.push_back(std::move(value)); }
    void AddCapture(std::shared_ptr<VariableExpr> value) { captures_.push_back(std::move(value)); }
    void Capture(Context& ctx, const ValuePtr& funcVal) const;
    int ParamCount() const override { return (int)parameters_.size(); }
    void SetBody(NodePtr value) { body_ = std::move(value); }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Function " << name_ << std::endl;
        for (const auto& param : parameters_)
            param->Dump(os, indent + 2);
        if (body_)
            body_->Dump(os, indent + 2);
    }
    std::string GetFriendlyName() const override { return name_ + "()"; }
    void ForEachChild(const std::function<void(const Node& nd, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        ScopeNew s(scope);
        for (const auto& param : parameters_)
            callback(*param, scope);
        if (body_)
        {
            callback(*body_, scope);
            if (recursive)
                body_->ForEachChild(callback, scope);
        }
    }
    const std::vector<std::shared_ptr<VariableExpr>>& GetCaptures() const { return captures_; }

    TypeHint typeHint_{ TypeHint::Any };
private:
    ValuePtr Execute(Context& ctx) const;
    std::string name_;
    std::vector<std::shared_ptr<VariableDefinition>> parameters_;
    std::vector<std::shared_ptr<VariableExpr>> captures_;
    NodePtr body_;
};

class If final : public Node
{
public:
    If() = default;
    constexpr Class GetClass() const override { return Class::If; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetExpr(NodePtr value) { expr_ = std::move(value); }
    const NodePtr& GetExpr() const { return expr_; }
    void SetTrueBranch(NodePtr value) { trueBrach_ = std::move(value); }
    const NodePtr& GetTrueBranch() const { return trueBrach_; }
    void SetFalseBranch(NodePtr value) { falseBrach_ = std::move(value); }
    const NodePtr& GetFalseBrach() const { return falseBrach_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "If" << std::endl;
        if (expr_)
            expr_->Dump(os, indent + 2);
        if (trueBrach_)
            trueBrach_->Dump(os, indent + 2);
        if (falseBrach_)
            falseBrach_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        ScopeNew s(scope);
        if (expr_)
        {
            callback(*expr_, scope);
            if (recursive)
                expr_->ForEachChild(callback, scope);
        }
        if (trueBrach_)
        {
            ScopeNew s(scope);
            callback(*trueBrach_, scope);
            if (recursive)
                trueBrach_->ForEachChild(callback, scope);
        }
        if (falseBrach_)
        {
            ScopeNew s(scope);
            callback(*falseBrach_, scope);
            if (recursive)
                falseBrach_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr expr_;
    NodePtr trueBrach_;
    NodePtr falseBrach_;
};

class Return final : public Node
{
public:
    Return() = default;
    constexpr Class GetClass() const override { return Class::Return; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetExpression(NodePtr expr) { expr_ = std::move(expr); }
    const NodePtr& GetExpression() const { return expr_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Return" << std::endl;
        if (expr_)
            expr_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node& nd, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (expr_)
        {
            callback(*expr_, scope);
            if (recursive)
                expr_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr expr_;
};

class Switch final : public Node
{
public:
    Switch() = default;
    constexpr Class GetClass() const override { return Class::Switch; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetExpression(NodePtr expression) { expression_ = std::move(expression); }
    void SetDefault(NodePtr def) { default_ = std::move(def); }
    void AddBlock(NodePtr label, NodePtr block)
    {
        blocks_.push_back(std::make_pair<NodePtr, NodePtr>(std::move(label), std::move(block)));
    }
    bool HasDefault() const { return !!default_; }
    bool HasLabel(const Value& value) const;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Switch" << std::endl;
        if (expression_)
            expression_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (expression_)
        {
            callback(*expression_, scope);
            if (recursive)
                expression_->ForEachChild(callback, scope);
        }
        if (default_)
        {
            ScopeNew s(scope);
            callback(*default_, scope);
            if (recursive)
                default_->ForEachChild(callback, scope);
        }
        for (const auto& block : blocks_)
        {
            if (block.first)
            {
                callback(*block.first, scope);
                if (recursive)
                    block.first->ForEachChild(callback, scope);
            }
            if (block.second)
            {
                ScopeNew s(scope);
                callback(*block.second, scope);
                if (recursive)
                    block.second->ForEachChild(callback, scope);
            }
        }
    }

private:
    NodePtr expression_;
    NodePtr default_;
    std::vector<std::pair<NodePtr, NodePtr>> blocks_;
};

class While final : public Node
{
public:
    While() = default;
    constexpr Class GetClass() const override { return Class::While; };
    ValuePtr Evaluate(Context& ctx) const override;
    void SetCondition(NodePtr value) { condition_ = std::move(value); }
    const NodePtr& GetCondition() const { return condition_; }
    void SetBlock(NodePtr value) { block_ = std::move(value); }
    const NodePtr& GetBlock() const { return block_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "While" << std::endl;
        if (condition_)
            condition_->Dump(os, indent + 2);
        if (block_)
            block_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        ScopeNew s(scope);
        if (condition_)
        {
            callback(*condition_, scope);
            if (recursive)
                condition_->ForEachChild(callback, scope);
        }
        if (block_)
        {
            ScopeNew s(scope);
            callback(*block_, scope);
            if (recursive)
                block_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr condition_;
    NodePtr block_;
};

class Defer final : public Node
{
public:
    Defer() = default;
    constexpr Class GetClass() const override { return Class::Defer; };
    ValuePtr Evaluate(Context& ctx) const override;
    ValuePtr Execute(Context& ctx) const;
    void SetBlock(NodePtr value) { block_ = std::move(value); }
    const NodePtr& GetBlock() const { return block_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Defer" << std::endl;
        if (block_)
            block_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (block_)
        {
            ScopeNew s(scope);
            callback(*block_, scope);
            if (recursive)
                block_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr block_;
};

// Expressions

class ValueExpr final : public Node
{
public:
    explicit ValueExpr(ValuePtr value) : value_(std::move(value)) {}
    constexpr Class GetClass() const override { return Class::ValueExpr; };
    ValuePtr Evaluate(Context&) const override;
    const ValuePtr& GetValue() const { return value_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "ValueExpr " << value_->ToString() << std::endl;
    }
    std::string GetFriendlyName() const override { return value_->ToString(); }

private:
    ValuePtr value_;
};

class VariableExpr final : public Node
{
public:
    explicit VariableExpr(std::string name) : name_(std::move(name)) {}
    constexpr Class GetClass() const override { return Class::VariableExpr; };
    ValuePtr Evaluate(Context& ctx) const override;
    const std::string& GetName() const { return name_; }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "VariableExpr " << name_ << std::endl;
    }
    std::string GetFriendlyName() const override
    {
        if (name_.empty())
            return Node::GetClassName();
        std::stringstream ss;
        ss << "variable: " << name_;
        return ss.str();
    }

private:
    std::string name_;
};

template<bool Conditional>
class _MemberAccess final : public Node
{
public:
    _MemberAccess(NodePtr owner, NodePtr value, bool computed) :
        owner_(std::move(owner)),
        value_(std::move(value)),
        computed_(computed)
    {
    }
    constexpr Class GetClass() const override { return Class::MemberAccess; };
    ValuePtr Evaluate(Context&) const override;
    ValuePtr EnsureSubscript(Context&, const ValuePtr& value);
    std::string GetName() const
    {
        if (const auto* v = To<VariableExpr>(value_.get()))
            return v->GetName();
        if (const auto* v = To<ValueExpr>(value_.get()))
            return v->GetValue()->ToString();
        return "";
    }
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        if constexpr (Conditional)
            os << "ConditionalMemberAccess" << std::endl;
        else
            os << "MemberAccess" << std::endl;
        owner_->Dump(os, indent + 2);
        value_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        callback(*value_, scope);
        if (recursive)
            value_->ForEachChild(callback, scope);
    }

private:
    NodePtr owner_;
    NodePtr value_;
    bool computed_;
};

using MemberAccess = _MemberAccess<false>;
using ConditionalMemberAccess = _MemberAccess<true>;

class CallExpr final : public Node
{
public:
    explicit CallExpr(NodePtr func) : func_(std::move(func)) {}
    constexpr Class GetClass() const override { return Class::CallExpr; };
    void AddArgument(NodePtr expr) { arguments_.push_back(std::move(expr)); }
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "CallExpr " << GetName() << std::endl;
        for (const auto& a : arguments_)
            a->Dump(os, indent + 2);
    }
    std::string GetName() const
    {
        if (const auto* v = To<VariableExpr>(func_.get()))
            return v->GetName();
        if (const auto* v = To<MemberAccess>(func_.get()))
            if (!v->GetName().empty())
                return v->GetName();
        if (const auto* v = To<ConditionalMemberAccess>(func_.get()))
            if (!v->GetName().empty())
                return v->GetName();

        return "(anonymous)";
    }
    const std::vector<NodePtr>& GetArguments() const { return arguments_; }
    std::string GetFriendlyName() const override
    {
        if (func_)
            return func_->GetFriendlyName();
        return Node::GetClassName();
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        callback(*func_, scope);
        if (recursive)
            func_->ForEachChild(callback, scope);
        for (const auto& arg : arguments_)
        {
            callback(*arg, scope);
            if (recursive)
                arg->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr func_;
    std::vector<NodePtr> arguments_;
};

class BinaryExpr final : public Node
{
public:
    enum class Op
    {
        // Logical
        Equal,
        Unequal,
        Lt,
        Gt,
        LtEqual,
        GtEqual,
        LogicalAnd,
        LogicalOr,
        StrictEqual,
        StrictUnequal,
        ThreeWayCmp,
        In,
        // Bitwise
        BitwiseAnd,
        BitwiseOr,
        BitwiseXor,
        Shl,
        Shr,
        // Sum
        Add,
        Sub,
        // Product
        Mul,
        Div,
        Mod,
        Pow,
        IDiv,
    };

    BinaryExpr(Op op, NodePtr lhs, NodePtr rhs) :
        op_(op),
        lhs_(std::move(lhs)),
        rhs_(std::move(rhs))
    {
    }
    constexpr Class GetClass() const override { return Class::BinaryExpr; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        switch (op_)
        {
        case Op::Equal:
            os << "Equal" << std::endl;
            break;
        case Op::StrictEqual:
            os << "StrictEqual" << std::endl;
            break;
        case Op::Unequal:
            os << "Unequal" << std::endl;
            break;
        case Op::StrictUnequal:
            os << "StrictUnequal" << std::endl;
            break;
        case Op::ThreeWayCmp:
            os << "ThreeWayCmp" << std::endl;
            break;
        case Op::In:
            os << "In" << std::endl;
            break;
        case Op::Lt:
            os << "Lt" << std::endl;
            break;
        case Op::Gt:
            os << "Gt" << std::endl;
            break;
        case Op::LtEqual:
            os << "LtEqual" << std::endl;
            break;
        case Op::GtEqual:
            os << "GtEqual" << std::endl;
            break;
        case Op::LogicalAnd:
            os << "LogicalAnd" << std::endl;
            break;
        case Op::LogicalOr:
            os << "LogicalOr" << std::endl;
            break;
        case Op::BitwiseAnd:
            os << "BitwiseAnd" << std::endl;
            break;
        case Op::BitwiseOr:
            os << "BitwiseOr" << std::endl;
            break;
        case Op::BitwiseXor:
            os << "BitwiseXor" << std::endl;
            break;
        case Op::Shl:
            os << "Shl" << std::endl;
            break;
        case Op::Shr:
            os << "Shr" << std::endl;
            break;
        case Op::Add:
            os << "Add" << std::endl;
            break;
        case Op::Sub:
            os << "Sub" << std::endl;
            break;
        case Op::Mul:
            os << "Mul" << std::endl;
            break;
        case Op::Div:
            os << "Div" << std::endl;
            break;
        case Op::Mod:
            os << "Mod" << std::endl;
            break;
        case Op::Pow:
            os << "Pow" << std::endl;
            break;
        case Op::IDiv:
            os << "IDiv" << std::endl;
            break;
        }
        lhs_->Dump(os, indent + 2);
        rhs_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node& nd, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        callback(*lhs_, scope);
        if (recursive)
            lhs_->ForEachChild(callback, scope);
        callback(*rhs_, scope);
        if (recursive)
            rhs_->ForEachChild(callback, scope);
    }

private:
    Op op_;
    NodePtr lhs_;
    NodePtr rhs_;
};

class UnaryExpr final : public Node
{
public:
    enum class Op
    {
        UnaryPlus,
        Negate,
        BitwiseNot,
        LogicalNot,
        CountOf,
        NameOf,
        Stringify,
        PreIncrement,
        PreDecrement,
        PostIncrement,
        PostDecrement,
    };
    UnaryExpr(Op op, NodePtr expr) : op_(op), expr_(std::move(expr)) {}
    constexpr Class GetClass() const override { return Class::UnaryExpr; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        switch (op_)
        {
        case Op::UnaryPlus:
            os << "Negate" << std::endl;
            break;
        case Op::Negate:
            os << "Negate" << std::endl;
            break;
        case Op::BitwiseNot:
            os << "BitwiseNot" << std::endl;
            break;
        case Op::LogicalNot:
            os << "LogicalNot" << std::endl;
            break;
        case Op::CountOf:
            os << "CountOf" << std::endl;
            break;
        case Op::NameOf:
            os << "NameOf" << std::endl;
            break;
        case Op::Stringify:
            os << "Stringify" << std::endl;
            break;
        case Op::PreIncrement:
            os << "PreIncrement" << std::endl;
            break;
        case Op::PreDecrement:
            os << "PreDecrement" << std::endl;
            break;
        case Op::PostIncrement:
            os << "PostIncrement" << std::endl;
            break;
        case Op::PostDecrement:
            os << "PostDecrement" << std::endl;
            break;
        }
        expr_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        callback(*expr_, scope);
        if (recursive)
            expr_->ForEachChild(callback, scope);
    }

private:
    Op op_;
    NodePtr expr_;
};

class TernaryExpr final : public Node
{
public:
    TernaryExpr(NodePtr cond, NodePtr trueExpr, NodePtr falseExpr) :
        condition_(std::move(cond)),
        trueExpr_(std::move(trueExpr)),
        falseExpr_(std::move(falseExpr))
    {
    }
    constexpr Class GetClass() const override { return Class::TernaryExpr; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "TernaryExpr" << std::endl;
        if (condition_)
            condition_->Dump(os, indent + 2);
        if (trueExpr_)
            trueExpr_->Dump(os, indent + 2);
        if (falseExpr_)
            falseExpr_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node&, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (condition_)
        {
            callback(*condition_, scope);
            if (recursive)
                condition_->ForEachChild(callback, scope);
        }
        if (trueExpr_)
        {
            callback(*trueExpr_, scope);
            if (recursive)
                trueExpr_->ForEachChild(callback, scope);
        }
        if (falseExpr_)
        {
            callback(*falseExpr_, scope);
            if (recursive)
                falseExpr_->ForEachChild(callback, scope);
        }
    }

private:
    NodePtr condition_;
    NodePtr trueExpr_;
    NodePtr falseExpr_;
};

class Range final : public Node
{
public:
    Range(NodePtr lhs, NodePtr rhs) :
        lhs_(std::move(lhs)),
        rhs_(std::move(rhs))
    {
    }
    constexpr Class GetClass() const override { return Class::Range; };
    ValuePtr Evaluate(Context& ctx) const override;
    void Dump(std::ostream& os, int indent) const override
    {
        os << std::string(indent, ' ');
        os << "Range" << std::endl;
        if (lhs_)
            lhs_->Dump(os, indent + 2);
        if (rhs_)
            rhs_->Dump(os, indent + 2);
    }
    void ForEachChild(const std::function<void(const Node& nd, const ScopeID&)>& callback, ScopeID& scope, bool recursive = true) const override
    {
        if (lhs_)
        {
            callback(*lhs_, scope);
            if (recursive)
                lhs_->ForEachChild(callback, scope);
        }
        if (rhs_)
        {
            callback(*rhs_, scope);
            if (recursive)
                rhs_->ForEachChild(callback, scope);
        }
    }
    const NodePtr& GetLhs() const { return lhs_; }
    const NodePtr& GetRhs() const { return rhs_; }
private:
    NodePtr lhs_;
    NodePtr rhs_;
};

template<>
constexpr bool Is<Assign>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Assign;
}
template<>
constexpr bool Is<CompoundAssign>(const Node& obj)
{
    return obj.GetClass() == Node::Class::CompoundAssign;
}
template<>
constexpr bool Is<TableInitialization>(const Node& obj)
{
    return obj.GetClass() == Node::Class::TableInitialization;
}
template<>
constexpr bool Is<Block>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Block;
}
template<>
constexpr bool Is<Root>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Root;
}
template<>
constexpr bool Is<Break>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Break;
}
template<>
constexpr bool Is<Do>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Do;
}
template<>
constexpr bool Is<For>(const Node& obj)
{
    return obj.GetClass() == Node::Class::For;
}
template<>
constexpr bool Is<ForEach>(const Node& obj)
{
    return obj.GetClass() == Node::Class::ForEach;
}
template<>
constexpr bool Is<Function>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Function;
}
template<>
constexpr bool Is<If>(const Node& obj)
{
    return obj.GetClass() == Node::Class::If;
}
template<>
constexpr bool Is<Switch>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Switch;
}
template<>
constexpr bool Is<While>(const Node& obj)
{
    return obj.GetClass() == Node::Class::While;
}
template<>
constexpr bool Is<Continue>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Continue;
}
template<>
constexpr bool Is<Return>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Return;
}
template<>
constexpr bool Is<ValueExpr>(const Node& obj)
{
    return obj.GetClass() == Node::Class::ValueExpr;
}
template<>
constexpr bool Is<CallExpr>(const Node& obj)
{
    return obj.GetClass() == Node::Class::CallExpr;
}
template<>
constexpr bool Is<VariableExpr>(const Node& obj)
{
    return obj.GetClass() == Node::Class::VariableExpr;
}
template<>
constexpr bool Is<VariableDefinition>(const Node& obj)
{
    return obj.GetClass() == Node::Class::VariableDefinition;
}
template<>
constexpr bool Is<BinaryExpr>(const Node& obj)
{
    return obj.GetClass() == Node::Class::BinaryExpr;
}
template<>
constexpr bool Is<UnaryExpr>(const Node& obj)
{
    return obj.GetClass() == Node::Class::UnaryExpr;
}
template<>
constexpr bool Is<TernaryExpr>(const Node& obj)
{
    return obj.GetClass() == Node::Class::TernaryExpr;
}
template<>
constexpr bool Is<MemberAccess>(const Node& obj)
{
    return obj.GetClass() == Node::Class::MemberAccess;
}
template<>
constexpr bool Is<Using>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Using;
}
template<>
constexpr bool Is<Range>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Range;
}

}
