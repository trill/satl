/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstddef>
#include <map>
#include <memory>
#include <ostream>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>
#include "typename.h"
#include "utils.h"

// Customize what types to use
#if !defined(INTEGER_TYPE)
#define INTEGER_TYPE int64_t
#endif
#if !defined(FLOATING_POINT_TYPE)
#define FLOATING_POINT_TYPE float
#endif

namespace sa::tl {

struct Error;
class Context;
class Node;
class Callable;
class Value;
class Table;
class MetaTable;
class Object;

using ValuePtr = std::shared_ptr<Value>;
using TablePtr = std::shared_ptr<Table>;
using ObjectPtr = std::shared_ptr<Object>;
using NodePtr = std::shared_ptr<Node>;
using CallablePtr = std::shared_ptr<Callable>;

using Integer = INTEGER_TYPE;
using Unsigned = std::make_unsigned<Integer>::type;
using Float = FLOATING_POINT_TYPE;

using StringPtr = std::shared_ptr<std::string>;
// Table keys can be one of these types
using KeyType = std::variant<bool, Integer, Float, std::string, CallablePtr, TablePtr, ObjectPtr>;
std::string KeyToString(const KeyType& key);

using FunctionArguments = std::vector<ValuePtr>;

class Container
{
public:
    virtual ~Container();
    [[nodiscard]] virtual constexpr bool IsTable() const { return false; }
    [[nodiscard]] virtual constexpr bool IsObject() const { return false; }
    [[nodiscard]] virtual constexpr bool IsModule() const { return false; }
};

using ContainerPtr = std::shared_ptr<Container>;

class Table : public std::map<KeyType, ValuePtr>, public Container
{
public:
    ~Table() override;
    [[nodiscard]] ValuePtr GetValue(const KeyType& key) const
    {
        const auto it = find(key);
        if (it != end())
            return it->second;
        return {};
    }
    [[nodiscard]] bool IsArray() const { return isArray_; }
    void Add(ValuePtr&& value);
    void Add(const ValuePtr& value);
    void Add(const KeyType& key, ValuePtr&& value);
    void Add(const KeyType& key, const ValuePtr& value);
    template<typename T>
    void Add(const KeyType& key, T value);
    template<typename T>
    void Add(T value);
    void Append(const Table& tbl);
    void Delete(const KeyType& key);
    void Clear();
    void CopyTo(Table& dst) const;
    std::optional<KeyType> GetValueKey(const Value& value) const;
    template<typename PredicateT>
    void DeleteIf(PredicateT&& predicate)
    {
        if (sa::tl::DeleteIf(*this, std::forward<PredicateT>(predicate)))
            isArray_ = false;
    }
    [[nodiscard]] size_t CountOf(const Value& value) const;
    [[nodiscard]] KeyType NextIndex() const { return nextIndex_; }
    [[nodiscard]] size_t Types() const { return types_; }

private:
    bool isArray_{ true };
    Integer nextIndex_{ 0 };
    size_t types_{ 0 };
    [[nodiscard]] constexpr bool IsTable() const final { return true; }
};

// A wrapper around a C++ object with meta data
class Object final : public Container
{
public:
    Object(std::string_view className, const MetaTable& meta, void* inst, bool _const = false) :
        className_(className),
        meta_(meta),
        instance_(inst),
        const_(_const)
    { }
    ~Object() override;

    [[nodiscard]] const MetaTable& GetMetatable() const { return meta_; }
    [[nodiscard]] void* GetInstance() const { return instance_; }
    // When the object this class wraps is destroyed, call this function because instance_ wouldn't point to a valid object anymore
    void ResetInstance() { instance_ = nullptr; }
    [[nodiscard]] const std::string_view& GetClassName() const { return className_; }
    [[nodiscard]] bool IsConst() const { return const_; }

private:
    [[nodiscard]] constexpr bool IsObject() const override { return true; }
    std::string_view className_;
    const MetaTable& meta_;
    // Pointer to the C++ object
    void* instance_{ nullptr };
    // Can only call const functions
    bool const_{ false };
};

inline constexpr size_t KEY_BOOL = 0;
inline constexpr size_t KEY_INT = 1;
inline constexpr size_t KEY_FLOAT = 2;
inline constexpr size_t KEY_STRING = 3;
inline constexpr size_t KEY_FUNCTION = 4;
inline constexpr size_t KEY_TABLE = 5;
inline constexpr size_t KEY_OBJECT = 6;

inline constexpr size_t INDEX_NULL = 0;
inline constexpr size_t INDEX_BOOL = 1;
inline constexpr size_t INDEX_INT = 2;
inline constexpr size_t INDEX_FLOAT = 3;
inline constexpr size_t INDEX_STRING = 4;
inline constexpr size_t INDEX_FUNCTION = 5;
inline constexpr size_t INDEX_TABLE = 6;
inline constexpr size_t INDEX_OBJECT = 7;

// A variable can be one of these types
using ValueType = std::variant<std::nullptr_t,
    bool,
    Integer,
    Float,
    StringPtr,
    CallablePtr,
    TablePtr,
    ObjectPtr>;

// Special literals
static constexpr std::string_view LITERAL_NULL = "null";
static constexpr std::string_view LITERAL_TRUE = "true";
static constexpr std::string_view LITERAL_FALSE = "false";
static constexpr std::string_view KEYWORD_CONSTRUCTOR = "constructor";

class Value
{
public:
    constexpr Value() noexcept : value_(nullptr) {}
    template<typename T>
    constexpr Value(T value) noexcept : value_(std::move(value)) //NOLINT(hicpp-explicit-conversions)
    {
    }
    Value(const Value&) noexcept  = default;
    Value(Value&&) noexcept  = default;

    ~Value() = default;

    Value& operator =(const Value&) = default;
    Value& operator =(Value&&) noexcept = default;

    [[nodiscard]] Value Copy() const;

    // Is<T>() should be used in combination with As<T>()
    template<typename T>
    [[nodiscard]] constexpr bool Is() const
    {
        return std::holds_alternative<T>(value_);
    }
    // As<T>() does not cast, it returns the value as reference
    template<typename T>
    [[nodiscard]] constexpr T& As()
    {
        return std::get<T>(value_);
    }
    template<typename T>
    [[nodiscard]] constexpr const T& As() const
    {
        return std::get<T>(value_);
    }

    // IsConvertible<T>() should be used with ConvertTo<T>()
    template<typename T>
    [[nodiscard]] constexpr bool IsConvertible() const
    {
        if constexpr (std::is_same_v<bool, T>)
            return value_.index() == INDEX_INT || value_.index() == INDEX_BOOL ||
                value_.index() == INDEX_OBJECT || value_.index() == INDEX_NULL;
        else if constexpr (std::is_integral_v<T>)
            return value_.index() == INDEX_FLOAT || value_.index() == INDEX_INT ||
                value_.index() == INDEX_BOOL;
        else if constexpr (std::is_floating_point_v<T>)
            return value_.index() == INDEX_FLOAT || value_.index() == INDEX_INT;

        else if constexpr (std::is_same_v<std::string, T>)
            return value_.index() == INDEX_STRING;
        else if constexpr (std::is_same_v<std::nullptr_t, T>)
            return value_.index() == INDEX_NULL;

        else if constexpr (std::is_same_v<StringPtr, T>)
            return value_.index() == INDEX_STRING;
        else if constexpr (std::is_same_v<TablePtr, T>)
            return value_.index() == INDEX_TABLE;
        else if constexpr (std::is_same_v<ObjectPtr, T>)
            return value_.index() == INDEX_OBJECT;
        else if constexpr (std::is_same_v<CallablePtr, T>)
            return value_.index() == INDEX_FUNCTION;
        else
            return false;
    }
    // ConvertTo<T>() casts to the type
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires (std::is_integral<T>::value && !std::is_same<bool, T>::value)
    {
        return static_cast<T>(ToInt());
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_floating_point<T>::value
    {
        return static_cast<T>(ToFloat());
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<bool, T>::value
    {
        return ToBool();
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<std::nullptr_t, T>::value
    {
        return nullptr;
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<std::string, T>::value
    {
        return ToString();
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<TablePtr, T>::value
    {
        return As<TablePtr>();
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<ObjectPtr, T>::value
    {
        return As<ObjectPtr>();
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<CallablePtr, T>::value
    {
        return As<CallablePtr>();
    }
    template<typename T>
    [[nodiscard]] T ConvertTo() const requires std::is_same<StringPtr, T>::value
    {
        return As<StringPtr>();
    }
    [[nodiscard]] bool ToBool() const;
    [[nodiscard]] std::string ToString() const;
    [[nodiscard]] Integer ToInt() const;
    [[nodiscard]] Unsigned ToUint() const;
    [[nodiscard]] Float ToFloat() const;
    [[nodiscard]] TablePtr ToTable() const;
    [[nodiscard]] KeyType ToKey(Context& ctx) const;
    [[nodiscard]] ContainerPtr ToContainer() const;
    [[nodiscard]] std::string Stringify(Context& ctx) const;
    // Value must match. This converts rhs' value to match our type when needed.
    [[nodiscard]] bool Equals(const Value& rhs) const;
    [[nodiscard]] bool Equals(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool Unequals(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Integer ThreeWayCmp(Context& ctx, const Value& rhs) const;
    // Type and value must match
    [[nodiscard]] bool StrictEquals(const Value& rhs) const;
    [[nodiscard]] bool StrictEquals(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool StrictUnequals(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool Gt(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool GtEquals(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool Lt(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool LtEquals(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool In(Context& ctx, const Value& rhs) const;
    [[nodiscard]] bool InRange(Context& ctx, const Value& first, const Value& last) const;
    Value PreInc(Context& ctx);
    Value PreDec(Context& ctx);
    Value PostInc(Context& ctx);
    Value PostDec(Context& ctx);
    Value AssignAdd(Context& ctx, const Value& rhs);
    Value AssignAnd(Context& ctx, const Value& rhs);
    Value AssignDiv(Context& ctx, const Value& rhs);
    Value AssignIDiv(Context& ctx, const Value& rhs);
    Value AssignMod(Context& ctx, const Value& rhs);
    Value AssignMul(Context& ctx, const Value& rhs);
    Value AssignOr(Context& ctx, const Value& rhs);
    Value AssignShl(Context& ctx, const Value& rhs);
    Value AssignShr(Context& ctx, const Value& rhs);
    Value AssignSub(Context& ctx, const Value& rhs);
    Value AssignXor(Context& ctx, const Value& rhs);
    Value AssignPow(Context& ctx, const Value& rhs);
    [[nodiscard]] Value Add(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Sub(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Mul(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Div(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value IDiv(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Range(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Mod(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Pow(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Shr(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value Shl(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value BitwiseAnd(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value BitwiseOr(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value BitwiseXor(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value BitwiseNot(Context& ctx) const;
    [[nodiscard]] Value UnaryPlus(Context& ctx) const;
    [[nodiscard]] Value Negate(Context& ctx) const;
    [[nodiscard]] Value LogicalNot(Context& ctx) const;
    [[nodiscard]] Value LogicalAnd(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value LogicalOr(Context& ctx, const Value& rhs) const;
    [[nodiscard]] Value CountOf(Context& ctx) const;
    [[nodiscard]] Value NameOf(Context& ctx) const;
    void Assign(Context& ctx, const Value& rhs);
    ValuePtr Call(Context& ctx, const FunctionArguments& args) const;
    [[nodiscard]] ValuePtr GetSubscript(Context& ctx, const KeyType& key, bool computed) const;
    [[nodiscard]] ValuePtr GetSubscriptRange(Context& ctx, const std::optional<KeyType>& first, const std::optional<KeyType>& last) const;
    void SetSubscript(Context& ctx, const KeyType& key, const Value& value, bool computed);
    [[nodiscard]] bool HasSubscript(const KeyType& key, bool computed) const;
    [[nodiscard]] constexpr bool IsCompatible(const Value& rhs) const
    {
        if (value_.index() == rhs.value_.index())
            return true;
        if (value_.index() == INDEX_NULL)
            return true;
        // int to float
        if (value_.index() == INDEX_FLOAT && rhs.value_.index() == INDEX_INT)
            return true;
        if (value_.index() == INDEX_INT && rhs.value_.index() == INDEX_FLOAT)
            return true;
        if (value_.index() == INDEX_OBJECT && rhs.value_.index() == INDEX_NULL)
            return true;

        return false;
    }
    [[nodiscard]] constexpr bool IsNull() const { return Is<std::nullptr_t>(); }
    [[nodiscard]] constexpr bool IsBool() const { return Is<bool>(); }
    [[nodiscard]] constexpr bool IsInt() const { return Is<Integer>(); }
    [[nodiscard]] constexpr bool IsFloat() const { return Is<Float>(); }
    [[nodiscard]] constexpr bool IsString() const { return Is<StringPtr>(); }
    [[nodiscard]] constexpr bool IsCallable() const { return Is<CallablePtr>(); }
    [[nodiscard]] constexpr bool IsTable() const { return Is<TablePtr>(); }
    [[nodiscard]] constexpr bool IsObject() const { return Is<ObjectPtr>(); }
    [[nodiscard]] constexpr size_t GetType() const { return value_.index(); }
    void Dump(std::ostream& os);
    [[nodiscard]] constexpr std::string_view GetTypeName() const
    {
        using namespace std::literals::string_view_literals;
        switch (value_.index())
        {
        case INDEX_NULL:
            return "null"sv;
        case INDEX_BOOL:
            return "bool"sv;
        case INDEX_INT:
            return "int"sv;
        case INDEX_FLOAT:
            return "float"sv;
        case INDEX_STRING:
            return "string"sv;
        case INDEX_FUNCTION:
            return "function"sv;
        case INDEX_TABLE:
            return "table"sv;
        case INDEX_OBJECT:
            return "object"sv;
        default:
            return "Unknown"sv;
        }
    }
    [[nodiscard]] bool IsConst() const { return const_; }
    void SetConst(bool value, bool recursive = true);
    void SetOwner(const ContainerPtr& value) { owner_ = value; }
    [[nodiscard]] ContainerPtr GetOwner() const { return owner_.lock(); }
    std::optional<std::string> GetName(const Context& ctx) const;
    template<typename... Args>
    ValuePtr operator()(Context& ctx, Args&&... args)
    {
        FunctionArguments a;
        VariadicVectorEmplace(a, std::forward<Args>(args)...);
        return Call(ctx, a);
    }
    void SetTemplate(bool value) { template_ = value; }
    [[nodiscard]] bool IsTemplate() const { return template_; }
    friend std::ostream& operator << (std::ostream& os, const Value& value)
    {
        if (value.IsTable())
        {
            for (const auto& item : *value.As<TablePtr>())
                os << *item.second << " ";
        }
        else
            os << value.ToString();
        return os;
    }

private:
    static ValuePtr FromContainer(const ContainerPtr& container);
    bool CheckConst(Context& ctx) const;
    ValuePtr CallNoThis(Context&, const FunctionArguments&) const;
    ValuePtr CallWithThis(Context&, const Value&, const FunctionArguments&) const;
    [[nodiscard]] ValuePtr GetOperator(const std::string& name) const;
    ValueType value_;
    // When this is a table member, owner_ is the table
    std::weak_ptr<Container> owner_;
    bool const_{ false };
    // True when this is a template literal
    bool template_{ false };
};

inline ValuePtr MakeNull()
{
    return std::make_shared<Value>();
}

inline TablePtr MakeTable()
{
    return std::make_shared<Table>();
}
TablePtr MakeTable(const Error& error);

template<typename... Args>
inline StringPtr MakeString(Args... value)
{
    return std::make_shared<std::string>(std::forward<Args>(value)...);
}
template<>
inline StringPtr MakeString(const char* value)
{
    if (!value)
        return std::make_shared<std::string>("");
    return std::make_shared<std::string>(value);
}

template<typename T>
inline ObjectPtr MakeObject(const MetaTable& meta, T* inst, bool _const = false)
{
    return std::make_shared<Object>(TypeName<T>::Get(), meta, inst, _const);
}
inline ObjectPtr MakeObject(const MetaTable& meta, std::string_view typeName, void* inst, bool _const = false)
{
    return std::make_shared<Object>(typeName, meta, inst, _const);
}

template<typename T>
inline ValuePtr MakeValue(T value)
{
    return std::make_shared<Value>(std::forward<T>(value));
}
template<typename T>
inline ValuePtr MakeConstValue(T value)
{
    auto result = MakeValue(std::forward<T>(value));
    result->SetConst(true);
    return result;
}
template<>
inline ValuePtr MakeValue(std::string value)
{
    return MakeValue(MakeString(std::forward<std::string>(value)));
}
template<>
inline ValuePtr MakeConstValue(std::string value)
{
    return MakeConstValue(MakeString(std::forward<std::string>(value)));
}

Value KeyToValue(const KeyType& value);

template<>
inline ValuePtr MakeValue(KeyType value)  //NOLINT(performance-unnecessary-value-param)
{
    return std::make_shared<Value>(KeyToValue(value));
}

template<typename T>
inline void Table::Add(const KeyType& key, T value)
{
    Add(key, MakeValue<T>(std::move(value)));
}
template<typename T>
inline void Table::Add(T value)
{
    Add(MakeValue<T>(std::move(value)));
}

}
