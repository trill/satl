/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <deque>
#include <map>
#include <memory>
#include <optional>
#include <stack>
#include <string>
#include <vector>
#include <format>
#include <type_traits>
#include "error.h"
#include "gc.h"
#include "metatable.h"
#include "native_function.h"
#include "node.h"
#include "non_copyable.h"
#include "string_hash.h"
#include "typename.h"
#include "value.h"
#include "variadic_vector.h"
#include "type_hint.h"
#include "ast.h"

namespace sa::tl {

struct Variable
{
    int scope;       // Scope depth
    int boundary;    // Function call boundary¹
    size_t name;
    ValuePtr value;
    std::string nameString;
};
// ¹ Since we do not have a real stack, but variables -- be it globals,
//  locals or function arguments -- are all stored in a single array,
//  potentially accessible from everywhere until they are removed by
//  popping a scope. A boundary is to separate variables of different
//  functions. So it is similar to a stack frame. Variables with the
//  same boundary are in the same stack frame.

class CallGuard;
class EvalGuard;
class Module;

void DefaultRuntimeErrorHandler(const Context& ctx, const Error& error);

template<typename T, bool WithLibraries, typename Callback>
struct RecursiveValueIterator
{
    static void IterateTable(const TablePtr& table, const Callback& callback)
    {
        for (const auto& value : *table)
            IterateValue(value.second, callback);
    }
    static void IterateValue(const ValuePtr& value, const Callback& callback)
    {
        if constexpr (!WithLibraries)
        {
            // Skip modules
            if (value->Is<TablePtr>() && value->As<TablePtr>()->IsModule())
                return;
        }
        if constexpr (!WithLibraries && std::is_same_v<NodePtr, T>)
        {
            // Skip native functions
            if (value->Is<CallablePtr>() && (Is<NativeFunction>(*value->As<CallablePtr>())
                    || Is<MemberFunction>(*value->As<CallablePtr>())))
                return;
        }

        if (value->Is<T>())
            callback(value->As<T>());
        else if (value->Is<TablePtr>())
            IterateTable(value->As<TablePtr>(), callback);
    }
};

// Scope 0 is library scope.
// Script scopes start with 1.
// -> Script variables do not conflict with library values.
inline constexpr int SCOPE_CURRENT = -1;
inline constexpr int SCOPE_ANY = -1;
inline constexpr int SCOPE_LIBRARY = 0;
inline constexpr int SCOPE_SCRIPT = 1;

using MetaTables = std::map<size_t, MetatablePtr>;

class Context
{
    SATL_NON_COPYABLE(Context);
    SATL_NON_MOVEABLE(Context);
    friend class CallGuard;
    friend class EvalGuard;
    friend class TryBlockGuard;
    friend class ScopePusher;

public:
    Context();
    // When MetaTables are stored elsewhere.
    explicit Context(MetaTables& metatables);
    ~Context();
    void AddValue(std::string_view name, ValuePtr value, int scope = SCOPE_CURRENT);
    void StashValue(ValuePtr value, size_t name = 0);
    bool HasStashedValue(const ValuePtr& value, size_t name = 0) const;
    void SaveCaptures(const Function* func, const ValuePtr& funcVal);
    void UseCaptures(const Function* func);
    void AddDefer(const Defer* defer);
    // Get Variable or function
    ValuePtr operator[](std::string_view name) const { return GetValue(name); }
    void SetValue(std::string_view name, ValuePtr value, int scope = SCOPE_ANY);
    [[nodiscard]] ValuePtr GetValue(std::string_view name) const;
    [[nodiscard]] ValuePtr GetValue(std::string_view name, int scope) const;
    [[nodiscard]] bool HasValue(std::string_view name) const;
    bool DeleteValue(std::string_view name);
    // Get script function
    [[nodiscard]] CallablePtr GetFunction(std::string_view name) const;
    // Call script or native function.
    template<typename... Args>
    ValuePtr CallFunction(std::string_view name, Args&&... args)
    {
        FunctionArguments a;
        VariadicVectorEmplace(a, std::forward<Args>(args)...);
        return InternalCallFunction(name, a);
    }
    // Add a variable to the scope depth.
    template<typename T>
    void AddVariable(std::string_view name, T&& value, int scope = SCOPE_CURRENT)
    {
        ValuePtr v = MakeValue<T>(std::move(value));
        AddValue(name, std::move(v), scope);
    }
    template<typename T>
    std::optional<T> GetValue(std::string_view name)
    {
        auto var = GetValue(name);
        if (var->Is<T>())
            return var->As<T>();
        return {};
    }
    std::optional<std::string> GetValueName(const Value& value) const;
    // Add a constant to the scope depth or the current scope.
    template<typename T>
    void AddConst(std::string_view name, T&& value, int scope = SCOPE_CURRENT)
    {
        ValuePtr v = MakeValue<T>(std::move(value));
        v->SetConst(true);
        AddValue(name, std::move(v), scope);
    }
    template<typename Class>
    MetaTable& AddMetatable()
    {
        static constexpr size_t classHash = StringHash(TypeName<Class>::Get());
        if (auto it = metatables_.find(classHash); it != metatables_.end())
            return *it->second;

        auto mt = MakeMetatable<Class>();
        auto& result = *mt;
        metatables_.emplace(classHash, std::move(mt));
        return result;
    }
    template<typename Class, typename Base>
    MetaTable& AddMetatable()
    {
        static_assert(std::is_base_of<Base, Class>::value);

        static constexpr size_t classHash = StringHash(TypeName<Class>::Get());
        if (auto it = metatables_.find(classHash); it != metatables_.end())
            return *it->second;

        static constexpr size_t baseClassHash = StringHash(TypeName<Base>::Get());
        const auto baseIt = metatables_.find(baseClassHash);
        // Register the base class before with AddMetatable()
        SATL_ASSERT(baseIt != metatables_.end());

        auto mt = MakeMetatable<Class>(baseIt->second.get());
        auto& result = *mt;
        metatables_.emplace(classHash, std::move(mt));
        return result;
    }
    template<typename Class>
    [[nodiscard]] MetaTable* GetMetatable() const
    {
        return GetMetatable(TypeName<Class>::Get());
    }
    [[nodiscard]] MetaTable* GetMetatable(std::string_view name) const
    {
        const auto it = metatables_.find(StringHash(name));
        if (it != metatables_.end())
            return it->second.get();
        return nullptr;
    }
    template<typename Class>
    [[nodiscard]] bool HasMetatable() const
    {
        return GetMetatable<Class>() != nullptr;
    }
    template<typename Class>
    ValuePtr SetInstance(std::string_view name, Class& inst, int scope = SCOPE_CURRENT, bool _const = false) requires (!std::is_const<Class>::value)
    {
        static constexpr size_t classHash = StringHash(TypeName<Class>::Get());
        const auto mtIt = metatables_.find(classHash);
        // Register the class before with AddMetatable()
        SATL_ASSERT(mtIt != metatables_.end());
        return SetInstance(*mtIt->second, name, inst, scope, _const);
    }
    template<typename Class>
    ValuePtr SetInstance(const MetaTable& meta, std::string_view name, Class& inst, int scope = SCOPE_CURRENT, bool _const = false) requires (!std::is_const<Class>::value)
    {
        SATL_ASSERT(meta.GetClassName() == TypeName<Class>::Get());
        ValuePtr v = MakeValue<ObjectPtr>(MakeObject(meta, &inst, _const));
        AddValue(name, v, scope);
        return v;
    }
    template<typename Class>
    ValuePtr SetInstance(std::string_view name, const Class& inst, int scope = SCOPE_CURRENT)
    {
        static constexpr size_t classHash = StringHash(TypeName<Class>::Get());
        const auto mtIt = metatables_.find(classHash);
        // Register the class before with AddMetatable()
        SATL_ASSERT(mtIt != metatables_.end());
        // Const objects are always const
        return SetInstance(*mtIt->second, name, inst, scope, true);
    }
    template<typename Class>
    ValuePtr SetInstance(const MetaTable& meta, std::string_view name, const Class& inst, int scope = SCOPE_CURRENT)
    {
        SATL_ASSERT(meta.GetClassName() == TypeName<Class>::Get());
        ValuePtr v = MakeValue<ObjectPtr>(MakeObject(meta, &const_cast<Class&>(inst), true));
        AddValue(name, v, scope);
        return v;
    }
    [[nodiscard]] bool HasErrors() const { return !errors_.empty(); }
    // Add a native function
    void AddFunction(const std::string& name, std::function<NativeFunctionSignature>&& func, int scope = SCOPE_CURRENT, bool safe = true);
    template<typename R, typename... Args>
    void AddFunction(const std::string& name, auto&& func, int scope = SCOPE_CURRENT, bool safe = true)
    {
        AddConst<CallablePtr>(name, std::make_shared<NativeFunctionGeneric<R, Args...>>(name, std::move(func), safe), scope);
    }
    template<typename T, bool WithLibraries, typename Callback>
    void ForEach(Callback callback) noexcept
    {
        using Iterator = RecursiveValueIterator<T, WithLibraries, Callback>;
        for (ssize_t i = variables_.size() - 1; i >= 0; --i)
        {
            const auto& variable = variables_[i];
            if constexpr (!WithLibraries)
            {
                if (variable.scope <= SCOPE_LIBRARY)
                    break;
            }
            Iterator::IterateValue(variable.value, callback);
        }
    }
    template<typename T, typename Callback>
    void ForEachCapture(Callback callback) noexcept
    {
        using Iterator = RecursiveValueIterator<T, false, Callback>;
        for (const auto& func : captures_)
        {
            for (const auto& value : func.second)
                Iterator::IterateValue(value.value, callback);
        }
    }
    template<typename Callback>
    void ForEachVariable(Callback callback, bool withLibraries = true) const noexcept
    {
        for (const auto& var : variables_)
        {
            if (!withLibraries)
            {
                if (var.value->Is<TablePtr>() && var.value->As<TablePtr>()->IsModule())
                    continue;
            }
            if (!callback(var))
                break;
        }
    }
    // Find all instances of objects which point to the same object and reset it to avoid use after free.
    // A good place to call it is the desctructor.
    void ResetObject(void* obj) noexcept;
    GC& GetGC() { return gc_; }

    // Run a top level node. Basically node->Evaluate(*this) but also calls remainig derfers.
    ValuePtr Run(const Node& node);

    // Loads an extension library
    bool LoadLibrary(const std::string& name);
    void RuntimeError(int code, std::string message);
    void TypeMismatch(const Value& lhs, const Value& rhs);
    void TypeMismatch(TypeHint lhs, const Value& rhs);
    // All runtime errors
    [[nodiscard]] const std::vector<Error>& GetErrors() const { return errors_; }
    // Return the last error with the code and remove all errors with the same code.
    // If code is 0 it returns the last error and clears all errors.
    Error TakeErrors(int code);
    void ResetErrors();
    [[nodiscard]] const std::deque<const Callable*>& GetCallstack() const { return callstack_; }
    // Source location that is currently executed
    [[nodiscard]] const Location& GetLocation() const { return currentLoc_; }
    void Unwind() { ++unwinding_; }
    [[nodiscard]] bool IsUnwiding() const { return unwinding_; };
    [[nodiscard]] int GetScope() const { return currentScope_; }
    [[nodiscard]] int GetBoundary() const { return currentBoundary_; }
    // Get last dlopen(), dlsym() error
    [[nodiscard]] const std::string& GetDlError() const { return dlError_; }
    [[nodiscard]] std::string FindFile(const std::string& name) const;

    bool break_{ false };
    bool fallthrough_{ false };
    bool continue_{ false };
    // Stop execution now
    bool stop_{ false };

    std::function<void(Context& ctx, const Node& node)> onEnter_;
    std::function<void(Context& ctx, const Node& node)> onLeave_;
    // Runime error
    std::function<void(const Context& ctx, const Error& error)> onError_{ DefaultRuntimeErrorHandler };
    bool sandboxed_{ false };
    // Search paths used by the FindFile() function
    std::vector<std::string> searchPaths_;

    void CallEnter(const Callable& node);
    void CallLeave(const Callable& node);
private:
    struct Capture
    {
        std::string name;
        ValuePtr value;
        // Whose capture is this. This way we can determine when a capture is no longer needed, i.e. when this
        // pointer expired, no shared_ptr is keeping it alive.
        std::weak_ptr<Value> func;
    };

    ValuePtr InternalCallFunction(std::string_view name, const FunctionArguments& arguments);
    void PushScope() { ++currentScope_; }
    void PopScope();
    void ExecuteDefers();
    void PushBoundary() { ++currentBoundary_; }
    void PopBoundary() { --currentBoundary_; }
    void EvaluateEnter(const Node& node);
    void EvaluateLeave(const Node& node);
    [[nodiscard]] bool IsCatching(int code) const
    {
        return (std::find_if(catchingCodes_.begin(),
                catchingCodes_.end(),
                [code](int current) { return current == code || current == 0; })
            != catchingCodes_.end());
    }
    [[nodiscard]] const Variable* GetVariable(std::string_view name, int minScope = SCOPE_LIBRARY) const;

    Location currentLoc_{};
    std::vector<Error> errors_;
    std::deque<const Callable*> callstack_;
    MetaTables& metatables_;
    MetaTables internalMetatables_;
    std::vector<Variable> variables_;
    std::map<const Function* const, std::vector<Capture>> captures_;
    std::stack<std::pair<int, const Defer* const>> defers_;
    int currentScope_{ SCOPE_SCRIPT };
    int currentBoundary_{ 0 };
    std::vector<uintptr_t> libraries_;
    std::vector<int> catchingCodes_;
    int unwinding_{ 0 };
    std::string dlError_;
    GC gc_;
};

template<typename T>
inline bool GetTableValue(Context& ctx, const Table& table, const std::string& name, T& result)
{
    if (auto v = table.GetValue(name))
    {
        if (!v->IsConvertible<T>())
        {
            ctx.RuntimeError(Error::TypeMismatch, std::format("Type mismatch. LHS {} is not compatible with RHS {}", TypeName<T>::Get(), v->GetTypeName()));
            return false;
        }
        result = v->ConvertTo<T>();
        return true;
    }
    ctx.RuntimeError(Error::InvalidArgument, std::format("Value must have a \"{}\" member", name));
    return false;
}

template<typename T>
inline bool GetTableOptionalValue(Context& ctx, const Table& table, const std::string& name, T& result)
{
    if (auto v = table.GetValue(name))
    {
        if (!v->IsConvertible<T>())
        {
            ctx.RuntimeError(Error::TypeMismatch, std::format("Type mismatch. LHS {} is not compatible with RHS {}", TypeName<T>::Get(), v->GetTypeName()));
            return false;
        }
        result = v->ConvertTo<T>();
        return true;
    }
    return false;
}

class [[nodiscard]] CallGuard
{
    SATL_NON_COPYABLE(CallGuard);
    SATL_NON_MOVEABLE(CallGuard);
private:
    Context& ctx_;
    const Node& node_;

public:
    CallGuard(Context& ctx, const Node& node) : ctx_(ctx), node_(node) { ctx_.EvaluateEnter(node_); }
    ~CallGuard() { ctx_.EvaluateLeave(node_); }
};

class [[nodiscard]] TryBlockGuard
{
    SATL_NON_COPYABLE(TryBlockGuard);
    SATL_NON_MOVEABLE(TryBlockGuard);
private:
    Context& ctx_;
    size_t count_{ 0 };

public:
    explicit TryBlockGuard(Context& ctx) : ctx_(ctx) {}
    ~TryBlockGuard() { ctx_.catchingCodes_.resize(ctx_.catchingCodes_.size() - count_); }
    void AddCode(int code)
    {
        ctx_.catchingCodes_.push_back(code);
        ++count_;
    }
};

// Restore previous error state
class [[nodiscard]] EvalGuard
{
    SATL_NON_COPYABLE(EvalGuard);
    SATL_NON_MOVEABLE(EvalGuard);
private:
    Context& ctx_;
    size_t count_;
    Location loc_;
    bool stop_;
public:
    explicit EvalGuard(Context& ctx) :
        ctx_(ctx),
        count_(ctx_.GetErrors().size()),
        loc_(ctx.currentLoc_),
        stop_(ctx_.stop_)
    {}
    ~EvalGuard()
    {
        if (ctx_.errors_.size() > count_)
            ctx_.errors_.resize(count_);
        ctx_.currentLoc_ = loc_;
        ctx_.stop_ = stop_;
    }
};

class [[nodiscard]] ScopePusher
{
    SATL_NON_COPYABLE(ScopePusher);
    SATL_NON_MOVEABLE(ScopePusher);
private:
    Context& ctx_;
    bool boundary_;

public:
    explicit ScopePusher(Context& ctx, bool boundary = false) :
        ctx_(ctx),
        boundary_(boundary)
    {
        if (boundary_)
            ctx.PushBoundary();
        ctx_.PushScope();
    }
    ~ScopePusher()
    {
        if (boundary_)
            ctx_.PopBoundary();
        ctx_.PopScope();
    }
};

}
