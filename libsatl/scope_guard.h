/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <utility>
#include "non_copyable.h"

namespace sa::tl {

template <typename Callback>
class [[nodiscard]] ScopeGuard
{
    SATL_NON_COPYABLE(ScopeGuard);
    SATL_NON_MOVEABLE(ScopeGuard);
private:
    Callback callback_;
public:
    explicit ScopeGuard(Callback&& callback) :
        callback_(std::move(callback))
    { }
    ~ScopeGuard()
    {
        callback_();
    }
};

template <typename Callback> ScopeGuard(Callback&&)->ScopeGuard<Callback>;

}
