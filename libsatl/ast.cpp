/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ast.h"
#include <format>
#include "context.h"
#include "parser.h"
#include "template_parser.h"

namespace sa::tl {

using std::literals::string_view_literals::operator""sv;

#define RETURN_ON_ERROR             \
    do {                            \
        if (ctx.stop_) [[unlikely]] \
            return MakeNull();      \
    } while (0)

// Basically a RETURN_ON_ERROR with a CallGuard
#define EVALUATE_ENTER              \
    do {                            \
        if (ctx.stop_) [[unlikely]] \
            return MakeNull();  \
        CallGuard cg(ctx, *this);   \
    } while (0)


// I think statement expressions are supported by Clang as well
#define TRY_EVAL(nd, ctx)                     \
    ({                                        \
        ValuePtr _result = nd->Evaluate(ctx); \
        if (ctx.stop_) [[unlikely]]           \
            return MakeNull();                \
        _result;                              \
    })
#define TRY_TO_KEY(val, ctx)                  \
    ({                                        \
        KeyType _result = val->ToKey(ctx);    \
        if (ctx.stop_) [[unlikely]]           \
            return {};                        \
        _result;                              \
    })

#define TYPE_HINT(ctx, hint, value)                               \
    do {                                                          \
        if (!CheckTypeHint(hint, value)) [[unlikely]]             \
        {                                                         \
            ctx.TypeMismatch(hint, *MakeValue(value));            \
            return MakeNull();                                    \
        }                                                         \
    } while (0)

#define RUNTIME_ERROR(ctx, code, msg) \
    do {                              \
        ctx.RuntimeError(code, msg);  \
        return MakeNull();            \
    } while (0)

inline static bool CheckTypeHint(TypeHint expected, const Value& value)
{
    if (expected == TypeHint::Any)
        return true;
    return value.GetType() == static_cast<size_t>(expected);
}

inline static bool CheckTypeHint(TypeHint expected, const KeyType& value)
{
    switch (expected)
    {
    case TypeHint::Any:
        return true;
    case TypeHint::Bool:
        return std::holds_alternative<bool>(value);
    case TypeHint::Int:
        return std::holds_alternative<Integer>(value);
    case TypeHint::Float:
        return std::holds_alternative<Float>(value);
    case TypeHint::String:
        return std::holds_alternative<std::string>(value);
    case TypeHint::Function:
        return std::holds_alternative<CallablePtr>(value);
    case TypeHint::Table:
        return std::holds_alternative<TablePtr>(value);
    case TypeHint::Object:
        return std::holds_alternative<ObjectPtr>(value);
    }
    SATL_ASSERT_FALSE();
}

ValuePtr Assign::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    auto rhsV = TRY_EVAL(rhs_, ctx);
    if (!rhsV)
        RUNTIME_ERROR(ctx, Error::Code::NullDereference, "Trying to access null value");

    if (Is<VariableDefinition>(*lhs_))
    {
        TYPE_HINT(ctx, To<VariableDefinition>(*lhs_).typeHint_, *rhsV);

        auto lhsV = TRY_EVAL(lhs_, ctx);
        if (!rhsV->IsTable())
            rhsV->SetConst(const_);
        else
            // Tables get their constness from the other table or
            // when this is a const assign:
            // const const_table = { 1, 2 };
            // table2 = const_table; // const
            // non_const_table = { 1, 2 }; // non const
            // const table4 = non_const_table; // const
            rhsV->SetConst(rhsV->IsConst() || const_);
        // We create a new ValuePtr from rhs because otherwise the value would point to the same value as rhs
        ctx.SetValue(lhsV->ToString(), MakeValue(*rhsV), global_ ? 0 : -1);
        return rhsV;
    }
    if (Is<CallExpr>(*lhs_))
    {
        // Can not assign to result of call expression
        RUNTIME_ERROR(ctx, Error::Code::TypeMismatch, "Invalid left-hand side in assignment");
    }
    if (Is<MemberAccess>(*lhs_))
    {
        auto lhsV = To<MemberAccess>(*lhs_).EnsureSubscript(ctx, rhsV);
        if (!lhsV)
            return {};
        lhsV->SetConst(const_);
        return lhsV;
    }
    if (Is<Function>(*rhs_))
    {
        To<Function>(*rhs_).Capture(ctx, rhsV);
    }

    auto lhsV = TRY_EVAL(lhs_, ctx);
    if (!lhsV)
    {
        RUNTIME_ERROR(ctx, Error::Code::TypeMismatch, "Invalid left-hand side in assignment");
    }
    lhsV->Assign(ctx, *rhsV);
    lhsV->SetConst(const_);
    return lhsV;
}

ValuePtr CompoundAssign::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    auto rhsV = TRY_EVAL(rhs_, ctx);

    switch (op_)
    {
    case Op::Add:
        return MakeValue(lhs_->Evaluate(ctx)->AssignAdd(ctx, *rhsV));
    case Op::And:
        return MakeValue(lhs_->Evaluate(ctx)->AssignAnd(ctx, *rhsV));
    case Op::Div:
        return MakeValue(lhs_->Evaluate(ctx)->AssignDiv(ctx, *rhsV));
    case Op::Mod:
        return MakeValue(lhs_->Evaluate(ctx)->AssignMod(ctx, *rhsV));
    case Op::Mul:
        return MakeValue(lhs_->Evaluate(ctx)->AssignMul(ctx, *rhsV));
    case Op::Or:
        return MakeValue(lhs_->Evaluate(ctx)->AssignOr(ctx, *rhsV));
    case Op::Shl:
        return MakeValue(lhs_->Evaluate(ctx)->AssignShl(ctx, *rhsV));
    case Op::Shr:
        return MakeValue(lhs_->Evaluate(ctx)->AssignShr(ctx, *rhsV));
    case Op::Sub:
        return MakeValue(lhs_->Evaluate(ctx)->AssignSub(ctx, *rhsV));
    case Op::Xor:
        return MakeValue(lhs_->Evaluate(ctx)->AssignXor(ctx, *rhsV));
    case Op::Pow:
        return MakeValue(lhs_->Evaluate(ctx)->AssignPow(ctx, *rhsV));
    case Op::IDiv:
        return MakeValue(lhs_->Evaluate(ctx)->AssignIDiv(ctx, *rhsV));
    }

    SATL_ASSERT_FALSE();
}

void TableInitialization::Add(ValuePtr key, NodePtr value, TypeHint typeHint)
{
    items_.push_back({ std::move(key), std::move(value), typeHint });
}

bool TableInitialization::HasKey(const Value& value) const
{
    if (value.IsNull())
        return false;
    for (const auto& item : items_)
    {
        if (item.key && item.key->StrictEquals(value))
            return true;
    }
    return false;
}

ValuePtr TableInitialization::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    auto table = MakeTable();
    for (const auto& item : items_)
    {
        const auto& key = item.key;
        // Keys are optional, if not given the index is the key
        const KeyType keyVal = !!key ? TRY_TO_KEY(key, ctx) : table->NextIndex();
        auto value = TRY_EVAL(item.value, ctx);
        TYPE_HINT(ctx, item.type, *value);
        if (!Is<Range>(*item.value))
        {
            // Table items know their table
            value->SetOwner(table);
            // We need to copy the value
            table->Add(keyVal, MakeValue(*value));
        }
        else
        {
            SATL_ASSERT(value->IsTable());
            // If this is a range
            table->Append(*value->As<TablePtr>());
        }
    }
    return MakeValue(table);
}

ValuePtr Block::Evaluate(Context& ctx) const
{
    // No need for Eval enter/leave
    RETURN_ON_ERROR;

    ValuePtr last;
    ScopePusher sp(ctx);
    for (const auto& child : children_)
    {
        if (ctx.IsUnwiding() || ctx.break_ || ctx.continue_)
            break;
        last = TRY_EVAL(child, ctx);
    }
    return last;
}

ValuePtr Root::Evaluate(Context& ctx) const
{
    // No need for Eval enter/leave
    RETURN_ON_ERROR;

    // Same as Block, but doesn't push a scope
    ValuePtr last;
    for (const auto& child : children_)
    {
        if (ctx.IsUnwiding() || ctx.break_ || ctx.continue_)
            break;
        last = TRY_EVAL(child, ctx);
    }
    return last;
}

ValuePtr Try::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ValuePtr last = MakeNull();
    if (body_)
    {
        TryBlockGuard tg(ctx);
        for (const auto& b : catchBlocks_)
            tg.AddCode(b.first);
        last = body_->Evaluate(ctx);
    }
    bool canContinue = !ctx.HasErrors();
    if (ctx.HasErrors())
    {
        if (!catchBlocks_.empty())
        {
            const auto& e = ctx.GetErrors().back();
            const auto* block = GetCatch(e.code);
            if (block)
            {
                const Error lastError = ctx.TakeErrors(e.code);
                if (!block->ident.empty())
                {
                    auto ex = MakeTable(lastError);
                    auto stack = MakeTable();
                    for (const auto& s : ctx.GetCallstack())
                    {
                        auto entry = MakeTable();
                        entry->Add<Integer>("line", s->loc_.line);
                        entry->Add<Integer>("col", s->loc_.col);
                        entry->Add("file", s->loc_.file);
                        entry->Add("name", s->GetFriendlyName());
                        stack->Add(std::move(entry));
                    }
                    ex->Add("stack", std::move(stack));
                    auto exValue = MakeValue(std::move(ex));
                    exValue->SetConst(true);
                    ctx.AddValue(block->ident, std::move(exValue));
                }
                ctx.stop_ = false;
                last = block->block->Evaluate(ctx);
                // It is possible to throw an error inside catch.
                canContinue = !ctx.stop_;
            }
        }
    }
    if (finally_)
    {
        ctx.stop_ = false;
        last = finally_->Evaluate(ctx);
        if (canContinue)
            // If there is an error in the finally block, that's unfortunate
            canContinue = !ctx.stop_;
    }
    if (!ctx.stop_)
        ctx.stop_ = !canContinue;

    return last;
}

ValuePtr Break::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ctx.break_ = true;
    return MakeNull();
}

ValuePtr Fallthrough::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ctx.fallthrough_ = true;
    return MakeNull();
}

ValuePtr Continue::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ctx.continue_ = true;
    return MakeNull();
}

ValuePtr Do::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ValuePtr last;
    do
    {
        RETURN_ON_ERROR;
        if (ctx.IsUnwiding() || ctx.break_)
            break;
        if (ctx.continue_)
        {
            ctx.continue_ = false;
            continue;
        }
        last = TRY_EVAL(block_, ctx);
    } while (condition_->Evaluate(ctx)->ToBool());
    ctx.break_ = false;
    return last;
}

ValuePtr For::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    // Expressions inside `for` may define variables, need a scope for that
    ScopePusher sp(ctx);
    ValuePtr last;
    if (init_)
        TRY_EVAL(init_, ctx);
    for (;;)
    {
        if (ctx.IsUnwiding() || ctx.break_)
            break;
        if (condition_)
        {
            auto cond = TRY_EVAL(condition_, ctx);
            if (!cond->ToBool())
                break;
        }

        if (block_ && !ctx.continue_)
            last = TRY_EVAL(block_, ctx);
        ctx.continue_ = false;

        if (increment_)
            TRY_EVAL(increment_, ctx);
    }
    ctx.break_ = false;
    return last;
}

ValuePtr ForEach::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ScopeGuard sg([&ctx] { ctx.break_ = false; });

    const ValuePtr key = !!key_ ? TRY_EVAL(key_, ctx) : ValuePtr{};
    TypeHint keyType = TypeHint::Any;
    if (auto* k = To<VariableDefinition>(key_.get()))
        keyType = k->typeHint_;

    ValuePtr value = TRY_EVAL(value_, ctx);
    TypeHint valueType = TypeHint::Any;
    if (auto* v = To<VariableDefinition>(value_.get()))
        valueType = v->typeHint_;

    if (Is<Range>(*container_))
    {
        // Ranges can be optimized
        ValuePtr result;
        Range& range = To<Range>(*container_);
        auto firstV = TRY_EVAL(range.GetLhs(), ctx);
        auto lastV = TRY_EVAL(range.GetRhs(), ctx);
        auto valueV = reverse_ ? lastV->Copy() : firstV->Copy();
        TYPE_HINT(ctx, valueType, valueV);
        Integer index = 0;
        if (key)
            TYPE_HINT(ctx, keyType, Value(index));

        while (valueV.InRange(ctx, *firstV, *lastV))
        {
            if (ctx.IsUnwiding() || ctx.break_)
                break;
            if (ctx.continue_)
            {
                ctx.continue_ = false;
                continue;
            }
            ScopePusher sp(ctx);
            if (key)
                ctx.SetValue(key->ToString(), MakeValue(index));

            ctx.SetValue(value->ToString(), MakeValue(valueV));
            if (block_)
                result = TRY_EVAL(block_, ctx);

            if (reverse_)
                valueV.PreDec(ctx);
            else
                valueV.PreInc(ctx);
            ++index;
        }
        return result;
    }

    // Not a range, so it must be a table or string
    auto container = TRY_EVAL(container_, ctx);

    if (container->IsTable())
    {
        // TODO: I'm sure so much code duplication isn't necessary
        const auto& table = *container->As<TablePtr>();
        ValuePtr result;

        if (!reverse_)
        {
            for (auto it = table.cbegin(); it != table.cend(); ++it)
            {
                if (ctx.IsUnwiding() || ctx.break_)
                    break;
                if (ctx.continue_)
                {
                    ctx.continue_ = false;
                    continue;
                }
                ScopePusher sp(ctx);
                if (key)
                {
                    TYPE_HINT(ctx, keyType, it->first);
                    ctx.SetValue(key->ToString(), MakeValue(it->first));
                }

                TYPE_HINT(ctx, valueType, *it->second);
                ctx.SetValue(value->ToString(), MakeValue(*it->second));
                if (block_)
                    result = TRY_EVAL(block_, ctx);
            }
            return result;
        }

        for (auto it = table.crbegin(); it != table.crend(); ++it)
        {
            if (ctx.IsUnwiding() || ctx.break_)
                break;
            if (ctx.continue_)
            {
                ctx.continue_ = false;
                continue;
            }
            ScopePusher sp(ctx);
            if (key)
            {
                TYPE_HINT(ctx, keyType, it->first);
                ctx.SetValue(key->ToString(), MakeValue(it->first));
            }

            TYPE_HINT(ctx, valueType, *it->second);
            ctx.SetValue(value->ToString(), MakeValue(*it->second));
            if (block_)
                result = TRY_EVAL(block_, ctx);
        }
        return result;
    }

    if (container->IsString())
    {
        const auto& string = *container->As<StringPtr>();
        ValuePtr result;
        if (keyType != TypeHint::Any && keyType != TypeHint::Int)
        {
            ctx.TypeMismatch(keyType, 0);
            return MakeNull();
        }
        if (valueType != TypeHint::Any && valueType != TypeHint::Int && valueType != TypeHint::String)
        {
            ctx.TypeMismatch(keyType, MakeString(""));
            return MakeNull();
        }

        ssize_t index = reverse_ ? string.size() - 1 : 0;
        while (index >= 0 && index < (ssize_t)string.size())
        {
            if (ctx.IsUnwiding() || ctx.break_)
                break;
            if (ctx.continue_)
            {
                ctx.continue_ = false;
                continue;
            }
            auto c = string.at(index);
            ScopePusher sp(ctx);
            if (key)
                ctx.SetValue(key->ToString(), MakeValue<Integer>(index));
            if (valueType == TypeHint::Int)
                ctx.SetValue(value->ToString(), MakeValue<Integer>(c));
            else
                ctx.SetValue(value->ToString(), MakeValue(std::string(1, c)));
            if (block_)
                result = TRY_EVAL(block_, ctx);
            if (reverse_)
                --index;
            else
                ++index;
        }
        return result;
    }

    RUNTIME_ERROR(ctx, Error::Code::TypeMismatch, "Table, string or range expected");
}

ValuePtr Using::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    // Use something that can not be used as identifier
    constexpr size_t USED_STASH_NAME = StringHash(" UsedStashName "sv);

    auto exprV = TRY_EVAL(expr_, ctx);
    if (ctx.HasStashedValue(exprV, USED_STASH_NAME))
        // Already used
        return MakeNull();

    if (ident_)
    {
        // Just an alias
        auto identV = TRY_EVAL(ident_, ctx);
        ctx.AddValue(identV->ToString(), exprV);
        ctx.StashValue(exprV, USED_STASH_NAME);
        return MakeNull();
    }

    if (exprV->IsTable())
    {
        for (const auto& item : *(exprV->As<TablePtr>()))
        {
            if (item.first.index() == KEY_STRING)
                ctx.AddValue(std::get<KEY_STRING>(item.first), item.second);
        }
    }
    else if (auto name = exprV->GetName(ctx); name.has_value())
    {
        ctx.AddValue(*name, exprV);
    }
    else
    {
        RUNTIME_ERROR(ctx, Error::Code::InvalidArgument, "Invalid using expression");
    }
    ctx.StashValue(exprV, USED_STASH_NAME);

    return MakeNull();
}

void Using::SetIdent(NodePtr value)
{
    ident_ = std::move(value);
}

void Using::SetEpxr(NodePtr value)
{
    expr_ = std::move(value);
}

ValuePtr Function::Evaluate(Context& ctx) const
{
    // This just adds the function to the scope.
    EVALUATE_ENTER;

    ValuePtr funcVal = MakeConstValue(std::static_pointer_cast<Callable>(const_cast<Function*>(this)->shared_from_this()));
    if (!name_.empty())
        ctx.SetValue(name_, funcVal);
    Capture(ctx, funcVal);

    return funcVal;
}

void Function::Capture(Context& ctx, const ValuePtr& funcVal) const
{
    ctx.SaveCaptures(this, funcVal);
}

ValuePtr Function::Execute(Context& ctx) const
{
    RETURN_ON_ERROR;

    // To be able to correctly unwind when there is a return statement.
    CALLABLE_CALLGUARD(ctx);
    CallGuard cg(ctx, *this);
    ctx.UseCaptures(this);
    // The call expression executes the function
    if (body_)
    {
        auto result = TRY_EVAL(body_, ctx);
        // A function returns the value of the return statement.
        if (ctx.IsUnwiding())
        {
            TYPE_HINT(ctx, typeHint_, *result);
            return result;
        }
    }
    // If there is no return statement a function returns null.
    // If it has a type hint this may fail.
    auto result = MakeNull();
    TYPE_HINT(ctx, typeHint_, *result);
    return result;
}

ValuePtr Function::operator()(Context& ctx, const FunctionArguments& args) const
{
    if ((int)args.size() != ParamCount())
    {
        RUNTIME_ERROR(ctx, Error::Code::ArgumentsMismatch,
            std::format("Wrong number of arguments for function \"{}\", expected {} but got {}", name_, ParamCount(),  args.size()));
    }
    ScopePusher sp(ctx);
    for (size_t i = 0; i < args.size(); ++i)
        ctx.AddValue(GetParameterName(i), args[i]);
    return Execute(ctx);
}

ValuePtr Function::operator()(Context& ctx, const Value& thisValue, const FunctionArguments& args) const
{
    if ((int)args.size() != ParamCount())
    {
        RUNTIME_ERROR(ctx, Error::Code::ArgumentsMismatch,
            std::format("Wrong number of arguments for function \"{}\", expected {} but got {}", name_, ParamCount(),  args.size()));
    }
    ScopePusher sp(ctx, true);
    for (size_t i = 0; i < args.size(); ++i)
        ctx.AddValue(GetParameterName(i), args[i]);

    ctx.AddValue("this"sv, MakeValue(thisValue));
    return Execute(ctx);
}

const std::string& Function::GetParameterName(size_t index) const
{
    SATL_ASSERT(index < parameters_.size());
    return parameters_[index]->GetName();
}

TypeHint Function::GetParameterType(size_t index) const
{
    SATL_ASSERT(index < parameters_.size());
    return parameters_[index]->typeHint_;
}

ValuePtr If::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    // Expressions inside `if` may define variables, need a scope for that
    ScopePusher sp(ctx);
    auto v = TRY_EVAL(expr_, ctx);
    bool bv = v->ToBool();
    if (bv && trueBrach_)
        return TRY_EVAL(trueBrach_, ctx);
    if (!bv && falseBrach_)
        return TRY_EVAL(falseBrach_, ctx);
    return MakeNull();
}

ValuePtr Return::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    // Returnable statements are responsible to reset it.
    if (expr_)
    {
        auto result = TRY_EVAL(expr_, ctx);
        ctx.Unwind();
        if (Is<Function>(*expr_))
            To<Function>(*expr_).Capture(ctx, result);
        return result;
    }
    ctx.Unwind();
    return MakeNull();
}

ValuePtr Switch::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ValuePtr last;
    const auto eV = TRY_EVAL(expression_, ctx);
    ctx.fallthrough_ = false;
    size_t execBlocks = 0;
    for (const auto& block : blocks_)
    {
        if (ctx.stop_)
            break;

        if (Is<Range>(*block.first))
        {
            const Range& range = To<Range>(*block.first);
            auto firstV = TRY_EVAL(range.GetLhs(), ctx);
            auto lastV = TRY_EVAL(range.GetRhs(), ctx);
            if (eV->InRange(ctx, *firstV, *lastV) || ctx.fallthrough_)
            {
                ++execBlocks;
                ctx.fallthrough_ = false;
                // Empty block is possible
                if (block.second)
                    last = TRY_EVAL(block.second, ctx);
            }
        }
        else
        {
            auto labelValue = TRY_EVAL(block.first, ctx);
            if (labelValue->Equals(ctx, *eV) || ctx.fallthrough_)
            {
                ++execBlocks;
                ctx.fallthrough_ = false;
                // Empty block is possible
                if (block.second)
                    last = TRY_EVAL(block.second, ctx);
            }
        }
    }
    if (!ctx.stop_ && execBlocks == 0 && default_)
        last = TRY_EVAL(default_, ctx);
    ctx.fallthrough_ = false;
    return last;
}

bool Switch::HasLabel(const Value& value) const
{
    for (const auto& block : blocks_)
    {
        if (auto* v = To<ValueExpr>(block.first.get()))
        {
            if (v->GetValue()->Equals(value))
                return true;
        }
    }
    return false;
}

ValuePtr While::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ValuePtr last;
    while (true)
    {
        // Expressions inside `while` may define variables, need a scope for that,
        // that is invalidated each iteration.
        ScopePusher sp(ctx);
        auto cond = TRY_EVAL(condition_, ctx);
        if (!cond->ToBool())
            break;

        if (ctx.IsUnwiding() || ctx.break_)
            break;
        if (ctx.continue_)
        {
            ctx.continue_ = false;
            continue;
        }
        last = TRY_EVAL(block_, ctx);
    };

    ctx.break_ = false;
    return last;
}

ValuePtr Defer::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ctx.AddDefer(this);
    return MakeNull();
}

ValuePtr Defer::Execute(Context& ctx) const
{
    EVALUATE_ENTER;

    // The call expression executes the function
    if (block_)
        return TRY_EVAL(block_, ctx);
    return MakeNull();
}

ValuePtr ValueExpr::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    if (!value_->IsTemplate())
    {
        // Note: This is a literal. If we would just return the pointer, this literal could be modifyable.
        // See "Imutable string literal" test
        if (value_->IsString())
            return MakeValue(value_->ToString());

        return value_;
    }

    SATL_ASSERT(value_->IsString());
    const auto& source = *value_->As<StringPtr>();
    if (source.empty())
        return MakeValue(MakeString(""));

    const auto evaluate = [&ctx](const std::string& source) -> ValuePtr
    {
        Parser parser(source);
        auto root = parser.ParseExpression();
        if (!root)
        {
            // Syntax error(s)
            for (const auto& e : parser.GetErrors())
                ctx.RuntimeError(Error::Code::SyntaxError, e.message);
            return MakeNull();
        }
        return root->Evaluate(ctx);
    };

    templ::Parser parser;
    const auto tokens = parser.Parse(source);
    const auto result = tokens.ToString(
        [&](const templ::Token& token) -> std::string
        {
            if (!ctx.stop_ && token.type == templ::Token::Type::Variable)
                return evaluate(token.value)->Stringify(ctx);
            return "";
        });
    RETURN_ON_ERROR;
    return MakeValue(result);
}

ValuePtr CallExpr::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ValuePtr funcPtr = TRY_EVAL(func_, ctx);

    if (funcPtr->IsCallable())
    {
        const auto* callablePtr = funcPtr->As<CallablePtr>().get();
        if (const Function* func = To<Function>(callablePtr))
        {
            // Script function
            FunctionArguments args;
            args.reserve(arguments_.size());
            for (size_t i = 0; i < arguments_.size(); ++i)
            {
                // Pass by value
                auto argPtr = MakeValue(*arguments_[i]->Evaluate(ctx));
                RETURN_ON_ERROR;
                TYPE_HINT(ctx, func->GetParameterType(i), *argPtr);
                if (!argPtr->IsTable())
                    argPtr->SetConst(false);
                args.push_back(std::move(argPtr));
            }
            return funcPtr->Call(ctx, args);
        }

        if (Is<MemberFunction>(callablePtr))
        {
            FunctionArguments args;
            args.reserve(arguments_.size());
            for (const auto& argument : arguments_)
            {
                auto argV = TRY_EVAL(argument, ctx);
                args.push_back(std::move(argV));
            }
            return funcPtr->Call(ctx, args);
        }

        if (const NativeFunction* func = To<NativeFunction>(callablePtr))
        {
            // Some native free function
            FunctionArguments args;
            args.reserve(arguments_.size());
            for (const auto& argument : arguments_)
            {
                auto argV = TRY_EVAL(argument, ctx);
                args.push_back(std::move(argV));
            }
            return (*func)(ctx, args);
        }
    }

    if (funcPtr->Is<TablePtr>())
    {
        // Calling table() is like a constructor.
        Value tableV = funcPtr->Copy();
        TablePtr& table = tableV.As<TablePtr>();

        const auto opIt = table->find(std::string(KEYWORD_CONSTRUCTOR));
        if (opIt != (*table).end() && opIt->second->IsCallable())
        {
            const auto& funcPtr = opIt->second->As<CallablePtr>();
            if (const Function* func = To<Function>(funcPtr.get()))
            {
                FunctionArguments args;
                args.reserve(arguments_.size());
                for (const auto& argument : arguments_)
                {
                    auto argV = TRY_EVAL(argument, ctx);
                    args.push_back(std::move(argV));
                }

                // Constructor function returns nothing.
                (*func)(ctx, table, args);
            }
        }
        else if (!arguments_.empty())
        {
            // When there is no custom constructor, we expect zero arguments
            RUNTIME_ERROR(ctx, Error::Code::ArgumentsMismatch,
                std::format("Wrong number of arguments for constructor, expected 0 but got {}", arguments_.size()));
        }
        return MakeValue(tableV);
    }

    RUNTIME_ERROR(ctx, Error::Code::TypeMismatch, std::format("\"{}\" is not a function but {}", GetName(), funcPtr->GetTypeName()));
}

ValuePtr VariableExpr::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    if (auto value = ctx.GetValue(name_))
        return value;

    // Maybe a script function
    if (auto func = ctx.GetFunction(name_))
        return MakeValue(func);

    RUNTIME_ERROR(ctx, Error::Code::UndefinedIdentifier, std::format("Undefined identifier \"{}\"", name_));
}

template<bool Conditional>
ValuePtr _MemberAccess<Conditional>::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    ValuePtr o = TRY_EVAL(owner_, ctx);
    if (o->IsNull())
    {
        if constexpr (Conditional)
            return MakeNull();
        else
            RUNTIME_ERROR(ctx, Error::Code::NullDereference, "Accessing a member of a null value");
    }

    if (computed_ && Is<Range>(*value_))
    {
        const Range& range = To<Range>(*value_);
        std::optional<KeyType> first;
        if (range.GetLhs())
        {
            auto lhsV = TRY_EVAL(range.GetLhs(), ctx);
            first = TRY_TO_KEY(lhsV, ctx);
        }

        std::optional<KeyType> last;
        if (range.GetRhs())
        {
            auto rhsV = TRY_EVAL(range.GetRhs(), ctx);
            last = TRY_TO_KEY(rhsV, ctx);
        }
        return o->GetSubscriptRange(ctx, first, last);
    }

    ValuePtr s = TRY_EVAL(value_, ctx);
    KeyType k = TRY_TO_KEY(s, ctx);
    if constexpr (Conditional)
    {
        if (!o->HasSubscript(k, computed_))
            return MakeNull();
    }
    return o->GetSubscript(ctx, k, computed_);
}

template<bool Conditional>
ValuePtr _MemberAccess<Conditional>::EnsureSubscript(Context& ctx, const ValuePtr& value)
{
    ValuePtr o = TRY_EVAL(owner_, ctx);
    if (o->IsNull())
    {
        if constexpr (Conditional)
            return MakeNull();
        else
            RUNTIME_ERROR(ctx, Error::Code::NullDereference, "Accessing a member of a null value");
    }
    ValuePtr s = TRY_EVAL(value_, ctx);
    KeyType k = TRY_TO_KEY(s, ctx);
    if (!o->HasSubscript(k, computed_) || value)
        o->SetSubscript(ctx, k, value ? *value : nullptr, computed_);
    RETURN_ON_ERROR;
    return o->GetSubscript(ctx, k, computed_);
}

template class _MemberAccess<false>;
template class _MemberAccess<true>;

ValuePtr BinaryExpr::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    const auto lhsV = TRY_EVAL(lhs_, ctx);
    switch (op_)
    {
    case Op::Equal:
        return MakeValue(lhsV->Equals(ctx, *rhs_->Evaluate(ctx)));
    case Op::StrictEqual:
        return MakeValue(lhsV->StrictEquals(ctx, *rhs_->Evaluate(ctx)));
    case Op::Unequal:
        return MakeValue(lhsV->Unequals(ctx, *rhs_->Evaluate(ctx)));
    case Op::StrictUnequal:
        return MakeValue(lhsV->StrictUnequals(ctx, *rhs_->Evaluate(ctx)));
    case Op::ThreeWayCmp:
        return MakeValue(lhsV->ThreeWayCmp(ctx, *rhs_->Evaluate(ctx)));
    case Op::In:
    {
        if (Is<Range>(*rhs_))
        {
            // If this is a range we just check if the value is inside the first and last values.
            // No need to create a table.
            const Range& range = To<Range>(*rhs_);
            auto firstV = TRY_EVAL(range.GetLhs(), ctx);
            auto lastV = TRY_EVAL(range.GetRhs(), ctx);
            return MakeValue(lhsV->InRange(ctx, *firstV, *lastV));
        }
        return MakeValue(lhsV->In(ctx, *rhs_->Evaluate(ctx)));
    }
    case Op::Lt:
        return MakeValue(lhsV->Lt(ctx, *rhs_->Evaluate(ctx)));
    case Op::Gt:
        return MakeValue(lhsV->Gt(ctx, *rhs_->Evaluate(ctx)));
    case Op::LtEqual:
        return MakeValue(lhsV->LtEquals(ctx, *rhs_->Evaluate(ctx)));
    case Op::GtEqual:
        return MakeValue(lhsV->GtEquals(ctx, *rhs_->Evaluate(ctx)));
    case Op::LogicalAnd:
        if (!lhsV->ToBool())
            // If lhs is not true the whole expression can never be true (Short-circuit evaluation)
            return MakeValue(*lhsV);
        return MakeValue(lhsV->LogicalAnd(ctx, *rhs_->Evaluate(ctx)));
    case Op::LogicalOr:
        if (lhsV->ToBool())
            // If lhs is true the whole expression can never be false (Short-circuit evaluation)
            return MakeValue(*lhsV);
        return MakeValue(lhsV->LogicalOr(ctx, *rhs_->Evaluate(ctx)));
    case Op::BitwiseAnd:
        return MakeValue(lhsV->BitwiseAnd(ctx, *rhs_->Evaluate(ctx)));
    case Op::BitwiseOr:
        return MakeValue(lhsV->BitwiseOr(ctx, *rhs_->Evaluate(ctx)));
    case Op::BitwiseXor:
        return MakeValue(lhsV->BitwiseXor(ctx, *rhs_->Evaluate(ctx)));
    case Op::Shl:
        return MakeValue(lhsV->Shl(ctx, *rhs_->Evaluate(ctx)));
    case Op::Shr:
        return MakeValue(lhsV->Shr(ctx, *rhs_->Evaluate(ctx)));
    case Op::Add:
        return MakeValue(lhsV->Add(ctx, *rhs_->Evaluate(ctx)));
    case Op::Sub:
        return MakeValue(lhsV->Sub(ctx, *rhs_->Evaluate(ctx)));
    case Op::Mul:
        return MakeValue(lhsV->Mul(ctx, *rhs_->Evaluate(ctx)));
    case Op::Div:
        return MakeValue(lhsV->Div(ctx, *rhs_->Evaluate(ctx)));
    case Op::Mod:
        return MakeValue(lhsV->Mod(ctx, *rhs_->Evaluate(ctx)));
    case Op::Pow:
        return MakeValue(lhsV->Pow(ctx, *rhs_->Evaluate(ctx)));
    case Op::IDiv:
        return MakeValue(lhsV->IDiv(ctx, *rhs_->Evaluate(ctx)));
    }

    SATL_ASSERT_FALSE();
}

ValuePtr UnaryExpr::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    auto value = TRY_EVAL(expr_, ctx);
    switch (op_)
    {
    case Op::UnaryPlus:
        return MakeValue(value->UnaryPlus(ctx));
    case Op::Negate:
        return MakeValue(value->Negate(ctx));
    case Op::BitwiseNot:
        return MakeValue(value->BitwiseNot(ctx));
    case Op::LogicalNot:
        return MakeValue(value->LogicalNot(ctx));
    case Op::CountOf:
        return MakeValue(value->CountOf(ctx));
    case Op::NameOf:
        return MakeValue(value->NameOf(ctx));
    case Op::Stringify:
        return MakeValue(value->Stringify(ctx));
    case Op::PreIncrement:
        return MakeValue(value->PreInc(ctx));
    case Op::PreDecrement:
        return MakeValue(value->PreDec(ctx));
    case Op::PostIncrement:
        return MakeValue(value->PostInc(ctx));
    case Op::PostDecrement:
        return MakeValue(value->PostDec(ctx));
    }

    SATL_ASSERT_FALSE();
}

ValuePtr TernaryExpr::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    auto cond = TRY_EVAL(condition_, ctx);
    if (cond->ToBool())
    {
        if (trueExpr_)
            return TRY_EVAL(trueExpr_, ctx);
        return cond;
    }
    return TRY_EVAL(falseExpr_, ctx);
}

ValuePtr Range::Evaluate(Context& ctx) const
{
    EVALUATE_ENTER;

    // The parser doesn't allow one of these to be null.
    SATL_ASSERT(lhs_);
    SATL_ASSERT(rhs_);

    CallGuard cg(ctx, *this);
    const auto lhsV = TRY_EVAL(lhs_, ctx);
    return MakeValue(lhsV->Range(ctx, *rhs_->Evaluate(ctx)));
}

}
