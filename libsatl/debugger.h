/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "context.h"
#include "node.h"
#include <set>
#include "non_copyable.h"

namespace sa::tl {

class Debugger
{
    SATL_NON_COPYABLE(Debugger);
    SATL_NON_MOVEABLE(Debugger);
public:
    explicit Debugger(Context& ctx);
    ~Debugger();
    void AddSource(const std::string& filename, const std::string& source);
    ValuePtr Run(const NodePtr& ast, bool stopOnFirst = false);
    void Stop();
    void Step();
    void AddBreakpoint(const Location& loc);
    void RemoveBreakpoint(const Location& loc);
    bool ToggleBreakpoint(const Location& loc);
    bool HasBreakpoint(const Location& loc);
    void AddWatch(const std::string& expr);
    void RemoveWatch(const std::string& expr);
    bool ToggleWatch(const std::string& expr);
    bool HasWatch(const std::string& expr);
    // Careful, these may change the state of the program
    ValuePtr Execute(const std::string& src);
    ValuePtr Evaluate(const NodePtr& node);
    ValuePtr Evaluate(const std::string& expr);
    template<typename Callback>
    void EvalWatches(Callback&& callback)
    {
        for (const auto& w : watches_)
        {
            ValuePtr res = Evaluate(w);
            if (!callback(w, res))
                break;
        }
    }
    std::optional<std::string> GetSourceLine(const Location& loc);

    [[nodiscard]] const std::deque<const Node*>& GetNodeStack() const { return nodeStack_; }
    [[nodiscard]] const std::set<Location>& GetBreakpoints() const { return breakpoints_; }
    std::function<void(const Node&)> onBreakpoint_;
    std::function<void(const Node&)> onStep_;
    std::function<void(const Context& ctx, const Error& error)> onError_;
private:
    Context& ctx_;
    std::map<std::string, std::vector<std::string>> sources_;
    std::set<Location> breakpoints_;
    std::vector<std::string> watches_;
    std::deque<const Node*> nodeStack_;
    bool stepping_{ false };

    void EvaluateError(int code, std::string message);
    void OnEnter(const Node& node);
    void OnLeave(const Node& node);
    void OnError(const Context& ctx, const Error& error) const;
};

}
