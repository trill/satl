/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include "error.h"
#include "node.h"
#include "value.h"
#include "convert.h"
#include "scope_guard.h"
#include "exceptions.h"

namespace sa::tl {

class Context;

using NativeFunctionSignature = ValuePtr(Context&, const FunctionArguments&);

class NativeFunction : public Callable
{
public:
    NativeFunction(std::string name, bool safe) : safe_(safe), name_(std::move(name)) {}
    constexpr Class GetClass() const final { return Class::NativeFunction; };
    std::string GetFriendlyName() const final { return name_ + "()"; }
    const std::string& GetName() const final { return name_; }

protected:
    bool safe_;
private:
    std::string name_;
};

class NativeFunctionImpl final : public NativeFunction
{
public:
    NativeFunctionImpl(std::string name, std::function<NativeFunctionSignature>&& func, bool safe) :
        NativeFunction(std::forward<std::string>(name), safe),
        func_(std::move(func))
    { }
    constexpr int ParamCount() const override
    {
        // Variable number of arguments
        return -1;
    }
    ValuePtr operator()(Context& ctx, const FunctionArguments& args) const override
    {
        SATL_ASSERT(func_);
        if (!ExecutionAllowed(ctx, safe_))
            return MakeNull();
        CALLABLE_CALLGUARD(ctx);
        return func_(ctx, std::forward<const FunctionArguments&>(args));
    }
    ValuePtr operator()(Context&, const Value&, const FunctionArguments&) const override
    {
        // Native (free) functions do not have a this pointer
        SATL_ASSERT_FALSE();
    }

private:
    std::function<NativeFunctionSignature> func_;
};

template<typename R, typename... Args>
class NativeFunctionGeneric final : public NativeFunction
{
public:
    using FuncType = R(Args...);
    NativeFunctionGeneric(std::string name, std::function<FuncType>&& func, bool safe) :
        NativeFunction(std::forward<std::string>(name), safe),
        func_(std::move(func))
    { }
    constexpr int ParamCount() const override
    {
        return sizeof...(Args);
    }
    ValuePtr operator()(Context& ctx, const FunctionArguments& args) const override
    {
        if (!ExecutionAllowed(ctx, safe_))
            return MakeNull();
        if (args.size() != sizeof...(Args))
        {
            Callable::RuntimeError(ctx,
                Error::Code::ArgumentsMismatch,
                std::format("Wrong number of arguments, expected {} but got {}", sizeof...(Args), args.size()));
            return MakeNull();
        }
        SATL_ASSERT(func_);

        CALLABLE_CALLGUARD(ctx);
        ValuePtr result = MakeNull();
        [&]<std::size_t... is>(std::index_sequence<is...>)
        {
            try
            {
                if constexpr (std::is_same_v<R, void>)
                    func_(Convert<Args>(args[is])...);
                else
                    result = MakeValue(func_(Convert<Args>(args[is])...));
            }
            catch (const Exception& ex)
            {
                Callable::RuntimeError(ctx, ex.GetCode(), ex.what());
            }
        }
        (std::index_sequence_for<Args...>{});
        return result;
    }
    ValuePtr operator()(Context& ctx, const Value&, const FunctionArguments& args) const override
    {
        // Native functions do not have a this, just drop it.
        return (*this)(ctx, args);
    }

private:
    std::function<FuncType> func_;
};

template<>
constexpr bool Is<NativeFunction>(const Node& obj)
{
    return obj.GetClass() == Node::Class::NativeFunction;
}

}
