/**
 * Copyright (c) 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <string_view>
#include <functional>
#include "value.h"

namespace sa::tl {

// By default unary operators (- and +) are handled by the script parser. Setting `unary` to true makes these functions support unary operators.
std::string ParseNumberString(std::string_view value, const std::function<void(std::string)>& error, bool unary = false);
bool ParseNumberString(const std::function<char()>& consume, const std::function<char(size_t)>& peek, const std::function<void(std::string)>& error, bool unary = false);
Value NumberStringToValue(std::string value, const std::function<void(std::string)>& error, bool unary = false);

}
