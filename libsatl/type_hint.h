/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <satl_assert.h>
#include "value.h"

namespace sa::tl {

enum class TypeHint
{
    Any,
    Bool = INDEX_BOOL,
    Int = INDEX_INT,
    Float = INDEX_FLOAT,
    String = INDEX_STRING,
    Function = INDEX_FUNCTION,
    Table = INDEX_TABLE,
    Object = INDEX_OBJECT
};

inline std::string TypeHintToString(TypeHint expected)
{
    switch (expected)
    {
    case TypeHint::Any:
        return "any";
    case TypeHint::Bool:
        return "bool";
    case TypeHint::Int:
        return "int";
    case TypeHint::Float:
        return "float";
    case TypeHint::String:
        return "string";
    case TypeHint::Table:
        return "table";
    case TypeHint::Function:
        return "function";
    case TypeHint::Object:
        return "object";
    }
    SATL_ASSERT_FALSE();
}

}
