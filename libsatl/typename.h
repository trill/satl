/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string_view>

namespace sa::tl {

namespace details {

constexpr int first_char_pos(const char* str, const char n)
{
    return (*str && *str != n) ? 1 + first_char_pos(str + 1, n) : 0;
}
constexpr int str_length(const char* str)
{
    return *str ? 1 + str_length(str + 1) : 0;
}
constexpr int last_char_pos(const char* str, const char n, int len)
{
    return (len && *(str + len) != n) ? 1 + last_char_pos(str, n, len - 1) : 0;
}
constexpr int is_char(const char* str, const char n)
{
    return *str == n;
}

}

/// Compile time typeid(T).name()
/// constexpr auto res = TypeName<Foo::Bar>::Get();
template<typename T>
struct TypeName
{
    /// Will return "Foo::Bar"
    static consteval auto Get()
    {
        // GCC will set __PRETTY_FUNCTION__ to something like:
        // sa::TypeName<T>::Get() [with T = Foo::Bar]
        // Clang: sa::TypeName<Foo::Bar>::Get() [T = Foo::Bar]
        constexpr const char* name = __PRETTY_FUNCTION__;
        constexpr int begin = details::first_char_pos(name, '=') + 2;
        constexpr int end = details::first_char_pos(name, ']');

        static_assert(end > begin);
        constexpr int length = end - begin;
        constexpr const char* ptr = &name[begin];
        return std::string_view(ptr, length);
    }
};

}
