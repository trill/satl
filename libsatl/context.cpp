/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "context.h"
#include <dlfcn.h>
#ifdef DEBUG_LIB_FILENAMES
#include <link.h>    // struct link_map
#endif
#include "ast.h"
#include "utils.h"

#if !defined(SATL_LIBRARY_PREFIX)
#   define SATL_LIBRARY_PREFIX "libsatl_"
#endif
#if !defined(SATL_LIBRARY_EXT)
#   define SATL_LIBRARY_EXT ".so"
#endif
#if !defined(SATL_LIBRARY_INIT_FUNCTION)
#   define SATL_LIBRARY_INIT_FUNCTION "Init"
#endif

namespace sa::tl {

void DefaultRuntimeErrorHandler(const Context& ctx, const Error& error)
{
    std::cerr << error << std::endl;
    if (!ctx.GetCallstack().empty())
    {
        std::cerr << "Callstack:" << std::endl;
        for (const auto& c : ctx.GetCallstack())
            std::cerr << "  " << *c << std::endl;
    }
}

Context::Context() :
    Context(internalMetatables_)
{}

Context::Context(MetaTables& metatables) :
    metatables_(metatables),
    gc_(*this)
{}

Context::~Context()
{
    // First remove variables
    variables_.clear();
    gc_.Shutdown();
    // Libraries may add Metatables, clear those before unloading them.
    metatables_.clear();

    // Then unload libraries
    for (auto it = libraries_.rbegin(); it != libraries_.rend(); ++it) // NOLINT(modernize-loop-convert)
        dlclose((void*)(*it));                                         // NOLINT(performance-no-int-to-ptr)
}

void Context::AddValue(std::string_view name, ValuePtr value, int scope)
{
    SATL_ASSERT(scope <= currentScope_);
    if (scope != SCOPE_CURRENT)
    {
        auto it = std::find_if(variables_.begin(), variables_.end(),
            [&scope](const Variable& current){ return current.scope > scope; });
        variables_.insert(it, { scope , 0, StringHash(name), std::move(value), std::string(name) });
        return;
    }
    variables_.push_back({ currentScope_, currentBoundary_, StringHash(name), std::move(value), std::string(name) });
}

std::optional<std::string> Context::GetValueName(const Value& value) const
{
    for (ssize_t i = variables_.size() - 1; i >= 0; --i)
    {
        if (variables_[i].value.get() == &value)
            return variables_[i].nameString;
    }
    return {};
}

void Context::StashValue(ValuePtr value, size_t name)
{
    variables_.push_back({ currentScope_, currentBoundary_, name, std::move(value), "" });
}

bool Context::HasStashedValue(const ValuePtr& value, size_t name) const
{

    for (ssize_t i = variables_.size(); i >= 0; --i)
    {
        const auto& current = variables_[i];
        if (current.scope < SCOPE_SCRIPT)
            return false;
        if (current.value == value && current.name == name)
            return true;
    }
    return false;
}

void Context::SaveCaptures(const Function* func, const ValuePtr& funcVal)
{
    captures_.erase(func);
    if (func->GetCaptures().empty())
        return;

    auto& funcCap = captures_[func];
    for (const auto& capture : func->GetCaptures())
        funcCap.push_back({ capture->GetName(), capture->Evaluate(*this), funcVal });
}

void Context::UseCaptures(const Function* func)
{
    const auto& funcCap = captures_.find(func);
    if (funcCap == captures_.end())
        return;
    if (funcCap->second.empty())
        return;

    for (const auto& cap : funcCap->second)
        AddValue(cap.name, cap.value);
}

void Context::AddDefer(const Defer* defer)
{
    defers_.emplace(currentScope_, defer);
}

void Context::PopScope()
{
    if (currentScope_ == 0)
        return;
    ExecuteDefers();
    for (ssize_t i = variables_.size() - 1; i >= 0; --i)
    {
        if (variables_[i].scope != currentScope_)
        {
            const int count = variables_.size() - i - 1;
            variables_.erase(variables_.end() - count, variables_.end());
            break;
        }
    }

    // Now delete all captures that do not have a valid function anymore.
    for (auto& cap : captures_)
    {
        DeleteIf(cap.second, [](const Capture& current) {
            return current.func.expired();
        });
    }
    DeleteIf(captures_, [](const auto& current) {
        return current.second.empty();
    });

    --currentScope_;
}

void Context::ExecuteDefers()
{
    while (!defers_.empty() && defers_.top().first == currentScope_)
    {
        defers_.top().second->Execute(*this);
        defers_.pop();
    }
}

void Context::SetValue(std::string_view name, ValuePtr value, int scope)
{
    if (const auto* var = GetVariable(name, 1))
    {
        if (scope == SCOPE_ANY || var->scope == scope)
        {
            // If this is a function and it is forward declared, it may already exist, then ignore it
            if (var->value->IsCallable() && value->IsCallable() && var->value->As<CallablePtr>() == value->As<CallablePtr>())
                return;

            if (var->value->IsCompatible(*value))
            {
                var->value->Assign(*this, *value);
                return;
            }
            if (var->scope == currentScope_)
            {
                // Found in current scope but not compatible
                TypeMismatch(*var->value, *value);
                return;
            }
        }
    }
    AddValue(name, std::forward<ValuePtr>(value), scope);
}

const Variable* Context::GetVariable(std::string_view name, int minScope) const
{
    size_t nameHash = StringHash(name);
    for (ssize_t i = variables_.size() - 1; i >= 0; --i)
    {
        if (variables_[i].scope < minScope)
            break;
        if (variables_[i].boundary != currentBoundary_)
            break;
        if (variables_[i].name == nameHash)
            return &variables_[i];
    }
    return nullptr;
}

ValuePtr Context::GetValue(std::string_view name) const
{
    size_t nameHash = StringHash(name);
    for (ssize_t i = variables_.size() - 1; i >= 0; --i)
    {
        if (variables_[i].name == nameHash)
            return variables_[i].value;
    }
    return {};
}

ValuePtr Context::GetValue(std::string_view name, int scope) const
{
    size_t nameHash = StringHash(name);
    for (const auto& var : variables_)
    {
        if (var.scope > scope)
            return {};
        if (var.name == nameHash && var.scope == scope)
            return var.value;
    }
    return {};
}

bool Context::HasValue(std::string_view name) const
{
    return !!GetValue(name);
}

CallablePtr Context::GetFunction(std::string_view name) const
{
    size_t nameHash = StringHash(name);
    for (ssize_t i = variables_.size() - 1; i >= 0; --i)
    {
        if (variables_[i].name == nameHash)
        {
            if (variables_[i].value->Is<CallablePtr>())
                return variables_[i].value->As<CallablePtr>();
        }
    }
    return {};
}

void Context::AddFunction(const std::string& name, std::function<NativeFunctionSignature>&& func, int scope, bool safe)
{
    AddConst<CallablePtr>(name, std::make_shared<NativeFunctionImpl>(name, std::move(func), safe), scope);
}

void Context::ResetObject(void* obj) noexcept
{
    if (!obj)
        return;

    ForEach<ObjectPtr, false>(
        [&](const ObjectPtr& current) {
            if (current->GetInstance() == obj)
                current->ResetInstance();
        });
    // There may be captured values not in any scope (i.e. not in variables_) we must reset
    ForEachCapture<ObjectPtr>(
        [&](const ObjectPtr& current) {
            if (current->GetInstance() == obj)
                current->ResetInstance();
        });
}

ValuePtr Context::InternalCallFunction(std::string_view name, const FunctionArguments& arguments)
{
    if (auto func = GetFunction(name))
        return (*func)(*this, arguments);
    RuntimeError(Error::Code::UnknownFunction, std::format("Function \"{}\" not found", name));
    return MakeNull();
}

void Context::RuntimeError(int code, std::string message)
{
    errors_.push_back({ Error::Type::RuntimeError, code, currentLoc_, std::move(message), {} });
    stop_ = true;
    if (!IsCatching(code) && onError_)
        onError_(*this, errors_.back());
}

void Context::TypeMismatch(const Value& lhs, const Value& rhs)
{
    RuntimeError(Error::Code::TypeMismatch,
        std::format("Type mismatch. LHS {} is not compatible with RHS {}", lhs.GetTypeName(), rhs.GetTypeName()));
}

void Context::TypeMismatch(TypeHint lhs, const Value& rhs)
{
    RuntimeError(Error::Code::TypeMismatch,
        std::format("Type mismatch. LHS {} is not compatible with RHS {}", TypeHintToString(lhs), rhs.GetTypeName()));
}

Error Context::TakeErrors(int code)
{
    if (errors_.empty())
        return {};

    if (code == 0)
    {
        Error result = errors_.back();
        errors_.clear();
        return result;
    }
    auto it = std::find_if(errors_.rbegin(), errors_.rend(), [&](const Error& current) -> bool {
        return current.code == code;
    });
    if (it == errors_.rend())
        return {};
    Error result = *it;
    DeleteIf(errors_, [&](const Error& current) -> bool {
        return current.code == code;
    });
    return result;
}

void Context::ResetErrors()
{
    errors_.clear();
    stop_ = false;
}

void Context::EvaluateEnter(const Node& node)
{
    currentLoc_ = node.loc_;
    if (onEnter_)
        onEnter_(*this, node);
}

void Context::EvaluateLeave(const Node& node)
{
    if (onLeave_)
        onLeave_(*this, node);
}

void Context::CallEnter(const Callable& node)
{
    callstack_.push_front(&node);
}

void Context::CallLeave(const Callable&)
{
    if (unwinding_ > 0)
        --unwinding_;
    callstack_.pop_front();
}

ValuePtr Context::Run(const Node& node)
{
    auto result = node.Evaluate(*this);
    if (!stop_ && Is<Root>(node))
        ExecuteDefers();
    return result;
}

bool Context::LoadLibrary(const std::string& name)
{
    dlError_.clear();

    std::string n = name;

loadLib:
    void* handle = dlopen(n.c_str(), RTLD_LAZY);
    if (!handle)
    {
        if (!n.starts_with(SATL_LIBRARY_PREFIX))
        {
            n = SATL_LIBRARY_PREFIX + n;
            goto loadLib;
        }
        if (!name.ends_with(SATL_LIBRARY_EXT))
        {
            n += SATL_LIBRARY_EXT;
            goto loadLib;
        }

        dlError_ = dlerror();
        return false;
    }

    bool (*initFunc)(Context&) = (bool (*)(Context&))dlsym(handle, SATL_LIBRARY_INIT_FUNCTION);
    auto* err = dlerror();
    if (err != nullptr)
    {
        dlclose(handle);
        dlError_ = err;
        return false;
    }

#ifdef DEBUG_LIB_FILENAMES
    {
        struct link_map *map;
        dlinfo(handle, RTLD_DI_LINKMAP, &map);
        if (map)
        {
            char *real = realpath(map->l_name, NULL);
            if (real)
                std::cerr << real << std::endl;
        }
    }
#endif

    // The library itself is responsible to check if it was initialized already,
    // e.g. when loading multiple times into the same context.
    bool result = initFunc(*this);

    libraries_.push_back((uintptr_t)handle);
    return result;
}

std::string Context::FindFile(const std::string& name) const
{
    if (name.empty())
        return {};

    const auto& currentFile = currentLoc_.file;
    auto file = FindRelatedFile(currentFile, name);
    if (FileExists(file))
        return file;

    for (const auto& spath : searchPaths_)
    {
        auto candidate = ConcatPath(spath, name);
        if (FileExists(candidate))
            return candidate;
    }
    return name;
}

}
