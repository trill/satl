/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <deque>
#include <string>
#include "location.h"

namespace sa::tl {

#define ENUMERATE_TOKEN_TYPES            \
    ENUMERATE_TOKEN_TYPE(Amp)            \
    ENUMERATE_TOKEN_TYPE(Assign)         \
    ENUMERATE_TOKEN_TYPE(AssignAdd)      \
    ENUMERATE_TOKEN_TYPE(AssignAnd)      \
    ENUMERATE_TOKEN_TYPE(AssignDiv)      \
    ENUMERATE_TOKEN_TYPE(AssignIDiv)     \
    ENUMERATE_TOKEN_TYPE(AssignMod)      \
    ENUMERATE_TOKEN_TYPE(AssignMul)      \
    ENUMERATE_TOKEN_TYPE(AssignOr)       \
    ENUMERATE_TOKEN_TYPE(AssignPower)    \
    ENUMERATE_TOKEN_TYPE(AssignShl)      \
    ENUMERATE_TOKEN_TYPE(AssignShr)      \
    ENUMERATE_TOKEN_TYPE(AssignSub)      \
    ENUMERATE_TOKEN_TYPE(AssignXor)      \
    ENUMERATE_TOKEN_TYPE(Asterisk)       \
    ENUMERATE_TOKEN_TYPE(At)             \
    ENUMERATE_TOKEN_TYPE(BackSlash)      \
    ENUMERATE_TOKEN_TYPE(BlockComment)   \
    ENUMERATE_TOKEN_TYPE(BracketClose)   \
    ENUMERATE_TOKEN_TYPE(BracketOpen)    \
    ENUMERATE_TOKEN_TYPE(Caret)          \
    ENUMERATE_TOKEN_TYPE(Colon)          \
    ENUMERATE_TOKEN_TYPE(Comma)          \
    ENUMERATE_TOKEN_TYPE(CompEquals)     \
    ENUMERATE_TOKEN_TYPE(CompNot)        \
    ENUMERATE_TOKEN_TYPE(CurlyClose)     \
    ENUMERATE_TOKEN_TYPE(CurlyOpen)      \
    ENUMERATE_TOKEN_TYPE(Decrement)      \
    ENUMERATE_TOKEN_TYPE(Dollar)         \
    ENUMERATE_TOKEN_TYPE(DoubleAsterisk) \
    ENUMERATE_TOKEN_TYPE(DoubleDollar)   \
    ENUMERATE_TOKEN_TYPE(DoubleDot)      \
    ENUMERATE_TOKEN_TYPE(Dot)            \
    ENUMERATE_TOKEN_TYPE(Eof)            \
    ENUMERATE_TOKEN_TYPE(Excl)           \
    ENUMERATE_TOKEN_TYPE(Hash)           \
    ENUMERATE_TOKEN_TYPE(Gt)             \
    ENUMERATE_TOKEN_TYPE(GtEq)           \
    ENUMERATE_TOKEN_TYPE(Ident)          \
    ENUMERATE_TOKEN_TYPE(Increment)      \
    ENUMERATE_TOKEN_TYPE(Invalid)        \
    ENUMERATE_TOKEN_TYPE(LineComment)    \
    ENUMERATE_TOKEN_TYPE(LogicalAnd)     \
    ENUMERATE_TOKEN_TYPE(LogicalOr)      \
    ENUMERATE_TOKEN_TYPE(Lt)             \
    ENUMERATE_TOKEN_TYPE(LtEq)           \
    ENUMERATE_TOKEN_TYPE(Minus)          \
    ENUMERATE_TOKEN_TYPE(Number)         \
    ENUMERATE_TOKEN_TYPE(ParenClose)     \
    ENUMERATE_TOKEN_TYPE(ParenOpen)      \
    ENUMERATE_TOKEN_TYPE(Percent)        \
    ENUMERATE_TOKEN_TYPE(Pipe)           \
    ENUMERATE_TOKEN_TYPE(Plus)           \
    ENUMERATE_TOKEN_TYPE(Question)       \
    ENUMERATE_TOKEN_TYPE(Semicolon)      \
    ENUMERATE_TOKEN_TYPE(Shl)            \
    ENUMERATE_TOKEN_TYPE(Shr)            \
    ENUMERATE_TOKEN_TYPE(Slash)          \
    ENUMERATE_TOKEN_TYPE(Spaceship)      \
    ENUMERATE_TOKEN_TYPE(StrictEq)       \
    ENUMERATE_TOKEN_TYPE(StrictUneq)     \
    ENUMERATE_TOKEN_TYPE(String)         \
    ENUMERATE_TOKEN_TYPE(TemplateString) \
    ENUMERATE_TOKEN_TYPE(Tilde)

struct Token
{
    enum class Type
    {
#define ENUMERATE_TOKEN_TYPE(v) v,
        ENUMERATE_TOKEN_TYPES
#undef ENUMERATE_TOKEN_TYPE
    };

    Type type = Type::Invalid;
    std::string value;
    Location loc;
};

constexpr std::string_view TokenTypeName(Token::Type type)
{
    switch (type)
    {
#define ENUMERATE_TOKEN_TYPE(v) \
    case Token::Type::v:        \
        return #v;
        ENUMERATE_TOKEN_TYPES
#undef ENUMERATE_TOKEN_TYPE
    }
    return "Unknown";
}

using TokenIterator = std::deque<Token>::iterator;

}
