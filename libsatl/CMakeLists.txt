project (libsatl CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(satl ${SOURCES})
set_property(TARGET satl PROPERTY POSITION_INDEPENDENT_CODE ON)

if (SATL_ENABLE_ASSERT)
    target_compile_definitions(satl PUBLIC SATL_ENABLE_ASSERT)
endif(SATL_ENABLE_ASSERT)
target_include_directories(satl PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(satl PRIVATE dl)
target_link_libraries(satl PUBLIC std_lib dbg_lib)
if (BUILD_TESTING)
    # Need std_lib and dbg_lib for tests
    add_subdirectory(tests)
endif()
