/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <satl.h>
#include <catch2/catch.hpp>
#include <object.h>
#include <factory.h>
#include <context.h>
#include <gc.h>
#include "test_run.h"

namespace test {

class TestLibbindTestClass : public sa::tl::bind::Object
{
    TYPED_OBJECT(TestLibbindTestClass);
public:
    TestLibbindTestClass(sa::tl::Context& ctx) :
        sa::tl::bind::Object(ctx)
    {}
    ~TestLibbindTestClass() override
    {
        std::cout << "~TestClass" << std::endl;
    }
    std::string GetString() const { return string_; }
    void SetString(std::string value) { string_ = std::move(value); }
private:
    std::string string_;
};

}

namespace sa::tl::bind {
template<>
sa::tl::MetaTable& Register<test::TestLibbindTestClass>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<test::TestLibbindTestClass, sa::tl::bind::Object>();
    result
        .AddProperty<test::TestLibbindTestClass, std::string>("string", &test::TestLibbindTestClass::GetString, &test::TestLibbindTestClass::SetString);

    return result;
}

}

TEST_CASE("libbind: Get/set/has property", "[libbind][property]")
{
    sa::tl::Context ctx;
    sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    auto& meta = sa::tl::bind::Register<test::TestLibbindTestClass>(ctx);
    ctx.AddFunction<sa::tl::Value>("test_class", [&ctx, &meta] () -> sa::tl::Value
    {
        return sa::tl::bind::CreateInstance<test::TestLibbindTestClass>(ctx, meta, ctx);
    });

    static constexpr const char* Src = R"tl(
test = test_class();
assert(test.has_property("string"));

test.set_property("string", "test_string");
assert(test.get_property("string") == "test_string");
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("libbind: GC/destroy()", "[libbind][gc]")
{
    sa::tl::Context ctx;
    sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    auto& meta = sa::tl::bind::Register<test::TestLibbindTestClass>(ctx);
    ctx.AddFunction<sa::tl::Value>("test_class", [&ctx, &meta] () -> sa::tl::Value
    {
        return sa::tl::bind::CreateInstance<test::TestLibbindTestClass>(ctx, meta, ctx);
    });

    static constexpr const char* Src = R"tl(
test = test_class();
// Removes it from the GC
test.destroy();
// Works in Release but not Debug mode
assert(test == null);
)tl";
    REQUIRE(RunSource(Src, ctx));
    REQUIRE(ctx.GetGC().GetSize() == 0);
}
