/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <satl.h>
#include <catch2/catch.hpp>
#include "test_run.h"

static sa::tl::ValuePtr RunSourceExpr(sa::tl::Context& ctx, const std::string& source)
{
    sa::tl::Parser parser(source);
    auto root = parser.Parse();
    if (!root)
    {
        // Syntax error(s)
        for (const auto& e : parser.GetErrors())
            std::cerr << e << std::endl;
        return {};
    }

    return root->Evaluate(ctx);
}

template<typename T>
static bool TestExpression(const std::string& src, T expected)
{
    sa::tl::Context ctx;
    auto result = RunSourceExpr(ctx, src);
    if (!result)
        return false;
    sa::tl::Value value(expected);
    if (!result->Equals(value))
    {
        std::cerr << result->ToString() << " != " << value.ToString() << std::endl;
        return false;
    }
    return true;
}

TEST_CASE("Simple Expressions", "[expression]")
{
    REQUIRE(TestExpression("result = 1;", 1));
    REQUIRE(TestExpression("result = (1);", 1));
    REQUIRE(TestExpression("result = -1;", -1));
    REQUIRE(TestExpression("result = 2 + -1;", 2 + -1));
    REQUIRE(TestExpression("result = 2 + 1;", 2 + 1));
    REQUIRE(TestExpression("result = (((2+(1))));", 2 + 1));
}

TEST_CASE("Sum Expressions", "[expression]")
{
    REQUIRE(TestExpression("result = 3+2+4;", 3 + 2 + 4));
    REQUIRE(TestExpression("result = (3+2)+4;", 3 + 2 + 4));
    REQUIRE(TestExpression("result = 3+(2+4);", 3 + 2 + 4));
    REQUIRE(TestExpression("result = (3+2+4);", 3 + 2 + 4));

    REQUIRE(TestExpression("result = 3-2-4;", 3 - 2 - 4));
    REQUIRE(TestExpression("result = (3-2)-4;", (3 - 2) - 4));
    REQUIRE(TestExpression("result = 3-(2-4);", 3 - (2 - 4)));
    REQUIRE(TestExpression("result = (3-2-4);", 3 - 2 - 4));
}

TEST_CASE("Product Expressions", "[expression]")
{
    REQUIRE(TestExpression("result = 3*2*4;", 3 * 2 * 4));
    REQUIRE(TestExpression("result = (3*2)*4;", 3 * 2 * 4));
    REQUIRE(TestExpression("result = 3*(2*4);", 3 * 2 * 4));
    REQUIRE(TestExpression("result = (3*2*4);", 3 * 2 * 4));

    REQUIRE(TestExpression("result = (3.0/2.0/4.0);", 3.0f / 2.0f / 4.0f));
    REQUIRE(TestExpression("result = ((3.0/2.0)/4.0);", (3.0f / 2.0f) / 4.0f));
    REQUIRE(TestExpression("result = 3.0/(2.0/4.0);", 3.0f / (2.0f / 4.0f)));

    REQUIRE(TestExpression("result = 3.7\\(4.6\\2.5);", 3 / (4 / 2)));

    REQUIRE(TestExpression("result = (3.0*(2.0/4.0));", 3.0f * (2.0f / 4.0f)));
}

TEST_CASE("Power Expressions", "[expression]")
{
    REQUIRE(TestExpression<sa::tl::Float>("result = (3.0**2.0);", std::pow(3.0, 2.0)));
    REQUIRE(TestExpression<sa::tl::Float>("result = 3.0**2.0 + 1;", std::pow(3.0, 2.0) + 1.0));
    REQUIRE(TestExpression<sa::tl::Float>("result = 3.0**2.0 * 2;", std::pow(3.0, 2.0) * 2.0));
}

TEST_CASE("Sum no parens Expressions", "[expression]")
{
    REQUIRE(TestExpression("result = 2*2+1;", 2 * 2 + 1));
    REQUIRE(TestExpression("result = 1+1+1;", 1 + 1 + 1));
    REQUIRE(TestExpression("result = 1-1-1;", 1 - 1 - 1));
    REQUIRE(TestExpression("result = 1-2-3-4;", 1 - 2 - 3 - 4));
}

TEST_CASE("Mul no parens Expressions", "[expression]")
{
    REQUIRE(TestExpression("result = 1 * 2 * 3;", 1 * 2 * 3));
    REQUIRE(TestExpression("result = 3 / 2 / 1;", 3 / 2 / 1));
}

TEST_CASE("Literal Expressions", "[expression]")
{
    REQUIRE(TestExpression<sa::tl::Integer>("result = 5.0095e+11;", 5.0095e+11));
    REQUIRE(TestExpression<sa::tl::Integer>("result = -4.44434e+12;", -4.44434e+12));
    REQUIRE(TestExpression<sa::tl::Float>("result = -4.44434e-2;", -4.44434e-2));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 0x2a;", 0x2a));
    REQUIRE(TestExpression<sa::tl::Integer>("result = -0x2a;", -0x2a));
    // Octal literals are here 0o...
    REQUIRE(TestExpression<sa::tl::Integer>("result = 0o42;", 042));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 0b101;", 0b101));
}

TEST_CASE("Comparison Expressions", "[expression]")
{
    REQUIRE(TestExpression<bool>("result = 1 == 1.0;", true));
    REQUIRE(TestExpression<bool>("result = 1 <= 1.0;", true));
    REQUIRE(TestExpression<bool>("result = 1 > 1.0;", false));
    REQUIRE(TestExpression<bool>("result = 2 > 1.0;", true));
}

TEST_CASE("Operator precedence", "[operator]")
{
    REQUIRE(TestExpression<bool>("result = true && false || true;", true && false || true));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 1 | 2 & 3;", 1 | 2 & 3));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 4 ^ 1 | 2;", 4 ^ 1 | 2));
}

TEST_CASE("Compund operator", "[operator]")
{
    REQUIRE(TestExpression<sa::tl::Integer>("result = 200; result += 60;", 200 + 60));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 200; result -= 60;", 200 - 60));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 200; result *= 60;", 200 * 60));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 200; result /= 60;", 200 / 60));
    REQUIRE(TestExpression<sa::tl::Integer>("result = 200; result ^= 60;", 200 ^ 60));
}

TEST_CASE("Post increment", "[expression]")
{
    static constexpr const char* Src = R"tl(
i = 0;
assert(i++ == 0);
assert(i == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Pre increment", "[expression]")
{
    static constexpr const char* Src = R"tl(
i = 0;
assert(++i == 1);
assert(i == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Pre/post increment member", "[expression]")
{
    static constexpr const char* Src = R"tl(
t = { i = 1 };
i = ++t.i;
assert(i == 2);
j = t.i++;
assert(j == 2);
assert(t.i == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Range", "[expression]")
{
    static constexpr const char* Src = R"tl(
t = 1..5;
assert(type(t) == TYPE_TABLE);
assert(#t == 5);
for (i = 0; i < #t; ++i)
    assert(t[i] == i + 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Range table", "[expression]")
{
    static constexpr const char* Src = R"tl(
// For table inititaliazion omitting the {} is syntactic sugar, so these expressions are the same.
// In most other cases these would be different.
t1 = 1..5;
t2 = {1..5};
assert(t1 == t2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Complex function expression", "[expression]")
{
    static constexpr const char* Src = R"tl(
x = function(x)
{
    return x;
}(5) == 5;
assert(x);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Complex function expression inline", "[expression]")
{
    static constexpr const char* Src = R"tl(
// Should also work as line expression
assert(function(x) { return x; }(5) == 5);
)tl";
    REQUIRE(Success(Src));
}
