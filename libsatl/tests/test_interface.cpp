/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <satl.h>
#include <ast.h>
#include <catch2/catch.hpp>

TEST_CASE("Get variable", "[interface][variable]")
{
    static constexpr const char* Src = R"tl(
i = 5;
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto i = ctx.GetValue("i");
    REQUIRE(i);
    REQUIRE(i->IsInt());
    REQUIRE(i->ToInt() == 5);
}

TEST_CASE("Set variable", "[interface][variable]")
{
    static constexpr const char* Src = R"tl(
assert(i == 5);
)tl";

    sa::tl::Context ctx;
    ctx.SetValue("i", sa::tl::MakeValue(5));
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto i = ctx.GetValue("i");
    REQUIRE(i);
    REQUIRE(i->IsInt());
    REQUIRE(i->ToInt() == 5);
}

TEST_CASE("AddGlobal(), GetGlobal()", "[interface][variable]")
{
    static constexpr const char* Src = R"tl(
assert(i == 5);
)tl";

    sa::tl::Context ctx;
    ctx.AddVariable("i", 5);
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto i = ctx.GetValue("i");
    REQUIRE(i);
    REQUIRE(i->IsInt());
    REQUIRE(i->ToInt() == 5);

    auto global = ctx.GetValue<sa::tl::Integer>("i");
    REQUIRE(global.has_value());
    REQUIRE(global.value() == 5);
}

TEST_CASE("Call native function", "[interface][function][native]")
{
    static constexpr const char* Src = R"tl(
assert(test() == 42);
)tl";

    sa::tl::Context ctx;
    ctx.AddFunction("test",
        [](sa::tl::Context&, const sa::tl::FunctionArguments&) -> sa::tl::ValuePtr { return sa::tl::MakeValue(42); });
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
}

TEST_CASE("Call native function 2", "[interface][function][native]")
{
    static constexpr const char* Src = R"tl(
assert(test(3, 4) == 7);
)tl";

    sa::tl::Context ctx;
    ctx.AddFunction<int, int, int>("test",
        [](int a, int b) -> int { return a + b; });
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
}

static int test_func(int a, int b) { return a + b; }

TEST_CASE("Call native function 3", "[interface][function][native]")
{
    static constexpr const char* Src = R"tl(
assert(test(3, 4) == 7);
)tl";

    sa::tl::Context ctx;
    ctx.AddFunction<int, int, int>("test", &test_func);
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
}

TEST_CASE("Call native function 4", "[interface][function][native]")
{
    static constexpr const char* Src = R"tl(
assert(test(3, 4) == 7);
)tl";

    sa::tl::Context ctx;
    ctx.AddFunction<int, int, int>("test", std::bind(test_func, std::placeholders::_1, std::placeholders::_2));
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
}

TEST_CASE("Call script function", "[interface][function][script]")
{
    static constexpr const char* Src = R"tl(
function theAnswer()
{
    return 42;
}
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto result = ctx.CallFunction("theAnswer");

    REQUIRE(result->ToInt() == 42);
}

TEST_CASE("Call script function argument", "[interface][function][script]")
{
    static constexpr const char* Src = R"tl(
function theAnswer(a, b)
{
    return a + b;
}
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto result = ctx.CallFunction("theAnswer", 21, 21);
    REQUIRE(result->IsInt());

    REQUIRE(result->ToInt() == 42);
}

TEST_CASE("Call script function multiple return values", "[interface][function][script]")
{
    static constexpr const char* Src = R"tl(
function theAnswer()
{
    return { 42, 21 };
}
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto result = ctx.CallFunction("theAnswer");
    REQUIRE(result->IsTable());
    auto tbl = result->ToTable();

    REQUIRE((*tbl)[0]->ToInt() == 42);
    REQUIRE((*tbl)[1]->ToInt() == 21);
}

TEST_CASE("Context subscript""[interface][function][script]")
{
    static constexpr const char* Src = R"tl(
answer = 42;
function theAnswer()
{
    return { 42, 21 };
}
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto func = ctx["theAnswer"];
    REQUIRE(func->IsCallable());
    auto a = ctx["answer"];
    REQUIRE(a->IsInt());
}

TEST_CASE("Function () operator", "[interface][function][script]")
{
    static constexpr const char* Src = R"tl(
function theAnswer(a, b)
{
    return a + b;
}
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    auto root = parser.Parse();
    REQUIRE(root);

    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
    auto func = ctx["theAnswer"];
    REQUIRE(func->IsCallable());
    auto result = (*func)(ctx, 22, 23);
    REQUIRE(result->IsInt());
    REQUIRE(result->ToInt() == 45);
}
