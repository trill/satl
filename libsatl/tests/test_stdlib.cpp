/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"

TEST_CASE("std function", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(#tab == 4);
assert(type(tab) == TYPE_TABLE);
assert(tab[2] == 3);

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("assert", "[dbg_lib]")
{
    static constexpr const char* Src = R"tl(
assert(true, "not here");


assert(false, "here");

println("not printed");
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::UserDefined));
}

TEST_CASE("assert 2", "[dbg_lib]")
{
    static constexpr const char* Src = R"tl(
x = assert("some value", "not here");

assert(x == "some value");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("warning", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
warning("This is the last warning!");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find table", "[std_lib][table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(std.find(tab, 3) == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find table 2", "[std_lib][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4 };
assert(std.find(tab, 3) == "c");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find table start", "[std_lib][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4 };
assert(std.find(tab, 3, "b") == "c");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find table start 2", "[std_lib][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4 };
assert(std.find(tab, 3, "d") == null);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("delete_if", "[std_lib][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4 };
assert(std.has_key(tab, "b"));
std.delete_if(tab, function(key, value)
{
    return key == "b";
});
assert(#tab == 3);
assert(!std.has_key(tab, "b"));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find string", "[std_lib][string]")
{
    static constexpr const char* Src = R"tl(
str = "Hello friends";
assert(std.find(str, "friends") == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("std_lib to_string()", "[std_lib][string]")
{
    static constexpr const char* Src = R"tl(
str = std.to_string(3);
assert(type(str) == TYPE_STRING);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("sort()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
var = { 3, 2, 1, 0 };
function compare(a, b)
{
    return a < b;
}
std.sort(var, compare);
assert(var[0] == 0);
assert(var[#var-1] == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("to_table()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
tab = std.to_table(str);
assert(#tab == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = std.format("hello %s %d fourtytwo %x %c == A", "friends", -5, 42, 65);
assert(str == "hello friends -5 fourtytwo 2a A == A");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 2", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = std.format("hello");
assert(str == "hello");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 3", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.format("%x", 42) == "2a");
assert(std.format("%#x", 42) == "0x2a");
assert(std.format("%#X", 42) == "0X2A");
assert(std.format("%o", 42) == "52");
assert(std.format("%#o", 42) == "052");
assert(std.format("%g", 42) == "42");
assert(std.format("%f", 42) == "42.000000");
assert(std.format("%e", 42) == "4.200000e+01");
assert(std.format("%p", 42) == "0x000000000000002a");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 4", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.format("%x %p %#X", 42, 42, 42) == "2a 0x000000000000002a 0X2A");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 5", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
x = 3.1415926535;
assert(std.format("%.2f", x) == "3.14");
assert(std.format("%9.2f", x) == "     3.14");
assert(std.format("%09.2f", x) == "000003.14");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 7", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
x = 3.1415926535;
assert(std.format("%+.2f", x) == "+3.14");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 6", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
x = "hello";
assert(std.format("%10s", x) == "     hello");
assert(std.format("%010s", x) == "00000hello");
assert(std.format("%-10s", x) == "hello     ");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 8", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.format("%x", -1) == "ffffffffffffffff");
assert(std.format("%#x", -1) == "0xffffffffffffffff");
assert(std.format("%#X", -1) == "0XFFFFFFFFFFFFFFFF");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("format() 9", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.format("%c%c%c", 65, 66, 67) == "ABC");
)tl";
    REQUIRE(Success(Src));
}
TEST_CASE("format() 10", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.format("%d%%", 10) == "10%");
)tl";
    REQUIRE(Success(Src));
}
TEST_CASE("format() 11", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.format("%p", null) == "0x0000000000000000");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("println()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
println("one", "two", "three");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("substr()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = "a cute little string";
s = std.substr(str, 2, 4);
assert(s == "cute");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("join()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
table = {1, 2, 3, 4};
s = std.join(",", table);
assert(s == "1,2,3,4");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("replace()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = "There is a theory which states that if";
str2 = std.replace(str, "i", "j");
assert(str2 == "There js a theory whjch states that jf");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("split()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = "There is a theory which states that if";
parts = std.split(str, " ", false, false);
assert(#parts == 8);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("has_key()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
table = { a = 1, b = 2, c = 3 };
assert(std.has_key(table, "a"));
assert(!std.has_key(table, "x"));
assert(!std.has_key(table, 0));
assert(!std.has_key(table, false));

array = { 0, 1, 2, 3 };
assert(std.has_key(array, 1));
assert(!std.has_key(array, 9));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("exec()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
const code = "i = 2 + 3;";
assert(std.exec(code) == 5);
)tl";
    REQUIRE(Success(Src));
}
TEST_CASE("eval()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
const code = "2 + 3";
assert(std.eval(code) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("resize()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
assert(#str == 5);
std.resize(str, 10);
assert(#str == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("resize() 2", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
assert(#str == 5);
std.resize(str, 10, "A");
assert(#str == 10);
assert(str == "helloAAAAA");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("serialize/unserialize", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
t = {
    {  1,  2,  3,  4 },
    {  5,  6,  7,  8 },
    {  9, 10, 11, 12 },
    { 13, 14, 15, 16 },
    { true, false, null, 200000.0 },
};
ser = std.serialize(t);
unser = std.unserialize(ser);
assert(t == unser);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("for_each()", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
t = {
    {  1,  2,  3,  4 },
    {  5,  6,  7,  8 },
    {  9, 10, 11, 12 },
    { 13, 14, 15, 16 },
    { { 1, 2 }, { 3, 4 }, { 5, 6 } , 16 },
    { true, false, null, 200000.0 },
};

count = 0;
std.for_each(t, function(k, v) {
    ++count;
    return true;
});

assert(count == 27);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("for_each() 2", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
t = {
    {  1,  2,  3,  4 },
    {  5,  6,  7,  8 },
    {  9, 10, 11, 12 },
    { 13, 14, 15, 16 },
    { { 1, 2 }, { 3, 4 }, { 5, 6 } , 16 },
    { true, false, null, 200000.0 },
};

count = 0;
std.for_each(t, function(k, v) {
    ++count;
    // Returning nothing evaluates to false
});

assert(count == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("call", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    return a + b;
}
assert(std.call(foo, {1, 2}) == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("call member function", "[std_lib]")
{
    // The call() function can call member functions, the first argument must be the object.
    static constexpr const char* Src = R"tl(
std.call(_gc.run, { _gc });
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("pcall success", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    return a + b;
}
res = std.pcall(foo, 1, 2);
assert(res.status);
assert(res.result == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("pcall fail", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    error(100, "Test");
}
res = std.pcall(foo, 1, 2);
assert(!res.status);
assert(res.error.code == 100);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("pcall member success", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.pcall(_gc.run, _gc).status);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("nparams", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    return a + b;
}
bar = {
    baz = function(a, b, c)
    {}
};
assert(std.nparams(foo) == 2);
assert(std.nparams(bar.baz) == 3);
assert(std.nparams(std.unserialize) == 1);
assert(std.nparams(std.delete) == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("type", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
a = 1.0;
assert(type(a) == TYPE_FLOAT);
assert(type(1) == TYPE_INT);
assert(type("foo") == TYPE_STRING);
assert(type({1,2,3}) == TYPE_TABLE);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("sort", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
table = {0,5,3,5,10,4};
std.sort(table, function(a, b) {
    return a < b;
});
assert(table == {0,3,4,5,5,10});
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("swap", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
a = 1;
b = 2;
std.swap(a, b);
assert(a == 2);
assert(b == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("contains string", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
a = "hello there";
assert(std.contains(a, "hell"));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("contains table", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
a = { "hell", "o", "there" };
assert(std.contains(a, "hell"));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("contains table 2", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
a = { {"hell", "o"}, "there" };
assert(std.contains(a, {"hell","o"}));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("parse_number", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
assert(std.parse_number("3.14") == 3.14);
assert(std.parse_number("0x2a") == 42);
assert(std.parse_number("0b101") == 5);
assert(std.parse_number("0o606") == 390);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find_if table", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
tbl = {
    a = 1,
    b = 2,
    c = 3
};
k = std.find_if(tbl, function(key, value) {
    if (value == 2)
        return true;
    return false;
});
// Prints b
assert(k == "b");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("find_if string", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
subject = "Hello there";
index = std.find_if(subject, function(index, char) {
    return char == "t";
});
// Prints 6
assert(index == 6);
)tl";
    REQUIRE(Success(Src));
}


TEST_CASE("is_array", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
arr = { 1, 2, 3 };
assert(std.is_array(arr));
arr2 = { 0 = 1, 1 = 2, 2 = 3 };
assert(std.is_array(arr2));
narr = { 1 = 1, 2 = 2, 3 = 3 };
assert(!std.is_array(narr));
narr2 = { "a" = 1, "b" = 2, "c" = 3 };
assert(!std.is_array(narr2));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table types", "[std_lib][table]")
{
    static constexpr const char* Src = R"tl(
arr = { 1, 2, 3, 4.9 };
assert((std.table_types(arr) & 1 << TYPE_INT) != 0);
assert(std.table_types(arr) != 1 << TYPE_INT);
assert((std.table_types(arr) & 1 << TYPE_FLOAT) != 0);
assert((std.table_types(arr) & 1 << TYPE_STRING) == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("utf8.substr", "[std_lib][unicode]")
{
    static constexpr const char* Src = R"tl(
t = "Hallöchen dort";
s = utf8.substr(t, 1, 7);
assert(s == "allöche");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("count_of table", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
t = { 1, 2, 3, 2, 3, 4, 1, 6, 2 };
assert(std.count_of(t, 1) == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("count_of table callback", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
t = { 1, 2, 3, 2, 3, 4, 1, 6, 2 };
assert(std.count_of(t, function(a) { return a == 2; }) == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("unique", "[std_lib]")
{
    static constexpr const char* Src = R"tl(
t = { 1, 2, 3, 2, 3, 4, 1, 6, 2 };
std.unique(t);
assert(#t == 5);
)tl";
    REQUIRE(Success(Src));
}
