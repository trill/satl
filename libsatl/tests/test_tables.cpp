/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"

TEST_CASE("Tables", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(#tab == 4);
assert(tab[2] == 3);

for (i = 0; i < #tab; i = i + 1)
{
    assert(tab[i] == i + 1);
}

tab[2] = 1;
assert(tab[2] == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table init", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { };

for (i = 0; i < 10; ++i)
{
    tab[#tab] = { x = i };
}

for (i = 0; i < #tab; ++i)
{
    assert(tab[i].x == i);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Subscript", "[table]")
{
    static constexpr const char* Src = R"tl(
t = {
    {  1,  2,  3,  4 },
    {  5,  6,  7,  8 },
    {  9, 10, 11, 12 },
    { 13, 14, 15, 16 },
};
assert(t[1][2] == 7);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Const table", "[statement][const][table]")
{
    static constexpr const char* Src = R"tl(
const tab = { 1, 2, 3, 4 };

tab[0] = 2;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::ConstAssign));
}

TEST_CASE("Const table 2", "[statement][const][table]")
{
    static constexpr const char* Src = R"tl(
const i = {
    a = 1,
    b = 2,
    c = {
        a = 2,
        b = 3
    }
};

assert(std.is_const(i));
assert(std.is_const(i.a));
assert(std.is_const(i.c.a));

i1 = i;
assert(std.is_const(i1));
assert(std.is_const(i1.a));
assert(std.is_const(i1.c.a));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Non const table", "[statement][const][table]")
{
    static constexpr const char* Src = R"tl(
i = {
    a = 1,
    b = 2,
    c = {
        a = 2,
        b = 3
    }
};

assert(!std.is_const(i));
assert(!std.is_const(i.a));
assert(!std.is_const(i.c.a));

i1 = i;
assert(!std.is_const(i1));
assert(!std.is_const(i1.a));
assert(!std.is_const(i1.c.a));

const i2 = i;
assert(std.is_const(i2));
assert(std.is_const(i2.a));
assert(std.is_const(i2.c.a));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Sub table", "[table]")
{
    static constexpr const char* Src = R"tl(
const tab = { 1, { 2, 9, 0}, 3, 4 };

t1 = tab[1];
assert(std.is_const(t1));
assert(t1[1] == 9);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Enum", "[table]")
{
    static constexpr const char* Src = R"tl(

const enum = {
    A = 0,
    B = 1,
    C = 2,
};

assert(enum.B == 1);
assert(enum.C == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Delete", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, { 2, 9, 0}, 3, 4 };
assert(#tab == 4);
std.delete(tab, 3);
assert(#tab == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Delete 2", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, { 2, 9, 0}, 3, 4 };
assert(#tab == 4);
tab -= 3;
assert(#tab == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Append", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(#tab == 4);
table2 = { 2, 4, 5 };
tab = tab + table2;
assert(#tab == 7);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Append 2", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(#tab == 4);
tab = tab + 1;
assert(#tab == 5);
assert(tab[4] == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Append 3", "[table]")
{
    static constexpr const char* Src = R"tl(
function func() {};

tab = { 1, 2, 3, 4 };
assert(#tab == 4);
tab = tab + func;
assert(#tab == 5);
assert(type(tab[4]) == TYPE_FUNCTION);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Append 4", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(#tab == 4);
tab += { 5, 6 };
assert(#tab == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Append 5", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
assert(#tab == 4);
tab = tab + { 5, 6 };
assert(#tab == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Swap", "[table]")
{
    static constexpr const char* Src = R"tl(
function swap(array, a, b)
{
    tmp = array[a];
    array[a] = array[b];
    array[b] = tmp;
}
tab = { 1, 2, 3, 4 };

assert(tab[0] == 1);
assert(tab[1] == 2);
swap(tab, 0, 1);
assert(tab[0] == 2);
assert(tab[1] == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table Copy", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3, 4 };
table2 = copy(tab);

table2[1] = 3;
assert(tab[0] == 1);
assert(tab[1] == 2);
assert(tab[2] == 3);
assert(tab[3] == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table not existing key", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { };
assert(tab[2] == null);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::InvalidSubscript));
}

TEST_CASE("Table create element assignment", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { };
tab[2] == 1;
assert(tab[2] == 1);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::InvalidSubscript));
}

TEST_CASE("Table subscript chain", "[table]")
{
    static constexpr const char* Src = R"tl(
t = {};
t.a = {};
t.a.b = 1;
assert(t.a.b == 1);

t.a.b += 5;
assert(t["a"].b == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table subscript chain 2", "[table]")
{
    static constexpr const char* Src = R"tl(
t = {};
t.a = {};
t.a.b = function(a) { return a; };

assert(t["a"].b(6) == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table subscript chain 3", "[table]")
{
    static constexpr const char* Src = R"tl(
t = {};
t.a = {};
t.a.b = function(a) {
    function b(x)
    {
        return x;
    }
    return b(a);
};

assert(t["a"].b(6) == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table subscript chain 4", "[table]")
{
    static constexpr const char* Src = R"tl(
t = {};
t.a = {};
t.a.b = function(a) {
    function b(x)
    {
        return x;
    }
    return b(a);
};

x = t.a.b;
assert(x(6) == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Subscript expression", "[statement]")
{
    static constexpr const char* Src = R"tl(
function func()
{
    return { 1, 2 };
}

x = func()[1];
assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Subscript expression 2", "[statement]")
{
    static constexpr const char* Src = R"tl(
x = { 1, 2, 3 }[1];

assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Subscript expression 3", "[statement]")
{
    static constexpr const char* Src = R"tl(
x = { a = 1, b = 2, c = 3 }.b;

assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table construct", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = {
    a = 1,
    b = 2
};
tab2 = tab();
assert(tab2.b == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table construct 2", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = {
    constructor = function(a, b)
    {
        this.a = a;
        this.b = b;
    },
    a = 1,
    b = 2
};

tab2 = tab(3, 4);
assert(tab2.b == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table construct 3", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = {
    constructor = function(a, b)
    {
        this.a = a;
        this.b = b;
    },
    a = 1,
    b = 2
};
assert(tab(2, 3).b == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table function", "[table]")
{
    static constexpr const char* Src = R"tl(
const module = {
  x = 42,
  getX = function() {
    return this.x;
  }
};

const unboundGetX = module.getX;
// This tries to be a somewhat sane language...
assert(unboundGetX() == 42);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table equality", "[table]")
{
    static constexpr const char* Src = R"tl(
t = {
    {  1,  2,  3,  4 },
    {  5,  6,  7,  8 },
    {  9, 10, 11, 12 },
    { 13, 14, 15, 16 },
    { true, false, null, 200000.0 },
};

t2 = copy(t);

assert(t == t2);
assert(t !== t2);

t3 = t;
assert(t == t3);
// They point to the same table
assert(t === t3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Append table as single element", "[table]")
{
    static constexpr const char* Src = R"tl(
table = { 1, 2, 3 };
table[#table] = { 4, 5, 6 };
assert(table == {1,2,3,{4,5,6}});
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function chaining", "[table]")
{
    static constexpr const char* Src = R"tl(
obj = {
    value = 0,
    add = function(v)
    {
        this.value += v;
        return this;
    },
    sub = function(v)
    {
        this.value -= v;
        return this;
    },
    show = function()
    {
        println(this.value);
    },
    get = function()
    {
        return this.value;
    }
};

x = obj();
assert(x.add(5).sub(2).get() == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table operator overload", "[table]")
{
    static constexpr const char* Src = R"tl(
obj = {
    value = 0,
    constructor = function(v)
    {
        this.value = v;
    },
    operator+ = function(v)
    {
        this.value += v.value;
        return this;
    },
    operator- = function(v)
    {
        this.value -= v.value;
        return this;
    },
    show = function()
    {
        println(this.value);
    },
    get = function()
    {
        return this.value;
    }
};

assert((obj(5) + obj(2)).get() == 7);
assert((obj(5) - obj(2)).get() == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function temporary", "[table]")
{
    static constexpr const char* Src = R"tl(
obj = {
    value = 0,
    add = function(v)
    {
        this.value += v;
        return this;
    },
    sub = function(v)
    {
        this.value -= v;
        return this;
    },
    show = function()
    {
        println(this.value);
    },
    get = function()
    {
        return this.value;
    }
};

// obj() creates a temporary object which needs to be kept alive until the whole chain is evaluated
assert(obj().add(5).sub(2).get() == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table argument reference", "[table]")
{
    static constexpr const char* Src = R"tl(
function inc(v)
{
    ++v[0];
}

val = { 0 };
for (i = 0; i < 10; ++i)
    inc(val);
assert(val[0] == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table <<= operator", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3 };
tab <<= { 4, 5, 6 };
assert(#tab == 4);
assert(tab == { 1, 2, 3, { 4, 5, 6 }});
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table << operator", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1, 2, 3 };
tab2 = tab << { 4, 5, 6 };
assert(#tab2 == 4);
assert(tab2 == { 1, 2, 3, { 4, 5, 6 }});
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice range", "[table]")
{
    static constexpr const char* Src = R"tl(
t = { 1, 2, 3, 4, 5, 6, 7, 8 };
s = t[2..4];
assert(#s == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice range 2", "[table]")
{
    static constexpr const char* Src = R"tl(
s = { 1, 2, 3, 4, 5, 6, 7, 8 }[2..4];
assert(#s == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice range 3", "[table]")
{
    static constexpr const char* Src = R"tl(
s = { 1, 2, 3, 4, 5, 6, 7, 8 }[5..];
assert(#s == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice range 4", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = 1..5;
slice = tab[2..];
assert(slice == tab[2..#tab - 1]);
assert(slice == tab[2..4]);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice range 5", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = 1..5;
assert(tab[..] == tab);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice range error", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = 1..;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Table slice range error 2", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = { 1.. };
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Table init range", "[table]")
{
    static constexpr const char* Src = R"tl(
s = { 1..3, 5 };
assert(#s == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table count", "[table]")
{
    static constexpr const char* Src = R"tl(
assert(#{ 1..3, 5 } == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table slice count", "[table]")
{
    static constexpr const char* Src = R"tl(
assert(#{ 1..3, 5 }[1..3] == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table type hint init", "[table]")
{
    static constexpr const char* Src = R"tl(
adventurer = {
    name: string = "Alice",
    cat: table = {
        name = "Dinah"
    }
};
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table type hint init fail", "[table]")
{
    static constexpr const char* Src = R"tl(
adventurer = {
    name: int = "Alice",
    cat: table = {
        name = "Dinah"
    }
};
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("Table init bool key", "[table]")
{
    static constexpr const char* Src = R"tl(
t = { false = 1, true = 2 };
assert(t[false] == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table init int key bool val", "[table]")
{
    static constexpr const char* Src = R"tl(
t = { 1 = false, 2 = true };
assert(t[1] == false);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table init duplicate key", "[table]")
{
    static constexpr const char* Src = R"tl(
t = { 1 = false, 1 = true };
assert(t[1] == false);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Table init duplicate key 2", "[table]")
{
    static constexpr const char* Src = R"tl(
t = { test = false, test = true };
assert(t[1] == false);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Table init duplicate key 3", "[table]")
{
    static constexpr const char* Src = R"tl(
t = { "test" = false, "test" = true };
assert(t[1] == false);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Table init auto values", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = {
    a = #,
    b = #
};
assert(tab.a == 0);
assert(tab.b == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table init auto values error", "[table]")
{
    static constexpr const char* Src = R"tl(
tab = {
    a = # + 1,
    b = # + 2
};
assert(tab.a == 0);
assert(tab.b == 1);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}
