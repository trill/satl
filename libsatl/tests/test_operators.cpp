/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"

TEST_CASE("Assignment", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 1;
assert(i === 1);
i = +2;
assert(i === 2);
i = -2;
assert(i === -2);
i += 4;
assert(i === 2);
i *= 2;
assert(i === 4);
i /= 2;
assert(i === 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Assignment 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 1 + 1.5;
assert(i === 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Assignment 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 1.5 + 1;
assert(i === 2.5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Bitwise", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2 << 1 == 4);
assert(2 >> 1 == 1);
assert((3 & 2) == 2);
assert((2 | 1) == 3);
assert((2 ^ 1) == 3);
assert((2 | ~1) == -2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Logical", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(!true == false);
assert(true && false == false);
assert(true && !false == true);
assert(true || false == true);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Arithmetic", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2 + 1 == 3);
assert(2 * 2 == 4);
assert(4 / 2 == 2);
assert(2 % 4 == 2);
assert(3 ** 2 == 9);
assert(+3 == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Unary associativity", "[operator]")
{
    static constexpr const char* Src = R"tl(
o = 1;
assert(!o === false);
assert(!o === !o);
assert(~o === ~o);
assert(+o === +o);
assert(-o === -o);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Cast", "[operator]")
{
    static constexpr const char* Src = R"tl(
// int
val = 1;
assert(val == 1);
val = 1.5;
assert(val == 1);

val2 = 1.4;
assert(val2 == 1.4);
val2 = 1;
assert(val2 == 1.0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Cast 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
val2 = 1.4;
assert(val2 == 1.4);
val2 = 1;
assert(val2 == 1.0);
val2 = "string";       // <- Type mismatch
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("Assign", "[operator]")
{
    static constexpr const char* Src = R"tl(
val2 = 1;
val2 = 1.7;
assert(val2 == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Assign 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
val = null;
assert(val == null);
val = 5;
assert(val == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Compare", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(1 == 1);
assert(1.0 != 1.1);
assert(type(1) != type(1.0));       // Type mismatch
assert("hello" == 'hello');
assert("hello" != 'friends');
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Compare 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(1 === 1);
assert(1.0 !== 1.1);
assert(1.0 !== 1);

assert("1" == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Compare Lt", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(!(1 < 1));
assert(1 < 2);
assert("a" < "aa");
assert("a" < "b");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Compare Gt", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(!(1 > 1));
assert(2 > 1);
assert("aa" > "a");
assert("b" > "a");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Increment", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 1;
++i;
assert(i == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Decrement", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 0;
--i;
assert(i == -1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression", "[operator]")
{
    static constexpr const char* Src = R"tl(
const i = true ? "yes" : "no";
assert(i == "yes");
const j = !true ? "yes" : "no";
assert(j == "no");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
str += (true ? " there" : " friends");
assert(str == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
str += true ? " there" : " friends";
assert(str == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression 4", "[operator]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
i = 1;
str += i == 1 ? " there" : " friends";
assert(str == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression 5", "[operator]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
i = 1;
str += (i == 1) ? " there" : " friends";
assert(str == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression short", "[operator]")
{
    static constexpr const char* Src = R"tl(
x = true ?: 1;
assert(x == true);
y = false ?: 1;
assert(y == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ternary expression short 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
times = 0;
function foo()
{
    ++times;
    return true;
}
// Note: foo() is called only once
x = foo() ?: "function returned something false-ish";
assert(x == true);
// Make sure this is really not reevaluated
assert(times == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Short-circuit &&", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 1;
j = 2;

function getI()
{
    return i;
}
function getJ()
{
    assert(false);
    return j;
}

if (getI() == 2 && getJ() == 2)
{
    assert(false);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Short-circuit ||", "[operator]")
{
    static constexpr const char* Src = R"tl(
i = 1;
j = 2;

function getI()
{
    return i;
}
function getJ()
{
    assert(false);
    return j;
}

if (getI() == 1 || getJ() == 2)
{
    assert(true);
}
else
{
    assert(false);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Operator overloading >", "[operator][overload]")
{
    static constexpr const char* Src = R"tl(
point = {
    x = 2,
    y = 3,
    init = function(x, y) {
        result = copy(point);
        result.x = x;
        result.y = y;
        return result;
    },
    operator> = function(b) {
        return (this.x > b.x) && (this.y > b.y);
    }
};

a = point.init(1, 2);
b = point.init(4, 5);

assert(b > a);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Operator overloading +", "[operator][overload]")
{
    static constexpr const char* Src = R"tl(
point = {
    x = 2,
    y = 3,
    init = function(x, y) {
        result = copy(point);
        result.x = x;
        result.y = y;
        return result;
    },
    operator+ = function(b) {
        if (type(b) == TYPE_INT)
            return point.init(this.x + b, this.y + b);
        return point.init(this.x + b.x, this.y + b.y);
    }
};

a = point.init(4, 3);
b = point.init(5, 60);

c = a + b;

assert(c.x == 9);
assert(c.y == 63);

d = a + 3;
assert(d.x == 7);
assert(d.y == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Operator overloading negate", "[operator][overload]")
{
    static constexpr const char* Src = R"tl(
point = {
    x = 2,
    y = 3,
    init = function(x, y) {
        result = copy(point);
        result.x = x;
        result.y = y;
        return result;
    },
    -operator = function() {
        return point.init(-this.x, -this.y);
    }
};

a = point.init(4, 3);
b = -a;
assert(b.x == -4);
assert(b.y == -3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Operator overloading in", "[operator][overload]")
{
    static constexpr const char* Src = R"tl(
size = {
    width = 0,
    height = 0,
    constructor = function(w, h) {
        this.width = w;
        this.height = h;
    },
    operator in = function(s) {
        return this.width < s.width &&
            this.height < s.height;
    }
};

s = size(4, 3);
bfalse = s in size(2, 1);
assert(!bfalse);
btrue = s in size(7, 8);
assert(btrue);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Operator overloading in range", "[operator][overload]")
{
    static constexpr const char* Src = R"tl(
size = {
    width = 0,
    height = 0,
    constructor = function(w, h) {
        this.width = w;
        this.height = h;
    },
    operator in = function(s) {
        if (#s == 2)
        {
            return (this.width > s[0]) && (this.width < s[1]) && this.height > s[0] && this.height < s[1];
        }
        assert(false);
    }
};

s = size(4, 3);
btrue = s in (1..8);
assert(btrue);
bfalse = s in (1..2);
assert(!bfalse);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Operator constructor", "[operator]")
{
    static constexpr const char* Src = R"tl(
point = {
    x = 2,
    y = 3,
    constructor = function(x, y) {
        this.x = x;
        this.y = y;
    },
};

a = point(4, 3);
assert(a.x == 4);
assert(a.y == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member access", "[operator]")
{
    static constexpr const char* Src = R"tl(
array = { 1, 2, 3, 4 };
assert(array[0] == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member access 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
tableOtherKeys = { a = 1, b = 2, c = 3, d = 4 };
assert(tableOtherKeys["a"] == 1);
assert(tableOtherKeys.a == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member access 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert({ 1, 2, 3, 4 }[1] == 2);
assert({ a = 1, b = 2, c = 3, d = 4 }.c == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member access 4", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = {};
t.a = {};
t.a.b = 1;

t.a.b += 5;
assert(t["a"].b == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member access 5", "[operator]")
{
    static constexpr const char* Src = R"tl(
function foo()
{
    return { a = 1, b = 2 };
}
assert(foo().b == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional member access", "[operator]")
{
    static constexpr const char* Src = R"tl(
x = null;
assert(x?.y?.z == null);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional member access 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
x = {};
x.y = null;
assert(x?.y?.z == null);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional member access 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
adventurer = {
    name = 'Alice',
    cat = {
        name = 'Dinah'
    }
};

dogName = adventurer?.dog?.name;
assert(dogName == null);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Computed member access", "[operator]")
{
    static constexpr const char* Src = R"tl(
math = {
    min = function(a, b)
    {
        if (a < b)
            return a;
        return b;
    }
};
x = math["min"](2,4);
assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional computed member access null", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = null;

m = t?[0] ?: 0;
assert(m == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional computed member access 1", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = {1};

m = t?[1] ?: 0;
// There is a t[0] but not a t[1] so its the default value
assert(m == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional computed member access 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = {1, 2};

m = t?[1] ?: 0;
assert(m == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional computed member access chained default", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = {1};

m = t?[0]?[0] ?: 0;
assert(m == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Conditional computed member access chained value", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = {{1}};

m = t?[0]?[0] ?: 0;
assert(m == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member access 6", "[operator]")
{
    static constexpr const char* Src = R"tl(
math = {
    min = function(a, b)
    {
        if (a < b)
            return a;
        return b;
    }
};
x = math.min(2,4);
assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Computed member access 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
ns = {
    function(a, b)
    {
        if (a < b)
            return a;
        return b;
    }
};
x = ns[0](2,4);
assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Computed member access 4", "[operator]")
{
    static constexpr const char* Src = R"tl(
ns = {
    function(a, b)
    {
        if (a < b)
            return a;
        return b;
    }
};
i = 0;
x = ns[i](2,4);
assert(x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Multidimensional member access read", "[operator]")
{
    static constexpr const char* Src = R"tl(
a = {
    { 1 },
    { 2 },
};
assert(a[1,0] == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Multidimensional member access read 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
a = {
    { 1 },
    { 2 },
};
assert(a[1,0] == a[1][0]);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Multidimensional member access write", "[operator]")
{
    static constexpr const char* Src = R"tl(
a = {
    { 1 },
    { 2 },
};
a[1,0] = 5;
assert(a[1,0] == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Multidimensional member access write const assign", "[operator]")
{
    static constexpr const char* Src = R"tl(
const a = {
    { 1 },
    { 2 },
};
a[1,0] = 5;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::ConstAssign));
}

TEST_CASE("Multidimensional conditional member access", "[operator]")
{
    static constexpr const char* Src = R"tl(
a = {
    { 1 },
    { 2 },
};
assert(a?[1,0] == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Multidimensional conditional member access null", "[operator]")
{
    static constexpr const char* Src = R"tl(
a = {
    { 1 },
    { 2 },
};
assert(a?[4,0] == null);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Spaceship number", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(1 <=> 1 == 0);
assert(1 <=> 0 == 1);
assert(0 <=> 1 == -1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Spaceship string", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert("a" <=> "a" == 0);
assert("b" <=> "a" == 1);
assert("a" <=> "b" == -1);
assert("a" <=> "aa" == -1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("In", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(3 in (1..3));
assert(3 in {1..3});
assert(!(5 in (1..3)));
assert(!(5 in {1..3}));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("In string", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert("A" in "ABC");
assert(!("a" in "ABC"));
assert(65 in "ABC");
assert(!(2 in "ABC"));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("[] overload", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = {
    val = std.new_array(10, 2),
    operator[] = function(key, rhs)
    {
        if (rhs == null)
            return this.val[key];
        this.val[key] = rhs;
    }
};
assert(t[1] == 2);
t[1] = 5;
assert(t[1] == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Stringify", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert($1 == "1");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Stringify 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert($"hello" == "hello");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Stringify 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
x = 10;
assert(`${x}` == $x);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Stringify evaluate expression", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert($(1+1) == "2");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Stringify overload", "[operator]")
{
    static constexpr const char* Src = R"tl(
tbl = {
    value = 0,
    constructor = function(v) { this.value = v; },
    $operator = function {
        return std.to_string(this.value);
    }
};

t2000 = tbl(2000);
assert($t2000 == "2000");
assert(`${t2000}` == "2000");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Stringify no overload", "[operator]")
{
    static constexpr const char* Src = R"tl(
tbl = {
    value = 0,
    constructor = function(v) { this.value = v; },
};

t2000 = tbl(2000);
assert($t2000 == "[table]");
assert(`${t2000}` == "[table]");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("CountOf", "[operator]")
{
    static constexpr const char* Src = R"tl(
t = { 1, 2, 3 };
assert(#t == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf local", "[operator]")
{
    static constexpr const char* Src = R"tl(
v = 1;
assert($$v === "v");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf function", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert($$println === "println");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf library function", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert($$std.chr === "chr");
assert($$std === "std");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf library function 2", "[operator]")
{
    static constexpr const char* Src = R"tl(
c = std.chr;
assert($$c === "chr");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf library function 3", "[operator]")
{
    static constexpr const char* Src = R"tl(
function foo() {};
assert($$foo === "foo");
bar = foo;
assert($$bar === "foo");
)tl";
    // If a function has a name it uses that name.
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf library function 4", "[operator]")
{
    static constexpr const char* Src = R"tl(
foo = function() {};
assert($$foo === "foo");
bar = foo;
assert($$bar === "bar");
)tl";
    // That's an anonymous function assigned to a variable, hence it does not have its own name,
    // therefore the name of the variable is used.
    REQUIRE(Success(Src));
}

TEST_CASE("NameOf library function fail", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert($c === "chr");
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::UndefinedIdentifier));
}

TEST_CASE("Overload NameOf", "[operator]")
{
    static constexpr const char* Src = R"tl(
tab = {
    $operator = function() { return "test"; }
};
assert($tab == "test");
// Subscript has highest precedence so $tab[..] doesn't work
assert(($tab)[..] == "test");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Reverse string", "[operator]")
{
    static constexpr const char* Src = R"tl(
str = "abc";
assert(~str == "cba");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Reverse table", "[operator]")
{
    static constexpr const char* Src = R"tl(
tab = {1, 2, 3};
assert(~tab == {3,2,1});
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Reverse table range", "[operator]")
{
    static constexpr const char* Src = R"tl(
tab = ~(1..3);
assert(tab == {3,2,1});
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("int < string: type mismath", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(1 < "2");
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("int > string: type mismath", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2 > "1");
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("int >= string: type mismath", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2 >= "2");
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("int <= string: type mismath", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2 <= "2");
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("Comparison int <=> float", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2 <= 2.0);
assert(2 == 2.0);
assert(2 !== 2.0);
assert(2 >= 2.0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Comparison float <=> int", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(2.0 <= 2);
assert(2.0 == 2);
assert(2.0 !== 2);
assert(2.0 >= 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("not not", "[operator]")
{
    static constexpr const char* Src = R"tl(
assert(!!true);
)tl";
    REQUIRE(Success(Src));
}
