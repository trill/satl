/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"
#include <utf8.h>

TEST_CASE("Literals", "[literal]")
{
    static constexpr const char* Src = R"tl(
// Testing number literals
hex = 0x2a;
assert(type(hex) == TYPE_INT);
assert(hex == 42);
oct = 0o24;
assert(type(oct) == TYPE_INT);
assert(oct == 20);
bin = 0b010101;
assert(type(bin) == TYPE_INT);
assert(bin == 21);
dec = 1.4e+2;
assert(dec == 140.0);
flt = 1.42;
assert(flt == 1.42);
float2 = 1.0;
assert(float2 == 1.0);

// Other literals
nullvar = null;
assert(nullvar == null);
str = "foor bar baz";
assert(str == "foor bar baz");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Escape sequences", "[literal]")
{
    static constexpr const char* Src = R"tl(
str = "this \t is a tab";

println(str);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Octal Escape sequence out or range", "[literal]")
{
    static constexpr const char* Src = R"tl(
str = "this \777 is a tab";

println(str);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Invalid Escape sequence", "[literal]")
{
    static constexpr const char* Src = R"tl(
str = "this \z is a tab";

println(str);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Raw string", "[literal]")
{
    static constexpr const char* Src = R"tl(
str = R"string(this is a raw string
anything goes here " '
another line)string";

lines = std.split(str, "\n");
assert(#lines == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Imutable string literal", "[literal]")
{
    static constexpr const char* Src = R"tl(
function t(v)
{
    content = "Testing:";
    content += v;
    return content;
}

assert(t("test1") == "Testing:test1");
assert(t("test2") == "Testing:test2");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("UTF-16 literal", "[literal][unicode]")
{
    static constexpr const char* Src = R"tl(
str = "\u221E";
assert(str == "∞");
str = "\u083A";
assert(str == "\u083A");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("UTF-32 literal", "[literal][unicode]")
{
    static constexpr const char* Src = R"tl(
str = "\U0001F6B3";
assert(str == "🚳");
assert(str == "\U0001F6B3");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("UTF-32 literal 2", "[literal][unicode]")
{
    static constexpr const char* Src = R"tl(
str = "\U00002603";
assert(str == "☃");
str = "\u2603";
assert(str == "☃");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Number separator valid", "[literal][number]")
{
    static constexpr const char* Src = R"tl(
i = 200_000;
assert(i == 200000);
b = 0b00_111;
assert(b == 0b00111);
h = 0xbad_cde;
assert(h == 0xbadcde);
o = 0o6_77;
assert(o == 0o677);

s = 10_1e+1_0;
assert(s == 101e10);
d = 3_1.3_1;
assert(d == 31.31);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Number separator not valid", "[literal][number]")
{
    static constexpr const char* Src = R"tl(
d = 200_000_;
assert(d == 200000);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}
