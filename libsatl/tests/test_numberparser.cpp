/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "../number_parser.h"

TEST_CASE("int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("12345", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == 12345);
}

TEST_CASE("neg int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-12345", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == -12345);
}

TEST_CASE("dec", "[parser]")
{
    auto str = sa::tl::ParseNumberString("3.1415", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsFloat());
    REQUIRE(val.As<sa::tl::Float>() == Approx(3.1415));
}

TEST_CASE("neg dec", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-3.1415", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsFloat());
    REQUIRE(val.As<sa::tl::Float>() == Approx(-3.1415));
}

TEST_CASE("sci int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("2e2", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == 2e2);
}

TEST_CASE("sci neg exp int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("2e-2", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsFloat());
    REQUIRE(val.As<sa::tl::Float>() == Approx(2e-2));
}

TEST_CASE("neg sci int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-2e2", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    }, true);
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == -2e2);
}

TEST_CASE("neg sci neg exp int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-2e-2", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    }, true);
    REQUIRE(val.IsFloat());
    REQUIRE(val.As<sa::tl::Float>() == Approx(-2e-2));
}

TEST_CASE("bin int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("0b1011100", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == 0b1011100);
}

TEST_CASE("neg bin int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-0b1011100", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    }, true);
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == -0b1011100);
}

TEST_CASE("oct int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("0o777", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == 0777);
}

TEST_CASE("neg oct int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-0o777", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    }, true);
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == -0777);
}

TEST_CASE("hex int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("0x2a", [](const std::string&){
        REQUIRE(false);
    });
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    });
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == 0x2a);
}

TEST_CASE("neg hex int", "[parser]")
{
    auto str = sa::tl::ParseNumberString("-0x2a", [](const std::string&){
        REQUIRE(false);
    }, true);
    auto val = sa::tl::NumberStringToValue(str, [](const std::string&){
        REQUIRE(false);
    }, true);
    REQUIRE(val.IsInt());
    REQUIRE(val.As<sa::tl::Integer>() == -0x2a);
}
