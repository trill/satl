/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <error.h>

namespace sa::tl {
class Context;
}

bool RunSource(const std::string& source, sa::tl::Context& ctx, int* error = nullptr);
bool Success(const std::string& source);
bool Fail(const std::string& source, int error = 0);
bool Fail(const std::string& source, sa::tl::Context& ctx, int error = 0);
