/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "test_run.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <satl.h>

static bool RunSource(const std::string& source, int* error = nullptr)
{
    sa::tl::Context ctx;
    return RunSource(source, ctx, error);
}

bool RunSource(const std::string& source, sa::tl::Context& ctx, int* error)
{
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(source);
    auto root = parser.Parse();
    if (!root)
    {
        if (error)
            *error = sa::tl::Error::SyntaxError;
        // Syntax error(s)
        for (const auto& e : parser.GetErrors())
            std::cerr << e << std::endl;
        return false;
    }

    root->Evaluate(ctx);
    if (ctx.HasErrors() && error)
        *error = ctx.GetErrors().back().code;
    return !ctx.HasErrors();
}

bool Success(const std::string& source)
{
    return RunSource(source);
}

bool Fail(const std::string& source, int error)
{
    int err = 0;
    bool res = RunSource(source, &err);
    if (res)
        return false;
    if (error != 0)
        return err == error;
    return true;
}

bool Fail(const std::string& source, sa::tl::Context& ctx, int error)
{
    int err = 0;
    bool res = RunSource(source, ctx, &err);
    if (res)
        return false;
    if (error != 0)
        return err == error;
    return true;
}
