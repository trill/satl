/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <satl.h>
#include <catch2/catch.hpp>
#include "test_run.h"
#include <vector>
#include <memory>

namespace test {

class TestClassbindTestClass
{
private:
public:
    int GetValue() const { return value_; }
    void SetValue(int value) { value_ = value; }
    void Say(std::string what)
    {
        std::cout << what << std::endl;
    }
    int GetValueX(int x) const { return value_ * x; }
    TestClassbindTestClass* OpAdd(const sa::tl::Value& rhs) const
    {
        static std::vector<std::unique_ptr<TestClassbindTestClass>> classes;
        auto result = std::make_unique<TestClassbindTestClass>();
        auto* resultPtr = result.get();
        classes.push_back(std::move(result));
        if (rhs.IsInt() || rhs.IsFloat())
            // Adding scalar value
            resultPtr->value_ = value_ + rhs.ToInt();
        else if (rhs.IsObject())
        {
            const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
            if (!ro->GetMetatable().InstanceOf<TestClassbindTestClass>())
                return nullptr;
            auto* obj = ro->GetMetatable().CastTo<TestClassbindTestClass>(ro.get());
            resultPtr->value_ = value_ + obj->value_;
        }
        return resultPtr;
    }
    TestClassbindTestClass* OpCompoundAdd(const sa::tl::Value& rhs)
    {
        if (rhs.IsInt() || rhs.IsFloat())
            // Adding scalar value
            value_ += rhs.ToInt();
        else if (rhs.IsObject())
        {
            const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
            if (!ro->GetMetatable().InstanceOf<TestClassbindTestClass>())
            {
                SATL_ASSERT_FALSE();
            }
            auto* obj = ro->GetMetatable().CastTo<TestClassbindTestClass>(ro.get());
            value_ += + obj->value_;
        }
        return this;
    }
    TestClassbindTestClass& OpInc()
    {
        ++value_;
        return *this;
    }
    sa::tl::Value OpNot() const
    {
        return true;
    }
    sa::tl::Value OpCountOf() const
    {
        return 42;
    }
    sa::tl::Value OpEq(const sa::tl::Value& rhs) const
    {
        if (rhs.IsObject())
        {
            const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
            if (!ro->GetMetatable().InstanceOf<TestClassbindTestClass>())
                return false;
            auto* obj = ro->GetMetatable().CastTo<TestClassbindTestClass>(ro.get());
            return { value_ == obj->value_ };
        }

        return { false };
    }
protected:
    int value_{ 0 };
};

class Derived : public TestClassbindTestClass
{
public:
    int DerivedFunc(int x) { return value_ + x; }
};

class ManagedObject
{
public:
    explicit ManagedObject(sa::tl::Context& ctx) :
        ctx_(ctx)
    {}
    int GetValue() const { return value_; }
    void SetValue(int value) { value_ = value; }
    ManagedObject* OpAdd(const sa::tl::Value& rhs) const
    {
        auto* resultPtr = ctx_.GetGC().CreateObject<ManagedObject>(ctx_);
        if (rhs.IsInt() || rhs.IsFloat())
            // Adding scalar value
            resultPtr->value_ = value_ + rhs.ToInt();
        else if (rhs.IsObject())
        {
            const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
            if (!ro->GetMetatable().InstanceOf<ManagedObject>())
                return nullptr;
            auto* obj = ro->GetMetatable().CastTo<ManagedObject>(ro.get());
            resultPtr->value_ = value_ + obj->value_;
        }
        return resultPtr;
    }
    ManagedObject& OpInc()
    {
        ++value_;
        return *this;
    }
private:
    sa::tl::Context& ctx_;
    int value_{ 0 };
};

}

TEST_CASE("Class Get/Set", "[class][property][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    // Create an instance of your C++ class
    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    // Tell the script engine what type of class it is
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
test.value = 5;

assert(test.value == 5);

cpy = copy(test);
assert(cpy.value == 5);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Class Function", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    // Create an instance of your C++ class
    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    // Tell the script engine what type of class it is
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
test.Say("Hello");
test.value = 5;
assert(test.GetValueX(2) == 10);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Store member function", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
say = test.Say;
assert(type(say) == TYPE_FUNCTION);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Call stored member function", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    // std.call() can also call a member function. It expects the first argument
    // to be the this pointer.
    static constexpr const char* Src = R"tl(
test.value = 5;
vx = test.GetValueX;
x = std.call(vx, { test, 2 });
assert(x == 10);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Const object property set", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst, -1, true);

    static constexpr const char* Src = R"tl(
test.value = 5;
)tl";
    REQUIRE(Fail(Src, ctx, sa::tl::Error::Code::CallingNonconstFunction));
}

TEST_CASE("Const object non const function", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst, -1, true);

    static constexpr const char* Src = R"tl(
test.Say("xyc");
)tl";
    REQUIRE(Fail(Src, ctx, sa::tl::Error::Code::CallingNonconstFunction));
}

TEST_CASE("Const object const function", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst, -1, true);

    static constexpr const char* Src = R"tl(
xy = test.GetValueX(2);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Class function Type mismatch", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
test.Say(null);
//test.value = 5;
//assert(test.GetValueX(2) == 10);
)tl";
    REQUIRE(Fail(Src, ctx, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("Class function Type mismatch 2", "[class][function][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
test.value = "test";
//assert(test.GetValueX(2) == 10);
)tl";
    REQUIRE(Fail(Src, ctx, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("Class Inheritance", "[class][inheritance][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddFunction<test::TestClassbindTestClass, void, std::string>("Say", &test::TestClassbindTestClass::Say)
        .AddFunction<test::TestClassbindTestClass, int, int>("GetValueX", &test::TestClassbindTestClass::GetValueX)
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    ctx.AddMetatable<test::Derived, test::TestClassbindTestClass>()
        .AddFunction<test::Derived, int, int>("DereivedFunc", &test::Derived::DerivedFunc);

    std::shared_ptr<test::Derived> inst = std::make_shared<test::Derived>();
    ctx.SetInstance<test::Derived>("test", *inst);

    static constexpr const char* Src = R"tl(
test.Say("Hello");
test.value = 5;
assert(test.GetValueX(2) == 10);
assert(test.DereivedFunc(2) == 7);
)tl";
    REQUIRE(RunSource(Src, ctx));
    auto testPtr = ctx["test"];
    REQUIRE(testPtr);
    REQUIRE(testPtr->Is<sa::tl::ObjectPtr>());
    const auto& objPtr = testPtr->As<sa::tl::ObjectPtr>();
    REQUIRE(objPtr->GetMetatable().InstanceOf<test::Derived>());
    REQUIRE(objPtr->GetMetatable().InstanceOf<test::TestClassbindTestClass>());
    REQUIRE(objPtr->GetMetatable().InstanceOf("test::TestClassbindTestClass"));
}

TEST_CASE("Object equality", "[object][compare][native]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);
    std::shared_ptr<test::TestClassbindTestClass> inst2 = std::make_shared<test::TestClassbindTestClass>();
    ctx.SetInstance<test::TestClassbindTestClass>("test_another", *inst2);

    static constexpr const char* Src = R"tl(
assert(test != test_another);

test2 = test;
assert(test2 == test);
assert(test2 != test_another);
assert(test2 === test);
assert(test2 != null);

test_another2 = copy(test_another);
assert(test_another2 == test_another);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Object binary operator overloading", "[object][operator]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue)
        .AddOperator<test::TestClassbindTestClass>("operator+", &test::TestClassbindTestClass::OpAdd)
        .AddOperator<test::TestClassbindTestClass>("operator+=", &test::TestClassbindTestClass::OpCompoundAdd)
        .AddOperator<test::TestClassbindTestClass>("operator==", &test::TestClassbindTestClass::OpEq);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    inst->SetValue(2);
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);
    std::shared_ptr<test::TestClassbindTestClass> inst2 = std::make_shared<test::TestClassbindTestClass>();
    inst2->SetValue(3);
    ctx.SetInstance<test::TestClassbindTestClass>("test_another", *inst2);

    static constexpr const char* Src = R"tl(
// Calls OpAdd() returns a new object
c = test + test_another;
assert(c.value == 5);
// Calls OpEq()
assert(c == c);
// Same pointer
assert(c === c);

// Calls OpCompoundAdd()
c += 5;
assert(c.value == 10);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Object unary operator overloading (inc)", "[object][operator]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddProperty<test::TestClassbindTestClass>("value", &test::TestClassbindTestClass::GetValue, &test::TestClassbindTestClass::SetValue)
        .AddOperator<test::TestClassbindTestClass>("++operator", &test::TestClassbindTestClass::OpInc);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    inst->SetValue(2);
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
++test;
assert(test.value == 3);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Object unary operator overloading (not)", "[object][operator]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddOperator("!operator", &test::TestClassbindTestClass::OpNot);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    inst->SetValue(2);
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
// The ! operator returns true
assert(test);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Object unary operator overloading (countof)", "[object][operator]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::TestClassbindTestClass>()
        .AddOperator("#operator", &test::TestClassbindTestClass::OpCountOf);

    std::shared_ptr<test::TestClassbindTestClass> inst = std::make_shared<test::TestClassbindTestClass>();
    inst->SetValue(2);
    ctx.SetInstance<test::TestClassbindTestClass>("test", *inst);

    static constexpr const char* Src = R"tl(
assert(#test == 42);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("GCd Object", "[object]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::ManagedObject>()
        .AddProperty<test::ManagedObject>("value", &test::ManagedObject::GetValue, &test::ManagedObject::SetValue)
        .AddOperator<test::ManagedObject>("operator+", &test::ManagedObject::OpAdd)
        .AddOperator<test::ManagedObject>("++operator", &test::ManagedObject::OpInc);
    auto* inst = ctx.GetGC().CreateObject<test::ManagedObject>(ctx);
    inst->SetValue(2);
    ctx.SetInstance<test::ManagedObject>("test", *inst);
    auto* inst2 = ctx.GetGC().CreateObject<test::ManagedObject>(ctx);
    inst2->SetValue(3);
    ctx.SetInstance<test::ManagedObject>("test_another", *inst2);

    static constexpr const char* Src = R"tl(
{
    c = test + test_another;
    assert(c.value == 5);
}
assert(_gc.allocs == 3);
// 16 * 3
assert(_gc.size == 48);
_gc.run();
// Counter is reset each run()
assert(_gc.allocs == 0);
// 16 * 2, `c` is no longer used because it ran out of scope
assert(_gc.size == 32);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("GCd Object capture", "[object]")
{
    sa::tl::Context ctx;
    ctx.AddMetatable<test::ManagedObject>()
        .AddProperty<test::ManagedObject>("value", &test::ManagedObject::GetValue, &test::ManagedObject::SetValue)
        .AddOperator<test::ManagedObject>("operator+", &test::ManagedObject::OpAdd)
        .AddOperator<test::ManagedObject>("++operator", &test::ManagedObject::OpInc);
    auto* inst = ctx.GetGC().CreateObject<test::ManagedObject>(ctx);
    inst->SetValue(2);
    ctx.SetInstance<test::ManagedObject>("test", *inst);
    auto* inst2 = ctx.GetGC().CreateObject<test::ManagedObject>(ctx);
    inst2->SetValue(3);
    ctx.SetInstance<test::ManagedObject>("test_another", *inst2);

    static constexpr const char* Src = R"tl(
func = null;
{
    c = test + test_another;
    assert(c.value == 5);
    // c would run out of scope but func captures it
    func = function[c]() { return c.value; };
}
assert(_gc.allocs == 3);
// 16 * 3
assert(_gc.size == 48);
_gc.run();
// Counter is reset each run()
assert(_gc.allocs == 0);
// `c` is still used as capture of func
assert(_gc.size == 48);
assert(func() == 5);
)tl";
    REQUIRE(RunSource(Src, ctx));
}

TEST_CASE("Duplicate metatable", "[object]")
{
    sa::tl::Context ctx;
    auto& mt = ctx.AddMetatable<test::ManagedObject>();
    // Adsding the same class twice does not add it, but just returns the previously added
    auto& mt2 = ctx.AddMetatable<test::ManagedObject>();
    REQUIRE(&mt == &mt2);
}
