/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"

TEST_CASE("Escape sequence", "[string]")
{
    static constexpr const char* Src = R"tl(
str = R"(hello "world")";
assert(str == "hello \"world\"");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Escape sequence hex", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello \x41";
assert(str == "hello A");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Escape sequence oct", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello \41";
assert(str == "hello !");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Raw String", "[string]")
{
    static constexpr const char* Src = R"tl(
str = R"(hello)";
assert(str == "hello");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Raw String 2", "[string]")
{
    static constexpr const char* Src = R"tl(
str = R"delim(hello)delim";
assert(str == "hello");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello";
str = str + " ";
str = str + "friends!";
assert(str == "hello friends!");

str2 = "hello " + 3;  // Assigning a number to a string -> number is converted to string
assert(str2 == "hello 3");

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat 2", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello friends!" + "a" + "b" + 3 + "" + 1.5;
assert(str == "hello friends!ab31.500000");

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat 3", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "" + 3;
assert(str == "3");

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat 4", "[string]")
{
    static constexpr const char* Src = R"tl(
s = "hello" " " "there";
assert(s == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat 5", "[string]")
{
    static constexpr const char* Src = R"tl(
s = `hello` ` ` `there`;
assert(s == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat 6", "[string]")
{
    static constexpr const char* Src = R"tl(
s = R"(hello)" R"( there)";
assert(s == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String concat 7", "[string]")
{
    static constexpr const char* Src = R"tl(
s = R"(hello)" " " R"(there)";
assert(s == "hello there");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String delete", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello friends!";
assert(str == "hello friends!");

std.delete(str, 2);
assert(str == "helo friends!");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String get subscript", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello friends!";
assert(str[0] == "h");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String set subscript", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello friends!";
assert(str == "hello friends!");
str[0] = 'H';
assert(str == "Hello friends!");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String set subscript int", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello friends!";
assert(str == "hello friends!");
str[0] = 0x48; // H
assert(str == "Hello friends!");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String literal subscript", "[string]")
{
    static constexpr const char* Src = R"tl(
assert("ABC"[0] == "A");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String literal subscript assign", "[string]")
{
    static constexpr const char* Src = R"tl(
"ABC"[0] = "X";
)tl";
    // Fails, can't assign to literal
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("String literal count", "[string]")
{
    static constexpr const char* Src = R"tl(
assert(#"ABC" == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String by ref", "[string]")
{
    static constexpr const char* Src = R"tl(
function x(str, i, c)
{
    str[i] = c;
}

str = "hello friends!";
x(str, 0, "H");
assert(str == "Hello friends!");

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String by ref 2", "[string]")
{
    static constexpr const char* Src = R"tl(
function x(str, i, c)
{
    // str2 will modify str
    str2 = str;
    str2[i] = c;
}

str = "hello friends!";
x(str, 0, "H");
assert(str == "Hello friends!");

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String by ref copy", "[string]")
{
    static constexpr const char* Src = R"tl(
function x(str, i, c)
{
    // Strings and tables are pointers, to duplicate them use copy()
    str2 = copy(str);
    str2[i] = c;
}

str = "hello friends!";
x(str, 0, "H");
println(str);

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String slice", "[string]")
{
    static constexpr const char* Src = R"tl(
str = "hello friends!";
str2 = std.slice(str, 1, 3);
assert(str2 == "el");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal", "[string]")
{
    static constexpr const char* Src = R"tl(
x = `string text ${1+1} string text`;
assert(x == "string text 2 string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal 2", "[string]")
{
    static constexpr const char* Src = R"tl(
function func(a)
{
    return a;
}
x = `string text ${func(2)} string text`;
assert(x == "string text 2 string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal 3", "[string]")
{
    static constexpr const char* Src = R"tl(
a = 5;
x = `string text ${a} string text`;
assert(x == "string text 5 string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal 4", "[string]")
{
    static constexpr const char* Src = R"tl(
a = 5;
x = `string text ${a} string text` + "string text string text";
assert(x == "string text 5 string textstring text string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal number literal expression", "[string]")
{
    static constexpr const char* Src = R"tl(
x = `string text ${5} string text` + "string text string text";
assert(x == "string text 5 string textstring text string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal 5", "[string]")
{
    static constexpr const char* Src = R"tl(
a = 5;
x = `string text ${a} string text` * 2;
assert(x == "string text 5 string textstring text 5 string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal 6", "[string]")
{
    static constexpr const char* Src = R"tl(
function foo(a)
{
    return a * 2;
}
y = `string text ${foo(1+1)} string text`;
assert(y == "string text 4 string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Template literal 7", "[string]")
{
    static constexpr const char* Src = R"tl(
x = `string text "${1+1}" string text`;
assert(x == "string text \"2\" string text");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String range", "[string]")
{
    static constexpr const char* Src = R"tl(
t = "Helo there";
s = t[5..8];
assert(s == "ther");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String range 2", "[string]")
{
    static constexpr const char* Src = R"tl(
s = "Helo there"[5..8];
assert(s == "ther");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String range comapre", "[string]")
{
    static constexpr const char* Src = R"tl(
s1 = "Hello ther";
s2 = "Hello there";
assert(s1[1..5] == s2[1..5]);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("String range auto end", "[string]")
{
    static constexpr const char* Src = R"tl(
s = "Hello there";
assert(s[2..] == "llo there");
)tl";
    REQUIRE(Success(Src));
}
