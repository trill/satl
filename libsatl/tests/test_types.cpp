/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"

TEST_CASE("bool set null", "[types]")
{
    static constexpr const char* Src = R"tl(
a = true;
a = null;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("int set null", "[types]")
{
    static constexpr const char* Src = R"tl(
a = 5;
a = null;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("float set null", "[types]")
{
    static constexpr const char* Src = R"tl(
a = 5.0;
a = null;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("string set null", "[types]")
{
    static constexpr const char* Src = R"tl(
a = "this is a test";
a = null;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("table set null", "[types]")
{
    static constexpr const char* Src = R"tl(
a = {};
a = null;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("table set int", "[types]")
{
    static constexpr const char* Src = R"tl(
a = {};
a = 2;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("Type hint", "[types]")
{
    static constexpr const char* Src = R"tl(
a: int = 0;
a = {};
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("Type hint 2", "[types]")
{
    static constexpr const char* Src = R"tl(
function foo(): any
{
    return 1;
}

a: int = foo();
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Type hint 3", "[types]")
{
    static constexpr const char* Src = R"tl(
function foo(): any
{
    return "hello";
}

a: int = foo();
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("Type hint 4", "[types]")
{
    static constexpr const char* Src = R"tl(
function foo(): int
{
    return "hello";
}

a: int = foo();
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

// The only type hint that is compatible with null is any.
TEST_CASE("Type hint 5", "[types]")
{
    static constexpr const char* Src = R"tl(
a: int = null;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}
TEST_CASE("Type hint 6", "[types]")
{
    static constexpr const char* Src = R"tl(
a: any = null;
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Type hint table", "[types]")
{
    static constexpr const char* Src = R"tl(
tab: table = {
    foo: int = 1,
    bar: string = "hello",
    baz: function = function(): int { return 1; }
};
assert(tab.foo === 1);
assert(tab.bar === "hello");
assert(tab.baz() === 1);
)tl";
    REQUIRE(Success(Src));
}
