/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <satl.h>
#include <catch2/catch.hpp>

TEST_CASE("Value Is", "[value]")
{
    sa::tl::Value bValue = true;
    sa::tl::Value iValue = 42;
    REQUIRE(bValue.Is<bool>());
    REQUIRE(iValue.Is<sa::tl::Integer>());
}

TEST_CASE("Value As", "[value]")
{
    sa::tl::Value value = true;
    REQUIRE(value.Is<bool>());
    REQUIRE(value.As<bool>() == true);
    sa::tl::Value iValaue = 42;
    REQUIRE(iValaue.As<sa::tl::Integer>() == 42);
}

TEST_CASE("Value IsConvertible null", "[value][null]")
{
    sa::tl::Value value = nullptr;
    REQUIRE(value.IsConvertible<bool>());
    REQUIRE(!value.IsConvertible<int>());
    REQUIRE(!value.IsConvertible<float>());
    REQUIRE(!value.IsConvertible<std::string>());
    REQUIRE(value.IsConvertible<std::nullptr_t>());
}

TEST_CASE("Value ConvertTo null", "[value][null]")
{
    sa::tl::Value value = nullptr;
    REQUIRE(value.ConvertTo<std::nullptr_t>() == nullptr);
}

TEST_CASE("Value IsConvertible bool", "[value][bool]")
{
    sa::tl::Value value = true;
    REQUIRE(value.IsConvertible<bool>());
    REQUIRE(value.IsConvertible<int>());
    REQUIRE(!value.IsConvertible<float>());
    REQUIRE(!value.IsConvertible<std::string>());
    REQUIRE(!value.IsConvertible<std::nullptr_t>());
}

TEST_CASE("Value ConvertTo bool", "[value][bool]")
{
    sa::tl::Value value = true;
    REQUIRE(value.ConvertTo<bool>() == true);
    REQUIRE(value.ConvertTo<int>() == 1);
}

TEST_CASE("Value IsConvertible int", "[value][int]")
{
    sa::tl::Value value = 42;
    REQUIRE(value.IsConvertible<bool>());
    REQUIRE(value.IsConvertible<int>());
    REQUIRE(value.IsConvertible<unsigned int>());
    REQUIRE(value.IsConvertible<int64_t>());
    REQUIRE(value.IsConvertible<uint64_t>());
    REQUIRE(value.IsConvertible<float>());
    REQUIRE(value.IsConvertible<double>());
    REQUIRE(!value.IsConvertible<std::string>());
    REQUIRE(!value.IsConvertible<std::nullptr_t>());
}

TEST_CASE("Value ConvertTo int", "[value][int]")
{
    sa::tl::Value value = 42;
    REQUIRE(value.ConvertTo<bool>() == true);
    REQUIRE(value.ConvertTo<int>() == 42);
    REQUIRE(value.ConvertTo<float>() == Approx(42.0f));
    REQUIRE(value.ConvertTo<double>() == Approx(42.0));
}

TEST_CASE("Value IsConvertible float", "[value][float]")
{
    sa::tl::Value value = 42.5f;
    REQUIRE(!value.IsConvertible<bool>());
    REQUIRE(value.IsConvertible<int>());
    REQUIRE(value.IsConvertible<unsigned int>());
    REQUIRE(value.IsConvertible<int64_t>());
    REQUIRE(value.IsConvertible<uint64_t>());
    REQUIRE(value.IsConvertible<float>());
    REQUIRE(value.IsConvertible<double>());
    REQUIRE(!value.IsConvertible<std::string>());
    REQUIRE(!value.IsConvertible<std::nullptr_t>());
}

TEST_CASE("Value ConvertTo float", "[value][float]")
{
    sa::tl::Value value = 42.5f;
    REQUIRE(value.ConvertTo<int>() == 42);
    REQUIRE(value.ConvertTo<float>() == 42.5f);
    REQUIRE(value.ConvertTo<double>() == 42.5f);
}

TEST_CASE("Value IsConvertible string", "[value][string]")
{
    sa::tl::Value value = sa::tl::MakeString("test");
    REQUIRE(!value.IsConvertible<bool>());
    REQUIRE(!value.IsConvertible<int>());
    REQUIRE(!value.IsConvertible<unsigned int>());
    REQUIRE(!value.IsConvertible<int64_t>());
    REQUIRE(!value.IsConvertible<uint64_t>());
    REQUIRE(value.IsConvertible<std::string>());
    REQUIRE(!value.IsConvertible<std::nullptr_t>());
}

TEST_CASE("Value IsCompatible null", "[value][null]")
{
    sa::tl::Value value = nullptr;
    REQUIRE(value.IsCompatible(sa::tl::Value(nullptr)));
    REQUIRE(value.IsCompatible(sa::tl::Value(1)));
    REQUIRE(value.IsCompatible(sa::tl::Value(1.5f)));
    REQUIRE(value.IsCompatible(sa::tl::Value(sa::tl::MakeString("test"))));
    REQUIRE(value.IsCompatible(sa::tl::Value(sa::tl::MakeTable())));
}

TEST_CASE("Value Assign null", "[value][null]")
{
    SECTION("null")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = nullptr;
        value.Assign(ctx, sa::tl::Value(nullptr));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(nullptr)));
    }
    SECTION("int")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = nullptr;
        value.Assign(ctx, sa::tl::Value(1));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1)));
    }
    SECTION("float")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = nullptr;
        value.Assign(ctx, sa::tl::Value(1.5f));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1.5f)));
    }
    SECTION("string")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = nullptr;
        value.Assign(ctx, sa::tl::Value(sa::tl::MakeString("test")));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(sa::tl::MakeString("test"))));
    }
}

TEST_CASE("Value IsCompatible int", "[value][int]")
{
    sa::tl::Value value = 1;
    REQUIRE(!value.IsCompatible(sa::tl::Value(nullptr)));
    REQUIRE(value.IsCompatible(sa::tl::Value(1)));
    REQUIRE(value.IsCompatible(sa::tl::Value(1.5f)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(sa::tl::MakeString("test"))));
    REQUIRE(!value.IsCompatible(sa::tl::Value(sa::tl::MakeTable())));
}

TEST_CASE("Value Assign int", "[value][int]")
{
    SECTION("null")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1;
        value.Assign(ctx, sa::tl::Value(nullptr));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1)));
    }
    SECTION("int")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1;
        value.Assign(ctx, sa::tl::Value(2));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(2)));
    }
    SECTION("float")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1;
        value.Assign(ctx, sa::tl::Value(1.5f));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1.5f)));
    }
    SECTION("string")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1;
        value.Assign(ctx, sa::tl::Value(sa::tl::MakeString("test")));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1)));
    }
}

TEST_CASE("Value IsCompatible float", "[value][float]")
{
    sa::tl::Value value = 1.5f;
    REQUIRE(!value.IsCompatible(sa::tl::Value(nullptr)));
    REQUIRE(value.IsCompatible(sa::tl::Value(1)));
    REQUIRE(value.IsCompatible(sa::tl::Value(1.5f)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(sa::tl::MakeString("test"))));
    REQUIRE(!value.IsCompatible(sa::tl::Value(sa::tl::MakeTable())));
}

TEST_CASE("Value Assign float", "[value][float]")
{
    SECTION("null")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1.5f;
        value.Assign(ctx, sa::tl::Value(nullptr));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1.5f)));
    }
    SECTION("int")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1.5f;
        value.Assign(ctx, sa::tl::Value(1));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1)));
    }
    SECTION("float")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1.5f;
        value.Assign(ctx, sa::tl::Value(2.5f));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(2.5f)));
    }
    SECTION("string")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = 1.5f;
        value.Assign(ctx, sa::tl::Value(sa::tl::MakeString("test")));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(1.5f)));
    }
}

TEST_CASE("Value IsCompatible string", "[value][string]")
{
    sa::tl::Value value = sa::tl::MakeString("test");
    REQUIRE(!value.IsCompatible(sa::tl::Value(nullptr)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(1)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(1.5f)));
    REQUIRE(value.IsCompatible(sa::tl::Value(sa::tl::MakeString("test"))));
    REQUIRE(!value.IsCompatible(sa::tl::Value(sa::tl::MakeTable())));
}

TEST_CASE("Value Assign string", "[value][string]")
{
    SECTION("null")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = sa::tl::MakeString("test");
        value.Assign(ctx, sa::tl::Value(nullptr));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(sa::tl::MakeString("test"))));
    }
    SECTION("int")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = sa::tl::MakeString("test");
        value.Assign(ctx, sa::tl::Value(1));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(sa::tl::MakeString("test"))));
    }
    SECTION("float")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = sa::tl::MakeString("test");
        value.Assign(ctx, sa::tl::Value(1.5f));
        REQUIRE(ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(sa::tl::MakeString("test"))));
    }
    SECTION("string")
    {
        sa::tl::Context ctx;
        sa::tl::Value value = sa::tl::MakeString("test");
        value.Assign(ctx, sa::tl::Value(sa::tl::MakeString("test2")));
        REQUIRE(!ctx.HasErrors());
        REQUIRE(value.Equals(sa::tl::Value(sa::tl::MakeString("test2"))));
    }
}

TEST_CASE("Value IsCompatible table", "[value][table]")
{
    sa::tl::Value value = sa::tl::MakeTable();
    REQUIRE(!value.IsCompatible(sa::tl::Value(nullptr)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(1)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(1.5f)));
    REQUIRE(!value.IsCompatible(sa::tl::Value(sa::tl::MakeString("test"))));
    REQUIRE(value.IsCompatible(sa::tl::Value(sa::tl::MakeTable())));
}
