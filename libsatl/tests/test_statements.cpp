/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"
#include <satl.h>

TEST_CASE("Empty", "[statement]")
{
    static constexpr const char* Src = R"tl(
i = 1;;;;
;;;;;
i += 3;
assert(i == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If", "[statement][if]")
{
    static constexpr const char* Src = R"tl(
a = 2;
if (a == 1)
{
    assert(false);
}
else
{
    assert(a != 1);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If 2", "[statement][if]")
{
    static constexpr const char* Src = R"tl(
a = 2;
if (a == 1)
{
    assert(false);
}
else if (a == 2)
    assert(a == 2);
else
{
    assert(a != 1);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If 3", "[statement][if]")
{
    static constexpr const char* Src = R"tl(
a = 2;
b = 4;
if (a == 1 && b == 3)
{
    assert(false);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table literal key", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
table = {
    "TEST" = 1,
    "TEST2" = 3
};

value = {
    a = 2,
    b = 4
};

if (value.a == table.TEST && value.b == table.TEST2)
{
    assert(false);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If 5", "[statement][if]")
{
    static constexpr const char* Src = R"tl(

a = 2;
b = 4;
c = 8;
d = 16;

if (a == 2 && b == 4 || c != 8)
{
}
else
    assert(false);

if (a == 2 && b == 4 && c != 8)
{
    assert(false);
}

if (true && false)
{
    assert(false);
}
if (true || false)
{
}
else
    assert(false);

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If 7", "[statement][if]")
{
    static constexpr const char* Src = R"tl(
i = 10;
if (false) { }
else while (i > 1) --i;

assert(i == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If 6", "[statement][if]")
{
    static constexpr const char* Src = R"tl(

a = 2;
b = 4;

if (a == 2 && b == 4 ? true : false)
{
}
else
    assert(false);

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("If assign", "[statement][if]")
{
    static constexpr const char* Src = R"tl(
if (a = 2)
{
}
else
    assert(false);

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch 1", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
a = 3;
switch (a)
{
    case 0:
    {
        assert(false);
    }
    case 1:
    {
        assert(false);
    }
    case 2:
    {
        assert(false);
    }
    case 3:
    {
        fallthrough;
    }
    case 4:
    {
        assert((a == 3) || (a == 4));
    }
    default:
    {
        assert(false);
    }
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch 2", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
// Switch with strings
str = "hello";
switch (str)
{
    case "rupert":
    {
        assert(false);
    }
    case "hello":
    {
        assert(str == "hello");
    }
    default:
    {
        assert(false);
    }
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch 3", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
// Switch with string expression
str = "hello";
switch (str)
{
    case "rupert":
    {
        assert(false);
    }
    case "he" + "llo":
    {
        assert(str == "hello");
    }
    default:
    {
        assert(false);
    }
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch 4", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
// Switch fall through
i = 2;
e = 0;
switch (i)
{
    case 1:
    {
        assert(false);
    }
    case 2:
    {
        ++e;
        fallthrough;
    }
    case 3:
    {
        ++e;
        fallthrough;
    }
    default:
    {
        assert(false);
    }
}
assert(e == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch 5", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
i = 0;
three = 0;
five = 0;

for (; i < 9; )
{
    switch (i) {
        case 3:
        {
            three = i;
        }
        case 5:
        {
            five = i;
        }
    }
    ++i;
}
assert(three == 3);
assert(five == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch 6", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
function switch_test(i)
{
    result = 0;
    switch (i)
    {
        case 1:
        {
            result += 1;
        }
        case 1 + 1:
        {
            result += 2;
            fallthrough;
        }
        default:
        {
            result += 4;
        }
        case 1 + 2:
        {
            result += 8;
        }
    }
    return result;
}

assert(switch_test(1) == 1);
assert(switch_test(2) == 10);
assert(switch_test(3) == 8);
assert(switch_test(4) == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Switch duplicate label", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
i = 2;
e = 0;
switch (i)
{
    case 1:
    {
        assert(false);
    }
    case 2:
    {
        ++e;
        fallthrough;
    }
    case 2:
    {
        ++e;
        fallthrough;
    }
    default:
    {
        assert(false);
    }
}
assert(e == 2);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Switch duplicate default", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
i = 2;
e = 0;
switch (i)
{
    case 1:
    {
        assert(false);
    }
    case 2:
    {
        ++e;
        fallthrough;
    }
    default:
    {
        ++e;
    }
    default:
    {
        assert(false);
    }
}
assert(e == 2);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Switch range", "[statement][switch]")
{
    static constexpr const char* Src = R"tl(
succ = false;
switch (2)
{
    case 1..2:
    {
        succ = true;
    }
}
assert(succ);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("For 1", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
for (i = 0; i < 10; i = i + 1)
{
    assert((i >= 0) && (i < 10));
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("For inc", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
for (i = 0; i < 10; ++i)
{
    assert((i >= 0) && (i < 10));
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("For no curly", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
for (i = 0; i < 10; i = i + 1)
    assert((i >= 0) && (i < 10));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("For 2", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
for (i = 0; i < 10; i = i + 1)
{
    if (i == 5)
        continue;
    assert(i != 5);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("For compound", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
for (i = 0; i < 10; i += 1)
{
    if (i == 5)
        continue;
    assert(i != 5);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("For 3", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
i = 0;
for (;;)
{
    if (i > 5)
        break;
    assert(i <= 5);
    i = i + 1;
}
assert(i == 6);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Foreach range", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
i = 0;
foreach (v in 1..5)
{
    assert(++i == v);
}
assert(i == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Foreach range wrong type", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
i = 0;
foreach (v : float in 1..5)
{
    assert(++i == v);
}
assert(i == 5);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("Foreach range key", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
i = 0;
foreach (k, v in 1..5)
{
    assert(k == i);
    assert(++i == v);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Foreach reverse", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
const t = {1, 2, 3, 4, 5};
foreach (k, v in ~{5, 4, 3, 2, 1})
{
    assert(t[#t - k - 1] == v);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Do", "[statement][do]")
{
    static constexpr const char* Src = R"tl(
i = 0;
do
{
    i = i + 1;
} while (i < 10);
assert(i == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("While", "[statement][while]")
{
    static constexpr const char* Src = R"tl(
i = 0;
while (i < 10)
{
    i = i + 1;
}
assert(i == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("While no curly", "[statement][while]")
{
    static constexpr const char* Src = R"tl(
i = 0;
while (i < 10)
    i = i + 1;
assert(i == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("While break", "[statement][while]")
{
    static constexpr const char* Src = R"tl(
c = 0;
function get()
{
    ++c;
    if (c > 1)
        return "q";
    return "xxx";
}

i = 0;
while (x = get())
{
    if (x == "q")
        break;
    ++i;
}
// The 2nd get() returns "q" -> break -> i is at 1
assert(i == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function", "[statement][function]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    // Nested function, only valid in this scope
    function bar(c, d)
    {
        return c * d;
    }
    return bar(a, b) + bar(a, b);
}
assert(foo(4, 6) == 48);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Return", "[statement][return]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    return a + b;
    assert(false);
}
assert(foo(4, 6) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Const", "[statement][const]")
{
    static constexpr const char* Src = R"tl(
const i = 1;

i = i + 1;
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::ConstAssign));
}

TEST_CASE("Global", "[statement][global]")
{
    static constexpr const char* Src = R"tl(
function foo()
{
    global bar = 5;
}

foo();
assert(bar == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Global 2", "[statement][global]")
{
    static constexpr const char* Src = R"tl(
{
    global bar = 5;
}

assert(bar == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Global const", "[statement][global]")
{
    static constexpr const char* Src = R"tl(
function foo()
{
    global const bar = 5;
}

foo();
assert(bar == 5);
assert(std.is_const(bar));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Global const 2", "[statement][global]")
{
    static constexpr const char* Src = R"tl(
{
    global const bar = 5;
}

assert(bar == 5);
assert(std.is_const(bar));
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Global const 3", "[statement][global]")
{
    static constexpr const char* Src = R"tl(
{
    // Syntax error
    const global bar = 5;
}
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::SyntaxError));
}

TEST_CASE("Include", "[statement][include]")
{
    static constexpr const char* Src = R"tl(
include "i = 5;";
assert(i == 5);
)tl";

    sa::tl::Context ctx;
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    sa::tl::Parser parser(Src);
    parser.onInclude_ = [](std::string& filename, const sa::tl::Location&) -> std::string
    {
        return filename;
    };
    auto root = parser.Parse();
    REQUIRE(root);
    root->Evaluate(ctx);
    REQUIRE(!ctx.HasErrors());
}

TEST_CASE("do", "[statement][do]")
{
    static constexpr const char* Src = R"tl(
number = 0;
do {
    number++;
} while (number < 9);
assert(number == 9);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("do no curly", "[statement][do]")
{
    static constexpr const char* Src = R"tl(
number = 0;
do
    number++;
while (number < 9);
assert(number == 9);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("do empty", "[statement][do]")
{
    static constexpr const char* Src = R"tl(
do ; while (false);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("for simple", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
j = 0;
for (i = 0; i < 10; ++i)
{
    assert(i == j);
    ++j;
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("for simple type hint", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
j = 0;
for (i: int = 0; i < 10; ++i)
{
    assert(i == j);
    ++j;
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("for simple type hint error", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
j = 0;
for (i: float = 0; i < 10; ++i)
{
    assert(i == j);
    ++j;
}
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("Ranged for table", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4 };

foreach (key, value in tab)
{
    assert(tab[key] == value);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for table type hint", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
tab: table = { a = 1, b = 2, c = 3, d = 4 };

foreach (key: string, value: int in tab)
{
    assert(tab[key] == value);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for table mixed", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4, e = "string", f = 1.5 };

foreach (key, value in tab)
{
    assert(tab[key] == value);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for table no curly", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
tab = { a = 1, b = 2, c = 3, d = 4 };

foreach (key, value in tab)
    assert(tab[key] == value);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for table inline init", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
foreach (key, value in { 1, 2, 3, 4 })
{
    assert(value == key + 1);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for table inline init no curly", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
foreach (key, value in { 1, 2, 3, 4 })
    assert(value == key + 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for table inline init 2", "[statement][table]")
{
    static constexpr const char* Src = R"tl(
foreach (key, value in tab = { 1, 2, 3, 4 })
{
    assert(tab[key] == value);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for string", "[statement][string]")
{
    static constexpr const char* Src = R"tl(
str = "hello world";
foreach (key, value in str)
{
    assert(str[key] == value);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Ranged for string int", "[statement][string]")
{
    static constexpr const char* Src = R"tl(
str = "hello world";
foreach (key, value: int in str)
{
    assert(std.ord(str[key]) == value);
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Reverse foreach table", "[statement][foreach]")
{
    static constexpr const char* Src = R"tl(
t = { 1, 2, 3 };
i = 3;
foreach (k, x in ~t)
{
    assert(i == x);
    assert(k == i-1);
    --i;
}
assert(i == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("foreach string", "[statement][foreach]")
{
    static constexpr const char* Src = R"tl(
str = "abc";
c = 0;
foreach (i, v in str)
{
    assert(str[c] == v);
    ++c;
}
// To make sure it iterated
assert(c == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Reverse foreach string", "[statement][foreach]")
{
    static constexpr const char* Src = R"tl(
str = "abc";
c = 3;
foreach (i, v in ~str)
{
    assert(str[c - 1] == v);
    --c;
}
// To make sure it iterated
assert(c == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Reverse foreach range", "[statement][foreach]")
{
    static constexpr const char* Src = R"tl(
i = 3;
foreach (v in ~(1..3))
{
    assert(v == i);
    --i;
}
// To make sure it iterated
assert(i == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Nested for", "[statement][for]")
{
    static constexpr const char* Src = R"tl(
const num = 5;

x = 0;
for (n = 0; n < num; ++n)
{
    for (i = n; i < num; ++i)
    {
        ++x;
    }
}
assert(x == 15);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Try catch simple", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
called = false;
try
{
    error("test");
}
catch
{
    called = true;
}
assert(called);
)tl";
    REQUIRE(Success(Src));

}

TEST_CASE("Try", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
const i = 1;
finallyCalled = false;
catchCalled = false;
try
{
    i = 2;
}
catch (ex)
{
    catchCalled = true;
    assert(ex.code == 4);
}
finally
{
    finallyCalled = true;
    assert(i == 1);
}
assert(i == 1);
assert(catchCalled);
assert(finallyCalled);
)tl";
    REQUIRE(Success(Src));

}

TEST_CASE("Try 2", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
catchCalled = false;
notCalled = false;
try
{
    error(50, "Test");
}
catch (ex == 50)
{
    catchCalled = true;
    assert(ex.code == 50);
}
catch (ex == 51)
{
    notCalled = true;
}
assert(catchCalled);
assert(!notCalled);
)tl";
    REQUIRE(Success(Src));

}

TEST_CASE("Try 3", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
catchCalled = false;
notCalled = false;
try
{
    error(50, "Test");
}
catch (ex == 50)
{
    catchCalled = true;
    assert(ex.code == 50);
}
catch
{
    notCalled = true;
}
assert(catchCalled);
assert(!notCalled);
)tl";
    REQUIRE(Success(Src));

}

TEST_CASE("Try catch", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
result = false;
const i = 1;
try
{
    i = 2;
}
catch
{
    result = true;
}
assert(result);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Try no catch", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
result = false;
i = 1;
try
{
    i = 2;
}
catch
{
    result = true;
}
assert(!result);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Try finally", "[statement][try]")
{
    static constexpr const char* Src = R"tl(
result = false;
i = 1;
try
{
    i = 2;
}
finally
{
    result = true;
}
assert(result);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Compound assignment", "[statement][expression]")
{
    static constexpr const char* Src = R"tl(
add = 1;
add += 2;
assert(add == 3);

sub = 1;
sub -= 2;
assert(sub == -1);

mul = 2;
mul *= 2;
assert(mul == 4);

str = "foo";
str += " bar";
assert(str == "foo bar");

str2 = "foo";
str2 *= 3;
assert(str2 == "foofoofoo");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Compound assignment const", "[statement][expression]")
{
    static constexpr const char* Src = R"tl(
const add = 1;
try
{
    add += 2;
}
catch {}

assert(add == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Null assignment", "[statement][expression]")
{
    static constexpr const char* Src = R"tl(
i = null;
assert(type(i) == TYPE_NULL);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Assignment expression", "[statement][expression]")
{
    static constexpr const char* Src = R"tl(
// An assignment returns the assigned value
assert((i = 1) == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Chained assignment", "[statement][expression]")
{
    static constexpr const char* Src = R"tl(
a = b = 1;
assert(a == 1);
assert(b == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("using", "[statement][using]")
{
    static constexpr const char* Src = R"tl(
using std;
assert(trim(" hello ") == "hello");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("using 3", "[statement][using]")
{
    static constexpr const char* Src = R"tl(
ns = {
    nested = {
        foo = function(x) { return x; }
    },
    bar = function(x) { return x * 2; }
};

using ns;
assert(bar(5) == 10);
assert(nested.foo(5) == 5);

using nested;
assert(foo(5) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("using 4", "[statement][using]")
{
    static constexpr const char* Src = R"tl(
ns = {
    nested = {
        foo = function(x) { return x; }
    },
    bar = function(x) { return x * 2; }
};

using ns.nested;

assert(foo(5) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("using 5", "[statement][using]")
{
    static constexpr const char* Src = R"tl(
ns = {
    nested = {
        foo = function(x) { return x; }
    },
    bar = function(x) { return x * 2; }
};

using n = ns;

assert(n.nested.foo(5) == 5);
assert(n.bar(5) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("using single function", "[statement][using]")
{
    static constexpr const char* Src = R"tl(
ns = {
    nested = {
        foo = function(x) { return x; }
    },
};

using ns.nested.foo;
assert(foo(5) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("using single function alias", "[statement][using]")
{
    static constexpr const char* Src = R"tl(
ns = {
    nested = {
        foo = function(x) { return x; }
    },
};

using foo2 = ns.nested.foo;

assert(foo2(5) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Assignment invalid LHS", "[statement][assignment]")
{
    static constexpr const char* Src = R"tl(
foo = function() {};

foo() = "foo";
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::TypeMismatch));
}

TEST_CASE("defer", "[statement]")
{
    static constexpr const char* Src = R"tl(
d1 = false;
d2 = false;
{
    defer {
        assert(d2);
        d1 = true;
    }
    defer {
        assert(!d1);
        d2 = true;
    }

}
assert(d1);
assert(d2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("defer 2", "[statement]")
{
    static constexpr const char* Src = R"tl(
{
    defer {
        // x is defined bellow.
        // Since this is executed when leaving the scope we have access to it.
        assert(x == 2);
    }

    x = 2;
}
)tl";
    REQUIRE(Success(Src));
}
