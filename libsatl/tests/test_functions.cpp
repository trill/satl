/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include "test_run.h"

TEST_CASE("Function call", "[function][variable][script]")
{
    static constexpr const char* Src = R"tl(
num = 5;
// n must be passed by value and can not be modified by fact()
function func(n)
{
    ++n;
    return n;
}

for (n = 0; n < num; ++n)
{
    for (k = 0; k <= n; ++k)
    {
        prev_n = n;
        term = func(n);
        assert(term == n + 1);
        assert(prev_n == n);
    }
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function variable", "[function][variable][script]")
{
    static constexpr const char* Src = R"tl(
function add(a, b)
{
    return a + b;
}

addFunc = add;
sum = addFunc(2, 3);
assert(sum == 5);

// Native function
printFunc = print;
printFunc("print", "func");

func = function(a, b)
{
    return a + b;
};

assert(func(3, 4) == 7);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function as argument", "[function][argument][script]")
{
    static constexpr const char* Src = R"tl(
function sub(a, b)
{
    return a - b;
}

function doFunc(func, a, b)
{
    return func(a, b);
}

sum = doFunc(sub, 2, 3);
assert(sum == -1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function as argument 2", "[function][argument][script]")
{
    static constexpr const char* Src = R"tl(
function doFunc(func, a, b)
{
    return func(a, b);
}

result = doFunc(function(a, b) { return a + b; }, 1, 2);
assert(result == 3);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function lexical scoping", "[function][script]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    x = 5;
    return a + b + x;
}

result = foo(1, 2);
assert(result == 8);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function lexical scoping 2", "[function][script]")
{
    static constexpr const char* Src = R"tl(
function sortbygrade(names, grades)
{
    std.sort(names, function (n1, n2)
    {
        return grades[n1] < grades[n2];
    });
}

names = {"Peter", "Paul", "Mary"};
grades = {Mary = 10, Paul = 7, Peter = 8};

sortbygrade(names, grades);
assert(names[0] == "Paul");
assert(names[1] == "Peter");
assert(names[2] == "Mary");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function table member", "[function][table][member][script]")
{
    static constexpr const char* Src = R"tl(
tab = {};
tab.a = function(a) {
    return a;
};
assert(tab.a("test") == "test");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function table member this", "[function][table][member][this][script]")
{
    static constexpr const char* Src = R"tl(
tab = {};
tab.a = function(a) {
    return a * this.b;
};
tab.b = 5;
assert(tab.a(2) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function table member this 2", "[function][member][script]")
{
    static constexpr const char* Src = R"tl(
function afunc(a) {
    // As a table member this function has a this pointer
    return a * this.b;
}

tab = {};
tab.a = afunc;
tab.b = 5;
assert(tab.a(2) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function table member this inline", "[function][member][script]")
{
    static constexpr const char* Src = R"tl(
const tab = {
    a = function(a)
    {
        return a * this.b;
    },
    b = 5
};
assert(tab.a(2) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function table member this inline2", "[function][member][table][script]")
{
    static constexpr const char* Src = R"tl(
const tab = {
    a = function(a)
    {
        return a * this.b;
    },
    b = 5
}
func = tab["a"];
assert(func(2) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function table member this copy", "[function][member][table][std_lib][script]")
{
    static constexpr const char* Src = R"tl(
tab = {
    a = function(a)
    {
        return a * this.b;
    },
    b = 5
};
// Create a copy and change b, so table can be used a class for table2
table2 = copy(tab);
table2.b = 10;
assert(table2.a(5) == 50);
assert(tab.a(2) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Table inheritance", "[function][inheritance][script]")
{
    static constexpr const char* Src = R"tl(
tab = {
    a = function(a)
    {
        return a * this.b;
    },
    b = 5
};
// Create a copy and change b, so table can be used a class for table2
table2 = copy(tab);
table2.b = 10;
// Add a new member
table2.c = 3;
table2.d = function(a)
{
    return this.a(a) + this.c;
};
assert(table2.d(5) == 53);
assert(tab.a(2) == 10);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Scope lex", "[function][scope]")
{
    static constexpr const char* Src = R"tl(
x = "outside";

function probe()
{
    x = "inside";
    return x;
}

assert(x == "outside");
assert(probe() == "inside");

)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Scope lex 2", "[function][scope]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    x = 5;
    return a + b + x;
}

result = foo(1, 2);
assert(result == 8);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Scope lex 3", "[function][scope]")
{
    static constexpr const char* Src = R"tl(
z = 5;
function foo(a, b)
{
    x = 5;
    return a + b + x + z;
}

result = foo(1, 2);
assert(result == 13);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Scope lex 4", "[function][scope]")
{
    static constexpr const char* Src = R"tl(
x = 1;
function foo(a, b)
{
    x = 5;
    return a + b + x;
}

result = foo(1, 2);
assert(result == 8);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Scope lex 5", "[function][scope]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    x = 5;
    return a + b + x;
}

function bar(a, b)
{
    // foo's x can't overwrite this x (function boundary)
    // Note: These functions do not have stacks
    x = 9;
    return foo(a, b);
}

result = bar(1, 2);
assert(result == 8);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Scope lex 6", "[function][scope]")
{
    static constexpr const char* Src = R"tl(
function foo(a, b)
{
    x = 5;
    return a + b + x;
}

function bar(a, b)
{
    // Can't read 'x' from foo()
    println(x);
    return foo(a, b);
}

result = bar(1, 2);
assert(result == 8);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::UndefinedIdentifier));
}

TEST_CASE("Missing return", "[function]")
{
    static constexpr const char* Src = R"tl(
function t() : string
{
    i = 1;
}
x = t();
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::TypeMismatch));
}

TEST_CASE("Capture", "[function]")
{
    static constexpr const char* Src = R"tl(
function get_foo()
{
    i = 0;
    function foo[i](a, b)
    {
        ++i;
        return a + b + i;
    };
    // Return a function
    return foo;
}
foo = get_foo();
assert(foo(1, 2) == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Capture 2", "[function]")
{
    static constexpr const char* Src = R"tl(
function get_foo() : function
{
    i = 0;
    // Return a function expression
    return function[i](a, b)
    {
        ++i;
        return a + b + i;
    };
}
foo = get_foo();
assert(foo(1, 2) == 4);
assert(foo(1, 2) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Closure argument 2", "[function]")
{
    static constexpr const char* Src = R"tl(
function do_func(func, a, b)
{
    return func(a, b);
}

function get_foo() : function
{
    i = 0;
    // Return a function expression
    return function[i](a, b)
    {
        ++i;
        return a + b + i;
    };
}
assert(do_func(get_foo(), 1, 2) == 4);
// This creates a new function object with a new i
assert(do_func(get_foo(), 1, 2) == 4);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Closure argument 3", "[function]")
{
    static constexpr const char* Src = R"tl(
function do_func(func, a, b)
{
    return func(a, b);
}

function get_foo() : function
{
    i = 0;
    // Return a function expression
    return function[i](a, b)
    {
        ++i;
        return a + b + i;
    };
}

foo = get_foo();
assert(do_func(foo, 1, 2) == 4);
// This uses the same function so it increases i
assert(do_func(foo, 1, 2) == 5);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Inline Closure argument", "[function]")
{
    static constexpr const char* Src = R"tl(
function do_func(func, a, b)
{
    return func(a, b);
}

i = 0;

assert(do_func(function[i](a, b) {
    ++i;
    return a + b + i;
}, 1, 2) == 4);

assert(i == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Empty capture fail", "[function]")
{
    static constexpr const char* Src = R"tl(
function get_foo()
{
    i = 0;
    return function[](a, b)
    {
        ++i;
        return a + b + i;
    };
}
foo = get_foo();
assert(foo(1, 2) == 4);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::UndefinedIdentifier));
}

TEST_CASE("Wrong capture fail", "[function]")
{
    static constexpr const char* Src = R"tl(
function get_foo()
{
    i = 0;
    return function[x](a, b)
    {
        return a + b;
    };
}
foo = get_foo();
assert(foo(1, 2) == 2);
)tl";
    REQUIRE(Fail(Src, sa::tl::Error::Code::UndefinedIdentifier));
}

TEST_CASE("Function expression followed by call expression", "[function]")
{
    static constexpr const char* Src = R"tl(
hello = function()
{
    return "Hello";
}();
assert(hello == "Hello");
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function expression followed by call expression 2", "[function]")
{
    static constexpr const char* Src = R"tl(
arg2x = function(arg)
{
    return arg * 2;
}(1);
assert(arg2x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function expression followed by call expression capture", "[function]")
{
    static constexpr const char* Src = R"tl(
mul = 2;
arg2x = function[mul](arg)
{
    return arg * mul;
}(1);
assert(arg2x == 2);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Immediate function call no assignment", "[function]")
{
    static constexpr const char* Src = R"tl(
called = false;
function[called]()
{
    called = true;
}();
assert(called);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function expression no assignment", "[function]")
{
    static constexpr const char* Src = R"tl(
// Doesn't make sense, because you can not call it, but it's not a syntax error
function()
{
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function expression no assignment no parens", "[function]")
{
    static constexpr const char* Src = R"tl(
// Doesn't make sense, because you can not call it, but it's not a syntax error
function
{
}
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function optional parens", "[function]")
{
    static constexpr const char* Src = R"tl(
succ = false;
function x
{
    succ = true;
}

x();
assert(succ);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function optional parens type hint", "[function]")
{
    static constexpr const char* Src = R"tl(
succ = false;
function x : bool
{
    succ = true;
    return true;
}

assert(x());
assert(succ);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function expression optional parens", "[function]")
{
    static constexpr const char* Src = R"tl(
succ = false;
x = function
{
    succ = true;
};

x();
assert(succ);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Function expression optional parens type hint", "[function]")
{
    static constexpr const char* Src = R"tl(
succ = false;
x = function : bool
{
    succ = true;
    return true;
};

assert(x());
assert(succ);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member function boundary", "[function]")
{
    // This code doesn't make sense but must execute without errors.
    // See samples/bench/matmul.tl for code that makes more sense.
    static constexpr const char* Src = R"tl(
matrix = {
    T = function()
    {
        const y = matrix();
        return y;
    },
    operator* = function(rhs)
    {
        y = matrix();
        const c = matrix.T();
        y.a = 1;
        return y;
    },

    a = 0
};

m1 = matrix();
m2 = matrix();
m3 = m1 * m2;
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member function no variable access", "[function]")
{
    static constexpr const char* Src = R"tl(
x = 1;
t = {
    foo = function
    {
        // This creates a new variable
        x = 0;
        return this;
    }
};
t.foo();
assert(x == 1);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Member function captured variable access", "[function]")
{
    static constexpr const char* Src = R"tl(
x = 1;
t = {
    foo = function[x]
    {
        // Set x in line 1 to 0
        x = 0;
        return this;
    }
};
t.foo();
assert(x == 0);
)tl";
    REQUIRE(Success(Src));
}

TEST_CASE("Forward declare", "[function]")
{
    static constexpr const char* Src = R"tl(
foo = null;

function bar
{
    foo();
}

called = false;
foo = function
{
    called = true;
};

bar();
assert(called);
)tl";
    REQUIRE(Success(Src));
}
