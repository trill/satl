/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "value.h"
#include "node.h"
#include "exceptions.h"
#include <format>

namespace sa::tl {

// These are called when converting script function arguments to arguments passed to native functions.
// They may throw an excection to bail out as soon as possible.
template<typename T>
inline T Convert(const ValuePtr& value)
{
    if constexpr (std::is_same_v<T, const ValuePtr&>)
    {
        return value;
    }
    else
    {
        if (value->IsConvertible<T>())
            return value->ConvertTo<T>();
        throw TypeMismatch(std::format("Can not convert {} to {}", value->GetTypeName(), TypeName<T>::Get()));
    }
}

template<>
inline std::string_view Convert(const ValuePtr& value)
{
    if (value->Is<StringPtr>())
    {
        if (const auto& val = value->As<StringPtr>(); !!val)
            return *val;
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to string", value->GetTypeName()));
}
template<>
inline const std::string& Convert(const ValuePtr& value)
{
    if (value->Is<StringPtr>())
    {
        if (const auto& val = value->As<StringPtr>(); !!val)
            return *val;
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to string", value->GetTypeName()));
}
template<>
inline const StringPtr& Convert(const ValuePtr& value)
{
    if (value->Is<StringPtr>())
    {
        if (const auto& val = value->As<StringPtr>(); !!val)
            return val;
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to string", value->GetTypeName()));
}
template<>
inline const char* Convert(const ValuePtr& value)
{
    if (value->Is<StringPtr>())
    {
        if (const auto& val = value->As<StringPtr>(); !!val)
            return val->c_str();
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to string", value->GetTypeName()));
}
template<>
inline Value Convert(const ValuePtr& value)
{
    if (!value)
        throw NullPtrDereference("The value is null");
    return *value;
}
template<>
inline const Value& Convert(const ValuePtr& value)
{
    if (!value)
        throw NullPtrDereference("The value is null");
    return *value;
}
template<>
inline ValuePtr Convert(const ValuePtr& value)
{
    return value;
}
template<>
inline const Callable& Convert(const ValuePtr& value)
{
    if (value->Is<CallablePtr>())
    {
        if (const auto& val = value->As<CallablePtr>(); !!val)
            return *val;
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to callable", value->GetTypeName()));
}
template<>
inline const TablePtr& Convert(const ValuePtr& value)
{
    if (value->Is<TablePtr>())
        return value->As<TablePtr>();
    throw TypeMismatch(std::format("Can not convert {} to table", value->GetTypeName()));
}
template<>
inline const Table& Convert(const ValuePtr& value)
{
    if (value->Is<TablePtr>())
    {
        if (const auto& val = value->As<TablePtr>(); !!val)
            return *val;
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to table", value->GetTypeName()));
}

template<>
inline const ObjectPtr& Convert(const ValuePtr& value)
{
    if (value->Is<ObjectPtr>())
        return value->As<ObjectPtr>();
    throw TypeMismatch(std::format("Can not convert {} to object", value->GetTypeName()));
}
template<>
inline const Object& Convert(const ValuePtr& value)
{
    if (value->Is<ObjectPtr>())
    {
        if (const auto& val = value->As<ObjectPtr>(); !!val)
            return *val;
        throw NullPtrDereference("The value is null");
    }
    throw TypeMismatch(std::format("Can not convert {} to object", value->GetTypeName()));
}

}
