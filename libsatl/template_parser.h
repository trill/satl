/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include <string_view>

namespace sa::tl::templ {

class Parser;

struct Token
{
    enum class Type
    {
        Invalid,
        String,
        Variable,
        Quote
    };

    Type type;
    size_t start;
    size_t end;
    std::string value;
};

class Tokens
{
    friend class Parser;
private:
    std::vector<Token> tokens_;
    size_t reserve_{ 256 };
public:
    [[nodiscard]] bool IsEmpty() const { return tokens_.empty(); }
    template<typename Callback>
    std::string ToString(Callback&& callback) const
    {
        std::string result;
        result.reserve(reserve_ + tokens_.size() * 10);
        for (const auto& token : tokens_)
        {
            switch (token.type)
            {
            case Token::Type::Variable:
            case Token::Type::Quote:
                result.append(callback(token));
                break;
            case Token::Type::String:
                result.append(token.value);
                break;
            default:
                break;
            }
        }
        return result;
    }
};

class Parser
{
private:
    size_t index_{ 0 };
    char quote_{ '\0' };
    bool inQuote_{ false };
    std::string_view source_;
    int openCurly_{ 0 };
    [[nodiscard]] bool Eof() const { return index_ >= source_.size(); }
    char Next() { return source_.at(index_++); }
    [[nodiscard]] char Peek(size_t offset = 1) const
    {
        if (source_.size() > index_ + offset)
            return source_.at(index_ + offset);
        return 0;
    }
    Token GetNextToken();
    void Reset()
    {
        index_ = 0;
        quote_ = '\0';
        inQuote_ = false;
    }
public:
    Tokens Parse(std::string_view source);
    void Append(std::string_view source, Tokens& tokens);
    template<typename Callback>
    static std::string Evaluate(std::string_view source, Callback&& callback)
    {
        Parser parser;
        const Tokens tokens = parser.Parse(source);
        return tokens.ToString(std::forward<Callback>(callback));
    }
    [[nodiscard]] bool HasError() const { return openCurly_ != 0; }
    bool quotesSupport_{ false };
};

}

