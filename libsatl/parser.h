/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <variant>
#include <format>
#include "error.h"
#include "lexer.h"
#include "type_hint.h"

namespace sa::tl {

#define SYNTAX_ERROR(loc, message)      \
    do {                                \
        SyntaxError(loc, message);      \
        return {};                      \
    } while (0)

#define ENUMERATE_KEYWORDS                         \
    ENUM_KEYWORD(Break, "break")                   \
    ENUM_KEYWORD(Case, "case")                     \
    ENUM_KEYWORD(Catch, "catch")                   \
    ENUM_KEYWORD(Const, "const")                   \
    ENUM_KEYWORD(Constructor, KEYWORD_CONSTRUCTOR) \
    ENUM_KEYWORD(Continue, "continue")             \
    ENUM_KEYWORD(Default, "default")               \
    ENUM_KEYWORD(Defer, "defer")                   \
    ENUM_KEYWORD(Do, "do")                         \
    ENUM_KEYWORD(Else, "else")                     \
    ENUM_KEYWORD(Fallthrough, "fallthrough")       \
    ENUM_KEYWORD(Finally, "finally")               \
    ENUM_KEYWORD(For, "for")                       \
    ENUM_KEYWORD(ForEach, "foreach")               \
    ENUM_KEYWORD(Function, "function")             \
    ENUM_KEYWORD(Global, "global")                 \
    ENUM_KEYWORD(If, "if")                         \
    ENUM_KEYWORD(In, "in")                         \
    ENUM_KEYWORD(Include, "include")               \
    ENUM_KEYWORD(Once, "once")                     \
    ENUM_KEYWORD(Operator, "operator")             \
    ENUM_KEYWORD(Return, "return")                 \
    ENUM_KEYWORD(Switch, "switch")                 \
    ENUM_KEYWORD(Try, "try")                       \
    ENUM_KEYWORD(Using, "using")                   \
    ENUM_KEYWORD(While, "while")

template<typename T, typename... Args>
inline std::shared_ptr<T> CreateNode(Location loc, Args&&... Arguments)
{
    auto t = std::make_shared<T>(std::forward<Args>(Arguments)...);
    std::swap(t->loc_, loc);
    return t;
};

class Parser
{
public:
    explicit Parser(std::string_view in);
    Parser(std::string_view in, const std::string& file);
    ~Parser();
    // Parse the source and return the root node
    NodePtr Parse();
    // Parse a single expression
    NodePtr ParseExpression();
    NodePtr ParseTable(bool safe);
    void SyntaxError(Location loc, std::string message)
    {
        errors_.push_back({ Error::Type::SyntaxError,
            Error::Code::SyntaxError,
            std::forward<Location>(loc),
            std::forward<std::string>(message),
            {} });
    }
    [[nodiscard]] const std::vector<Token>& GetComments() const { return comments_; }
    // All syntax errors
    [[nodiscard]] const std::vector<Error>& GetErrors() const { return errors_; }
    // Return either the parsed AST as NodePtr or the string contents of the file.
    std::function<std::variant<std::string, NodePtr>(std::string& filename, const Location& loc)> onInclude_;

private:
    struct Context
    {
        bool safeTables = false;
        std::vector<std::string> includes;
    };
    bool Lex();
    NodePtr InternalParse(Context& ctx);
    Token PreviousToken(const TokenIterator& it);
    enum class Keyword
    {
        None,
#define ENUM_KEYWORD(c, w) \
        c,
        ENUMERATE_KEYWORDS
#undef ENUM_KEYWORD
    };

    static Keyword GetKeyword(const Token& token)
    {
        if (token.type != Token::Type::Ident)
            return Keyword::None;
#define ENUM_KEYWORD(c, w) \
        if (token.value == w) \
            return Keyword::c;
        ENUMERATE_KEYWORDS
#undef ENUM_KEYWORD
        return Keyword::None;
    }
    static std::string_view GetKeywordName(Keyword kw)
    {
        if (kw == Keyword::None)
            return "";
        switch (kw)
        {
#define ENUM_KEYWORD(c, w) \
        case Keyword::c: \
            return w;
        ENUMERATE_KEYWORDS
#undef ENUM_KEYWORD
        default:
            return "";
        }
    }
    TypeHint GetTypeHint(const Token& token)
    {
        if (token.value == "bool")
            return TypeHint::Bool;
        if (token.value == "int")
            return TypeHint::Int;
        if (token.value == "float")
            return TypeHint::Float;
        if (token.value == "string")
            return TypeHint::String;
        if (token.value == "table")
            return TypeHint::Table;
        if (token.value == "function")
            return TypeHint::Function;
        if (token.value == "object")
            return TypeHint::Object;
        if (token.value == "any")
            return TypeHint::Any;
        SyntaxError(token.loc, std::format("Unknown type hint \"{}\"", token.value));
        return TypeHint::Any;
    }
    static bool IsOperatorSymbol(const Token& token)
    {
        // clang-format off
        return token.type == Token::Type::Amp
            || token.type == Token::Type::AssignAdd
            || token.type == Token::Type::AssignDiv
            || token.type == Token::Type::AssignIDiv
            || token.type == Token::Type::AssignMod
            || token.type == Token::Type::AssignMul
            || token.type == Token::Type::AssignOr
            || token.type == Token::Type::AssignPower
            || token.type == Token::Type::AssignShl
            || token.type == Token::Type::AssignShr
            || token.type == Token::Type::AssignSub
            || token.type == Token::Type::AssignXor
            || token.type == Token::Type::Asterisk
            || token.type == Token::Type::At
            || token.type == Token::Type::BackSlash
            || token.type == Token::Type::BracketClose
            || token.type == Token::Type::BracketOpen
            || token.type == Token::Type::CompEquals
            || token.type == Token::Type::CompNot
            || token.type == Token::Type::Decrement
            || token.type == Token::Type::Dollar
            || token.type == Token::Type::DoubleDollar
            || token.type == Token::Type::DoubleAsterisk
            || token.type == Token::Type::Excl
            || token.type == Token::Type::Gt
            || token.type == Token::Type::GtEq
            || token.type == Token::Type::Hash
            || token.type == Token::Type::Increment
            || token.type == Token::Type::LogicalAnd
            || token.type == Token::Type::LogicalOr
            || token.type == Token::Type::Lt
            || token.type == Token::Type::LtEq
            || token.type == Token::Type::Minus
            || token.type == Token::Type::ParenClose
            || token.type == Token::Type::ParenOpen
            || token.type == Token::Type::Percent
            || token.type == Token::Type::Pipe
            || token.type == Token::Type::Plus
            || token.type == Token::Type::Shl
            || token.type == Token::Type::Slash
            || token.type == Token::Type::StrictEq
            || token.type == Token::Type::StrictUneq
            || token.type == Token::Type::Tilde;
        // clang-format on
    }
    static bool IsOperatorKeyword(const Token& token)
    {
        return GetKeyword(token) == Keyword::In;
    }
    static bool IsOperator(const Token& token)
    {
        return IsOperatorSymbol(token) || IsOperatorKeyword(token);
    }
    static bool IsIdent(const Token& token)
    {
        return token.type == Token::Type::Ident && GetKeyword(token) == Keyword::None && token.value != LITERAL_FALSE
            && token.value != LITERAL_TRUE && token.value != LITERAL_NULL;
    }
    static bool IsValue(const Token& token);
    static bool IsUnaryOperator(const std::string& name);
    static int GetOperatorParamCount(const std::string& name);
    static bool IsAssignment(const TokenIterator& it);
    static std::string CollectStrings(TokenIterator& it, Token::Type type);

    ValuePtr ParseValueOrIdent(TokenIterator& it);
    NodePtr ParseStatement(TokenIterator& it);
    NodePtr ParseIf(TokenIterator& it);
    NodePtr ParseReturn(TokenIterator& it);
    NodePtr ParseBreak(TokenIterator& it);
    NodePtr ParseFallthrough(TokenIterator& it);
    NodePtr ParseContinue(TokenIterator& it);
    NodePtr ParseSwitch(TokenIterator& it);
    NodePtr ParseFor(TokenIterator& it);
    NodePtr ParseForEach(TokenIterator& it);
    NodePtr ParseWhile(TokenIterator& it);
    NodePtr ParseDo(TokenIterator& it);
    NodePtr ParseFunction(TokenIterator& it, int numParams = -1);
    NodePtr ParseBlock(TokenIterator& it, bool requireCurly);
    NodePtr ParseConstAssign(TokenIterator& it, bool isGlobal = false);
    NodePtr ParseGlobalAssign(TokenIterator& it);
    NodePtr ParseTableInit(TokenIterator& it);
    NodePtr ParseInclude(TokenIterator& it);
    NodePtr ParseTry(TokenIterator& it);
    NodePtr ParseUsing(TokenIterator& it);
    NodePtr ParseDefer(TokenIterator& it);
    NodePtr ParseVariableDefinition(TokenIterator& it, bool typeHint = true);

    // Expressions
    NodePtr ParseExpression(TokenIterator& it);
    NodePtr ParseTernaryOrAssignExpression(TokenIterator& it);
    NodePtr ParseRangeExpression(TokenIterator& it);
    NodePtr ParseLogicalOrExpression(TokenIterator& it);
    NodePtr ParseLogicalAndExpression(TokenIterator& it);
    NodePtr ParseBitwiseOrExpression(TokenIterator& it);
    NodePtr ParseBitwiseXorExpression(TokenIterator& it);
    NodePtr ParseBitwiseAndExpression(TokenIterator& it);
    NodePtr ParseEqualityExpression(TokenIterator& it);
    NodePtr ParseRelationalExpression(TokenIterator& it);
    NodePtr ParseShiftExpression(TokenIterator& it);
    NodePtr ParseSumExpression(TokenIterator& it);
    NodePtr ParseProductExpression(TokenIterator& it);
    NodePtr ParsePowerExpression(TokenIterator& it);
    NodePtr ParseUnaryExpression(TokenIterator& it);
    NodePtr ParseTermExpression(TokenIterator& it);
    NodePtr ParseCallExpression(TokenIterator& it, NodePtr symbol);
    NodePtr ParseValueExpression(TokenIterator& it);
    template<typename T>
    NodePtr ParseComputedMembers(TokenIterator& it, NodePtr owner);
    NodePtr ParseMemberAccess(TokenIterator& it, NodePtr owner);
    NodePtr ParseMemberAccessChain(TokenIterator& it, NodePtr owner);
    NodePtr ParsePostOperators(TokenIterator& it, NodePtr owner);
    NodePtr ParseMemberCall(TokenIterator& it, NodePtr owner);

    bool Expect(const Token& token, Token::Type type)
    {
        if (token.type != type)
        {
            SyntaxError(token.loc, std::format("Expecting {}", TokenTypeName(type)));
            return false;
        }
        return true;
    }
    bool Expect(const Token& token, Keyword keyword)
    {
        if (GetKeyword(token) != keyword)
        {
            SyntaxError(token.loc, std::format("Expecting \"{}\"", GetKeywordName(keyword)));
            return false;
        }
        return true;
    }
    bool ExpectIdent(const Token& token)
    {
        if (!Expect(token, Token::Type::Ident))
            return false;
        if (!IsIdent(token))
        {
            SyntaxError(token.loc, "Identifier expected");
            return false;
        }
        return true;
    }
    bool ExptectValueOrIdent(const Token& token)
    {
        if (IsValue(token))
            return true;
        return ExpectIdent(token);
    }
    bool HasErrors() const { return !errors_.empty(); }
    bool HasInclude(const std::string& filename) const;
    Lexer lexer_;
    std::deque<Token> tokens_;
    std::vector<Token> comments_;
    std::vector<Error> errors_;
    struct
    {
        int returnable = 0;
        int breakable = 0;
        int continueable = 0;
        int fallthroughable = 0;
        int catchable = 0;
    } state_;
    Context* ctx_{ nullptr };
};

}
