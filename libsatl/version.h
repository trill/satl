/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#define SATL_VERSION_MAJOR 1
#define SATL_VERSION_MINOR 12

#define SATL_VERSION_CREATE(major, minor) (((major) << 8) | (minor))
#define SATL_VERSION_STR(a) #a
#define SATL_VERSION_STR1(a) SATL_VERSION_STR(a)

#define SATL_VERSION SATL_VERSION_CREATE(SATL_VERSION_MAJOR, SATL_VERSION_MINOR)
#define SATL_VERSION_STRING SATL_VERSION_STR1(SATL_VERSION_MAJOR) "." SATL_VERSION_STR1(SATL_VERSION_MINOR)
