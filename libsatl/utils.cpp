/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "utils.h"
#if __has_include(<wordexp.h>)
// Linux only
#include <wordexp.h>
#define HAVE_WORDEXP
#endif
#include <fstream>
#include <filesystem>

namespace sa::tl {

bool FileExists(const std::string& name)
{
    if (DirExists(name))
        return false;
    std::ifstream infile(name);
    return infile.good();
}

bool DirExists(const std::string& name)
{
    namespace fs = std::filesystem;
    std::error_code err;
    fs::path p(name);
    bool isDir = fs::is_directory(p, err);
    if (err)
        return false;
    return isDir;
}

bool HiddenFile(const std::string& name)
{
    auto fn = ExtractFileName(name);
    if (fn == "." || fn == "..")
        return false;
    return fn.starts_with('.');
}

std::string ExtractFileName(const std::string& name)
{
    namespace fs = std::filesystem;
    fs::path p(name);
    return p.filename();
}

std::string ExtractFileExt(const std::string& name)
{
    namespace fs = std::filesystem;
    fs::path p(name);
    return p.extension();
}

std::string ChangeFileExt(const std::string& name, const std::string& ext)
{
    namespace fs = std::filesystem;
    fs::path p(name);
    if (!ext.empty())
        p.replace_extension(ext);
    else
        p.replace_extension();
    return p.string();
}

std::string ParentPath(const std::string& name)
{
    namespace fs = std::filesystem;
    fs::path p(name);
    return p.parent_path();
}

std::string AddSlash(const std::string& name)
{
    if (name.empty())
        return "/";
    if (name.ends_with('/'))
        return name;
    return name + "/";
}

std::string ConcatPath(const std::string& path, const std::string& name)
{
    std::string result = path;
    if (result.back() != '/' && result.back() != '\\' && name.front() != '/' && name.front() != '\\')
        result += '/';
    result += name;
    return result;
}

std::string FindRelatedFile(const std::string& base, const std::string& name)
{
    if (base.empty())
        return name;
    if (name.starts_with("file:///"))
        return name;
    if (std::filesystem::is_regular_file(name))
        return name;

    auto basePath = ParentPath(base);
    return ConcatPath(basePath, name);
}

std::string ReadScriptFile(const std::string& filename)
{
    std::string fn = filename;
    if (fn.starts_with("file:///"))
        fn.erase(0, 7);
    std::ifstream f(fn);
    if (!f.is_open())
        return "";

    std::string line;
    std::stringstream ss;
    while (std::getline(f, line))
    {
        ss << line << '\n';
    }
    f.close();
    return ss.str();
}

bool IsWhite(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isspace(*it)) ++it;
    return !s.empty() && it == s.end();
}

std::vector<std::string> Split(const std::string& str, const std::string& delim, bool keepEmpty, bool keepWhite)
{
    std::vector<std::string> result;

    size_t beg = 0;
    for (size_t end = 0; (end = str.find(delim, end)) != std::string::npos; ++end)
    {
        auto substring = str.substr(beg, end - beg);
        beg = end + delim.length();
        if (!keepEmpty && substring.empty())
            continue;
        if (!keepWhite && IsWhite(substring))
            continue;
        result.push_back(substring);
    }

    auto substring = str.substr(beg);
    if (substring.empty())
    {
        if (keepEmpty)
            result.push_back(substring);
    }
    else if (IsWhite(substring))
    {
        if (keepWhite)
            result.push_back(substring);
    }
    else
        result.push_back(substring);

    return result;
}

std::string ExpandPath(const char* path)
{
#ifdef HAVE_WORDEXP
    wordexp_t p;
    wordexp(path, &p, 0);
    std::string result;
    if (p.we_wordc != 0)
    {
        result = p.we_wordv[0];
    }
    wordfree(&p);
    return result;
#else
    const char* home = getenv("HOME");
    if (!home)
        return path;
    std::string result = path;
    ReplaceSubstring<char>(result, "~", std::string(home));
    return result;
#endif
}

}
