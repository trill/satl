/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>

namespace sa::tl {

template <typename T>
void VariadicVectorEmplace(std::vector<T>&) {}

template <typename T, typename First, typename... Args>
void VariadicVectorEmplace(std::vector<T>& v, First&& first, Args&&... args)
{
    v.push_back(MakeValue(std::forward<First>(first)));
    VariadicVectorEmplace(v, std::forward<Args>(args)...);
}

}
