/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <type_traits>
#include <functional>
#include "location.h"
#include "non_copyable.h"
#include "satl_assert.h"
#include "value.h"

namespace sa::tl {

class ScopeNew;

class ScopeID
{
    friend class ScopeNew;

public:
    [[nodiscard]] int Level() const { return level_; }
    [[nodiscard]] int Number() const { return number_; }

private:
    int number_{ 0 };
    int level_{ 0 };
};

class ScopeNew
{
    SATL_NON_COPYABLE(ScopeNew);
    SATL_NON_MOVEABLE(ScopeNew);
public:
    explicit ScopeNew(ScopeID& s) : scope_(s)
    {
        ++scope_.number_;
        ++scope_.level_;
    }
    ~ScopeNew() { --scope_.level_; }

private:
    ScopeID& scope_;
};

class Context;

#define ENUMERATE_NODE_TYPES                     \
    ENUMERATE_NODE_TYPE(Assign)                  \
    ENUMERATE_NODE_TYPE(BinaryExpr)              \
    ENUMERATE_NODE_TYPE(Block)                   \
    ENUMERATE_NODE_TYPE(Break)                   \
    ENUMERATE_NODE_TYPE(CallExpr)                \
    ENUMERATE_NODE_TYPE(CompoundAssign)          \
    ENUMERATE_NODE_TYPE(ConditionalMemberAccess) \
    ENUMERATE_NODE_TYPE(Continue)                \
    ENUMERATE_NODE_TYPE(Do)                      \
    ENUMERATE_NODE_TYPE(Defer)                   \
    ENUMERATE_NODE_TYPE(Fallthrough)             \
    ENUMERATE_NODE_TYPE(For)                     \
    ENUMERATE_NODE_TYPE(ForEach)                 \
    ENUMERATE_NODE_TYPE(Function)                \
    ENUMERATE_NODE_TYPE(If)                      \
    ENUMERATE_NODE_TYPE(MemberAccess)            \
    ENUMERATE_NODE_TYPE(MemberFunction)          \
    ENUMERATE_NODE_TYPE(NativeFunction)          \
    ENUMERATE_NODE_TYPE(Range)                   \
    ENUMERATE_NODE_TYPE(Return)                  \
    ENUMERATE_NODE_TYPE(Root)                    \
    ENUMERATE_NODE_TYPE(Switch)                  \
    ENUMERATE_NODE_TYPE(TableInitialization)     \
    ENUMERATE_NODE_TYPE(TernaryExpr)             \
    ENUMERATE_NODE_TYPE(Try)                     \
    ENUMERATE_NODE_TYPE(UnaryExpr)               \
    ENUMERATE_NODE_TYPE(Using)                   \
    ENUMERATE_NODE_TYPE(ValueExpr)               \
    ENUMERATE_NODE_TYPE(VariableDefinition)      \
    ENUMERATE_NODE_TYPE(VariableExpr)            \
    ENUMERATE_NODE_TYPE(While)

class Node : public std::enable_shared_from_this<Node>
{
    SATL_NON_COPYABLE(Node);
    SATL_NON_MOVEABLE(Node);
public:
    enum class Class
    {
#define ENUMERATE_NODE_TYPE(v) v,
        ENUMERATE_NODE_TYPES
#undef ENUMERATE_NODE_TYPE
    };
    virtual ~Node() = default;
    virtual ValuePtr Evaluate(Context&) const
    {
        return {};
    }
    virtual void Dump(std::ostream&, int) const
    {
    }
    virtual constexpr Class GetClass() const = 0;
    constexpr const char* GetClassName() const
    {
        switch (GetClass())
        {
#define ENUMERATE_NODE_TYPE(v) \
    case Class::v:             \
        return #v;
            ENUMERATE_NODE_TYPES
#undef ENUMERATE_NODE_TYPE
        }
        return "Unknown";
    }
    virtual std::string GetFriendlyName() const
    {
        return GetClassName();
    }
    virtual void ForEachChild(const std::function<void(const Node&, const ScopeID&)>&,
        ScopeID&,
        [[maybe_unused]] bool recursive = true) const {};
    friend std::ostream& operator<<(std::ostream& os, const Node& value)
    {
        os << value.loc_ << ": " << value.GetFriendlyName();
        return os;
    }

    Location loc_;

protected:
    constexpr Node() = default;
};

class Callable : public Node
{
public:
    virtual ValuePtr operator()(Context&, const FunctionArguments&) const = 0;
    virtual ValuePtr operator()(Context&, const Value&, const FunctionArguments&) const = 0;
    virtual constexpr int ParamCount() const = 0;
    virtual const std::string& GetName() const = 0;

protected:
    void Enter(Context& ctx) const;
    void Leave(Context& ctx) const;
    static bool ExecutionAllowed(Context& ctx, bool safe);
    static void RuntimeError(Context& ctx, int code, std::string message);
};

#define CALLABLE_CALLGUARD(ctx) \
    Enter(ctx);                 \
    ScopeGuard _callguard([&] { Leave(ctx); })

template<typename T>
constexpr bool Is(const Node&)
{
    static_assert(std::is_base_of<Node, T>::value);
    return false;
}

/// Returns false when obj is null
template<typename T>
constexpr bool Is(const Node* obj)
{
    static_assert(std::is_base_of<Node, T>::value);
    return obj && Is<T>(*obj);
}

template<>
constexpr bool Is<Node>(const Node&)
{
    return true;
}

template<typename T>
inline const T& To(const Node& obj)
{
    static_assert(std::is_base_of<Node, T>::value);
    SATL_ASSERT(Is<T>(obj));
    return static_cast<const T&>(obj);
}

template<typename T>
inline T& To(Node& obj)
{
    static_assert(std::is_base_of<Node, T>::value);
    SATL_ASSERT(Is<T>(obj));
    return static_cast<T&>(obj);
}

template<typename T>
inline const T* To(const Node* obj)
{
    static_assert(std::is_base_of<Node, T>::value);
    if (!Is<T>(obj))
        return nullptr;
    return static_cast<const T*>(obj);
}

template<typename T>
inline T* To(Node* obj)
{
    static_assert(std::is_base_of<Node, T>::value);
    if (!Is<T>(obj))
        return nullptr;
    return static_cast<T*>(obj);
}

template<>
constexpr bool Is<Callable>(const Node& obj)
{
    return obj.GetClass() == Node::Class::Function || obj.GetClass() == Node::Class::NativeFunction
        || obj.GetClass() == Node::Class::MemberFunction;
}

}
