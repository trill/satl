/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "module.h"

namespace sa::tl {

Module::Module() = default;

void Module::AddFunction(std::string name, std::function<NativeFunctionSignature>&& func, bool safe)
{
    Add(std::move(name), MakeConstValue(std::make_shared<NativeFunctionImpl>(name, std::move(func), safe)));
}

}
