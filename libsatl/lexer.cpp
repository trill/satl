/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "lexer.h"
#include "location.h"
#include "utf8.h"
#include "number_parser.h"
#include <limits>
#include <format>

namespace sa::tl {

Token Lexer::Next()
{
    if (pos_ == 0)
    {
        SkipUTF8BOM();
        // Shebang must be the very first thing
        if (IsShebang())
        {
            Consume();
            do
            {
                Consume();
            } while (!IsLineEnd() && !IsEof());
        }
    }
    while (true)
    {
        if (IsWhite())
        {
            do
            {
                Consume();
            } while (IsWhite());
        }
        else
            break;
    }

    Token token;
    token.loc = { .file = file_, .line = line_, .col = (size_t)col_ };

    if (IsLineComment())
        ParseLineComment(token);
    else if (IsBlockCommentStart())
        ParseBlockComment(token);
    else if (IsRawString())
        ParseRawString(token);
    else if (IsLineToken())
        ParseLineToken(token);
    else if (IsColToken())
        ParseColToken(token);
    else if (IsFileToken())
        ParseFileToken(token);
    else if (IsFirstIdentChar())
        ParseIdent(token);
    else if (IsSymbol())
        ParseSymbol(token);
    else if (IsDigit())
        ParseNumber(token);
    else if (IsQuote())
        ParseString(token);
    else if (IsEof())
        token.type = Token::Type::Eof;

    if (token.type == Token::Type::Invalid)
        Error(std::format("Invalid token \"{}\"", current_));
    return token;
}

void Lexer::SkipUTF8BOM()
{
    if (current_ == (char)0xEF && Peek(1) == (char)0xBB && Peek(2) == (char)0xBF)
    {
        Consume();
        Consume();
        Consume();
    }
}

void Lexer::Consume()
{
    ++pos_;
    if (IsEof())
    {
        current_ = '\0';
        return;
    }
    current_ = in_[pos_];
    ++col_;
    if (current_ == '\n')
    {
        // \n is a white space so sombebody calls Consume() again and col_ will be 0
        col_ = -1;
        ++line_;
    }
}

void Lexer::ParseLineComment(Token& token)
{
    size_t start = pos_;
    token.type = Token::Type::LineComment;
    Consume();
    do
    {
        Consume();
    } while (!IsLineEnd() && !IsEof());
    token.value = std::string(&in_[start], pos_ - start);
}

void Lexer::ParseBlockComment(Token& token)
{
    PositionStore ps(*this);
    size_t start = pos_;
    token.type = Token::Type::BlockComment;
    Consume();
    do
    {
        Consume();
    } while (!IsBlockCommentEnd() && !IsEof());
    if (!IsEof())
    {
        Consume();
        Consume();
    }
    else
    {
        ps.Restore();
        Error("Unterminated block comment");
    }
    token.value = std::string(&in_[start], pos_ - start);
}

void Lexer::ParseString(Token& token)
{
    const char quote = current_;
    PositionStore ps(*this);
    // Consume "
    Consume();
    token.type = quote == '`' ? Token::Type::TemplateString : Token::Type::String;
    if (current_ == quote)
    {
        // Empty string: ""
        Consume();
        return;
    }

    auto isHexDigit = [](char c) -> bool
    {
        return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
    };
    auto isOctDigit = [](char c) -> bool
    {
        return c >= '0' && c <= '7';
    };

    bool escaped = false;
    do
    {
        if (escaped)
        {
            switch (current_) {
            case '\'':
                token.value += '\'';
                break;
            case '`':
                if (quote == '`')
                {
                    token.value += '`';
                    break;
                }
                [[fallthrough]];
            case '\"':
                token.value += '\"';
                break;
            case '\?':
                token.value += '\?';
                break;
            case '\\':
                token.value += '\\';
                break;
            case 'a':
                // Just skip
                break;
            case 'b':
                token.value += '\b';
                break;
            case 'v':
                token.value += '\v';
                break;
            case 'f':
                token.value += '\f';
                break;
            case 'n':
                token.value += '\n';
                break;
            case 'r':
                token.value += '\r';
                break;
            case 't':
                token.value += '\t';
                break;
            case 'x':
            {
                if (isHexDigit(Peek(1)))
                {
                    char hex[3] = {};
                    Consume();
                    hex[0] = current_;
                    if (isHexDigit(Peek(1)))
                    {
                        Consume();
                        hex[1] = current_;
                    }
                    token.value += (char)std::stoi(hex, nullptr, 16);
                }
                else
                    Error("Expecting hex digits for escape sequence");
                break;
            }
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            {
                char hex[4] = {};
                hex[0] = current_;
                if (isOctDigit(Peek(1)))
                {
                    Consume();
                    hex[1] = current_;
                    if (isOctDigit(Peek(1)))
                    {
                        Consume();
                        hex[2] = current_;
                    }
                }
                auto value = std::stoi(hex, nullptr, 8);
                // https://www.open-std.org/jtc1/sc22/WG14/www/docs/n1256.pdf §6.4.4.4 Paragraph 9:
                // The value of an octal or hexadecimal escape sequence shall be in the range of
                // representable values for the type unsigned char for an integer character constant
                if (value <= std::numeric_limits<unsigned char>::max())
                    token.value += (char)value;
                else
                    Error("Octal escape sequence out of range");
                break;
            }
            case 'u':
            {
                if (isHexDigit(Peek(1)) && isHexDigit(Peek(2)) && isHexDigit(Peek(3)) && isHexDigit(Peek(4)))
                {
                    char hex[5] = {};
                    for (int i = 0; i < 4; ++i)
                    {
                        Consume();
                        hex[i] = current_;
                    }
                    uint32_t value = std::stoi(hex, nullptr, 16);
                    token.value += utf8::Encode({ value });
                }
                else
                    Error("Malformed escape sequence, expecting two bytes");
                break;
            }
            case 'U':
            {
                // Same as \u but 4 bytes expected
                if (isHexDigit(Peek(1)) && isHexDigit(Peek(2)) && isHexDigit(Peek(3)) && isHexDigit(Peek(4))
                    && isHexDigit(Peek(5)) && isHexDigit(Peek(6)) && isHexDigit(Peek(7)) && isHexDigit(Peek(8)))
                {
                    char hex[9] = {};
                    for (int i = 0; i < 8; ++i)
                    {
                        Consume();
                        hex[i] = current_;
                    }
                    uint32_t value = std::stol(hex, nullptr, 16);
                    token.value += utf8::Encode({ value });
                }
                else
                    Error("Malformed escape sequence, expecting four bytes");
                break;
            }
            default:
                Error(std::format("Unknown escape sequence \"{}\"", current_));
                break;
            }
            escaped = false;
        }
        else if (current_ == '\\')
            escaped = true;
        else
            token.value += current_;

        Consume();

        if (IsEof())
        {
            ps.Restore();
            Error("Unterminated string");
            return;
        }
    } while (current_ != quote || escaped);
    // Consume quote
    Consume();
}

bool Lexer::Matches(std::string_view what) const
{
    for (size_t i = 0; i < what.size(); ++i)
    {
        if (Peek(i) != what[i])
            return false;
    }
    return true;
}

void Lexer::ParseRawString(Token& token)
{
    // Raw strings are like in C++:
    // R"something( the string )something"

    PositionStore ps(*this);
    // Consume R"
    Consume();
    Consume();

    token.type = Token::Type::String;
    size_t delimStart = pos_;
    while (current_ != '(')
    {
        if (current_ == ')' || current_ == '\\' || IsWhite())
        {
            Error(std::format("Raw string literal delimiter can not contain \"{}\"", current_));
            return;
        }
        Consume();
        if (IsEof())
        {
            Error("( expected");
            return;
        }
    }
    size_t delimEnd = pos_;

    // Consume (
    Consume();
    const std::string realDelim = std::format("){}\"", std::string_view(&in_[delimStart], delimEnd - delimStart));

    size_t contentStart = pos_;

    while (!Matches(realDelim))
    {
        Consume();
        if (IsEof())
        {
            ps.Restore();
            Error("Unterminated string");
            return;
        }
    }
    token.value = std::string(&in_[contentStart], pos_ - contentStart);
    // Consume delimiter
    for (size_t i = 0; i < realDelim.size(); ++i)
        Consume();
}

void Lexer::ParseIdent(Token& token)
{
    token.type = Token::Type::Ident;
    size_t start = pos_;
    do
    {
        Consume();
    } while (IsIdentChar());
    token.value = std::string(&in_[start], pos_ - start);
}

void Lexer::ParseLineToken(Token& token)
{
    token.type = Token::Type::Number;
    for (size_t i = 0; i < LINE_TOKEN.length(); ++i)
        Consume();
    token.value = std::to_string(token.loc.line + 1);
}

void Lexer::ParseColToken(Token& token)
{
    token.type = Token::Type::Number;
    for (size_t i = 0; i < COL_TOKEN.length(); ++i)
        Consume();
    token.value = std::to_string(token.loc.col + 1);
}

void Lexer::ParseFileToken(Token& token)
{
    token.type = Token::Type::String;
    for (size_t i = 0; i < FILE_TOKEN.length(); ++i)
        Consume();
    token.value = token.loc.file;
}

void Lexer::ParseNumber(Token& token)
{
    token.type = Token::Type::Number;
    size_t start = pos_;

    --pos_;
    bool success = ParseNumberString(
        [&]() -> char
        {
            Consume();
            return current_;
        },
        [&](size_t off) -> char
        {
            if (pos_ + off < in_.length())
                return in_[pos_ + off];
            return '\0';
        },
        [&](std::string msg) { Error(std::move(msg)); });

    if (!success)
        return;

    token.value = std::string(&in_[start], pos_ - start);
}

void Lexer::ParseSymbol(Token& token)
{
    size_t start = pos_;
    switch (current_)
    {
    case '&':
    {
        if (Peek(1) == '&')
        {
            Consume();
            token.type = Token::Type::LogicalAnd;
        }
        else if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignAnd;
        }
        else
            token.type = Token::Type::Amp;
        break;
    }
    case '#':
        token.type = Token::Type::Hash;
        break;
    case '$':
        if (Peek(1) == '$')
        {
            Consume();
            token.type = Token::Type::DoubleDollar;
        }
        else
            token.type = Token::Type::Dollar;
        break;
    case '*':
        if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignMul;
        }
        else if (Peek(1) == '*')
        {
            Consume();
            if (Peek(1) == '=')
            {
                Consume();
                token.type = Token::Type::AssignPower;
            }
            else
                token.type = Token::Type::DoubleAsterisk;
        }
        else
            token.type = Token::Type::Asterisk;
        break;
    case '@':
        token.type = Token::Type::At;
        break;
    case '\\':
        if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignIDiv;
        }
        else
            token.type = Token::Type::BackSlash;
        break;
    case '[':
        token.type = Token::Type::BracketOpen;
        break;
    case ']':
        token.type = Token::Type::BracketClose;
        break;
    case '^':
        if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignXor;
        }
        else
            token.type = Token::Type::Caret;
        break;
    case ':':
        token.type = Token::Type::Colon;
        break;
    case ',':
        token.type = Token::Type::Comma;
        break;
    case '{':
        token.type = Token::Type::CurlyOpen;
        break;
    case '}':
        token.type = Token::Type::CurlyClose;
        break;
    case '.':
        if (Peek(1) == '.')
        {
            Consume();
            token.type = Token::Type::DoubleDot;
        }
        else
            token.type = Token::Type::Dot;
        break;
    case '=':
    {
        if (Peek(1) == '=')
        {
            Consume();
            if (Peek(1) == '=')
            {
                Consume();
                token.type = Token::Type::StrictEq;
            }
            else
                token.type = Token::Type::CompEquals;
        }
        else
            token.type = Token::Type::Assign;
        break;
    }
    case '!':
    {
        if (Peek(1) == '=')
        {
            Consume();
            if (Peek(1) == '=')
            {
                Consume();
                token.type = Token::Type::StrictUneq;
            }
            else
                token.type = Token::Type::CompNot;
        }
        else
            token.type = Token::Type::Excl;
        break;
    }
    case '-':
        if (Peek(1) == '-')
        {
            Consume();
            token.type = Token::Type::Decrement;
        }
        else if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignSub;
        }
        else
            token.type = Token::Type::Minus;
        break;
    case '(':
        token.type = Token::Type::ParenOpen;
        break;
    case ')':
        token.type = Token::Type::ParenClose;
        break;
    case '%':
        if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignMod;
        }
        else
            token.type = Token::Type::Percent;
        break;
    case '|':
    {
        if (Peek(1) == '|')
        {
            Consume();
            token.type = Token::Type::LogicalOr;
        }
        else if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignOr;
        }
        else
            token.type = Token::Type::Pipe;
        break;
    }
    case '+':
        if (Peek(1) == '+')
        {
            Consume();
            token.type = Token::Type::Increment;
        }
        else if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignAdd;
        }
        else
            token.type = Token::Type::Plus;
        break;
    case '?':
        token.type = Token::Type::Question;
        break;
    case ';':
        token.type = Token::Type::Semicolon;
        break;
    case '/':
        if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::AssignDiv;
        }
        else
            token.type = Token::Type::Slash;
        break;
    case '~':
        token.type = Token::Type::Tilde;
        break;
    case '<':
    {
        if (Peek(1) == '=')
        {
            Consume();
            if (Peek(1) == '>')
            {
                Consume();
                token.type = Token::Type::Spaceship;
            }
            else
                token.type = Token::Type::LtEq;
        }
        else if (Peek(1) == '<')
        {
            Consume();
            if (Peek(1) == '=')
            {
                Consume();
                token.type = Token::Type::AssignShl;
            }
            else
                token.type = Token::Type::Shl;
        }
        else
            token.type = Token::Type::Lt;
        break;
    }
    case '>':
    {
        if (Peek(1) == '=')
        {
            Consume();
            token.type = Token::Type::GtEq;
        }
        else if (Peek(1) == '>')
        {
            Consume();
            if (Peek(1) == '=')
            {
                Consume();
                token.type = Token::Type::AssignShr;
            }
            else
                token.type = Token::Type::Shr;
        }
        else
            token.type = Token::Type::Gt;
        break;
    }
    default:
    {
        Error(std::format("Unexpected symbol \"{}\"", current_));
        return;
    }
    }
    token.value = std::string(&in_[start], pos_ - start + 1);
    Consume();
}

}
