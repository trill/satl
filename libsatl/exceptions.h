/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <stdexcept>
#include "error.h"

namespace sa::tl {

class Exception : public std::runtime_error
{
public:
    Exception(int code, const std::string& what) : std::runtime_error(what), code_(code) {}
    int GetCode() const { return code_; }

private:
    int code_;
};

class TypeMismatch : public Exception
{
public:
    explicit TypeMismatch(const std::string& what) : Exception(Error::Code::TypeMismatch, what) {}
};

class ArgumentsMismatch : public Exception
{
public:
    explicit ArgumentsMismatch(const std::string& what) : Exception(Error::Code::ArgumentsMismatch, what) {}
};

class ConstnessMismatch : public Exception
{
public:
    explicit ConstnessMismatch(const std::string& what) : Exception(Error::Code::CallingNonconstFunction, what) {}
};

class NullPtrDereference : public Exception
{
public:
    explicit NullPtrDereference(const std::string& what) : Exception(Error::Code::NullDereference, what) {}
};

}
