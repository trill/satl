/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cctype>
#include <functional>
#include <cstddef>
#include <string>
#include <string_view>
#include "token.h"

namespace sa::tl {

static constexpr std::string_view EOF_TOKEN = "__eof__";
static constexpr std::string_view FILE_TOKEN = "__file__";
static constexpr std::string_view LINE_TOKEN = "__line__";
static constexpr std::string_view COL_TOKEN = "__col__";

class PositionStore;

class Lexer
{
    friend class PositionStore;
public:
    Lexer(std::string_view in, std::string file) : in_(in), file_(std::move(file))
    {
        if (!in_.empty())
            current_ = in_[pos_];
    }

    Token Next();
    void Reset()
    {
        current_ = '\0';
        pos_ = 0;
        line_ = 0;
        col_ = 0;
        if (!in_.empty())
            current_ = in_[pos_];
    }
    [[nodiscard]] const std::string& GetFilename() const { return file_; }
    std::function<void(size_t pos, const std::string& file, size_t line, size_t col, std::string message)> onError;

private:
    void SkipUTF8BOM();
    [[nodiscard]] bool IsShebang() const { return current_ == '#' && Peek(1) == '!'; }
    [[nodiscard]] bool IsQuote() const { return current_ == '"' || current_ == '\'' || current_ == '`'; }
    [[nodiscard]] bool IsRawString() const { return current_ == 'R' && Peek(1) == '"'; }
    [[nodiscard]] bool IsDigit() const { return current_ >= '0' && current_ <= '9'; }
    [[nodiscard]] bool IsSymbol() const
    {
        return current_ == '&' || current_ == '#' || current_ == '*' || current_ == '[' || current_ == ']'
            || current_ == '^' || current_ == ':' || current_ == ',' || current_ == '{' || current_ == '}'
            || current_ == '.' || current_ == '=' || current_ == '!' || current_ == '-' || current_ == '('
            || current_ == ')' || current_ == '%' || current_ == '|' || current_ == '+' || current_ == '?'
            || current_ == ';' || current_ == '/' || current_ == '~' || current_ == '<' || current_ == '>'
            || current_ == '\\'|| current_ == '@' || current_ == '$';
    }
    [[nodiscard]] bool IsWhite() const { return IsCharWhite(current_); }
    [[nodiscard]] bool IsEof() const
    {
        if (pos_ >= in_.length())
            return true;
        return Matches(EOF_TOKEN);
    }
    [[nodiscard]] bool IsLineToken() const
    {
        return Matches(LINE_TOKEN);
    }
    [[nodiscard]] bool IsColToken() const
    {
        return Matches(COL_TOKEN);
    }
    [[nodiscard]] bool IsFileToken() const
    {
        return Matches(FILE_TOKEN);
    }
    [[nodiscard]] bool IsFirstIdentChar() const { return isalpha(current_) || current_ == '_'; }
    [[nodiscard]] bool IsIdentChar() const { return IsFirstIdentChar() || IsDigit(); }
    [[nodiscard]] char Peek(size_t off = 0) const
    {
        if (pos_ + off < in_.length())
            return in_[pos_ + off];
        return '\0';
    }
    [[nodiscard]] bool IsLineComment() const { return current_ == '/' && Peek(1) == '/'; }
    [[nodiscard]] bool IsBlockCommentStart() const { return current_ == '/' && Peek(1) == '*'; }
    [[nodiscard]] bool IsBlockCommentEnd() const { return current_ == '*' && Peek(1) == '/'; }
    [[nodiscard]] bool IsLineEnd() const { return IsEof() || current_ == '\r' || current_ == '\n'; }
    void Error(std::string message)
    {
        if (onError)
            onError(pos_, file_, line_, col_, std::move(message));
    }
    void Consume();
    [[nodiscard]] bool Matches(std::string_view what) const;
    void ParseLineComment(Token& token);
    void ParseBlockComment(Token& token);
    void ParseString(Token& token);
    void ParseRawString(Token& token);
    void ParseIdent(Token& token);
    void ParseNumber(Token& token);
    void ParseSymbol(Token& token);
    void ParseLineToken(Token& token);
    void ParseColToken(Token& token);
    void ParseFileToken(Token& token);

    static bool IsCharWhite(char c)
    {
        return c == ' ' || c == '\t' || c == '\r' || c == '\n';
    }
    char current_{ '\0' };
    size_t pos_{ 0 };
    size_t line_{ 0 };
    int col_{ 0 };
    std::string_view in_;
    std::string file_;
};

class PositionStore
{
public:
    explicit PositionStore(Lexer& lexer) :
        lexer_(lexer),
        pos_(lexer_.pos_),
        line_(lexer_.line_),
        col_(lexer_.col_),
        file_(lexer_.file_)
    {}
    void Restore()
    {
        lexer_.pos_ = pos_;
        lexer_.col_ = col_;
        lexer_.line_ = line_;
        lexer_.file_ = file_;
    }
private:
    Lexer& lexer_;
    size_t pos_;
    size_t line_;
    int col_;
    std::string file_;
};

}
