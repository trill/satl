/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "value.h"
#include <cmath>
#include <limits>
#include "context.h"
#include "metatable.h"
#include "node.h"
#include <sstream>
#include <format>

namespace sa::tl {

// Helper type for the visitor
template<class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};
// Explicit deduction guide (not needed as of C++20, unless you use QtCreators codemodel which totally freaks out,
// Also clangd is not happy without it.)
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

std::string KeyToString(const KeyType& key)
{
    std::string result;
    std::visit(overloaded{ [&result](std::nullptr_t) { result = LITERAL_NULL; },
                   [&result](bool arg) { result = (arg ? LITERAL_TRUE : LITERAL_FALSE); },
                   [&result](Integer arg) { result = std::to_string(arg); },
                   [&result](Float arg) { result = std::to_string(arg); },
                   [&result](const std::string& arg) { result = arg; },
                   [&result](const CallablePtr&) { result = "[function]"; },
                   [&result](const TablePtr&) { result = "[table]"; },
                   [&result](const ObjectPtr&) { result = "[object]"; } },
        key);

    return result;
}

namespace {

template<typename T>
constexpr bool FloatEquals(T lhs, T rhs)
{
    static_assert (std::is_floating_point_v<T>);
    return lhs + std::numeric_limits<T>::epsilon() >= rhs && lhs - std::numeric_limits<T>::epsilon() <= rhs;
}

}

Container::~Container() = default;

ValuePtr Value::FromContainer(const ContainerPtr& container)
{
    if (container->IsTable() || container->IsModule())
        return MakeValue(std::static_pointer_cast<Table>(container));
    if (container->IsObject())
        return MakeValue(std::static_pointer_cast<Object>(container));
    return {};
}

Table::~Table() = default;

void Table::Add(ValuePtr&& value)
{
    Add(nextIndex_, std::forward<ValuePtr>(value));
}

void Table::Add(const ValuePtr& value)
{
    Add(nextIndex_, value);
}

void Table::Add(const KeyType& key, ValuePtr&& value)
{
    if (isArray_)
    {
        isArray_ = key.index() == KEY_INT && std::get<Integer>(key) == nextIndex_;
        if (isArray_)
            ++nextIndex_;
    }
    types_ |= 1u << value->GetType();
    emplace(key, std::move(value));
}

void Table::Add(const KeyType& key, const ValuePtr& value)
{
    Add(key, MakeValue(*value));
}

void Table::Append(const Table& tbl)
{
    if (tbl.IsArray())
    {
        for (const auto& i : tbl)
            Add(i.second);
    }
    else
    {
        for (const auto& i : tbl)
            Add(i.first, i.second);
    }
}

void Table::Delete(const KeyType& key)
{
    if (isArray_)
    {
        // It's no longer an array when deleting an item in the middle
        if (key.index() == KEY_INT && std::get<Integer>(key) != nextIndex_ - 1)
            isArray_ = false;
    }
    erase(key);
}

void Table::Clear()
{
    clear();
    isArray_ = true;
    nextIndex_ = 0;
    types_ = 0;
}

void Table::CopyTo(Table& dst) const
{
    for (const auto& ti : *this)
        dst.Add(ti.first, ti.second->Copy());
    dst.isArray_ = isArray_;
    dst.nextIndex_ = nextIndex_;
}

Object::~Object() = default;

Value Value::Copy() const
{
    if (IsString())
    {
        Value result = MakeString(*std::get<INDEX_STRING>(value_));
        result.template_ = template_;
        // A copy of a string doesn't get its constness
        return result;
    }
    if (IsTable())
    {
        Value result;
        TablePtr tab = MakeTable();
        const auto& table = std::get<INDEX_TABLE>(value_);
        table->CopyTo(*tab);
        result.value_.emplace<INDEX_TABLE>(std::move(tab));
        result.SetConst(IsConst(), false);
        return result;
    }
    return *this;
}

std::optional<KeyType> Table::GetValueKey(const Value& value) const
{
    for (const auto& v : *this)
        if (v.second.get() == &value)
            return v.first;
    return {};
}

size_t Table::CountOf(const Value& value) const
{
    size_t result = 0;
    for (const auto& v : *this)
    {
        if (v.second->StrictEquals(value))
            ++result;
    }
    return result;
}

bool Value::ToBool() const
{
    switch (value_.index())
    {
    case INDEX_NULL:
        return false;
    case INDEX_BOOL:
        return std::get<INDEX_BOOL>(value_);
    case INDEX_INT:
        return std::get<INDEX_INT>(value_) != 0;
    case INDEX_FLOAT:
        return static_cast<int>(std::get<INDEX_FLOAT>(value_)) != 0;
    case INDEX_STRING:
        if (!std::get<INDEX_STRING>(value_))
            return false;
        return !std::get<INDEX_STRING>(value_)->empty();
    case INDEX_FUNCTION:
        return !!std::get<INDEX_FUNCTION>(value_);
    case INDEX_TABLE:
        if (!std::get<INDEX_TABLE>(value_))
            return false;
        return !std::get<INDEX_TABLE>(value_)->empty();
    case INDEX_OBJECT:
        return !!std::get<INDEX_OBJECT>(value_)->GetInstance();
    }
    SATL_ASSERT_FALSE();
}

std::string Value::ToString() const
{
    switch (value_.index())
    {
    case INDEX_NULL:
        return std::string(LITERAL_NULL);
    case INDEX_BOOL:
        return std::get<INDEX_BOOL>(value_) ? std::string(LITERAL_TRUE) : std::string(LITERAL_FALSE);
    case INDEX_INT:
        return std::to_string(std::get<INDEX_INT>(value_));
    case INDEX_FLOAT:
    {
        std::stringstream ss;
        ss.imbue(std::locale::classic());
        ss << std::get<INDEX_FLOAT>(value_);
        std::string result = ss.str();
        if (result.find('.') == std::string::npos)
            return result + ".0";
        return result;
    }
    case INDEX_STRING:
        return *std::get<INDEX_STRING>(value_);
    case INDEX_FUNCTION:
        return "[function]";
    case INDEX_TABLE:
        return "[table]";
    case INDEX_OBJECT:
        return std::format("[{}]", As<ObjectPtr>()->GetClassName());
    }
    SATL_ASSERT_FALSE();
}

Value KeyToValue(const KeyType& value)
{
    switch (value.index())
    {
    case KEY_BOOL:
        return { std::get<KEY_BOOL>(value) };
    case KEY_INT:
        return { std::get<KEY_INT>(value) };
    case KEY_FLOAT:
        return { std::get<KEY_FLOAT>(value) };
    case KEY_STRING:
        return { MakeString(std::get<KEY_STRING>(value)) };
    case KEY_FUNCTION:
        return { std::get<KEY_FUNCTION>(value) };
    case KEY_TABLE:
        return { std::get<KEY_TABLE>(value) };
    case KEY_OBJECT:
        return { std::get<KEY_OBJECT>(value) };
    }
    SATL_ASSERT_FALSE();
}

Integer Value::ToInt() const
{
    switch (value_.index())
    {
    case INDEX_NULL:
        return 0;
    case INDEX_BOOL:
        return std::get<INDEX_BOOL>(value_) ? 1 : 0;
    case INDEX_INT:
        return std::get<INDEX_INT>(value_);
    case INDEX_FLOAT:
        return static_cast<Integer>(std::get<INDEX_FLOAT>(value_));
    case INDEX_STRING:
        return static_cast<Integer>(std::atoi(std::get<INDEX_STRING>(value_)->c_str()));
    case INDEX_FUNCTION:
    case INDEX_TABLE:
    case INDEX_OBJECT:
        return 0;
    }
    SATL_ASSERT_FALSE();
}

Unsigned Value::ToUint() const
{
    return static_cast<Unsigned>(ToInt());
}

Float Value::ToFloat() const
{
    switch (value_.index())
    {
    case INDEX_NULL:
        return 0;
    case INDEX_BOOL:
        return std::get<INDEX_BOOL>(value_) ? 1 : 0;
    case INDEX_INT:
        return static_cast<Float>(std::get<INDEX_INT>(value_));
    case INDEX_FLOAT:
        return std::get<INDEX_FLOAT>(value_);
    case INDEX_STRING:
        return static_cast<Float>(std::atof(std::get<INDEX_STRING>(value_)->c_str()));
    case INDEX_FUNCTION:
    case INDEX_TABLE:
    case INDEX_OBJECT:
        return 0;
    }
    SATL_ASSERT_FALSE();
}

TablePtr Value::ToTable() const
{
    if (IsTable())
        return std::get<INDEX_TABLE>(value_);
    if (IsString())
    {
        // Convert the string into a table with the characters
        const auto& str = *std::get<INDEX_STRING>(value_);
        auto result = MakeTable();
        for (size_t i = 0; i < str.size(); ++i)
            result->Add((Integer)i, std::string(1, str[i]));
        return result;
    }
    // Otherwise make a table with us as the only member
    auto result = MakeTable();
    result->Add(0, MakeValue(*this));
    return result;
}

KeyType Value::ToKey(Context& ctx) const
{
    switch (value_.index())
    {
    case INDEX_BOOL:
        return std::get<INDEX_BOOL>(value_);
    case INDEX_INT:
        return std::get<INDEX_INT>(value_);
    case INDEX_FLOAT:
        return std::get<INDEX_FLOAT>(value_);
    case INDEX_STRING:
        // We want to copy the string because when the string changes the key shouldn't also change
        return *std::get<INDEX_STRING>(value_);
    case INDEX_FUNCTION:
        return std::get<INDEX_FUNCTION>(value_);
    case INDEX_TABLE:
        return std::get<INDEX_TABLE>(value_);
    case INDEX_OBJECT:
        return std::get<INDEX_OBJECT>(value_);
    default:
        ctx.RuntimeError(Error::Code::TypeMismatch, std::format("Can not convert {} to a key", GetTypeName()));
        return {};
    }
}

ContainerPtr Value::ToContainer() const
{
    if (IsTable())
        return std::get<INDEX_TABLE>(value_);
    if (IsObject())
        return std::get<INDEX_OBJECT>(value_);
    return {};
}

std::string Value::Stringify(Context& ctx) const
{
    if (auto func = GetOperator("$operator"))
    {
        auto res = func->CallWithThis(ctx, *this, { });
        return res->ToString();
    }
    return ToString();
}

bool Value::Equals(const Value& rhs) const
{
    if (StrictEquals(rhs))
        return true;

    switch (value_.index())
    {
    case INDEX_BOOL:
        return std::get<INDEX_BOOL>(value_) == rhs.ToBool();
    case INDEX_FLOAT:
        return FloatEquals(std::get<INDEX_FLOAT>(value_), rhs.ToFloat());
    case INDEX_INT:
        return std::get<INDEX_INT>(value_) == rhs.ToInt();
    case INDEX_NULL:
        return rhs.value_.index() == INDEX_NULL;
    case INDEX_STRING:
        if (!std::get<INDEX_STRING>(value_))
            return rhs.IsNull();
        return *std::get<INDEX_STRING>(value_) == rhs.ToString();
    case INDEX_FUNCTION:
        if (!std::get<INDEX_FUNCTION>(value_))
            return rhs.IsNull();
        if (!rhs.IsCallable())
            return false;
        return std::get<INDEX_FUNCTION>(value_) == std::get<INDEX_FUNCTION>(rhs.value_);
    case INDEX_TABLE:
    {
        if (!std::get<INDEX_TABLE>(value_))
            return rhs.IsNull();
        if (!rhs.IsTable())
            return false;
        const auto& a = *std::get<INDEX_TABLE>(value_);
        const auto& b = *std::get<INDEX_TABLE>(rhs.value_);
        if (a.size() != b.size())
            return false;
        // Comparing tables compares in worst case every element
        for (const auto& ia : a)
        {
            const auto& ib = b.find(ia.first);
            if (ib == b.end())
                return false;
            if (!ia.second->Equals(*ib->second))
                return false;
        }
        return true;
    }
    case INDEX_OBJECT:
        if (rhs.value_.index() == INDEX_NULL)
            return std::get<INDEX_OBJECT>(value_)->GetInstance() == nullptr;
        if (!rhs.IsObject())
            return false;
        return std::get<INDEX_OBJECT>(value_)->GetInstance() == std::get<INDEX_OBJECT>(rhs.value_)->GetInstance();
    }

    return false;
}

bool Value::Equals(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator=="))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    return Equals(rhs);
}

bool Value::Unequals(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator!="))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    return !Equals(rhs);
}

Integer Value::ThreeWayCmp(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator<=>"))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToInt();
    }
    if (Lt(ctx, rhs))
        return -1;
    if (Gt(ctx, rhs))
        return 1;
    return 0;
}

bool Value::StrictEquals(const Value& rhs) const
{
    if (this == &rhs)
        return true;
    if (value_.index() != rhs.value_.index())
        return false;
    if (value_.index() == INDEX_FLOAT)
        return FloatEquals(std::get<INDEX_FLOAT>(value_), std::get<INDEX_FLOAT>(rhs.value_));
    if (value_.index() == INDEX_STRING)
        return *std::get<INDEX_STRING>(value_) == *std::get<INDEX_STRING>(rhs.value_);
    return value_ == rhs.value_;
}

bool Value::StrictEquals(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator==="))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    return StrictEquals(rhs);
}

bool Value::StrictUnequals(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator!=="))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    return !StrictEquals(rhs);
}

bool Value::Gt(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator>"))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    if (IsNull() || rhs.IsNull())
        return false;

    if (IsString() && rhs.IsString())
        return *std::get<INDEX_STRING>(value_) > *std::get<INDEX_STRING>(rhs.value_);

    if (IsCompatible(rhs))
    {

        switch (value_.index())
        {
        case INDEX_FLOAT:
            return std::get<INDEX_FLOAT>(value_) > rhs.ToFloat();
        case INDEX_INT:
            return std::get<INDEX_INT>(value_) > rhs.ToInt();
        }
    }

    ctx.TypeMismatch(*this, rhs);
    return false;
}

bool Value::GtEquals(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator>="))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }
    if (IsCompatible(rhs))
        return Equals(rhs) || Gt(ctx, rhs);
    ctx.TypeMismatch(*this, rhs);
    return false;
}

bool Value::Lt(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator<"))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    if (IsNull() || rhs.IsNull())
        return false;

    if (IsString() && rhs.IsString())
        return *std::get<INDEX_STRING>(value_) < *std::get<INDEX_STRING>(rhs.value_);

    if (IsCompatible(rhs))
    {

        switch (value_.index())
        {
        case INDEX_FLOAT:
            return std::get<INDEX_FLOAT>(value_) < rhs.ToFloat();
        case INDEX_INT:
            return std::get<INDEX_INT>(value_) < rhs.ToInt();
        }
    }

    ctx.TypeMismatch(*this, rhs);
    return false;
}

bool Value::LtEquals(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator<="))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }

    if (IsCompatible(rhs))
        return Equals(rhs) || Lt(ctx, rhs);
    ctx.TypeMismatch(*this, rhs);
    return false;
}

bool Value::In(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operatorin"))
    {
        auto res = op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        return res->ToBool();
    }
    if (rhs.IsTable())
    {
        const auto& tbl = *rhs.As<TablePtr>();
        auto it = std::find_if(tbl.begin(), tbl.end(), [&](const std::pair<KeyType, ValuePtr>& current) {
            return current.second->Equals(*this);
        });
        return it != tbl.end();
    }

    // RHS a string and LHS a string -> check if LHS is contained in RHS
    // "ab" in "abc" = true
    if (rhs.IsString() && IsString())
    {
        const auto& thisStr = *As<StringPtr>();
        const auto& rhsStr = *rhs.As<StringPtr>();
        return rhsStr.find(thisStr) != std::string::npos;
    }
    // RHS a string and LHS an int (character) -> check if the character can be found in the string
    // 65 in "ABC" = true
    if (rhs.IsString() && IsInt())
    {
        auto thisInt = ToUint();
        if (thisInt > std::numeric_limits<uint8_t>::max())
            return false;
        const auto& rhsStr = *rhs.As<StringPtr>();
        return rhsStr.find((char)thisInt) != std::string::npos;
    }
    return false;
}

bool Value::InRange(Context& ctx, const Value& first, const Value& last) const
{
    if (auto op = GetOperator("operatorin"))
    {
        auto arg = MakeTable();
        arg->Add(first);
        arg->Add(last);
        auto res = op->CallWithThis(ctx, *this, { MakeValue(arg) });
        return res->ToBool();
    }
    return GtEquals(ctx, first) && LtEquals(ctx, last);
}

Value Value::PreInc(Context& ctx)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("++operator"))
    {
        auto res = *op->CallWithThis(ctx, *this, { });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT)
    {
        ++std::get<INDEX_INT>(value_);
        return value_;
    }
    if (value_.index() == INDEX_FLOAT)
    {
        ++std::get<INDEX_FLOAT>(value_);
        return value_;
    }

    ctx.RuntimeError(Error::Code::TypeMismatch, "No pre increase operator");;
    return {};
}

Value Value::PreDec(Context& ctx)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("--operator"))
    {
        auto res = *op->CallWithThis(ctx, *this, { });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT)
    {
        --std::get<INDEX_INT>(value_);
        return value_;
    }
    if (value_.index() == INDEX_FLOAT)
    {
        --std::get<INDEX_FLOAT>(value_);
        return value_;
    }

    ctx.RuntimeError(Error::Code::TypeMismatch, "No pre decrease operator");;
    return {};
}

Value Value::PostInc(Context& ctx)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator++"))
    {
        auto res = *op->CallWithThis(ctx, *this, { });
        value_ = res.value_;
        // This returns whatever the operator returned, not the previous value
        return value_;
    }

    if (value_.index() == INDEX_INT)
    {
        auto result = value_;
        std::get<INDEX_INT>(value_)++;
        return result;
    }
    if (value_.index() == INDEX_FLOAT)
    {
        auto result = value_;
        std::get<INDEX_FLOAT>(value_)++;
        return result;
    }

    ctx.RuntimeError(Error::Code::TypeMismatch, "No post increase operator");;
    return {};
}

Value Value::PostDec(Context& ctx)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator--"))
    {
        auto res = *op->CallWithThis(ctx, *this, { });
        value_ = res.value_;
        // This returns whatever the operator returned, not the previous value
        return value_;
    }

    if (value_.index() == INDEX_INT)
    {
        auto result = value_;
        std::get<INDEX_INT>(value_)--;
        return result;
    }
    if (value_.index() == INDEX_FLOAT)
    {
        auto result = value_;
        std::get<INDEX_FLOAT>(value_)--;
        return result;
    }

    ctx.RuntimeError(Error::Code::TypeMismatch, "No post decrease operator");
    return {};
}

Value Value::AssignAdd(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator+="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        std::get<INDEX_INT>(value_) += rhs.ToInt();
        return value_;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
    {
        std::get<INDEX_FLOAT>(value_) += rhs.ToFloat();
        return value_;
    }
    if (value_.index() == INDEX_STRING)
    {
        std::string& str = *std::get<INDEX_STRING>(value_);
        str += rhs.ToString();
        return value_;
    }
    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& tbl = std::get<INDEX_TABLE>(value_);
        std::visit(
            overloaded{ [tbl](std::nullptr_t) { tbl->Add(nullptr); },
                [&tbl](bool arg) { tbl->Add(arg); },
                [&tbl](Integer arg) { tbl->Add(arg); },
                [&tbl](Float arg) { tbl->Add(arg); },
                [&tbl](const StringPtr& arg) { tbl->Add(arg); },
                [&tbl](const CallablePtr& arg) { tbl->Add(arg); },
                [&tbl](const TablePtr& arg)
                {
                    for (const auto& item : (*arg))
                    {
                        if (item.first.index() == KEY_INT)
                            tbl->Add(item.second);
                        else
                            tbl->Add(item.first, item.second);
                    }
                },
                [&tbl](const ObjectPtr& arg) { tbl->Add(arg); } },
            rhs.value_);
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignAnd(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator&="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        Unsigned val = static_cast<Unsigned>(std::get<INDEX_INT>(value_));
        val &= rhs.ToUint();
        std::get<INDEX_INT>(value_) = static_cast<Integer>(val);
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignDiv(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator/="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        const auto rhsV = rhs.ToInt();
        if (rhsV == 0)
        {
            ctx.RuntimeError(Error::Code::MathError, "Division by zero");
            return {};
        }
        std::get<INDEX_INT>(value_) /= rhsV;
        return value_;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
    {
        std::get<INDEX_FLOAT>(value_) /= rhs.ToFloat();
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignIDiv(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator\\="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        const auto rhsV = rhs.ToInt();
        if (rhsV == 0)
        {
            ctx.RuntimeError(Error::Code::MathError, "Division by zero");
            return {};
        }
        std::get<INDEX_INT>(value_) /= rhsV;
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignMod(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator%="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        std::get<INDEX_INT>(value_) %= rhs.ToInt();
        return value_;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
    {
        std::get<INDEX_FLOAT>(value_) = std::fmod(std::get<INDEX_FLOAT>(value_), rhs.ToFloat());
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignMul(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator*="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        std::get<INDEX_INT>(value_) *= rhs.ToInt();
        return value_;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
    {
        std::get<INDEX_FLOAT>(value_) *= rhs.ToFloat();
        return value_;
    }
    if (value_.index() == INDEX_STRING && (rhs.value_.index() == INDEX_INT))
    {
        // "str" * 3 = "strstrstr"
        std::string val = *std::get<INDEX_STRING>(value_);
        std::get<INDEX_STRING>(value_)->clear();
        for (auto i = 0; i < rhs.ToInt(); ++i)
            *std::get<INDEX_STRING>(value_) += val;
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignOr(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator|="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        Unsigned val = static_cast<Unsigned>(std::get<INDEX_INT>(value_));
        val |= rhs.ToUint();
        std::get<INDEX_INT>(value_) = static_cast<Integer>(val);
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignShl(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator<<="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        Unsigned val = static_cast<Unsigned>(std::get<INDEX_INT>(value_));
        val <<= rhs.ToUint();
        std::get<INDEX_INT>(value_) = static_cast<Integer>(val);
        return value_;
    }

    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& tbl = std::get<INDEX_TABLE>(value_);
        std::visit(
            overloaded{ [tbl](std::nullptr_t) { tbl->Add(nullptr); },
                [&tbl](bool arg) { tbl->Add(arg); },
                [&tbl](Integer arg) { tbl->Add(arg); },
                [&tbl](Float arg) { tbl->Add(arg); },
                [&tbl](const StringPtr& arg) { tbl->Add(arg); },
                [&tbl](const CallablePtr& arg) { tbl->Add(arg); },
                [&tbl](const TablePtr& arg) { tbl->Add(arg); },
                [&tbl](const ObjectPtr& arg) { tbl->Add(arg); } },
            rhs.value_);
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignShr(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator>>="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        Unsigned val = static_cast<Unsigned>(std::get<INDEX_INT>(value_));
        val >>= rhs.ToUint();
        std::get<INDEX_INT>(value_) = static_cast<Integer>(val);
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignSub(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator-="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        std::get<INDEX_INT>(value_) -= rhs.ToInt();
        return value_;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
    {
        std::get<INDEX_FLOAT>(value_) -= rhs.ToFloat();
        return value_;
    }
    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& tbl = std::get<INDEX_TABLE>(value_);
        // Remove table element by key where rhs is the key:
        auto key = rhs.ToKey(ctx);
        if (ctx.HasErrors())
            return {};
        if (!tbl->contains(key))
        {
            ctx.RuntimeError(Error::Code::InvalidSubscript, std::format("Key \"{}\" not found", KeyToString(key)));
            return {};
        }
        tbl->Delete(key);
        return value_;
    }
    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignXor(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator^="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        Unsigned val = static_cast<Unsigned>(std::get<INDEX_INT>(value_));
        val ^= rhs.ToUint();
        std::get<INDEX_INT>(value_) = static_cast<Integer>(val);
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::AssignPow(Context& ctx, const Value& rhs)
{
    if (!CheckConst(ctx))
        return {};

    if (auto op = GetOperator("operator**="))
    {
        auto res = *op->CallWithThis(ctx, *this, { MakeValue(rhs) });
        value_ = res.value_;
        return value_;
    }

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        if (rhs.IsFloat())
        {
            Float val = static_cast<Float>(std::get<INDEX_INT>(value_));
            std::get<INDEX_INT>(value_) = static_cast<Integer>(std::pow(val, rhs.ToFloat()));
        }
        else
            std::get<INDEX_INT>(value_) = std::pow(std::get<INDEX_INT>(value_), rhs.ToInt());
        return value_;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
    {
        Float val = std::get<INDEX_FLOAT>(value_);
        val = std::pow(val, rhs.ToFloat());
        std::get<INDEX_FLOAT>(value_) = val;
        return value_;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Add(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator+"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
        return std::get<INDEX_INT>(value_) + rhs.ToInt();
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
        return std::get<INDEX_FLOAT>(value_) + rhs.ToFloat();
    if (value_.index() == INDEX_STRING)
    {
        std::string result = *std::get<INDEX_STRING>(value_);
        std::visit(overloaded{ [&result](std::nullptr_t) { result += LITERAL_NULL; },
                       [&result](bool arg) { result += (arg ? LITERAL_TRUE : LITERAL_FALSE); },
                       [&result](Integer arg) { result += std::to_string(arg); },
                       [&result](Float arg) { result += std::to_string(arg); },
                       [&result](const StringPtr& arg) { result += (*arg); },
                       [&](const CallablePtr&) { ctx.TypeMismatch(*this, rhs); },
                       [&](const TablePtr&) { ctx.TypeMismatch(*this, rhs); },
                       [&](const ObjectPtr&) { ctx.TypeMismatch(*this, rhs); } },
            rhs.value_);

        return MakeString(result);
    }
    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& result = std::get<INDEX_TABLE>(value_);
        std::visit(
            overloaded{ [&result](std::nullptr_t)
                { result->Add(nullptr); },
                [&result](bool arg) { result->Add(arg); },
                [&result](Integer arg)
                { result->Add(arg); },
                [&result](Float arg)
                { result->Add(arg); },
                [&result](const StringPtr& arg)
                { result->Add(arg); },
                [&result](const CallablePtr& arg)
                { result->Add(arg); },
                [&result](const TablePtr& arg)
                {
                    for (const auto& item : (*arg))
                    {
                        if (item.first.index() == KEY_INT)
                            result->Add(item.second);
                        else
                            result->Add(item.first, item.second);
                    }
                },
                [&result](const ObjectPtr& arg)
                { result->Add(arg); } },
            rhs.value_);

        return result;
    }
    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Sub(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator-"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
        return std::get<INDEX_INT>(value_) - rhs.ToInt();
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
        return std::get<INDEX_FLOAT>(value_) - rhs.ToFloat();
    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& table = std::get<INDEX_TABLE>(value_);
        // Remove table element by key where rhs is the key:
        auto key = rhs.ToKey(ctx);
        if (ctx.HasErrors())
            return {};
        if (!table->contains(key))
        {
            ctx.RuntimeError(Error::Code::InvalidSubscript, std::format("Key \"{}\" not found", KeyToString(key)));
            return {};
        }
        table->Delete(key);
        return table;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Mul(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator*"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
        return std::get<INDEX_INT>(value_) * rhs.ToInt();
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
        return std::get<INDEX_FLOAT>(value_) * rhs.ToFloat();
    if (value_.index() == INDEX_STRING && (rhs.value_.index() == INDEX_INT))
    {
        // "str" * 3 = "strstrstr"
        std::string result;
        std::string val = *std::get<INDEX_STRING>(value_);
        for (auto i = 0; i < rhs.ToInt(); ++i)
            result += val;
        return MakeString(result);
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Div(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator/"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        const auto rhsV = rhs.ToInt();
        if (rhsV == 0)
        {
            ctx.RuntimeError(Error::Code::MathError, "Division by zero");
            return {};
        }
        return std::get<INDEX_INT>(value_) / rhsV;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
        return std::get<INDEX_FLOAT>(value_) / rhs.ToFloat();

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::IDiv(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator\\"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if ((value_.index() == INDEX_INT || value_.index() == INDEX_FLOAT) && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        const auto rhsV = rhs.ToInt();
        if (rhsV == 0)
        {
            ctx.RuntimeError(Error::Code::MathError, "Division by zero");
            return {};
        }
        return ToInt() / rhsV;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Range(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator.."))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (!IsInt())
    {
        ctx.RuntimeError(Error::Code::TypeMismatch,
            "Type mismatch. Integer type required for range expression");
        return {};
    }
    if (!rhs.IsInt())
    {
        ctx.RuntimeError(Error::Code::TypeMismatch,
            "Type mismatch. Integer type required for range expression");
        return {};
    }
    TablePtr result = MakeTable();
    Integer index = 0;
    for (Integer i = As<Integer>(); i <= rhs.As<Integer>(); ++i, ++index)
    {
        result->Add(index, i);
    }
    return result;
}

Value Value::Mod(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator%"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        const auto rhsV = rhs.ToInt();
        if (rhsV == 0)
        {
            ctx.RuntimeError(Error::Code::MathError, "Division by zero");
            return {};
        }
        return std::get<INDEX_INT>(value_) % rhsV;
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
        return std::fmod(std::get<INDEX_FLOAT>(value_), rhs.ToFloat());

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Pow(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator**"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && (rhs.value_.index() == INDEX_INT || rhs.value_.index() == INDEX_FLOAT))
    {
        if (rhs.IsFloat())
            return (Integer)std::pow((Float)std::get<INDEX_INT>(value_), rhs.ToFloat());
        return (Integer)std::pow(std::get<INDEX_INT>(value_), rhs.ToInt());
    }
    if (value_.index() == INDEX_FLOAT && (rhs.value_.index() == INDEX_FLOAT || rhs.value_.index() == INDEX_INT))
        return (Float)std::pow(std::get<INDEX_FLOAT>(value_), rhs.ToFloat());

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Shr(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator>>"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && rhs.value_.index() == INDEX_INT)
        return static_cast<Integer>(static_cast<Unsigned>(std::get<INDEX_INT>(value_)) >> rhs.ToUint());

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::Shl(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator<<"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() == INDEX_INT && rhs.value_.index() == INDEX_INT)
    {
        return static_cast<Integer>(ToUint() << rhs.ToUint());
    }

    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& result = std::get<INDEX_TABLE>(value_);
        std::visit(
            overloaded{ [&result](std::nullptr_t)
                { result->Add(nullptr); },
                [&result](bool arg) { result->Add(arg); },
                [&result](Integer arg)
                { result->Add(arg); },
                [&result](Float arg)
                { result->Add(arg); },
                [&result](const StringPtr& arg)
                { result->Add(arg); },
                [&result](const CallablePtr& arg)
                { result->Add(arg); },
                [&result](const TablePtr& arg)
                { result->Add(arg); },
                [&result](const ObjectPtr& arg)
                { result->Add(arg); } },
            rhs.value_);

        return result;
    }

    ctx.TypeMismatch(*this, rhs);
    return {};
}

Value Value::BitwiseAnd(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator&"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() != INDEX_INT || value_.index() != rhs.value_.index())
    {
        ctx.TypeMismatch(*this, rhs);
        return {};
    }
    return static_cast<Integer>(ToUint() & rhs.ToUint());
}

Value Value::BitwiseOr(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator|"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() != INDEX_INT || value_.index() != rhs.value_.index())
    {
        ctx.TypeMismatch(*this, rhs);
        return {};
    }
    return static_cast<Integer>(ToUint() | rhs.ToUint());
}

Value Value::BitwiseXor(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator^"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    if (value_.index() != INDEX_INT || value_.index() != rhs.value_.index())
    {
        ctx.TypeMismatch(*this, rhs);
        return {};
    }
    return static_cast<Integer>(ToUint() ^ rhs.ToUint());
}

Value Value::BitwiseNot(Context& ctx) const
{
    if (auto op = GetOperator("~operator"))
        return *op->CallWithThis(ctx, *this, { });

    if (value_.index() == INDEX_INT)
        return static_cast<Integer>(~ToUint());
    if (value_.index() == INDEX_STRING)
    {
        auto result = MakeString(ToString());
        std::reverse(result->begin(), result->end());
        return result;
    }
    if (value_.index() == INDEX_TABLE)
    {
        auto result = MakeTable();
        const auto& table = *As<TablePtr>();
        std::vector<ValuePtr> sorted;
        sorted.reserve(table.size());
        for (const auto& item : table)
        {
            sorted.push_back(item.second);
        }
        std::reverse(sorted.begin(), sorted.end());
        for (size_t i = 0; i < sorted.size(); ++i)
            result->Add((Integer)i, sorted[i]);
        return result;
    }
    ctx.RuntimeError(Error::Code::TypeMismatch, "Type mismatch");
    return {};
}

Value Value::UnaryPlus(Context& ctx) const
{
    if (auto op = GetOperator("+operator"))
        return *op->CallWithThis(ctx, *this, { });

    if (value_.index() == INDEX_INT)
        return std::get<INDEX_INT>(value_);
    if (value_.index() == INDEX_FLOAT)
        return std::get<INDEX_FLOAT>(value_);
    ctx.RuntimeError(Error::Code::TypeMismatch, "Type mismatch");
    return {};
}

Value Value::Negate(Context& ctx) const
{
    if (auto op = GetOperator("-operator"))
        return *op->CallWithThis(ctx, *this, { });

    if (value_.index() == INDEX_INT)
        return -std::get<INDEX_INT>(value_);
    if (value_.index() == INDEX_FLOAT)
        return -std::get<INDEX_FLOAT>(value_);
    ctx.RuntimeError(Error::Code::TypeMismatch, "Type mismatch");
    return {};
}

Value Value::LogicalNot(Context& ctx) const
{
    if (auto op = GetOperator("!operator"))
        return *op->CallWithThis(ctx, *this, { });

    return !ToBool();
}

Value Value::LogicalAnd(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator&&"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    return ToBool() && rhs.ToBool();
}

Value Value::LogicalOr(Context& ctx, const Value& rhs) const
{
    if (auto op = GetOperator("operator||"))
        return *op->CallWithThis(ctx, *this, { MakeValue(rhs) });

    return ToBool() || rhs.ToBool();
}

Value Value::CountOf(Context& ctx) const
{
    if (auto op = GetOperator("#operator"))
        return *op->CallWithThis(ctx, *this, { });

    if (IsTable())
    {
        const auto& table = As<TablePtr>();
        return static_cast<Integer>(table->size());
    }
    if (IsString())
    {
        const auto& str = As<StringPtr>();
        return static_cast<Integer>(str->length());
    }
    ctx.RuntimeError(Error::Code::TypeMismatch, "Not a table or a string");
    return {};
}

Value Value::NameOf(Context& ctx) const
{
    if (auto op = GetOperator("$$operator"))
        return *op->CallWithThis(ctx, *this, { });
    auto name = GetName(ctx);
    return MakeString(name.value_or(""));
}

void Value::Assign(Context& ctx, const Value& rhs)
{
    if (!IsCompatible(rhs))
    {
        ctx.TypeMismatch(*this, rhs);
        return;
    }
    if (const_)
    {
        ctx.RuntimeError(Error::Code::ConstAssign, "Can not assign to constant");
        return;
    }

    if (value_.index() == INDEX_OBJECT && rhs.value_.index() == INDEX_OBJECT)
    {
        const auto& thisObj = std::get<INDEX_OBJECT>(value_);
        const auto& thatObj = std::get<INDEX_OBJECT>(rhs.value_);
        const auto& thisMeta = thisObj->GetMetatable();
        if (thisMeta.InstanceOf(thatObj->GetClassName()))
        {
            // Can only assign an object from the same type
            std::get<INDEX_OBJECT>(value_) = std::get<INDEX_OBJECT>(rhs.value_);
            return;
        }
        ctx.RuntimeError(Error::Code::TypeMismatch, std::format("Can not assign a {} to a {}", thatObj->GetClassName(), thisObj->GetClassName()));
        return;
    }

    if (value_.index() == rhs.value_.index())
    {
        value_ = rhs.value_;
        return;
    }

    if (value_.index() == INDEX_NULL)
    {
        // Only when this is null it can change its type
        value_ = rhs.value_;
        return;
    }

    // Once a variable was defined it does not change its type, but is casted
    std::visit(overloaded{
                   [&](std::nullptr_t)
                   {
                       // Assigning null to an object resets it, but keeps the Meta
                       if (value_.index() == INDEX_OBJECT)
                           std::get<INDEX_OBJECT>(value_)->ResetInstance();
                       else
                           ctx.TypeMismatch(*this, rhs);
                   },
                   [&](bool) {
                       ctx.TypeMismatch(*this, rhs);
                   },
                   [&](Integer arg)
                   {
                       if (value_.index() == INDEX_FLOAT)
                           value_ = static_cast<Float>(arg);
                       else
                           value_ = arg;
                   },
                   [&](Float arg)
                   {
                       if (value_.index() == INDEX_INT)
                           value_ = static_cast<Integer>(arg);
                       else
                           value_ = arg;
                   },
                   [&](const StringPtr&) {
                       ctx.TypeMismatch(*this, rhs);
                   },
                   [&](const CallablePtr&) {
                       ctx.TypeMismatch(*this, rhs);
                   },
                   [&](const TablePtr&) {
                       ctx.TypeMismatch(*this, rhs);
                   },
                   [&](const ObjectPtr&) {
                       ctx.TypeMismatch(*this, rhs);
                   } },
        rhs.value_);
}

void Value::Dump(std::ostream& os)
{
    switch (value_.index())
    {
    case INDEX_NULL:
        os << LITERAL_NULL;
        break;
    case INDEX_BOOL:
        os << (std::get<INDEX_BOOL>(value_) ? LITERAL_TRUE : LITERAL_FALSE);
        break;
    case INDEX_INT:
        os << std::get<INDEX_INT>(value_);
        break;
    case INDEX_FLOAT:
        os << std::get<INDEX_FLOAT>(value_);
        break;
    case INDEX_STRING:
        os << *std::get<INDEX_STRING>(value_);
        break;
    case INDEX_FUNCTION:
        os << "[function]";
        break;
    case INDEX_TABLE:
        os << "[table]";
        break;
    case INDEX_OBJECT:
        os << "[object]";
        break;
    default:
        SATL_ASSERT_FALSE();
    }
}

ValuePtr Value::GetSubscript(Context& ctx, const KeyType& key, bool computed) const
{
    if (computed)
    {
        if (auto op = GetOperator("operator[]"))
            return op->CallWithThis(ctx, *this, { MakeValue(KeyToValue(key)), MakeNull() });
    }

    if (IsTable())
    {
        const auto& table = std::get<INDEX_TABLE>(value_);
        const auto it = table->find(key);
        if (it == table->end())
        {
            ctx.RuntimeError(Error::Code::InvalidSubscript, std::format("Key \"{}\" not found", KeyToString(key)));
            return {};
        }
        it->second->SetOwner(ToContainer());
        // The owner of the subscript shouldn't expire before the subscript
        ctx.StashValue(MakeValue(table));
        return it->second;
    }
    if (IsString())
    {
        if (key.index() != KEY_INT)
        {
            ctx.RuntimeError(Error::Code::TypeMismatch, "Index must be an integer type");
            return {};
        }
        const auto& str = std::get<INDEX_STRING>(value_);
        size_t index = static_cast<size_t>(std::get<KEY_INT>(key));
        if (index < str->length())
            return MakeValue(MakeString(1, (*str)[index]));
        ctx.RuntimeError(Error::Code::IndexOutOfBounds, "Index out of bounds");
        return {};
    }
    if (IsObject())
    {
        const ObjectPtr& o = As<ObjectPtr>();
        if (!o)
        {
            ctx.RuntimeError(Error::Code::NullDereference, "Object is null");
            return {};
        }
        const auto keyString = KeyToString(key);
        // Get property
        if (auto func = o->GetMetatable().GetPropertyGetter(keyString))
            return (*func)(ctx, value_, {});
        // Member function
        if (auto func = o->GetMetatable().GetFunction(keyString))
        {
            auto res = MakeValue(func);
            res->SetOwner(ToContainer());
            ctx.StashValue(MakeValue(*this));
            return res;
        }
        ctx.RuntimeError(Error::Code::InvalidSubscript, std::format("Key \"{}\" not found", keyString));
        return {};
    }
    ctx.RuntimeError(Error::Code::TypeMismatch, "Not a container");
    return {};
}

ValuePtr Value::GetSubscriptRange(Context& ctx, const std::optional<KeyType>& first, const std::optional<KeyType>& last) const
{
    if (first.has_value() && first->index() != KEY_INT)
    {
        ctx.RuntimeError(Error::Code::TypeMismatch, "Range keys must be integers");
        return {};
    }
    if (last.has_value() && last->index() != KEY_INT)
    {
        ctx.RuntimeError(Error::Code::TypeMismatch, "Range keys must be integers");
        return {};
    }

    if (IsTable())
    {
        const auto& table = std::get<INDEX_TABLE>(value_);
        Integer firstValue = first.has_value() ? std::get<Integer>(*first) : 0;
        Integer lastValue = last.has_value() ? std::get<Integer>(*last) : table->size() - 1;
        auto result = MakeTable();
        for (Integer i = firstValue; i <= lastValue; ++i)
        {
            result->Add(GetSubscript(ctx, i, true));
            if (ctx.stop_)
                return {};
        }
        return MakeValue(result);
    }
    if (IsString())
    {
        const auto& str = std::get<INDEX_STRING>(value_);
        Integer firstValue = first.has_value() ? std::get<Integer>(*first) : 0;
        Integer lastValue = last.has_value() ? std::get<Integer>(*last) : str->size() - 1;
        if (firstValue >= lastValue)
        {
            ctx.RuntimeError(Error::Code::TypeMismatch, "Range start must be smaller than range end");
            return {};
        }
        std::string resString = str->substr(firstValue, lastValue - firstValue + 1);
        return MakeValue(resString);
    }
    ctx.RuntimeError(Error::Code::TypeMismatch, "Not a container");
    return {};
}

bool Value::HasSubscript(const KeyType& key, bool computed) const
{
    if (computed && GetOperator("operator[]"))
        return true;

    if (IsTable())
    {
        const auto& table = std::get<INDEX_TABLE>(value_);
        const auto it = table->find(key);
        return it != table->end();
    }
    if (IsString())
    {
        if (key.index() != KEY_INT)
            return false;
        const auto& str = std::get<INDEX_STRING>(value_);
        size_t index = static_cast<size_t>(std::get<KEY_INT>(key));
        return index < str->length();
    }
    if (IsObject())
    {
        // Get property
        const ObjectPtr& o = As<ObjectPtr>();
        if (o)
        {
            if (auto func = o->GetMetatable().GetPropertyGetter(KeyToString(key)))
                return true;
        }
    }
    return false;
}

void Value::SetSubscript(Context& ctx, const KeyType& key, const Value& value, bool computed)
{
    if (computed)
    {
        if (auto op = GetOperator("operator[]"))
        {
            op->CallWithThis(ctx, *this, { MakeValue(KeyToValue(key)), MakeValue(value) });
            return;
        }
    }

    if (IsObject())
    {
        // Set property
        const ObjectPtr& o = As<ObjectPtr>();
        if (!o)
        {
            ctx.RuntimeError(Error::Code::NullDereference, "Object is null");
            return;
        }
        if (auto func = o->GetMetatable().GetPropertySetter(KeyToString(key)))
        {
            (*func)(ctx, value_, { MakeValue(value) });
            return;
        }
        ctx.RuntimeError(Error::Code::InvalidSubscript, std::format("Unknown subscript \"{}\"", KeyToString(key)));
        return;
    }
    if (const_)
    {
        ctx.RuntimeError(Error::Code::ConstAssign, "Can not assign to constant");
        return;
    }
    if (IsTable())
    {
        auto& table = std::get<INDEX_TABLE>(value_);
        (*table)[key] = MakeValue(value);
        return;
    }
    if (IsString())
    {
        if (key.index() != KEY_INT)
        {
            ctx.RuntimeError(Error::Code::TypeMismatch, "Index must be an integer type");
            return;
        }
        if (value.IsString())
        {
            std::string strValue = value.ToString();
            if (strValue.length() != 1)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Value must be one character");
                return;
            }
            auto& str = std::get<INDEX_STRING>(value_);
            size_t index = static_cast<size_t>(std::get<KEY_INT>(key));
            if (index >= str->length())
            {
                ctx.RuntimeError(Error::Code::IndexOutOfBounds, "Index out of bounds");
                return;
            }
            (*str)[index] = strValue[0];
            return;
        }
        if (value.IsInt())
        {
            Integer v = value.ToInt();
            if (v < 0 || v > 255)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Value must be between 0 and 255");
                return;
            }

            auto& str = std::get<INDEX_STRING>(value_);
            size_t index = static_cast<size_t>(std::get<KEY_INT>(key));
            if (index >= str->length())
            {
                ctx.RuntimeError(Error::Code::IndexOutOfBounds, "Index out of bounds");
                return;
            }
            (*str)[index] = v;
            return;
        }
        ctx.RuntimeError(Error::Code::TypeMismatch, "string or integer expected");
        return;
    }
    ctx.RuntimeError(Error::Code::TypeMismatch, "Not a container");
}

ValuePtr Value::Call(Context& ctx, const FunctionArguments& args) const
{
    if (auto owner = GetOwner())
    {
        auto ownerValue = Value::FromContainer(owner);
        if (!ownerValue)
        {
            ctx.RuntimeError(Error::Code::NullDereference, "Function owner is null");
            return {};
        }
        return CallWithThis(ctx, *ownerValue, args);
    }
    return CallNoThis(ctx, args);
}

ValuePtr Value::CallNoThis(Context& ctx, const FunctionArguments& args) const
{
    if (!IsCallable())
    {
        ctx.RuntimeError(Error::Code::UnknownFunction, "Not a function");
        return {};
    }
    const auto& nd = *std::get<INDEX_FUNCTION>(value_);
    return (nd)(ctx, args);
}

ValuePtr Value::CallWithThis(Context& ctx, const Value& thisValue, const FunctionArguments& args) const
{
    if (!IsCallable())
    {
        ctx.RuntimeError(Error::Code::UnknownFunction, "Not a function");
        return {};
    }
    const auto& nd = *std::get<INDEX_FUNCTION>(value_);
    return (nd)(ctx, thisValue, args);
}

ValuePtr Value::GetOperator(const std::string& name) const
{
    if (value_.index() == INDEX_TABLE)
    {
        const TablePtr& tbl = std::get<INDEX_TABLE>(value_);
        const auto opIt = tbl->find(name);
        if (opIt == tbl->end())
            return {};
        if (!opIt->second->IsCallable())
            return {};
        return opIt->second;
    }
    if (value_.index() == INDEX_OBJECT)
    {
        const ObjectPtr& obj = std::get<INDEX_OBJECT>(value_);
        if (auto op = obj->GetMetatable().GetOperator(name))
            return MakeValue(op);
    }
    return {};
}

void Value::SetConst(bool value, bool recursive)
{
    if (value == const_)
        return;
    const_ = value;
    if (recursive && IsTable())
    {
        for (auto& item : *As<TablePtr>())
            item.second->SetConst(value);
    }
}

bool Value::CheckConst(Context& ctx) const
{
    if (const_ && !IsObject())
    {
        ctx.RuntimeError(Error::Code::ConstAssign, "Can not assign to constant");
        return false;
    }
    return true;
}

std::optional<std::string> Value::GetName(const Context& ctx) const
{
    if (IsCallable())
    {
        const auto& name = As<CallablePtr>()->GetName();
        if (!name.empty())
            return name;
    }
    if (auto owner = GetOwner())
    {
        if (owner->IsTable())
        {
            const auto& table = static_cast<const Table&>(*owner);
            auto key = table.GetValueKey(*this);
            if (key.has_value() && key->index() == KEY_STRING)
                return std::get<KEY_STRING>(*key);
        }
        return {};
    }
    return ctx.GetValueName(*this);
}

TablePtr MakeTable(const Error& error)
{
    auto result = MakeTable();
    result->Add<Integer>("code", error.code);
    result->Add("file", error.loc.file);
    result->Add<Integer>("line", error.loc.line);
    result->Add<Integer>("col", error.loc.col);
    result->Add("message", error.message);
    return result;
}

}
