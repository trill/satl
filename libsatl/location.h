/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstddef>
#include <string>
#include <ostream>

namespace sa::tl {

// Zero based
struct Location
{
    std::string file;
    size_t line = 0;
    size_t col = 0;

    int operator-(const Location& rhs) const
    {
        int result = file.compare(rhs.file) * 10000;
        result += (int)(line - rhs.line) * 1000;
        result += (int)(col - rhs.col);
        return result;
    }
    bool operator ==(const Location& rhs) const
    {
        if (col != rhs.col)
            return false;
        if (line != rhs.line)
            return false;
        if (file != rhs.file)
            return false;
        return true;
    }
    bool operator !=(const Location& rhs) const
    {
        return !(*this == rhs);
    }
    bool operator <(const Location& rhs) const
    {
        return *this - rhs < 0;
    }
    bool operator >(const Location& rhs) const
    {
        return *this - rhs > 0;
    }
    bool operator <=(const Location& rhs) const
    {
        return (*this == rhs) || (*this - rhs < 0);
    }
    bool operator >=(const Location& rhs) const
    {
        return (*this == rhs) || (*this - rhs > 0);
    }
    friend std::ostream& operator<<(std::ostream& os, const Location& value)
    {
        // Lines are zero based
        if (!value.file.empty())
            os << value.file << ":";
        os << value.line + 1 << "," << value.col + 1;
        return os;
    }
};

}
