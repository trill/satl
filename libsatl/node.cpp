/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "node.h"
#include "context.h"

namespace sa::tl {

void Callable::Enter(Context& ctx) const
{
    ctx.CallEnter(*this);
}

void Callable::Leave(Context& ctx) const
{
    ctx.CallLeave(*this);
}

bool Callable::ExecutionAllowed(Context& ctx, bool safe)
{
    if (!ctx.sandboxed_ || safe)
        return true;
    RuntimeError(ctx, Error::Code::UnsafeFunction, "Calling an unsafe function");
    return false;
}

void Callable::RuntimeError(Context& ctx, int code, std::string message)
{
    ctx.RuntimeError(code, std::forward<std::string>(message));
}

}
