/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "metatable.h"
#include "context.h"

namespace sa::tl {

ValuePtr MemberFunction::operator()(Context& ctx, const FunctionArguments& args) const
{
    // We keep this, because then the call() function of the std lib can call
    // member functions.
    // First argument must be the this pointer
    SATL_ASSERT(!args.empty());
    return (*this)(ctx, *args[0], FunctionArguments(args.begin() + 1, args.end()));
}

ValuePtr MemberFunction::operator()(Context& ctx, const Value& thisValue, const FunctionArguments& args) const
{
    if (!ExecutionAllowed(ctx, safe_))
        return MakeNull();
    SATL_ASSERT(thisValue.Is<ObjectPtr>());
    const auto& objPtr = thisValue.As<ObjectPtr>();
    SATL_ASSERT(objPtr);
    if (objPtr->GetInstance() == nullptr)
    {
        ctx.RuntimeError(Error::Code::NullDereference, "Object of member function is null");
        return MakeNull();
    }
    try
    {
        CALLABLE_CALLGUARD(ctx);
        return (*this)(objPtr, args);
    }
    catch (const Exception& ex)
    {
        ctx.RuntimeError(ex.GetCode(), ex.what());
    }
    return MakeNull();
}

MemberFunctionPtr MetaTable::GetFunction(const std::string& name) const
{
    const auto it = functions_.find(name);
    if (it != functions_.end())
        return it->second;
    if (base_)
        return base_->GetFunction(name);
    return {};
}

MemberFunctionPtr MetaTable::GetOperator(const std::string& name) const
{
    const auto it = operators_.find(name);
    if (it != operators_.end())
        return it->second;
    if (base_)
        return base_->GetOperator(name);
    return {};
}

MemberFunctionPtr MetaTable::GetPropertyGetter(const std::string& name) const
{
    const auto it = propertyGetters_.find(name);
    if (it != propertyGetters_.end())
        return it->second;
    if (base_)
        return base_->GetPropertyGetter(name);
    return {};
}

MemberFunctionPtr MetaTable::GetPropertySetter(const std::string& name) const
{
    const auto it = propertySetters_.find(name);
    if (it != propertySetters_.end())
        return it->second;
    if (base_)
        return base_->GetPropertySetter(name);
    return {};
}

}
