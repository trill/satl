/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <string>
#include <optional>
#include <vector>
#include "satl_assert.h"

#pragma once

namespace sa::tl {

bool FileExists(const std::string& name);
bool DirExists(const std::string& name);
bool HiddenFile(const std::string& name);
std::string ExtractFileName(const std::string& name);
std::string ExtractFileExt(const std::string& name);
std::string ChangeFileExt(const std::string& name, const std::string& ext);
std::string ParentPath(const std::string& name);
std::string AddSlash(const std::string& name);
std::string FindRelatedFile(const std::string& base, const std::string& name);
std::string ReadScriptFile(const std::string& filename);
std::string ConcatPath(const std::string& path, const std::string& name);
std::vector<std::string> Split(const std::string& str,
    const std::string& delim,
    bool keepEmpty = false,
    bool keepWhite = true);
bool IsWhite(const std::string& s);

template<typename ContainerT, typename PredicateT>
bool DeleteIf(ContainerT& items, const PredicateT& predicate)
{
    bool result = false;
    for (auto it = items.begin(); it != items.end();)
    {
        if (predicate(*it))
        {
            result = true;
            it = items.erase(it);
        }
        else
            ++it;
    }
    return result;
}

template<typename charType>
inline charType ToLower(charType)
{
    SATL_ASSERT_FALSE();
}
template<>
inline char ToLower<char>(char c)
{
    return static_cast<char>(std::tolower(c));
}
template<>
inline wchar_t ToLower<wchar_t>(wchar_t c)
{
    return static_cast<wchar_t>(std::towlower(c));
}
template<typename charType>
inline charType ToUpper(charType)
{
    SATL_ASSERT_FALSE();
}
template<>
inline char ToUpper<char>(char c)
{
    return static_cast<char>(std::toupper(c));
}
template<>
inline wchar_t ToUpper<wchar_t>(wchar_t c)
{
    return static_cast<wchar_t>(std::towupper(c));
}

template<typename charType>
inline std::basic_string<charType> StringToUpper(const std::basic_string<charType>& s)
{
    std::basic_string<charType> result;
    result.resize(s.length());
    for (size_t i = 0; i < s.length(); ++i)
        result[i] = ToUpper<charType>(s[i]);
    return result;
}
template<typename charType>
inline std::basic_string<charType> StringToLower(const std::basic_string<charType>& s)
{
    std::basic_string<charType> result;
    result.resize(s.length());
    for (size_t i = 0; i < s.length(); ++i)
        result[i] = ToLower<charType>(s[i]);
    return result;
}

template<typename charType>
inline bool PatternMatch(const std::basic_string<charType>& string, const std::basic_string<charType>& pattern)
{
    // http://xoomer.virgilio.it/acantato/dev/wildcard/wildmatch.html#evolution
    bool star = false;
    const charType* s = nullptr;
    const charType* p = nullptr;
    const charType* str = string.c_str();
    const charType* pat = pattern.c_str();

loopStart:
    for (s = str, p = pat; *s; ++s, ++p)
    {
        switch (*p)
        {
        case '?':
            if (*s == '.')
                goto starCheck;
            break;
        case '*':
            star = true;
            str = s, pat = p;
            do
            {
                ++pat;
            } while (*pat == '*');
            if (!*pat)
                return true;
            goto loopStart;
        default:
            if (ToLower<charType>(*s) != ToLower<charType>(*p))
                goto starCheck;
            break;
        }
    }
    while (*p == '*')
        ++p;
    return (!*p);
starCheck:
    if (!star)
        return false;
    str++;
    goto loopStart;
}

/// Replace all occurrences of search with replace in subject
/// Returns true if it replaced something
template<typename charType>
inline bool ReplaceSubstring(std::basic_string<charType>& subject,
    const std::basic_string<charType>& search,
    const std::basic_string<charType>& replace)
{
    if (search.empty())
        return false;
    bool result = false;

    using string_type = std::basic_string<charType>;
    string_type newString;
    newString.reserve(subject.length());

    size_t lastPos = 0;
    size_t findPos = 0;

    while ((findPos = subject.find(search, lastPos)) != string_type::npos)
    {
        newString.append(subject, lastPos, findPos - lastPos);
        newString += replace;
        lastPos = findPos + search.length();
        result = true;
    }

    newString += subject.substr(lastPos);

    subject.swap(newString);
    return result;
}

template<typename charType>
inline std::basic_string<charType> Trim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    // Left
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return std::basic_string<charType>(); // no content

    // Right
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

template<typename charType>
inline std::basic_string<charType> LeftTrim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::basic_string<charType>::npos)
        return ""; // no content

    return str.substr(strBegin, std::basic_string<charType>::npos);
}

template<typename charType>
inline std::basic_string<charType> RightTrim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    const auto strEnd = str.find_last_not_of(whitespace);
    if (strEnd == std::basic_string<charType>::npos)
        return ""; // no content

    return str.substr(0, strEnd + 1);
}

// Case insensitive string comparison
template<typename charType>
inline bool StringEquals(const std::basic_string<charType>& str, const std::basic_string<charType>& str2)
{
    if (str.size() != str.size())
        return false;
    auto lowerS1 = StringToLower(str);
    auto lowerS2 = StringToLower(str2);
    return lowerS1 == lowerS2;
}

std::string ExpandPath(const char* path);

template<typename T>
inline std::optional<T> ToNumber(const std::string& number)
{
    T result;
    auto [ptr, error] = std::from_chars(number.data(), number.data() + number.size(), result);
    if (error != std::errc{})
        return {};
    return result;
}

}
