/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parser.h"
#include "scope_guard.h"
#include "ast.h"
#include "template_parser.h"
#include "number_parser.h"

namespace sa::tl {

bool Parser::IsAssignment(const TokenIterator& it)
{
    if (it->type == Token::Type::Assign)
        return true;
    if (it->type == Token::Type::Colon && (it + 1)->type == Token::Type::Ident)
        return (it + 2)->type == Token::Type::Assign;
    return false;
}

bool Parser::IsValue(const Token& token)
{
    if (token.type == Token::Type::Number)
        return true;

    if (token.type == Token::Type::String || token.type == Token::Type::TemplateString)
        return true;

    if (token.type == Token::Type::Ident)
    {
        if (token.value == LITERAL_TRUE || token.value == LITERAL_FALSE)
            return true;
        if (token.value == LITERAL_NULL)
            return true;
    }
    return false;
}

bool Parser::IsUnaryOperator(const std::string& name)
{
    return name == "++operator"
        || name == "--operator"
        || name == "operator++"
        || name == "operator--"
        || name == "+operator"
        || name == "-operator"
        || name == "!operator"
        || name == "#operator"
        || name == "$operator"
        || name == "$$operator";
}

int Parser::GetOperatorParamCount(const std::string& name)
{
    if (name == "operator[]")
        // Computed member access takes 2 arguments: key and rhs
        return 2;
    if (IsUnaryOperator(name))
        return 0;
    return 1;
}

Parser::Parser(std::string_view in) : lexer_(in, "")
{
    lexer_.onError = [this](size_t, const std::string& file, size_t line, size_t col, std::string message)
    {
        errors_.push_back({ Error::Type::SyntaxError, Error::Code::SyntaxError, Location{ file, line, col }, std::move(message), {} });
    };
}

Parser::Parser(std::string_view in, const std::string& file) : lexer_(in, file)
{
    lexer_.onError = [this](size_t, const std::string& file, size_t line, size_t col, std::string message)
    {
        errors_.push_back({ Error::Type::SyntaxError, Error::Code::SyntaxError, Location{ file, line, col }, std::move(message), {} });
    };
}

Parser::~Parser() = default;

bool Parser::Lex()
{
    lexer_.Reset();
    tokens_.clear();
    Token token = lexer_.Next();
    while (token.type != Token::Type::Eof)
    {
        if (token.type == Token::Type::Invalid)
            // Syntax error
            return false;
        if (token.type == Token::Type::BlockComment || token.type == Token::Type::LineComment)
            comments_.push_back(std::move(token));
        else
            tokens_.push_back(std::move(token));
        token = lexer_.Next();
    }
    // Add EOF
    tokens_.push_back(std::move(token));
    return true;
}

NodePtr Parser::Parse()
{
    Context ctx;
    return InternalParse(ctx);
}

NodePtr Parser::InternalParse(Context& ctx)
{
    ctx_ = &ctx;
    if (!Lex())
        return {};

    std::shared_ptr<Root> root = CreateNode<Root>({ lexer_.GetFilename(), 0, 0 });
    for (auto it = tokens_.begin(); it != tokens_.end();)
    {
        if (it->type == Token::Type::Eof)
            break;

        auto node = ParseStatement(it);
        if (!errors_.empty())
            return {};
        if (node)
            root->Add(std::move(node));
        if (it->type == Token::Type::Semicolon)
            ++it;
    }

    return root;
}

Token Parser::PreviousToken(const TokenIterator& it)
{
    if (it == tokens_.begin())
        return {};
    return *(it - 1);
}

NodePtr Parser::ParseExpression()
{
    if (!Lex())
        return {};
    auto it = tokens_.begin();
    Context ctx;
    ctx_ = &ctx;
    return ParseExpression(it);
}

NodePtr Parser::ParseTable(bool safe)
{
    if (!Lex())
        return {};
    auto it = tokens_.begin();
    Context ctx;
    ctx.safeTables = safe;
    ctx_ = &ctx;
    return ParseTableInit(it);
}

NodePtr Parser::ParseStatement(TokenIterator& it)
{
    Keyword keyword = GetKeyword(*it);
    switch (keyword)
    {
    case Keyword::If:
        return ParseIf(it);
    case Keyword::For:
        return ParseFor(it);
    case Keyword::ForEach:
        return ParseForEach(it);
    case Keyword::While:
        return ParseWhile(it);
    case Keyword::Do:
        return ParseDo(it);
    case Keyword::Function:
        return ParseFunction(it);
    case Keyword::Switch:
        return ParseSwitch(it);
    case Keyword::Include:
        return ParseInclude(it);
    case Keyword::Using:
        return ParseUsing(it);
    case Keyword::Defer:
        return ParseDefer(it);
    case Keyword::Return:
        if (state_.returnable != 0)
            return ParseReturn(it);
        else
            SYNTAX_ERROR(it->loc, "return outside of function");
    case Keyword::Break:
        if (state_.breakable != 0)
            return ParseBreak(it);
        else
            SYNTAX_ERROR(it->loc, "break without for, do or while");
    case Keyword::Continue:
        if (state_.continueable != 0)
            return ParseContinue(it);
        else
            SYNTAX_ERROR(it->loc, "continue without for, do or while");
    case Keyword::Fallthrough:
        if (state_.fallthroughable != 0)
            return ParseFallthrough(it);
        else
            SYNTAX_ERROR(it->loc, "fallthrough without switch .. case");
    case Keyword::Else:
        SYNTAX_ERROR(it->loc, "else without if");
    case Keyword::Case:
        SYNTAX_ERROR(it->loc, "case without switch");
    case Keyword::Const:
        return ParseConstAssign(it);
    case Keyword::Global:
        return ParseGlobalAssign(it);
    case Keyword::Default:
        SYNTAX_ERROR(it->loc, "default without switch");
    case Keyword::Try:
        return ParseTry(it);
    case Keyword::Catch:
        SYNTAX_ERROR(it->loc, "catch without try");
    case Keyword::Finally:
        SYNTAX_ERROR(it->loc, "finally without try");
    case Keyword::Operator:
        SYNTAX_ERROR(it->loc, "operator outside table");
    case Keyword::Constructor:
        SYNTAX_ERROR(it->loc, "constructor outside table");
    case Keyword::Once:
        SYNTAX_ERROR(it->loc, "once without include");
    case Keyword::In:
        SYNTAX_ERROR(it->loc, "Unexpected in keyword");
    case Keyword::None:
        break;
    }

    if (it->type == Token::Type::CurlyOpen)
        // New scope
        return ParseBlock(it, true);
    if (it->type == Token::Type::Semicolon)
    {
        // Empty statement is ok
        ++it;
        return {};
    }
    if (IsValue(*it))
        SYNTAX_ERROR(it->loc, std::format("Expected statement not literal \"{}\"", it->value));

    auto result = ParseExpression(it);
    if (!result)
        SYNTAX_ERROR(it->loc, std::format("Expected statement not \"{}\"", it->value));

    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    return result;
}

bool Parser::HasInclude(const std::string& filename) const
{
    return std::find_if(ctx_->includes.begin(), ctx_->includes.end(), [&filename](const std::string& current) {
        return current == filename;
    }) != ctx_->includes.end();
}

NodePtr Parser::ParseInclude(TokenIterator& it)
{
    ++it;
    bool once = false;
    if (GetKeyword(*it) == Keyword::Once)
    {
        ++it;
        once = true;
    }
    if (!Expect(*it, Token::Type::String))
        return {};
    if (!onInclude_)
        SYNTAX_ERROR(it->loc, "No onInclude_ handler assigned");
    auto filename = it->value;
    auto loc = it->loc;;
    ++it;
    if (!Expect(*it, Token::Type::Semicolon))
        return {};

    auto contents = onInclude_(filename, loc);
    if (once && HasInclude(filename))
        return {};

    ctx_->includes.push_back(filename);
    if (std::holds_alternative<NodePtr>(contents))
        return std::get<NodePtr>(contents);
    Parser parser(std::get<std::string>(contents), filename);
    auto root = parser.InternalParse(*ctx_);
    for (const auto& e : parser.errors_)
    {
        auto ince = e;
        ince.included = loc;
        errors_.push_back(std::move(ince));
    }
    return root;
}

NodePtr Parser::ParseTry(TokenIterator& it)
{
    auto result = CreateNode<Try>(it->loc, ParseBlock(++it, true));
    Keyword keyword = GetKeyword(*it);
    while (keyword == Keyword::Catch)
    {
        Try::CatchBlock block;
        int err = 0;
        ++it;
        if (it->type == Token::Type::ParenOpen)
        {
            ++it;
            if (!ExpectIdent(*it))
                return {};
            block.ident = it->value;
            ++it;
            if (it->type == Token::Type::CompEquals)
            {
                ValuePtr value = ParseValueOrIdent(++it);
                if (!value->IsInt())
                    SYNTAX_ERROR(it->loc, "Integer literal expected");
                err = (int)value->ToInt();
            }
            if (!Expect(*it, Token::Type::ParenClose))
                return {};
            ++it;
        }
        block.block = ParseBlock(it, true);
        if (!result->AddCatch(err, std::move(block)))
            SYNTAX_ERROR(it->loc, std::format("Duplicate catch block for error {}", err));
        keyword = GetKeyword(*it);
    }
    if (keyword == Keyword::Finally)
    {
        ++it;
        result->SetFinally(ParseBlock(it, true));
    }
    if (!result->IsValid())
        SYNTAX_ERROR(it->loc, "try needs a catch and/or finally block");
    return result;
}

NodePtr Parser::ParseUsing(TokenIterator& it)
{
    /*
     using [ident = ]expression;
     */
    auto result = CreateNode<Using>(it->loc);
    auto next = (it + 1);
    if (IsIdent(*next) && (next + 1)->type == Token::Type::Assign)
    {
        ++it;
        result->SetIdent(ParseVariableDefinition(it, false));
    }
    result->SetEpxr(ParseExpression(++it));
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    return result;
}

NodePtr Parser::ParseDefer(TokenIterator& it)
{
    auto result = CreateNode<Defer>(it->loc);
    result->SetBlock(ParseBlock(++it, false));
    return result;
}

NodePtr Parser::ParseVariableDefinition(TokenIterator& it, bool typeHint)
{
    std::string symbol = it->value;
    auto result = CreateNode<VariableDefinition>(it->loc, std::move(symbol));
    ++it;
    if (typeHint && it->type == Token::Type::Colon)
    {
        ++it;
        result->typeHint_ = GetTypeHint(*it);
        ++it;
    }
    return result;
}

NodePtr Parser::ParseConstAssign(TokenIterator& it, bool isGlobal)
{
    ++it;
    if (!ExpectIdent(*it))
        return {};
    auto result = ParseExpression(it);
    To<Assign>(*result).SetConst(true);
    To<Assign>(*result).SetGlobal(isGlobal);
    return result;
}

NodePtr Parser::ParseGlobalAssign(TokenIterator& it)
{
    ++it;
    if (GetKeyword(*it) == Keyword::Const)
        return ParseConstAssign(it, true);
    if (!ExpectIdent(*it))
        return {};
    auto result = ParseExpression(it);
    To<Assign>(*result).SetGlobal(true);
    return result;
}

NodePtr Parser::ParseTableInit(TokenIterator& it)
{
    auto parseTypeHint = [this](TokenIterator& it) -> TypeHint
    {
        TypeHint result = TypeHint::Any;
        if (it->type == Token::Type::Colon)
        {
            ++it;
            result = GetTypeHint(*it);
            ++it;
        }
        return result;
    };

    auto result = CreateNode<TableInitialization>(it->loc);
    while (it->type != Token::Type::CurlyClose)
    {
        ++it;
        if (it->type == Token::Type::Eof)
            SYNTAX_ERROR(it->loc, "Unexpected end of file");
        if (HasErrors())
            return {};

        if ((GetKeyword(*it) == Keyword::Constructor))
        {
            if (ctx_->safeTables)
                SYNTAX_ERROR(it->loc, "Unexpected constructor");

            // constructor = function

            TypeHint typeHint = parseTypeHint(it);

            ++it;
            if (!Expect(*it, Token::Type::Assign))
                return {};
            ++it;
            if (!Expect(*it, Keyword::Function))
                return {};
            if (result->HasKey(MakeString(KEYWORD_CONSTRUCTOR)))
                SYNTAX_ERROR(it->loc, std::format("Duplicate key in table initialization \"{}\"", KEYWORD_CONSTRUCTOR));
            result->Add(MakeValue(std::string(KEYWORD_CONSTRUCTOR)), ParseFunction(it), typeHint);
        }
        else if ((GetKeyword(*it) == Keyword::Operator) && (IsOperator(*(it + 1))))
        {
            if (ctx_->safeTables)
                SYNTAX_ERROR(it->loc, "Unexpected operator");
            // operator<something>
            ++it;
            std::string opName;
            if (it->type == Token::Type::BracketOpen && (it + 1)->type == Token::Type::BracketClose)
            {
                ++it;
                opName = "operator[]";
            }
            else
                opName = "operator" + it->value;

            int nParams = GetOperatorParamCount(opName);
            TypeHint typeHint = parseTypeHint(it);

            ++it;
            if (!Expect(*it, Token::Type::Assign))
                return {};
            ++it;
            if (!Expect(*it, Keyword::Function))
                return {};

            if (result->HasKey(MakeString(opName)))
                SYNTAX_ERROR(it->loc, std::format("Duplicate key in table initialization \"{}\"", opName));
            result->Add(MakeValue(opName), ParseFunction(it, nParams), typeHint);
        }
        else if (IsOperatorSymbol(*it) && (GetKeyword(*(it + 1)) == Keyword::Operator))
        {
            if (ctx_->safeTables)
                SYNTAX_ERROR(it->loc, "Unexpected operator");
            // <something>operator
            std::string opName = it->value + "operator";

            ++it;

            TypeHint typeHint = parseTypeHint(it);

            ++it;
            if (!Expect(*it, Token::Type::Assign))
                return {};
            ++it;
            if (!Expect(*it, Keyword::Function))
                return {};
            if (result->HasKey(MakeString(opName)))
                SYNTAX_ERROR(it->loc, std::format("Duplicate key in table initialization \"{}\"", opName));
            result->Add(MakeValue(opName), ParseFunction(it, GetOperatorParamCount(opName)), typeHint);
        }
        else
        {
            ValuePtr key;
            TypeHint typeHint = TypeHint::Any;
            if ((IsIdent(*it) || IsValue(*it))
                && ((it + 1)->type == Token::Type::Assign || (it + 1)->type == Token::Type::Colon))
            {
                // An assignment here means: key = value
                // In a table initialization a key must be a literal. We also accppet identifiers
                // because the quotes may be omitted for string keys.
                if (!ExptectValueOrIdent(*it))
                    return {};
                key = ParseValueOrIdent(it);
                typeHint = parseTypeHint(it);
                ++it;
            }
            if (it->type == Token::Type::Hash)
            {
                if (key && result->HasKey(*key))
                    SYNTAX_ERROR(it->loc, std::format("Duplicate key in table initialization \"{}\"", key->ToString()));
                auto item = CreateNode<ValueExpr>(it->loc, MakeValue<Integer>(result->GetCount()));
                result->Add(key, std::move(item), typeHint);
                ++it;
            }
            else if (it->type != Token::Type::CurlyClose)
            {
                if (!ctx_->safeTables)
                {
                    if (key && result->HasKey(*key))
                        SYNTAX_ERROR(it->loc, std::format("Duplicate key in table initialization \"{}\"", key->ToString()));
                    result->Add(key, ParseExpression(it), typeHint);
                }
                else
                {
                    if (GetKeyword(*it) == Keyword::Function)
                        SYNTAX_ERROR(it->loc, "Unexpected function");

                    if (key && result->HasKey(*key))
                        SYNTAX_ERROR(it->loc, std::format("Duplicate key in table initialization \"{}\"", key->ToString()));
                    result->Add(key, ParseExpression(it), typeHint);
                }
            }
        }
        if (it->type != Token::Type::CurlyClose && !Expect(*it, Token::Type::Comma))
            return {};
    }
    ++it;
    return result;
}

NodePtr Parser::ParseIf(TokenIterator& it)
{
    /*
    if (expr)
    {
        true block;
    }
    [else
    {
        false block;
    }]
    */
    auto result = CreateNode<If>(it->loc);
    ++it;
    if (!Expect(*it, Token::Type::ParenOpen))
        return {};
    ++it;

    result->SetExpr(ParseExpression(it));
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    result->SetTrueBranch(ParseBlock(it, false));
    if (it->type == Token::Type::Semicolon)
        ++it;

    if (GetKeyword(*it) == Keyword::Else)
    {
        ++it;
        result->SetFalseBranch(ParseStatement(it));
    }

    return result;
}

NodePtr Parser::ParseReturn(TokenIterator& it)
{
    /*
     * return [expr];
     */
    auto result = CreateNode<Return>(it->loc);
    ++it;
    if (it->type == Token::Type::Semicolon)
        return result;

    result->SetExpression(ParseExpression(it));
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    return result;
}

NodePtr Parser::ParseBreak(TokenIterator& it)
{
    ++it;
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    return CreateNode<Break>((it - 1)->loc);
}

NodePtr Parser::ParseFallthrough(TokenIterator& it)
{
    ++it;
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    return CreateNode<Fallthrough>((it - 1)->loc);
}

NodePtr Parser::ParseContinue(TokenIterator& it)
{
    auto result = CreateNode<Continue>(it->loc);
    ++it;
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    return result;
}

NodePtr Parser::ParseSwitch(TokenIterator& it)
{
    /*
    switch (expression)
    {
        case Label:
        {
            do things;
            fallthrough;
        }
        default:
        {
            other things;
        }
    }
    */

    auto result = CreateNode<Switch>(it->loc);
    ++it;
    if (!Expect(*it, Token::Type::ParenOpen))
        return {};
    ++it;
    result->SetExpression(ParseExpression(it));
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    if (!Expect(*it, Token::Type::CurlyOpen))
        return {};
    ++it;

    while (it->type != Token::Type::CurlyClose)
    {
        if (HasErrors())
            return {};
        Keyword keyword = GetKeyword(*it);
        if (keyword != Keyword::Case && keyword != Keyword::Default)
            SYNTAX_ERROR(it->loc, "case or default expected");
        ++it;
        if (it->type == Token::Type::Eof)
            SYNTAX_ERROR(it->loc, "Unexpected end of file");

        if (keyword == Keyword::Case)
        {
            ++state_.fallthroughable;
            ScopeGuard b([this]() { --state_.fallthroughable; });
            auto labelExpr = ParseExpression(it);
            if (!Expect(*it, Token::Type::Colon))
                return {};
            ++it;
            if (auto* le = To<ValueExpr>(labelExpr.get()))
            {
                // When the case label is a literal we can check for duplicates
                if (result->HasLabel(*le->GetValue()))
                    SYNTAX_ERROR(it->loc, std::format("Duplicate case label \"{}\"", le->GetValue()->ToString()));
            }
            result->AddBlock(std::move(labelExpr), ParseBlock(it, true));
        }
        else if (keyword == Keyword::Default)
        {
            if (!Expect(*it, Token::Type::Colon))
                return {};
            if (!result->HasDefault())
                result->SetDefault(ParseBlock(++it, true));
            else
                SYNTAX_ERROR(it->loc, "Duplicate default in switch statement");
        }
        else
        {
            SYNTAX_ERROR(it->loc, std::format("Unexpected \"{}\"", it->value));
        }
    }
    // Consume }
    ++it;

    return result;
}

NodePtr Parser::ParseFor(TokenIterator& it)
{
    /*
    for (init; condition; increment)
    {
        block;
    }
    */

    ++state_.breakable;
    ++state_.continueable;
    ScopeGuard b(
        [this]()
        {
            --state_.breakable;
            --state_.continueable;
        });
    auto result = CreateNode<For>(it->loc);

    ++it;
    if (!Expect(*it, Token::Type::ParenOpen))
        return {};
    ++it;
    if (it->type != Token::Type::Semicolon)
        result->SetInit(ParseExpression(it));
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    ++it;
    if (it->type != Token::Type::Semicolon)
        result->SetCondition(ParseExpression(it));
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    ++it;
    if (it->type != Token::Type::ParenClose)
        result->SetIncrement(ParseExpression(it));
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    result->SetBlock(ParseBlock(it, false));

    return result;
}

NodePtr Parser::ParseForEach(TokenIterator& it)
{
    /*
     foreach ([key,] value in container)
     {
         block;
     }
     */

    ++state_.breakable;
    ++state_.continueable;
    ScopeGuard b(
        [this]()
        {
            --state_.breakable;
            --state_.continueable;
        });
    ++it;
    auto result = CreateNode<ForEach>(it->loc);
    if (!Expect(*it, Token::Type::ParenOpen))
        return {};
    ++it;
    if (!ExpectIdent(*it))
        return {};

    auto first = ParseVariableDefinition(it);
    NodePtr second;
    if (it->type == Token::Type::Comma)
    {
        ++it;
        second = ParseVariableDefinition(it);
    }
    if (!Expect(*it, Keyword::In))
        return {};
    if (second)
    {
        result->SetKey(first);
        result->SetValue(second);
    }
    else
    {
        result->SetValue(first);
    }
    ++it;

    if (it->type == Token::Type::Tilde)
    {
        // Note: The foreach statement should reverse iterate instead of creating a reverse copy.
        // This way also the keys are preserved.
        result->SetReverse();
        ++it;
    }
    if (IsIdent(*it) && (it + 1)->type == Token::Type::Assign)
        // e.g. foreach (key, value in tab = { 1, 2, 3, 4 })
        result->SetContainer(ParseExpression(it));
    else
        result->SetContainer(ParseExpression(it));
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    result->SetBlock(ParseBlock(it, false));

    return result;
}

NodePtr Parser::ParseWhile(TokenIterator& it)
{
    /*
    while (expr)
    {
        block;
    }
    */

    ++state_.breakable;
    ++state_.continueable;
    ScopeGuard b(
        [this]()
        {
            --state_.breakable;
            --state_.continueable;
        });

    auto result = CreateNode<While>(it->loc);
    ++it;
    if (!Expect(*it, Token::Type::ParenOpen))
        return {};
    ++it;
    result->SetCondition(ParseExpression(it));
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    result->SetBlock(ParseBlock(it, false));

    return result;
}

NodePtr Parser::ParseDo(TokenIterator& it)
{
    /*
    do
    {
        block;
    } while(expr);
    */

    ++state_.breakable;
    ++state_.continueable;
    ScopeGuard b(
        [this]()
        {
            --state_.breakable;
            --state_.continueable;
        });

    auto result = CreateNode<Do>(it->loc);
    ++it;
    bool haveCurly = it->type == Token::Type::CurlyOpen;
    auto block = ParseBlock(it, haveCurly);
    if (!haveCurly)
    {
        if (!static_cast<Block&>(*block).IsEmpty())
        {
            // If we dont have curlies it's up to us to parse the semicolon.
            Expect(*it, Token::Type::Semicolon);
            ++it;
        }
        // If the block is empty it may be something like this:
        // do ; while (false);
        // Then the semicolon is eaten by ParseStatement()
    }
    result->SetBlock(std::move(block));

    Keyword kw = GetKeyword(*it);
    if (kw != Keyword::While)
        SYNTAX_ERROR(it->loc, "while expected");
    ++it;
    if (!Expect(*it, Token::Type::ParenOpen))
        return {};
    result->SetCondition(ParseExpression(it));
    --it;
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    if (!Expect(*it, Token::Type::Semicolon))
        return {};
    ++it;
    return result;
}

NodePtr Parser::ParseFunction(TokenIterator& it, int numParams)
{
    /*
    function [name]([arguments...])
    {
        block;
    }
    */

    ++state_.returnable;
    ScopeGuard b([this]() { --state_.returnable; });

    ++it;
    auto result = CreateNode<Function>(it->loc);
    if (IsIdent(*it))
    {
        result->SetName(it->value);
        ++it;
    }

    // Captures
    if (it->type == Token::Type::BracketOpen)
    {
        ++it;
        while (it->type != Token::Type::BracketClose)
        {
            switch (it->type)
            {
            case Token::Type::Ident:
            {
                if (!ExpectIdent(*it))
                    return {};
                result->AddCapture(CreateNode<VariableExpr>(it->loc, it->value));
                break;
            }
            case Token::Type::Comma:
                break;
            default:
                SYNTAX_ERROR(it->loc, std::format("Unexpected {}", TokenTypeName(it->type)));
            }
            ++it;
        }
        ++it;
    }

    if (it->type == Token::Type::ParenOpen)
    {
        ++it;
        while (it->type != Token::Type::ParenClose)
        {
            switch (it->type)
            {
            case Token::Type::Ident:
                if (!ExpectIdent(*it))
                    return {};
                result->AddParameter(std::static_pointer_cast<VariableDefinition>(ParseVariableDefinition(it)));
                --it;
                break;
            case Token::Type::Comma:
                break;
            default:
                SYNTAX_ERROR(it->loc, std::format("Unexpected {}", TokenTypeName(it->type)));
            }
            ++it;
        }
        ++it;
    }

    if (numParams != -1)
    {
        if (result->ParamCount() != numParams)
        {
            SYNTAX_ERROR(it->loc, std::format("Wrong number of paramters, expecting {} but got {}", numParams, result->ParamCount()));
        }
    }
    // Return value type hint
    if (it->type == Token::Type::Colon)
    {
        ++it;
        result->typeHint_ = GetTypeHint(*it);
        ++it;
    }
    result->SetBody(ParseBlock(it, true));

    if (!result->HasName() && it->type == Token::Type::ParenOpen)
        // Function expression followed by a call expression
        return ParseCallExpression(it, result);

    return result;
}

NodePtr Parser::ParseBlock(TokenIterator& it, bool requireCurly)
{
    auto result = CreateNode<Block>(it->loc);
    bool needCurly = false;
    if (requireCurly)
    {
        needCurly = true;
        if (!Expect(*it, Token::Type::CurlyOpen))
            return {};
        ++it;
    }
    else if (it->type == Token::Type::CurlyOpen)
    {
        ++it;
        needCurly = true;
    }

    if (!needCurly)
    {
        // No curly -> single statement
        if (auto nd = ParseStatement(it))
            result->Add(std::move(nd));
        return result;
    }

    while (it->type != Token::Type::CurlyClose)
    {
        if (it->type == Token::Type::Eof)
            SYNTAX_ERROR(it->loc, "Unexpected end of file");
        if (HasErrors())
            return {};
        if (auto nd = ParseStatement(it))
            result->Add(std::move(nd));
        if (it->type == Token::Type::Semicolon)
            ++it;
    }
    // Consume }
    ++it;
    return result;
}

// Expressions

// https://en.cppreference.com/w/cpp/language/operator_precedence

NodePtr Parser::ParseExpression(TokenIterator& it)
{
    return ParseTernaryOrAssignExpression(it);
}

NodePtr Parser::ParseTernaryOrAssignExpression(TokenIterator& it)
{
    // 15.: Ternary and assignments
    auto lhs = ParseRangeExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Question:
        {
            ++it;
            if (it->type == Token::Type::Colon)
                // expr ?: false
                lhs = CreateNode<TernaryExpr>(it->loc, std::move(lhs), nullptr, ParseRangeExpression(++it));
            else
            {
                // expr ? true : false
                auto trueExpr = ParseRangeExpression(it);
                if (!Expect(*it, Token::Type::Colon))
                    return {};
                lhs = CreateNode<TernaryExpr>(it->loc, std::move(lhs), std::move(trueExpr), ParseRangeExpression(++it));
            }
            break;
        }            
        case Token::Type::Assign:
            // On the RHS of an assigment there can be any expression, not just range expressions upwards,
            // even another assignment.
            lhs = CreateNode<Assign>(lhs->loc_, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignAdd:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Add, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignAnd:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::And, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignDiv:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Div, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignIDiv:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::IDiv, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignMod:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Mod, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignMul:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Mul, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignOr:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Or, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignPower:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Pow, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignShl:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Shl, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignShr:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Shr, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignSub:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Sub, std::move(lhs), ParseExpression(++it));
            break;
        case Token::Type::AssignXor:
            lhs = CreateNode<CompoundAssign>(it->loc, CompoundAssign::Op::Xor, std::move(lhs), ParseExpression(++it));
            break;
        default:
            return lhs;
        }
    }
}

NodePtr Parser::ParseRangeExpression(TokenIterator& it)
{
    // 14.
    bool isSliceExpression = PreviousToken(it).type == Token::Type::BracketOpen;
    NodePtr lhs;
    if (!isSliceExpression || it->type != Token::Type::DoubleDot)
        lhs = ParseLogicalOrExpression(it);
    if (it->type == Token::Type::DoubleDot)
    {
        NodePtr rhs;
        if (isSliceExpression && (it + 1)->type == Token::Type::BracketClose)
            ++it;
        else
            rhs = ParseLogicalOrExpression(++it);
        return CreateNode<Range>(it->loc, std::move(lhs), std::move(rhs));
    }
    return lhs;
}

NodePtr Parser::ParseLogicalOrExpression(TokenIterator& it)
{
    // 13.
    auto lhs = ParseLogicalAndExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::LogicalOr:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::LogicalOr, std::move(lhs), ParseLogicalAndExpression(++it));
            break;
        default:
            return lhs;
        }
    }
    return lhs;
}

NodePtr Parser::ParseLogicalAndExpression(TokenIterator& it)
{
    // 12.
    auto lhs = ParseBitwiseOrExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::LogicalAnd:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::LogicalAnd, std::move(lhs), ParseBitwiseOrExpression(++it));
            break;
        default:
            return lhs;
        }
    }
    return lhs;
}

NodePtr Parser::ParseBitwiseOrExpression(TokenIterator& it)
{
    // 11.
    auto lhs = ParseBitwiseXorExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Pipe:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::BitwiseOr, std::move(lhs), ParseBitwiseXorExpression(++it));
            break;
        default:
            return lhs;
        }
    }
}

NodePtr Parser::ParseBitwiseXorExpression(TokenIterator& it)
{
    // 10.
    auto lhs = ParseBitwiseAndExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Caret:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::BitwiseXor, std::move(lhs), ParseBitwiseAndExpression(++it));
            break;
        default:
            return lhs;
        }
    }
}

NodePtr Parser::ParseBitwiseAndExpression(TokenIterator& it)
{
    // 9.
    auto lhs = ParseEqualityExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Amp:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::BitwiseAnd, std::move(lhs), ParseEqualityExpression(++it));
            break;
        default:
            return lhs;
        }
    }
}

NodePtr Parser::ParseEqualityExpression(TokenIterator& it)
{
    // 8.
    auto lhs = ParseRelationalExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::CompEquals:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Equal, std::move(lhs), ParseRelationalExpression(++it));
            break;
        case Token::Type::StrictEq:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::StrictEqual, std::move(lhs), ParseRelationalExpression(++it));
            break;
        case Token::Type::CompNot:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Unequal, std::move(lhs), ParseRelationalExpression(++it));
            break;
        case Token::Type::StrictUneq:
            lhs = CreateNode<BinaryExpr>(
                it->loc, BinaryExpr::Op::StrictUnequal, std::move(lhs), ParseRelationalExpression(++it));
            break;
        default:
            return lhs;
        }
    }
    return lhs;
}

NodePtr Parser::ParseRelationalExpression(TokenIterator& it)
{
    // 7.
    auto lhs = ParseShiftExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Lt:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Lt, std::move(lhs), ParseShiftExpression(++it));
            break;
        case Token::Type::LtEq:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::LtEqual, std::move(lhs), ParseShiftExpression(++it));
            break;
        case Token::Type::Gt:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Gt, std::move(lhs), ParseShiftExpression(++it));
            break;
        case Token::Type::GtEq:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::GtEqual, std::move(lhs), ParseShiftExpression(++it));
            break;
        case Token::Type::Spaceship:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::ThreeWayCmp, std::move(lhs), ParseShiftExpression(++it));
            break;
        case Token::Type::Ident:
        {
            Keyword kw = GetKeyword(*it);
            if (kw == Keyword::In)
            {
                lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::In, std::move(lhs), ParseShiftExpression(++it));
                break;
            }
            [[fallthrough]];
        }
        default:
            return lhs;
        }
    }
    return lhs;
}

NodePtr Parser::ParseShiftExpression(TokenIterator& it)
{
    // 6.
    auto lhs = ParseSumExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Shr:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Shr, std::move(lhs), ParseSumExpression(++it));
            break;
        case Token::Type::Shl:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Shl, std::move(lhs), ParseSumExpression(++it));
            break;
        default:
            return lhs;
        }
    }
    return lhs;
}

NodePtr Parser::ParseSumExpression(TokenIterator& it)
{
    // 5.
    auto lhs = ParseProductExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Plus:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Add, std::move(lhs), ParseProductExpression(++it));
            break;
        case Token::Type::Minus:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Sub, std::move(lhs), ParseProductExpression(++it));
            break;
        default:
            return lhs;
        }
    }
    return lhs;
}

NodePtr Parser::ParseProductExpression(TokenIterator& it)
{
    // 4.
    auto lhs = ParsePowerExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::Asterisk:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Mul, std::move(lhs), ParsePowerExpression(++it));
            break;
        case Token::Type::Slash:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Div, std::move(lhs), ParsePowerExpression(++it));
            break;
        case Token::Type::BackSlash:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::IDiv, std::move(lhs), ParsePowerExpression(++it));
            break;
        case Token::Type::Percent:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Mod, std::move(lhs), ParsePowerExpression(++it));
            break;
        default:
            return lhs;
        }
    }
}

NodePtr Parser::ParsePowerExpression(TokenIterator& it)
{
    // 3.
    auto lhs = ParseUnaryExpression(it);
    while (true)
    {
        switch (it->type)
        {
        case Token::Type::DoubleAsterisk:
            lhs = CreateNode<BinaryExpr>(it->loc, BinaryExpr::Op::Pow, std::move(lhs), ParseUnaryExpression(++it));
            break;
        default:
            return lhs;
        }
    }
}

NodePtr Parser::ParseUnaryExpression(TokenIterator& it)
{
    // 2.
    switch (it->type)
    {
    case Token::Type::Plus:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::UnaryPlus, ParseTermExpression(++it));
    case Token::Type::Minus:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::Negate, ParseTermExpression(++it));
    case Token::Type::Tilde:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::BitwiseNot, ParseTermExpression(++it));
    case Token::Type::Excl:
        if ((it + 1)->type == Token::Type::Excl)
            // Support !!expr, basically a to bool
            return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::LogicalNot, ParseUnaryExpression(++it));
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::LogicalNot, ParseTermExpression(++it));
    case Token::Type::Hash:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::CountOf, ParseTermExpression(++it));
    case Token::Type::Dollar:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::Stringify, ParseTermExpression(++it));
    case Token::Type::DoubleDollar:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::NameOf, ParseTermExpression(++it));
    case Token::Type::Increment:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::PreIncrement, ParseTermExpression(++it));
    case Token::Type::Decrement:
        return CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::PreDecrement, ParseTermExpression(++it));
    default:
        return ParseTermExpression(it);
    }
}

NodePtr Parser::ParseTermExpression(TokenIterator& it)
{
    // 1.
    NodePtr result;
    if (GetKeyword(*it) == Keyword::Function)
    {
        result = ParseFunction(it);
    }
    else if (it->type == Token::Type::ParenOpen)
    {
        ++it;
        result = ParseExpression(it);
        if (!Expect(*it, Token::Type::ParenClose))
            return {};
        ++it;
    }
    else if (IsValue(*it))
    {
        // Simple value.
        result = ParseValueExpression(it);
    }
    else if (it->type == Token::Type::CurlyOpen)
    {
        // Term expression can also be a table initialization { ... }
        result = ParseTableInit(it);
    }
    else if (IsIdent(*it))
    {
        std::string symbol = it->value;
        ++it;
        if (IsAssignment(it))
        {
            --it;
            result = ParseVariableDefinition(it);
        }
        else
            result = CreateNode<VariableExpr>((it - 1)->loc, std::move(symbol));
    }
    else
    {
        SYNTAX_ERROR(it->loc, "Expected term expression");
    }

    // Parse Member access (2), call expressions (1)
    if (!result)
        return {};
    result = ParseMemberCall(it, std::move(result));
    if (!result)
        // Syntax error
        return {};
    // Post de-/increment (1)
    return ParsePostOperators(it, std::move(result));
}

NodePtr Parser::ParseMemberAccessChain(TokenIterator& it, NodePtr owner)
{
    SATL_ASSERT(owner);
    NodePtr result = std::move(owner);
    while (it->type == Token::Type::BracketOpen
        || it->type == Token::Type::Dot
        || (it->type == Token::Type::Question && ((it + 1)->type == Token::Type::Dot || (it + 1)->type == Token::Type::BracketOpen)))
    {
        result = ParseMemberAccess(it, std::move(result));
        if (!result)
            return {};
    }
    return result;
}

template<typename T>
NodePtr Parser::ParseComputedMembers(TokenIterator& it, NodePtr owner)
{
    // Parse between []
    NodePtr result = std::move(owner);
    do
    {
        ++it;
        result = CreateNode<T>(it->loc, std::move(result), ParseExpression(it), true);
    } while (it->type == Token::Type::Comma);
    return result;
}

NodePtr Parser::ParseMemberAccess(TokenIterator& it, NodePtr owner)
{
    SATL_ASSERT(owner);
    NodePtr result = std::move(owner);
    if (it->type == Token::Type::BracketOpen)
    {
        result = ParseComputedMembers<MemberAccess>(it, std::move(result));
        if (!Expect(*it, Token::Type::BracketClose))
            return {};
        ++it;
    }
    else if (it->type == Token::Type::Dot)
    {
        ++it;
        if (!ExpectIdent(*it))
            return {};
        result = CreateNode<MemberAccess>(it->loc, std::move(result), ParseValueExpression(it), false);
    }
    else if (it->type == Token::Type::Question)
    {
        ++it;

        if (it->type == Token::Type::Dot)
        {
            ++it;
            if (!ExpectIdent(*it))
                return {};
            result = CreateNode<ConditionalMemberAccess>(
                it->loc, std::move(result), ParseValueExpression(it), false);
        }
        else if (it->type == Token::Type::BracketOpen)
        {
            result = ParseComputedMembers<ConditionalMemberAccess>(it, std::move(result));
            if (!Expect(*it, Token::Type::BracketClose))
                return {};
            ++it;
        }
        else
            SATL_ASSERT_FALSE();
    }
    return result;
}

NodePtr Parser::ParseMemberCall(TokenIterator& it, NodePtr owner)
{
    SATL_ASSERT(owner);
    NodePtr result = std::move(owner);
    while (it->type == Token::Type::BracketOpen
        || it->type == Token::Type::Dot
        || (it->type == Token::Type::Question && ((it +1)->type == Token::Type::Dot || (it +1)->type == Token::Type::BracketOpen))
        || it->type == Token::Type::ParenOpen)
    {
        result = (it->type == Token::Type::ParenOpen) ?
            ParseCallExpression(it, std::move(result)) :
            ParseMemberAccessChain(it, std::move(result));
        if (!result)
            return {};
    }
    return result;
}

NodePtr Parser::ParsePostOperators(TokenIterator& it, NodePtr owner) // NOLINT(readability-convert-member-functions-to-static)
{
    SATL_ASSERT(owner);
    NodePtr result = std::move(owner);
    if (it->type == Token::Type::Increment)
    {
        // expression++
        ++it;
        result = CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::PostIncrement, std::move(result));
    }
    else if (it->type == Token::Type::Decrement)
    {
        // expression--
        ++it;
        result = CreateNode<UnaryExpr>(it->loc, UnaryExpr::Op::PostDecrement, std::move(result));
    }

    return result;
}

NodePtr Parser::ParseCallExpression(TokenIterator& it, NodePtr symbol)
{
    SATL_ASSERT(it->type == Token::Type::ParenOpen);
    // Calling a variable, e.g. table.member(arg);
    auto result = CreateNode<CallExpr>((it - 1)->loc, std::move(symbol));

    if (it->type != Token::Type::ParenClose)
    {
        do
        {
            // Parse argument list
            ++it;
            if (it->type == Token::Type::ParenClose)
                break;
            result->AddArgument(ParseExpression(it));
        } while (it->type == Token::Type::Comma);
    }
    if (!Expect(*it, Token::Type::ParenClose))
        return {};
    ++it;
    return result;
}

NodePtr Parser::ParseValueExpression(TokenIterator& it)
{
    return CreateNode<ValueExpr>(it->loc, ParseValueOrIdent(it));
}

std::string Parser::CollectStrings(TokenIterator& it, Token::Type type)
{
    std::string result;
    while (it->type == type)
    {
        result += it->value;
        ++it;
    }
    return result;
}

ValuePtr Parser::ParseValueOrIdent(TokenIterator& it)
{
    if (it->type == Token::Type::Number)
    {
        std::string strValue = it->value;
        ++it;
        return MakeValue(NumberStringToValue(strValue, [&](std::string msg) {
            SyntaxError((it - 1)->loc, std::move(msg));
        }));
    }

    if (it->type == Token::Type::String)
        return MakeValue(CollectStrings(it, Token::Type::String));

    if (it->type == Token::Type::TemplateString)
    {
        std::string value = CollectStrings(it, Token::Type::TemplateString);
        --it;
        templ::Parser parser;
        auto tokens = parser.Parse(value);
        if (parser.HasError())
            SYNTAX_ERROR(it->loc, "Unmatched curly brackets in template literal.");
        tokens.ToString(
            [this, &it](const templ::Token& token) -> std::string
            {
                if (token.type == templ::Token::Type::Variable)
                {
                    Parser parser(token.value);
                    parser.ParseExpression();
                    if (parser.HasErrors())
                        SyntaxError(it->loc, std::format("Error in template expression `{}`: {}", token.value, parser.GetErrors().back().message));
                    return "";
                }
                return "";
            });
        if (!errors_.empty())
            return MakeNull();

        ++it;
        ValuePtr result = MakeValue(value);
        result->SetTemplate(true);
        return result;
    }

    if (it->type == Token::Type::Ident && it->value == LITERAL_TRUE)
    {
        ++it;
        return MakeValue(true);
    }
    if (it->type == Token::Type::Ident && it->value == LITERAL_FALSE)
    {
        ++it;
        return MakeValue(false);
    }
    if (it->type == Token::Type::Ident && it->value == LITERAL_NULL)
    {
        ++it;
        return MakeValue(nullptr);
    }
    if (GetKeyword(*it) != Keyword::None)
        SYNTAX_ERROR(it->loc, std::format("Unexpected keyword \"{}\"", it->value));
    // Identifier
    std::string value = it->value;
    ++it;
    return MakeValue(value);
}

}
