/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <ostream>
#include <string>
#include "location.h"

namespace sa::tl {

#define ENUMERATE_ERRORS                     \
    ENUMERATE_ERROR(NoError)                 \
    ENUMERATE_ERROR(Unknown)                 \
    ENUMERATE_ERROR(SyntaxError)             \
    ENUMERATE_ERROR(TypeMismatch)            \
    ENUMERATE_ERROR(ConstAssign)             \
    ENUMERATE_ERROR(ArgumentsMismatch)       \
    ENUMERATE_ERROR(InvalidArgument)         \
    ENUMERATE_ERROR(UnknownFunction)         \
    ENUMERATE_ERROR(NotAFunction)            \
    ENUMERATE_ERROR(UndefinedIdentifier)     \
    ENUMERATE_ERROR(ConstantRequired)        \
    ENUMERATE_ERROR(InvalidSubscript)        \
    ENUMERATE_ERROR(IndexOutOfBounds)        \
    ENUMERATE_ERROR(MathError)               \
    ENUMERATE_ERROR(DebuggerError)           \
    ENUMERATE_ERROR(NullDereference)         \
    ENUMERATE_ERROR(UnsafeFunction)          \
    ENUMERATE_ERROR(CallingNonconstFunction) \
    ENUMERATE_ERROR(OSError)

struct Error
{
    enum class Type
    {
        SyntaxError,
        RuntimeError
    };
    enum Code : int
    {
#define ENUMERATE_ERROR(v) v,
        ENUMERATE_ERRORS
#undef ENUMERATE_ERROR
        UserDefined = 100,
    };

    Type type;
    int code;
    Location loc;
    std::string message;
    // When this is an error in a file that was included, this is the location where it was included.
    Location included;
    friend std::ostream& operator<<(std::ostream& os, const Error& value)
    {
        if (value.type == Error::Type::SyntaxError)
            os << "Syntax Error";
        else
            os << "Runtime Error:" << value.code;
        os << " (" << value.loc << "): " << value.message;
        if (!value.included.file.empty())
            os << " (included " << value.included << ")";
        return os;
    }
};

}
