/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "gc.h"
#include <set>
#include "context.h"
#include "utils.h"

namespace sa::tl {

GCObject::~GCObject() = default;

GCObject::Impl::~Impl() = default;

GC::GC(Context& ctx) : ctx_(ctx)
{ }

GC::~GC() = default;

void GC::Shutdown()
{
    shuttingDown_ = true;
    objects_.clear();
}

bool GC::Remove(void* inst)
{
    if (shuttingDown_)
        return false;
    auto it = objects_.find((uintptr_t)inst);
    if (it != objects_.end() && it->second.GetLocked() == 0)
    {
        size_ -= it->second.GetSize();
        objects_.erase((uintptr_t)inst);
        return true;
    }
    return false;
}

bool GC::IsAlive(const void* inst) const
{
    if (shuttingDown_)
        return false;
    return objects_.contains((uintptr_t)inst);
}

WeakObject GC::GetWeak(const void* inst) const
{
    if (shuttingDown_)
        return { this, nullptr };
    if (!objects_.contains((uintptr_t)inst))
        return { this, nullptr };
    return { this, inst };
}

StrongObject GC::GetStrong(void* inst)
{
    if (shuttingDown_)
        return StrongObject{ nullptr };
    auto it = objects_.find((uintptr_t)inst);
    if (it == objects_.end())
        return StrongObject{ nullptr };
    return StrongObject{ &it->second };
}

void GC::Run()
{
    if (pause_)
        return;
    if (shuttingDown_)
        return;

    allocs_ = 0;
    std::set<uintptr_t> used;
    ctx_.ForEach<ObjectPtr, false>([&](const ObjectPtr& current) { used.emplace((uintptr_t)current->GetInstance()); });
    ctx_.ForEachCapture<ObjectPtr>([&](const ObjectPtr& current) { used.emplace((uintptr_t)current->GetInstance()); });
    DeleteIf(objects_,
        [&](const auto& current)
        {
            if (current.second.GetLocked() != 0)
                return false;
            if (!used.contains(current.first))
            {
                size_ -= current.second.GetSize();
                return true;
            }
            return false;
        });
}

void GC::CheckTrigger()
{
    switch (trigger_.kind)
    {
    case Trigger::Kind::Allocs:
        if (allocs_ >= trigger_.allocs)
            Run();
        break;
    case Trigger::Kind::Size:
        if (size_ >= trigger_.size)
            Run();
        break;
    case Trigger::Kind::Both:
        if (allocs_ >= trigger_.allocs || size_ >= trigger_.size)
            Run();
        break;
    case Trigger::Kind::None:
        break;
    }
}

}
