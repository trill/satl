/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cassert>
#include <cstdlib>
#include <iostream>

#if defined(__GNUC__)
#   define SATL_ALWAYS_INLINE [[gnu::always_inline]] inline
#elif defined(__clang__)
#   define SATL_ALWAYS_INLINE inline __attribute__((always_inline))
#endif

#if defined(SATL_ENABLE_ASSERT)
namespace sa::tl::details {
[[noreturn]] SATL_ALWAYS_INLINE void assertion_failed(const char* msg, const char* file, unsigned line, const char* func)
{
    std::cerr << "Assertion failed: " << msg << " in " << file << ":" << line << " " << func << std::endl;
    abort();
}
}

// Make sure there is an assert that never returns
#define SATL_ASSERT(expr) (static_cast<bool>(expr) ? (void)0 : sa::tl::details::assertion_failed(#expr, __FILE__, __LINE__, __PRETTY_FUNCTION__))
#define SATL_ASSERT_FALSE() SATL_ASSERT(false)
#else
#define SATL_ASSERT assert
#define SATL_ASSERT_FALSE() abort()
#endif
