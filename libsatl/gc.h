/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <unordered_map>
#include <memory>
#include "value.h"
#include <non_copyable.h>

namespace sa::tl {

class Context;
class GC;
class WeakObject;
class StrongObject;

class GCObject
{
    SATL_NON_COPYABLE(GCObject);
    friend class StrongObject;
public:
    template<typename T>
    explicit GCObject(T* o) :
        impl_(std::make_unique<TImpl<T>>(o))
    { }
    ~GCObject();
    void* GetPtr() const
    {
        if (impl_)
            return impl_->GetPtr();
        return nullptr;
    }
    size_t GetSize() const
    {
        if (impl_)
            return impl_->GetSize();
        return 0;
    }
    int GetLocked() const
    {
        return locked_;
    }
private:
    struct Impl
    {
    public:
        [[nodiscard]] virtual void* GetPtr() const = 0;
        [[nodiscard]] virtual constexpr size_t GetSize() const = 0;
        virtual ~Impl();
    };
    template<typename T>
    struct TImpl final : public Impl
    {
        explicit TImpl(T* o) :
            object(o)
        { }
        [[nodiscard]] void* GetPtr() const override
        {
            return object.get();
        }
        [[nodiscard]] constexpr size_t GetSize() const override { return sizeof(T); };
        std::unique_ptr<T> object;
    };
    std::unique_ptr<Impl> impl_;
    int locked_{ 0 };
};

class WeakObject
{
    friend class GC;
public:
    WeakObject() = default;
    ~WeakObject() = default;

    // Returns nullptr when the object is dead already
    template<typename T, typename Base>
    [[nodiscard]] T* GetPtr() const;
private:
    WeakObject(const GC* gc, const void* object) :
        gc_(gc),
        object_(object)
    { }
    const GC* gc_{ nullptr };
    // The GCObject may be deleted already, so let's keep a pointer to the real object instead.
    const void* object_{ nullptr };
};

class StrongObject
{
    friend class GC;
public:
    StrongObject() = default;
    StrongObject(const StrongObject& other) :
        object_(other.object_)
    {
        if (object_)
            ++object_->locked_;
    }
    StrongObject(StrongObject&&) noexcept = default;
    StrongObject& operator =(const StrongObject& other)
    {
        object_ = other.object_;
        if (object_)
            ++object_->locked_;
        return *this;
    }
    StrongObject& operator =(StrongObject&&) noexcept = default;

    ~StrongObject()
    {
        if (object_)
            --object_->locked_;
    }

    void Release()
    {
        if (object_)
        {
            --object_->locked_;
            object_ = nullptr;
        }
    }
    // Since strongly referenced objects are not deleted, this always returns a valid pointer.
    template<typename T, typename Base = T>
    [[nodiscard]] T* GetPtr() const
    {
        static_assert(std::is_base_of<Base, T>::value);
        if (object_)
        {
            Base* base = static_cast<Base*>(object_->GetPtr());
            if constexpr (std::is_same_v<T, Base>)
                return base;
            else
                return dynamic_cast<T*>(base);
        }
        return nullptr;
    }
private:
    explicit StrongObject(GCObject* object) :
        object_(object)
    { }
    // In contrast to WeakObject this GCObject must be alive so we can just reference it.
    GCObject* object_{ nullptr };
};

// Garbage collector for bound C++ objects
class GC
{
    friend class Context;
public:
    explicit GC(Context& ctx);
    ~GC();
    void Run();
    // Create a new managed object
    template<typename T, typename... Args>
    T* CreateObject(Args&&... Arguments)
    {
        if (!pause_)
        {
            CheckTrigger();
            // We don't want to count that allocation when paused, because when un-pausing it'll immediately run it.
            ++allocs_;
        }
        size_ += sizeof(T);
        peak_ = std::max(size_, peak_);
        auto* obj = new T(std::forward<Args>(Arguments)...);
        objects_.emplace((uintptr_t)obj, obj);
        return obj;
    }
    bool IsAlive(const void* inst) const;
    WeakObject GetWeak(const void* inst) const;
    StrongObject GetStrong(void* inst);
    // Remove and delete an object. This fails when there is a strong reference.
    bool Remove(void* inst);
    Integer GetSize() const { return (Integer)size_; }
    Integer GetPeak() const { return (Integer)peak_; }
    Integer GetAllocs() const { return (Integer)allocs_; }
    Integer GetCount() const { return (Integer)objects_.size(); }
    Integer GetTriggerSize() const { return (Integer)trigger_.size; }
    Integer GetTriggerAllocs() const { return (Integer)trigger_.allocs; }
    bool GetPause() const { return pause_; }
    void SetPause(bool value) { pause_ = value; }
    void SetTriggerSize(Integer value) { trigger_.size = value; }
    void SetTriggerAllocs(Integer value) { trigger_.allocs = value; }
    Integer GetTriggerKind() const { return (Integer)trigger_.kind; }
    void SetTriggerKind(Integer value)
    {
        if (value >= 0 && value <= (Integer)Trigger::Kind::Both)
            trigger_.kind = (Trigger::Kind)value;
    }
    struct Trigger
    {
        enum class Kind
        {
            None,
            Allocs,
            Size,
            Both
        } kind = Kind::Allocs;
        // Run GC every XXX allocations
        size_t allocs = 100;
        // Run GC when allocation size exceeds XXX
        size_t size = 1024 * 1024;
    } trigger_;
private:
    void Shutdown();
    void CheckTrigger();
    Context& ctx_;
    std::unordered_map<uintptr_t, GCObject> objects_;
    bool pause_{ false };
    // Total size allocated for objects
    size_t size_{ 0 };
    size_t peak_{ 0 };
    // Allocations since last GC
    size_t allocs_{ 0 };
    bool shuttingDown_{ false };
};

template<typename T, typename Base = T>
inline T* WeakObject::GetPtr() const
{
    static_assert(std::is_base_of<Base, T>::value);
    if (object_ && gc_ && gc_->IsAlive(object_))
    {
        // We need something with a vtable for dynamic_cast
        Base* base = static_cast<Base*>(const_cast<void*>(object_));
        if constexpr (std::is_same_v<T, Base>)
            return base;
        else
            return dynamic_cast<T*>(base);
    }
    return nullptr;
}

}
