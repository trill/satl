/**
 * Copyright (c) 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "number_parser.h"
#include "utils.h"
#include <cmath>
#include <stdexcept>
#include <format>

namespace sa::tl {

std::string ParseNumberString(std::string_view value, const std::function<void(std::string)>& error, bool unary)
{
    if (value.empty())
        return "";

    size_t offset = 0;

    bool success = ParseNumberString(
        [&]() -> char
        {
            if (offset < value.length())
                return value[offset++];
            return '\0';
        },
        [&](size_t off) -> char
        {
            if (offset + off < value.length())
                return value[offset + off];
            return '\0';
        },
        error, unary);
    if (!success)
        return "";
    return { value.data(), offset };
}

bool ParseNumberString(const std::function<char()>& consume, const std::function<char(size_t)>& peek, const std::function<void(std::string)>& error, bool unary)
{
    char current = consume();

    enum class NumberType
    {
        Decimal,
        Hex,
        Octal,
        Bin
    } numberType = NumberType::Decimal;

    auto isDigit = [&numberType](char c) -> bool
    {
        switch (numberType)
        {
        case NumberType::Decimal:
            return isdigit(c);
        case NumberType::Hex:
            return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
        case NumberType::Octal:
            return c >= '0' && c <= '7';
        case NumberType::Bin:
            return c == '0' || c == '1';
        default:
            return false;
        }
    };

    if (unary && current == '-')
        current = consume();
    else if (unary && current == '+')
        current = consume();

    if (current == '0')
    {
        current = consume();
        if (current == 'x' || current == 'X')
        {
            current = consume();
            numberType = NumberType::Hex;
        }
        else if (current == 'o' || current == 'O')
        {
            current = consume();
            numberType = NumberType::Octal;
        }
        else if (current == 'b' || current == 'B')
        {
            current = consume();
            numberType = NumberType::Bin;
        }
    }

    while (isDigit(current) || current == '_')
    {
        if (current == '_')
        {
            if (!isDigit(peek(1)))
            {
                error("Number separator can not be last");
                return false;
            }
        }
        current = consume();
    }

    // Yes, a decimal point must be followed by a digit, otherwise it's not a decimal point!
    if (current == '.' && isdigit(peek(1)))
    {
        if (numberType != NumberType::Decimal)
        {
            error("Malformed number");
            return false;
        }
        while (isdigit(peek(1)) || peek(1) == '_')
        {
            if (peek(1) == '_')
            {
                if (!isDigit(peek(2)))
                {
                    error("Number separator can not be last");
                    return false;
                }
            }
            consume();
        }
        current = consume();
    }

    if ((current == 'e' || current == 'E'))
    {
        if (numberType != NumberType::Decimal)
        {
            error("Malformed number");
            return false;
        }
        current = consume();

        if (current == '+' || current == '-')
            current = consume();

        if (!isdigit(current))
        {
            error("Malformed number");
            return false;
        }

        while (isdigit(current) || current == '_')
        {
            if (current == '_')
            {
                if (!isDigit(peek(1)))
                {
                    error("Number separator can not be last");
                    return false;
                }
            }
            current = consume();
        }
    }

    return true;
}

Value NumberStringToValue(std::string value, const std::function<void(std::string)>& error, bool unary)
{
    ReplaceSubstring<char>(value, "_", "");
    bool neg = false;

    auto sign = [&neg]<typename T>(T v) -> T {
        if (neg)
            return -v;
        return v;
    };

    if (unary)
    {
        if (value[0] == '-')
        {
            neg = true;
            value.erase(0, 1);
        }
        else if (value[0] == '+')
            value.erase(0, 1);
    }
    try
    {
        if (value.starts_with("0x") || value.starts_with("0X"))
        {
            value.erase(0, 2);
            if constexpr (sizeof(Integer) == 8)
                return sign(static_cast<Integer>(std::stoll(value, nullptr, 16)));
            else
                return sign(static_cast<Integer>(std::stol(value, nullptr, 16)));
        }
        if (value.starts_with("0b") || value.starts_with("0B"))
        {
            value.erase(0, 2);
            if constexpr (sizeof(Integer) == 8)
                return sign(static_cast<Integer>(std::stoll(value, nullptr, 2)));
            else
                return sign(static_cast<Integer>(std::stol(value, nullptr, 2)));
        }
        if (value.starts_with("0o") || value.starts_with("0O"))
        {
            value.erase(0, 2);
            if constexpr (sizeof(Integer) == 8)
                return sign(static_cast<Integer>(std::stoll(value, nullptr, 8)));
            else
                return sign(static_cast<Integer>(std::stol(value, nullptr, 8)));
        }
    }
    catch (const std::out_of_range& ex)
    {
        error(std::format("Value {} out of range", value));
        return {};
    }
    catch (const std::invalid_argument& ex)
    {
        error(std::format("Value \"{}\" can not be converted", value));
        return {};
    }

    const auto v = ToNumber<Float>(value);
    if (!v.has_value())
    {
        error(std::format("Malformed number \"{}\"", value));
        return {};
    }
    if ((value.find('.') == std::string::npos) && (std::fmod(v.value(), 1.0) == 0.0))
        // When it doesn't have a fractional part make it an integer
        return sign(static_cast<Integer>(v.value()));
    return sign(static_cast<Float>(v.value()));
}

}
