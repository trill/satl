/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "template_parser.h"

namespace sa::tl::templ {

Token Parser::GetNextToken()
{
    Token result;
    result.type = Token::Type::Invalid;
    result.start = index_;
    while (!Eof())
    {
        char c = Next();
        switch (c)
        {
        case '$':
        {
            char c2 = Peek(0);
            if (c2 == '{')
            {
                if (result.type != Token::Type::Invalid)
                {
                    result.end = index_ - 1;
                    result.value = std::string(&source_[result.start], result.end - result.start);
                    --index_;
                    return result;
                }
                ++openCurly_;
                result.type = Token::Type::Variable;
                result.start += 2;
                ++index_;
            }
            break;
        }
        case '}':
            if (result.type == Token::Type::Variable)
            {
                --openCurly_;
                result.end = index_ - 1;
                result.value = std::string(&source_[result.start], result.end - result.start);
                return result;
            }
            break;
        case '\0':
            result.end = index_;
            result.value = std::string(&source_[result.start], result.end - result.start);
            return result;
        case '`':
        case '\'':
        case '"':
            if (quotesSupport_)
            {
                if (inQuote_ && quote_ != c)
                    continue;
                if (result.type == Token::Type::String)
                {
                    result.end = index_ - 1;
                    result.value = std::string(&source_[result.start], result.end - result.start);
                    --index_;
                    return result;
                }
                result.type = Token::Type::Quote;
                result.end = index_ - 1;
                result.value = c;
                inQuote_ = !inQuote_;
                quote_ = inQuote_ ? c : '\0';
                return result;
            }
            // If not supporting quotes fall through default handler
            [[fallthrough]];
        default:
            if (result.type == Token::Type::Invalid)
                result.type = Token::Type::String;
            break;
        }
    }
    result.end = index_;
    result.value = std::string(&source_[result.start], result.end - result.start);
    return result;
}

Tokens Parser::Parse(std::string_view source)
{
    source_ = source;
    Reset();
    Tokens result;
    result.reserve_ = source_.length();
    while (!Eof())
        result.tokens_.push_back(GetNextToken());
    return result;
}

void Parser::Append(std::string_view source, Tokens& tokens)
{
    source_ = source;
    Reset();
    tokens.reserve_ += source_.length();
    while (!Eof())
        tokens.tokens_.push_back(GetNextToken());
}

}
