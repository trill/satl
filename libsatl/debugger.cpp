/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "debugger.h"
#include "parser.h"
#include "utils.h"

namespace sa::tl {

Debugger::Debugger(Context& ctx) : ctx_(ctx)
{
    ctx_.onEnter_ = [this](Context&, const Node& node) { this->OnEnter(node); };
    ctx_.onLeave_ = [this](Context&, const Node& node) { this->OnLeave(node); };
    if (ctx_.onError_)
        onError_ = ctx_.onError_;
    ctx_.onError_ = [this](const Context& ctx, const Error& error) { this->OnError(ctx, error); };
}

Debugger::~Debugger() = default;

void Debugger::AddSource(const std::string& filename, const std::string& source)
{
    if (filename.empty() || source.empty())
        return;
    std::vector<std::string> lines = Split(source, "\n", true, true);
    sources_.emplace(filename, std::move(lines));
}

std::optional<std::string> Debugger::GetSourceLine(const Location& loc)
{
    auto it = sources_.find(loc.file);
    if (it == sources_.end())
        return {};
    if (loc.line >= it->second.size())
        return {};
    return it->second[loc.line];
}

ValuePtr Debugger::Run(const NodePtr& ast, bool stopOnFirst)
{
    if (stopOnFirst)
        stepping_ = true;
    return ast->Evaluate(ctx_);
}

void Debugger::Stop()
{
    ctx_.stop_ = true;
}

void Debugger::Step()
{
    stepping_ = true;
}

ValuePtr Debugger::Execute(const std::string& src)
{
    Parser parser(src);
    auto root = parser.Parse();
    if (!root)
    {
        for (const auto& e : parser.GetErrors())
            EvaluateError(Error::Code::DebuggerError, e.message);
        return MakeNull();
    }
    EvalGuard eg(ctx_);
    return root->Evaluate(ctx_);
}

ValuePtr Debugger::Evaluate(const NodePtr& node)
{
    EvalGuard eg(ctx_);
    return node->Evaluate(ctx_);
}

ValuePtr Debugger::Evaluate(const std::string& expr)
{
    Parser parser(expr);
    auto root = parser.ParseExpression();
    if (!root)
    {
        for (const auto& e : parser.GetErrors())
            EvaluateError(Error::Code::DebuggerError, e.message);
        return MakeNull();
    }
    EvalGuard eg(ctx_);
    return root->Evaluate(ctx_);
}

void Debugger::AddBreakpoint(const Location& loc)
{
    breakpoints_.emplace(loc);
}

void Debugger::RemoveBreakpoint(const Location& loc)
{
    breakpoints_.erase(loc);
}

bool Debugger::ToggleBreakpoint(const Location& loc)
{
    if (HasBreakpoint(loc))
    {
        RemoveBreakpoint(loc);
        return false;
    }
    AddBreakpoint(loc);
    return true;
}

bool Debugger::HasBreakpoint(const Location& loc)
{
    return breakpoints_.find(loc) != breakpoints_.end();
}

bool Debugger::ToggleWatch(const std::string& expr)
{
    if (HasWatch(expr))
    {
        RemoveWatch(expr);
        return false;
    }
    AddWatch(expr);
    return true;
}

void Debugger::AddWatch(const std::string& expr)
{
    watches_.push_back(expr);
}

void Debugger::RemoveWatch(const std::string& expr)
{
    const auto it = std::find_if(
        watches_.begin(), watches_.end(), [&expr](const auto& current) { return expr == current; });
    if (it != watches_.end())
        watches_.erase(it);
}

bool Debugger::HasWatch(const std::string& expr)
{
    const auto it = std::find_if(
        watches_.begin(), watches_.end(), [&expr](const auto& current) { return expr == current; });
    return it != watches_.end();
}

void Debugger::OnEnter(const Node& node)
{
    nodeStack_.push_front(&node);
    if (breakpoints_.contains(node.loc_))
    {
        if (onBreakpoint_)
        {
            stepping_ = false;
            onBreakpoint_(node);
            // We do not want to trigger an onStep
            return;
        }
    }
    if (stepping_)
    {
        stepping_ = false;
        if (onStep_)
            onStep_(node);
        // To continue stepping call Step() again
    }
}

void Debugger::OnLeave(const Node&)
{
    nodeStack_.pop_front();
}

void Debugger::OnError(const Context& ctx, const Error& error) const
{
    if (onError_)
        onError_(ctx, error);
}

void Debugger::EvaluateError(int code, std::string message)
{
    // Call directly OnError() we do not want to stop execution just because of an evaluation error
    OnError(ctx_, { Error::Type::RuntimeError, code, Location(), std::move(message), {} });
}

}
