/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <type_traits>
#include <format>
#include "native_function.h"
#include "node.h"
#include "satl_assert.h"
#include "non_copyable.h"
#include "exceptions.h"

namespace sa::tl {

namespace details {

template<typename T, typename R, typename... Args>
struct MemberType
{
    using Type = R (T::*)(Args...);
};
template<typename T, typename R, typename... Args>
struct MemberTypeConst
{
    using Type = R (T::*)(Args...) const;
};

}

class MemberFunction : public Callable
{
public:
    MemberFunction(std::string name, bool safe) : safe_(safe), name_(std::move(name)) {};
    constexpr Class GetClass() const final { return Class::MemberFunction; };
    virtual constexpr bool Const() const = 0;
    ValuePtr operator()(Context&, const FunctionArguments& args) const final;
    ValuePtr operator()(Context& ctx, const Value& thisValue, const FunctionArguments& args) const final;
    std::string GetFriendlyName() const final { return name_ + "()"; }
    const std::string& GetName() const final { return name_; }
protected:
    virtual ValuePtr operator()(const ObjectPtr& object, const FunctionArguments& args) const = 0;
    bool safe_;
private:
    std::string name_;
};

template<bool IsConst, bool IsOp, typename T, typename R, typename... Args>
class MemberFunctionImpl;

template<>
constexpr bool Is<MemberFunction>(const Node& obj)
{
    return obj.GetClass() == Node::Class::MemberFunction;
}

using MemberFunctionPtr = std::shared_ptr<MemberFunction>;
using MemberFunctions = std::map<std::string, MemberFunctionPtr>;

class MetaTable
{
    SATL_NON_COPYABLE(MetaTable);
    SATL_NON_MOVEABLE(MetaTable);
public:
    explicit MetaTable(std::string_view className) :
        className_(className)
    { }
    MetaTable(std::string_view className, const MetaTable* base) :
        className_(className),
        base_(base)
    { }
    ~MetaTable() = default;
    // Add a non const function
    template<typename T, typename R, typename... Args>
    MetaTable& AddFunction(const std::string& name, typename details::MemberType<T, R, Args...>::Type&& func, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        functions_.emplace(name, std::make_shared<MemberFunctionImpl<false, false, T, R, Args...>>(name, safe, std::move(func)));
        return *this;
    }
    // Add a const function
    template<typename T, typename R, typename... Args>
    MetaTable& AddFunction(const std::string& name, typename details::MemberTypeConst<T, R, Args...>::Type&& func, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        functions_.emplace(name, std::make_shared<MemberFunctionImpl<true, false, T, R, Args...>>(name, safe, std::move(func)));
        return *this;
    }
    // Note: Getters must be const
    template<typename T, typename GetType, typename SetType>
    MetaTable& AddProperty(const std::string& name, GetType (T::*getter)() const, void (T::*setter)(SetType), bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        propertyGetters_.emplace(name, std::make_shared<MemberFunctionImpl<true, false, T, GetType>>(name, safe, std::move(getter)));
        propertySetters_.emplace(name, std::make_shared<MemberFunctionImpl<false, false, T, void, SetType>>(name, safe, std::move(setter)));
        return *this;
    }
    template<typename T, typename GetType>
    MetaTable& AddProperty(const std::string& name, GetType (T::*getter)() const, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        propertyGetters_.emplace(
            name, std::make_shared<MemberFunctionImpl<true, false, T, GetType>>(name, safe, std::move(getter)));
        return *this;
    }
    // Unary
    template<typename T>
    MetaTable& AddOperator(const std::string& name, T& (T::*op)(), bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<false, true, T, T&>>(name, safe, std::move(op)));
        return *this;
    }
    template<typename T>
    MetaTable& AddOperator(const std::string& name, T& (T::*op)() const, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<true, true, T, T&>>(name, safe, std::move(op)));
        return *this;
    }
    template<typename T>
    MetaTable& AddOperator(const std::string& name, Value (T::*op)(), bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<false, true, T, Value>>(name, safe, std::move(op)));
        return *this;
    }
    template<typename T>
    MetaTable& AddOperator(const std::string& name, Value (T::*op)() const, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<true, true, T, Value>>(name, safe, std::move(op)));
        return *this;
    }
    // Binary
    template<typename T>
    MetaTable& AddOperator(const std::string& name, T* (T::*op)(const Value&) const, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<true, true, T, T*, const Value&>>(name, safe, std::move(op)));
        return *this;
    }
    // Compound, same as Binary but not const
    template<typename T>
    MetaTable& AddOperator(const std::string& name, T* (T::*op)(const Value&), bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<false, true, T, T*, const Value&>>(name, safe, std::move(op)));
        return *this;
    }
    // Binary can also return a Value, e.g. `lhs == rhs -> bool`
    template<typename T>
    MetaTable& AddOperator(const std::string& name, Value (T::*op)(const Value&) const, bool safe = true)
    {
        SATL_ASSERT(TypeName<T>::Get() == className_);
        operators_.emplace(name, std::make_shared<MemberFunctionImpl<true, true, T, Value, const Value&>>(name, safe, std::move(op)));
        return *this;
    }
    // Similar to dynamic_cast
    template<typename T>
    [[nodiscard]] T* CastTo(Object* object) const
    {
        if (!object)
            return nullptr;

        static constexpr auto name = TypeName<T>::Get();
        if (name == className_)
            return static_cast<T*>(object->GetInstance());

        if (base_)
        {
            if (T* result = base_->CastTo<T>(object))
                return result;
        }

        return nullptr;
    }
    template<typename T>
    [[nodiscard]] bool InstanceOf() const
    {
        if (TypeName<T>::Get() == className_)
            return true;
        if (base_)
            return base_->InstanceOf<T>();
        return false;
    }
    [[nodiscard]] bool InstanceOf(std::string_view className) const
    {
        if (className_ == className)
            return true;
        if (base_)
            return base_->InstanceOf(className);
        return false;
    }
    [[nodiscard]] MemberFunctionPtr GetFunction(const std::string& name) const;
    [[nodiscard]] MemberFunctionPtr GetOperator(const std::string& name) const;
    [[nodiscard]] MemberFunctionPtr GetPropertyGetter(const std::string& name) const;
    [[nodiscard]] MemberFunctionPtr GetPropertySetter(const std::string& name) const;
    [[nodiscard]] const MemberFunctions& GetFunctions() const { return functions_; }
    [[nodiscard]] const MemberFunctions& GetOperators() const { return operators_; }
    [[nodiscard]] const MemberFunctions& GetPropertyGetters() const { return propertyGetters_; }
    [[nodiscard]] const MemberFunctions& GetPropertySetters() const { return propertySetters_; }
    [[nodiscard]] const std::string_view& GetClassName() const { return className_; }
    [[nodiscard]] const MetaTable* GetBase() const { return base_; }

    // Convenience function to get the underlying object
    template <typename T>
    static T* GetObject(const ObjectPtr& object)
    {
        if (!object)
            return nullptr;

        const auto& meta = object->GetMetatable();
        return meta.CastTo<T>(object.get());
    }
    template <typename T>
    static T* GetObject(const Value& object)
    {
        if (!object.IsObject())
            return nullptr;
        return GetObject<T>(object.As<ObjectPtr>());
    }

private:
    MemberFunctions functions_;
    MemberFunctions operators_;
    MemberFunctions propertySetters_;
    MemberFunctions propertyGetters_;
    std::string_view className_;
    const MetaTable* base_{ nullptr };
};

template<bool IsConst, bool IsOp, typename T, typename R, typename... Args>
class MemberFunctionImpl final : public MemberFunction
{
private:
    using NonconstFuncType = R (T::*)(Args...);
    using ConstFuncType = R (T::*)(Args...) const;
    using ObjectType = std::conditional<IsConst, const T*, T*>::type;
public:
    using FuncType = std::conditional<IsConst, ConstFuncType, NonconstFuncType>::type;

    MemberFunctionImpl(std::string name, bool safe, FuncType&& func) :
        MemberFunction(std::forward<std::string>(name), safe),
        func_(std::move(func))
    {}

    constexpr int ParamCount() const override { return sizeof...(Args); };
    constexpr bool Const() const override { return IsConst; };

private:
    ValuePtr operator()(const ObjectPtr& object, const FunctionArguments& args) const override
    {
        if (args.size() != sizeof...(Args))
        {
            throw ArgumentsMismatch(std::format("Wrong number of arguments, expected {} but got {}", sizeof...(Args), args.size()));
        }

        if constexpr (!IsConst)
        {
            if (object->IsConst())
                throw ConstnessMismatch(std::format("Calling the non-const function {} of a const object", GetName()));
        }

        ObjectType o = object->GetMetatable().CastTo<T>(object.get());
        SATL_ASSERT(o);
        ValuePtr result = MakeNull();
        [&]<std::size_t... is>(std::index_sequence<is...>)
        {
            if constexpr (std::is_same_v<R, void>)
            {
                (o->*func_)(Convert<Args>(args[is])...);
            }
            else if constexpr (IsOp)
            {
                // Operators are a bit different because they can return different types.
                if constexpr (std::is_same_v<R, T*>)
                {
                    // Binary operators return the result as pointer,
                    // because we do not keep track of allocations.
                    T* opRes = (o->*func_)(Convert<Args>(args[is])...);
                    if (opRes)
                    {
                        Value v = MakeObject(object->GetMetatable(), opRes);
                        result = MakeValue(v);
                    }
                }
                else if constexpr (std::is_same_v<R, Value>)
                {
                    // Operators can also return a Value
                    result = MakeValue((o->*func_)(Convert<Args>(args[is])...));
                }
                else if constexpr (std::is_same_v<R, T&>)
                {
                    // Unary operators return themself by reference
                    T& opRes = (o->*func_)(Convert<Args>(args[is])...);
                    Value v = MakeObject(object->GetMetatable(), &opRes);
                    result = MakeValue(v);
                }
            }
            else
            {
                result = MakeValue((o->*func_)(Convert<Args>(args[is])...));
            }
        }
        (std::index_sequence_for<Args...>{});
        return result;
    }
    FuncType func_;
};

using MetatablePtr = std::unique_ptr<MetaTable>;

template<typename T>
inline MetatablePtr MakeMetatable(const MetaTable* base = nullptr)
{
    return std::make_unique<MetaTable>(TypeName<T>::Get(), base);
}

}
