/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#define SATL_NON_COPYABLE(t) \
private:                     \
    t(const t&) = delete;    \
    t& operator=(const t&) = delete

#define SATL_NON_MOVEABLE(t) \
private:                     \
    t(t&&) = delete;         \
    t& operator=(t&&) = delete
