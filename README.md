# sa::tl

Let's make a scripting language, because ... why not.

![Sly](sly/sly_740.png?raw=true)

## Features

* Tiny embeddable script interpreter in around 11k LOC with exception handling, rudimentary OOP features, operator overloading, simple debugger, type safe binding API for C++ and more.
* Handwritten lexer and recursive descent parser. 100% AI free code.
* AST interpreter.
* There is no main function. A script is a sequence of statements.
* Strictly single threaded, that's a *feature*. Of course, it is possible to implement threading/concurrency with a library, but it would be a challenge to make it not-so-crashy.
* Supports line comments (`//`) and block comments (`/* multiline comment */`).
* All variables must be initialized.
* It has the following types: *null*, *bool*, *integer*, *float*, *string*, *function*, *table* and *object* (C++ object) with optional type hinting.
* Variables can not change their types, they get their type by initializing them.
* Number literals: Hex (`0x2a`), Oct (`0o24`), Bin (`0b101010`), Dec (`3.1415`).
* Other special literals: `null`, `true`, `false`.
* Statements: `if`, `switch`, `for`, `foreach`, `do`, `while`, `function`, `try`, `defer`.
* [First class functions](https://en.wikipedia.org/wiki/First-class_function), [Higher-order functions](https://en.wikipedia.org/wiki/Higher-order_function), [Anonymous functions](https://en.wikipedia.org/wiki/Anonymous_function), [Nested functions](https://en.wikipedia.org/wiki/Nested_function) with lexical scoping and non-local variables ([Closures](https://en.wikipedia.org/wiki/Closure_(computer_programming))), [Function composition](https://en.wikipedia.org/wiki/Function_composition_(computer_science))
* Strings delimiter is either single or double quotes.
* All strings can be multiline.
* String concatenation with the `+` operator (`str = "hello" + " " + "friends!"`). String multiplication with the `*` operator: `str = "foo" * 3; // str == "foofoofoo"`.
* Raw string literals like C++: `str = R"string(this is a raw string anything goes here " ')string"`.
* Basic Unicode/UTF-8 support
* Normal strings (i.e. non raw strings) can contain escape sequences.
* Template literals (delimited with backticks): `` `string text ${expression} string text` ``
* Operator overloading for tables and C++ classes.
* Built in JSON support.
* Easy way to add custom functions.
* Easy and type safe (even const correct, sortof) binding of C++ classes with functions, properties and operators with optional Garbage Collector for managing the lifetime of C ++ objects.
* UI library, HTTP library, XML library, PostgreSQL client library, Graphics library.
* Extensible with custom libraries
* LSP compatible Language Server, works at least with Kate, QtCreator and Neovim
* [KSyntaxHighlighting](https://api.kde.org/frameworks/syntax-highlighting/html/) [definition](satl.xml)
* Some [Vim integration](vim/), syntax, lspconfig

## License

AGPL 3.0

## Requirement

* Linux
* C++23
* CMake
* [Catch2 v2](https://github.com/catchorg/Catch2/tree/v2.x), on Arch you can install it with `sudo pacman -S catch2-v2` (optional)
* [readline](https://archlinux.org/packages/core/x86_64/readline/) (optional)
* ccache (optional)

Some libraries require additional packages.

## Build

```sh
$ git clone https://codeberg.org/trill/satl.git
$ cd satl
$ mkdir build && cd build
$ cmake ..
$ make
```

Optional:

```sh
$ sudo make install
```

On [Arch](https://wiki.archlinux.org/title/CMake_package_guidelines#Prefix_and_library_install_directories) you may want to run CMake with:

```sh
$ cmake .. -DCMAKE_INSTALL_PREFIX=/usr
```

### CMake options

* `SATL_ENABLE_ASSERT` Enable assert() in Release mode (default ON)
* `SATL_TESTS` Build unit tests (default ON)
* `SATL_MATHLIB` Build math library (default ON)
* `SATL_OSLIB` Build OS library (default ON)
* `SATL_IOLIB` Build IO library (default ON)
* `SATL_REGEXLIB` Build regular expression library (default ON)
* `SATL_DALIB` Data access library (default ON)
* `SATL_PGLIB` Build PostgreSQL client library (default ON)
* `SATL_SQLITELIB` Build SQLite library (default ON)
* `SATL_UILIB` Build UI library, requires GTK (default ON)
* `SATL_HTTPLIB` Build HTTP library, requires [cpp-httplib](https://github.com/yhirose/cpp-httplib) (default ON)
* `SATL_ICONVLIB` Build iconv library, requires iconv (default ON)
* `SATL_GFXLIB` Build Graphics library, requires [stb](https://github.com/nothings/stb) (default ON)
* `SATL_XMLLIB` Build XML library, requires [PugiXML](https://pugixml.org/) (default ON)
* `SATL_SAMPLELIB` Build sample extension library (default OFF)
* `SATL_INSTALLMAN` Install man page (default ON)

### Running the tests

When built with `SATL_TESTS` you can run the tests with the following command in the `build` directory:

```sh
$ make test
```

When using ninja:

```sh
$ ninja test
```

## Usage

See the Manual for documentation attached to the [Release](https://codeberg.org/trill/satl/releases).

## Example

### Functions are fist class values

```
function sub(a, b)
{
    return a - b;
}

function doFunc(func, a, b)
{
    return func(a, b);
}

// Pass function as argument
sum = doFunc(sub, 2, 3);
println(sum);

// Pass inline defined function
sum = doFunc(function(a, b) { return a + b; }, 1, 2);
println(sum);
```

See [samples](https://codeberg.org/trill/satl/src/branch/master/samples) for more samples.

## Neovim

Neovim integration with syntax highlight and LSP.

![Neovim](nvim.png?raw=true)

