if exists("b:current_syntax")
  finish
endif

syntax keyword satlKeywords
    \ break
    \ case
    \ catch
    \ const
    \ constructor
    \ continue
    \ default
    \ do
    \ else
    \ fallthrough
    \ finally
    \ for
    \ foreach
    \ function
    \ global
    \ if
    \ in
    \ include
    \ once
    \ operator
    \ return
    \ switch
    \ try
    \ using
    \ while
syntax keyword satlLexerKeywords
    \ __line__
    \ __col__
    \ __file__

syntax keyword satlNull null
syntax keyword satlThis this
syntax keyword satlBooleanTrue true
syntax keyword satlBooleanFalse false

syntax match   satlOperator         "[&#*\[\]^.=!-()%|+?\\/~<>@$]" skipwhite skipempty
syntax match   satlDelimiter        "[\(\){},;]"

" Credits: https://github.com/ovikrai/mojo-syntax/blob/main/syntax/mojo.vim#L91
syntax match   satlNumber            "\<0[oO]\%(_\=\o\)\+\>"
syntax match   satlNumber            "\<0[xX]\%(_\=\x\)\+\>"
syntax match   satlNumber            "\<0[bB]\%(_\=[01]\)\+\>"
syntax match   satlNumber            "\<\%([1-9]\%(_\=\d\)*\|0\+\%(_\=0\)*\)\>"
syntax match   satlNumber            "\<\d\%(_\=\d\)*[eE][+-]\=\d\%(_\=\d\)*\>"
syntax match   satlFloat             "\<\d\%(_\=\d\)*\.\%([eE][+-]\=\d\%(_\=\d\)*\)\=\%(\W\|$\)\@="
" Slightly modified, because we don't have leading, traling decimal points, that would be a syntax error.
syntax match   satlFloat             "\<\d\%(\d\%(_\=\d\)*\)\=\.\d\%(_\=\d\)*\%([eE][+-]\=\d\%(_\=\d\)*\)\=\>"

syntax match   satlIdentifier        /\v[_a-zA-Z]+[_a-zA-Z0-9]*/

syntax match   satlEscape  contained "\v\\((x\x{2})|(u(\x{4}))|(U(\x{8}))|["?\\abvfnrt]|(0[0-7]{0,2}))"

syntax match   satlParen             "("
syntax match   satlFunction          "\w\+\s*(" contains=satlParen

syntax region  satlString            start=+\z(["']\)+   end=+\z1+ contains=satlEscape,@Spell extend
syntax region  satlRawString matchgroup=satlRawDelimiter start=@\%(u8\|[uLU]\)\=R"\z([[:alnum:]_{}[\]#<>%:;.?*\+\-/\^&|~!=,"']\{,16}\)(@ end=/)\z1"/ contains=@Spell
syntax region  satlTemplateExpression contained matchgroup=satlTemplateBraces start=+${+ end=+}+ contains=satlIdentifier,satlOperator,satlDelimiter,satlNumber,satlFloat,satlFunction,satlString,satlKeywords
syntax region  satlTemplateString    start=+`+           end=+`+   contains=satlTemplateExpression,satlEscape,@Spell extend

syntax keyword satlCommentTodo       contained TODO FIXME XXX TBD NOTE
syntax region  satlComment           start=+//+ end=/$/     contains=satlCommentTodo,@Spell extend keepend
syntax region  satlComment           start=+/\*+  end=+\*/+ contains=satlCommentTodo,@Spell fold extend keepend

syntax match   satlDataType /\v:\s*\zs(bool|int|float|string|table|function|object|any)/
syntax region  satlEndOfFile matchgroup=satlEofMarker start="\<__eof__\>" end="\%$"

syntax region  satlShebangLine       start="\%1l#!" end="$"

highlight default link satlString String
highlight default link satlTemplateString String
highlight default link satlRawString String
highlight default link satlEscape SpecialChar
highlight default link satlNumber Number
highlight default link satlFloat Float
highlight default link satlKeywords Keyword
highlight default link satlBooleanTrue Boolean
highlight default link satlBooleanFalse Boolean
highlight default link satlNull Type
highlight default link satlThis Identifier
highlight default link satlIdentifier Identifier
highlight default link satlOperator Operator
highlight default link satlDelimiter Delimiter
highlight default link satlComment Comment
highlight default link satlCommentTodo Todo
highlight default link satlFunction Function
highlight default link satlDataType Type
highlight default link satlRawDelimiter Delimiter
highlight default link satlTemplateBraces Delimiter
highlight default link satlLexerKeywords Constant
highlight default link satlEofMarker Macro
highlight default link satlEndOfFile Comment
highlight default link satlShebangLine Comment
