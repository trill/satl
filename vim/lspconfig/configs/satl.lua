local util = require 'lspconfig.util'

return {
  default_config = {
    cmd = {
      "satld",
--      "--log", "/tmp/satld.log"
    },
    filetypes = { 'satl' },
    root_dir = util.find_git_ancestor,
    single_file_support = true,
  },

  docs = {
    description = [[
https://codeberg.org/trill/satl

Language server for sa::tl.
]],
    default_config = {
      root_dir = [[util.root_pattern(".git")]],
    },
  },
}
