/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <chrono>

using namespace std;

class Timer
{
public:
    auto ns() const { return chrono::duration_cast<chrono::nanoseconds>(Clock::now() - started_).count(); }
    auto us() const { return chrono::duration_cast<chrono::microseconds>(Clock::now() - started_).count(); }
    auto ms() const { return chrono::duration_cast<chrono::milliseconds>(Clock::now() - started_).count(); }
private:
    using Clock = chrono::steady_clock;
    using Time = Clock::time_point;
    Time started_{ Clock::now() };
};
