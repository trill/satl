/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "debugrepl.h"
#ifdef HAVE_READLINE
#include <readline/history.h>
#include <readline/readline.h>
#endif
#include <cstdio>
#include <functional>
#include <sstream>
#include <string>
#include <vector>
#include <utils.h>
#include <dbg_utils.h>

static bool GetInput(const std::string& prompt, std::string& input)
{
#ifdef HAVE_READLINE
    char* i = readline(prompt.c_str());
    if (!i)
        return false;
    input.assign(i);
    free(i);
    return true;
#else
    std::cout << prompt;
    return static_cast<bool>(std::getline(std::cin, input));
#endif
}

static ReplCommand ParseCommand(const std::string& input, std::string& args)
{
    auto p = input.find(' ');
    std::string cmd;
    if (p != std::string::npos)
    {
        cmd = input.substr(0, p);
        args = input.substr(p + 1, std::string::npos);
    }
    else
        cmd = input;

    if (cmd == "b")
        return ReplCommand::Breakpoint;
    if (cmd == "bh")
        return ReplCommand::BreakpointHere;
    if (cmd == "lb")
        return ReplCommand::ListBreakpoints;
    if (cmd == "bt")
        return ReplCommand::Backtrace;
    if (cmd == "s")
        return ReplCommand::Step;
    if (cmd == "ns")
        return ReplCommand::NodeStep;
    if (cmd == "c")
        return ReplCommand::Continue;
    if (cmd == "e")
        return ReplCommand::Evaluate;
    if (cmd == "w")
        return ReplCommand::Watch;
    if (cmd == "ew")
        return ReplCommand::EvalWatches;
    if (cmd == "x")
        return ReplCommand::Execute;
    if (cmd == "n")
        return ReplCommand::DumpNode;
    if (cmd == "l")
        return ReplCommand::DumpLocals;
    if (cmd == "q")
        return ReplCommand::Quit;
    if (cmd == "h")
        return ReplCommand::Help;

    std::cerr << "Unknown command " << cmd << std::endl;
    return ReplCommand::Error;
}

static void PrintHelp()
{
    std::cout << "Debug Commands:" << std::endl;
    std::cout << " b [<file>:]<line>,<col>: Toggle breakpoint" << std::endl;
    std::cout << " bh: Toggle breakpoint here" << std::endl;
    std::cout << " s: Line Step" << std::endl;
    std::cout << " ns: Node Step" << std::endl;
    std::cout << " c: Continue" << std::endl;
    std::cout << " e <expression>: Evaluate expression" << std::endl;
    std::cout << " w <expression>: Watch/unwatch expression" << std::endl;
    std::cout << " ew: Evaluate all watches" << std::endl;
    std::cout << " x <source>: Execute some code" << std::endl;
    std::cout << " lb: List breakpoints" << std::endl;
    std::cout << " bt: Backtrace" << std::endl;
    std::cout << " n: Dump current node" << std::endl;
    std::cout << " l: Dump all local variables" << std::endl;
    std::cout << " h: Show this help" << std::endl;
    std::cout << " q: Quit" << std::endl;
}

DebugRepl::DebugRepl(sa::tl::Context& ctx, sa::tl::Debugger& dbg) : ctx_(ctx), dbg_(dbg)
{
    dbg_.onStep_ = std::bind(&DebugRepl::OnStep, this, std::placeholders::_1);
    dbg_.onBreakpoint_ = std::bind(&DebugRepl::OnBreakpoint, this, std::placeholders::_1);
#ifdef HAVE_READLINE
    rl_bind_key('\t', rl_complete);
#endif
}

ReplCommand DebugRepl::GetCommand(const sa::tl::Node& node)
{
    std::string input;
    std::stringstream ss;
    ss << node.loc_ << "> ";
    if (!GetInput(ss.str(), input))
        return ReplCommand::None;

    auto res = ParseCommand(input, argument_);
#ifdef HAVE_READLINE
    if (res != ReplCommand::None && res != ReplCommand::Error)
        add_history(input.c_str());
#endif
    return res;
}

void DebugRepl::DoCommand(const sa::tl::Node& node)
{
    auto cmd = GetCommand(node);
    while (cmd != ReplCommand::None)
    {
        switch (cmd)
        {
        case ReplCommand::None:
            return;
        case ReplCommand::Breakpoint:
            ToggleBreakpoint();
            break;
        case ReplCommand::BreakpointHere:
            if (dbg_.ToggleBreakpoint(node.loc_))
                std::cout << "Added breakpoint at ";
            else
                std::cout << "Removed breakpoint at ";
            std::cout << node.loc_ << std::endl;
            break;
        case ReplCommand::ListBreakpoints:
        {
            for (const auto& bp : dbg_.GetBreakpoints())
                std::cout << bp << std::endl;
            break;
        }
        case ReplCommand::Backtrace:
        {
            for (const auto& nd : dbg_.GetNodeStack())
                std::cout << nd->loc_ << ": " << nd->GetFriendlyName() << std::endl;
            break;
        }
        case ReplCommand::Step:
        {
            lineStepLine_ = (int)currentLoc_.line;
            dbg_.Step();
            return;
        }
        case ReplCommand::NodeStep:
        {
            lineStepLine_ = -1;
            dbg_.Step();
            return;
        }
        case ReplCommand::Continue:
            return;
        case ReplCommand::Quit:
            dbg_.Stop();
            return;
        case ReplCommand::Error:
            break;
        case ReplCommand::Evaluate:
        {
            if (!argument_.empty())
            {
                auto res = dbg_.Evaluate(sa::tl::Trim(argument_));
                sa::tl::DumpValue(std::cout, *res, 0, true);
            }
            else
                std::cerr << "Argument for evaluate is empty" << std::endl;
            break;
        }
        case ReplCommand::Execute:
        {
            if (!argument_.empty())
            {
                auto res = dbg_.Execute(sa::tl::Trim(argument_));
                sa::tl::DumpValue(std::cout, *res, 0, true);
            }
            else
                std::cerr << "Argument for execute is empty" << std::endl;
            break;
        }
        case ReplCommand::Watch:
            if (!argument_.empty())
                dbg_.ToggleWatch(sa::tl::Trim(argument_));
            else
                std::cerr << "Argument for watch is empty" << std::endl;
            break;
        case ReplCommand::EvalWatches:
            PrintWatches();
            break;
        case ReplCommand::DumpNode:
            if (!dbg_.GetNodeStack().empty())
                dbg_.GetNodeStack().back()->Dump(std::cout, 0);
            break;
        case ReplCommand::DumpLocals:
        {
            ctx_.ForEachVariable([&](const sa::tl::Variable& current) {
                if (current.boundary != ctx_.GetBoundary())
                    return true;
                if (colors_)
                    std::cout << "\033[1m";
                std::cout << current.nameString;
                if (colors_)
                    std::cout << "\033[0m";
                std::cout << std::endl;
                sa::tl::DumpValue(std::cout, *current.value, 0, true);
                return true;
            });
            break;
        }
        case ReplCommand::Help:
            PrintHelp();
            break;
        }
        cmd = GetCommand(node);
    };
}

void DebugRepl::ToggleBreakpoint()
{
    auto p = argument_.find(',');
    if (p == std::string::npos)
    {
        std::cerr << "Wrong breakpoint format, expecting [<file>:]<line>,<col>" << std::endl;
        return;
    }
    std::string sline = sa::tl::Trim(argument_.substr(0, p));
    std::string sfile = currentLoc_.file;
    if (auto fp = sline.find(':'))
    {
        sfile = sline.substr(fp + 1, std::string::npos);
        sline.erase(fp);
    }
    std::string scol = sa::tl::Trim(argument_.substr(p + 1, std::string::npos));
    sa::tl::Location bp;
    bp.file = sfile;
    try
    {
        bp.line = std::stoi(sline) - 1;
        bp.col = std::stoi(scol) - 1;
    }
    catch (const std::invalid_argument& ex)
    {
        std::cerr << ex.what() << std::endl;
        return;
    }
    if (dbg_.ToggleBreakpoint(bp))
        std::cout << "Added breakpoint at ";
    else
        std::cout << "Removed breakpoint at ";
    std::cout << bp << std::endl;
}

void DebugRepl::OnStep(const sa::tl::Node& node)
{
    if (ctx_.stop_)
        return;
    currentLoc_ = node.loc_;
    if (lineStepLine_ == (int)currentLoc_.line)
    {
        dbg_.Step();
        return;
    }

    lineStepLine_ = -1;

    PrtintSourceLine(node);
    PrintWatches();
    DoCommand(node);
}

void DebugRepl::OnBreakpoint(const sa::tl::Node& node)
{
    if (ctx_.stop_)
        return;
    std::cout << "Hit breakpoint at " << node.loc_ << std::endl;
    currentLoc_ = node.loc_;
    PrtintSourceLine(node);
    PrintWatches();
    DoCommand(node);
}

void DebugRepl::PrtintSourceLine(const sa::tl::Node& nd)
{
    auto sourceLine = dbg_.GetSourceLine(nd.loc_);
    if (sourceLine.has_value())
    {
        const auto& line = sourceLine.value();
        if (colors_)
        {
            std::cout << line.substr(0, nd.loc_.col);
            std::cout << "\033[32m";
            std::cout << line.substr(nd.loc_.col);
            std::cout << "\033[0m";
            std::cout << std::endl;
        }
        else
            std::cout << line << std::endl;
    }
    else
        nd.Dump(std::cout, 2);
}

void DebugRepl::PrintWatches()
{
    dbg_.EvalWatches([this](const std::string& expr, const sa::tl::ValuePtr& result)
    {
        if (colors_)
            std::cout << "\033[1m";
        std::cout << expr;
        if (colors_)
            std::cout << "\033[0m";
        std::cout << std::endl;
        sa::tl::DumpValue(std::cout, *result, 2, true);
        return true;
    });
}
