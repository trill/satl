/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <getopt.h>
#include <satl.h>
#include <version.h>
#include <fstream>
#include <sstream>
#include "context.h"
#include "timer.h"
#include "utils.h"
#include <dbg_utils.h>
#include <debugger.h>
#include "debugrepl.h"
#ifdef HAVE_READLINE
#include <readline/history.h>
#include <readline/readline.h>
#endif

static bool printTime = false;
static bool printAST = false;
static bool noExec = false;
static bool noInit = false;
static bool allowLibs = true;
static constexpr const char* INIT_FILE = "~/.config/sly/init.tl";

static void ShowCopy()
{
    std::cout << "Copyright (C) 2021-2023 Stefan Ascher" << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
}

static void ShowInfo()
{
    static constexpr const char* SLY = R"tl( --------------------------------
/ Hello, I am Sly, your friendly \
\ sa::tl script executor.        /
 --------------------------------
       ___-__        /
      /  ___  \     /
     /  / ,. \ |  O  O
     | |  \_/  |  |_/
     _\_\____ /--(__)
   _/_____________/
)tl";
    std::cout << SLY;
}

static void ShowHelp(const char* arg0)
{
    std::cout << "Usage: " << arg0 << " [<options>] [<filename>|-] [<arguments ...>]" << std::endl << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "  -t, --print-time   Print execution time" << std::endl;
    std::cout << "  -a, --print-ast    Print parsed AST" << std::endl;
    std::cout << "  -p, --print-result Print result of evaluation" << std::endl;
    std::cout << "  -e, --exec-source  Execute source, code is expected as option argument. No effect when run with the -r option." << std::endl;
    std::cout << "  -i, --init-file    Use a custoom init script" << std::endl;
    std::cout << "  -c, --check-only   No execute, check syntax errors only. No effect when run with the -r option." << std::endl;
    std::cout << "  -s, --sandboxed    Run in sandboxed environment. Calling an unsafe function causes an error." << std::endl;
    std::cout << "  -d, --debug        Run in debugger. Can not be used with the -r option." << std::endl;
    std::cout << "  -r, --repl         REPL mode.  Can not be used with the -d option." << std::endl;
    std::cout << "  -I, --no-init      Don't run init.tl script" << std::endl;
    std::cout << "  -L, --no-libs      Don't allow to load libraries" << std::endl;
    std::cout << "  -h, --help         Show help and exit" << std::endl;
    std::cout << "  -v, --version      Show program version and exit" << std::endl << std::endl;
    std::cout << "If filename is `-` it reads the script from the standard input stream." << std::endl << std::endl;
    std::cout << "Everything after the filename will be passed as arguments to the script." << std::endl;
}

static std::string GetPath()
{
    char buf[PATH_MAX + 1];
    if (readlink("/proc/self/exe", buf, sizeof(buf) - 1) == -1)
        return "";
    return buf;
}

static std::string ReadStdIn()
{
    std::string line;
    std::stringstream ss;
    while (std::getline(std::cin, line))
    {
        ss << line << '\n';
    }
    return ss.str();
}

static bool GetInput(const std::string& prompt, std::string& input)
{
#ifdef HAVE_READLINE
    char* i = readline(prompt.c_str());
    if (!i)
        return false;
    input.assign(i);
    free(i);
    return true;
#else
    std::cout << prompt;
    return static_cast<bool>(std::getline(std::cin, input));
#endif
}


static void AddSearchPath(sa::tl::Context& ctx, const std::string& name)
{
    if (name.empty())
        return;
    if (sa::tl::DirExists(name))
    {
        ctx.searchPaths_.push_back(name);
        return;
    }
    auto path = sa::tl::ParentPath(name);
    if (!path.empty() && sa::tl::DirExists(path))
        ctx.searchPaths_.push_back(path);
}

static void LoadLib(sa::tl::Context& ctx, const std::string& name)
{
    if (!allowLibs)
    {
        ctx.RuntimeError(sa::tl::Error::UserDefined, "Loading libraries not allowed.");
        return;
    }
    if (name == "std")
    {
        sa::tl::AddStdLib(ctx);
        return;
    }
    if (name == "dbg")
    {
        sa::tl::AddDbgLib(ctx);
        return;
    }
    std::string n = name;
    if (!n.starts_with("libsatl_"))
        n = "libsatl_" + n;
    if (!name.ends_with(".so"))
        n += ".so";
    bool result = ctx.LoadLibrary(n);
    if (!result)
        ctx.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Failed to load library \"{}\". {}", name, ctx.GetDlError()));
}

static bool RunInit(sa::tl::Context& ctx, const std::string& initFile)
{
    const std::string fileName = sa::tl::ExpandPath(initFile.c_str());
    std::ifstream file(fileName);
    if (!file.good())
        return true;
    std::string line;
    std::stringstream ss;
    while (std::getline(file, line))
    {
        ss << line << '\n';
    }
    file.close();
    const std::string source = ss.str();

    sa::tl::Parser parser(source, initFile);
    parser.onInclude_ = [&parser, &initFile](std::string& filename, const sa::tl::Location& loc) -> std::string
    {
        filename = sa::tl::FindRelatedFile(initFile, filename);
        std::string source = sa::tl::ReadScriptFile(filename);
        if (source.empty())
            parser.SyntaxError(loc, std::format("File \"{}\" not found", filename));
        return source;
    };
    auto root = parser.Parse();
    if (!root)
    {
        // Syntax error(s)
        for (const auto& e : parser.GetErrors())
            std::cerr << e << std::endl;
        return false;
    }

    root->Evaluate(ctx);

    return !ctx.HasErrors();
}

static bool Repl(const std::string& exe, const std::vector<std::string>& args, const std::string& initFile, bool sandboxed, int& exitCode)
{
    std::cout << "Sly REPL" << std::endl << std::endl;
    ShowCopy();
    std::cout << std::endl << "Hit Ctrl+D to exit" << std::endl;
    std::cout << std::endl;
    bool colors = !getenv("NO_COLOR");

    sa::tl::Context ctx;
    ctx.AddFunction<void, std::string>("load_lib", [&ctx](const std::string& name) {
        LoadLib(ctx, name);
    });
    ctx.AddFunction<void>("lock_libs", [](){ allowLibs = false; });

    if (!noInit)
    {
        if (!RunInit(ctx, initFile))
            return false;
    }
    ctx.sandboxed_ = sandboxed;
    sa::tl::TablePtr scriptArgs = sa::tl::MakeTable();
    for (size_t i = 0; i < args.size(); ++i)
        scriptArgs->Add((sa::tl::Integer)i, args[i]);
    ctx.AddConst<sa::tl::TablePtr>("_args", std::move(scriptArgs));
    ctx.AddConst("_exe", sa::tl::MakeString(exe));
    ctx.AddVariable("_exit_code", 0);

    std::string input;
    std::vector<std::string> pieces;
    while (GetInput(pieces.empty() ? "> " : "+ ", input))
    {
        auto trimmed = sa::tl::Trim<char>(input);
        if (!trimmed.empty())
        {
#ifdef HAVE_READLINE
            add_history(input.c_str());
#endif
            pieces.push_back(trimmed);
        }
        if (pieces.empty())
            continue;

        std::string source;
        for (const auto& s : pieces)
            source += s + "\n";

        sa::tl::Parser parser(source);
        parser.onInclude_ = [&](std::string& filename, const sa::tl::Location& loc) -> std::string
        {
            filename = sa::tl::FindRelatedFile("./_dummy_", filename);
            std::string source = sa::tl::ReadScriptFile(filename);
            if (source.empty())
                parser.SyntaxError(loc, std::format("File \"{}\" not found", filename));
            AddSearchPath(ctx, filename);
            return source;
        };

        auto root = parser.Parse();
        if (!root)
        {
            // Syntax error(s)
            for (const auto& e : parser.GetErrors())
                std::cerr << e << std::endl;
            // Maybe more input.
            // TODO: This should be more sophisticated.
            continue;
        }
        if (printAST)
            root->Dump(std::cout, 0);

        auto res = root->Evaluate(ctx);
        if (colors)
            std::cout << "\033[1;30m";
        if (res->IsTable())
            sa::tl::DumpValue(std::cout, *res, 0, false);
        else
            std::cout << res->ToString() << std::endl;
        if (colors)
            std::cout << "\033[0m";
        auto ec = ctx.GetValue("_exit_code");
        if (ec && ec->IsInt())
            exitCode = ec->ToInt();

        pieces.clear();
        ctx.ResetErrors();
    }

    return true;
}

static bool Run(std::string_view source, std::string sourceFile,
    const std::string& exe, const std::vector<std::string>& args, bool debug, const std::string& initFile,
    bool sandboxed, bool printResult, int& exitCode)
{
    sa::tl::Context ctx;

    std::unique_ptr<sa::tl::Debugger> debugger;
    if (debug)
        debugger = std::make_unique<sa::tl::Debugger>(ctx);

    ctx.AddFunction<void, std::string>("load_lib", [&ctx](const std::string& name) {
        LoadLib(ctx, name);
    });
    ctx.AddFunction<void>("lock_libs", [](){ allowLibs = false; });

    if (!noInit)
    {
        if (!RunInit(ctx, initFile))
            return false;
    }

    // Add the path of the source file to the search paths so it can find files
    // load()ed with relative filename.
    AddSearchPath(ctx, sourceFile);
    ctx.sandboxed_ = sandboxed;
    if (debugger)
        debugger->AddSource(sourceFile, std::string(source));

    sa::tl::Parser parser(source, sourceFile);
    parser.onInclude_ = [&](std::string& filename, const sa::tl::Location& loc) -> std::string
    {
        filename = sa::tl::FindRelatedFile(sourceFile, filename);
        std::string source = sa::tl::ReadScriptFile(filename);
        if (source.empty())
            parser.SyntaxError(loc, std::format("File \"{}\" not found", filename));
        if (debugger)
            debugger->AddSource(filename, source);
        AddSearchPath(ctx, filename);
        return source;
    };
    auto root = parser.Parse();
    if (!root)
    {
        // Syntax error(s)
        for (const auto& e : parser.GetErrors())
            std::cerr << e << std::endl;
        return false;
    }
    if (printAST)
        root->Dump(std::cout, 0);
    ctx.AddConst("_source", std::move(sourceFile));
    sa::tl::TablePtr scriptArgs = sa::tl::MakeTable();
    for (size_t i = 0; i < args.size(); ++i)
        scriptArgs->Add((sa::tl::Integer)i, args[i]);
    ctx.AddConst<sa::tl::TablePtr>("_args", std::move(scriptArgs));
    ctx.AddConst("_exe", sa::tl::MakeString(exe));
    ctx.AddVariable("_exit_code", 0);

    if (!noExec)
    {
        if (!debug)
        {
            Timer timer;
            auto result = ctx.Run(*root);
            if (printResult)
                std::cout << result->ToString() << std::endl;
            auto ec = ctx.GetValue("_exit_code");
            if (ec && ec->IsInt())
                exitCode = ec->ToInt();

            if (printTime)
            {
                auto elapsed = timer.us();
                if (elapsed > 1000)
                    std::cout << (elapsed / 1000) << "ms" << std::endl;
                else
                    std::cout << elapsed << "us" << std::endl;
            }
        }
        else
        {
            DebugRepl repl(ctx, *debugger);
            if (getenv("NO_COLOR"))
                repl.colors_ = false;
            debugger->Run(root, true);
        }
        if (ctx.HasErrors())
            return false;
    }
    return true;
}

int main(int argc, char** argv)
{
    std::string initFile;
    if (const char* f = getenv("SATL_INIT_SCRIPT"))
        initFile = f;
    else
        initFile = INIT_FILE;

    int c;

    static struct option longOptions[] = { { "print-time", no_argument, nullptr, 't' },
        { "print-ast", no_argument, nullptr, 'a' },
        { "print-result", no_argument, nullptr, 'p' },
        { "exec-source", required_argument, nullptr, 'e' },
        { "check-only", no_argument, nullptr, 'c' },
        { "sandboxed", no_argument, nullptr, 's' },
        { "debug", no_argument, nullptr, 'd' },
        { "repl", no_argument, nullptr, 'r' },
        { "no-init", no_argument, nullptr, 'I' },
        { "init-file", required_argument, nullptr, 'i' },
        { "no-libs", no_argument, nullptr, 'L' },
        { "help", no_argument, nullptr, 'h' },
        { "version", no_argument, nullptr, 'v' },
        { nullptr, 0, nullptr, 0 } };

    bool debug = false;
    bool repl = false;
    int optionIndex = 0;
    bool sandboxed = false;
    bool printResult = false;
    std::string sourceCode;

    while ((c = getopt_long(argc, argv, "tape:csdrIi:Lhv", longOptions, &optionIndex)) != -1)
    {
        switch (c)
        {
        case 't':
            printTime = true;
            break;
        case 'a':
            printAST = true;
            break;
        case 'p':
            printResult = true;
            break;
        case 'e':
            sourceCode = optarg;
            break;
        case 'i':
            initFile = optarg;
            break;
        case 'c':
            noExec = true;
            break;
        case 'd':
            debug = true;
            break;
        case 'r':
            repl = true;
            break;
        case 's':
            sandboxed = true;
            break;
        case 'I':
            noInit = true;
            break;
        case 'L':
            allowLibs = false;
            break;
        case 'h':
            ShowInfo();
            ShowCopy();
            std::cout << std::endl;
            ShowHelp(argv[0]);
            exit(0);
        case 'v':
            std::cout << "Sly " << SATL_VERSION_STRING << std::endl;
            ShowCopy();
            exit(0);
        default:
            break;
        }
    }

    if (repl && debug)
    {
        std::cerr << "-r and -d can not be used together" << std::endl;
        return 1;
    }

    std::string filename;
    std::vector<std::string> scriptArgs;
    if (optind < argc && sourceCode.empty())
    {
        // When no source provided this is the filename
        filename = argv[optind++];
    }
    if (!repl && filename.empty() && sourceCode.empty())
    {
        ShowInfo();
        std::cout << std::endl;
        std::cerr << "Please provide a filename or a source with the -e option" << std::endl << std::endl;
        ShowHelp(argv[0]);
        return 0;
    }

    for (int i = optind; i < argc; ++i)
    {
        scriptArgs.emplace_back(argv[i]);
    }

    int exitCode = 0;
    bool result = false;
    if (!repl)
    {
        std::string contents;
        if (filename == "-")
            contents = ReadStdIn();
        else if (!sourceCode.empty())
            contents = sourceCode;
        else if (!filename.empty())
        {
            contents = sa::tl::ReadScriptFile(filename);
            if (contents.empty())
                std::cerr << std::format("File \"{}\" not found", filename) << std::endl;
        }
        if (contents.empty())
            return 1;
        result = Run(contents, filename, GetPath(), scriptArgs, debug, initFile, sandboxed, printResult, exitCode);
    }
    else
    {
        result = Repl(GetPath(), scriptArgs, initFile, sandboxed, exitCode);
    }

    if (!result)
        return exitCode != 0 ? exitCode : 1;
    return exitCode;
}
