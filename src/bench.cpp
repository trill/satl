/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "timer.h"
#include <satl.h>

/*
 * Python 3.9.5:
 *   real    0m0,106s
 *   user    0m0,106s
 *   sys     0m0,000s
 *
 * Ruby 3.0:
 *   real    0m0,076s
 *   user    0m0,072s
 *   sys     0m0,003s
 *
 * Lua 5.4.3:
 *   real    0m0,036s
 *   user    0m0,034s
 *   sys     0m0,000s
 *
 * LuaJIT 2.0.5 (WTF?):
 *   real    0m0,006s
 *   user    0m0,005s
 *   sys     0m0,000s
 *
 * Native (`g++ -O3 fib.cpp -o fib`):
 *   real    0m0,003s
 *   user    0m0,003s
 *   sys     0m0,000s
 *
 * sa::tl (sigh):
 *   real    0m0,402s
 *   user    0m0,398s
 *   sys     0m0,000s
 */
// 5x slower than Python ;(

int main()
{
    static constexpr const char* Soruce = R"tl(
function fib(n)
{
    if (n < 2)
        return n;
    return fib(n - 1) + fib(n - 2);
}

for (i = 0; i < 100; ++i)
    assert(fib(20) === 6765);
)tl";

    sa::tl::Parser parser(Soruce);
    auto root = parser.Parse();
    sa::tl::Context ctx;
    sa::tl::AddDbgLib(ctx);

    Timer timer;
    root->Evaluate(ctx);

    auto elapsed = timer.ms();
    std::cout << elapsed << "ms" << std::endl;
    return 0;
}
