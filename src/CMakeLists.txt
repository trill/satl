project (satl CXX)

include(GNUInstallDirs)
include(CheckIncludeFiles)
# FILE type is needed to find readline
check_include_files("stdio.h;readline/history.h;readline/readline.h" HAVE_READLINE_H)

set(PG_SOURCES main.cpp timer.h)
set(SLY_SOURCES sly.cpp debugrepl.h debugrepl.cpp timer.h)
set(BENCH_SOURCES bench.cpp timer.h)

add_executable(pg ${PG_SOURCES})
add_executable(sly ${SLY_SOURCES})
add_executable(bench ${BENCH_SOURCES})

target_link_libraries(sly satl pthread std_lib dbg_lib)

if (HAVE_READLINE_H)
    target_link_libraries(sly readline)
    target_compile_definitions(sly PRIVATE HAVE_READLINE)
endif(HAVE_READLINE_H)

set_property(TARGET sly PROPERTY POSITION_INDEPENDENT_CODE True)

target_link_libraries(pg satl pthread std_lib dbg_lib bind)
set_property(TARGET pg PROPERTY POSITION_INDEPENDENT_CODE True)
target_compile_options(pg PRIVATE -Wno-unused-function)

target_link_libraries(bench satl pthread)
set_property(TARGET bench PROPERTY POSITION_INDEPENDENT_CODE True)

if (BUILD_TESTING)
    add_test(
        NAME ScriptsTestRuns
        COMMAND sly -I "${CMAKE_SOURCE_DIR}/samples/tests/run.tl"
    )
    add_test(
        NAME JSONScriptsTestRuns
        COMMAND sly -I "${CMAKE_SOURCE_DIR}/samples/tests/json_suite.tl"
    )
endif(BUILD_TESTING)

install(TARGETS sly DESTINATION bin)
if (SATL_INSTALLMAN)
    install(FILES ${CMAKE_SOURCE_DIR}/sly.1 DESTINATION ${CMAKE_INSTALL_FULL_MANDIR}/man1)
endif(SATL_INSTALLMAN)
