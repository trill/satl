/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <satl.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include "timer.h"
#include <object.h>
#include <factory.h>
#include <string>

// Playground

namespace test {

class TestClass : public sa::tl::bind::Object
{
    TYPED_OBJECT(TestClass);
public:
    TestClass(sa::tl::Context& ctx) :
        Object(ctx)
    {}
    std::string GetString() const { return string_; }
    void SetString(std::string value) { string_ = std::move(value); }
    void NonConst(std::string x)
    {
        std::cout << "non-const " << x << std::endl;
    }
    void Const(std::string x) const
    {
        std::cout << "const " << x << std::endl;
    }
    sa::tl::Value OpNot() const
    {
        return true;
    }

private:
    std::string string_;
};

}

namespace sa::tl::bind {
template<>
sa::tl::MetaTable& Register<test::TestClass>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<test::TestClass, Object>();
    result
        .AddFunction<test::TestClass, void, std::string>("non_const_func", &test::TestClass::NonConst)
        .AddFunction<test::TestClass, void, std::string>("const_func", &test::TestClass::Const)
        .AddProperty("string", &test::TestClass::GetString, &test::TestClass::SetString)
        .AddOperator("!operator", &test::TestClass::OpNot);

    return result;
}

}

static void AddLibs(sa::tl::Context& ctx);

static bool Test()
{
    static constexpr const char* Src = R"tl(
const j = R"json(["a a"])json";

v = json.load_string(j);
dump(v);
)tl";

    sa::tl::Parser parser(Src);
    parser.onInclude_ = [](std::string& filename, const sa::tl::Location&) -> std::string
    {
        std::cout << filename << std::endl;
        return "i = 1;";
    };
    auto root = parser.Parse();
    if (!root)
    {
        // Syntax error(s)
        for (const auto& e : parser.GetErrors())
            std::cerr << e << std::endl;
        return false;
    }

    sa::tl::Context ctx;
    AddLibs(ctx);
    sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    auto& meta = sa::tl::bind::Register<test::TestClass>(ctx);
    test::TestClass test(ctx);
    ctx.SetInstance(meta, "test", test, -1, true);
    ctx.AddFunction<sa::tl::Value>("test_class", [&ctx, &meta] () -> sa::tl::Value
    {
        return sa::tl::bind::CreateInstance<test::TestClass>(ctx, meta, ctx);
    });

    Timer timer;
    [[maybe_unused]] auto result = ctx.Run(*root);

    auto elapsed = timer.ms();
    std::cout << elapsed << "ms" << std::endl;
    if (ctx.HasErrors())
        return false;
    return true;
}

static void AddLibs(sa::tl::Context& ctx)
{
    sa::tl::AddStdLib(ctx);
    sa::tl::AddDbgLib(ctx);
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/http/libsatl_http.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }
/*    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/io/libsatl_io.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }*/
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/ui/libsatl_ui.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/xml/libsatl_xml.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/math/libsatl_math.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }
/*    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/da/libsatl_da.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/os/libsatl_os.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/gfx/libsatl_gfx.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }*/
    if (!ctx.LoadLibrary("/home/sa/src/satl/build-Debug/libraries/sqlite/libsatl_sqlite.so"))
    {
        std::cerr << "Error loading library " << ctx.GetDlError() << std::endl;
    }

    ctx.AddFunction<bool, std::string>("load_lib",
        [&ctx](const std::string& name) -> bool
        {
            if (name.ends_with(".so"))
            {
                bool result = ctx.LoadLibrary(name);
                if (!result)
                    ctx.RuntimeError(sa::tl::Error::Code::UserDefined, "Error loading library \"" + name + "\". " + ctx.GetDlError());
                return result;
            }
            if (name == "std")
            {
                sa::tl::AddStdLib(ctx);
                return true;
            }
            if (name == "dbg")
            {
                sa::tl::AddDbgLib(ctx);
                return true;
            }
            ctx.RuntimeError(sa::tl::Error::Code::InvalidArgument, "Unknown library \"" + name + "\"");
            return false;
        });
}

int main()
{
    return Test() ? 0 : 1;
}
