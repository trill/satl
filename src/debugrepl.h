/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <context.h>
#include <debugger.h>

enum class ReplCommand
{
    Error,
    None,
    Help,
    Quit,
    Step,
    NodeStep,
    Continue,
    Breakpoint,
    BreakpointHere,
    ListBreakpoints,
    Backtrace,
    Evaluate,
    Watch,
    EvalWatches,
    Execute,
    DumpNode,
    DumpLocals
};

class DebugRepl
{
public:
    DebugRepl(sa::tl::Context& ctx, sa::tl::Debugger& dbg);
    bool colors_{ true };
private:
    void OnStep(const sa::tl::Node& node);
    void OnBreakpoint(const sa::tl::Node& node);
    ReplCommand GetCommand(const sa::tl::Node&);
    void DoCommand(const sa::tl::Node&);
    void ToggleBreakpoint();
    void PrintWatches();
    void PrtintSourceLine(const sa::tl::Node& nd);

    sa::tl::Context& ctx_;
    sa::tl::Debugger& dbg_;
    std::string argument_;
    sa::tl::Location currentLoc_;
    int lineStepLine_{ -1 };
};
