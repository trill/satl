% !TEX root = manual.tex
% vim: textwidth=80 tabstop=4 shiftwidth=4 expandtab
%
% Copyright (c) 2022-2024, Stefan Ascher
%
% SPDX-License-Identifier: CC-BY-4.0
%

\usepackage{silence}
\WarningsOff[everypage]

\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{imakeidx}
\usepackage{listings}
\usepackage{nonumonpart}   % Part pages without page numbers
\usepackage{xcolor}
\usepackage{textcomp}
\usepackage{booktabs, multirow, tabularx, threeparttable}
\usepackage{float}
\usepackage{microtype}
\usepackage[english]{babel}
\usepackage{url}
\usepackage[inline]{enumitem}
\usepackage{fancyvrb}
\usepackage{fancyhdr}
\usepackage{afterpage}
\usepackage{multicol}
\usepackage[colorlinks, breaklinks, pdftex]{hyperref}
\usepackage[depth=4]{bookmark}% Show up to level 4 (\paragraph) in bookmarks
\usepackage{chngcntr}% table numbers prefixed with the corresponding chapter number
\usepackage{datetime2}
\makeatletter
\newcommand{\monthyeardate}{%
	\DTMenglishmonthname{\@dtm@month}, \@dtm@year
}
\makeatother
\counterwithin{table}{chapter}
\AtBeginDocument{\counterwithin{lstlisting}{chapter}}
\usepackage[pages=some,placement=top]{background}
\backgroundsetup{
    scale=1,
    color=black,
    opacity=0.95,
    angle=0,
    contents={%
        \includegraphics[width=0.9\paperwidth]{sly_1080.png}
    }
}

\hypersetup{
    linkcolor=black,
    citecolor=black,
    filecolor=black,
    urlcolor=black,
    pdftitle = {sa::tl The Manual v. \Version},
    pdfauthor = {\Author}
}
\definecolor{darkgreen}{rgb}{0.0, 0.4, 0.0}
\definecolor{darkred}{rgb}{0.75, 0.0, 0.0}
\definecolor{comment}{rgb}{0.0, 0.5, 0.0}
\definecolor{string}{rgb}{0.7, 0.2, 0.1}
\definecolor{keyword}{rgb}{0.0, 0.0, 0.8}
\definecolor{type}{rgb}{0.17, 0.44, 0.27}

\lstset{ %
    basicstyle=\ttfamily\footnotesize,
    stringstyle=\color{string},
    commentstyle=\color{comment}\slshape,
    keywordstyle=\color{keyword}\bfseries,
    ndkeywordstyle={\color{type}},
    numberstyle={\color{black}},
    identifierstyle={\color{black}},
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    extendedchars=true,
    keepspaces=true,
    numbers=none,
    numbersep=5pt,
    showspaces=false,
    showtabs=false,
    stepnumber=5,
    frame=single,
    tabsize=4,
    inputencoding=utf8
}
% listings cannot handle UTF8-characters.
% Oh man that took me hours to figure out!
\lstset{literate=%
    {Ö}{{\"O}}1
    {Ä}{{\"A}}1
    {Ü}{{\"U}}1
    {ß}{{\ss}}2
    {ü}{{\"u}}1
    {ä}{{\"a}}1
    {ö}{{\"o}}1
}

\DefineVerbatimEnvironment{code}{Verbatim}{fontsize=\scriptsize,frame=none,framesep=2mm}

% <keystroke>
\newcommand{\keystroke}[1]{$\langle\hbox{\texttt{#1}}\rangle$}
\newcommand{\icode}[1]{\texttt{#1}}
\newcommand{\scode}[1]{\lstinline[language=satl,basicstyle=\ttfamily\normalsize]'#1'}
\newcommand{\ccode}[1]{\lstinline[language=c++,basicstyle=\ttfamily\normalsize]'#1'}

\newcommand{\Cpp}{C++}
% A Box
\newcommand{\hint}[2]{
    \begin{center}
        \setlength\fboxsep{4pt}
        \fbox{\medskip\parbox{0.95\textwidth}{
            \textbf{#1~~}\emph{#2}
        }\medskip}
    \end{center}
}
% FAQ
\newcommand{\question}[1]{\item[\textbf{Question}] #1 \vspace{-1.5ex}}
\newcommand{\answer}[1]{\item[\textbf{Answer}] #1}
\newcommand*{\blankpage}{%
    \null
    \thispagestyle{empty}
    \addtocounter{page}{-1}
    \newpage
}
\newcommand*\bang{!}
