#!/bin/bash

# Convert all *.dot files to PDFs

for i in ./*.dot; do
    dot -Tpdf -o ${i%.dot}.pdf $i
done
