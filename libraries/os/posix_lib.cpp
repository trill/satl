/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "posix_lib.h"
#include <fcntl.h>
#include <module.h>
#include <cstring>
#include <sys/utsname.h>
#include <sys/stat.h>
#include "os_error.h"
#include <linux/kernel.h>
#include <sys/sysinfo.h>
#include <sys/socket.h>
#include <csignal>

using namespace sa::tl;

void AddPosixLib(Context& ctx)
{
    auto module = MakeModule();

    module->AddConst<Integer>("CLOCKS_PER_SEC", CLOCKS_PER_SEC);

    // Errors
    module->AddConst<Integer>("EPERM", EPERM);
    module->AddConst<Integer>("ENOENT", ENOENT);
    module->AddConst<Integer>("ESRCH", ESRCH);
    module->AddConst<Integer>("EINTR", EINTR);
    module->AddConst<Integer>("EIO", EIO);
    module->AddConst<Integer>("ENXIO", ENXIO);
    module->AddConst<Integer>("E2BIG", E2BIG);
    module->AddConst<Integer>("ENOEXEC", ENOEXEC);
    module->AddConst<Integer>("EBADF", EBADF);
    module->AddConst<Integer>("ECHILD", ECHILD);
    module->AddConst<Integer>("EAGAIN", EAGAIN);
    module->AddConst<Integer>("ENOMEM", ENOMEM);
    module->AddConst<Integer>("EACCES", EACCES);
    module->AddConst<Integer>("EFAULT", EFAULT);
    module->AddConst<Integer>("ENOTBLK", ENOTBLK);
    module->AddConst<Integer>("EBUSY", EBUSY);
    module->AddConst<Integer>("EEXIST", EEXIST);
    module->AddConst<Integer>("EXDEV", EXDEV);
    module->AddConst<Integer>("ENODEV", ENODEV);
    module->AddConst<Integer>("ENOTDIR", ENOTDIR);
    module->AddConst<Integer>("EISDIR", EISDIR);
    module->AddConst<Integer>("EINVAL", EINVAL);
    module->AddConst<Integer>("ENFILE", ENFILE);
    module->AddConst<Integer>("EMFILE", EMFILE);
    module->AddConst<Integer>("ENOTTY", ENOTTY);
    module->AddConst<Integer>("ETXTBSY", ETXTBSY);
    module->AddConst<Integer>("ENOSPC", ENOSPC);
    module->AddConst<Integer>("ESPIPE", ESPIPE);
    module->AddConst<Integer>("EROFS", EROFS);
    module->AddConst<Integer>("EMLINK", EMLINK);
    module->AddConst<Integer>("EPIPE", EPIPE);
    module->AddConst<Integer>("EDOM", EDOM);
    module->AddConst<Integer>("ERANGE", ERANGE);

    // open() flags
    module->AddConst<Integer>("O_APPEND", O_APPEND);
    module->AddConst<Integer>("O_ASYNC", O_ASYNC);
    module->AddConst<Integer>("O_CLOEXEC", O_CLOEXEC);
    module->AddConst<Integer>("O_CREAT", O_CREAT);
    module->AddConst<Integer>("O_DIRECT", O_DIRECT);
    module->AddConst<Integer>("O_DIRECTORY", O_DIRECTORY);
    module->AddConst<Integer>("O_DSYNC", O_DSYNC);
    module->AddConst<Integer>("O_EXCL", O_EXCL);
    module->AddConst<Integer>("O_LARGEFILE", O_LARGEFILE);
    module->AddConst<Integer>("O_NOATIME", O_NOATIME);
    module->AddConst<Integer>("O_NOCTTY", O_NOCTTY);
    module->AddConst<Integer>("O_NOFOLLOW", O_NOFOLLOW);
    module->AddConst<Integer>("O_NONBLOCK", O_NONBLOCK);
    module->AddConst<Integer>("O_NDELAY", O_NDELAY);
    module->AddConst<Integer>("O_PATH", O_PATH);
    module->AddConst<Integer>("O_SYNC", O_SYNC);
    module->AddConst<Integer>("O_TMPFILE", O_TMPFILE);
    module->AddConst<Integer>("O_TRUNC", O_TRUNC);

    // Seek whence
    module->AddConst<Integer>("SEEK_SET", SEEK_SET);
    module->AddConst<Integer>("SEEK_CUR", SEEK_CUR);
    module->AddConst<Integer>("SEEK_END", SEEK_END);
    module->AddConst<Integer>("SEEK_DATA", SEEK_DATA);
    module->AddConst<Integer>("SEEK_HOLE", SEEK_HOLE);

    // Signals
    module->AddConst<int>("SIGINT", SIGINT);
    module->AddConst<int>("SIGQUIT", SIGQUIT);
    module->AddConst<int>("SIGKILL", SIGKILL);
    module->AddConst<int>("SIGTERM", SIGTERM);
    module->AddConst<int>("SIGCONT", SIGCONT);
    module->AddConst<int>("SIGSTOP", SIGSTOP);

    module->AddFunction<Integer>("clock", &clock);
    module->AddFunction<std::string, std::string>("getenv",
        [](const std::string& name) -> std::string
        {
            char* value = getenv(name.c_str());
            if (value)
                return value;
            return "";
        });
    module->AddFunction("setenv",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.empty())
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            const std::string name = args[0]->ToString();
            if (args.size() == 2)
            {
                const std::string value = args[1]->ToString();
                int result = setenv(name.c_str(), value.c_str(), 1);
                return MakeValue(result);
            }
            int result = unsetenv(name.c_str());
            return MakeValue(result);
        }, false);
    module->AddFunction<std::string, std::string>("realpath", [&ctx](const std::string& path) -> std::string {
        char buf[PATH_MAX];
        char* res = realpath(path.c_str(), buf);
        if (res == nullptr)
        {
            RaiseOSError(ctx);
            return "";
        }
        return res;
    });

    module->AddFunction<Integer>("fork", &fork, false);
    module->AddFunction<Integer, std::string, TablePtr>("execv",
        [&ctx](const std::string& path, const TablePtr& arg) -> Integer
        {
            std::vector<std::string> args;
            for (const auto& a : *arg)
                args.push_back(a.second->ToString());
            char** arr = new char*[arg->size() + 1];
            for (size_t i = 0; i < args.size(); ++i)
                arr[i] = (char*)args[i].c_str();
            arr[args.size()] = nullptr;

            auto r = execv(path.c_str(), arr);
            if (r < 0)
                RaiseOSError(ctx);
            delete[] arr;
            return r;
        }, false);
    module->AddFunction<Integer, std::string, TablePtr>("execvp",
        [&ctx](const std::string& path, const TablePtr& arg) -> Integer
        {
            std::vector<std::string> args;
            for (const auto& a : *arg)
                args.push_back(a.second->ToString());
            char** arr = new char*[arg->size() + 1];
            for (size_t i = 0; i < args.size(); ++i)
                arr[i] = (char*)args[i].c_str();
            arr[args.size()] = nullptr;

            auto r = execvp(path.c_str(), arr);
            if (r < 0)
                RaiseOSError(ctx);
            delete[] arr;
            return r;
        }, false);
    module->AddFunction<Integer, std::string, const Callable&>("popen",
        [&ctx](const std::string& cmd, const Callable& callback) -> Integer
        {
            if (callback.ParamCount() != 1)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return 0;
            }

            std::array<char, 128> buffer{};
            FILE* pipe = popen(cmd.c_str(), "r");
            if (!pipe)
            {
                ctx.RuntimeError(Error::Code::OSError, "Can not start command");
                return 0;
            }
            while (fgets(buffer.data(), 128, pipe) != nullptr)
            {
                if (ctx.stop_)
                    break;
                callback(ctx, { MakeValue(std::string(buffer.data())) });
            }
            return (Integer)pclose(pipe);
        }, false);
    module->AddFunction<Integer>("getpid", &getpid, false);
    module->AddFunction<Integer>("getppid", &getppid, false);
    module->AddFunction<Integer>("getgid", &getgid, false);
    module->AddFunction<Integer>("getuid", &getuid, false);
    module->AddFunction<TablePtr>("uname",
        []() -> TablePtr
        {
            utsname u = {};
            uname(&u);
            auto result = MakeTable();
            result->Add("sysname", std::string(u.sysname));
            result->Add("nodename", std::string(u.nodename));
            result->Add("release", std::string(u.release));
            result->Add("version", std::string(u.version));
            result->Add("machine", std::string(u.machine));
            return result;
        });
    module->AddFunction<TablePtr>("sysinfo",
        [&ctx]() -> TablePtr
        {
            struct sysinfo s_info = {};
            int error = sysinfo(&s_info);
            if (error != 0)
            {
                RaiseOSError(ctx);
                return MakeTable();
            }

            auto result = MakeTable();
            result->Add<Integer>("uptime", s_info.uptime);
            result->Add<Integer>("loads0", s_info.loads[0]);
            result->Add<Integer>("loads1", s_info.loads[1]);
            result->Add<Integer>("loads2", s_info.loads[2]);
            result->Add<Integer>("totalram", s_info.totalram);
            result->Add<Integer>("freeram", s_info.freeram);
            result->Add<Integer>("sharedram", s_info.sharedram);
            result->Add<Integer>("bufferram", s_info.bufferram);
            result->Add<Integer>("totalswap", s_info.totalswap);
            result->Add<Integer>("freeswap", s_info.freeswap);
            result->Add<Integer>("procs", s_info.procs);
            result->Add<Integer>("totalhigh", s_info.totalhigh);
            result->Add<Integer>("freehigh", s_info.freehigh);
            result->Add<Integer>("mem_unit", s_info.mem_unit);
            return result;
        });
    module->AddFunction<Value, std::string>("stat",
        [&ctx](const std::string& path) -> Value
        {
            struct stat s = {};
            auto res = stat(path.c_str(), &s);
            if (res != 0)
            {
                RaiseOSError(ctx);
                return res;
            }
            auto result = MakeTable();
            result->Add<Integer>("st_dev", s.st_dev);
            result->Add<Integer>("st_ino", s.st_ino);
            result->Add<Integer>("st_mode", s.st_mode);
            result->Add<Integer>("st_nlink", s.st_nlink);
            result->Add<Integer>("st_uid", s.st_uid);
            result->Add<Integer>("st_gid", s.st_gid);
            result->Add<Integer>("st_rdev", s.st_rdev);
            result->Add<Integer>("st_size", s.st_size);
            result->Add<Integer>("st_blksize", s.st_blksize);
            result->Add<Integer>("st_blocks", s.st_blocks);
            result->Add<Integer>("st_atim", s.st_atim.tv_sec);
            result->Add<Integer>("st_mtim", s.st_mtim.tv_sec);
            result->Add<Integer>("st_ctim", s.st_ctim.tv_sec);
            return result;
        }, false);
    module->AddFunction<std::string, std::string>("readlink",
        [&ctx](const std::string& path) -> std::string
        {
            char buff[PATH_MAX + 1] = {};
            ssize_t nbytes = readlink(path.c_str(), buff, PATH_MAX - 1);
            if (nbytes == -1)
            {
                RaiseOSError(ctx);
                return "";
            }
            return { buff, static_cast<size_t>(nbytes) };
        }, false);
    module->AddFunction<void, std::string>("perror",
        [](const std::string& msg) -> void
        {
            perror(msg.empty() ? nullptr : msg.c_str());
        });
    module->AddFunction<Integer>("errno",
        []() -> Integer
        {
            return errno;
        });
    module->AddFunction<std::string, Integer>("strerror",
        [](Integer err) -> std::string
        {
            return std::strerror((int)err);
        });
    module->AddFunction<Integer, const char*, int>("access", &access, false);
    // https://www.man7.org/linux/man-pages/man2/open.2.html
    module->AddFunction<Integer, const char*, int>("open", &open, false);
    module->AddFunction<Integer, const char*, int>("creat", &creat, false);
    module->AddFunction<Integer, const char*, int>("mkfifo", &mkfifo, false);
    // https://www.man7.org/linux/man-pages/man2/pipe.2.html
    module->AddFunction<Integer, const TablePtr&, int>("pipe",
        [](const TablePtr& pipefd, int flags) -> Integer
        {
            int fds[2] = {};
            int res = pipe2(fds, flags);
            pipefd->Clear();
            pipefd->Add(fds[0]);
            pipefd->Add(fds[1]);
            return res;
        }, false);
    // https://www.man7.org/linux/man-pages/man2/socketpair.2.html
    module->AddFunction<Integer, int, int, int, const TablePtr&>("socketpair",
        [](int domain, int type, int protocol, const TablePtr& sv) -> Integer
        {
            int fds[2] = {};
            int res = socketpair(domain, type, protocol, fds);
            sv->Clear();
            sv->Add(fds[0]);
            sv->Add(fds[1]);
            return res;
        }, false);
    // https://www.man7.org/linux/man-pages/man2/close.2.html
    module->AddFunction<Integer, Integer>("close", &close, false);
    // https://www.man7.org/linux/man-pages/man2/write.2.html
    module->AddFunction<Integer, int, std::string, int>("write",
        [](int fd, const std::string& buf, int size) -> Integer
        {
            return write(fd, buf.data(), size);
        }, false);
    // https://www.man7.org/linux/man-pages/man2/read.2.html
    module->AddFunction<Integer, int, const StringPtr&, int>("read",
        [](int fd, const StringPtr& buf, int size) -> Integer
        {
            buf->resize(size);
            return read(fd, buf->data(), size);
        }, false);
    // https://www.man7.org/linux/man-pages/man2/lseek.2.html
    module->AddFunction<Integer, int, unsigned, int>("lseek", &lseek, false);
    module->AddFunction<std::string>("getwd",
        []() -> std::string
        {
            char buf[PATH_MAX] = {};
            char* res = getcwd(buf, sizeof(buf));
            if (res == nullptr)
                return "";
            return buf;
        }, false);
    module->AddFunction<Integer, const char*, int>("mkdir", &mkdir, false);
    module->AddFunction<Integer, const char*>("rmdir", &rmdir, false);
    module->AddFunction<Integer, const char*>("chdir", &chdir, false);
    module->AddFunction<Integer, const char*, const char*>("link", &link, false);
    module->AddFunction<Integer, const char*>("unlink", &unlink, false);
    module->AddFunction<Integer, const char*, const char*>("rename", &rename, false);
    module->AddFunction<Integer, const char*, int>("chmod", &chmod, false);
    module->AddFunction<Integer, const char*, int, int>("chown", &chown, false);
    module->AddFunction<unsigned, unsigned>("umask", &umask, false);
    module->AddFunction<int, int, int>("kill", &kill, false);

    ctx.AddConst("posix", std::move(module), SCOPE_LIBRARY);
}
