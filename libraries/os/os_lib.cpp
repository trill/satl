/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <ast.h>
#include <context.h>
#include <module.h>
#include <utils.h>
#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <thread>
#include <ctime>
#include <sys/types.h>
#include <unistd.h>
#include "posix_lib.h"
#include <clocale>
#include <spawn.h>

using namespace sa::tl;

static std::string RemoveSlash(const std::string& name)
{
    if (name.empty())
        return "";
    if (name.ends_with('/'))
        return name.substr(0, name.length() - 1);
    return name;
}

extern "C" bool Init(Context& ctx);
extern "C" bool Init(Context& ctx)
{
    if (ctx.HasValue("os"))
        return true;

    auto module = MakeModule();
    module->AddConst<Integer>("LC_CTYPE", LC_CTYPE);
    module->AddConst<Integer>("LC_NUMERIC", LC_NUMERIC);
    module->AddConst<Integer>("LC_TIME", LC_TIME);
    module->AddConst<Integer>("LC_COLLATE", LC_COLLATE);
    module->AddConst<Integer>("LC_MONETARY", LC_MONETARY);
    module->AddConst<Integer>("LC_MESSAGES", LC_MESSAGES);
    module->AddConst<Integer>("LC_ALL", LC_ALL);

    module->AddFunction<std::string, std::string>("file_read",
        [](const std::string& filename) -> std::string
        {
            // Read file contents
            // file_read(filename) -> string
            if (filename.empty())
                return "";
            std::ifstream f(filename);
            if (!f.is_open())
                return "";
            std::stringstream buffer;
            buffer << f.rdbuf();
            return buffer.str();
        });
    module->AddFunction<bool, std::string, std::string>("file_write",
        [](const std::string& filename, const std::string& contents) -> bool
        {
            // Write to file
            // file_write(filename, content) -> bool
            if (filename.empty())
                return false;
            std::ofstream f(filename);
            if (!f.is_open())
                return false;
            f << contents;
            return true;
        }, false);
    module->AddFunction<bool, std::string>("file_exists", &FileExists);
    module->AddFunction<int, std::string>("file_size",
        [](const std::string& file) -> int
        {
            std::ifstream in(file, std::ifstream::ate | std::ifstream::binary);
            if (!in.good())
                return -1;
            return (int)in.tellg();
        });
    module->AddFunction<bool, std::string, std::string>("file_copy",
        [](const std::string& src, const std::string& dst) -> bool
        {
            // Copy a file
            // file_copy(source, dest) -> bool
            std::ifstream fsrc(src, std::ios::binary);
            if (!fsrc.is_open())
                return false;
            std::ofstream fdst(dst, std::ios::binary);
            if (!fdst.is_open())
                return false;
            fdst << fsrc.rdbuf();
            return true;
        }, false);
    module->AddFunction<bool, std::string>("make_dir",
        [&ctx](const std::string& dirname) -> bool
        {
            // Make a directory
            // make_dir(name) -> bool
            namespace fs = std::filesystem;
            fs::path p(dirname);
            std::error_code err;
            bool exists = fs::exists(p, err);
            if (err)
            {
                ctx.RuntimeError(Error::Code::OSError, err.message());
                return false;
            }
            if (exists)
                return true;
            bool success = fs::create_directories(p, err);
            if (err)
            {
                ctx.RuntimeError(Error::Code::OSError, err.message());
                return false;
            }
            return success;
        }, false);
    module->AddFunction<bool, std::string>("dir_exists", &DirExists);
    module->AddFunction<bool, std::string>("hidden_file", &HiddenFile);
    module->AddFunction("scan_dir",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // scan_dir(dirname, callback[, recursive]);
            // Callback must return true -> continue or false -> break
            namespace fs = std::filesystem;

            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[1]->Is<CallablePtr>())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Callback must be a function");
                return MakeNull();
            }
            const auto& callback = *args[1]->As<CallablePtr>();
            if (callback.ParamCount() != 1)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return MakeNull();
            }
            bool recursive = false;
            if (args.size() == 3)
            {
                recursive = args[2]->ToBool();
            }
            const std::string dirname = args[0]->ToString();
            fs::path dir = fs::path(dirname);

            auto call = [&ctx, &callback](std::string dir) -> bool
            {
                auto result = callback(ctx, { MakeValue(std::move(dir)) });
                if (!result)
                    return false;
                return result->ToBool();
            };

            auto iterate = [&]<typename Iterator>(Iterator it)
            {
                for (const auto& dirEntry : it)
                {
                    if (!call(dirEntry.path().string()))
                        break;
                }
            };

            if (!recursive)
                iterate(fs::directory_iterator(dir));
            else
                iterate(fs::recursive_directory_iterator(dir));
            return MakeNull();
        });
    module->AddFunction<std::string>("get_current_dir",
        [&ctx]() -> std::string
        {
            namespace fs = std::filesystem;

            std::error_code err;
            auto path = fs::current_path(err);
            if (!err)
                return path.string();
            ctx.RuntimeError(Error::Code::OSError, err.message());
            return {};
        });
    module->AddFunction<bool, std::string>("set_current_dir",
        [](const std::string& dirname) -> bool
        {
            namespace fs = std::filesystem;
            fs::path dir = fs::path(dirname);

            std::error_code err;
            fs::current_path(dir, err);
            return !err;
        });
    module->AddFunction<Integer, const char*>("system", &system, false);
    module->AddFunction<bool, std::string>("run_command",
        [](const std::string& command) -> bool
        {
            char* cmd = (char*)command.c_str();
            pid_t pid = 0;
            char* argv[] = { (char*)"sh", (char*)"-c", cmd, nullptr };
            int status = posix_spawn(&pid, "/bin/sh", nullptr, nullptr, argv, environ);
            return status == 0;
        }, false);
    module->AddFunction<Integer, std::string>("file_time",
        [&ctx](const std::string& value) -> Integer
        {
            namespace fs = std::filesystem;

            fs::path p = fs::path(value);
            std::error_code err;
            auto time = fs::last_write_time(p, err);
            if (err)
            {
                ctx.RuntimeError(Error::Code::OSError, err.message());
                return 0;
            }
            std::time_t cftime = std::chrono::system_clock::to_time_t(
                std::chrono::file_clock::to_sys(time));

            return static_cast<Integer>(cftime);
        });
    module->AddFunction<std::string, const char*>("expand_path", &ExpandPath);
    module->AddFunction<std::string, const std::string&>("extract_filename", &ExtractFileName);
    module->AddFunction<std::string, const std::string&>("extract_fileext", &ExtractFileExt);
    module->AddFunction<std::string, const std::string&, const std::string&>("change_fileext", &ChangeFileExt);
    module->AddFunction<std::string, const std::string&>("parent_path", &ParentPath);
    module->AddFunction<std::string, const std::string&>("add_slash", &AddSlash);
    module->AddFunction<std::string, const std::string&>("remove_slash", &RemoveSlash);
    module->AddFunction<std::string, const std::string&, const std::string&>("concat_path", &ConcatPath);
    module->AddFunction<void, Integer>(
        "sleep", [](Integer milli) -> void { std::this_thread::sleep_for(std::chrono::milliseconds(milli)); });
    module->AddFunction<std::string>("temp_path",
        []() -> std::string
        {
            namespace fs = std::filesystem;
            return fs::temp_directory_path();
        });
    module->AddFunction<void, int>("exit", &exit, false);
    module->AddFunction<Value, std::string>("exec",
        [&ctx](const std::string& cmd) -> Value
        {
            std::string result;
            std::array<char, 128> buffer{};
            FILE* pipe = popen(cmd.c_str(), "r");
            if (!pipe)
            {
                ctx.RuntimeError(Error::Code::OSError, "Can not start command");
                return nullptr;
            }
            while (fgets(buffer.data(), 128, pipe) != nullptr)
            {
                if (ctx.stop_)
                    break;
                std::string out(buffer.data());
                result.append(out);
            }
            return MakeString(result);
        }, false);
    module->AddFunction<std::string, std::string>("escape_argument",
        [](const std::string& value) -> std::string
        {
            std::string result = value;
            ReplaceSubstring<char>(result, "\"", "\\\"");
            if (result.find_first_of(" \t\"") != std::string::npos)
                result = "\"" + result + "\"";
            return result;
        });
    module->AddFunction<std::string, int, const char*>("setlocale", &std::setlocale);
    module->AddFunction<std::string, int>("getlocale",
        [](int cat) { return std::setlocale(cat, nullptr); });

    ctx.AddConst("os", std::move(module), SCOPE_LIBRARY);

    AddPosixLib(ctx);
    return true;
}
