/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "os_error.h"
#include <cstring>

void RaiseOSError(sa::tl::Context& ctx)
{
    ctx.RuntimeError(sa::tl::Error::Code::OSError, std::strerror(errno));
}
