/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <metatable.h>
#include <module.h>
#include <context.h>
#include "compress.h"

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("lz4") || ctx.HasValue("zlib") || ctx.HasValue("zstd"))
        return true;

#ifdef SA_LZ4_SUPPORT
    auto lz4 = sa::tl::MakeModule();

    lz4->AddFunction<std::string, std::string>(
        "compress",
        [&ctx](const std::string& in) -> std::string
        {
            sa::lz4_compress compress;
            std::string out;
            size_t outSize = in.length();
            out.resize(outSize);
            bool res = compress(in.data(), in.length(), out.data(), outSize);
            if (!res)
            {
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Error compressing data");
                return "";
            }
            out.resize(outSize);
            return out;
        });
    lz4->AddFunction<std::string, std::string>(
        "decompress",
        [&ctx](const std::string& in) -> std::string
        {
            sa::lz4_decompress decompress;
            std::string out;
            size_t outSize = in.length() * 2;
            out.resize(outSize);
            bool res = decompress(in.data(), in.length(), out.data(), outSize);
            if (!res)
            {
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Error decompressing data");
                return "";
            }
            out.resize(outSize);
            return out;
        });

    ctx.AddConst("lz4", std::move(lz4), sa::tl::SCOPE_LIBRARY);
#endif

#ifdef SA_ZLIB_SUPPORT
    auto zlib = sa::tl::MakeModule();

    zlib->AddFunction<std::string, std::string, bool>(
        "compress",
        [&ctx](const std::string& in, bool gzip) -> std::string
        {
            sa::zlib_compress compress(gzip);
            std::string out;
            size_t outSize = in.length();
            out.resize(outSize);
            bool res = compress(in.data(), in.length(), out.data(), outSize);
            if (!res)
            {
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Error compressing data");
                return "";
            }
            out.resize(outSize);
            return out;
        });
    zlib->AddFunction<std::string, std::string, bool>(
        "decompress",
        [&ctx](const std::string& in, bool gzip) -> std::string
        {
            sa::zlib_decompress decompress(gzip);
            std::string out;
            size_t outSize = in.length() * 2;
            out.resize(outSize);
            bool res = decompress(in.data(), in.length(), out.data(), outSize);
            if (!res)
            {
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Error decompressing data");
                return "";
            }
            out.resize(outSize);
            return out;
        });
    zlib->AddFunction<sa::tl::Integer, std::string>(
        "crc32",
        [](const std::string& in) -> sa::tl::Integer
        {
            return crc32(0L, (const unsigned char*)in.data(), in.length());
        });

    ctx.AddConst("zlib", std::move(zlib), sa::tl::SCOPE_LIBRARY);
#endif

#ifdef SA_ZSTD_SUPPORT
    auto zstd = sa::tl::MakeModule();

    zstd->AddFunction<std::string, std::string>(
        "compress",
        [&ctx](const std::string& in) -> std::string
        {
            sa::zstd_compress compress;
            std::string out;
            size_t outSize = ZSTD_compressBound(in.length());
            out.resize(outSize);
            bool res = compress(in.data(), in.length(), out.data(), outSize);
            if (!res)
            {
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Error compressing data");
                return "";
            }
            out.resize(outSize);
            return out;
        });
    zstd->AddFunction<std::string, std::string>(
        "decompress",
        [&ctx](const std::string& in) -> std::string
        {
            sa::zstd_decompress decompress;
            std::string out;
            size_t outSize = ZSTD_getFrameContentSize(in.data(), in.length());
            out.resize(outSize);
            bool res = decompress(in.data(), in.length(), out.data(), outSize);
            if (!res)
            {
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Error decompressing data");
                return "";
            }
            out.resize(outSize);
            return out;
        });

    ctx.AddConst("zstd", std::move(zstd), sa::tl::SCOPE_LIBRARY);
#endif

    return true;
}
