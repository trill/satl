#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <compress.h>
#include <string>

#ifdef SA_ZLIB_SUPPORT
TEST_CASE("sa::compress zlib")
{
    sa::zlib_compress compress(false);
    std::string input = "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic";
    std::string output;
    output.resize(input.length());
    size_t outputSize = output.length();
    compress(input.c_str(), input.length(), output.data(), outputSize);

    sa::zlib_decompress decompress(false);
    std::string decompressed;
    decompressed.resize(outputSize * 2);
    size_t decompressedSize = decompressed.length();
    decompress(output.c_str(), outputSize, decompressed.data(), decompressedSize);
    decompressed.resize(decompressedSize);
    REQUIRE(input == decompressed);
    float ratio = (float)outputSize / (float)input.length();
    (void)ratio;
}

TEST_CASE("sa::compress zlib gzip")
{
    sa::zlib_compress compress;
    std::string input = "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic";
    std::string output;
    output.resize(input.length());
    size_t outputSize = output.length();
    compress(input.c_str(), input.length(), output.data(), outputSize);

    sa::zlib_decompress decompress;
    std::string decompressed;
    decompressed.resize(outputSize * 2);
    size_t decompressedSize = decompressed.length();
    decompress(output.c_str(), outputSize, decompressed.data(), decompressedSize);
    decompressed.resize(decompressedSize);
    REQUIRE(input == decompressed);
    float ratio = (float)outputSize / (float)input.length();
    (void)ratio;
}

TEST_CASE("sa::compress zlib crc32")
{
    std::string input = "This is only a test";
    auto sum = crc32(0L, (const unsigned char*)input.data(), input.length());
    REQUIRE(sum == 0xfda9e1cf);
}
#endif

#ifdef SA_LZ4_SUPPORT
TEST_CASE("sa::compress lz4")
{
    sa::lz4_compress compress;
    std::string input = "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic";
    std::string output;
    output.resize(input.length());
    size_t outputSize = output.length();
    compress(input.data(), input.length(), output.data(), outputSize);

    sa::lz4_decompress decompress;
    std::string decompressed;
    decompressed.resize(outputSize * 2);
    size_t decompressedSize = decompressed.length();
    decompress(output.data(), outputSize, decompressed.data(), decompressedSize);
    decompressed.resize(decompressedSize);
    REQUIRE(input == decompressed);
    float ratio = (float)outputSize / (float)input.length();
    (void)ratio;
}
#endif

#ifdef SA_ZSTD_SUPPORT
TEST_CASE("sa::compress zstd")
{
    sa::zstd_compress compress;
    std::string input = "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic";
    std::string output;
    size_t outSize = ZSTD_compressBound(input.length());
    output.resize(outSize);
    compress(input.data(), input.length(), output.data(), outSize);
    output.resize(outSize);

    sa::zstd_decompress decompress;
    std::string decompressed;
    size_t outSizeD = ZSTD_getFrameContentSize(output.data(), output.length());
    decompressed.resize(outSizeD);
    decompress(output.data(), outSize, decompressed.data(), outSizeD);
    decompressed.resize(outSizeD);
    REQUIRE(input == decompressed);
}
#endif
