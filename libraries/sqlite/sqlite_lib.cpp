/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <metatable.h>
#include <module.h>
#include <node.h>
#include <context.h>
#include "connection.h"

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("sqlite"))
        return true;
    
    sa::tl::MetaTable& connMeta = sa::tl::bind::Register<sqlite::Connection>(ctx);

    auto module = sa::tl::MakeModule();
    module->AddFunction<sa::tl::Value, const std::string&>(
        "connect",
        [&ctx, &connMeta](const std::string& filename) -> sa::tl::Value
        {
            auto* connection = ctx.GetGC().CreateObject<sqlite::Connection>(ctx, filename);
            if (connection->Connect())
                return sa::tl::MakeObject(connMeta, connection);
            ctx.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Failed to open file {}.", filename));
            ctx.GetGC().Remove(connection);
            return {};
        });
    module->AddFunction<void, sa::tl::ObjectPtr>("close",
        [&ctx, &connMeta](const sa::tl::ObjectPtr& connObj) -> void
        {
            if (auto* conn = connMeta.CastTo<sqlite::Connection>(connObj.get()))
            {
                ctx.GetGC().Remove(conn);
                return;
            }

            ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not an instance of Connection");
        });

    ctx.AddConst("sqlite", std::move(module), sa::tl::SCOPE_LIBRARY);

    return true;
}
