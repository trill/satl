/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sqlite3.h>
#include <string>
#include <value.h>
#include <context.h>
#include <node.h>
#include <non_copyable.h>
#include <object.h>

namespace sqlite {

class Result;

class Connection
{
    SATL_NON_COPYABLE(Connection);
    SATL_NON_MOVEABLE(Connection);
public:
    Connection(sa::tl::Context& ctx, std::string filename);
    ~Connection();
    bool Connect();
    std::string EscapeString(const std::string& s);
    sa::tl::Integer GetLastInsertId();
    void ExecuteQuery(const std::string& query);
    sa::tl::TablePtr StoreQuery(const std::string& query);
    void StoreQueryCallback(const std::string& query, const sa::tl::Callable& callback);
    std::string GetFilename() const { return filename_; }
private:
    void InternalQuery(const std::string& query);
    std::shared_ptr<Result> InternalSelectQuery(const std::string& query);
    std::shared_ptr<Result> VerifyResult(std::shared_ptr<Result> result);
    void FreeResult(Result* res);
    sa::tl::Context& ctx_;
    sqlite3* handle_{ nullptr };
    std::string filename_;
    bool connected_{ false };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<sqlite::Connection>(sa::tl::Context& ctx);
}
