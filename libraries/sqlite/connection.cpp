/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "connection.h"
#include <ast.h>
#include <scope_guard.h>
#include "result.h"

namespace sqlite {

Connection::Connection(sa::tl::Context& ctx,
    std::string filename) :
    ctx_(ctx),
    filename_(std::move(filename))
{
}

Connection::~Connection()
{
    if (handle_)
        sqlite3_close(handle_);
}

bool Connection::Connect()
{
    if (sqlite3_open(filename_.c_str(), &handle_) != SQLITE_OK)
        return false;
    connected_ = true;
    return true;
}

std::string Connection::EscapeString(const std::string& s)
{
    if (s.empty())
        return "''";

    char* output = new char[s.length() * 2 + 3];
    sqlite3_snprintf((int)s.length() * 2 + 3, output, "%Q", s.c_str());
    std::string r(output);
    delete[] output;
    return r;
}

sa::tl::Integer Connection::GetLastInsertId()
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No file opened");
        return 0;
    }

    static const std::string query = "SELECT last_insert_rowid()";
    sqlite3_stmt* stmt;
    sa::tl::ScopeGuard deleteGguard([&stmt]() { sqlite3_finalize(stmt); });
    if (sqlite3_prepare_v2(handle_, query.c_str(), (int)query.length(), &stmt, nullptr) != SQLITE_OK)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, sqlite3_errmsg(handle_));
        return 0;
    }

    int step = sqlite3_step(stmt);
    if (step != SQLITE_ROW)
        return 0;

    return static_cast<sa::tl::Integer>(sqlite3_column_int64(stmt, 0));
}

std::shared_ptr<Result> Connection::VerifyResult(std::shared_ptr<Result> result)
{
    if (!result->Next())
        return {};
    return result;
}

void Connection::FreeResult(Result* res)
{
    delete res;
}

void Connection::ExecuteQuery(const std::string& query)
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No file opened");
        return;
    }
    InternalQuery(query);
}

void Connection::InternalQuery(const std::string& query)
{
    sqlite3_stmt* stmt;
    sa::tl::ScopeGuard deleteGguard([&stmt]() { sqlite3_finalize(stmt); });
    if (sqlite3_prepare_v2(handle_, query.c_str(), (int)query.length(), &stmt, nullptr) != SQLITE_OK)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, sqlite3_errmsg(handle_));
        return;
    }

    int ret = sqlite3_step(stmt);
    if (ret != SQLITE_OK && ret != SQLITE_DONE && ret != SQLITE_ROW)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, sqlite3_errmsg(handle_));
        return;
    }
}

sa::tl::TablePtr Connection::StoreQuery(const std::string& query)
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No file opened");
        return {};
    }
    auto result = sa::tl::MakeTable();
    sa::tl::Integer rows = 0;
    for (auto res = InternalSelectQuery(query); res; res = res->Next())
    {
        auto row = sa::tl::MakeTable();
        for (int32_t column = 0; column < res->GetColumns(); ++column)
        {
            const auto columnName = res->GetColumnName(column);
            row->Add(columnName, res->GetValue(column));
        }
        result->Add(rows, sa::tl::MakeValue(row));
        ++rows;
    }
    return result;
}

void Connection::StoreQueryCallback(const std::string& query, const sa::tl::Callable& callback)
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No file opened");
        return;
    }
    if (callback.ParamCount() != 1)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return;
    }

    for (auto res = InternalSelectQuery(query); res; res = res->Next())
    {
        if (ctx_.stop_)
            break;
        auto row = sa::tl::MakeTable();
        for (int32_t column = 0; column < res->GetColumns(); ++column)
        {
            const auto columnName = res->GetColumnName(column);
            row->Add(columnName, res->GetValue(column));
        }
        auto ret = callback(ctx_, { MakeValue(row) });
        if (!ret->ToBool())
            break;
    }
}

std::shared_ptr<Result> Connection::InternalSelectQuery(const std::string& query)
{
    if (!connected_)
        return {};

    sqlite3_stmt* stmt;
    if (sqlite3_prepare_v2(handle_, query.c_str(), (int)query.length(), &stmt, nullptr) != SQLITE_OK)
    {
        sqlite3_finalize(stmt);
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, sqlite3_errmsg(handle_));
        return {};
    }

    std::shared_ptr<Result> results(new Result(stmt),
        std::bind(&Connection::FreeResult, this, std::placeholders::_1));
    return VerifyResult(results);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<sqlite::Connection>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<sqlite::Connection>();
    result
        .AddFunction<sqlite::Connection, std::string, const std::string&>("escape", &sqlite::Connection::EscapeString)
        .AddFunction<sqlite::Connection, sa::tl::Integer>("last_id", &sqlite::Connection::GetLastInsertId)
        .AddFunction<sqlite::Connection, void, const std::string&>("execute", &sqlite::Connection::ExecuteQuery)
        .AddFunction<sqlite::Connection, sa::tl::TablePtr, const std::string&>("query", &sqlite::Connection::StoreQuery)
        .AddFunction<sqlite::Connection, void, const std::string&, const sa::tl::Callable&>(
            "query_callback", &sqlite::Connection::StoreQueryCallback)

        .AddProperty("filename", &sqlite::Connection::GetFilename);
    return result;
}

}
