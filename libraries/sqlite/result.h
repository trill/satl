/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sqlite3.h>
#include "connection.h"
#include <memory>
#include <value.h>

namespace sqlite {

class Result : public std::enable_shared_from_this<Result>
{
    friend class Connection;
public:
    ~Result();
    std::shared_ptr<Result> Next();
    int32_t GetColumns() const { return columns_; }
    std::string GetColumnName(int32_t column);
    int64_t GetInt(const sa::tl::Value& column);
    double GetFloat(const sa::tl::Value& column);
    std::string GetString(const sa::tl::Value& column);
    sa::tl::ValuePtr GetValue(const sa::tl::Value& column);
protected:
    explicit Result(sqlite3_stmt* res);
private:
    int GetColumnType(const sa::tl::Value& column);
    int GetColumnIndex(const sa::tl::Value& value);
    using ListNames = std::map<std::string, uint32_t>;
    ListNames listNames_;
    sqlite3_stmt* handle_;
    bool rowAvailable_;
    int32_t columns_;
};

}
