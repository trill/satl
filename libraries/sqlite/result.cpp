/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "result.h"

namespace sqlite {

Result::Result(sqlite3_stmt* res) :
    handle_(res),
    rowAvailable_(false)
{
    listNames_.clear();
    columns_ = sqlite3_column_count(handle_);
    for (int32_t i = 0; i < columns_; i++)
    {
        listNames_[sqlite3_column_name(handle_, i)] = i;
    }
}

Result::~Result()
{
    sqlite3_finalize(handle_);
}

std::shared_ptr<Result> Result::Next()
{
    bool rowAvail = (sqlite3_step(handle_) == SQLITE_ROW);
    return rowAvail ? shared_from_this() : std::shared_ptr<Result>();
}

int64_t Result::GetInt(const sa::tl::Value& column)
{
    return sqlite3_column_int64(handle_, GetColumnIndex(column));
}

double Result::GetFloat(const sa::tl::Value& column)
{
    return sqlite3_column_double(handle_, GetColumnIndex(column));
}

std::string Result::GetString(const sa::tl::Value& column)
{
    auto index = GetColumnIndex(column);
    if (index == -1)
        return {};
    const auto* text = sqlite3_column_text(handle_, index);
    auto bytes = sqlite3_column_bytes(handle_,  index);
    return { (const char*)text, (size_t)bytes };
}

int Result::GetColumnIndex(const sa::tl::Value& value)
{
    if (value.IsInt())
        return value.ToInt();
    if (value.IsString())
    {
        ListNames::iterator it = listNames_.find(value.ToString());
        if (it != listNames_.end())
            return it->second;
    }
    return -1;
}

std::string Result::GetColumnName(int32_t column)
{
    return sqlite3_column_name(handle_, column);
}

int Result::GetColumnType(const sa::tl::Value& column)
{
    return sqlite3_column_type(handle_, GetColumnIndex(column));
}

sa::tl::ValuePtr Result::GetValue(const sa::tl::Value& column)
{
    switch (GetColumnType(column))
    {
    case SQLITE_NULL:
        return sa::tl::MakeNull();
    case SQLITE_INTEGER:
        return sa::tl::MakeValue(GetInt(column));
    case SQLITE_FLOAT:
        return sa::tl::MakeValue((sa::tl::Float)GetFloat(column));
    default:
        return sa::tl::MakeValue(GetString(column));
    }
}

}
