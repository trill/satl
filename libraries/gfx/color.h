/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <context.h>
#include <limits>
#include <value.h>
#include <object.h>

namespace gfx {

template<typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs && lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

constexpr float Hue2Rgb(float p, float q, float t)
{
    if (t < 0.0f) t += 1.0f;
    if (t > 1.0f) t -= 1.0f;
    if (t < 1.0f / 6.0f)
        return p + (q - p) * 6.0f * t;
    if (t < 1.0f / 2.0f)
        return q;
    if (t < 2.0f / 3.0f)
        return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
    return p;
}

constexpr std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> RgbToColor(float r, float g, float b, float a = 1.0f)
{
    return { static_cast<uint8_t>(std::clamp(r, 0.0f, 1.0f) * 255.0f),
        static_cast<uint8_t>(std::clamp(g, 0.0f, 1.0f) * 255.0f),
        static_cast<uint8_t>(std::clamp(b, 0.0f, 1.0f) * 255.0f),
        static_cast<uint8_t>(std::clamp(a, 0.0f, 1.0f) * 255.0f) };
}

constexpr std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> HslToColor(float h, float s, float l)
{
    float r = 0.0f, g = 0.0f, b = 0.0f;
    if (Equals(s, 0.0f))
        r = g = b = 1;
    else
    {
        const float q = l < 0.5f ? l * (1.0f + s) : l + s - l * s;
        const float p = 2.0f * l - q;
        r = Hue2Rgb(p, q, h + 1.0f / 3.0f);
        g = Hue2Rgb(p, q, h);
        b = Hue2Rgb(p, q, h - 1.0f / 3.0f);
    }

    return RgbToColor(r, g, b);
}

constexpr std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> HsvToColor(float h, float s, float v)
{
    float r = 0.0f, g = 0.0f, b = 0.0f;
    int i = static_cast<int>(h * 6.0f);
    const float f = h * 6.0f - (float)i;
    const float p = v * (1.0f - s);
    const float q = v * (1.0f - f * s);
    const float t = v * (1.0f - (1.0f - f) * s);


    switch (i % 6)
    {
    case 0: r = v, g = t, b = p; break;
    case 1: r = q, g = v, b = p; break;
    case 2: r = p, g = v, b = t; break;
    case 3: r = p, g = q, b = v; break;
    case 4: r = t, g = p, b = v; break;
    case 5: r = v, g = p, b = q; break;
    }
    return RgbToColor(r, g, b);
}

class Color final : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Color);
    SATL_NON_MOVEABLE(Color);
    TYPED_OBJECT(Color);
public:
    explicit Color(sa::tl::Context& ctx);
    Color(sa::tl::Context& ctx, sa::tl::Integer r, sa::tl::Integer g, sa::tl::Integer b);
    Color(sa::tl::Context& ctx, sa::tl::Integer r, sa::tl::Integer g, sa::tl::Integer b, sa::tl::Integer a);
    Color(sa::tl::Context& ctx, const sa::tl::Table& color);
    Color(sa::tl::Context& ctx, const sa::tl::Value& color);
    Color(sa::tl::Context& ctx, const std::string& value);
    ~Color() override;

    [[nodiscard]] sa::tl::Integer GetR() const { return r_; }
    [[nodiscard]] sa::tl::Integer GetG() const { return g_; }
    [[nodiscard]] sa::tl::Integer GetB() const { return b_; }
    [[nodiscard]] sa::tl::Integer GetA() const { return a_; }
    void SetR(sa::tl::Integer value) { r_ = (uint8_t)std::clamp<sa::tl::Integer>(value, 0, 255); }
    void SetG(sa::tl::Integer value) { g_ = (uint8_t)std::clamp<sa::tl::Integer>(value, 0, 255); }
    void SetB(sa::tl::Integer value) { b_ = (uint8_t)std::clamp<sa::tl::Integer>(value, 0, 255); }
    void SetA(sa::tl::Integer value) { a_ = (uint8_t)std::clamp<sa::tl::Integer>(value, 0, 255); }
    [[nodiscard]] uint32_t To32() const;
    [[nodiscard]] uint32_t To24() const;
    [[nodiscard]] std::string ToString() const;
    [[nodiscard]] sa::tl::TablePtr ToTable() const;
    [[nodiscard]] std::tuple<float, float, float> ToRgb() const
    {
        return { (float)r_ / 255.0f, (float)g_ / 255.0f, (float)b_ / 255.0f };
    }
    [[nodiscard]] std::tuple<float, float, float, float> ToRgba() const
    {
        return { (float)r_ / 255.0f, (float)g_ / 255.0f, (float)b_ / 255.0f, (float)a_ / 255.0f };
    }
    // Returns h, s, l in range 0..1
    [[nodiscard]] std::tuple<float, float, float> ToHsl() const
    {
        const float r = (float)r_ / 255.0f;
        const float g = (float)g_ / 255.0f;
        const float b = (float)b_ / 255.0f;

        const float min = std::min(r, std::min(g, b));
        const float max = std::max(r, std::max(g, b));
        float h = 0.0f, s = 0.0f, l = (max + min) * 0.5f;
        if (Equals(max, min))
            h = s = 0;
        else
        {
            const float d = max - min;
            s = l > 0.5f ? d / (2.0f - max - min) : d / (max - min);
            if (Equals(max, r))
                h = (g - b) / d + (g < b ? 6.0f : 0.0f);
            else if (Equals(max, g))
                h = (b - r) / d + 2.0f;
            else if (Equals(max, b))
                h = (r - g) / d + 4.0f;

            h /= 6;
        }
        return { h, s, l };
    }
    [[nodiscard]] sa::tl::TablePtr ToHslScript() const;
    // Returns h, s, v in range 0..1
    [[nodiscard]] std::tuple<float, float, float> ToHsv() const
    {
        const float r = (float)r_ / 255.0f;
        const float g = (float)g_ / 255.0f;
        const float b = (float)b_ / 255.0f;

        const float min = std::min(r, std::min(g, b));
        const float max = std::max(r, std::max(g, b));
        float h = 0.0f, s = 0.0f, v = max;
        const float d = max - min;
        s = Equals(max, 0.0f) ? 0.0f : d / max;
        if (Equals(max, min))
            h = 0;
        else
        {
            if (Equals(max, r))
                h = (g - b) / d + (g < b ? 6.0f : 0.0f);
            else if (Equals(max, g))
                h = (b - r) / d + 2.0f;
            else if (Equals(max, b))
                h = (r - g) / d + 4.0f;

            h /= 6;
        }
        return { h, s, v };
    }
    [[nodiscard]] sa::tl::TablePtr ToHsvScript() const;
    [[nodiscard]] float GetHue() const;
    [[nodiscard]] float GetSaturation() const;
    [[nodiscard]] float GetLightness() const;
    [[nodiscard]] float GetValue() const;
    void SetHue(float value);
    void SetSaturation(float value);
    void SetLightness(float value);
    void SetValue(float value);

    constexpr bool operator==(const Color& rhs) const { return r_ == rhs.r_ && g_ == rhs.g_ && b_ == rhs.b_ && a_ == rhs.a_; }
    constexpr bool operator!=(const Color& rhs) const { return r_ != rhs.r_ || g_ != rhs.g_ || b_ != rhs.b_ || a_ != rhs.a_; }

    [[nodiscard]] sa::tl::Value OpEq(const sa::tl::Value& rhs) const;
    [[nodiscard]] sa::tl::Value OpIneq(const sa::tl::Value& rhs) const;
    [[nodiscard]] Color* OpAdd(const sa::tl::Value& rhs) const;
    [[nodiscard]] Color* OpCompoundAdd(const sa::tl::Value& rhs);
    [[nodiscard]] Color* OpSub(const sa::tl::Value& rhs) const;
    [[nodiscard]] Color* OpCompoundSub(const sa::tl::Value& rhs);
    [[nodiscard]] Color* OpMul(const sa::tl::Value& rhs) const;
    [[nodiscard]] Color* OpCompoundMul(const sa::tl::Value& rhs);
    [[nodiscard]] Color* OpDiv(const sa::tl::Value& rhs) const;
    [[nodiscard]] Color* OpCompoundDiv(const sa::tl::Value& rhs);

    void AlphaBlend(const Color& rhs, bool keepAlpha);
    void AlphaBlend(const sa::tl::Value& rhs, bool keepAlpha);
    void GreyScale();
    void BlackWhite();
    void Invert(bool invertAlpha);
    void Lerp(const Color& rhs, float t, bool lerpAlpha);
    void Lerp(const sa::tl::Value& rhs, float t, bool lerpAlpha);
    void Tint(float factor);
    void Shade(float factor);

    uint8_t r_{ 0 };
    uint8_t g_{ 0 };
    uint8_t b_{ 0 };
    uint8_t a_{ 255 };
};

} // namespace gfx

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<gfx::Color>(sa::tl::Context& ctx);
}
