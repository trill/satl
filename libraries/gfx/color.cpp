/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "color.h"
#include <typename.h>
#include <charconv>
#include <cmath>
#include <iomanip>
#include <sstream>

namespace gfx {

Color::Color(sa::tl::Context& ctx) :
    Object(ctx)
{
}

Color::Color(sa::tl::Context& ctx, sa::tl::Integer r, sa::tl::Integer g, sa::tl::Integer b) :
    Object(ctx),
    r_((uint8_t)std::clamp<sa::tl::Integer>(r, 0, 255)),
    g_((uint8_t)std::clamp<sa::tl::Integer>(g, 0, 255)),
    b_((uint8_t)std::clamp<sa::tl::Integer>(b, 0, 255))
{
}

Color::Color(sa::tl::Context& ctx, sa::tl::Integer r, sa::tl::Integer g, sa::tl::Integer b, sa::tl::Integer a) :
    Object(ctx),
    r_((uint8_t)std::clamp<sa::tl::Integer>(r, 0, 255)),
    g_((uint8_t)std::clamp<sa::tl::Integer>(g, 0, 255)),
    b_((uint8_t)std::clamp<sa::tl::Integer>(b, 0, 255)),
    a_((uint8_t)std::clamp<sa::tl::Integer>(a, 0, 255))
{
}

Color::Color(sa::tl::Context& ctx, const sa::tl::Table& color) :
    Object(ctx)
{
    sa::tl::GetTableValue(ctx_, color, "r", r_);
    sa::tl::GetTableValue(ctx_, color, "g", g_);
    sa::tl::GetTableValue(ctx_, color, "b", b_);
    sa::tl::GetTableOptionalValue(ctx_, color, "a", a_);
}

Color::Color(sa::tl::Context& ctx, const sa::tl::Value& color) :
    Object(ctx)
{
    if (color.IsTable())
    {
        const auto& table = color.As<sa::tl::TablePtr>();

        sa::tl::GetTableValue(ctx_, *table, "r", r_);
        sa::tl::GetTableValue(ctx_, *table, "g", g_);
        sa::tl::GetTableValue(ctx_, *table, "b", b_);
        sa::tl::GetTableOptionalValue(ctx_, *table, "a", a_);
        return;
    }

    if (color.IsObject())
    {
        const auto& object = color.As<sa::tl::ObjectPtr>();
        const auto& colorMeta = object->GetMetatable();
        if (!colorMeta.InstanceOf<Color>())
        {
            ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Expecting a Color object");
            return;
        }
        auto* colorObj = colorMeta.CastTo<Color>(object.get());
        r_ = colorObj->r_;
        g_ = colorObj->g_;
        b_ = colorObj->b_;
        a_ = colorObj->a_;
        return;
    }

    if (color.IsString())
    {
        const auto& value = *color.As<sa::tl::StringPtr>();
        size_t start = value.front() == '#' ? 1 : 0;
        if ((value.length() - start) % 2 != 0)
            return;

        const size_t count = (value.length() - start) / 2;
        std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, r_, 16);
        if (count > 1)
        {
            ++start;
            std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, g_, 16);
        }
        if (count > 2)
        {
            ++start;
            std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, b_, 16);
        }
        if (count > 3)
        {
            ++start;
            std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, a_, 16);
        }
        return;
    }
    if (color.IsInt())
    {
        auto intcol = color.ToUint();
        uint8_t a = (intcol >> 24u) & 0xffu;
        uint8_t r = (intcol >> 16u) & 0xffu;
        uint8_t g = (intcol >> 8u) & 0xffu;
        uint8_t b = (intcol >> 0u) & 0xffu;
        r_ = std::clamp<uint8_t>(r, 0, 255);
        g_ = std::clamp<uint8_t>(g, 0, 255);
        b_ = std::clamp<uint8_t>(b, 0, 255);
        if (a != 0)
            a_ = a;
        return;
    }

    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Expecting a Color object, table, string or int");
}

Color::Color(sa::tl::Context& ctx, const std::string& value) :
    Object(ctx)
{
    size_t start = value.front() == '#' ? 1 : 0;
    if ((value.length() - start) % 2 != 0)
        return;

    const size_t count = (value.length() - start) / 2;
    std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, r_, 16);
    if (count > 1)
    {
        ++start;
        std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, g_, 16);
    }
    if (count > 2)
    {
        ++start;
        std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, b_, 16);
    }
    if (count > 3)
    {
        ++start;
        std::from_chars(value.data() + (start * 2), value.data() + (start * 2) + 2, a_, 16);
    }
}

Color::~Color() = default;

uint32_t Color::To32() const
{
    return (a_ << 24u) | (r_ << 16u) | (g_ << 8u) | b_;
}

uint32_t Color::To24() const
{
    return (r_ << 16u) | (g_ << 8u) | b_;
}

std::string Color::ToString() const
{
    std::stringstream ss;
    ss << std::hex;
    ss << std::setw(2) << std::setfill('0') << (int)r_ << std::setw(2) << std::setfill('0') << (int)g_ << std::setw(2)
       << std::setfill('0') << (int)b_ << std::setw(2) << std::setfill('0') << (int)a_;
    return ss.str();
}

sa::tl::TablePtr Color::ToTable() const
{
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Integer>("r", r_);
    result->Add<sa::tl::Integer>("g", g_);
    result->Add<sa::tl::Integer>("b", b_);
    result->Add<sa::tl::Integer>("a", a_);

    return result;
}

sa::tl::TablePtr Color::ToHslScript() const
{
    auto [h, s, l] = ToHsl();
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("h", h);
    result->Add<sa::tl::Float>("s", s);
    result->Add<sa::tl::Float>("l", l);

    return result;
}

sa::tl::TablePtr Color::ToHsvScript() const
{
    auto [h, s, v] = ToHsv();
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("h", h);
    result->Add<sa::tl::Float>("s", s);
    result->Add<sa::tl::Float>("v", v);

    return result;
}

float Color::GetHue() const
{
    auto [h, s, l] = ToHsl();
    return h;
}

float Color::GetSaturation() const
{
    auto [h, s, l] = ToHsl();
    return s;
}

float Color::GetLightness() const
{
    auto [h, s, l] = ToHsl();
    return l;
}

float Color::GetValue() const
{
    auto [h, s, v] = ToHsv();
    return v;
}

void Color::SetHue(float value)
{
    auto [h, s, l] = ToHsl();
    h = value;
    auto [r, g, b, a] = HslToColor(h, s, l);
    r_ = r;
    g_ = g;
    b_ = b;
}

void Color::SetSaturation(float value)
{
    auto [h, s, l] = ToHsl();
    s = value;
    auto [r, g, b, a] = HslToColor(h, s, l);
    r_ = r;
    g_ = g;
    b_ = b;
}

void Color::SetLightness(float value)
{
    auto [h, s, l] = ToHsl();
    l = value;
    auto [r, g, b, a] = HslToColor(h, s, l);
    r_ = r;
    g_ = g;
    b_ = b;
}

void Color::SetValue(float value)
{
    auto [h, s, v] = ToHsv();
    v = value;
    auto [r, g, b, a] = HsvToColor(h, s, v);
    r_ = r;
    g_ = g;
    b_ = b;
}

sa::tl::Value Color::OpEq(const sa::tl::Value& rhs) const
{
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (auto* obj = ro->GetMetatable().CastTo<Color>(ro.get()))
            return { *this == *obj };
    }

    return false;
}

sa::tl::Value Color::OpIneq(const sa::tl::Value& rhs) const
{
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (auto* obj = ro->GetMetatable().CastTo<Color>(ro.get()))
            return { *this != *obj };
    }

    return true;
}

Color* Color::OpAdd(const sa::tl::Value& rhs) const
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Adding scalar value
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        result->r_ = std::clamp<sa::tl::Integer>(r_ + rhs.ToInt(), 0, 255);
        result->g_ = std::clamp<sa::tl::Integer>(g_ + rhs.ToInt(), 0, 255);
        result->b_ = std::clamp<sa::tl::Integer>(b_ + rhs.ToInt(), 0, 255);
        return result;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
            return nullptr;
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        result->r_ = std::clamp(r_ + obj->r_, 0, 255);
        result->g_ = std::clamp(g_ + obj->g_, 0, 255);
        result->b_ = std::clamp(b_ + obj->b_, 0, 255);
        result->a_ = std::clamp(a_ + obj->a_, 0, 255);
        return result;
    }
    return nullptr;
}

Color* Color::OpCompoundAdd(const sa::tl::Value& rhs)
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        r_ = std::clamp<sa::tl::Integer>(r_ + rhs.ToInt(), 0, 255);
        g_ = std::clamp<sa::tl::Integer>(g_ + rhs.ToInt(), 0, 255);
        b_ = std::clamp<sa::tl::Integer>(b_ + rhs.ToInt(), 0, 255);
        return this;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
        {
            ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch,
                std::format("Can not add a {} to a {}", ro->GetClassName(), sa::tl::TypeName<Color>::Get()));
            return nullptr;
        }
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        r_ = std::clamp(r_ + obj->r_, 0, 255);
        g_ = std::clamp(g_ + obj->g_, 0, 255);
        b_ = std::clamp(b_ + obj->b_, 0, 255);
        a_ = std::clamp(a_ + obj->a_, 0, 255);
        return this;
    }
    return this;
}

Color* Color::OpSub(const sa::tl::Value& rhs) const
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        result->r_ = std::clamp<sa::tl::Integer>(r_ - rhs.ToInt(), 0, 255);
        result->g_ = std::clamp<sa::tl::Integer>(g_ - rhs.ToInt(), 0, 255);
        result->b_ = std::clamp<sa::tl::Integer>(b_ - rhs.ToInt(), 0, 255);
        return result;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
            return nullptr;
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        result->r_ = std::clamp(r_ - obj->r_, 0, 255);
        result->g_ = std::clamp(g_ - obj->g_, 0, 255);
        result->b_ = std::clamp(b_ - obj->b_, 0, 255);
        result->a_ = std::clamp(a_ - obj->a_, 0, 255);
        return result;
    }
    return nullptr;
}

Color* Color::OpCompoundSub(const sa::tl::Value& rhs)
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        r_ = std::clamp<sa::tl::Integer>(r_ - rhs.ToInt(), 0, 255);
        g_ = std::clamp<sa::tl::Integer>(g_ - rhs.ToInt(), 0, 255);
        b_ = std::clamp<sa::tl::Integer>(b_ - rhs.ToInt(), 0, 255);
        return this;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
        {
            ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch,
                std::format("Can not add a {} to a {}", ro->GetClassName(), sa::tl::TypeName<Color>::Get()));
            return nullptr;
        }
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        r_ = std::clamp(r_ - obj->r_, 0, 255);
        g_ = std::clamp(g_ - obj->g_, 0, 255);
        b_ = std::clamp(b_ - obj->b_, 0, 255);
        a_ = std::clamp(a_ - obj->a_, 0, 255);
        return this;
    }
    return this;
}

Color* Color::OpMul(const sa::tl::Value& rhs) const
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        result->r_ = std::clamp<sa::tl::Integer>(r_ * rhs.ToInt(), 0, 255);
        result->g_ = std::clamp<sa::tl::Integer>(g_ * rhs.ToInt(), 0, 255);
        result->b_ = std::clamp<sa::tl::Integer>(b_ * rhs.ToInt(), 0, 255);
        return result;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
            return nullptr;
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        result->r_ = std::clamp(r_ * obj->r_, 0, 255);
        result->g_ = std::clamp(g_ * obj->g_, 0, 255);
        result->b_ = std::clamp(b_ * obj->b_, 0, 255);
        result->a_ = std::clamp(a_ * obj->a_, 0, 255);
        return result;
    }
    return nullptr;
}

Color* Color::OpCompoundMul(const sa::tl::Value& rhs)
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        r_ = std::clamp<sa::tl::Integer>(r_ * rhs.ToInt(), 0, 255);
        g_ = std::clamp<sa::tl::Integer>(g_ * rhs.ToInt(), 0, 255);
        b_ = std::clamp<sa::tl::Integer>(b_ * rhs.ToInt(), 0, 255);
        return this;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
        {
            ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch,
                std::format("Can not add a {} to a {}", ro->GetClassName(), sa::tl::TypeName<Color>::Get()));
            return nullptr;
        }
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        r_ = std::clamp(r_ * obj->r_, 0, 255);
        g_ = std::clamp(g_ * obj->g_, 0, 255);
        b_ = std::clamp(b_ * obj->b_, 0, 255);
        a_ = std::clamp(a_ * obj->a_, 0, 255);
        return this;
    }
    return this;
}

Color* Color::OpDiv(const sa::tl::Value& rhs) const
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        result->r_ = std::clamp<sa::tl::Integer>(r_ / rhs.ToInt(), 0, 255);
        result->g_ = std::clamp<sa::tl::Integer>(g_ / rhs.ToInt(), 0, 255);
        result->b_ = std::clamp<sa::tl::Integer>(b_ / rhs.ToInt(), 0, 255);
        return result;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
            return nullptr;
        auto* result = ctx_.GetGC().CreateObject<Color>(ctx_);
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        result->r_ = std::clamp(r_ / obj->r_, 0, 255);
        result->g_ = std::clamp(g_ / obj->g_, 0, 255);
        result->b_ = std::clamp(b_ / obj->b_, 0, 255);
        result->a_ = std::clamp(a_ / obj->a_, 0, 255);
        return result;
    }
    return nullptr;
}

Color* Color::OpCompoundDiv(const sa::tl::Value& rhs)
{
    if (rhs.IsInt() || rhs.IsFloat())
    {
        // Scalar value
        r_ = std::clamp<sa::tl::Integer>(r_ / rhs.ToInt(), 0, 255);
        g_ = std::clamp<sa::tl::Integer>(g_ / rhs.ToInt(), 0, 255);
        b_ = std::clamp<sa::tl::Integer>(b_ / rhs.ToInt(), 0, 255);
        return this;
    }
    if (rhs.IsObject())
    {
        const sa::tl::ObjectPtr& ro = rhs.As<sa::tl::ObjectPtr>();
        if (!ro->GetMetatable().InstanceOf<Color>())
        {
            ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch,
                std::format("Can not add a {} to a {}", ro->GetClassName(), sa::tl::TypeName<Color>::Get()));
            return nullptr;
        }
        auto* obj = ro->GetMetatable().CastTo<Color>(ro.get());
        r_ = std::clamp(r_ / obj->r_, 0, 255);
        g_ = std::clamp(g_ / obj->g_, 0, 255);
        b_ = std::clamp(b_ / obj->b_, 0, 255);
        a_ = std::clamp(a_ / obj->a_, 0, 255);
        return this;
    }
    return this;
}

void Color::AlphaBlend(const Color& rhs, bool keepAlpha)
{
    const float alpha = (float)rhs.a_ / 255.0f;
    r_ = (uint8_t)(((float)rhs.r_ * alpha) + ((1.0f - alpha) * (float)r_));
    g_ = (uint8_t)(((float)rhs.g_ * alpha) + ((1.0f - alpha) * (float)g_));
    b_ = (uint8_t)(((float)rhs.b_ * alpha) + ((1.0f - alpha) * (float)b_));
    if (!keepAlpha)
        a_ = rhs.a_;
}

void Color::AlphaBlend(const sa::tl::Value& rhs, bool keepAlpha)
{
    Color col = Color(ctx_, rhs);
    if (ctx_.HasErrors())
        return;
    AlphaBlend(col, keepAlpha);
}

void Color::GreyScale()
{
    const double dr = (double)r_;
    const double dg = (double)g_;
    const double db = (double)b_;
    const double v = sqrt((dr * dr + dg * dg + db * db) / 3.0f);
    r_ = g_ = b_ = (uint8_t)v;
}

void Color::BlackWhite()
{
    const double dr = (double)r_;
    const double dg = (double)g_;
    const double db = (double)b_;
    const uint8_t v = (uint8_t)sqrt((dr * dr + dg * dg + db * db) / 3.0f);
    if (v > 127)
        r_ = g_ = b_ = 0;
    else
        r_ = g_ = b_ = 1;
}

void Color::Invert(bool invertAlpha)
{
    r_ = (uint8_t)(255 - r_);
    g_ = (uint8_t)(255 - g_);
    b_ = (uint8_t)(255 - b_);
    if (invertAlpha)
        a_ = (uint8_t)(255 - a_);
}

void Color::Lerp(const Color& rhs, float t, bool lerpAlpha)
{
    const float invt = 1.0f - t;
    r_ = r_ * invt + (float)rhs.r_ * t;
    g_ = g_ * invt + (float)rhs.g_ * t;
    b_ = b_ * invt + (float)rhs.b_ * t;
    if (lerpAlpha)
        a_ = a_ * invt + (float)rhs.a_ * t;
}

void Color::Lerp(const sa::tl::Value& rhs, float t, bool lerpAlpha)
{
    Color col = Color(ctx_, rhs);
    if (ctx_.HasErrors())
        return;
    Lerp(col, t, lerpAlpha);
}

void Color::Tint(float factor)
{
    const int r = r_ + static_cast<int>((float)(255 - r_) * factor);
    const int g = g_ + static_cast<int>((float)(255 - g_) * factor);
    const int b = b_ + static_cast<int>((float)(255 - b_) * factor);
    r_ = (uint8_t)std::clamp(r, 0, 255);
    g_ = (uint8_t)std::clamp(g, 0, 255),
    b_ = (uint8_t)std::clamp(b, 0, 255);
}

void Color::Shade(float factor)
{
    const int r = static_cast<int>((float)r_ * (1.0f - factor));
    const int g = static_cast<int>((float)g_ * (1.0f - factor));
    const int b = static_cast<int>((float)b_ * (1.0f - factor));
    r_ = (uint8_t)std::clamp(r, 0, 255);
    g_ = (uint8_t)std::clamp(g, 0, 255),
    b_ = (uint8_t)std::clamp(b, 0, 255);
}

} // namespace gfx

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<gfx::Color>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<gfx::Color, sa::tl::bind::Object>();
    result
        .AddFunction<gfx::Color, std::string>("to_string", &gfx::Color::ToString)
        .AddFunction<gfx::Color, sa::tl::TablePtr>("to_table", &gfx::Color::ToTable)
        .AddFunction<gfx::Color, uint32_t>("to_32", &gfx::Color::To32)
        .AddFunction<gfx::Color, uint32_t>("to_24", &gfx::Color::To24)
        .AddFunction<gfx::Color, sa::tl::TablePtr>("to_hsl", &gfx::Color::ToHslScript)
        .AddFunction<gfx::Color, sa::tl::TablePtr>("to_hsv", &gfx::Color::ToHsvScript)
        .AddFunction<gfx::Color, void, const sa::tl::Value&, bool>("alpha_blend", &gfx::Color::AlphaBlend)
        .AddFunction<gfx::Color, void>("grey_scale", &gfx::Color::GreyScale)
        .AddFunction<gfx::Color, void>("black_white", &gfx::Color::BlackWhite)
        .AddFunction<gfx::Color, void, bool>("invert", &gfx::Color::Invert)
        .AddFunction<gfx::Color, void, const sa::tl::Value&, float, bool>("lerp", &gfx::Color::Lerp)
        .AddFunction<gfx::Color, void, float>("tint", &gfx::Color::Tint)
        .AddFunction<gfx::Color, void, float>("shade", &gfx::Color::Shade)
        .AddProperty("r", &gfx::Color::GetR, &gfx::Color::SetR)
        .AddProperty("g", &gfx::Color::GetG, &gfx::Color::SetG)
        .AddProperty("b", &gfx::Color::GetB, &gfx::Color::SetB)
        .AddProperty("a", &gfx::Color::GetA, &gfx::Color::SetA)
        .AddProperty("hue", &gfx::Color::GetHue, &gfx::Color::SetHue)
        .AddProperty("saturation", &gfx::Color::GetSaturation, &gfx::Color::SetSaturation)
        .AddProperty("lightness", &gfx::Color::GetLightness, &gfx::Color::SetLightness)
        .AddProperty("value", &gfx::Color::GetValue, &gfx::Color::SetValue)
        .AddOperator("operator+", &gfx::Color::OpAdd)
        .AddOperator("operator+=", &gfx::Color::OpCompoundAdd)
        .AddOperator("operator-", &gfx::Color::OpSub)
        .AddOperator("operator-=", &gfx::Color::OpCompoundSub)
        .AddOperator("operator*", &gfx::Color::OpMul)
        .AddOperator("operator*=", &gfx::Color::OpCompoundMul)
        .AddOperator("operator/", &gfx::Color::OpDiv)
        .AddOperator("operator/=", &gfx::Color::OpCompoundDiv)
        .AddOperator("operator==", &gfx::Color::OpEq)
        .AddOperator("operator!=", &gfx::Color::OpIneq);
    return result;
}

}
