/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace gfx {

enum class ClipType : int
{
    None,
    Inside,
    Outside
};

class ClipRegion
{
public:
    [[nodiscard]] bool IsInside(int x, int y) const
    {
        return (left_ < x) && (right_ > x) &&
            (top_ < y) && (bottom_ > y);
    }
    [[nodiscard]] bool IsOutside(int x, int y) const
    {
        return !IsInside(x, y);
    }
    int left_{ -1 };
    int top_{ -1 };
    int right_{ -1 };
    int bottom_{ -1 };
};

}
