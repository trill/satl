# gfx_lib

Library to work with colors and bitmaps.

## Requirements

* [stb](https://github.com/nothings/stb) on Arch you can install it from AUR https://aur.archlinux.org/packages/stb

## Example

~~~
load_lib("libsatl_gfx.so");

// You should have DejaVuSans.ttf somewhere where it can find it.
const font_file = "DejaVuSans.ttf";

// Create a bitmap we draw to
bitmap = gfx.bitmap(600, 480, 3);
// Whiteout the bitmap
bitmap.clear(gfx.WHITE);

// Draw outlined rect
bitmap.draw_rect(10, 10, 50, 50, gfx.BLACK, 2);
// Draw filled rect
bitmap.fill_rect(10, 70, 50, 100, gfx.RED);

// Draw outlined circle
bitmap.draw_circle(300, 100, 50, gfx.RED, 2);
// Draw filled circle
bitmap.fill_circle(400, 150, 50, gfx.RED);

// Text y offset
y = 250;
// Draw some text
bitmap.draw_text(10, y, "Hello there!", font_file, 100, gfx.BLACK);
// y offset by text height
y += bitmap.get_text_height("Hello there!", font_file, 100) + 20;
// Draw UTF-8 text
bitmap.draw_text(10, y, "UTF-8: öäüß ☃ ☕", font_file, 80, gfx.BLUE);

// Save the bitmap as PNG
bitmap.save("draw_suff.png");
~~~

![draw_stuff](draw_suff.png?raw=true)

Well, yeah, text rending needs some fine tuning, I guess.

## API

See the Manual for the API documentation.
