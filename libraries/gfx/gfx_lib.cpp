/**
 * Copyright (c) 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <module.h>
// https://github.com/nothings/stb required, on Arch you can install it from AUR
// https://aur.archlinux.org/packages/stb
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include <stb/stb_image_resize2.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb/stb_truetype.h>
#include "bitmap.h"
#include "color.h"
#include <factory.h>

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("gfx"))
        return true;

    sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    sa::tl::MetaTable& colorMeta = sa::tl::bind::Register<gfx::Color>(ctx);
    sa::tl::MetaTable& bitmapMeta = sa::tl::bind::Register<gfx::Bitmap>(ctx);

    auto module = sa::tl::MakeModule();
    module->AddConst<int>("CLIP_TYPE_NONE", (int)gfx::ClipType::None);
    module->AddConst<int>("CLIP_TYPE_INSIDE", (int)gfx::ClipType::Inside);
    module->AddConst<int>("CLIP_TYPE_OUTSIDE", (int)gfx::ClipType::Outside);

    module->AddFunction("color",
        [&colorMeta](sa::tl::Context& ctx, const sa::tl::FunctionArguments& args) -> sa::tl::ValuePtr
        {
            sa::tl::ValuePtr color;
            if (args.empty())
                return sa::tl::bind::CreateInstanceValuePtr<gfx::Color>(ctx, colorMeta, ctx);
            if (args.size() == 1)
                return sa::tl::bind::CreateInstanceValuePtr<gfx::Color>(ctx, colorMeta, ctx, *args[0]);
            if (args.size() == 3)
            {
                if (args[0]->IsInt() && args[1]->IsInt() && args[2]->IsInt())
                    return sa::tl::bind::CreateInstanceValuePtr<gfx::Color>(ctx, colorMeta, ctx, args[0]->ToInt(), args[1]->ToInt(), args[2]->ToInt());
                if (args[0]->IsFloat() && args[1]->IsFloat() && args[2]->IsFloat())
                {
                    return sa::tl::bind::CreateInstanceValuePtr<gfx::Color>(ctx, colorMeta, ctx,
                        static_cast<sa::tl::Integer>(args[0]->ToFloat() * 255.0f),
                        static_cast<sa::tl::Integer>(args[1]->ToFloat() * 255.0f),
                        static_cast<sa::tl::Integer>(args[2]->ToFloat() * 255.0f));
                }
                ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Three integer or float values expected");
                return sa::tl::MakeNull();
            }
            if (args.size() == 4)
            {
                if (args[0]->IsInt() && args[1]->IsInt() && args[2]->IsInt() && args[3]->IsInt())
                    return sa::tl::bind::CreateInstanceValuePtr<gfx::Color>(ctx, colorMeta, ctx, args[0]->ToInt(), args[1]->ToInt(), args[2]->ToInt(), args[3]->ToInt());
                if (args[0]->IsFloat() && args[1]->IsFloat() && args[2]->IsFloat() && args[3]->IsFloat())
                {
                    return sa::tl::bind::CreateInstanceValuePtr<gfx::Color>(ctx, colorMeta, ctx,
                        static_cast<sa::tl::Integer>(args[0]->ToFloat() * 255.0f),
                        static_cast<sa::tl::Integer>(args[1]->ToFloat() * 255.0f),
                        static_cast<sa::tl::Integer>(args[2]->ToFloat() * 255.0f),
                        static_cast<sa::tl::Integer>(args[3]->ToFloat() * 255.0f));
                }
                ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Four integer or float values expected");
                return sa::tl::MakeNull();
            }
            ctx.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments");
            return sa::tl::MakeNull();
        });
    module->AddFunction("bitmap",
        [&bitmapMeta](sa::tl::Context& ctx, const sa::tl::FunctionArguments& args) -> sa::tl::ValuePtr
        {
            if (args.empty())
                return sa::tl::bind::CreateInstanceValuePtr<gfx::Bitmap>(ctx, bitmapMeta, ctx);
            if (args.size() == 1)
            {
                if (args[0]->IsString())
                    return sa::tl::bind::CreateInstanceValuePtr<gfx::Bitmap>(ctx, bitmapMeta, ctx, args[0]->ToString());
                if (args[0]->IsObject())
                {
                    const auto& object = args[0]->As<sa::tl::ObjectPtr>();
                    const auto& meta = object->GetMetatable();
                    if (auto* bitmapObj = meta.CastTo<gfx::Bitmap>(object.get()))
                        return sa::tl::bind::CreateInstanceValuePtr<gfx::Bitmap>(ctx, bitmapMeta, ctx, *bitmapObj);
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, std::format("Expecting a Bitmap object, but got a {}", object->GetClassName()));
                    return sa::tl::MakeNull();
                }
                ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String or Object expected");
                return sa::tl::MakeNull();
            }
            if (args.size() == 3)
            {
                if (!args[0]->IsInt() || !args[1]->IsInt() || !args[2]->IsInt())
                {
                    ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Three integer values expected");
                    return sa::tl::MakeNull();
                }
                int components = args[2]->ToInt();
                if (components < 1 || components > 4)
                {
                    ctx.RuntimeError(sa::tl::Error::Code::InvalidArgument, "Components must be a value between 1 and 4");
                    return sa::tl::MakeNull();
                }
                return sa::tl::bind::CreateInstanceValuePtr<gfx::Bitmap>(ctx, bitmapMeta, ctx, args[0]->ToInt(), args[1]->ToInt(), args[2]->ToInt());
            }
            ctx.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments");
            return sa::tl::MakeNull();
        });

    module->SetInstance(colorMeta, "BLACK", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 0, 0, 0, 255), true);
    module->SetInstance(colorMeta, "GREY", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 127, 127, 127, 255), true);
    module->SetInstance(colorMeta, "WHITE", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 255, 255, 255, 255), true);
    module->SetInstance(colorMeta, "TRANSPARENT", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 0, 0, 0, 0), true);
    module->SetInstance(colorMeta, "RED", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 255, 0, 0, 255), true);
    module->SetInstance(colorMeta, "GREEN", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 0, 255, 0, 255), true);
    module->SetInstance(colorMeta, "BLUE", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 0, 0, 255, 255), true);
    module->SetInstance(colorMeta, "CYAN", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 0, 255, 255, 255), true);
    module->SetInstance(colorMeta, "MAGENTA", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 255, 0, 255, 255), true);
    module->SetInstance(colorMeta, "YELLOW", *sa::tl::bind::CreateInstancePtr<gfx::Color>(ctx, ctx, 255, 255, 0, 255), true);

    ctx.AddConst("gfx", std::move(module), sa::tl::SCOPE_LIBRARY);
    return true;
}
