/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <non_copyable.h>
#include <context.h>
#include "color.h"
#include "clip.h"
#include <ast.h>
#include <object.h>

namespace gfx {

class Bitmap final : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Bitmap);
    SATL_NON_MOVEABLE(Bitmap);
    TYPED_OBJECT(Bitmap);
public:
    explicit Bitmap(sa::tl::Context& ctx);
    Bitmap(sa::tl::Context& ctx, const std::string& filename);
    Bitmap(sa::tl::Context& ctx, const Bitmap& other);
    Bitmap(sa::tl::Context& ctx, int width, int height, int components);
    ~Bitmap() override;

    sa::tl::Value GetPixelScript(int x, int y);
    [[nodiscard]] Color GetPixel(int x, int y) const;
    void SetPixel(int x, int y, const sa::tl::Value& value);
    void SetPixel(int x, int y, const Color& value);
    [[nodiscard]] int GetWidth() const { return width_; }
    [[nodiscard]] int GetHeight() const { return height_; }
    [[nodiscard]] int GetComponents() const { return components_; }
    [[nodiscard]] std::string GetDataString() const;
    void SetBitmap(int width, int height, int components, const std::string& data);
    bool Resize(int width, int height);
    void Clear(const sa::tl::Value& value);
    void Clear(const Color& color);
    void Invert();
    void GreyScale();
    void Tint(float factor);
    void Shade(float factor);
    void Lerp(const Color& rhs, float factor);
    void Lerp(const sa::tl::Value& rhs, float factor);
    void AlphaBlend(int x, int y, const Color& c, bool keepAlpha = true);
    void AlphaBlend(int x, int y, const Bitmap& b, bool keepAlpha = true);
    void AlphaBlendLerp(int x, int y, const Color& c, const Color& lerpColor, float lerpFactor, bool keepAlpha = true);
    void AlphaBlendLerp(int x, int y, const Bitmap& b, const Color& lerpColor, float lerpFactor, bool keepAlpha = true);
    void DrawPoint(int x, int y, int size, const Color& color);
    void DrawLine(int x1, int y1, int x2, int y2, int size, const Color& color);
    void DrawRectangle(int x1, int y1, int x2, int y2, const Color& color, int thickness);
    void FillRectangle(int x1, int y1, int x2, int y2, const Color& color);
    void DrawCircle(int x, int y, int radius, const Color& color, int thickness);
    void FillCircle(int x, int y, int radius, const Color& color);
    void DrawEllipse(int cx, int cy, int rx, int ry, const Color& color, int thickness);
    void FillEllipse(int cx, int cy, int rx, int ry, const Color& color);
    void DrawText(int x, int y, const std::string& text, const std::string& fontFile, int size, const Color& fontColor);
    int GetTextWidth(const std::string& text, const std::string& fontFile, int size);
    int GetTextHeight(const std::string& text, const std::string& fontFile, int size);
    void FloodFill(int x, int y, const Color& color);
    void FloodFill(int x, int y, const Color& oldColor, const Color& color);
    void BoundaryFill(int x, int y, const Color& bColor, const Color& color);
    void PlotFunction(int x, int y, int from, int to, int thickness, const Color& color, const sa::tl::Callable& callback);

    void AlphaBlend(int x, int y, const sa::tl::Value& c, bool keepAlpha);
    void AlphaBlendBitmap(int x, int y, const sa::tl::Value& b, bool keepAlpha);
    void AlphaBlendLerp(int x, int y, const sa::tl::Value& c, const sa::tl::Value& lerpColor, float lerpFactor, bool keepAlpha);
    void AlphaBlendLerpBitmap(int x, int y, const sa::tl::Value& b, const sa::tl::Value& lerpColor, float lerpFactor, bool keepAlpha);
    void DrawPoint(int x, int y, int size, const sa::tl::Value& color);
    void DrawLine(int x1, int y1, int x2, int y2, int size, const sa::tl::Value& color);
    void DrawRectangle(int x1, int y1, int x2, int y2, const sa::tl::Value& color, int thickness);
    void FillRectangle(int x1, int y1, int x2, int y2, const sa::tl::Value& color);
    void DrawCircle(int x, int y, int radius, const sa::tl::Value& color, int thickness);
    void FillCircle(int x, int y, int radius, const sa::tl::Value& color);
    void DrawEllipse(int cx, int cy, int rx, int ry, const sa::tl::Value& color, int thickness);
    void FillEllipse(int cx, int cy, int rx, int ry, const sa::tl::Value& color);
    void DrawText(int x, int y, const std::string& text, const std::string& fontFile, int size, const sa::tl::Value& fontColor);
    void FloodFill(int x, int y, const sa::tl::Value& color);
    void BoundaryFill(int x, int y, const sa::tl::Value& bColor, const sa::tl::Value& color);
    void PlotFunction(int x, int y, int from, int to, int thickness, const sa::tl::Value& color, const sa::tl::Callable& callback);
    void SetClipRegion(const sa::tl::Value& region);
    [[nodiscard]] sa::tl::Value GetClipRegion() const;
    void SetClipType(int value);
    [[nodiscard]] int GetClipType() const { return (int)clipType_; }

    [[nodiscard]] bool Save(const std::string& filename) const;
private:
    [[nodiscard]] bool SavePPM(const std::string& filename) const;
    [[nodiscard]] bool SavePBM(const std::string& filename) const;
    [[nodiscard]] bool SavePGM(const std::string& filename) const;
    unsigned char* GetPixelData(int x, int y);
    [[nodiscard]] bool IsVisible(int x, int y) const;
    static void Ellipse(int rx, int ry, const std::function<void(int,int)>& callback);
    int width_{ 0 };
    int height_{ 0 };
    int components_{ 0 };
    unsigned char* data_{ nullptr };
    ClipType clipType_{ ClipType::None };
    std::unique_ptr<ClipRegion> clipRegion_;
};

} // namespace gfx

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<gfx::Bitmap>(sa::tl::Context& ctx);
}
