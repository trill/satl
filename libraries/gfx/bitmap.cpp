/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "bitmap.h"
#include <satl_assert.h>
#include <stb/stb_image.h>
#include <stb/stb_image_resize2.h>
#include <stb/stb_image_write.h>
#include <stb/stb_truetype.h>
#include <utils.h>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <utf8.h>

namespace gfx {

Bitmap::Bitmap(sa::tl::Context& ctx) :
    Object(ctx)
{
}

Bitmap::Bitmap(sa::tl::Context& ctx, const std::string& filename) :
    Object(ctx)
{
    data_ = stbi_load(filename.c_str(), &width_, &height_, &components_, 0);
}

Bitmap::Bitmap(sa::tl::Context& ctx, const Bitmap& other) :
    Object(ctx)
{
    width_ = other.width_;
    height_ = other.height_;
    components_ = other.components_;
    size_t size = (size_t)width_ * (size_t)height_ * (size_t)components_;
    data_ = (unsigned char*)malloc(size);
    memcpy(data_, other.data_, size);
}

Bitmap::Bitmap(sa::tl::Context& ctx, int width, int height, int components) :
    Object(ctx)
{
    width_ = width;
    height_ = height;
    components_ = components;
    data_ = (unsigned char*)malloc((size_t)width_ * (size_t)height_ * (size_t)components_);
}

Bitmap::~Bitmap()
{
    if (data_)
    {
        free(data_);
        data_ = nullptr;
    }
}

sa::tl::Value Bitmap::GetPixelScript(int x, int y)
{
    Color pixel = GetPixel(x, y);
    auto* color = ctx_.GetGC().CreateObject<Color>(ctx_, pixel.r_, pixel.g_, pixel.b_, pixel.a_);
    auto* colorMeta = ctx_.GetMetatable<Color>();
    SATL_ASSERT(colorMeta);
    return sa::tl::MakeObject(*colorMeta, color);
}

Color Bitmap::GetPixel(int x, int y) const
{
    if (!data_)
        return Color(ctx_);
    if (x < 0 || y < 0 || x >= width_ || y >= height_)
        return Color(ctx_);
    const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
    uint8_t r = 0, g = 0, b = 0, a = 255;
    if (components_ == 1)
        // B/W
        r = g = b = data_[(index * components_)];
    else if (components_ == 3)
    {
        // RGB
        r = data_[(index * components_)];
        g = data_[(index * components_ + 1)];
        b = data_[(index * components_ + 2)];
    }
    else if (components_ == 4)
    {
        // RGBA
        r = data_[(index * components_)];
        g = data_[(index * components_ + 1)];
        b = data_[(index * components_ + 2)];
        a = data_[(index * components_ + 3)];
    }
    else
        return Color(ctx_);
    return { ctx_, r, g, b, a };
}

unsigned char* Bitmap::GetPixelData(int x, int y)
{
    if (!data_)
        return nullptr;
    if (x < 0 || y < 0 || x >= width_ || y >= height_)
        return nullptr;
    const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
    return &data_[index * components_];
}

void Bitmap::SetPixel(int x, int y, const sa::tl::Value& value)
{
    Color color = Color(ctx_, value);
    if (ctx_.HasErrors())
        return;
    SetPixel(x, y, color);
}

void Bitmap::SetPixel(int x, int y, const Color& color)
{
    if (!data_)
        return;
    if (!IsVisible(x, y))
        return;

    if (x < 0 || y < 0 || x >= width_ || y >= height_)
        return;
    const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
    if (components_ == 1)
        // B/W
        data_[(index * components_)] = color.r_;
    else if (components_ == 3)
    {
        // RGB
        data_[(index * components_)] = color.r_;
        data_[(index * components_ + 1)] = color.g_;
        data_[(index * components_ + 2)] = color.b_;
    }
    else if (components_ == 4)
        // RGBA
        *reinterpret_cast<uint32_t*>(&data_[(index * components_)]) = color.To32();
}

bool Bitmap::Save(const std::string& filename) const
{
    if (!data_)
        return false;
    auto ext = sa::tl::ExtractFileExt(filename);
    if (sa::tl::StringEquals<char>(ext, ".png"))
        return stbi_write_png(filename.c_str(), width_, height_, components_, data_, width_ * components_);
    if (sa::tl::StringEquals<char>(ext, ".bmp"))
        return stbi_write_bmp(filename.c_str(), width_, height_, components_, data_);
    if (sa::tl::StringEquals<char>(ext, ".jpg") || sa::tl::StringEquals<char>(ext, ".jpeg"))
        return stbi_write_jpg(filename.c_str(), width_, height_, components_, data_, 100);
    if (sa::tl::StringEquals<char>(ext, ".tga"))
        return stbi_write_tga(filename.c_str(), width_, height_, components_, data_);
    if (sa::tl::StringEquals<char>(ext, ".ppm"))
        return SavePPM(filename);
    if (sa::tl::StringEquals<char>(ext, ".pbm"))
        return SavePBM(filename);
    if (sa::tl::StringEquals<char>(ext, ".pgm"))
        return SavePGM(filename);
    return false;
}

bool Bitmap::SavePPM(const std::string& filename) const
{
    std::fstream stream(filename, std::ios_base::out);
    if (!stream.is_open())
        return false;
    // https://en.wikipedia.org/wiki/Netpbm#File_formats
    stream << "P6 " << width_ << " " << height_ << " 255 ";
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            Color pixel = GetPixel(x, y);
            stream << (char)pixel.GetR() << (char)pixel.GetG() << (char)pixel.GetB();
        }
    }
    return true;
}

bool Bitmap::SavePBM(const std::string& filename) const
{
    std::fstream stream(filename, std::ios_base::out);
    if (!stream.is_open())
        return false;
    // https://en.wikipedia.org/wiki/Netpbm#File_formats
    // Black/White
    size_t numBits = 0;
    unsigned char value = 0;
    stream << "P4 " << width_ << " " << height_ << " ";
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            Color pixel = GetPixel(x, y);
            pixel.BlackWhite();
            if (numBits == 7)
            {
                stream << value;
                value = 0;
                numBits = 0;
            }
            else
            {
                if (pixel.GetR() == 1)
                    value |= 1u << numBits;
                ++numBits;
            }
        }
    }
    if (value != 0)
        stream << value;
    return true;
}

bool Bitmap::SavePGM(const std::string& filename) const
{
    std::fstream stream(filename, std::ios_base::out);
    if (!stream.is_open())
        return false;
    // https://en.wikipedia.org/wiki/Netpbm#File_formats
    // Greyscale
    stream << "P5 " << width_ << " " << height_ << " 255 ";
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            Color pixel = GetPixel(x, y);
            pixel.GreyScale();
            stream << (char)pixel.GetR();
        }
    }
    return true;
}

bool Bitmap::Resize(int width, int height)
{
    if (!data_)
        return false;

    // static_cast<stbir_pixel_layout>(components_) should work because we have 1..4 components and these are:
    // 1: BW = STBIR_1CHANNEL
    // 2: STBIR_2CHANNEL
    // 3: RGB
    // 4: RGBA
    if (components_ < 1 || components_ > 4)
        return false;

    unsigned char* newData = (unsigned char*)malloc((size_t)width * (size_t)height * (size_t)components_);

    bool result = stbir_resize_uint8_srgb(
        data_, width_, height_, width_ * components_, newData, width, height, width * components_, static_cast<stbir_pixel_layout>(components_));
    if (result)
    {
        free(data_);
        data_ = newData;
        width_ = width;
        height_ = height;
    }
    else
    {
        // Error
        free(newData);
    }
    return result;
}

std::string Bitmap::GetDataString() const
{
    if (!data_)
        return "";
    return { (const char*)data_, (size_t)width_ * (size_t)height_ * (size_t)components_ };
}

void Bitmap::SetBitmap(int width, int height, int components, const std::string& data)
{
    if ((size_t)width_ * (size_t)height_ * (size_t)components_ != data.size())
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "Wrong dimensions");
        return;
    }
    if (data_)
        free(data_);
    width_ = width;
    height_ = height;
    components_ = components;
    size_t size = (size_t)width_ * (size_t)height_ * (size_t)components_;
    data_ = (unsigned char*)malloc(size);
    memcpy(data_, data.data(), size);
}

void Bitmap::Clear(const sa::tl::Value& value)
{
    Color color = Color(ctx_, value);
    if (ctx_.HasErrors())
        return;
    Clear(color);
}

void Bitmap::Clear(const Color& color)
{
    if (components_ == 4)
    {
        const uint32_t uc = color.To32();
        for (int y = 0; y < height_; ++y)
        {
            for (int x = 0; x < width_; ++x)
            {
                const size_t index = (size_t)y * (size_t)width_ + (size_t)x;
                *reinterpret_cast<uint32_t*>(&data_[(index * components_)]) = uc;
            }
        }
    }
    else
    {
        for (int y = 0; y < height_; ++y)
        {
            for (int x = 0; x < width_; ++x)
                SetPixel(x, y, color);
        }
    }
}

void Bitmap::Invert()
{
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            auto pixel = GetPixel(x, y);
            pixel.Invert(false);
            SetPixel(x, y, pixel);
        }
    }
}

void Bitmap::GreyScale()
{
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            auto pixel = GetPixel(x, y);
            pixel.GreyScale();
            SetPixel(x, y, pixel);
        }
    }
}

void Bitmap::Tint(float factor)
{
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            auto pixel = GetPixel(x, y);
            pixel.Tint(factor);
            SetPixel(x, y, pixel);
        }
    }
}

void Bitmap::Shade(float factor)
{
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            auto pixel = GetPixel(x, y);
            pixel.Shade(factor);
            SetPixel(x, y, pixel);
        }
    }
}

void Bitmap::Lerp(const Color& rhs, float factor)
{
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            auto pixel = GetPixel(x, y);
            pixel.Lerp(rhs, factor, false);
            SetPixel(x, y, pixel);
        }
    }
}

void Bitmap::Lerp(const sa::tl::Value& rhs, float factor)
{
    Color col = Color(ctx_, rhs);
    if (ctx_.HasErrors())
        return;
    Lerp(col, factor);
}

void Bitmap::AlphaBlend(int x, int y, const Color& c, bool keepAlpha)
{
    if (x < 0 || y < 0 || x >= width_ || y >= height_)
        return;

    auto pixel = GetPixel(x, y);
    pixel.AlphaBlend(c, keepAlpha);
    SetPixel(x, y, pixel);
}

void Bitmap::AlphaBlend(int x, int y, const Bitmap& b, bool keep_alpha)
{
    for (int _y = 0; _y < b.GetHeight(); ++_y)
    {
        if (_y + y >= height_)
            break;
        for (int _x = 0; _x < b.GetWidth(); ++_x)
        {
            if (_x + x >= width_)
                break;
            AlphaBlend(_x + x, _y + y, b.GetPixel(_x, _y), keep_alpha);
        }
    }
}

void Bitmap::AlphaBlendLerp(int x, int y, const Color& c, const Color& lerpColor, float lerpFactor, bool keepAlpha)
{
    if (x < 0 || y < 0 || x >= width_ || y >= height_)
        return;
    Color cl { ctx_, c.r_, c.g_, c.b_, c.a_ };
    cl.Lerp(lerpColor, lerpFactor, false);
    auto pixel = GetPixel(x, y);
    pixel.AlphaBlend(cl, keepAlpha);
    SetPixel(x, y, pixel);
}

void Bitmap::AlphaBlendLerp(int x, int y, const Bitmap& b, const Color& lerpColor, float lerpFactor, bool keepAlpha)
{
    for (int _y = 0; _y < b.GetHeight(); ++_y)
    {
        if (_y + y >= height_)
            break;
        for (int _x = 0; _x < b.GetWidth(); ++_x)
        {
            if (_x + x >= width_)
                break;

            AlphaBlendLerp(_x + x, _y + y, b.GetPixel(_x, _y), lerpColor, lerpFactor, keepAlpha);
        }
    }
}

void Bitmap::DrawPoint(int x, int y, int size, const Color& color)
{
    if (size < 2)
        return AlphaBlend(x, y, color, false);
    for (int _y = -(size / 2); _y < size / 2; ++_y)
    {
        for (int _x = -(size / 2); _x < size / 2; ++_x)
            AlphaBlend(x + _x, y + _y, color, false);
    }
}

void Bitmap::DrawLine(int x1, int y1, int x2, int y2, int size, const Color& color)
{
    const bool steep = (abs(y2 - y1) > abs(x2 - x1));
    if (steep)
    {
        std::swap(x1, y1);
        std::swap(x2, y2);
    }

    if (x1 > x2)
    {
        std::swap(x1, x2);
        std::swap(y1, y2);
    }

    const int dx = x2 - x1;
    const int dy = abs(y2 - y1);

    int error = dx / 2;
    const int ystep = (y1 < y2) ? 1 : -1;
    int y = y1;
    const int maxX = x2;
    for (int x = x1; x <= maxX; ++x)
    {
        if (steep)
            DrawPoint(y, x, size, color);
        else
            DrawPoint(x, y, size, color);

        error -= dy;
        if (error < 0)
        {
            y += ystep;
            error += dx;
        }
    }
}

void Bitmap::DrawRectangle(int x1, int y1, int x2, int y2, const Color& color, int thickness)
{
    for (int y = y1; y <= y2; ++y)
    {
        for (int x = x1; x <= x2; ++x)
            if (y == y1 || y == y2 || x == x1 || x == x2)
                DrawPoint(x, y, thickness, color);
    }
}

void Bitmap::FillRectangle(int x1, int y1, int x2, int y2, const Color& color)
{
    for (int y = y1; y <= y2; ++y)
    {
        for (int x = x1; x <= x2; ++x)
            SetPixel(x, y, color);
    }
}

void Bitmap::DrawCircle(int x, int y, int radius, const Color& color, int thickness)
{
    // https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int _x = 0;
    int _y = radius;

    DrawPoint(x, y + radius, thickness, color);
    DrawPoint(x, y - radius, thickness, color);
    DrawPoint(x + radius, y, thickness, color);
    DrawPoint(x - radius, y, thickness, color);

    while (_x < _y)
    {
        if (f >= 0)
        {
            --_y;
            ddF_y += 2;
            f += ddF_y;
        }
        ++_x;
        ddF_x += 2;
        f += ddF_x + 1;
        DrawPoint(x + _x, y + _y, thickness, color);
        DrawPoint(x - _x, y + _y, thickness, color);
        DrawPoint(x + _x, y - _y, thickness, color);
        DrawPoint(x - _x, y - _y, thickness, color);
        DrawPoint(x + _y, y + _x, thickness, color);
        DrawPoint(x - _y, y + _x, thickness, color);
        DrawPoint(x + _y, y - _x, thickness, color);
        DrawPoint(x - _y, y - _x, thickness, color);
    }
}

void Bitmap::FillCircle(int x, int y, int radius, const Color& color)
{
    int offsetx = 0, offsety = radius, d = radius - 1;

    while (offsety >= offsetx)
    {
        DrawLine(x - offsety, y + offsetx, x + offsety, y + offsetx, 1, color);
        DrawLine(x - offsetx, y + offsety, x + offsetx, y + offsety, 1, color);
        DrawLine(x - offsetx, y - offsety, x + offsetx, y - offsety, 1, color);
        DrawLine(x - offsety, y - offsetx, x + offsety, y - offsetx, 1, color);

        if (d >= 2 * offsetx)
        {
            d -= 2 * offsetx + 1;
            offsetx += 1;
        }
        else if (d < 2 * (radius - offsety))
        {
            d += 2 * offsety - 1;
            offsety -= 1;
        }
        else
        {
            d += 2 * (offsety - offsetx - 1);
            offsety -= 1;
            offsetx += 1;
        }
    }
}

void Bitmap::DrawEllipse(int cx, int cy, int rx, int ry, const Color& color, int thickness)
{
    Ellipse(rx, ry,
        [&](int x, int y)
        {
            DrawPoint(cx + x, cy + y, thickness, color);
            DrawPoint(cx - x, cy + y, thickness, color);
            DrawPoint(cx + x, cy - y, thickness, color);
            DrawPoint(cx - x, cy - y, thickness, color);
        });
}

// https://web.archive.org/web/20120225095359/http://homepage.smc.edu/kennedy_john/belipse.pdf
void Bitmap::Ellipse(int rx, int ry, const std::function<void(int,int)>& callback)
{
    int twoASquare = 2 * rx * rx;
    int twoBSquare = 2 * ry * ry;

    {
        int x = rx;
        int y = 0;
        int xChange = ry * ry * (1 - 2 * rx);
        int yChange = rx * rx;
        int ellipseError = 0;
        int stopingX = twoBSquare * rx;
        int stoppingY = 0;

        while (stopingX >= stoppingY)
        {
            callback(x, y);
            ++y;
            stoppingY += twoASquare;
            ellipseError += yChange;
            yChange += twoASquare;

            if ((2 * ellipseError + xChange) > 0)
            {
                --x;
                stopingX -= twoBSquare;
                ellipseError += xChange;
                xChange += twoBSquare;
            }
        }
    }

    {
        int x = 0;
        int y = ry;
        int xChange = ry * ry;
        int yChange = rx * rx * (1 - 2 * ry);
        int ellipseError = 0;
        int stoppingX = 0;
        int stoppingY = twoASquare * ry;

        while (stoppingX <= stoppingY)
        {
            callback(x, y);
            ++x;
            stoppingX += twoBSquare;
            ellipseError += xChange;
            xChange += twoBSquare;

            if ((2 * ellipseError + yChange) > 0)
            {
                --y;
                stoppingY -= twoASquare;
                ellipseError += yChange;
                yChange += twoASquare;
            }
        }
    }
}

void Bitmap::FillEllipse(int cx, int cy, int rx, int ry, const Color& color)
{
    Ellipse(rx, ry,
        [&](int x, int y)
        {
            // Upper
            for (int _y = cy - y; _y < cy; ++_y)
            {
                SetPixel(cx + x, _y, color);
                SetPixel(cx - x, _y, color);
            }
            // Lower
            for (int _y = cy; _y <= cy + y; ++_y)
            {
                SetPixel(cx - x, _y, color);
                SetPixel(cx + x, _y, color);
            }
        });
}

int Bitmap::GetTextWidth(const std::string& text, const std::string& fontFile, int size)
{
    std::ifstream ifs(fontFile.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
    if (!ifs.is_open())
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Unable to open file {}", fontFile));
        return 0;
    }

    std::ifstream::pos_type fileSize = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    std::vector<char> buffer(fileSize);
    ifs.read(buffer.data(), fileSize);

    stbtt_fontinfo font;
    stbtt_InitFont(&font, (const unsigned char*)buffer.data(), 0);

    float scale = stbtt_ScaleForPixelHeight(&font, (float)size);
    int ascent = 0;
    stbtt_GetFontVMetrics(&font, &ascent, nullptr, nullptr);

    auto codepoints = sa::utf8::Decode(text);
    int result = 0;
    for (size_t i = 0; i < codepoints.size(); ++i)
    {
        int advance = 0, lsb = 0, x0 = 0, y0 = 0, x1 = 0, y1 = 0;
        stbtt_GetCodepointHMetrics(&font, (int)codepoints[i], &advance, &lsb);
        stbtt_GetCodepointBitmapBox(&font, (int)codepoints[i], scale, scale, &x0, &y0, &x1, &y1);
        result += x1 - x0;
        result += (int)((float)advance * scale);
        if (i + 1 < codepoints.size())
            result += scale * stbtt_GetCodepointKernAdvance(&font, codepoints[i], codepoints[i + 1]);
    }
    return result;
}

int Bitmap::GetTextHeight(const std::string& text, const std::string& fontFile, int size)
{
    std::ifstream ifs(fontFile.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
    if (!ifs.is_open())
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Unable to open file {}", fontFile));
        return 0;
    }

    std::ifstream::pos_type fileSize = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    std::vector<char> buffer(fileSize);
    ifs.read(buffer.data(), fileSize);

    stbtt_fontinfo font;
    stbtt_InitFont(&font, (const unsigned char*)buffer.data(), 0);

    float scale = stbtt_ScaleForPixelHeight(&font, (float)size);
    int ascent = 0;
    stbtt_GetFontVMetrics(&font, &ascent, nullptr, nullptr);

    auto codepoints = sa::utf8::Decode(text);
    int result = 0;
    for (auto codepoint : codepoints)
    {
        int advance = 0, lsb = 0, x0 = 0, y0 = 0, x1 = 0, y1 = 0;
        stbtt_GetCodepointHMetrics(&font, (int)codepoint, &advance, &lsb);
        stbtt_GetCodepointBitmapBox(&font, (int)codepoint, scale, scale, &x0, &y0, &x1, &y1);
        result = std::max(result, y1 - y0);
    }
    return result;
}

void Bitmap::DrawText(int x,
    int y,
    const std::string& text,
    const std::string& fontFile,
    int size,
    const Color& fontColor)
{
    std::ifstream ifs(fontFile.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
    if (!ifs.is_open())
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Unable to open file {}", fontFile));
        return;
    }

    std::ifstream::pos_type fileSize = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    std::vector<char> buffer(fileSize);
    ifs.read(buffer.data(), fileSize);

    stbtt_fontinfo font;
    stbtt_InitFont(&font, (const unsigned char*)buffer.data(), 0);

    float scale = stbtt_ScaleForPixelHeight(&font, (float)size);
    int ascent = 0;
    stbtt_GetFontVMetrics(&font, &ascent, nullptr, nullptr);
    int baseline = (int)((float)ascent * scale);

    auto codepoints = sa::utf8::Decode(text);
    Color transparent = Color{ ctx_, 0, 0, 0, 0 };
    int xpos = x;
    for (size_t i = 0; i < codepoints.size(); ++i)
    {
        int advance = 0, lsb = 0, x0 = 0, y0 = 0, x1 = 0, y1 = 0;
        stbtt_GetCodepointHMetrics(&font, (int)codepoints[i], &advance, &lsb);
        stbtt_GetCodepointBitmapBox(&font, (int)codepoints[i], scale, scale, &x0, &y0, &x1, &y1);

        Bitmap bmp(ctx_, x1 - x0, y1 - y0, 4);
        bmp.Clear(transparent);

        stbtt_MakeCodepointBitmap(
            &font, bmp.data_, bmp.width_, bmp.height_, bmp.width_ * 4, scale, scale, (int)codepoints[i]);
        // if you want to render
        // a sequence of characters, you really need to render each bitmap to a temp buffer, then
        // "alpha blend" that into the working buffer
        AlphaBlendLerp(xpos, y + baseline + y0, bmp, fontColor, 1.0f, true);

        xpos += (int)((float)advance * scale);
        if (i + 1 < codepoints.size())
            xpos += scale * stbtt_GetCodepointKernAdvance(&font, codepoints[i], codepoints[i + 1]);
    }
}

void Bitmap::DrawText(int x, int y, const std::string& text, const std::string& fontFile, int size, const sa::tl::Value& fontColor)
{
    Color fc(ctx_, fontColor);
    if (ctx_.HasErrors())
        return;
    DrawText(x, y, text, fontFile, size, fc);
}

void Bitmap::FloodFill(int x, int y, const Color& oldColor, const Color& color)
{
    if (GetPixel(x, y) == oldColor)
    {
        SetPixel(x, y, color);
        FloodFill(x - 1, y, oldColor, color);
        FloodFill(x + 1, y, oldColor, color);
        FloodFill(x, y - 1, oldColor, color);
        FloodFill(x, y + 1, oldColor, color);
        FloodFill(x + 1, y + 1, oldColor, color);
        FloodFill(x - 1, y + 1, oldColor, color);
        FloodFill(x - 1, y - 1, oldColor, color);
        FloodFill(x + 1, y - 1, oldColor, color);
    }
}

void Bitmap::FloodFill(int x, int y, const Color& color)
{
    Color oldCol = GetPixel(x, y);
    FloodFill(x, y, oldCol, color);
}

void Bitmap::BoundaryFill(int x, int y, const Color& bColor, const Color& color)
{
    Color pixel = GetPixel(x, y);
    if (pixel != bColor && pixel != color)
    {
        SetPixel(x, y, color);
        BoundaryFill(x + 1, y, bColor, color);
        BoundaryFill(x, y + 1, bColor, color);
        BoundaryFill(x - 1, y, bColor, color);
        BoundaryFill(x, y - 1, bColor, color);
        BoundaryFill(x - 1, y - 1, bColor, color);
        BoundaryFill(x - 1, y + 1, bColor, color);
        BoundaryFill(x + 1, y - 1, bColor, color);
        BoundaryFill(x + 1, y + 1, bColor, color);
    }
}

void Bitmap::PlotFunction(int x, int y, int from, int to, int thickness, const Color& color, const sa::tl::Callable& callback)
{
    int lastX = x;
    int lastY = y;
    for (int i = from; i <= to; ++i)
    {
        auto arg = sa::tl::MakeValue(i);
        auto v = callback(ctx_, { arg });
        if (v->IsNull())
            // Skip this value
            continue;
        if (v->IsBool() && !v->ToBool())
            // When returning false, stop with it
            break;
        int iVal = (int)v->ToInt();
        DrawLine(lastX, lastY, x + i, y + iVal, thickness, color);
        lastX = x + i;
        lastY = y + iVal;
    }
}

void Bitmap::AlphaBlend(int x, int y, const sa::tl::Value& c, bool keepAlpha)
{
    Color col = Color(ctx_, c);
    if (ctx_.HasErrors())
        return;
    AlphaBlend(x, y, col, keepAlpha);
}

void Bitmap::AlphaBlendBitmap(int x, int y, const sa::tl::Value& b, bool keepAlpha)
{
    const auto* bmp = sa::tl::MetaTable::GetObject<Bitmap>(b);
    if (!bmp)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Bitmap object expected");
        return;
    }
    AlphaBlend(x, y, *bmp, keepAlpha);
}

void Bitmap::AlphaBlendLerp(int x,
    int y,
    const sa::tl::Value& c,
    const sa::tl::Value& lerpColor,
    float lerpFactor,
    bool keepAlpha)
{
    Color col = Color(ctx_, c);
    if (ctx_.HasErrors())
        return;
    Color lerpcol = Color(ctx_, lerpColor);
    if (ctx_.HasErrors())
        return;
    AlphaBlendLerp(x, y, col, lerpcol, lerpFactor, keepAlpha);
}

void Bitmap::AlphaBlendLerpBitmap(int x,
    int y,
    const sa::tl::Value& b,
    const sa::tl::Value& lerpColor,
    float lerpFactor,
    bool keepAlpha)
{
    Color col = Color(ctx_, lerpColor);
    if (ctx_.HasErrors())
        return;
    const auto* bmp = sa::tl::MetaTable::GetObject<Bitmap>(b);
    if (!bmp)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Bitmap object expected");
        return;
    }

    AlphaBlendLerp(x, y, *bmp, col, lerpFactor, keepAlpha);
}

void Bitmap::DrawPoint(int x, int y, int size, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    DrawPoint(x, y, size, col);
}

void Bitmap::DrawLine(int x1, int y1, int x2, int y2, int size, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    DrawLine(x1, y1, x2, y2, size, col);
}

void Bitmap::DrawRectangle(int x1, int y1, int x2, int y2, const sa::tl::Value& color, int thickness)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    DrawRectangle(x1, y1, x2, y2, col, thickness);
}

void Bitmap::FillRectangle(int x1, int y1, int x2, int y2, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    FillRectangle(x1, y1, x2, y2, col);
}

void Bitmap::DrawCircle(int x, int y, int radius, const sa::tl::Value& color, int thickness)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    DrawCircle(x, y, radius, col, thickness);
}

void Bitmap::FillCircle(int x, int y, int radius, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    FillCircle(x, y, radius, col);
}

void Bitmap::DrawEllipse(int cx, int cy, int rx, int ry, const sa::tl::Value& color, int thickness)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    DrawEllipse(cx, cy, rx, ry, col, thickness);
}

void Bitmap::FillEllipse(int cx, int cy, int rx, int ry, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    FillEllipse(cx, cy, rx, ry, col);
}

void Bitmap::FloodFill(int x, int y, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    FloodFill(x, y, col);
}

void Bitmap::BoundaryFill(int x, int y, const sa::tl::Value& bColor, const sa::tl::Value& color)
{
    Color col = Color(ctx_, color);
    if (ctx_.HasErrors())
        return;
    Color bcol = Color(ctx_, bColor);
    if (ctx_.HasErrors())
        return;
    BoundaryFill(x, y, bcol, col);
}

void Bitmap::PlotFunction(int x, int y, int from, int to, int thickness, const sa::tl::Value& color, const sa::tl::Callable& callback)
{
    Color col(ctx_, color);
    if (ctx_.HasErrors())
        return;
    if (callback.ParamCount() != 1)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return;
    }
    PlotFunction(x, y, from, to, thickness, col, callback);
}

bool Bitmap::IsVisible(int x, int y) const
{
    if (!(x >= 0 && y >= 0 && x < width_ && y < height_))  // NOLINT(readability-simplify-boolean-expr)
        return false;
    if (clipType_ == ClipType::None || !clipRegion_)
        return true;
    if (clipType_ == ClipType::Inside)
        return clipRegion_->IsInside(x, y);
    if (clipType_ == ClipType::Outside)
        return clipRegion_->IsOutside(x, y);
    return true;
}

void Bitmap::SetClipRegion(const sa::tl::Value& region)
{
    if (region.IsNull())
    {
        clipRegion_.reset();
        return;
    }
    if (!region.IsTable())
    {
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Expecting null or table");
        return;
    }
    clipRegion_ = std::make_unique<ClipRegion>();
    const auto& tbl = *region.As<sa::tl::TablePtr>();
    sa::tl::GetTableValue(ctx_, tbl, "left", clipRegion_->left_);
    sa::tl::GetTableValue(ctx_, tbl, "top", clipRegion_->top_);
    sa::tl::GetTableValue(ctx_, tbl, "right", clipRegion_->right_);
    sa::tl::GetTableValue(ctx_, tbl, "bottom", clipRegion_->bottom_);
}

sa::tl::Value Bitmap::GetClipRegion() const
{
    if (!clipRegion_)
        return nullptr;
    auto result = sa::tl::MakeTable();
    result->Add("left", clipRegion_->left_);
    result->Add("top", clipRegion_->top_);
    result->Add("right", clipRegion_->right_);
    result->Add("bottom", clipRegion_->bottom_);
    return result;
}

void Bitmap::SetClipType(int value)
{
    if (value < (int)ClipType::None || value > (int)ClipType::Outside)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::InvalidArgument, std::format("Expecting a value between {} and {}", (int)ClipType::None, (int)ClipType::Outside));
        return;
    }
    clipType_ = (ClipType)value;
}

} // namespace gfx

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<gfx::Bitmap>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<gfx::Bitmap, sa::tl::bind::Object>();
    result
        .AddFunction<gfx::Bitmap, bool, const std::string&>("save", &gfx::Bitmap::Save)
        .AddFunction<gfx::Bitmap, bool, int, int>("resize", &gfx::Bitmap::Resize)
        .AddFunction<gfx::Bitmap, void, const sa::tl::Value&>("clear", &gfx::Bitmap::Clear)
        .AddFunction<gfx::Bitmap, void>("invert", &gfx::Bitmap::Invert)
        .AddFunction<gfx::Bitmap, void>("grey_scale", &gfx::Bitmap::GreyScale)
        .AddFunction<gfx::Bitmap, void, float>("tint", &gfx::Bitmap::Tint)
        .AddFunction<gfx::Bitmap, void, float>("shade", &gfx::Bitmap::Shade)
        .AddFunction<gfx::Bitmap, void, const sa::tl::Value&, float>("lerp", &gfx::Bitmap::Lerp)
        .AddFunction<gfx::Bitmap, sa::tl::Value, int, int>("get_pixel", &gfx::Bitmap::GetPixelScript)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&>("set_pixel", &gfx::Bitmap::SetPixel)
        .AddFunction<gfx::Bitmap, void, int, int, int, const std::string&>("set_bitmap", &gfx::Bitmap::SetBitmap)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&, bool>("alpha_blend", &gfx::Bitmap::AlphaBlend)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&, bool>("alpha_blend_bitmap", &gfx::Bitmap::AlphaBlendBitmap)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&, const sa::tl::Value&, float, bool>("alpha_blend_lerp", &gfx::Bitmap::AlphaBlendLerp)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&, const sa::tl::Value&, float, bool>("alpha_blend_lerp_bitmap", &gfx::Bitmap::AlphaBlendLerpBitmap)
        .AddFunction<gfx::Bitmap, void, int, int, int, const sa::tl::Value&>("draw_point", &gfx::Bitmap::DrawPoint)
        .AddFunction<gfx::Bitmap, void, int, int, int, int, int, const sa::tl::Value&>("draw_line", &gfx::Bitmap::DrawLine)
        .AddFunction<gfx::Bitmap, void, int, int, int, int, const sa::tl::Value&, int>("draw_rect", &gfx::Bitmap::DrawRectangle)
        .AddFunction<gfx::Bitmap, void, int, int, int, int, const sa::tl::Value&>("fill_rect", &gfx::Bitmap::FillRectangle)
        .AddFunction<gfx::Bitmap, void, int, int, int, const sa::tl::Value&, int>("draw_circle", &gfx::Bitmap::DrawCircle)
        .AddFunction<gfx::Bitmap, void, int, int, int, const sa::tl::Value&>("fill_circle", &gfx::Bitmap::FillCircle)
        .AddFunction<gfx::Bitmap, void, int, int, int, int, const sa::tl::Value&, int>("draw_ellipse", &gfx::Bitmap::DrawEllipse)
        .AddFunction<gfx::Bitmap, void, int, int, int, int, const sa::tl::Value&>("fill_ellipse", &gfx::Bitmap::FillEllipse)
        .AddFunction<gfx::Bitmap, void, int, int, const std::string&, const std::string&, int, const sa::tl::Value&>("draw_text", &gfx::Bitmap::DrawText)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&>("flood_fill", &gfx::Bitmap::FloodFill)
        .AddFunction<gfx::Bitmap, void, int, int, const sa::tl::Value&, const sa::tl::Value&>("boundary_fill", &gfx::Bitmap::BoundaryFill)
        .AddFunction<gfx::Bitmap, int, const std::string&, const std::string&, int>("get_text_width", &gfx::Bitmap::GetTextWidth)
        .AddFunction<gfx::Bitmap, int, const std::string&, const std::string&, int>("get_text_height", &gfx::Bitmap::GetTextHeight)
        .AddFunction<gfx::Bitmap, void, int, int, int, int, int, const sa::tl::Value&, const sa::tl::Callable&>("plot_function", &gfx::Bitmap::PlotFunction)
        .AddProperty("clip_region", &gfx::Bitmap::GetClipRegion, &gfx::Bitmap::SetClipRegion)
        .AddProperty("clip_type", &gfx::Bitmap::GetClipType, &gfx::Bitmap::SetClipType)
        .AddProperty("width", &gfx::Bitmap::GetWidth)
        .AddProperty("height", &gfx::Bitmap::GetHeight)
        .AddProperty("components", &gfx::Bitmap::GetComponents)
        .AddProperty("data", &gfx::Bitmap::GetDataString);
    return result;
}

}
