project (satl_math CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(satl_math SHARED ${SOURCES})
set_property(TARGET satl_math PROPERTY POSITION_INDEPENDENT_CODE ON)
target_link_libraries(satl_math satl)
install(TARGETS satl_math LIBRARY)
