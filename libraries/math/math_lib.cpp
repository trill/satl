/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <module.h>
#include <cmath>
#include <iomanip>
#include <limits>
#include <numbers>
#include <random>
#include <sstream>
#include <algorithm>

using namespace sa::tl;

template<typename T>
constexpr bool Equals(T lhs, T rhs)
{
    if constexpr (std::is_floating_point<T>::value)
        return lhs + std::numeric_limits<T>::epsilon() >= rhs && lhs - std::numeric_limits<T>::epsilon() <= rhs;
    else
        return lhs == rhs;
}

constexpr float Hue2Rgb(float p, float q, float t)
{
    if (t < 0.0f)
        t += 1.0f;
    if (t > 1.0f)
        t -= 1.0f;
    if (t < 1.0f / 6.0f)
        return p + (q - p) * 6.0f * t;
    if (t < 1.0f / 2.0f)
        return q;
    if (t < 2.0f / 3.0f)
        return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
    return p;
}

constexpr std::tuple<float, float, float> Hsv2Rgb(float h, float s, float v)
{
    float r = 0.0f, g = 0.0f, b = 0.0f;
    int i = static_cast<int>(h * 6.0f);
    const float f = h * 6.0f - (float)i;
    const float p = v * (1.0f - s);
    const float q = v * (1.0f - f * s);
    const float t = v * (1.0f - (1.0f - f) * s);

    switch (i % 6)
    {
    case 0:
        r = v, g = t, b = p;
        break;
    case 1:
        r = q, g = v, b = p;
        break;
    case 2:
        r = p, g = v, b = t;
        break;
    case 3:
        r = p, g = q, b = v;
        break;
    case 4:
        r = t, g = p, b = v;
        break;
    case 5:
        r = v, g = p, b = q;
        break;
    }
    return { r, g, b };
}

constexpr std::tuple<float, float, float> Hsl2Rgb(float h, float s, float l)
{
    float r = 0.0f, g = 0.0f, b = 0.0f;
    if (Equals(s, 0.0f))
        r = g = b = 1;
    else
    {
        const float q = l < 0.5f ? l * (1.0f + s) : l + s - l * s;
        const float p = 2.0f * l - q;
        r = Hue2Rgb(p, q, h + 1.0f / 3.0f);
        g = Hue2Rgb(p, q, h);
        b = Hue2Rgb(p, q, h - 1.0f / 3.0f);
    }
    return { r, g, b };
}

constexpr std::tuple<float, float, float> Rgb2Hsl(float r, float g, float b)
{

    const float min = std::min(r, std::min(g, b));
    const float max = std::max(r, std::max(g, b));
    float h = 0.0f, s = 0.0f, l = (max + min) * 0.5f;
    if (Equals(max, min))
        h = s = 0;
    else
    {
        const float d = max - min;
        s = l > 0.5f ? d / (2.0f - max - min) : d / (max - min);
        if (Equals(max, r))
            h = (g - b) / d + (g < b ? 6.0f : 0.0f);
        else if (Equals(max, g))
            h = (b - r) / d + 2.0f;
        else if (Equals(max, b))
            h = (r - g) / d + 4.0f;

        h /= 6;
    }
    return { h, s, l };
}

constexpr std::tuple<float, float, float> Rgb2Hsv(float r, float g, float b)
{
    const float min = std::min(r, std::min(g, b));
    const float max = std::max(r, std::max(g, b));
    float h = 0.0f, v = max;
    const float d = max - min;
    float s = Equals(max, 0.0f) ? 0.0f : d / max;
    if (Equals(max, min))
        h = 0;
    else
    {
        if (Equals(max, r))
            h = (g - b) / d + (g < b ? 6.0f : 0.0f);
        else if (Equals(max, g))
            h = (b - r) / d + 2.0f;
        else if (Equals(max, b))
            h = (r - g) / d + 4.0f;

        h /= 6;
    }
    return { h, s, v };
}

static std::random_device dev;
static std::mt19937 rng(dev());

static std::tuple<float, float, float> RandomColor(float s = 0.99f, float v = 0.99f)
{
    // https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
    std::uniform_real_distribution<float> uid(0.0f, 1.0f);
    constexpr float golden_ratio_conjugate = 0.618033988749895f;
    static float h = uid(rng);
    h += golden_ratio_conjugate;
    h = fmod(h, 1.0);
    return Hsv2Rgb(h, s, v);
}

static std::tuple<float, float, float> GenerateColor(float s = 0.99f, float v = 0.99f)
{
    // https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
    std::uniform_real_distribution<float> uid(0.0f, 1.0f);
    constexpr float golden_ratio_conjugate = 0.618033988749895f;
    static float h = 0.1f;
    h += golden_ratio_conjugate;
    h = fmod(h, 1.0);
    return Hsv2Rgb(h, s, v);
}

extern "C" bool Init(Context& ctx);
extern "C" bool Init(Context& ctx)
{
    if (ctx.HasValue("math"))
        return true;

    auto module = MakeModule();
    module->AddConst<Float>("E", std::numbers::e_v<Float>);
    module->AddConst<Float>("LOG2E", std::numbers::log2e_v<Float>);
    module->AddConst<Float>("LOG10E", std::numbers::log10e_v<Float>);
    module->AddConst<Float>("PI", std::numbers::pi_v<Float>);
    module->AddConst<Float>("TWO_PI", std::numbers::pi_v<Float> * (Float)2);
    module->AddConst<Float>("PI_HALF", std::numbers::pi_v<Float> / (Float)2);
    module->AddConst<Float>("PI_FOURTH", std::numbers::pi_v<Float> / (Float)4);
    module->AddConst<Float>("INV_PI", std::numbers::inv_pi_v<Float>);
    module->AddConst<Float>("INV_SQRTPI", std::numbers::inv_sqrtpi_v<Float>);
    module->AddConst<Float>("LN2", std::numbers::ln2_v<Float>);
    module->AddConst<Float>("LN10", std::numbers::ln10_v<Float>);
    module->AddConst<Float>("SQRT2", std::numbers::sqrt2_v<Float>);
    module->AddConst<Float>("SQRT3", std::numbers::sqrt3_v<Float>);
    module->AddConst<Float>("INV_SQRT3", std::numbers::inv_sqrt3_v<Float>);
    module->AddConst<Float>("EGAMMA", std::numbers::egamma_v<Float>);
    module->AddConst<Float>("PHI", std::numbers::phi_v<Float>);
    module->AddConst<Float>("INF", std::numeric_limits<Float>::infinity());
    module->AddConst<Float>("DEG_TO_RAD", std::numbers::pi_v<Float> / (Float)180.0);

    module->AddFunction<Value, Value, Value>("min",
        [&ctx](const Value& a, const Value& b) -> Value
        {
            if (a.Lt(ctx, b))
                return a;
            return b;
        });
    module->AddFunction<Value, Value, Value>("max",
        [&ctx](const Value& a, const Value& b) -> Value
        {
            if (a.Gt(ctx, b))
                return a;
            return b;
        });
    module->AddFunction<Value, const Table&>("min_val",
        [&ctx](const Table& values) -> Value
        {
            if (values.empty())
                return {};

            Value result = {};
            bool isfirst = true;
            for (const auto& v : values)
            {
                if (isfirst)
                {
                    isfirst = false;
                    result = *v.second;
                    continue;
                }
                if (result.Lt(ctx, *v.second))
                    result = *v.second;
            }
            return result;
        });
    module->AddFunction<Value, const Table&>("max_val",
        [&ctx](const Table& values) -> Value
        {
            if (values.empty())
                return {};

            Value result = {};
            bool isfirst = true;
            for (const auto& v : values)
            {
                if (isfirst)
                {
                    isfirst = false;
                    result = *v.second;
                    continue;
                }
                if (result.Gt(ctx, *v.second))
                    result = *v.second;
            }
            return result;
        });
    module->AddFunction<Value, const Table&>("sum",
        [&ctx](const Table& values) -> Value
        {
            if (values.empty())
                return {};

            Value result = {};
            bool isfirst = true;
            for (const auto& v : values)
            {
                if (isfirst)
                {
                    isfirst = false;
                    result = *v.second;
                    continue;
                }
                result.AssignAdd(ctx, *v.second);
            }
            return result;
        });
    module->AddFunction<bool, Integer>("isprime",
        [](Integer value) -> bool
        {
            if (value <= 1)
                return false;
            for (Integer i = 2; i <= (Integer)sqrt((double)value); ++i)
            {
                if (value % i == 0)
                    return false;
            }
            return true;
        });
    module->AddFunction<Float, Float>("sin", [](Float a) -> Float { return std::sin(a); });
    module->AddFunction<Float, Float>("sinh", [](const Float& a) -> Float { return std::sinh(a); });
    module->AddFunction<Float, Float>("cos", [](Float a) -> Float { return std::cos(a); });
    module->AddFunction<Float, Float>("cosh", [](Float a) -> Float { return std::cosh(a); });
    module->AddFunction<Float, Float>("tan", [](Float a) -> Float { return std::tan(a); });
    module->AddFunction<Float, Float>("tanh", [](Float a) -> Float { return std::tanh(a); });
    module->AddFunction<Float, Float>("atan", [](const Float& a) -> Float { return std::atan(a); });
    module->AddFunction<Float, Float, Float>("atan2", [](Float a, Float b) -> Float { return std::atan2(a, b); });
    module->AddFunction<Float, Float>("log", [](Float a) -> Float { return std::log(a); });
    module->AddFunction<Float, Float>("log10", [](Float a) -> Float { return std::log10(a); });
    module->AddFunction<Value, Value, Value>("pow",
        [](const Value& a, const Value& b) -> Value
        {
            if (a.IsInt())
                return static_cast<Integer>(std::pow(a.As<Integer>(), b.ToInt()));
            return static_cast<Float>(std::pow(a.ToFloat(), b.ToFloat()));
        });
    module->AddFunction<Value, Value>("abs",
        [](const Value& a) -> Value
        {
            if (a.IsInt())
                return static_cast<Integer>(std::abs(a.As<Integer>()));
            return static_cast<Float>(std::abs(a.ToFloat()));
        });
    module->AddFunction<Float, Float>("ceil", [](Float a) -> Float { return std::ceil(a); });
    module->AddFunction<Float, Float>("floor", [](Float a) -> Float { return std::floor(a); });
    module->AddFunction<Float, Float>("sqrt", [](Float a) -> Float { return std::sqrt(a); });
    module->AddFunction<Float, Value>("exp", [](const Value& a) -> Float { return std::exp(a.ToFloat()); });
    module->AddFunction<bool, Value>("isnan",
        [](const Value& value) -> bool
        {
            if (value.IsInt())
                return std::isnan(value.ToInt());
            if (value.IsFloat())
                return std::isnan(value.ToFloat());
            return false;
        });
    module->AddFunction<bool, Value>("isinf",
        [](const Value& value) -> bool
        {
            if (value.IsInt())
                return std::isinf(value.ToInt());
            if (value.IsFloat())
                return std::isinf(value.ToFloat());
            return false;
        });
    module->AddFunction<bool, Value>("isnormal",
        [](const Value& value) -> bool
        {
            if (value.IsInt())
                return std::isnormal(value.ToInt());
            if (value.IsFloat())
                return std::isnormal(value.ToFloat());
            return false;
        });
    module->AddFunction<Value, Value, Value, Value>("clamp",
        [&ctx](const Value& v, const Value& min, const Value& max) -> Value
        {
            if (v.Lt(ctx, min))
                return min;
            if (v.Gt(ctx, max))
                return max;
            return v;
        });
    module->AddFunction<Value, Value, Value, Value>("lerp",
        [&ctx](const Value& lhs, const Value& rhs, const Value& i) -> Value
        { return lhs.Mul(ctx, i.Sub(ctx, Value((Float)1.0))).Add(ctx, rhs.Mul(ctx, i)); });
    module->AddFunction<Value, Value>("deg_to_rad",
        [&ctx](const Value& v) -> Value
        {
            static const Value factor = std::numbers::pi_v<Float> / (Float)180.0;
            return v.Mul(ctx, factor);
        });
    module->AddFunction<Value, Value>("rad_to_deg",
        [&ctx](const Value& v) -> Value
        { return v.Div(ctx, Value(std::numbers::pi_v<Float>)).Mul(ctx, Value((Float)180.0)); });
    module->AddFunction<TablePtr, int, int, int>("hsv_to_rgb",
        [](float h, float s, float v) -> TablePtr
        {
            TablePtr result = MakeTable();
            auto [r, g, b] = Hsv2Rgb(h, s, v);
            result->Add<Float>("r", r);
            result->Add<Float>("g", g);
            result->Add<Float>("b", b);
            return result;
        });
    module->AddFunction<TablePtr, int, int, int>("hsl_to_rgb",
        [](float h, float s, float l) -> TablePtr
        {
            TablePtr result = MakeTable();
            auto [r, g, b] = Hsl2Rgb(h, s, l);
            result->Add<Float>("r", r);
            result->Add<Float>("g", g);
            result->Add<Float>("b", b);
            return result;
        });
    module->AddFunction<TablePtr, int, int, int>("rgb_to_hsl",
        [](float r, float g, float b) -> TablePtr
        {
            TablePtr result = MakeTable();
            auto [h, s, l] = Rgb2Hsl(r, g, b);
            result->Add<Float>("h", h);
            result->Add<Float>("s", s);
            result->Add<Float>("l", l);
            return result;
        });
    module->AddFunction<TablePtr, int, int, int>("rgb_to_hsv",
        [](float r, float g, float b) -> TablePtr
        {
            TablePtr result = MakeTable();
            auto [h, s, v] = Rgb2Hsv(r, g, b);
            result->Add<Float>("h", h);
            result->Add<Float>("s", s);
            result->Add<Float>("v", v);
            return result;
        });
    module->AddFunction<TablePtr, uint32_t>("value_to_rgba",
        [](uint32_t value) -> TablePtr
        {
            TablePtr result = MakeTable();
            const float a = (float)((value >> 24u) & 0xffu) / 255.0f;
            const float r = (float)((value >> 16u) & 0xffu) / 255.0f;
            const float g = (float)((value >> 8u) & 0xffu) / 255.0f;
            const float b = (float)((value >> 0u) & 0xffu) / 255.0f;
            result->Add<Float>("r", r);
            result->Add<Float>("g", g);
            result->Add<Float>("b", b);
            result->Add<Float>("a", a);
            return result;
        });
    module->AddFunction<TablePtr, uint32_t>("value_to_rgb",
        [](uint32_t value) -> TablePtr
        {
            TablePtr result = MakeTable();
            const float r = (float)((value >> 16u) & 0xffu) / 255.0f;
            const float g = (float)((value >> 8u) & 0xffu) / 255.0f;
            const float b = (float)((value >> 0u) & 0xffu) / 255.0f;
            result->Add<Float>("r", r);
            result->Add<Float>("g", g);
            result->Add<Float>("b", b);
            return result;
        });
    module->AddFunction<uint32_t, Float, Float, Float, Float>("rgba_to_value",
        [](Float r, Float g, Float b, Float a) -> uint32_t
        {
            uint8_t ir = static_cast<uint8_t>(std::clamp(r, 0.0f, 1.0f) * 255.0f);
            uint8_t ig = static_cast<uint8_t>(std::clamp(g, 0.0f, 1.0f) * 255.0f);
            uint8_t ib = static_cast<uint8_t>(std::clamp(b, 0.0f, 1.0f) * 255.0f);
            uint8_t ia = static_cast<uint8_t>(std::clamp(a, 0.0f, 1.0f) * 255.0f);

            return (ia << 24u) | (ir << 16u) | (ig << 8u) | ib;
        });
    module->AddFunction<uint32_t, Float, Float, Float>("rgb_to_value",
        [](Float r, Float g, Float b) -> uint32_t
        {
            uint8_t ir = static_cast<uint8_t>(std::clamp(r, 0.0f, 1.0f) * 255.0f);
            uint8_t ig = static_cast<uint8_t>(std::clamp(g, 0.0f, 1.0f) * 255.0f);
            uint8_t ib = static_cast<uint8_t>(std::clamp(b, 0.0f, 1.0f) * 255.0f);

            return (ir << 16u) | (ig << 8u) | ib;
        });
    module->AddFunction("rand",
        [](sa::tl::Context& ctx, const std::vector<sa::tl::ValuePtr>& args) -> sa::tl::ValuePtr
        {
            /*
             * rand()
             * rand(max)
             * rand(min, max)
             **/
            auto genFloatRnd = [](float min = 0.0f, float max = 1.0f) -> float
            {
                std::uniform_real_distribution<Float> uid(min, max);
                return uid(rng);
            };

            if (args.empty())
            {
                return MakeValue<Float>(genFloatRnd());
            }

            if (!args[0]->IsFloat() && !args[0]->IsInt())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Number expected");
                return MakeNull();
            }

            const float arg1 = args[0]->ToFloat();
            const float result = args.size() > 1 ? genFloatRnd(arg1, args[1]->ToFloat()) : genFloatRnd(0.0f, arg1);
            if (args[0]->IsFloat())
                return MakeValue<Float>(result);
            return MakeValue<Integer>(static_cast<Integer>(result));
        });
    module->AddFunction("random_color",
        [](sa::tl::Context& ctx, const std::vector<sa::tl::ValuePtr>& args) -> sa::tl::ValuePtr
        {
            /*
             * random_color()
             * random_color(s)
             * random_color(s, v)
             **/
            float s = 0.99f;
            float v = 0.99f;

            if (!args.empty())
            {
                if (!args[0]->IsFloat() && !args[0]->IsInt())
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Number expected");
                    return MakeNull();
                }
                s = args[0]->ToFloat();
            }
            if (args.size() > 1)
            {
                if (!args[1]->IsFloat() && !args[1]->IsInt())
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Number expected");
                    return MakeNull();
                }
                s = args[1]->ToFloat();
            }

            auto [r, g, b] = RandomColor(s, v);
            TablePtr result = MakeTable();
            result->Add<Float>("r", r);
            result->Add<Float>("g", g);
            result->Add<Float>("b", b);
            return MakeValue(result);
        });
    module->AddFunction("generate_color",
        [](sa::tl::Context& ctx, const std::vector<sa::tl::ValuePtr>& args) -> sa::tl::ValuePtr
        {
            /*
             * generate_color()
             * generate_color(s)
             * generate_color(s, v)
             **/
            float s = 0.99f;
            float v = 0.99f;

            if (!args.empty())
            {
                if (!args[0]->IsFloat() && !args[0]->IsInt())
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Number expected");
                    return MakeNull();
                }
                s = args[0]->ToFloat();
            }
            if (args.size() > 1)
            {
                if (!args[1]->IsFloat() && !args[1]->IsInt())
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Number expected");
                    return MakeNull();
                }
                s = args[1]->ToFloat();
            }

            auto [r, g, b] = GenerateColor(s, v);
            TablePtr result = MakeTable();
            result->Add<Float>("r", r);
            result->Add<Float>("g", g);
            result->Add<Float>("b", b);
            return MakeValue(result);
        });
    module->AddFunction<std::string>("random_uuid",
        []() -> std::string
        {
            uint8_t bytes[16] = {};
            std::uniform_int_distribution<uint8_t> uid(0, 255);
            for (size_t i = 0; i < 16; ++i)
            {
                bytes[i] = uid(rng);
            }
            bytes[6] &= ~(1u << 7u);
            bytes[6] |= 1u << 6u;
            bytes[6] &= ~(1u << 5u);
            bytes[6] &= ~(1u << 4u);
            bytes[8] |= 1u << 7u;
            bytes[8] &= ~(1u << 6u);

            std::stringstream result;
            result << std::hex << std::setfill('0')
                   << std::setw(2) << static_cast<int>(bytes[0])
                   << std::setw(2) << static_cast<int>(bytes[1])
                   << std::setw(2) << static_cast<int>(bytes[2])
                   << std::setw(2) << static_cast<int>(bytes[3]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[4])
                   << std::setw(2) << static_cast<int>(bytes[5]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[6])
                   << std::setw(2) << static_cast<int>(bytes[7]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[8])
                   << std::setw(2) << static_cast<int>(bytes[9]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[10])
                   << std::setw(2) << static_cast<int>(bytes[11])
                   << std::setw(2) << static_cast<int>(bytes[12])
                   << std::setw(2) << static_cast<int>(bytes[13])
                   << std::setw(2) << static_cast<int>(bytes[14])
                   << std::setw(2) << static_cast<int>(bytes[15]);
            return result.str();
        });
    module->AddFunction<std::string>("random_ulid",
        []() -> std::string
        {
            uint8_t bytes[16] = {};
            // 0..5 Time
            auto timestamp = std::time(nullptr);
            bytes[0] = static_cast<uint8_t>(timestamp >> 40);
            bytes[1] = static_cast<uint8_t>(timestamp >> 32);
            bytes[2] = static_cast<uint8_t>(timestamp >> 24);
            bytes[3] = static_cast<uint8_t>(timestamp >> 16);
            bytes[4] = static_cast<uint8_t>(timestamp >> 8);
            bytes[5] = static_cast<uint8_t>(timestamp);

            // 6..15 Random
            std::uniform_int_distribution<uint8_t> uid(0, 255);
            for (size_t i = 6; i < 16; ++i)
            {
                bytes[i] = uid(rng);
            }

            std::stringstream result;
            result << std::hex << std::setfill('0')
                   << std::setw(2) << static_cast<int>(bytes[0])
                   << std::setw(2) << static_cast<int>(bytes[1])
                   << std::setw(2) << static_cast<int>(bytes[2])
                   << std::setw(2) << static_cast<int>(bytes[3]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[4])
                   << std::setw(2) << static_cast<int>(bytes[5]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[6])
                   << std::setw(2) << static_cast<int>(bytes[7]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[8])
                   << std::setw(2) << static_cast<int>(bytes[9]) << "-";
            result << std::setw(2) << static_cast<int>(bytes[10])
                   << std::setw(2) << static_cast<int>(bytes[11])
                   << std::setw(2) << static_cast<int>(bytes[12])
                   << std::setw(2) << static_cast<int>(bytes[13])
                   << std::setw(2) << static_cast<int>(bytes[14])
                   << std::setw(2) << static_cast<int>(bytes[15]);
            return result.str();
        });
    module->AddFunction<void, const Value&>("random_shuffle",
        [&ctx](const Value& value) -> void
        {
            if (value.IsTable())
            {
                const auto& table = value.As<TablePtr>();
                std::vector<ValuePtr> arr;
                arr.reserve(table->size());
                for (auto& item : *table)
                {
                    arr.push_back(item.second);
                }
                std::shuffle(arr.begin(), arr.end(), rng);
                table->clear();
                for (size_t i = 0; i < arr.size(); ++i)
                    table->Add((Integer)i, arr[i]);
                return;
            }
            if (value.IsString())
            {
                auto& str = *value.As<StringPtr>();
                std::shuffle(str.begin(), str.end(), rng);
                return;
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Table or string expected");
        });
    module->AddFunction<void, const Value&>("random_choice",
        [&ctx](const Value& value) -> ValuePtr
        {
            if (value.IsTable())
            {
                const auto& table = value.As<TablePtr>();
                if (table->empty())
                    return MakeNull();
                if (!table->IsArray())
                {
                    ctx.RuntimeError(Error::Code::InvalidArgument, "Array expected");
                    return MakeNull();
                }
                std::uniform_int_distribution<Integer> uid(0, table->size());
                Integer key = uid(rng);
                return table->at(key);
            }
            if (value.IsString())
            {
                auto& str = *value.As<StringPtr>();
                std::uniform_int_distribution<Integer> uid(0, str.length());
                Integer key = uid(rng);
                return MakeValue<Integer>(str.at(key));
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Table or string expected");
            return MakeNull();
        });
    module->AddFunction<Value, TablePtr>("mean",
        [&ctx](const TablePtr& v) -> Value
        {
            if (v->empty())
                return 0;
            // Use the first value to get the type
            Value result = *v->begin()->second;
            for (const auto& i : *v)
            {
                if (i == *v->begin())
                    continue;
                result.AssignAdd(ctx, *i.second);
                if (ctx.HasErrors())
                    return {};
            }
            result.AssignDiv(ctx, Value((Integer)v->size()));
            return result;
        });
    module->AddFunction<Value, TablePtr>("geom_mean",
        [&ctx](const TablePtr& v) -> Value
        {
            if (v->empty())
                return 0;
            // Use the first value to get the type
            Value result = *v->begin()->second;
            for (const auto& i : *v)
            {
                if (i == *v->begin())
                    continue;
                result.AssignMul(ctx, *i.second);
                if (ctx.HasErrors())
                    return {};
            }
            result.AssignPow(ctx, Value((Float)(1.0 / (Float)v->size())));
            return result;
        });
    module->AddFunction<Value, TablePtr>("median",
        [&ctx](const TablePtr& v) -> Value
        {
            if (v->empty())
                return 0;
            std::vector<ValuePtr> arr;
            arr.reserve(v->size());
            for (const auto& i : *v)
            {
                arr.push_back(i.second);
            }
            std::sort(arr.begin(), arr.end(), [&](const ValuePtr& a, const ValuePtr& b) { return a->Lt(ctx, *b); });
            if (arr.size() % 2 == 0)
            {
                const auto& v1 = arr[arr.size() / 2 - 1];
                const auto& v2 = arr[arr.size() / 2];
                Value result = v1->Add(ctx, *v2);
                if (ctx.HasErrors())
                    return {};
                result.AssignDiv(ctx, 2);
                return result;
            }
            return *arr[arr.size() / 2];
        });
    module->AddFunction<TablePtr, Value, Value, Value>("linespace",
        [&ctx](const Value& start, const Value& end, const Value& count) -> TablePtr
        {
            if (!count.IsInt())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "count must be an integer type");
                return {};
            }
            if (!start.IsInt() && !start.IsFloat() && !end.IsInt() && !end.IsFloat())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Type must be int or float");
                return {};
            }
            if (start.GetType() != end.GetType())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "start and end must be the same type");
                return {};
            }

            auto result = MakeTable();
            auto generate = [&result]<typename T>(T first, T last, Integer count) -> void
            {
                int index = 0;
                T increment = (last - first) / (T)count;
                for (T i = first; i < last && index < count; i += increment)
                    result->emplace(index++, MakeValue<T>(i));
            };
            if (start.IsInt())
                generate(start.ToInt(), end.ToInt(), count.ToInt());
            else
                generate(start.ToFloat(), end.ToFloat(), count.ToInt());
            return result;
        });
    module->AddFunction<TablePtr, Value, Value, Value>("randomspace",
        [&ctx](const Value& start, const Value& end, const Value& count) -> TablePtr
        {
            if (!count.IsInt())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "count must be an integer type");
                return {};
            }
            if (!start.IsInt() && !start.IsFloat() && !end.IsInt() && !end.IsFloat())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Type must be int or float");
                return {};
            }
            if (start.GetType() != end.GetType())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "start and end must be the same type");
                return {};
            }

            auto genRnd = []<typename T>(T min, T max) -> T
            {
                if constexpr (std::is_integral_v<T>)
                {
                    std::uniform_int_distribution<T> uid(min, max);
                    return uid(rng);
                }
                else
                {
                    std::uniform_real_distribution<T> uid(min, max);
                    return uid(rng);
                }
            };

            auto result = MakeTable();
            auto generate = [&]<typename T>(T first, T last, Integer count) -> void
            {
                for (Integer i = 0; i < count; ++i)
                    result->emplace(i, MakeValue<T>(genRnd(first, last)));
            };
            if (start.IsInt())
                generate(start.ToInt(), end.ToInt(), count.ToInt());
            else
                generate(start.ToFloat(), end.ToFloat(), count.ToInt());
            return result;
        });
    module->AddFunction<TablePtr, Integer, Integer>("divmod",
        [](Integer x, Integer y) -> TablePtr
        {
            auto r = std::div(x, y);
            auto result = MakeTable();
            result->Add("quot", r.quot);
            result->Add("rem", r.rem);
            return result;
        });

    ctx.AddConst("math", std::move(module), SCOPE_LIBRARY);

    return true;
}
