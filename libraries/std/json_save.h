/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <string>
#include <json.h>

sa::json::JSON JsonSave(const sa::tl::Table& tbl);
std::string TableToJsonString(const sa::tl::TablePtr& tbl);
