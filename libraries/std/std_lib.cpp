/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "std_lib.h"
#include <ast.h>
#include <context.h>
#include <module.h>
#include <parser.h>
#include <utils.h>
#include <algorithm>
#include <chrono>
#include <bit>
#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>
#include <limits>
#include <sstream>
#include <version.h>
#include "base64.h"
#include "format.h"
#include <number_parser.h>
#include "std_utils.h"
#include <template_parser.h>
#include "utf8_lib.h"
#include "json_lib.h"
#include "value.h"
#include "bits.h"

namespace sa::tl {

namespace {
template<typename T>
constexpr T Sign(T value)
{
    if (value > 0)
        return 1;
    if (value < 0)
        return -1;
    return 0;
}
}

void AddStdLib(Context& ctx)  // NOLINT(readability-function-size)
{
    if (ctx.HasValue("std"))
        return;
    auto stdModule = MakeModule();

    stdModule->AddConst<Integer>("VERSION", SATL_VERSION);
    stdModule->AddConst<Integer>("VERSION_MAJOR", SATL_VERSION_MAJOR);
    stdModule->AddConst<Integer>("VERSION_MINOR", SATL_VERSION_MINOR);
    stdModule->AddConst<std::string>("VERSION_STRING", SATL_VERSION_STRING);
#ifdef NDEBUG
    stdModule->AddConst("VERSION_DEBUG", false);
#else
    stdModule->AddConst("VERSION_DEBUG", true);
#endif
    // Limits
    stdModule->AddConst<Integer>("MAX_INT", std::numeric_limits<Integer>::max());
    stdModule->AddConst<Float>("MAX_FLOAT", std::numeric_limits<Float>::max());
    stdModule->AddFunction<Integer, Value>("sizeof",
        [](const Value& value) -> Integer
        {
            if (value.IsInt())
                return (Integer)sizeof(Integer);
            if (value.IsFloat())
                return (Integer)sizeof(Float);
            if (value.IsString())
                return (Integer)value.ToString().length();
            return 0;
        });
    stdModule->AddFunction<Integer, Value>("to_int",
        [](const Value& value) -> Integer
        {
            // Cast to int
            return value.ToInt();
        });
    stdModule->AddFunction<Float, Value>("to_float",
        [](const Value& value) -> Float
        {
            // Cast to float
            return value.ToFloat();
        });
    stdModule->AddFunction<bool, Value>("to_bool",
        [](const Value& value) -> bool
        {
            // Cast to bool
            return value.ToBool();
        });
    stdModule->AddFunction<std::string, Value>("to_string",
        [](const Value& value) -> std::string
        {
            // Cast to string
            return value.ToString();
        });
    stdModule->AddFunction<TablePtr, Value>("to_table",
        [](const Value& value) -> TablePtr
        {
            // Create a table of anything
            return value.ToTable();
        });
    stdModule->AddFunction<Value, int, Value>("to_type",
        [&ctx](int type, const Value& value) -> Value
        {
            switch (type)
            {
            case INDEX_NULL:
                return {};
            case INDEX_BOOL:
                return value.ToBool();
            case INDEX_INT:
                return value.ToInt();
            case INDEX_FLOAT:
                return value.ToFloat();
            case INDEX_STRING:
                return MakeString(value.ToString());
            case INDEX_TABLE:
                return value.ToTable();
            case INDEX_FUNCTION:
                if (value.IsCallable())
                    return value;
                [[fallthrough]];
            case INDEX_OBJECT:
                if (value.IsObject())
                    return value;
                [[fallthrough]];
            default:
                ctx.RuntimeError(Error::Code::InvalidArgument, std::format("Invalid type {}", type));
                return {};
            }
        });
    stdModule->AddFunction<bool, Value>("is_const", [](const Value& value) -> bool { return value.IsConst(); });
    stdModule->AddFunction<bool, Value>(
        "is_number", [](const Value& value) -> bool { return value.IsInt() || value.IsFloat(); });
    stdModule->AddFunction<bool, Value, Value>("has_key",
        [&ctx](const Value& value, const Value& key) -> bool
        {
            if (key.IsNull())
                return false;
            if (value.IsTable() || value.IsString() || value.IsObject())
                return value.HasSubscript(key.ToKey(ctx), false);
            return false;
        });
    stdModule->AddFunction<bool, const Value&, const Value&>("contains",
        [](const Value& haystack, const Value& needle) -> bool
        {
            if (haystack.IsTable())
            {
                const auto& table = *haystack.As<TablePtr>();
                auto it = std::find_if(table.begin(),
                    table.end(),
                    [&](const auto& current)
                    {
                        // Don't use the Equals() function that throws errors
                        return current.second->Equals(needle);
                    });
                return it != table.end();
            }
            if (haystack.IsString())
            {
                const auto& string = *haystack.As<StringPtr>();
                std::string find = needle.ToString();
                if (find.empty())
                    return false;
                return string.find(find) != std::string::npos;
            }
            return false;
        });
    stdModule->AddFunction<bool, const Value&>("empty",
        [](const Value& value) -> bool
        {
            if (value.IsNull())
                return true;
            if (value.IsString())
                return value.As<StringPtr>()->empty();
            if (value.IsTable())
                return value.As<TablePtr>()->empty();
            if (value.IsObject())
                return value.As<ObjectPtr>()->GetInstance() == nullptr;
            return false;
        });
    stdModule->AddFunction<std::string, int>("chr",
        [&ctx](int value) -> std::string
        {
            if (value < 0 || value > 255)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Value must be between 0 and 255");
                return {};
            }
            return { (char)value };
        });
    stdModule->AddFunction<int, std::string>("ord",
        [&ctx](const std::string& value) -> int
        {
            if (value.length() != 1)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "String must be exactly one character long");
                return {};
            }
            return (int)value[0];
        });
    stdModule->AddFunction<TablePtr, Integer, Value>("new_array",
        [](Integer size, const Value& value) -> TablePtr
        {
            auto result = MakeTable();
            for (Integer i = 0; i < size; ++i)
            {
                result->Add(i, value);
            }
            return result;
        });
    stdModule->AddFunction<Value, std::string>("parse_number",
        [&ctx](const std::string& str) -> Value
        {
            auto value = ParseNumberString(str, [&](std::string msg) {
                ctx.RuntimeError(Error::Code::InvalidArgument, std::move(msg));
            }, true);
            if (value.empty())
                return {};
            return NumberStringToValue(value, [&](std::string msg) {
                ctx.RuntimeError(Error::Code::InvalidArgument, std::move(msg));
            }, true);
        });
    stdModule->AddFunction<Integer, std::string, Integer>("stoi",
        [&ctx](const std::string& str, Integer base) -> Integer
        {
            try
            {
                return std::stoll(str, nullptr, base);
            }
            catch (const std::out_of_range& ex)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Out of range");
                return 0;
            }
            catch (const std::invalid_argument& ex)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, ex.what());
                return 0;
            }
        });
    stdModule->AddFunction<void, const Value&>("clear",
        [&ctx](const Value& value) -> void
        {
            if (value.IsTable())
            {
                const auto& table = value.As<TablePtr>();
                table->Clear();
                return;
            }
            if (value.IsString())
            {
                const auto& str = value.As<StringPtr>();
                str->clear();
                return;
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Not a table or string");
        });
    stdModule->AddFunction<void, const Value&, const Value&>("delete",
        // Delete an item from a table or string by key
        [&ctx](const Value& value, const Value& key) -> void
        {
            if (value.IsTable())
            {
                const auto& table = value.As<TablePtr>();
                table->Delete(key.ToKey(ctx));
                return;
            }
            if (value.IsString())
            {
                const auto& str = value.As<StringPtr>();
                auto index = key.ToKey(ctx);
                if (index.index() != KEY_INT)
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Index is not an integer type");
                    return;
                }
                str->erase(std::get<KEY_INT>(index), 1);
                return;
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Not a table or string");
        });
    stdModule->AddFunction<void, TablePtr, const Callable&>(
        "delete_if", [&ctx](const TablePtr &table, const Callable &callback) -> void {
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return;
            }
            table->DeleteIf(
                [&](const auto& current)
                {
                    if (ctx.stop_)
                        return false;
                    auto res = callback(ctx, { MakeValue(current.first), MakeValue(*current.second) });
                    if (res)
                        return res->ToBool();
                    return false;
                });
        });
    stdModule->AddFunction<Value, Value, const Callable&>("find_if",
        [&ctx](const Value& subject, const Callable& callback) -> Value
        {
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return {};
            }
            if (subject.IsTable())
            {
                for (const auto& item : *subject.As<TablePtr>())
                {
                    if (ctx.stop_)
                        return {};
                    auto res = callback(ctx, { MakeValue(item.first), MakeValue(*item.second) });
                    if (res && res->ToBool())
                        return KeyToValue(item.first);
                }
                return {};
            }
            if (subject.IsString())
            {
                Integer result = 0;
                const std::string& str = *subject.As<StringPtr>();
                for (auto c : str)
                {
                    if (ctx.stop_)
                        return {};
                    auto res = callback(ctx, {MakeValue(result), MakeValue(std::string(1, c)) });
                    if (res && res->ToBool())
                        return result;
                    ++result;
                }
                return {};
            }

            ctx.RuntimeError(Error::Code::TypeMismatch, "String or table expected");
            return {};
        });
    stdModule->AddFunction<Value, Value, const Callable&>("rfind_if",
        [&ctx](const Value& subject, const Callable& callback) -> Value
        {
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return {};
            }
            if (subject.IsTable())
            {
                const auto& tbl = *subject.As<TablePtr>();
                for (auto it = tbl.rbegin(); it != tbl.rend(); ++it)  // NOLINT(modernize-loop-convert)
                {
                    if (ctx.stop_)
                        return {};
                    auto res = callback(ctx, { MakeValue(it->first), MakeValue(*it->second) });
                    if (res && res->ToBool())
                        return KeyToValue(it->first);
                }
                return {};
            }
            if (subject.IsString())
            {
                const std::string& str = *subject.As<StringPtr>();
                for (Integer index = (Integer)str.length() - 1; index >= 0; --index)
                {
                    if (ctx.stop_)
                        return {};
                    auto res = callback(ctx, { MakeValue(index), MakeValue(std::string(1, str[index])) });
                    if (res && res->ToBool())
                        return index;
                }
                return {};
            }

            ctx.RuntimeError(Error::Code::TypeMismatch, "String or table expected");
            return {};
        });
    stdModule->AddFunction<TablePtr, TablePtr, const Callable&>("group_by",
        [&ctx](const TablePtr& table, const Callable& callback) -> TablePtr
        {
            auto result = MakeTable();
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return result;
            }

            for (const auto& item : *table)
            {
                if (ctx.stop_)
                    return {};
                auto res = callback(ctx, { MakeValue(item.first), item.second });
                if (!res)
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Callback function must return a value");
                    return MakeTable();
                }
                auto key = res->ToKey(ctx);
                if (ctx.stop_)
                    return MakeTable();
                TablePtr tbl = {};
                if (!result->contains(key))
                {
                    tbl = MakeTable();
                    result->Add(key, tbl);
                }
                else
                    tbl = result->at(key)->As<TablePtr>();

                tbl->Add((Integer)tbl->size(), item.second);
            }

            return result;
        });
    stdModule->AddFunction<TablePtr, TablePtr, const Callable&>("select_if",
        [&ctx](const TablePtr& table, const Callable& callback) -> TablePtr
        {
            auto result = MakeTable();
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return result;
            }

            for (const auto& item : *table)
            {
                if (ctx.stop_)
                    return {};
                auto res = callback(ctx, { MakeValue(item.first), item.second });
                if (!res)
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Callback function must return a value");
                    return MakeTable();
                }
                if (res->ToBool())
                {
                    result->Add(item.first, item.second);
                }
            }

            return result;
        });
    stdModule->AddFunction<int, TablePtr, Value>("count_of",
        [&ctx](const TablePtr& table, const Value& value) -> int
        {
            if (!value.IsCallable())
                return table->CountOf(value);

            const Callable& callback = *value.As<CallablePtr>();
            if (callback.ParamCount() != 1)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return 0;
            }
            int result = 0;
            for (const auto& v : *table)
            {
                auto res = callback(ctx, { v.second });
                if (res->ToBool())
                    ++result;
            }
            return result;
        });
    stdModule->AddFunction<void, TablePtr>("unique",
        [](const TablePtr& table)
        {
            table->DeleteIf([&](const auto& current) { return table->CountOf(*current.second) > 1; });
        });
    stdModule->AddFunction<void, TablePtr, const Callable&>("for_each",
        [&ctx](const TablePtr& table, const Callable& callback) -> void
        {
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return;
            }

            struct Iterator
            {
                static bool IterateTable(Context& ctx, const TablePtr& table, const Callable& func)
                {
                    for (const auto& item : *table)
                        if (!Iterate(ctx, item.first, item.second, func))
                            return false;
                    return true;
                }
                static bool Iterate(Context& ctx, const KeyType& key, const ValuePtr& value, const Callable& func)
                {
                    if (value->Is<TablePtr>())
                        return IterateTable(ctx, value->As<TablePtr>(), func);
                    if (ctx.stop_)
                        return false;
                    auto res = func(ctx, { MakeValue(key), MakeValue(*value) });
                    if (res)
                        return res->ToBool();
                    return false;
                }
            };

            Iterator::IterateTable(ctx, table, callback);
        });
    stdModule->AddFunction<std::string, std::string, int, std::string>("left_pad",
        [&ctx](const std::string& value, int length, const std::string& fill) -> std::string
        {
            if (fill.empty())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Fill character can't be empty");
                return "";
            }
            if (value.size() < (size_t)length)
                return std::string(length - value.size(), fill[0]) + value;
            return value;
        });
    stdModule->AddFunction<std::string, std::string, int, std::string>("right_pad",
        [&ctx](const std::string& value, int length, const std::string& fill) -> std::string
        {
            if (fill.empty())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Fill character can't be empty");
                return "";
            }
            if (value.size() < (size_t)length)
                return value + std::string(length - value.size(), fill[0]);
            return value;
        });
    stdModule->AddFunction<std::string, std::string, int>("expand_tabs",
        [&](const std::string& value, int tabsize) -> std::string
        {
            auto result = value;
            ReplaceSubstring<char>(result, std::string("\t"), std::string(tabsize, ' '));
            return result;
        });
    stdModule->AddFunction<bool, std::string, std::string>("starts_with",
        [&](const std::string& value, const std::string& search) -> bool
        {
            return value.starts_with(search);
        });
    stdModule->AddFunction<bool, std::string, std::string>("ends_with",
        [&](const std::string& value, const std::string& search) -> bool
        {
            return value.ends_with(search);
        });
    stdModule->AddFunction("find",
        // Find the key of a value in a table or string
        // find(heystack, needle[, start]) -> key;
        // Return null if not found
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            const auto& heystack = args[0];
            const auto& needle = args[1];
            std::optional<KeyType> start;
            if (args.size() > 2)
            {
                start = args[2]->ToKey(ctx);
                if (ctx.HasErrors())
                    return MakeNull();
            }
            if (heystack->IsTable())
            {
                const auto table = heystack->ToTable();
                bool reachedStart = !start.has_value();
                for (const auto& item : (*table))
                {
                    if (!reachedStart)
                    {
                        if (item.first == start.value())
                            reachedStart = true;
                        else
                            continue;
                    }
                    if (item.second->Equals(ctx, *needle))
                        return MakeValue(item.first);
                }
                return MakeNull();
            }
            if (heystack->IsString())
            {
                auto strHeystack = heystack->ToString();
                auto strNeedle = needle->ToString();
                size_t start = 0;
                if (args.size() > 2 && args[2]->IsInt())
                    start = static_cast<size_t>(args[2]->ToInt());
                auto pos = strHeystack.find(strNeedle, start);
                if (pos == std::string::npos)
                    return MakeNull();
                return MakeValue<Integer>(static_cast<Integer>(pos));
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Not a table or string");
            return MakeNull();
        });
    stdModule->AddFunction("slice",
        // Get subset of table or string
        // slice(subject, from[, end]) -> table
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            const auto& subject = args[0];
            if (subject->IsTable())
            {
                auto start = args[1]->ToKey(ctx);
                std::optional<KeyType> end;
                if (args.size() > 2)
                    end = args[1]->ToKey(ctx);

                auto table = subject->ToTable();
                auto newTable = subject->Copy().ToTable();
                bool reachedStart = false;
                for (const auto& item : (*table))
                {
                    if (!reachedStart)
                    {
                        if (item.first == start)
                            reachedStart = true;
                        else
                            continue;
                    }
                    if (end.has_value())
                    {
                        if (item.first == end.value())
                            break;
                    }

                    newTable->Add(item.first, item.second->Copy());
                }
                return MakeValue(newTable);
            }
            if (subject->IsString())
            {
                auto str = subject->ToString();
                auto index = static_cast<size_t>(args[1]->ToInt());
                size_t count = std::string::npos;
                if (args.size() > 2)
                {
                    auto end = static_cast<size_t>(args[2]->ToInt());
                    count = end - index;
                }
                return MakeValue(str.substr(index, count));
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Not a table or string");
            return MakeNull();
        });
    stdModule->AddFunction("resize",
        // Resize a string
        // resize(string, newlength, [ch = '\0'])
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            const auto& subject = args[0];
            if (!subject->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Not a string");
                return MakeNull();
            }
            const auto& length = args[1];
            if (!length->IsInt())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Integer type expected");
                return MakeNull();
            }
            char ch = '\0';
            if (args.size() > 2)
            {
                if (args[2]->IsString())
                {
                    if (args[2]->As<StringPtr>()->length() != 1)
                    {
                        ctx.RuntimeError(Error::Code::InvalidArgument, "Value must be one character");
                        return MakeNull();
                    }
                    ch = args[2]->As<StringPtr>()->at(0);
                }
                else if (args[2]->IsInt())
                {
                    auto chI = args[2]->ToUint();
                    if (chI > 255)
                    {
                        ctx.RuntimeError(Error::Code::InvalidArgument, "Value must be between 0..255");
                        return MakeNull();
                    }
                    ch = static_cast<char>(chI);
                }
                else
                {
                    ctx.RuntimeError(Error::Code::TypeMismatch, "Integer  or string type expected");
                    return MakeNull();
                }
            }
            subject->As<StringPtr>()->resize(length->ToUint(), ch);
            return MakeNull();
        });
    stdModule->AddFunction("split",
        // Split a string
        // split(subject, delimiter[, keepEmpty[, keepWhite]]) -> table
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            const auto& subject = args[0];
            if (!subject->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Not a string");
                return MakeNull();
            }
            const auto& delim = args[1];
            if (!delim->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Not a string");
                return MakeNull();
            }

            bool keepEmpty = false;
            bool keepWhite = true;
            if (args.size() > 2)
                keepEmpty = args[2]->ToBool();
            if (args.size() > 3)
                keepWhite = args[3]->ToBool();
            auto parts = Split(subject->ToString(), delim->ToString(), keepEmpty, keepWhite);
            auto result = MakeTable();
            for (size_t i = 0; i < parts.size(); ++i)
                result->Add((Integer)i, parts[i]);
            return MakeValue(std::move(result));
        });
    stdModule->AddFunction<void, const TablePtr&, const Callable&>("sort",
        [&ctx](const TablePtr& table, const Callable& callback) -> void
        {
            // sort(table, sortfunc)
            // This will modify table and get rid of the keys
            if (callback.ParamCount() != 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Callback function must have 2 parameters");
                return;
            }

            std::vector<ValuePtr> sorted;
            sorted.reserve(table->size());
            for (auto& item : *table)
            {
                sorted.push_back(item.second);
            }
            std::sort(sorted.begin(),
                sorted.end(),
                [&](const ValuePtr& a, const ValuePtr& b) {
                    return callback(ctx, { a, b })->ToBool();
                });
            table->Clear();
            for (size_t i = 0; i < sorted.size(); ++i)
                table->Add((Integer)i, sorted[i]);
        });
    stdModule->AddFunction<std::string, std::string>("to_lower", &StringToLower<char>);
    stdModule->AddFunction<std::string, std::string>("to_upper", &StringToUpper<char>);
    stdModule->AddFunction<bool, std::string>("is_lower", [](const std::string& value) {
        if (value.empty())
            return false;
        for (char c : value)
        {
            if (!std::islower(c))
                return false;
        }
        return true;
    });
    stdModule->AddFunction<bool, std::string>("is_upper", [](const std::string& value) {
        if (value.empty())
            return false;
        for (char c : value)
        {
            if (!std::isupper(c))
                return false;
        }
        return true;
    });
    stdModule->AddFunction("substr",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // substr(string, pos[, length]) -> string
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }
            const auto subjectV = args[0]->ToString();
            auto pos = args[1]->ToUint();
            if (pos >= subjectV.length())
            {
                ctx.RuntimeError(Error::Code::IndexOutOfBounds, "Index out of bounds");
                return MakeNull();
            }
            auto n = std::string::npos;
            if (args.size() > 2)
                n = args[2]->ToUint();
            return MakeValue(subjectV.substr(pos, n));
        });
    stdModule->AddFunction<bool, std::string, std::string>("pattern_match", &PatternMatch<char>);
    stdModule->AddFunction("format",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // format(format[, args...]) -> string
            return Format(ctx, args);
        });
    stdModule->AddFunction<std::string, std::string, std::string, std::string>("replace",
        [](std::string subjectV, const std::string& searchV, const std::string& replaceV) -> std::string
        {
            // replace(subject, search, replace) -> string
            ReplaceSubstring<char>(subjectV, searchV, replaceV);
            return subjectV;
        });
    stdModule->AddFunction<Value, std::string>("length",
        [](const std::string& value) -> int
        {
            return value.length();
        });
    stdModule->AddFunction("trim",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.empty())
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }
            const auto subjectV = args[0]->ToString();
            std::string white = " \t";
            if (args.size() > 1)
                white = args[1]->ToString();
            return MakeValue(Trim(subjectV, white));
        });
    stdModule->AddFunction("left_trim",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.empty())
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }
            const auto subjectV = args[0]->ToString();
            std::string white = " \t";
            if (args.size() > 1)
                white = args[1]->ToString();
            return MakeValue(LeftTrim(subjectV, white));
        });
    stdModule->AddFunction("right_trim",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.empty())
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }
            const auto subjectV = args[0]->ToString();
            std::string white = " \t";
            if (args.size() > 1)
                white = args[1]->ToString();
            return MakeValue(RightTrim(subjectV, white));
        });
    stdModule->AddFunction<Integer, std::string>("string_hash", &StringHash);
    stdModule->AddFunction<std::string, std::string, const Table&>("join",
        [](const std::string& delim, const Table& table) -> std::string
        {
            // join(delim, table) -> string
            std::stringstream ss;
            size_t i = 0;
            for (const auto& item : table)
            {
                ss << item.second->ToString();
                if (i + 1 < table.size())
                    ss << delim;
                ++i;
            }
            return ss.str();
        });
    stdModule->AddFunction("wrap",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // wrap(subject[, length = 70[, tabsize = 0]]) -> table
            if (args.empty())
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }

            std::string subject = args[0]->ToString();
            size_t len = 70;
            size_t tabsize = 0;
            if (args.size() > 1)
                len = args[1]->ToUint();
            if (args.size() > 2)
                tabsize = args[2]->ToUint();
            auto result = MakeTable();
            auto lines = Split(subject, "\n", true, true);
            if (tabsize > 0)
            {
                for (auto& line : lines)
                    ReplaceSubstring(line, std::string("\t"), std::string(tabsize, ' '));
            }
            Integer index = 0;
            for (const auto& line : lines)
            {
                size_t current = 0;
                size_t last = 0;
                do
                {
                    current = last + len > line.length()
                        ? line.length()
                        : (line.find_last_of(" .,?!:;-\t", std::min(line.length() - 1, last + len)) + 1);
                    if (current <= last)
                        current = std::min(last + len, line.length());
                    result->Add(
                        index++, RightTrim(line.substr(last, current - last), std::string(" \t")));
                    last = current;
                } while (current < line.length());
            }

            return MakeValue(result);
        });
    stdModule->AddFunction<TablePtr>("now",
        []() -> TablePtr
        {
            // Get current time
            // now() -> table
            std::time_t t = std::time(nullptr);
            std::tm* now = std::localtime(&t);
            auto result = MakeTable();
            result->Add<Integer>("sec", now->tm_sec);
            result->Add<Integer>("min", now->tm_min);
            result->Add<Integer>("hour", now->tm_hour);
            result->Add<Integer>("day", now->tm_mday);
            result->Add<Integer>("month", now->tm_mon + 1);
            result->Add<Integer>("year", now->tm_year + 1900);
            result->Add<Integer>("wday", now->tm_wday);
            result->Add<Integer>("yday", now->tm_yday);
            result->Add<bool>("isdst", now->tm_isdst);

            return result;
        });
    stdModule->AddFunction<TablePtr, Integer>("localtime",
        [](Integer value) -> TablePtr
        {
            std::time_t t = static_cast<std::time_t>(value);
            std::tm* tm = std::localtime(&t);
            auto result = MakeTable();
            result->Add<Integer>("sec", tm->tm_sec);
            result->Add<Integer>("min", tm->tm_min);
            result->Add<Integer>("hour", tm->tm_hour);
            result->Add<Integer>("day", tm->tm_mday);
            result->Add<Integer>("month", tm->tm_mon + 1);
            result->Add<Integer>("year", tm->tm_year + 1900);
            result->Add<Integer>("wday", tm->tm_wday);
            result->Add<Integer>("yday", tm->tm_yday);
            result->Add<bool>("isdst", tm->tm_isdst);

            return result;
        });
    stdModule->AddFunction<TablePtr, Integer>("gmtime",
        [](Integer value) -> TablePtr
        {
            std::time_t t = static_cast<std::time_t>(value);
            std::tm* tm = std::gmtime(&t);
            auto result = MakeTable();
            result->Add<Integer>("sec", tm->tm_sec);
            result->Add<Integer>("min", tm->tm_min);
            result->Add<Integer>("hour", tm->tm_hour);
            result->Add<Integer>("day", tm->tm_mday);
            result->Add<Integer>("month", tm->tm_mon + 1);
            result->Add<Integer>("year", tm->tm_year + 1900);
            result->Add<Integer>("wday", tm->tm_wday);
            result->Add<Integer>("yday", tm->tm_yday);
            result->Add<bool>("isdst", tm->tm_isdst);

            return result;
        });
    stdModule->AddFunction<Integer, const Table&>("mktime",
        [&ctx](const Table& value) -> Integer
        {
            std::tm tm = {};
            GetTableOptionalValue(ctx, value, "sec", tm.tm_sec);
            GetTableOptionalValue(ctx, value, "min", tm.tm_min);
            GetTableOptionalValue(ctx, value, "hour", tm.tm_hour);
            GetTableOptionalValue(ctx, value, "day", tm.tm_mday);
            GetTableOptionalValue(ctx, value, "month", tm.tm_mon);
            GetTableOptionalValue(ctx, value, "year", tm.tm_year);
            GetTableOptionalValue(ctx, value, "isdst", tm.tm_isdst);
            if (tm.tm_mon > 0)
                --tm.tm_mon;
            if (tm.tm_year >= 1900)
                tm.tm_year -= 1900;
            return std::mktime(&tm);
        });
    stdModule->AddFunction<std::string, std::string, const Table&>("strftime",
        [&ctx](const std::string& format, const Table& value) -> std::string
        {
            std::tm tm = {};
            GetTableOptionalValue(ctx, value, "sec", tm.tm_sec);
            GetTableOptionalValue(ctx, value, "min", tm.tm_min);
            GetTableOptionalValue(ctx, value, "hour", tm.tm_hour);
            GetTableOptionalValue(ctx, value, "day", tm.tm_mday);
            GetTableOptionalValue(ctx, value, "month", tm.tm_mon);
            GetTableOptionalValue(ctx, value, "year", tm.tm_year);
            GetTableOptionalValue(ctx, value, "isdst", tm.tm_isdst);
            if (tm.tm_mon > 0)
                --tm.tm_mon;
            if (tm.tm_year >= 1900)
                tm.tm_year -= 1900;
            char buff[255] = {};
            if (std::strftime(buff, sizeof(buff), format.c_str(), &tm))
                return buff;
            return "";
        });
    stdModule->AddFunction<Integer>("unix_time",
        []() -> Integer
        {
            // Get Unix time stamp
            // unix_time() -> int
            const auto p1 = std::chrono::system_clock::now();
            return static_cast<Integer>(
                std::chrono::duration_cast<std::chrono::seconds>(p1.time_since_epoch()).count());
        });
    stdModule->AddFunction<Value, std::string>("exec",
        [&ctx](const std::string& expr) -> Value
        {
            // Execute a script
            // exec(string) -> Value
            sa::tl::Parser parser(expr);
            auto root = parser.Parse();
            if (!root)
            {
                // Syntax error(s)
                for (const auto& e : parser.GetErrors())
                    ctx.RuntimeError(Error::Code::SyntaxError, e.message);
                return {};
            }
            return *root->Evaluate(ctx);
            // Executing an arbitrary string is not safe
        }, false);
    stdModule->AddFunction<Value, std::string>("eval",
        [&ctx](const std::string& expr) -> Value
        {
            // Evaluate expression
            // eval(string) -> Value
            sa::tl::Parser parser(expr);
            auto root = parser.ParseExpression();
            if (!root)
            {
                // Syntax error(s)
                for (const auto& e : parser.GetErrors())
                    ctx.RuntimeError(Error::Code::SyntaxError, e.message);
                return {};
            }
            return *root->Evaluate(ctx);
        }, false);
    stdModule->AddFunction<std::string, ValuePtr>("serialize",
        [&ctx](const ValuePtr& table) -> std::string
        {
            if (!table->IsTable())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Table expected");
                return "";
            }
            return Serialize(*table);
        });
    stdModule->AddFunction<Value, std::string>("unserialize",
        [&ctx](const std::string& expr) -> Value
        {
            sa::tl::Parser parser(expr);
            auto root = parser.ParseTable(true);
            if (!root)
            {
                // Syntax error(s)
                for (const auto& e : parser.GetErrors())
                    ctx.RuntimeError(Error::Code::SyntaxError, e.message);
                return {};
            }
            return *root->Evaluate(ctx);
        });
    stdModule->AddFunction<Integer, TablePtr>("table_types",
        [](const TablePtr& table) -> Integer
        {
            return static_cast<Integer>(table->Types());
        });
    stdModule->AddFunction<Value, const Callable&, const Table&>("call",
        [&ctx](const Callable& callable, const Table& arguments) -> Value
        {
            FunctionArguments args;
            for (const auto& a : (arguments))
                args.push_back(a.second);
            return *(callable)(ctx, args);
        });
    stdModule->AddFunction("pcall",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.empty())
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Missing function argument");
                return MakeNull();
            }
            if (!args[0]->IsCallable())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Function expected");
                return MakeNull();
            }

            const auto& callable = args[0]->As<CallablePtr>();
            if (!callable)
            {
                // Calling a function that is not assigned does not stop execution
                TryBlockGuard tg(ctx);
                tg.AddCode(0);
                auto result = MakeTable();
                ctx.RuntimeError(Error::Code::NullDereference, "Function is null");
                Error error = ctx.TakeErrors(0);
                result->Add("error", MakeTable(error));
                result->Add("status", false);
                ctx.stop_ = false;
                return MakeValue(result);
            }

            auto result = MakeTable();
            EvalGuard eg(ctx);
            TryBlockGuard tg(ctx);
            tg.AddCode(0);
            auto funcres = (*callable)(ctx, FunctionArguments(args.begin() + 1, args.end()));
            if (ctx.stop_)
            {
                Error error = ctx.TakeErrors(0);
                result->Add("error", MakeTable(error));
                result->Add("status", false);
                ctx.stop_ = false;
            }
            else
            {
                result->Add("result", funcres);
                result->Add("status", true);
            }
            return MakeValue(result);
        });
    stdModule->AddFunction<int, const Callable&>("nparams",
        [](const Callable& callable) -> int
        {
            return callable.ParamCount();
        });
    stdModule->AddFunction<void, const ValuePtr&, const ValuePtr&>("swap",
        [&ctx](const ValuePtr& a, const ValuePtr& b) -> void
        {
            if (a->IsConst() || b->IsConst())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Can not swap const values");
                return;
            }
            if (a->GetType() != b->GetType())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Arguments have different types");
                return;
            }
            std::swap(*a, *b);
        });
    stdModule->AddFunction<Value, Value>("sign",
        [&ctx](Value a) -> Value
        {
            if (!a.IsFloat() && !a.IsInt())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "Need int or float");
                return {};
            }
            if (a.IsFloat())
                return { Sign(a.As<Float>()) };
            return { Sign(a.As<Integer>()) };
        });
    stdModule->AddFunction<std::string, const std::string&>("base64_encode",
        [](const std::string& a) -> std::string
        {
            return Base64Encode((const unsigned char*)a.data(), a.size());
        });
    stdModule->AddFunction<std::string, const std::string&>("base64_decode",
        [](const std::string& a) -> std::string
        {
            return Base64Decode(a.data(), a.size());
        });
    stdModule->AddFunction<std::string, std::string, const Callable&>("parse_template",
        [&ctx](const std::string& templ, const Callable& callback) -> std::string
        {
            if (callback.ParamCount() != 1)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
                return {};
            }
            templ::Parser parser;
            const auto tokens = parser.Parse(templ);
            if (parser.HasError())
            {
                ctx.RuntimeError(Error::Code::SyntaxError, "Unmatched curly brackets in template.");
                return "";
            }

            auto result = tokens.ToString(
                [&](const templ::Token& token) -> std::string
                {
                    if (token.type == templ::Token::Type::Variable)
                    {
                        if (ctx.stop_)
                            return "";
                        auto res = callback(ctx, { MakeValue(token.value) });
                        if (res)
                            return res->ToString();
                        return "";
                    }
                    return token.value;
                });
            if (ctx.stop_)
                return "";
            return result;
        });
    stdModule->AddFunction<Integer, const Table&, Integer>("combine_numbers", &CombineNumbers);
    stdModule->AddFunction<bool, const Value&>("is_array",
        [](const Value& value) -> bool
        {
            if (!value.IsTable())
                return false;
            return value.As<TablePtr>()->IsArray();
        });
    stdModule->AddFunction<std::string, std::string, Unsigned>("elide",
        [](const std::string& value, Unsigned max) -> std::string
        {
            if (value.size() <= max)
                return value;
            return value.substr(0, max - 3) + "...";
        });
    stdModule->AddFunction<void, const ValuePtr&>("reverse",
        [&ctx](const ValuePtr& value)
        {
            if (value->IsConst())
            {
                ctx.RuntimeError(Error::Code::ConstAssign, "Can not assign to constant");
                return;
            }
            if (value->IsString())
            {
                auto& str = *value->As<StringPtr>();
                std::reverse(str.begin(), str.end());
                return;
            }
            if (value->IsTable())
            {
                auto& table = *value->As<TablePtr>();
                std::vector<ValuePtr> sorted;
                sorted.reserve(table.size());
                for (auto& item : table)
                {
                    sorted.push_back(item.second);
                }
                std::reverse(sorted.begin(), sorted.end());
                table.clear();
                for (size_t i = 0; i < sorted.size(); ++i)
                    table.Add((Integer)i, sorted[i]);
                return;
            }
            ctx.RuntimeError(Error::Code::TypeMismatch, "Not a table or string");
        });
    stdModule->AddFunction<Integer, Integer>("isalnum", &isalnum);
    stdModule->AddFunction<Integer, Integer>("isalpha", &isalpha);
    stdModule->AddFunction<Integer, Integer>("isascii", &isascii);
    stdModule->AddFunction<Integer, Integer>("isblank", &isblank);
    stdModule->AddFunction<Integer, Integer>("iscntrl", &iscntrl);
    stdModule->AddFunction<Integer, Integer>("isdigit", &isdigit);
    stdModule->AddFunction<Integer, Integer>("isgraph", &isgraph);
    stdModule->AddFunction<Integer, Integer>("islower", &islower);
    stdModule->AddFunction<Integer, Integer>("isprint", &isprint);
    stdModule->AddFunction<Integer, Integer>("ispunct", &ispunct);
    stdModule->AddFunction<Integer, Integer>("isspace", &isspace);
    stdModule->AddFunction<Integer, Integer>("isupper", &isupper);
    stdModule->AddFunction<Integer, Integer>("isxdigit", &isxdigit);
    stdModule->AddFunction<Integer, Integer>("toascii", &toascii);
    stdModule->AddFunction<Integer, Integer>("toupper", &toupper);
    stdModule->AddFunction<Integer, Integer>("tolower", &tolower);
    stdModule->AddFunction<int, const char*, const char*>("compare", &strcmp);
    stdModule->AddFunction<Value, const std::string&, const Object&>(
        "object_cast",
        [&ctx](const std::string& typeName, const Object& obj) -> Value
        {
            if (!obj.GetInstance())
            {
                ctx.RuntimeError(Error::NullDereference, "Object is null");
                return {};
            }
            const auto* meta = ctx.GetMetatable(typeName);
            if (!meta)
            {
                ctx.RuntimeError(Error::InvalidArgument, std::format("Type \"{}\" not found", typeName));
                return {};
            }
            if (!meta->InstanceOf(typeName))
            {
                ctx.RuntimeError(Error::InvalidArgument, std::format("Object is not of type \"{}\"", typeName));
                return {};
            }
            return MakeObject(*meta, meta->GetClassName(), obj.GetInstance());
        });
    stdModule->AddFunction<std::string, const Object&>(
        "object_type",
        [](const Object& obj) -> std::string
        {
            return std::string(obj.GetClassName());
        });
    stdModule->AddFunction<Value, Value, const Table&, Value>(
        "match",
        [&ctx](const Value& value, const Table& cases, const Value& e) -> Value
        {
            return Match(ctx, value, cases, e);
        });
    stdModule->AddFunction<std::string, std::string>(
        "load_from_file",
        [&ctx](const std::string& filename) -> std::string
        {
            std::ifstream ifs(filename);
            if (!ifs.good())
            {
                ctx.RuntimeError(Error::InvalidArgument, std::format("Unable to open file {}", filename));
                return "";
            }
            std::string result;
            result.assign((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
            return result;
        });
    stdModule->AddFunction<void, std::string, Value>(
        "save_to_file",
        [&ctx](const std::string& filename, const Value& content)
        {
            std::ofstream ofs(filename);
            if (!ofs.good())
            {
                ctx.RuntimeError(Error::InvalidArgument, std::format("Unable to open file {}", filename));
                return;
            }
            if (content.IsTable())
            {
                ofs << Serialize(content);
            }
            else if (content.IsCallable() || content.IsObject())
            {
                // Not supported
            }
            else
                ofs << content.ToString();
            ofs.close();
        });

    ctx.AddConst("std", std::move(stdModule), SCOPE_LIBRARY);

    auto bitModule = MakeModule();
    bitModule->AddFunction<Integer, Integer>("bswap", &std::byteswap<Integer>);
    bitModule->AddFunction<Integer, Unsigned, int>("rotl", &std::rotl<Unsigned>);
    bitModule->AddFunction<Integer, Unsigned, int>("rotr", &std::rotr<Unsigned>);
    bitModule->AddFunction<Integer, Unsigned, Unsigned>("set", [](Unsigned bitSet, Unsigned bits) -> Integer {
        sa::bits::set(bitSet, bits);
        return bitSet;
    });
    bitModule->AddFunction<Integer, Unsigned, Unsigned>("un_set", [](Unsigned bitSet, Unsigned bits) -> Integer {
        sa::bits::un_set(bitSet, bits);
        return bitSet;
    });
    bitModule->AddFunction<bool, Unsigned, Unsigned>("is_set", &sa::bits::is_set<Unsigned, Unsigned>);
    bitModule->AddFunction<bool, Unsigned, Unsigned>("is_any_set", &sa::bits::is_any_set<Unsigned, Unsigned>);
    bitModule->AddFunction<bool, Unsigned, Unsigned>("is_none_set", &sa::bits::is_none_set<Unsigned, Unsigned>);
    bitModule->AddFunction<Integer, Unsigned>("flip_all", [](Unsigned bitSet) -> Integer {
        sa::bits::flip(bitSet);
        return bitSet;
    });
    bitModule->AddFunction<Integer, Unsigned, Unsigned>("flip_bits", [](Unsigned bitSet, Unsigned bits) -> Integer {
        sa::bits::flip(bitSet, bits);
        return bitSet;
    });
    bitModule->AddFunction<std::string, Unsigned>("to_string", [](Unsigned bitSet) -> std::string {
        return sa::bits::to_string(bitSet);
    });

    ctx.AddConst("bit", std::move(bitModule), SCOPE_LIBRARY);

    auto errModule = MakeModule();
#define ENUMERATE_ERROR(v) errModule->AddConst<Integer>(#v, static_cast<Integer>(Error::v));
    ENUMERATE_ERRORS
#undef ENUMERATE_ERROR
    errModule->AddConst<Integer>("UserDefined", static_cast<Integer>(Error::UserDefined));
    ctx.AddConst("errors", std::move(errModule), SCOPE_LIBRARY);

    // Top level functions
    ctx.AddFunction<Value, const Value&>(
        "copy",
        [](const Value& value) -> Value
        {
            // Create a deep copy of the value
            return value.Copy();
        }, SCOPE_LIBRARY);
    ctx.AddFunction<int, const Value&>(
        "type",
        [](const Value& value) -> int
        {
            // Return type of variable
            return (int)value.GetType();
        }, SCOPE_LIBRARY);
    ctx.AddFunction<bool, std::string>(
        "defined",
        [&ctx](const std::string& ident) -> int
        {
            return ctx.HasValue(ident);
        }, SCOPE_LIBRARY);
    // Return values of type()
    ctx.AddConst<Integer>("TYPE_NULL", INDEX_NULL, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_BOOL", INDEX_BOOL, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_INT", INDEX_INT, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_FLOAT", INDEX_FLOAT, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_STRING", INDEX_STRING, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_FUNCTION", INDEX_FUNCTION, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_TABLE", INDEX_TABLE, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("TYPE_OBJECT", INDEX_OBJECT, SCOPE_LIBRARY);
    ctx.AddFunction(
        "print",
        [](Context&, const FunctionArguments& args) -> ValuePtr
        {
            // print([args...])
            // Without newline
            if (!args.empty())
            {
                for (size_t i = 0; i < args.size(); ++i)
                {
                    std::cout << args[i]->ToString();
                    if (i + 1 < args.size())
                        std::cout << '\t';
                }
                std::cout << std::flush;
            }
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction("printf",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // printf(format[, args...])
            // Without newline
            // Same as print(format(...))
            auto value = Format(ctx, args);
            if (ctx.HasErrors())
                return MakeNull();

            std::cout << value->ToString() << std::flush;
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction(
        "println",
        [](Context&, const FunctionArguments& args) -> ValuePtr
        {
            // println([args...])
            // With newline
            if (!args.empty())
            {
                for (size_t i = 0; i < args.size(); ++i)
                {
                    std::cout << args[i]->ToString();
                    if (i + 1 < args.size())
                        std::cout << '\t';
                }
            }
            std::cout << std::endl;
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction(
        "error",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // error([[code, ]message])
            std::string message = "Userdefined error";
            int code = Error::Code::UserDefined;
            if (args.size() > 1)
            {
                code = static_cast<int>(args[0]->ToInt());
                message = args[1]->ToString();
            }
            else if (!args.empty())
            {
                if (args[0]->IsTable())
                {
                    // When the argument is a table, assume we want to rethrow an error
                    const auto& err = *args[0]->As<TablePtr>();
                    const auto codeIt = err.find("code");
                    if (codeIt != err.end())
                        code = static_cast<int>(codeIt->second->ToInt());
                    const auto msgIt = err.find("message");
                    if (msgIt != err.end())
                        message = msgIt->second->ToString();
                }
                else if (args[0]->IsString())
                    message = args[0]->ToString();
            }
            ctx.RuntimeError(code, message);
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction(
        "warning",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            std::cout << "Warning (" << ctx.GetLocation() << "): ";
            if (!args.empty())
            {
                for (size_t i = 0; i < args.size(); ++i)
                {
                    std::cout << args[i]->ToString();
                    if (i + 1 < args.size())
                        std::cout << '\t';
                }
            }
            std::cout << std::endl;
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction<void>(
        "sandbox",
        [&ctx]()
        {
            ctx.sandboxed_ = true;
        },
        0);
    ctx.AddFunction<bool>(
        "is_sandboxed",
        [&ctx]() -> bool
        {
            return ctx.sandboxed_;
        }, SCOPE_LIBRARY);
    ctx.AddFunction("load",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            static std::map<std::string, std::weak_ptr<Value>> loaded;
            if (args.empty())
            {
                ctx.RuntimeError(Error::ArgumentsMismatch, "Filename argument expected");
                return MakeNull();
            }
            if (args.size() > 2)
            {
                ctx.RuntimeError(Error::ArgumentsMismatch, "Expecting one or two arguments");
                return MakeNull();
            }
            std::string filename = args[0]->ToString();
            bool once = false;
            if (args.size() > 1)
                once = args[1]->ToBool();


            std::string foundFile = ctx.FindFile(filename);
            if (!FileExists(foundFile))
            {
                ctx.RuntimeError(Error::InvalidArgument, std::format("No file found for \"{}\"", filename));
                return MakeNull();
            }

            if (once)
            {
                auto it = loaded.find(foundFile);
                if (it != loaded.end())
                    return it->second.lock();
            }

            auto contents = sa::tl::ReadScriptFile(foundFile);
            if (contents.empty())
                return MakeNull();

            sa::tl::Parser parser(contents, filename);
            parser.onInclude_ = [&](std::string& _filename, const sa::tl::Location& loc) -> std::string
            {
                _filename = sa::tl::FindRelatedFile(filename, _filename);
                std::string source = sa::tl::ReadScriptFile(_filename);
                if (source.empty())
                    parser.SyntaxError(loc, std::format("File {} not found", filename));
                return source;
            };
            auto root = parser.Parse();
            if (!root)
            {
                // Syntax error(s)
                for (const auto& e : parser.GetErrors())
                    ctx.RuntimeError(Error::SyntaxError, e.message);
                return MakeNull();
            }
            auto result = root->Evaluate(ctx);
            if (once)
               loaded[foundFile] = result;
            return result;
        });
    ctx.AddConst<Integer>("GC_TRIGGER_NONE", (Integer)GC::Trigger::Kind::None, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("GC_TRIGGER_ALLOCS", (Integer)GC::Trigger::Kind::Allocs, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("GC_TRIGGER_SIZE", (Integer)GC::Trigger::Kind::Size, SCOPE_LIBRARY);
    ctx.AddConst<Integer>("GC_TRIGGER_BOTH", (Integer)GC::Trigger::Kind::Both, SCOPE_LIBRARY);
    sa::tl::MetaTable& gcMeta = ctx.AddMetatable<GC>();
    gcMeta
        .AddFunction<GC, void>("run", &GC::Run)
        .AddProperty<GC, Integer>("trigger_size", &GC::GetTriggerSize, &GC::SetTriggerSize)
        .AddProperty<GC, Integer>("trigger_allocs", &GC::GetTriggerAllocs, &GC::SetTriggerAllocs)
        .AddProperty<GC, Integer>("trigger_kind", &GC::GetTriggerKind, &GC::SetTriggerKind)
        .AddProperty<GC, bool>("pause", &GC::GetPause, &GC::SetPause)
        .AddProperty<GC, Integer>("count", &GC::GetCount)
        .AddProperty<GC, Integer>("size", &GC::GetSize)
        .AddProperty<GC, Integer>("peak", &GC::GetPeak)
        .AddProperty<GC, Integer>("allocs", &GC::GetAllocs);

    ctx.SetInstance<GC>(gcMeta, "_gc", ctx.GetGC(), SCOPE_LIBRARY);

    AddUTF8Lib(ctx);
    AddJSONLib(ctx);
}
}
