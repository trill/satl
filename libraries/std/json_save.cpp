/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "json_save.h"

template<typename T>
static void Add(sa::json::JSON& json, const sa::tl::KeyType& key, T&& value)
{
    switch (json.GetType())
    {
    case sa::json::JSON::Type::Array:
        json.Append(std::move(value));
        break;
    case sa::json::JSON::Type::Object:
        json[sa::tl::KeyToString(key)] = std::move(value);
        break;
    default:
        SATL_ASSERT_FALSE();
    }
}

static void AddValue(sa::json::JSON& json, const sa::tl::KeyType& key, const sa::tl::Value& value)
{
    switch (value.GetType())
    {
    case sa::tl::INDEX_NULL:
    {
        Add(json, key, nullptr);
        break;
    }
    case sa::tl::INDEX_BOOL:
    {
        Add(json, key, value.ToBool());
        break;
    }
    case sa::tl::INDEX_INT:
    {
        Add(json, key, value.ToInt());
        break;
    }
    case sa::tl::INDEX_FLOAT:
    {
        Add(json, key, value.ToFloat());
        break;
    }
    case sa::tl::INDEX_STRING:
    {
        Add(json, key, value.ToString());
        break;
    }
    case sa::tl::INDEX_TABLE:
    {
        auto tbl = value.As<sa::tl::TablePtr>()->IsArray() ? sa::json::Array() : sa::json::Object();

        for (const auto& item : *value.As<sa::tl::TablePtr>())
        {
            AddValue(tbl, item.first, *item.second);
        }
        Add(json, key, std::move(tbl));
        break;
    }
    }
}

sa::json::JSON JsonSave(const sa::tl::Table& tbl)
{
    auto result = tbl.IsArray() ? sa::json::Array() : sa::json::Object();

    for (const auto& item : tbl)
    {
        AddValue(result, item.first, *item.second);
    }
    return result;
}

std::string TableToJsonString(const sa::tl::TablePtr& tbl)
{
    if (!tbl)
        return "";
    return JsonSave(*tbl).Dump();
}
