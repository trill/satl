/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "json_load.h"

static void AddJson(const sa::tl::TablePtr& tbl, const sa::tl::KeyType& key, const sa::json::JSON& json)
{
    switch (json.GetType())
    {
    case sa::json::JSON::Type::Null:
        tbl->Add(key, sa::tl::MakeNull());
        break;
    case sa::json::JSON::Type::Bool:
        tbl->Add(key, sa::tl::MakeValue<bool>(static_cast<bool>(json)));
        break;
    case sa::json::JSON::Type::Int:
        tbl->Add(key, sa::tl::MakeValue<sa::tl::Integer>(static_cast<sa::json::Integer>(json)));
        break;
    case sa::json::JSON::Type::Float:
        tbl->Add(key, sa::tl::MakeValue<sa::tl::Float>(static_cast<sa::json::Float>(json)));
        break;
    case sa::json::JSON::Type::String:
        tbl->Add(key, sa::tl::MakeValue(sa::tl::MakeString(static_cast<std::string>(json))));
        break;
    case sa::json::JSON::Type::Object:
    {
        auto t = sa::tl::MakeTable();
        const sa::json::ObjectType& obj = static_cast<const sa::json::ObjectType&>(json);
        for (const auto& oi : obj)
            AddJson(t, oi.first, oi.second);
        tbl->Add(key, sa::tl::MakeValue(t));
        break;
    }
    case sa::json::JSON::Type::Array:
    {
        auto t = sa::tl::MakeTable();
        const sa::json::ArrayType& arr = static_cast<const sa::json::ArrayType&>(json);
        for (const auto& oi : arr)
            AddJson(t, t->NextIndex(), oi);
        tbl->Add(key, sa::tl::MakeValue(t));
        break;
    }
    }
}

sa::tl::TablePtr JsonLoadString(const std::string& json, const sa::json::ErrorFunc& error)
{
    sa::json::JSON j = sa::json::Parse(json, error);
    return JsonLoadJson(j);
}

sa::tl::TablePtr JsonLoadJson(const sa::json::JSON& json)
{
    sa::tl::TablePtr result = sa::tl::MakeTable();
    auto key = result->NextIndex();
    AddJson(result, key, json);
    auto val = result->GetValue(key);
    if (!val)
        return sa::tl::MakeTable();
    return val->ToTable();
}
