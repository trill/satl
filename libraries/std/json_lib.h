/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <context.h>

void AddJSONLib(sa::tl::Context& ctx);
