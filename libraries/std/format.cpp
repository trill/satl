/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "format.h"
#include <iomanip>
#include <iostream>
#include <sstream>

using namespace sa::tl;

ValuePtr Format(Context& ctx, const FunctionArguments& args)
{
    // format(format[, args...]) -> string
    if (args.empty())
    {
        ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
        return MakeNull();
    }
    if (!args[0]->IsString())
    {
        ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
        return MakeNull();
    }
    const auto format = args[0]->ToString();
    size_t current = 1;

    auto checkArg = [&]() -> bool
    {
        if (current >= args.size())
        {
            ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
            return false;
        }
        return true;
    };

    enum Flag : uint32_t
    {
        LeftJustify = 1u,
        ForcePlus = 1u << 1u,
        PrecedeZero = 1u << 2u,
        LeftPadZero = 1u << 3u
    };

    bool inSpecifier = false;

    // %[flags][width][.precision]specifier
    uint32_t flags = 0;
    int width = -1;
    int precision = -1;

    auto reset = [&]()
    {
        flags = 0;
        width = -1;
        precision = -1;
    };

    std::stringstream result;
    int defPrec = (int)result.precision();
    auto setFormat = [&]()
    {
        if (precision > -1)
            result << std::setprecision(precision);
        if (width > -1)
            result << std::setw(width);
        if (flags & LeftJustify)
            result << std::left;
        if (flags & LeftPadZero)
            result << std::setfill('0');
    };
    auto resetFormat = [&]()
    {
        result << std::nouppercase;
        result << std::defaultfloat;
        result << std::setprecision(defPrec);
        result << std::setw(1);
        result << std::right;
        result << std::setfill(' ');
    };

    for (auto it = format.begin(); it != format.end(); ++it)
    {
        auto getNumber = [&]() -> int
        {
            size_t start = it - format.begin();
            size_t length = 0;
            while (isdigit(*it))
            {
                ++length;
                ++it;
            }
            if (it == format.end())
                return -1;
            --it;
            if (length == 0)
                return -1;
            std::string n = std::string(&format[start], length);
            return atoi(n.c_str());
        };

        if (!inSpecifier)
        {
            while ((*it) != '%')
            {
                if (it == format.end())
                    break;
                result << (*it);
                ++it;
            }
            if (it == format.end())
                break;
            ++it;
        }

        inSpecifier = true;
        switch (*it)
        {
        case '-':
            flags |= LeftJustify;
            break;
        case '+':
            flags |= ForcePlus;
            break;
        case '#':
            flags |= PrecedeZero;
            break;
        case '0':
            flags |= LeftPadZero;
            break;
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            width = getNumber();
            break;
        case '.':
            ++it;
            precision = getNumber();
            break;
        case 'a':
        case 'A':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsInt() && !val->IsFloat())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            result << std::hexfloat;
            if ((*it) == 'A')
                result << std::uppercase;
            setFormat();
            if (flags & ForcePlus && val->ToFloat() > 0)
                result << "+";
            result << val->ToFloat();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 'c':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (val->IsString())
            {
                auto str = val->ToString();
                if (str.empty())
                {
                    ctx.RuntimeError(Error::Code::InvalidArgument, "String is empty");
                    return MakeNull();
                }
                result << str[0];
            }
            else if (val->IsInt() || val->IsFloat())
            {
                auto i = std::clamp<Integer>(val->ToInt(), 0, 255);
                setFormat();
                result << static_cast<char>(i);
                resetFormat();
            }
            else
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            reset();
            inSpecifier = false;
            break;
        }
        case 'd':
        case 'i':
        case 'u':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsInt() && !val->IsFloat())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            result << std::dec;
            setFormat();
            if ((*it) == 'd' || (*it) == 'i')
            {
                if (flags & ForcePlus && val->ToInt() > 0)
                    result << "+";
                result << val->ToInt();
            }
            else
                result << val->ToUint();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 'e':
        case 'E':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsInt() && !val->IsFloat())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            result << std::dec << std::scientific;
            if ((*it) == 'E')
                result << std::uppercase;
            setFormat();
            if (flags & ForcePlus && val->ToFloat() > 0)
                result << "+";
            result << val->ToFloat();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 'f':
        case 'F':
        case 'g':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsInt() && !val->IsFloat())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            result << std::dec;
            if ((*it) == 'f')
                result << std::fixed;
            if ((*it) == 'F')
                result << std::uppercase;

            setFormat();
            if (flags & ForcePlus && val->ToFloat() > 0)
                result << "+";
            result << val->ToFloat();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 'o':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsInt() && !val->IsFloat())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            if (flags & PrecedeZero)
                result << "0";
            setFormat();
            result << std::oct << val->ToUint();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 'p':
        {
            const auto& val = args[current++];
            if (val->IsObject())
                result << val->As<ObjectPtr>()->GetInstance();
            else if (val->IsTable())
                result << val->As<TablePtr>().get();
            else if (val->IsString())
                result << val->As<StringPtr>().get();
            else if (val->IsCallable())
                result << val->As<CallablePtr>().get();
            else if (val->IsNull())
                result << "0x" << std::nouppercase << std::setw(sizeof(uintptr_t) * 2) << std::setfill('0') << std::hex
                       << 0;
            else
                result << "0x" << std::nouppercase << std::setw(sizeof(uintptr_t) * 2) << std::setfill('0') << std::hex
                       << val->ToUint();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 's':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsString())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            setFormat();
            result << val->ToString();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case 'x':
        case 'X':
        {
            if (!checkArg())
                return MakeNull();
            const auto& val = args[current++];
            if (!val->IsInt() && !val->IsFloat())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "Unsupported argument type");
                return MakeNull();
            }
            if ((*it) == 'X')
            {
                if (flags & PrecedeZero)
                    result << "0X";
                result << std::uppercase;
            }
            else
            {
                if (flags & PrecedeZero)
                    result << "0x";
            }
            setFormat();
            result << std::hex << val->ToUint();
            resetFormat();
            reset();
            inSpecifier = false;
            break;
        }
        case '%':
            result << '%';
            reset();
            inSpecifier = false;
            break;
        }
    }
    return MakeValue(result.str());
}
