/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "utf8_lib.h"
#include <module.h>
#include <utf8.h>

using namespace sa;
using namespace sa::tl;

void AddUTF8Lib(Context& ctx)
{
    auto module = MakeModule();
    module->AddFunction<std::string, std::string>("to_upper", &utf8::ToUpper);
    module->AddFunction<std::string, std::string>("to_lower", &utf8::ToLower);
    module->AddFunction<bool, std::string>("is_upper", &utf8::IsUpper);
    module->AddFunction<bool, std::string>("is_lower", &utf8::IsLower);
    module->AddFunction<int, std::string, std::string>("compare", &utf8::Compare);
    module->AddFunction<Integer, std::string>("length", &utf8::Length);
    module->AddFunction("substr",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // substr(string, pos[, length]) -> string
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }
            const auto subjectV = args[0]->ToString();
            auto cp = utf8::Decode(subjectV);

            auto pos = args[1]->ToUint();
            if (pos >= cp.size())
            {
                ctx.RuntimeError(Error::Code::IndexOutOfBounds, "Index out of bounds");
                return MakeNull();
            }
            auto n = std::string::npos;
            if (args.size() > 2)
                n = args[2]->ToUint() + pos;
            std::vector<uint32_t> rescp;
            for (size_t i = pos; i < n; ++i)
            {
                if (i >= cp.size())
                    break;
                rescp.push_back(cp[i]);
            }
            return MakeValue(utf8::Encode(rescp));
        });
    module->AddFunction<TablePtr, const std::string&>("to_codepoints",
        [](const std::string& a) -> TablePtr
        {
            auto result = MakeTable();
            auto cp = utf8::Decode(a);
            Integer index = 0;
            for (auto c : cp)
                result->Add<int>(index++, c);
            return result;
        });
    module->AddFunction<std::string, const Table&>("to_string",
        [](const Table& a) -> std::string
        {
            std::vector<uint32_t> cps;
            cps.reserve(a.size());
            for (const auto& v : a)
                cps.push_back(v.second->ToUint());
            return utf8::Encode(cps);
        });
    module->AddFunction<void, const StringPtr&>("reverse",
        [](const StringPtr& a)
        {
            std::vector<uint32_t> cps = utf8::Decode(*a);
            std::reverse(cps.begin(), cps.end());
            a->assign(utf8::Encode(cps));
        });
    module->AddFunction<Integer, const std::string&, size_t>("get_codepoint",
        [](const std::string& a, size_t pos) -> Integer
        {
            std::vector<uint32_t> cps = utf8::Decode(a);
            if (pos < cps.size())
                return cps.at(pos);
            return 0;
        });
    module->AddFunction<void, const StringPtr&, size_t, uint32_t>("set_codepoint",
        [](const StringPtr& a, size_t pos, uint32_t value)
        {
            std::vector<uint32_t> cps = utf8::Decode(*a);
            if (pos >= cps.size())
                return;
            cps[pos] = value;
            a->assign(utf8::Encode(cps));
        });
    module->AddFunction<std::string, Integer>("chr",
        [&ctx](Integer value) -> std::string
        {
            if (value < 0 || value > std::numeric_limits<uint32_t>::max())
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, std::format("Value must be between 0 and {}", std::numeric_limits<uint32_t>::max()));
                return {};
            }
            return utf8::Encode({ static_cast<uint32_t>(value) });
        });
    module->AddFunction<int, std::string>("ord",
        [&ctx](const std::string& value) -> Integer
        {
            auto cps = utf8::Decode(value);
            if (value.length() != 1)
            {
                ctx.RuntimeError(Error::Code::InvalidArgument, "String must be exactly one character long");
                return {};
            }
            return static_cast<Integer>(cps[0]);
        });

    ctx.AddConst("utf8", std::move(module), SCOPE_LIBRARY);
}
