// https://github.com/gaspardpetit/base64
// https://raw.githubusercontent.com/gaspardpetit/base64/master/src/polfosol/polfosol.h
// https://github.com/gaspardpetit/base64/blob/master/src/JouniMalinen/jounimalinen.h
// http://stackoverflow.com/questions/180947/base64-decode-snippet-in-c

#pragma once

#include <string>

static constexpr unsigned char base64_index[256] = {
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 62  , 0x80, 62  , 0x80, 63  ,
    52  , 53  , 54  , 55  , 56  , 57  , 58  , 59  , 60  , 61  , 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0   , 1   , 2   , 3   , 4   , 5   , 6   , 7   , 8   , 9   , 10  , 11  , 12  , 13  , 14  ,
    15  , 16  , 17  , 18  , 19  , 20  , 21  , 22  , 23  , 24  , 25  , 0x80, 0x80, 0x80, 0x80, 63  ,
    0x80, 26  , 27  , 28  , 29  , 30  , 31  , 32  , 33  , 34  , 35  , 36  , 37  , 38  , 39  , 40  ,
    41  , 42  , 43  , 44  , 45  , 46  , 47  , 48  , 49  , 50  , 51  , 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80
};

static constexpr unsigned char base64_chars[64] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};

inline std::string Base64Encode(const unsigned char* src, size_t len)
{
    size_t olen = 4 * ((len + 2) / 3); /* 3-byte blocks to 4-byte */

    if (olen < len)
        return {}; /* integer overflow */

    std::string outStr;
    outStr.resize(olen);
    unsigned char* out = reinterpret_cast<unsigned char*>(outStr.data());

    const unsigned char* end = src + len;
    const unsigned char* in = src;
    unsigned char* pos = out;
    while (end - in >= 3)
    {
        *pos++ = base64_chars[in[0] >> 2u];
        *pos++ = base64_chars[((in[0] & 0x03u) << 4u) | (in[1] >> 4u)];
        *pos++ = base64_chars[((in[1] & 0x0fu) << 2u) | (in[2] >> 6u)];
        *pos++ = base64_chars[in[2] & 0x3fu];
        in += 3;
    }

    if (end - in)
    {
        *pos++ = base64_chars[in[0] >> 2u];
        if (end - in == 1)
        {
            *pos++ = base64_chars[(in[0] & 0x03u) << 4u];
            *pos++ = '=';
        }
        else
        {
            *pos++ = base64_chars[((in[0] & 0x03u) << 4u) | (in[1] >> 4u)];
            *pos++ = base64_chars[(in[1] & 0x0fu) << 2u];
        }
        *pos++ = '=';
    }

    return outStr;
}

inline std::string Base64Decode(const void* data, size_t len)
{
    const unsigned char* p = static_cast<const unsigned char*>(data);
    int pad = len > 0 && (len % 4 || p[len - 1] == '=');
    const size_t L = ((len + 3) / 4 - pad) * 4;
    std::string str;
    str.resize(3 * ((len + 3) / 4));

    int j = 0;
    for (size_t i = 0; i < L; i += 4)
    {
        int n = base64_index[p[i]] << 18u | base64_index[p[i + 1]] << 12u |
            base64_index[p[i + 2]] << 6u | base64_index[p[i + 3]];
        str[j++] = static_cast<char>(n >> 16u);
        str[j++] = static_cast<char>(n >> 8u & 0xFFu);
        str[j++] = static_cast<char>(n & 0xFFu);
    }
    if (pad)
    {
        unsigned n = base64_index[p[L]] << 18u | base64_index[p[L + 1]] << 12u;
        str[j++] = static_cast<char>(n >> 16u);

        if (len > L + 2 && p[L + 2] != '=')
        {
            n |= base64_index[p[L + 2]] << 6u;
            str[j++] = static_cast<char>(n >> 8u & 0xFFu);
        }
    }

    str.resize(j);
    return str;
}
