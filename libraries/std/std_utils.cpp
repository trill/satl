/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "std_utils.h"
#include <context.h>
#include <iomanip>

namespace sa::tl {

Unsigned CombineNumbers(const Table& numbers, Unsigned size)
{
    // Convert a number sequence to a number
    Unsigned result = 0;
    unsigned shift = (numbers.size() - 1) * size;
    for (const auto& n : numbers)
    {
        result |= n.second->ToUint() << (shift * 8);
        shift -= size;
    }
    return result;
}

Value Match(Context& ctx, const Value& value, const Table& cases, const Value& e)
{
    auto key = value.ToKey(ctx);
    if (ctx.stop_)
        return {};

    auto it = cases.find(key);
    if (it != cases.end())
        return *it->second;
    return e;
}

std::string Serialize(const Value& table)
{
    struct Serializer
    {
        static void IterateTable(std::stringstream& result, const Table& table)
        {
            result << "{ ";
            for (const auto& value : table)
            {
                auto key = MakeValue(value.first);
                if (!key->IsString() && !key->IsInt() && !key->IsFloat())
                    // All other key types can not be used for serialization
                    continue;

                if (key->IsString())
                    result << "\"";
                result << key->ToString();
                if (key->IsString())
                    result << "\"";
                result << " = ";

                Serialize(result, *value.second);
                auto e = table.rbegin()->second;
                if (&value != &(*table.rbegin()))
                    result << ", ";
            }
            result << " }";
        }
        static void Serialize(std::stringstream& result, const Value& value)
        {
            if (value.Is<TablePtr>())
                IterateTable(result, *value.As<TablePtr>());
            else
            {
                if (value.IsCallable() || value.IsObject())
                    // Unsupported for serialization
                    return;

                if (value.IsString())
                    result << "\"";
                if (value.IsFloat())
                    result << value.ToFloat();
                else
                    result << value.ToString();
                if (value.IsString())
                    result << "\"";
            }
        }
    };

    std::stringstream result;
    result.imbue(std::locale::classic());
    result << std::fixed;
    result << std::setprecision(6);
    Serializer::Serialize(result, table);
    return result.str();
}

}
