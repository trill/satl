/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "std_utils.h"
#include <value.h>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "../json_load.h"
#include "../json_save.h"
#include "../base64.h"
#include <parser.h>
#include <context.h>

TEST_CASE("CombineNumbers byte", "[std]")
{
    auto tbl = sa::tl::MakeTable();
    int index = 0;
    tbl->Add(index++, sa::tl::MakeValue(0x00));
    tbl->Add(index++, sa::tl::MakeValue(0x00));
    tbl->Add(index++, sa::tl::MakeValue(0x40));
    tbl->Add(index++, sa::tl::MakeValue(0x00));
    auto result = sa::tl::CombineNumbers(*tbl, 1);
    REQUIRE(result == 0x00004000);
}

TEST_CASE("CombineNumbers int", "[std]")
{
    auto tbl = sa::tl::MakeTable();
    int index = 0;

    tbl->Add(index++, sa::tl::MakeValue(0x02BC));
    tbl->Add(index++, sa::tl::MakeValue(0x004E));
    auto result = sa::tl::CombineNumbers(*tbl, 2);
    REQUIRE(result == 0x02BC004E);
}

TEST_CASE("CombineNumbers single int", "[std]")
{
    auto tbl = sa::tl::MakeTable();
    int index = 0;

    tbl->Add(index++, sa::tl::MakeValue(0x02BC));
    auto result = sa::tl::CombineNumbers(*tbl, 2);
    REQUIRE(result == 0x02BC);
}

TEST_CASE("Base64Encode", "[std][base64]")
{
    static constexpr std::string_view Src = "This is only a test!";
    auto result = Base64Encode((const unsigned char*)Src.data(), Src.length());
    REQUIRE(result == "VGhpcyBpcyBvbmx5IGEgdGVzdCE=");
}

TEST_CASE("Base64Decode", "[std][base64]")
{
    static constexpr std::string_view Src = "VGhpcyBpcyBvbmx5IGEgdGVzdCE=";
    auto result = Base64Decode((const unsigned char*)Src.data(), Src.length());
    REQUIRE(result == "This is only a test!");
}

TEST_CASE("JsonLoadString simple", "[std][json]")
{
    static constexpr const char* Src = R"json(
{
    "id" : 1
}
)json";
    auto tbl = JsonLoadString(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(tbl);
    REQUIRE(tbl->size() == 1);
}

TEST_CASE("JsonLoadString object", "[std][json][load]")
{
    static constexpr const char* Src = R"json(
{
  "id" : 1,
  "val" : "string"
}
)json";
    auto tbl = JsonLoadString(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(tbl);
    auto id = tbl->GetValue("id");
    REQUIRE(id);
    REQUIRE(id->Equals(1));

    auto val = tbl->GetValue("val");
    REQUIRE(val);
    REQUIRE(val->Equals(sa::tl::MakeString("string")));
}

TEST_CASE("JsonLoadString array", "[std][json][load]")
{
    static constexpr const char* Src = R"json(
{
  "id" : [1, 2, 3]
}
)json";
    auto tbl = JsonLoadString(Src, [](size_t, const std::string&) {
        REQUIRE(false);
    });
    REQUIRE(tbl);
    auto id = tbl->GetValue("id");
    REQUIRE(id);
    REQUIRE(id->Is<sa::tl::TablePtr>());
    auto idTbl = id->As<sa::tl::TablePtr>();
    REQUIRE(idTbl->size() == 3);
}

TEST_CASE("JsonSave array", "[std][json][save]")
{
    auto tbl = sa::tl::MakeTable();
    tbl->Add(0, sa::tl::MakeValue(1));
    tbl->Add(1, sa::tl::MakeValue(2));
    auto json = JsonSave(*tbl);
//    std::cout << json.Dump() << std::endl;
    REQUIRE(json.Size() == 2);
    sa::json::Integer i1 = json[0u];
    sa::json::Integer i2 = json[1u];
    REQUIRE(i1 == 1);
    REQUIRE(i2 == 2);
}

TEST_CASE("JsonSave array 2", "[std][json][save]")
{
    static constexpr const char* Src = R"tl(
{
    {  1,  2,  3,  4 },
    {  5,  6,  7,  8 },
    {  9, 10, 11, 12 },
    { 13, 14, 15, 16 },
    { true, false, null, 200000.0 },
}
)tl";
    sa::tl::Parser parser(Src);
    auto root = parser.ParseTable(true);
    sa::tl::Context ctx;
    auto value = *root->Evaluate(ctx);
    REQUIRE(value.IsTable());

    auto json = JsonSave(*value.As<sa::tl::TablePtr>());
    REQUIRE(json.Size() == 5);
    sa::json::Integer i = json[1u][0u];
    REQUIRE(i == 5);
//    std::cout << json.Dump() << std::endl;

}

TEST_CASE("JsonSave object", "[std][json][save]")
{
    static constexpr const char* Src = R"tl(
{
    a = {  1,  2,  3,  4 },
    b = {  5,  6,  7,  8 }
}
)tl";
    sa::tl::Parser parser(Src);
    auto root = parser.ParseTable(true);
    sa::tl::Context ctx;
    auto value = *root->Evaluate(ctx);
    REQUIRE(value.IsTable());

    auto json = JsonSave(*value.As<sa::tl::TablePtr>());
    REQUIRE(json.Size() == 2);
    const sa::json::JSON& a = json["a"];
    REQUIRE(a.GetType() == sa::json::JSON::Type::Array);
    sa::json::Integer a0 = a[0u];
    REQUIRE(a0 == 1);
//    std::cout << json.Dump() << std::endl;

}
