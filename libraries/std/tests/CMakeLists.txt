project (std_tests CXX)

file(GLOB TEST_SOURCES std_tests.cpp)

add_executable(std_tests ${TEST_SOURCES})

set_property(TARGET std_tests PROPERTY POSITION_INDEPENDENT_CODE True)
target_link_libraries(std_tests satl std_lib pthread)
add_test(stdTestRuns std_tests)
