/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <string>
#include <json.h>

sa::tl::TablePtr JsonLoadString(const std::string& json, const sa::json::ErrorFunc& error);
sa::tl::TablePtr JsonLoadJson(const sa::json::JSON& json);
