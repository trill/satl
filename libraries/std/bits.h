/**
 * Copyright 2020-2024 Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

// This is a bit like std::bitset, but it allows an arbitrary underlying type,
// i.e. you could also use it with an uint64_t.
// Also there is no class, just some free functions.

#include <string>

namespace sa::bits {

// Return number of bits that fit into T
template <typename T>
constexpr size_t count()
{
    return sizeof(T) * 8;
}

template <typename T, typename U>
constexpr void set(T& bit_set, U bits)
{
    bit_set |= static_cast<T>(bits);
}

template <typename T, typename U>
constexpr void un_set(T& bit_set, U bits)
{
    bit_set &= ~static_cast<T>(bits);
}

// Test if single bit is set in bit_set
template <typename T, typename U>
[[nodiscard]] constexpr bool is_set(T bit_set, U bits)
{
    return (bit_set & static_cast<T>(bits)) == static_cast<T>(bits) && (bits != 0 || bit_set == static_cast<T>(bits));
}

// Test if any bit of bits is set in bit_set
template <typename T, typename U>
[[nodiscard]] constexpr bool is_any_set(T bit_set, U bits)
{
    return (bit_set & static_cast<T>(bits)) != 0 && (bits != 0 || bit_set == static_cast<T>(bits));
}

// Test if no bit in bits is set in bit_set, i.e. !is_any_set()
template <typename T, typename U>
[[nodiscard]] constexpr bool is_none_set(T bit_set, U bits)
{
    return !is_any_set(bit_set, bits);
}

template <typename T, typename U>
[[nodiscard]] constexpr bool equals(T bit_set, U bits)
{
    return bit_set == static_cast<T>(bits);
}

// Flip all bits 0101 -> 1010
template <typename T>
constexpr void flip(T& bit_set)
{
    bit_set = ~bit_set;
}

template <typename T, typename U>
constexpr void flip(T& bit_set, U bits)
{
    bit_set ^= bits;
}

// Convert the bit set to a string, returns e.g. 00000000000000000000000000001010 when bit 2 and bit 4 are set
template <typename T>
std::string to_string(T value)
{
    constexpr size_t c = count<T>();
    std::string result;
    result.resize(c);
    for (size_t i = 1; i <= c; ++i)
        result[c - i] = is_set(value, 1 << (i - 1)) ? '1' : '0';
    return result;
}

}
