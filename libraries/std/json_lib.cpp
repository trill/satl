/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "json_lib.h"
#include <module.h>
#include <fstream>
#include "json_load.h"
#include "json_save.h"

using namespace sa::tl;

void AddJSONLib(sa::tl::Context& ctx)
{
    auto module = MakeModule();
    module->AddFunction<TablePtr, std::string>("load_file",
        [&ctx](const std::string& filename) -> TablePtr
        {
            std::ifstream f(filename);
            if (!f.is_open())
            {
                ctx.RuntimeError(Error::Code::UserDefined, std::format("Unable to open file {}", filename));
                return MakeTable();
            }
            std::stringstream buffer;
            buffer << f.rdbuf();
            return JsonLoadString(buffer.str(), [&ctx](size_t pos, const std::string& msg) {
                ctx.RuntimeError(Error::Code::UserDefined, std::format("JSON parse error at {}: {}", pos, msg));
            });
        });
    module->AddFunction<TablePtr, std::string>("load_string",
        [&ctx](const std::string& json) -> TablePtr
        {
            return JsonLoadString(json, [&ctx](size_t pos, const std::string& msg) {
                ctx.RuntimeError(Error::Code::UserDefined, std::format("JSON parse error at {}: {}", pos, msg));
            });
        });
    module->AddFunction<std::string, TablePtr>("save", &TableToJsonString);

    ctx.AddConst("json", std::move(module), SCOPE_LIBRARY);
}
