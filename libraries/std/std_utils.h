/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <string>

namespace sa::tl {

class Context;

Unsigned CombineNumbers(const Table& numbers, Unsigned size);
Value Match(Context& ctx, const Value& value, const Table& cases, const Value& e);
std::string Serialize(const Value& table);

}
