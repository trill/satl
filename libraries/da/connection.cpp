/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "connection.h"

namespace da {

Connection::Connection(sa::tl::Context& ctx) :
    ctx_(ctx)
{ }

Connection::~Connection() = default;

std::string Connection::Escape(const std::string& value)
{
    return value;
}

std::string Connection::Unescape(const std::string& value)
{
    return value;
}

} // namespace da
