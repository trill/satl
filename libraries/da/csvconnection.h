/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "connection.h"
#include <vector>
#include <fstream>

namespace da {

class CSVConnection final : public Connection
{
    SATL_NON_COPYABLE(CSVConnection);
    SATL_NON_MOVEABLE(CSVConnection);
public:
    CSVConnection(sa::tl::Context& ctx, std::string filename, bool headers, char delim);
    ~CSVConnection() override;
    bool Connect() override;
    sa::tl::TablePtr Next() override;
    std::string Escape(const std::string& value) override;
    std::string Unescape(const std::string& value) override;
private:
    void ReadHeaders();
    std::vector<std::string> ParseLine(const std::string& line) const;
    std::string filename_;
    bool headers_{ false };
    char delim_{ ';' };
    std::fstream stream_;
    std::vector<std::string> columns_;
};

} // namespace da

