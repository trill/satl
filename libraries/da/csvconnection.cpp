/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "csvconnection.h"
#include <utils.h>

namespace da {

CSVConnection::CSVConnection(sa::tl::Context& ctx, std::string filename, bool headers, char delim) :
    Connection(ctx),
    filename_(std::move(filename)),
    headers_(headers),
    delim_(delim)
{
}

CSVConnection::~CSVConnection()
{
    if (stream_.is_open())
    {
        stream_.close();
    }
}

bool CSVConnection::Connect()
{
    if (stream_.is_open())
        return true;
    stream_.open(filename_);
    if (!stream_.is_open())
        return false;
    if (headers_)
        ReadHeaders();
    return true;
}

void CSVConnection::ReadHeaders()
{
    std::string line;
    if (std::getline(stream_, line))
    {
        columns_ = sa::tl::Split(line, std::string(1, delim_), true, true);
    }
}

std::vector<std::string> CSVConnection::ParseLine(const std::string& line) const
{
    std::vector<std::string> result;
    bool quoted = false;
    char last = '\0';
    std::string col;
    for (char c : line)
    {
        if (c == '\"')
        {
            if (last != '\"')
                quoted = !quoted;
            else
            {
                col += '"';
                last = '\0';
                continue;
            }
        }
        else
        {
            if (c == delim_ && !quoted)
            {
                result.push_back(col);
                col.clear();
            }
            else
                col += c;
        }
        last = c;
    }
    if (!col.empty())
        result.push_back(col);
    return result;
}

sa::tl::TablePtr CSVConnection::Next()
{
    std::string line;
    if (std::getline(stream_, line))
    {
        line = sa::tl::Trim(line);
        if (line.empty())
            return sa::tl::MakeTable();

        auto result = sa::tl::MakeTable();
        auto cols = ParseLine(line);
        if (headers_)
        {
            if (columns_.size() != cols.size())
            {
                ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "Column mismatch");
                return {};
            }
            for (size_t i = 0; i < cols.size(); ++i)
                result->Add(columns_[i], sa::tl::MakeValue(Unescape(cols[i])));
        }
        else
        {
            for (size_t i = 0; i < cols.size(); ++i)
                result->Add((sa::tl::Integer)i, sa::tl::MakeValue(Unescape(cols[i])));
        }
        return result;
    }
    return sa::tl::MakeTable();
}

std::string CSVConnection::Escape(const std::string& value)
{
    std::string result = value;
    bool needquotes = sa::tl::ReplaceSubstring(result, std::string("\""), std::string("\"\""));
    if (needquotes || result.find_first_of(" \t" + std::string(1, delim_)) != std::string::npos)
        result = "\"" + result + "\"";
    return result;
}

std::string CSVConnection::Unescape(const std::string& value)
{
    std::string result = value;
    if (result.starts_with('\"') && result.ends_with('\"'))
    {
        result.erase(0);
        result.erase(result.size() - 1);
    }
    sa::tl::ReplaceSubstring(result, std::string("\"\""), std::string("\""));
    return result;
}

} // namespace da
