/**
 * Copyright (c) 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <module.h>
#include "connection.h"
#include "csvconnection.h"

using namespace sa::tl;

extern "C" bool Init(Context& ctx);
extern "C" bool Init(Context& ctx)
{
    if (ctx.HasValue("da"))
        return true;
    
    sa::tl::MetaTable& connMeta = ctx.AddMetatable<da::Connection>();
    connMeta.AddFunction<da::Connection, sa::tl::TablePtr>("next", &da::Connection::Next)
        .AddFunction<da::Connection, std::string, const std::string&>("ecape", &da::Connection::Escape)
        .AddFunction<da::Connection, std::string, const std::string&>("unecape", &da::Connection::Unescape);

    auto module = MakeModule();
    module->AddFunction<sa::tl::Value, const std::string&, bool, const std::string&>("csv",
        [&ctx, &connMeta](const std::string& filename, bool headers, const std::string& delim) -> sa::tl::Value {
            if (delim.size() != 1)
            {
                ctx.RuntimeError(sa::tl::Error::Code::UserDefined, "Delimiter must be one character");
                return {};
            }
            auto* connection = ctx.GetGC().CreateObject<da::CSVConnection>(ctx, filename, headers, delim[0]);
            if (connection->Connect())
            {
                sa::tl::Value v = sa::tl::MakeObject(connMeta, connection);
                return v;
            }
            ctx.GetGC().Remove(connection);
            ctx.RuntimeError(sa::tl::Error::Code::UserDefined, "Unable to open file " + filename);
            return {};
        });

    ctx.AddConst("da", std::move(module), SCOPE_LIBRARY);
    return true;
}
