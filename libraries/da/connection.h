/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <context.h>
#include <non_copyable.h>

namespace da {

class Connection
{
    SATL_NON_COPYABLE(Connection);
    SATL_NON_MOVEABLE(Connection);
public:
    explicit Connection(sa::tl::Context& ctx);
    virtual ~Connection();
    virtual bool Connect() = 0;
    virtual sa::tl::TablePtr Next() = 0;
    virtual std::string Escape(const std::string& value);
    virtual std::string Unescape(const std::string& value);
protected:
    sa::tl::Context& ctx_;
};

} // namespace da
