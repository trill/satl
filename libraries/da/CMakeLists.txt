project (satl_da CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(satl_da SHARED ${SOURCES})
set_property(TARGET satl_da PROPERTY POSITION_INDEPENDENT_CODE ON)
target_link_libraries(satl_da satl)

install(TARGETS satl_da LIBRARY)
