# ext_lib

sa::tl extension library example. This library is compiled to a shared object (`.so`) and loaded at runtime.
Then it calls the `bool Init(sa::tl::Context& ctx)` function which the library must export. In this function
the library can add modules and functions to the Context. The library is unloaded when the Context is destroyed.

## Load a library

To load a library into a Context, use the `Context::LoadExtLibrary()` function which expects the name of the library.

