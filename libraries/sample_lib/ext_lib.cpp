/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <module.h>

class TestClass
{
public:
    int GetValue() const { return value_; }
    void SetValue(int value) { value_ = value; }
    void Say(std::string what)
    {
        std::cout << what << std::endl;
    }
    int GetValueX(int x) { return value_ * x; }
protected:
    int value_{ 0 };
};

class Derived : public TestClass
{
public:
    int DerivedFunc(int x) { return value_ + x; }
};

static std::unique_ptr<Derived> object;

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("ext"))
        return true;
    auto module = sa::tl::MakeModule();
    module->AddFunction<void, std::string>("test_func",
        [](const std::string& str) -> void
        {
            std::cout << str << " form ext_lib" << std::endl;
        });

    // We can also bind C++ classes here:
    auto& baseMeta = ctx.AddMetatable<TestClass>();
    baseMeta.AddFunction<TestClass, void, std::string>("Say", &TestClass::Say)
        .AddFunction<TestClass, int, int>("GetValueX", &TestClass::GetValueX)
        .AddProperty<TestClass>("value", &TestClass::GetValue, &TestClass::SetValue);

    auto& derivedMeta = ctx.AddMetatable<Derived, TestClass>();
    derivedMeta.AddFunction<Derived, int, int>("DereivedFunc", &Derived::DerivedFunc);

    object = std::make_unique<Derived>();
    // Tell the script engine what type of class it is
    module->SetInstance<Derived>(derivedMeta, "ext_object", *object);

    ctx.AddConst("ext", std::move(module), 0);

    return true;
}
