/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <iostream>

namespace sa::tl {

void DumpValue(std::ostream& os, const Value& value, int indent, bool withType);

}
