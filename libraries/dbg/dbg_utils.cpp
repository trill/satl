/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "dbg_utils.h"
#include <metatable.h>

namespace sa::tl {

void DumpValue(std::ostream& os, const Value& value, int indent, bool withType)
{
    struct Dumper
    {
        static void IterateTable(std::ostream& os, const TablePtr& table, int indent, bool withType)
        {
            for (const auto& value : *table)
            {
                auto val = MakeValue(value.first);
                os << std::string(indent, ' ');
                if (withType)
                    os << "[" << val->GetTypeName() << "] ";
                os << val->ToString() << " = ";
                if (value.second->Is<TablePtr>())
                {
                    if (withType)
                        os << "[table @ " << value.second.get() << "]";
                    os << std::endl;
                }
                Dump(os, *value.second, indent + 2, withType);
            }
        }
        static void DumpMeta(std::ostream& os, const MetaTable& meta, int indent, bool withType)
        {
            os << meta.GetClassName() << std::endl;
            for (const auto& func : meta.GetFunctions())
            {
                os << std::string(indent + 2, ' ');
                if (withType)
                    os << "[function @ " << func.second.get() << "] ";
                os << func.first << " (" << func.second->ParamCount() << ")";
                if (func.second->Const())
                    os << " [const]";
                os << std::endl;
            }
            for (const auto& func : meta.GetPropertyGetters())
            {
                os << std::string(indent + 2, ' ');
                if (withType)
                    os << "[property @ " << func.second.get() << "] ";
                os << func.first;
                if (meta.GetPropertySetters().find(func.first) != meta.GetPropertySetters().end())
                    os << " (read/write)";
                else
                    os << " (readonly)";
                os << std::endl;
            }
            for (const auto& func : meta.GetOperators())
            {
                os << std::string(indent + 2, ' ');
                if (withType)
                    os << "[operator @ " << func.second.get() << "] ";
                os << func.first;
                if (func.second->Const())
                    os << " [const]";
                os << std::endl;
            }
            if (const auto* baseMeta = meta.GetBase())
            {
                DumpMeta(os, *baseMeta, indent, withType);
            }
        }
        static void Dump(std::ostream& os, const Value& value, int indent, bool withType)
        {
            if (value.Is<TablePtr>())
                IterateTable(os, value.As<TablePtr>(), indent, withType);
            else
            {
                if (withType)
                {
                    os << "[";
                    if (value.IsConst())
                        os << "const ";
                    os << value.GetTypeName() << " @ ";
                    if (value.IsObject())
                        os << value.As<ObjectPtr>()->GetInstance();
                    else
                        os << &value;
                    os << "] ";
                }
                if (value.IsObject())
                    DumpMeta(os, value.As<ObjectPtr>()->GetMetatable(), indent, withType);
                else
                    os << value.ToString() << std::endl;
            }
        }
    };
    Dumper::Dump(os, value, indent, withType);
}

}
