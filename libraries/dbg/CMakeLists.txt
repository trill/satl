project (dbg_lib CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(dbg_lib ${SOURCES})
target_link_libraries(dbg_lib satl)
target_include_directories(dbg_lib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
set_property(TARGET dbg_lib PROPERTY POSITION_INDEPENDENT_CODE True)
