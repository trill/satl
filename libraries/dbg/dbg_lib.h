/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace sa::tl {

class Context;

void AddDbgLib(Context& ctx);

}
