/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "dbg_lib.h"
#include <iostream>
#include <context.h>
#include "dbg_utils.h"

namespace sa::tl {

void AddDbgLib(Context& ctx)
{
    if (ctx.GetValue("assert"))
        return;
    ctx.AddFunction("assert",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            if (args.empty())
                return MakeNull();
            if (args[0]->ToBool())
                return args[0];

            ctx.RuntimeError(Error::Code::UserDefined, args.size() > 1 ? args[1]->ToString() : "Assertion failed");
            return args[0];
        }, SCOPE_LIBRARY);
    ctx.AddFunction("trace",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            std::cout << ctx.GetLocation().line << "," << ctx.GetLocation().col << ": ";
            for (const auto& arg : args)
                std::cout << arg->ToString() << "\t";
            std::cout << std::endl;
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction<Integer, ValuePtr>("address_of",
        [](const ValuePtr& value) -> Integer
        {
            if (value->IsObject())
                return (Integer)value->As<ObjectPtr>()->GetInstance();
            return (Integer)value.get();
        }, SCOPE_LIBRARY);
    ctx.AddFunction("dump",
        [](Context&, const FunctionArguments& args) -> ValuePtr {
            if (args.empty())
                return MakeNull();

            const auto& value = args[0];
            bool withType = args.size() > 1 ? args[1]->ToBool() : false;
            DumpValue(std::cout, *value, 0, withType);
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction("dump_vars",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr {
            bool withType = !args.empty() ? args[0]->ToBool() : false;
            bool withLibs = args.size() > 1 ? args[1]->ToBool() : false;
            ctx.ForEachVariable([&](const Variable& current) {
                std::cout << current.nameString;
                std::cout << std::endl;
                DumpValue(std::cout, *current.value, 0, withType);
                return true;
            }, withLibs);
            return MakeNull();
        }, SCOPE_LIBRARY);
    ctx.AddFunction<void>("backtrace",
        [&ctx]() {
            for (const auto& nd : ctx.GetCallstack())
            {
                std::cout << nd->loc_ << ": " << nd->GetFriendlyName() << std::endl;
            }
        }, SCOPE_LIBRARY);
}

}
