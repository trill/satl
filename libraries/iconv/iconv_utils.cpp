/**
 * Copyright 2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "iconv_utils.h"
#include <context.h>
#include <iconv.h>
#include <cerrno>
#include <format>
#include <vector>
#include <cstring>
#include <scope_guard.h>

std::string IconvConvert(sa::tl::Context& ctx, const std::string& srcCoding, const std::string& dstCoding, const std::string& input, bool ignoreError)
{
    iconv_t conv = iconv_open(dstCoding.c_str(), srcCoding.c_str());
    if (conv == (iconv_t)-1)
    {
        if (errno == EINVAL)
        {
            ctx.RuntimeError(
                sa::tl::Error::UserDefined, std::format("Unsupported conversion from {} to {}", srcCoding, dstCoding));
            return {};
        }
        ctx.RuntimeError(sa::tl::Error::UserDefined, std::strerror(errno));
        return {};
    }

    sa::tl::ScopeGuard sg([&]
    {
        iconv_close(conv);
    });
    std::vector<char> inBuf(input.begin(), input.end());
    char* srcPtr = inBuf.data();
    size_t srcSize = input.size();

    std::vector<char> buf(1024);
    std::string dst;
    dst.reserve(input.size());
    while (0 < srcSize)
    {
        char* dstPtr = buf.data();
        size_t dstSize = buf.size();
        size_t res = iconv(conv, &srcPtr, &srcSize, &dstPtr, &dstSize);
        if (res == (size_t)-1)
        {
            if (errno == E2BIG)
                // ignore this error
                continue;
            if (ignoreError)
            {
                // skip character
                ++srcPtr;
                --srcSize;
                continue;
            }

            switch (errno)
            {
            case EILSEQ:
            case EINVAL:
                ctx.RuntimeError(sa::tl::Error::UserDefined, "Invalid multibyte characters");
                return {};
            default:
                ctx.RuntimeError(sa::tl::Error::UserDefined, std::strerror(errno));
                return {};
            }
        }
        dst.append(buf.data(), buf.size() - dstSize);
    }
    return dst;
}
