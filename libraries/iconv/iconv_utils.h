/**
 * Copyright 2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace sa::tl {
class Context;
}

std::string IconvConvert(sa::tl::Context& ctx, const std::string& srcCoding, const std::string& dstCoding, const std::string& input, bool ignoreError);
