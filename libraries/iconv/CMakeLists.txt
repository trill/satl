project (satl_iconv CXX)

find_package(Iconv)

if (Iconv_FOUND)
    file(GLOB SOURCES *.cpp *.h)

    add_library(satl_iconv SHARED ${SOURCES})
    set_property(TARGET satl_iconv PROPERTY POSITION_INDEPENDENT_CODE ON)

    target_include_directories(satl_iconv PRIVATE ${Iconv_INCLUDE_DIRS})
    target_link_libraries(satl_iconv satl ${Iconv_LIBRARIES})
    install(TARGETS satl_iconv LIBRARY)
endif(Iconv_FOUND)
