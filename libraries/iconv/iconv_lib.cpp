/**
 * Copyright (c) 2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <metatable.h>
#include <module.h>
#include <node.h>
#include <context.h>
#include <value.h>
#include "iconv_utils.h"

using namespace sa::tl;

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("iconv"))
        return true;

    auto module = sa::tl::MakeModule();
    module->AddFunction<std::string, const std::string&, const std::string&, const std::string&>("convert",
        [&ctx](const std::string& srcCoding, const std::string& dstCoding, const std::string& input) -> std::string
        {
            return IconvConvert(ctx, srcCoding, dstCoding, input, true);
        });

    ctx.AddConst("iconv", std::move(module), SCOPE_LIBRARY);

    return true;
}
