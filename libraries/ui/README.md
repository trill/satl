# ui_lib

## Introduction

GTK 3 binding.

## Requirements

* GTK 3

## Screenshot

![Screenshot](sample.png?raw=true)

## Minimal example

~~~
// Load the libary
load_lib("libsatl_ui.so");
// Create a window
wnd = ui.window("Hello UI");

wnd.on_closing = function(sender)
{
    // Exit event loop when closing the window
    ui.quit();
};

// Show the window
wnd.show();

// Run event loop
ui.main();
~~~

## Loading Window from table

~~~
load_lib("libsatl_ui.so");

wnd_def =
{
    type = "window",
    name = "Hello UI",
    margins = { left = 5, top = 5, right = 5, bottom = 5 },
    position = { x = 10, y = 50 },
    size = { x = 300, y = 200 },
    children =
    {
        {
            type = "entry",
            name = "Textbox",
        },
        {
            type = "combobox",
            name = "combo",
            items = { "Item1", "Item2" }
        },
        {
            type = "horizontal_scale",
            name = "hscale",
            range = { min = 10, max = 42 }
        },
        {
            type = "button",
            name = "OK",
            // Assigning a function expression also works
            on_clicked = function()
            {
                this.window.close();
            }
        },
    },
    on_closing = ui.quit
};

wnd = ui.load(wnd_def);
wnd.show();
ui.main();
~~~

![Screenshot](sample2.png?raw=true)

## API

See the Manual for the API documentation.
