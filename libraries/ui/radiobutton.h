/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

class Radiobutton final : public Widget
{
public:
    Radiobutton(sa::tl::Context& ctx, std::string name, Radiobutton* member = nullptr);
    ~Radiobutton();

    [[nodiscard]] bool GetActive() const;
    void SetActive(bool value);
    void JoinGroup(const sa::tl::ObjectPtr& member);
    [[nodiscard]] sa::tl::CallablePtr GetOnToggled() const { return onToggled_; }
    void SetOnToggled(sa::tl::CallablePtr value) { onToggled_ = std::move(value); }
private:
    static void OnToggled(GtkToggleButton* button, Radiobutton* data);
    sa::tl::CallablePtr onToggled_;
    bool updating_{ false };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Radiobutton>(sa::tl::Context& ctx);
}
