/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "cairosurface.h"
#include <cairo-pdf.h>
#include <cairo-svg.h>
#include <cairo-ps.h>

namespace ui {

CairoSurface::CairoSurface(sa::tl::Context& ctx) :
    Object(ctx)
{ }

CairoSurface::CairoSurface(sa::tl::Context& ctx, cairo_surface_t* surface) :
    Object(ctx),
    surface_(surface)
{ }

CairoSurface::~CairoSurface()
{
    if (own_ && surface_)
    {
        cairo_surface_destroy(surface_);
        surface_ = nullptr;
    }
}

void CairoSurface::Finish()
{
    if (!surface_)
        return;
    cairo_surface_finish(surface_);
}

void CairoSurface::Flush()
{
    if (!surface_)
        return;
    cairo_surface_flush(surface_);
}

bool CairoSurface::CreateImageSurface(cairo_format_t format, int width, int height)
{
    surface_ = cairo_image_surface_create(format, width, height);
    own_ = true;
    auto status = cairo_surface_status(surface_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoSurface::CreatePDFSurface(std::string filename, sa::tl::Float width, sa::tl::Float height)
{
    surface_ = cairo_pdf_surface_create(filename.c_str(), width, height);
    own_ = true;
    auto status = cairo_surface_status(surface_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoSurface::CreatePSSurface(std::string filename, sa::tl::Float width, sa::tl::Float height)
{
    surface_ = cairo_ps_surface_create(filename.c_str(), width, height);
    own_ = true;
    auto status = cairo_surface_status(surface_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoSurface::CreateSVGSurface(std::string filename, sa::tl::Float width, sa::tl::Float height)
{
    surface_ = cairo_svg_surface_create(filename.c_str(), width, height);
    own_ = true;
    auto status = cairo_surface_status(surface_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoSurface::CreateFromPNG(std::string filename)
{
    surface_ = cairo_image_surface_create_from_png(filename.c_str());
    own_ = true;
    auto status = cairo_surface_status(surface_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoSurface::CreateForRectangle(const sa::tl::ObjectPtr& target, sa::tl::Float x, sa::tl::Float y, sa::tl::Float width, sa::tl::Float height)
{
    if (CairoSurface* t = sa::tl::MetaTable::GetObject<CairoSurface>(target))
    {
        surface_ = cairo_surface_create_for_rectangle(t->GetHandle(), x, y, width, height);
        own_ = true;
        auto status = cairo_surface_status(surface_);
        if (status != CAIRO_STATUS_SUCCESS)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
            return false;
        }
        return true;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "CairoSurface object exprected");
    return false;
}

bool CairoSurface::CreateSimilar(const sa::tl::ObjectPtr& other, int content, sa::tl::Float width, sa::tl::Float height)
{
    if (CairoSurface* t = sa::tl::MetaTable::GetObject<CairoSurface>(other))
    {
        surface_ = cairo_surface_create_similar(t->GetHandle(), (cairo_content_t)content, width, height);
        own_ = true;
        auto status = cairo_surface_status(surface_);
        if (status != CAIRO_STATUS_SUCCESS)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
            return false;
        }
        return true;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "CairoSurface object exprected");
    return false;
}

bool CairoSurface::CreateSimilarImage(const sa::tl::ObjectPtr& other, int format, sa::tl::Float width, sa::tl::Float height)
{
    if (CairoSurface* t = sa::tl::MetaTable::GetObject<CairoSurface>(other))
    {
        surface_ = cairo_surface_create_similar_image(t->GetHandle(), (cairo_format_t)format, width, height);
        own_ = true;
        auto status = cairo_surface_status(surface_);
        if (status != CAIRO_STATUS_SUCCESS)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
            return false;
        }
        return true;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "CairoSurface object exprected");
    return false;
}

void CairoSurface::ShowPage()
{
    if (!surface_)
        return;
    cairo_surface_show_page(surface_);
}

void CairoSurface::CopyPage()
{
    if (!surface_)
        return;
    cairo_surface_copy_page(surface_);
}

void CairoSurface::MarkDirty()
{
    if (!surface_)
        return;
    cairo_surface_mark_dirty(surface_);
}

void CairoSurface::MarkDirtyRectangle(int x, int y, int width, int height)
{
    if (!surface_)
        return;
    cairo_surface_mark_dirty_rectangle(surface_, x, y, width, height);
}

int CairoSurface::GetContent() const
{
    if (!surface_)
        return 0;
    return cairo_surface_get_content(surface_);
}

bool CairoSurface::SetSize(const sa::tl::Value& width, const sa::tl::Value& height)
{
    if (!surface_)
        return false;

    auto type = cairo_surface_get_type(surface_);
    switch (type)
    {
    case CAIRO_SURFACE_TYPE_IMAGE:
        return false;
    case CAIRO_SURFACE_TYPE_PDF:
        cairo_pdf_surface_set_size(surface_, width.ToFloat(), height.ToFloat());
        return true;
    case CAIRO_SURFACE_TYPE_PS:
        cairo_ps_surface_set_size(surface_, width.ToFloat(), height.ToFloat());
        return true;
    case CAIRO_SURFACE_TYPE_SVG:
        return false;
    default:
        return false;
    }
}

void CairoSurface::SetEPS(bool value)
{
    if (!surface_)
        return;
    if (cairo_surface_get_type(surface_) == CAIRO_SURFACE_TYPE_PS)
        cairo_ps_surface_set_eps(surface_, value);
}

bool CairoSurface::GetEPS() const
{
    if (!surface_)
        return false;
    if (cairo_surface_get_type(surface_) == CAIRO_SURFACE_TYPE_PS)
        return cairo_ps_surface_get_eps(surface_);
    return false;
}

bool CairoSurface::WriteToPNG(const std::string& filename)
{
    if (!surface_)
        return false;
    if (cairo_surface_get_type(surface_) == CAIRO_SURFACE_TYPE_IMAGE)
    {
        auto status = cairo_surface_write_to_png(surface_, filename.c_str());
        if (status != CAIRO_STATUS_SUCCESS)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
            return false;
        }
        return true;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "Not an image surface");
    return false;
}

sa::tl::TablePtr CairoSurface::GetDeviceScale() const
{
    if (!surface_)
        return sa::tl::MakeTable();
    double x, y;
    cairo_surface_get_device_scale(surface_, &x, &y);
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("x", x);
    result->Add<sa::tl::Float>("y", y);
    return result;
}

void CairoSurface::SetDeviceScale(const sa::tl::Table& value)
{
    if (!surface_)
        return;
    double x = 0, y = 0;
    sa::tl::GetTableValue(ctx_, value, "x", x);
    sa::tl::GetTableValue(ctx_, value, "y", y);
    cairo_surface_set_device_scale(surface_, x, y);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::CairoSurface>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::CairoSurface, Object>();
    result
        .AddFunction<ui::CairoSurface, bool, const sa::tl::Value&, const sa::tl::Value&>("set_size", &ui::CairoSurface::SetSize)
        .AddFunction<ui::CairoSurface, void>("finish", &ui::CairoSurface::Finish)
        .AddFunction<ui::CairoSurface, void>("flush", &ui::CairoSurface::Flush)
        .AddFunction<ui::CairoSurface, void>("show_page", &ui::CairoSurface::ShowPage)
        .AddFunction<ui::CairoSurface, void>("copy_page", &ui::CairoSurface::CopyPage)
        .AddFunction<ui::CairoSurface, void>("mark_dirty", &ui::CairoSurface::MarkDirty)
        .AddFunction<ui::CairoSurface, void, int , int, int, int>("mark_dirt_rectangle", &ui::CairoSurface::MarkDirtyRectangle)
        .AddFunction<ui::CairoSurface, bool, const std::string&>("write_to_png", &ui::CairoSurface::WriteToPNG)
        .AddProperty("eps", &ui::CairoSurface::GetEPS, &ui::CairoSurface::SetEPS)
        .AddProperty("content", &ui::CairoSurface::GetContent)
        .AddProperty("device_scale", &ui::CairoSurface::GetDeviceScale, &ui::CairoSurface::SetDeviceScale);

    return result;
}

}
