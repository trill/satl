/**
 * Copyright 2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

class Calendar : public Widget
{
    SATL_NON_COPYABLE(Calendar);
    SATL_NON_MOVEABLE(Calendar);
    TYPED_OBJECT(Calendar);
public:
    Calendar(sa::tl::Context& ctx, std::string name);
    ~Calendar() override;

    void ClearMarks();
    void MarkDay(unsigned day);
    void UnmarkDay(unsigned day);
    bool DayIsMarked(unsigned day);

    sa::tl::TablePtr GetValue() const;
    void SetValue(const sa::tl::Table& value);
    int GetDisplayOptions() const;
    void SetDisplayOptions(int value);

    [[nodiscard]] sa::tl::CallablePtr GetOnDaySelected() const { return onDaySelected_; }
    void SetOnDaySelected(sa::tl::CallablePtr value) { onDaySelected_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnDaySelectedDoubleClick() const { return onDaySelectedDoubleClick_; }
    void SetOnDaySelectedDoubleClick(sa::tl::CallablePtr value) { onDaySelectedDoubleClick_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnMonthChanged() const { return onMonthChanged_; }
    void SetOnMonthChanged(sa::tl::CallablePtr value) { onMonthChanged_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnNextMonth() const { return onNextMonth_; }
    void SetOnNextMonth(sa::tl::CallablePtr value) { onNextMonth_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnPrevMonth() const { return onPrevMonth_; }
    void SetOnPrevMonth(sa::tl::CallablePtr value) { onPrevMonth_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnNextYear() const { return onNextYear_; }
    void SetOnNextYear(sa::tl::CallablePtr value) { onNextYear_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnPrevYear() const { return onPrevYear_; }
    void SetOnPrevYear(sa::tl::CallablePtr value) { onPrevYear_ = std::move(value); }

private:
    static void OnDaySelected(GtkButton* button, Calendar* sender);
    static void OnDaySelectedDoubleClick(GtkButton* button, Calendar* sender);
    static void OnMonthChanged(GtkButton* button, Calendar* sender);
    static void OnNextMonth(GtkButton* button, Calendar* sender);
    static void OnNextYear(GtkButton* button, Calendar* sender);
    static void OnPrevMonth(GtkButton* button, Calendar* sender);
    static void OnPrevYear(GtkButton* button, Calendar* sender);
    sa::tl::CallablePtr onDaySelected_;
    sa::tl::CallablePtr onDaySelectedDoubleClick_;
    sa::tl::CallablePtr onMonthChanged_;
    sa::tl::CallablePtr onNextMonth_;
    sa::tl::CallablePtr onNextYear_;
    sa::tl::CallablePtr onPrevMonth_;
    sa::tl::CallablePtr onPrevYear_;
};

}

namespace sa::tl::bind {
template<>
sa::tl::MetaTable& Register<ui::Calendar>(sa::tl::Context& ctx);
}
