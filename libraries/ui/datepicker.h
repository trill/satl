/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

class DatePicker final : public Widget
{
    SATL_NON_COPYABLE(DatePicker);
    SATL_NON_MOVEABLE(DatePicker);
    TYPED_OBJECT(DatePicker);
public:
    DatePicker(sa::tl::Context& ctx, std::string name);

    [[nodiscard]] sa::tl::TablePtr GetValue() const;
    void SetValue(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
private:
    static void OnSpinChanged(GtkSpinButton*, DatePicker* data);
    static void OnComboChanged(GtkComboBox*, DatePicker* data);
    sa::tl::CallablePtr onChanged_;
    gulong changedHandlerYears_{ 0 };
    gulong changedHandlerMonths_{ 0 };
    gulong changedHandlerDays_{ 0 };
    GtkWidget* years_{ nullptr };
    GtkWidget* months_{ nullptr };
    GtkWidget* days_{ nullptr };
};

} // namespace ui

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::DatePicker>(sa::tl::Context& ctx);
}
