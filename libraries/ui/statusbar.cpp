/**
 * Copyright (c) 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "statusbar.h"

namespace ui {

void Statusbar::OnTextPopped(GtkStatusbar*, guint context_id, gchar* text, Statusbar* data)
{
    if (data->onTextPopped_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onTextPopped_),
                0,
                2,
                [&](int index)
                {
                    switch (index)
                    {
                        case 0:
                            args.push_back(sa::tl::MakeValue(context_id));
                            break;
                        case 1:
                            args.push_back(sa::tl::MakeValue(std::string(text)));
                            break;
                    }
                }))
            return;
        (*data->onTextPopped_)(data->ctx_, *data->GetSelfPtr(), args);
    }
}

void Statusbar::OnTextPushed(GtkStatusbar*, guint context_id, gchar* text, Statusbar* data)
{
    if (data->onTextPushed_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onTextPushed_),
                0,
                2,
                [&](int index)
                {
                    switch (index)
                    {
                        case 0:
                            args.push_back(sa::tl::MakeValue(context_id));
                            break;
                        case 1:
                            args.push_back(sa::tl::MakeValue(std::string(text)));
                            break;
                    }
                }))
            return;
        (*data->onTextPushed_)(data->ctx_, *data->GetSelfPtr(), args);
    }
}

Statusbar::Statusbar(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_statusbar_new(), std::forward<std::string>(name))
{
    g_signal_connect(GetHandle(), "text-popped", G_CALLBACK(Statusbar::OnTextPopped), this);
    g_signal_connect(GetHandle(), "text-pushed", G_CALLBACK(Statusbar::OnTextPushed), this);
    gtk_widget_show(GetHandle());
}

Statusbar::~Statusbar() = default;

int Statusbar::GetContextId(const std::string& contextDesciption)
{
    return gtk_statusbar_get_context_id(GTK_STATUSBAR(GetHandle()), contextDesciption.c_str());
}

void Statusbar::Pop(int contextId)
{
    gtk_statusbar_pop(GTK_STATUSBAR(GetHandle()), contextId);
}

int Statusbar::Push(int contextId, const std::string& text)
{
    return gtk_statusbar_push(GTK_STATUSBAR(GetHandle()), contextId, text.c_str());
}

void Statusbar::Remove(int contextId, int messageId)
{
    gtk_statusbar_remove(GTK_STATUSBAR(GetHandle()), contextId, messageId);
}

void Statusbar::RemoveAll(int contextId)
{
    gtk_statusbar_remove_all(GTK_STATUSBAR(GetHandle()), contextId);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Statusbar>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Statusbar, ui::Widget>();
    result
        .AddFunction<ui::Statusbar, int, const std::string&>("get_context_id", &ui::Statusbar::GetContextId)
        .AddFunction<ui::Statusbar, void, int>("pop", &ui::Statusbar::Pop)
        .AddFunction<ui::Statusbar, int, int, const std::string&>("push", &ui::Statusbar::Push)
        .AddFunction<ui::Statusbar, void, int, int>("remove", &ui::Statusbar::Remove)
        .AddFunction<ui::Statusbar, void, int>("remove_all", &ui::Statusbar::RemoveAll)
        .AddProperty("on_text_popped", &ui::Statusbar::GetOnTextPopped, &ui::Statusbar::SetOnTextPopped)
        .AddProperty("on_text_pushed", &ui::Statusbar::GetOnTextPushed, &ui::Statusbar::SetOnTextPushed);

    return result;
}

}

