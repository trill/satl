/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <gtk/gtk.h>
#include "window.h"
#include <ast.h>
#include "menu.h"
#include <factory.h>

namespace ui {

gboolean Window::OnClosing(GtkWidget*, GdkEvent*, Window* wnd)
{
    if (wnd->onClosing_)
    {
        auto self = wnd->GetSelfPtr();
        (*wnd->onClosing_)(wnd->ctx_, *self, {});
        return false;
    }
    // TRUE to stop other handlers from being invoked for the event. FALSE to propagate the event further.
    return true;
}

gboolean Window::OnWindowState(GtkWidget*, GdkEventWindowState* event, Window* wnd)
{
    if ((event->changed_mask & GDK_WINDOW_STATE_ICONIFIED) == 0 &&
        (event->changed_mask & GDK_WINDOW_STATE_MAXIMIZED) == 0 &&
        (event->changed_mask & GDK_WINDOW_STATE_FULLSCREEN) == 0)
        return false;

    if (event->new_window_state & GDK_WINDOW_STATE_ICONIFIED)
        wnd->windowState_ = WindowState::Minimized;
    else if (event->new_window_state & GDK_WINDOW_STATE_MAXIMIZED)
        wnd->windowState_ = WindowState::Maximized;
    else if (event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN)
        wnd->windowState_ = WindowState::Fullscreen;
    else if ((event->new_window_state & GDK_WINDOW_STATE_ICONIFIED) == 0 &&
        (event->new_window_state & GDK_WINDOW_STATE_MAXIMIZED) == 0 &&
        (event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN) == 0)
        wnd->windowState_ = WindowState::Normal;

    if (wnd->onWindowState_)
        (*wnd->onWindowState_)(wnd->ctx_, *wnd->GetSelfPtr(), {});

    // FALSE to propagate the event further.
    return false;
}

Window::Window(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_window_new(GTK_WINDOW_TOPLEVEL), std::forward<std::string>(name)),
    container_(GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0)))
{
    gtk_widget_set_events(GetHandle(), GDK_BUTTON_PRESS_MASK | GDK_KEY_PRESS_MASK);
    // A window can only have one child
    gtk_widget_set_hexpand(GTK_WIDGET(container_), TRUE);
    gtk_widget_set_halign(GTK_WIDGET(container_), GTK_ALIGN_FILL);
    gtk_widget_set_vexpand(GTK_WIDGET(container_), TRUE);
    gtk_widget_set_valign(GTK_WIDGET(container_), GTK_ALIGN_FILL);
    gtk_container_add(GTK_CONTAINER(GetHandle()), GTK_WIDGET(container_));
    SetTitle(GetName());
    g_signal_connect(GTK_WINDOW(GetHandle()), "delete-event", G_CALLBACK(Window::OnClosing), this);
    gtk_widget_show(GTK_WIDGET(container_));
}

Window::~Window() = default;

void Window::Show()
{
    if (visible_)
        return;

    gtk_window_present(GTK_WINDOW(GetHandle()));
    visible_ = true;
    g_signal_connect(GetHandle(), "window-state-event", G_CALLBACK(Window::OnWindowState), this);

    if (onShow_)
        (*onShow_)(ctx_, *GetSelfPtr(), {});
}

void Window::Close()
{
    gtk_window_close(GTK_WINDOW(GetHandle()));
    visible_ = false;
}

void Window::Resize(int width, int height)
{
    gtk_window_resize(GTK_WINDOW(GetHandle()), width, height);
}

void Window::Move(int x, int y)
{
    gtk_window_move(GTK_WINDOW(GetHandle()), x, y);
}

void Window::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    const auto& o = child.As<sa::tl::ObjectPtr>();
    if (MenuBar* w = o->GetMetatable().CastTo<MenuBar>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        menubar_ = *w;
        gtk_box_pack_start(GTK_BOX(container_), w->GetHandle(), FALSE, FALSE, 0);
        InternalAdd(w);
        return;
    }
    if (auto* w = o->GetMetatable().CastTo<Menu>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        MenuBar* mb = menubar_.GetPtr<MenuBar, Object>();
        if (!mb)
        {
            mb = sa::tl::bind::CreateInstancePtr<MenuBar>(ctx_, ctx_, "Menubar");
            menubar_ = *mb;
            gtk_box_pack_start(GTK_BOX(container_), mb->GetHandle(), FALSE, FALSE, 0);
            InternalAdd(mb);
        }
        mb->Add(child);
        return;
    }

    Widget::Add(child);
}

void Window::Maximize()
{
    gtk_window_maximize(GTK_WINDOW(GetHandle()));
}

void Window::Minimize()
{
    gtk_window_iconify(GTK_WINDOW(GetHandle()));
}

void Window::Unminimize()
{
    gtk_window_deiconify(GTK_WINDOW(GetHandle()));
}

void Window::Fullscreen()
{
    gtk_window_fullscreen(GTK_WINDOW(GetHandle()));
}

std::string Window::GetTitle() const
{
    return { gtk_window_get_title(GTK_WINDOW(GetHandle())) };
}

void Window::SetTitle(const std::string& value)
{
    gtk_window_set_title(GTK_WINDOW(GetHandle()), value.c_str());
}

sa::tl::TablePtr Window::GetPosition() const
{
    int x = 0, y = 0;
    gtk_window_get_position(GTK_WINDOW(GetHandle()), &x, &y);
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add("x", x);
    result->Add("y", y);
    return result;
}

void Window::SetPosition(const sa::tl::Table& value)
{
    int x = 0, y = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "x", x))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "y", y))
        return;
    gtk_window_move(GTK_WINDOW(GetHandle()), x, y);
}

sa::tl::TablePtr Window::GetSize() const
{
    int x = 0, y = 0;
    gtk_window_get_size(GTK_WINDOW(GetHandle()), &x, &y);
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add("x", x);
    result->Add("y", y);
    return result;
}

void Window::SetSize(const sa::tl::Table& value)
{
    int x = 0, y = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "x", x))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "y", y))
        return;
    gtk_window_resize(GTK_WINDOW(GetHandle()), x, y);
}

sa::tl::TablePtr Window::GetDefaultSize() const
{
    int x = 0, y = 0;
    gtk_window_get_default_size(GTK_WINDOW(GetHandle()), &x, &y);
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add("x", x);
    result->Add("y", y);
    return result;
}

void Window::SetDefaultSize(const sa::tl::Table& value)
{
    int x = 0, y = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "x", x))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "y", y))
        return;
    gtk_window_set_default_size(GTK_WINDOW(GetHandle()), x, y);
}

bool Window::GetResizeable() const
{
    return gtk_window_get_resizable(GTK_WINDOW(GetHandle()));
}

void Window::SetResizeable(bool value)
{
    gtk_window_set_resizable(GTK_WINDOW(GetHandle()), value ? TRUE : FALSE);
}

int Window::GetWindowType() const
{
    return gtk_window_get_type_hint(GTK_WINDOW(GetHandle()));
}

void Window::SetWindowType(int value)
{
    gtk_window_set_type_hint(GTK_WINDOW(GetHandle()), (GdkWindowTypeHint)value);
}

void Window::SetMargins(const sa::tl::Table& value)
{
    int left = 0, top = 0, right = 0, bottom = 0;

    if (!sa::tl::GetTableValue(ctx_, value, "left", left))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "top", top))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "right", right))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "bottom", bottom))
        return;
    gtk_widget_set_margin_start(GTK_WIDGET(container_), left);
    gtk_widget_set_margin_top(GTK_WIDGET(container_), top);
    gtk_widget_set_margin_end(GTK_WIDGET(container_), right);
    gtk_widget_set_margin_bottom(GTK_WIDGET(container_), bottom);
}

sa::tl::TablePtr Window::GetMargins() const
{
    auto result = sa::tl::MakeTable();
    result->Add("left", gtk_widget_get_margin_start(GTK_WIDGET(container_)));
    result->Add("top", gtk_widget_get_margin_top(GTK_WIDGET(container_)));
    result->Add("right", gtk_widget_get_margin_end(GTK_WIDGET(container_)));
    result->Add("bottoom", gtk_widget_get_margin_bottom(GTK_WIDGET(container_)));

    return result;
}

sa::tl::TablePtr Window::GetRect() const
{
    gint x = 0; gint y = 0; gint width = 0; gint height = 0;
    gtk_window_get_position(GTK_WINDOW(GetHandle()), &x, &y);
    gtk_window_get_size(GTK_WINDOW(GetHandle()), &width, &height);
    auto result = sa::tl::MakeTable();
    result->Add("left", x);
    result->Add("top", y);
    result->Add("right", x + width);
    result->Add("bottom", y + height);

    return result;
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Window>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Window, ui::Widget>();
    result
        .AddFunction<ui::Window, void>("close", &ui::Window::Close)
        .AddFunction<ui::Window, void>("maximize", &ui::Window::Maximize)
        .AddFunction<ui::Window, void>("minimize", &ui::Window::Minimize)
        .AddFunction<ui::Window, void>("unminimize", &ui::Window::Unminimize)
        .AddFunction<ui::Window, void>("fullscreen", &ui::Window::Fullscreen)
        // Change size of ui::Window: ui::Window.resize(new_width, new_height)
        .AddFunction<ui::Window, void, int, int>("resize", &ui::Window::Resize)
        // Move the ui::Window: ui::Window(new_x, new_y)
        .AddFunction<ui::Window, void, int, int>("move", &ui::Window::Move)
        .AddProperty("title", &ui::Window::GetTitle, &ui::Window::SetTitle)
        .AddProperty("window_state", &ui::Window::GetWindowState)
        .AddProperty("position", &ui::Window::GetPosition, &ui::Window::SetPosition)
        .AddProperty("size", &ui::Window::GetSize, &ui::Window::SetSize)
        .AddProperty("default_size", &ui::Window::GetDefaultSize, &ui::Window::SetDefaultSize)
        .AddProperty("resizeable", &ui::Window::GetResizeable, &ui::Window::SetResizeable)
        .AddProperty("window_type", &ui::Window::GetWindowType, &ui::Window::SetWindowType)
        .AddProperty("on_closing", &ui::Window::GetOnClosing, &ui::Window::SetOnClosing)
        .AddProperty("on_window_state", &ui::Window::GetOnWindowState, &ui::Window::SetOnWindowState)
        .AddProperty("on_show", &ui::Window::GetOnShow, &ui::Window::SetOnShow);
    return result;
}

}
