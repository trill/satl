/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <object.h>

namespace ui {

class Timer final : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Timer);
    SATL_NON_MOVEABLE(Timer);
    TYPED_OBJECT(Timer);
public:
    Timer(sa::tl::Context& ctx, int intervalMs);
    ~Timer() override;
    void Start();
    void Stop();
    [[nodiscard]] bool IsRunning() const { return running_; }
    [[nodiscard]] bool IsSingle() const { return single_; }
    void SetSingle(bool value);
    [[nodiscard]] sa::tl::CallablePtr GetOnTimer() const { return onTimer_; }
    void SetOnTimer(sa::tl::CallablePtr value) { onTimer_ = std::move(value); }
private:
    int intervalMs_;
    bool running_{ false };
    unsigned timer_{ 0 };
    bool single_{ false };
    sa::tl::CallablePtr onTimer_;
    bool DoOnTimer();
    static gboolean OnTimer(void* data);
};

void Timeout(sa::tl::Context& ctx, int inervalMs, sa::tl::CallablePtr callback);
sa::tl::Value Interval(sa::tl::Context& ctx, int inervalMs, sa::tl::CallablePtr callback);

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Timer>(sa::tl::Context& ctx);
}
