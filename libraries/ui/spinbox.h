/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Spinbox final : public Widget
{
    SATL_NON_COPYABLE(Spinbox);
    SATL_NON_MOVEABLE(Spinbox);
    TYPED_OBJECT(Spinbox);
public:
    Spinbox(sa::tl::Context& ctx, std::string name);
    ~Spinbox() override;

    [[nodiscard]] int GetValue() const;
    void SetValue(int value);
    [[nodiscard]] sa::tl::TablePtr GetRange() const;
    void SetRange(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
private:
    static void OnValueChanged(GtkSpinButton*, Spinbox* data);
    sa::tl::CallablePtr onChanged_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Spinbox>(sa::tl::Context& ctx);
}
