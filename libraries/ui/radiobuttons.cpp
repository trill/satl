/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "radiobuttons.h"
#include <ast.h>

namespace ui {

void Radiobuttons::OnToggled(GtkToggleButton* tb, Radiobuttons* btn)
{
    if (btn->updating_)
        return;
    if (!gtk_toggle_button_get_active(tb))
        return;
    if (btn->onToggled_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(btn->ctx_,
                (*btn->onToggled_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(btn->GetValue()));
                }))
            return;
        (*btn->onToggled_)(btn->ctx_, *btn->GetSelfPtr(), args);
    }
}

Radiobuttons::Radiobuttons(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_box_new(GTK_ORIENTATION_VERTICAL, 0), std::forward<std::string>(name))
{
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_widget_show(GetHandle());
}

Radiobuttons::~Radiobuttons() = default;

sa::tl::TablePtr Radiobuttons::GetItems() const
{
    auto result = sa::tl::MakeTable();
    for (const auto& item : items_)
        result->Add(sa::tl::MakeValue(item));
    return result;
}

void Radiobuttons::SetItems(const sa::tl::TablePtr& value)
{
    if (!items_.empty())
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "Can not set radio items when it already has items");
        return;
    }
    for (const auto& item : *value)
        AddItem(item.second->ToString());
}

void Radiobuttons::AddItem(std::string text)
{
    GtkRadioButton* previous = nullptr;
    if (!buttons_.empty())
        previous = GTK_RADIO_BUTTON(buttons_.back());
    GtkWidget* b = gtk_radio_button_new_with_label_from_widget(previous, text.c_str());
    buttons_.push_back(b);
    g_signal_connect(b, "toggled", G_CALLBACK(Radiobuttons::OnToggled), this);
    gtk_container_add(GTK_CONTAINER(GetHandle()), b);
    gtk_widget_show(b);
    items_.push_back(std::move(text));
}

int Radiobuttons::GetSelected() const
{
    for (int i = 0; i < (int)buttons_.size(); ++i)
    {
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(buttons_[i])))
            return i;
    }
    return -1;
}

void Radiobuttons::SetSelected(int value)
{
    updating_ = true;
    for (int i = 0; i < (int)buttons_.size(); ++i)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(buttons_[i]), i == value);
    updating_ = false;
}

sa::tl::Value Radiobuttons::GetValue() const
{
    int sel = GetSelected();
    if (sel >= 0 && sel < (int)items_.size())
        return sa::tl::MakeString(items_[sel]);
    return nullptr;
}

void Radiobuttons::SetValue(const sa::tl::Value& value)
{
    if (value.IsString())
    {
        const auto& v = *value.As<sa::tl::StringPtr>();
        for (size_t i = 0; i < items_.size(); ++i)
        {
            if (items_[i] == v)
            {
                SetSelected((int)i);
                break;
            }
        }
        return;
    }

    if (value.IsNull())
    {
        SetSelected(-1);
        return;
    }

    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String or null exprected");
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Radiobuttons>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Radiobuttons, ui::Widget>();
    result
        .AddFunction<ui::Radiobuttons, void, std::string>("add_item", &ui::Radiobuttons::AddItem)

        .AddProperty("selected", &ui::Radiobuttons::GetSelected, &ui::Radiobuttons::SetSelected)
        .AddProperty("value", &ui::Radiobuttons::GetValue, &ui::Radiobuttons::SetValue)
        .AddProperty("items", &ui::Radiobuttons::GetItems, &ui::Radiobuttons::SetItems)
        .AddProperty("on_changed", &ui::Radiobuttons::GetOnChanged, &ui::Radiobuttons::SetOnChanged);
    return result;
}

}
