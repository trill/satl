/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "grid.h"
#include <ast.h>
#include "menu.h"

namespace ui {

Grid::Grid(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_grid_new(), std::forward<std::string>(name))
{
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_widget_show(GetHandle());
}

Grid::~Grid() = default;

void Grid::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    auto& o = child.As<sa::tl::ObjectPtr>();
    if (Widget* w = o->GetMetatable().CastTo<Widget>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        gtk_grid_attach(GTK_GRID(GetHandle()), w->GetHandle(), GetLeft(), GetTop(), 1, 1);
        InternalAdd(w);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not Widget instance");
}

void Grid::Add(Widget* child)
{
    SATL_ASSERT(child);
    if (child->HasParent())
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
        return;
    }
    gtk_grid_attach(GTK_GRID(GetHandle()), child->GetHandle(), GetLeft(), GetTop(), 1, 1);
    InternalAdd(child);
}

sa::tl::Value Grid::GetChildAt(int left, int top)
{
    auto* child = gtk_grid_get_child_at(GTK_GRID(GetHandle()), left, top);
    if (!child)
        return nullptr;
    auto* childWidget = GetWidgetFromHandle(child, false);
    if (!childWidget)
        return nullptr;
    const auto* meta = ctx_.GetMetatable(childWidget->GetTypeName());
    if (meta)
        return sa::tl::MakeObject(*meta, childWidget);
    return nullptr;
}

int Grid::GetRowSpacing() const
{
    return (int)gtk_grid_get_row_spacing(GTK_GRID(GetHandle()));
}

void Grid::SetRowSpacing(int value)
{
    gtk_grid_set_row_spacing(GTK_GRID(GetHandle()), value);
}

int Grid::GetColSpacing() const
{
    return (int)gtk_grid_get_column_spacing(GTK_GRID(GetHandle()));
}

void Grid::SetColSpacing(int value)
{
    gtk_grid_set_column_spacing(GTK_GRID(GetHandle()), value);
}

bool Grid::GetColHomogeneous() const
{
    return gtk_grid_get_column_homogeneous(GTK_GRID(GetHandle()));
}

void Grid::SetColHomogeneous(bool value)
{
    gtk_grid_set_column_homogeneous(GTK_GRID(GetHandle()), value);
}

bool Grid::GetRowHomogeneous() const
{
    return gtk_grid_get_row_homogeneous(GTK_GRID(GetHandle()));
}

void Grid::SetRowHomogeneous(bool value)
{
    gtk_grid_set_row_homogeneous(GTK_GRID(GetHandle()), value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Grid>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Grid, ui::Widget>();
    result
        .AddFunction<ui::Grid, sa::tl::Value, int, int>("get_child_at", &ui::Grid::GetChildAt)
        .AddProperty("num_cols", &ui::Grid::GetNumCols, &ui::Grid::SetNumCols)
        .AddProperty("col_homogeneous", &ui::Grid::GetColHomogeneous, &ui::Grid::SetColHomogeneous)
        .AddProperty("row_homogeneous", &ui::Grid::GetRowHomogeneous, &ui::Grid::SetRowHomogeneous)
        .AddProperty("row_spacing", &ui::Grid::GetRowSpacing, &ui::Grid::SetRowSpacing)
        .AddProperty("col_spacing", &ui::Grid::GetColSpacing, &ui::Grid::SetColSpacing);
    return result;
}

}
