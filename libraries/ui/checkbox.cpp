/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "checkbox.h"
#include <ast.h>

namespace ui {

void Checkbox::OnToggled(GtkToggleButton*, Checkbox *btn)
{
    if (btn->onToggled_)
        (*btn->onToggled_)(btn->ctx_, *btn->GetSelfPtr(), {});
}

Checkbox::Checkbox(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_check_button_new_with_label(name.c_str()), name)
{
    g_signal_connect(GetHandle(), "toggled", G_CALLBACK(Checkbox::OnToggled), this);
    gtk_widget_show(GetHandle());
}

Checkbox::~Checkbox() = default;

std::string Checkbox::GetText() const
{
    return gtk_button_get_label(GTK_BUTTON(GetHandle()));
}

void Checkbox::SetText(const std::string& value)
{
    gtk_button_set_label(GTK_BUTTON(GetHandle()), value.c_str());
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Checkbox>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Checkbox, ui::Widget>();
    result
        .AddProperty("text", &ui::Checkbox::GetText, &ui::Checkbox::SetText)
        .AddProperty("on_toggled", &ui::Checkbox::GetOnToggled, &ui::Checkbox::SetOnToggled);
    return result;
}

}
