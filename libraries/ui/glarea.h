/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

// A widget that allows drawing with OpenGL.
class GLArea : public Widget
{
    SATL_NON_COPYABLE(GLArea);
    SATL_NON_MOVEABLE(GLArea);
    TYPED_OBJECT(GLArea);
public:
    GLArea(sa::tl::Context& ctx, std::string name);
    ~GLArea() override;

    void MakeCurrent();

    [[nodiscard]] sa::tl::CallablePtr GetOnRender() const { return onRealize_; }
    void SetOnRender(sa::tl::CallablePtr value) { onRealize_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnResize() const { return onResize_; }
    void SetOnResize(sa::tl::CallablePtr value) { onResize_ = std::move(value); }
protected:
    virtual bool DoOnRender(GdkGLContext*);
    virtual void DoOnResize(int width, int height);
private:
    sa::tl::CallablePtr onRealize_;
    sa::tl::CallablePtr onResize_;
    static bool OnRender(GtkGLArea*, GdkGLContext* context, GLArea* area);
    static void OnResize(GtkGLArea*, int width, int height, GLArea* area);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::GLArea>(sa::tl::Context& ctx);
}
