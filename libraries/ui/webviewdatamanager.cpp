/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifdef HAVE_WEBKIT

#include "webviewdatamanager.h"
#include <satl_assert.h>
#include "webviewcookiemanager.h"
#include <factory.h>

namespace ui {

WebViewDataManager::WebViewDataManager(sa::tl::Context& ctx) :
    Object(ctx)
{
}

WebViewDataManager::WebViewDataManager(sa::tl::Context& ctx, WebKitWebsiteDataManager* handle) :
    Object(ctx),
    handle_(handle)
{ }

bool WebViewDataManager::Create(const sa::tl::Table& options)
{
    std::vector<std::pair<std::string, std::string>> opts;

    for (const auto& opt : options)
        opts.emplace_back(sa::tl::KeyToString(opt.first), opt.second->ToString());

    // This is not nice :(
    switch (opts.size())
    {
    case 1:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    nullptr);
        break;
    case 2:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    nullptr);
        break;
    case 3:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    opts[2].first.c_str(), opts[2].second.c_str(),
                    nullptr);
        break;
    case 4:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    opts[2].first.c_str(), opts[2].second.c_str(),
                    opts[3].first.c_str(), opts[3].second.c_str(),
                    nullptr);
        break;
    case 5:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    opts[2].first.c_str(), opts[2].second.c_str(),
                    opts[3].first.c_str(), opts[3].second.c_str(),
                    opts[4].first.c_str(), opts[4].second.c_str(),
                    nullptr);
        break;
    case 6:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    opts[2].first.c_str(), opts[2].second.c_str(),
                    opts[3].first.c_str(), opts[3].second.c_str(),
                    opts[4].first.c_str(), opts[4].second.c_str(),
                    opts[5].first.c_str(), opts[5].second.c_str(),
                    nullptr);
        break;
    case 7:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    opts[2].first.c_str(), opts[2].second.c_str(),
                    opts[3].first.c_str(), opts[3].second.c_str(),
                    opts[4].first.c_str(), opts[4].second.c_str(),
                    opts[5].first.c_str(), opts[5].second.c_str(),
                    opts[6].first.c_str(), opts[6].second.c_str(),
                    nullptr);
        break;
    case 8:
        handle_ = webkit_website_data_manager_new(
                    opts[0].first.c_str(), opts[0].second.c_str(),
                    opts[1].first.c_str(), opts[1].second.c_str(),
                    opts[2].first.c_str(), opts[2].second.c_str(),
                    opts[3].first.c_str(), opts[3].second.c_str(),
                    opts[4].first.c_str(), opts[4].second.c_str(),
                    opts[5].first.c_str(), opts[5].second.c_str(),
                    opts[6].first.c_str(), opts[6].second.c_str(),
                    opts[7].first.c_str(), opts[7].second.c_str(),
                    nullptr);
        break;
    default:
        ctx_.RuntimeError(sa::tl::Error::InvalidArgument, "Wrong number of options, expecting 1 to 8");
        return false;
    }

    return !!handle_;
}

bool WebViewDataManager::CreateEphemeral()
{
    handle_ = webkit_website_data_manager_new_ephemeral();
    return !!handle_;
}

bool WebViewDataManager::IsEphemeral() const
{
    SATL_ASSERT(handle_);
    return webkit_website_data_manager_is_ephemeral(handle_);
}

std::string WebViewDataManager::GetBaseDataDirectory() const
{
    SATL_ASSERT(handle_);
    if (const auto* value = webkit_website_data_manager_get_base_data_directory(handle_))
        return value;
    return "";
}

std::string WebViewDataManager::GetBaseCacheDirectory() const
{
    SATL_ASSERT(handle_);
    if (const auto* value = webkit_website_data_manager_get_base_cache_directory(handle_))
        return value;
    return "";
}

sa::tl::Value WebViewDataManager::GetCookieManager() const
{
    auto* meta = ctx_.GetMetatable<WebViewCookieManager>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<WebViewCookieManager>(ctx_, *meta, ctx_, webkit_website_data_manager_get_cookie_manager(GetHandle()));
}

bool WebViewDataManager::IsItpEnabled() const
{
    SATL_ASSERT(handle_);
    return webkit_website_data_manager_get_itp_enabled(handle_);
}

void WebViewDataManager::SetItpEnabled(bool value)
{
    SATL_ASSERT(handle_);
    webkit_website_data_manager_set_itp_enabled(handle_, value);
}

bool WebViewDataManager::IsPersistentCredenticalStorageEanbled() const
{
    SATL_ASSERT(handle_);
    return webkit_website_data_manager_get_persistent_credential_storage_enabled(handle_);
}

void WebViewDataManager::SetPersistentCredenticalStorageEanbled(bool value)
{
    SATL_ASSERT(handle_);
    webkit_website_data_manager_set_persistent_credential_storage_enabled(handle_, value);
}

int WebViewDataManager::GetTlsErrorPolicy() const
{
    SATL_ASSERT(handle_);
    return webkit_website_data_manager_get_tls_errors_policy(handle_);
}

void WebViewDataManager::SetTlsErrorPolicy(int value)
{
    SATL_ASSERT(handle_);
    webkit_website_data_manager_set_tls_errors_policy(handle_, (WebKitTLSErrorsPolicy)value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::WebViewDataManager>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::WebViewDataManager, sa::tl::bind::Object>();
    result
        .AddProperty("ephemeral", &ui::WebViewDataManager::IsEphemeral)
        .AddProperty("cookie_manager", &ui::WebViewDataManager::GetCookieManager)
        .AddProperty("base_data_directory", &ui::WebViewDataManager::GetBaseDataDirectory)
        .AddProperty("base_cache_directory", &ui::WebViewDataManager::GetBaseCacheDirectory)
        .AddProperty("itp_enabled", &ui::WebViewDataManager::IsItpEnabled, &ui::WebViewDataManager::SetItpEnabled)
        .AddProperty("persistent_credential_storage_enabled", &ui::WebViewDataManager::IsPersistentCredenticalStorageEanbled, &ui::WebViewDataManager::SetPersistentCredenticalStorageEanbled)
        .AddProperty("tls_error_policy", &ui::WebViewDataManager::GetTlsErrorPolicy, &ui::WebViewDataManager::SetTlsErrorPolicy);
    return result;
}

}

#endif
