/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Button : public Widget
{
    SATL_NON_COPYABLE(Button);
    SATL_NON_MOVEABLE(Button);
    TYPED_OBJECT(Button);
public:
    Button(sa::tl::Context& ctx, std::string name);
    ~Button() override;

    [[nodiscard]] std::string GetText() const;
    void SetText(const std::string& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnClicked() const { return onClicked_; }
    void SetOnClicked(sa::tl::CallablePtr value) { onClicked_ = std::move(value); }
protected:
    Button(sa::tl::Context& ctx, GtkWidget* handle, std::string name);
private:
    static void OnClicked(GtkButton* button, Button* btn);
    sa::tl::CallablePtr onClicked_;
};

class ColorButton : public Button
{
    SATL_NON_COPYABLE(ColorButton);
    SATL_NON_MOVEABLE(ColorButton);
    TYPED_OBJECT(ColorButton);
public:
    static const GdkRGBA black;
    ColorButton(sa::tl::Context& ctx, std::string name);
    ~ColorButton() override;
    void SetColor(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::TablePtr GetColor() const;
    [[nodiscard]] sa::tl::CallablePtr GetOnColorSet() const { return onColorSet_; }
    void SetOnColorSet(sa::tl::CallablePtr value) { onColorSet_ = std::move(value); }
private:
    static void OnColorSet(GtkButton* button, ColorButton* btn);
    sa::tl::CallablePtr onColorSet_;
};

class FontButton : public Button
{
    SATL_NON_COPYABLE(FontButton);
    SATL_NON_MOVEABLE(FontButton);
    TYPED_OBJECT(FontButton);
public:
    FontButton(sa::tl::Context& ctx, std::string name);
    ~FontButton() override;
    [[nodiscard]] sa::tl::TablePtr GetFont() const;
    [[nodiscard]] sa::tl::CallablePtr GetOnFontSet() const { return onFontSet_; }
    void SetOnFontSet(sa::tl::CallablePtr value) { onFontSet_ = std::move(value); }
private:
    static void OnFontSet(GtkFontButton* button, FontButton* btn);
    sa::tl::CallablePtr onFontSet_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Button>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::ColorButton>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::FontButton>(sa::tl::Context& ctx);
}
