/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "list_box.h"
#include <ast.h>
#include "menu.h"

namespace ui {

void ListBox::OnRowSelected(GtkListBox*, GtkListBoxRow* row, ListBox* data)
{
    if (data->onSelected_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onSelected_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(row ? gtk_list_box_row_get_index(row) : -1));
                }))
            return;

        (*data->onSelected_)(data->ctx_, *data->GetSelfPtr(), args);
    }
}

ListBox::ListBox(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_list_box_new(), std::forward<std::string>(name))
{
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(GetHandle()), GTK_SELECTION_SINGLE);
    selectedEvent_ = g_signal_connect(GetHandle(), "row-selected", G_CALLBACK(ListBox::OnRowSelected), this);
    gtk_widget_show(GetHandle());
}

ListBox::~ListBox() = default;

void ListBox::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    const auto& o = child.As<sa::tl::ObjectPtr>();
    if (Widget* w = o->GetMetatable().CastTo<Widget>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        gtk_list_box_insert(GTK_LIST_BOX(GetHandle()), w->GetHandle(), -1);
        InternalAdd(w);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not Widget instance");
}

bool ListBox::IsSelected(int index)
{
    if (index < 0)
        return false;
    auto* row = gtk_list_box_get_row_at_index(GTK_LIST_BOX(GetHandle()), index);
    if (!row)
        return false;
    return gtk_list_box_row_is_selected(row);
}

int ListBox::GetSelected() const
{
    auto* row = gtk_list_box_get_selected_row(GTK_LIST_BOX(GetHandle()));
    if (!row)
        return -1;
    return gtk_list_box_row_get_index(row);
}

void ListBox::SetSelected(int value)
{
    auto* row = gtk_list_box_get_row_at_index(GTK_LIST_BOX(GetHandle()), value);
    if (!row)
        return;
    g_signal_handler_block(GetHandle(), selectedEvent_);
    gtk_list_box_select_row(GTK_LIST_BOX(GetHandle()), row);
    g_signal_handler_unblock(GetHandle(), selectedEvent_);
}

bool ListBox::GetMultiselect() const
{
    return gtk_list_box_get_selection_mode(GTK_LIST_BOX(GetHandle())) == GTK_SELECTION_MULTIPLE;
}

void ListBox::SetMultiselect(bool value)
{
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(GetHandle()), value ? GTK_SELECTION_MULTIPLE : GTK_SELECTION_SINGLE);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::ListBox>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::ListBox, ui::Widget>();
    result
        .AddFunction<ui::ListBox, bool, int>("is_selected", &ui::ListBox::IsSelected)
        .AddProperty("selected", &ui::ListBox::GetSelected, &ui::ListBox::SetSelected)
        .AddProperty("multi_select", &ui::ListBox::GetMultiselect, &ui::ListBox::SetMultiselect)
        .AddProperty("on_selected", &ui::ListBox::GetOnSelected, &ui::ListBox::SetOnSelected);
    return result;
}

}
