/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "menu.h"
#include <ast.h>
#include <factory.h>

namespace ui {

MenuBar::MenuBar(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_menu_bar_new(), std::forward<std::string>(name))
{
    gtk_widget_show(GetHandle());
}

MenuBar::~MenuBar() = default;

void MenuBar::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    const auto& o = child.As<sa::tl::ObjectPtr>();
    if (Menu* w = o->GetMetatable().CastTo<Menu>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        auto* subMenuItem = sa::tl::bind::CreateInstancePtr<MenuItem>(ctx_, ctx_, w->GetName());
        gtk_menu_item_set_submenu(GTK_MENU_ITEM(subMenuItem->GetHandle()), w->GetHandle());
        gtk_menu_shell_append(GTK_MENU_SHELL(GetHandle()), subMenuItem->GetHandle());
        InternalAdd(w);
        return;
    }
    if (MenuItem* w = o->GetMetatable().CastTo<MenuItem>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        gtk_menu_shell_append(GTK_MENU_SHELL(GetHandle()), w->GetHandle());
        InternalAdd(w);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Menu or MenuItem instance");
}

Menu::Menu(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_menu_new(), std::forward<std::string>(name))
{
    gtk_widget_show(GetHandle());
}

Menu::~Menu() = default;

void Menu::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    const auto& o = child.As<sa::tl::ObjectPtr>();
    if (Menu* w = o->GetMetatable().CastTo<Menu>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        auto* subMenuItem = sa::tl::bind::CreateInstancePtr<MenuItem>(ctx_, ctx_, w->GetName());
        gtk_menu_item_set_submenu(GTK_MENU_ITEM(subMenuItem->GetHandle()), w->GetHandle());
        gtk_menu_shell_append(GTK_MENU_SHELL(GetHandle()), subMenuItem->GetHandle());
        InternalAdd(w);
        return;
    }
    if (MenuItem* w = o->GetMetatable().CastTo<MenuItem>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        gtk_menu_shell_append(GTK_MENU_SHELL(GetHandle()), w->GetHandle());
        InternalAdd(w);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Menu or MenuItem instance");
}

void Menu::Popup()
{
    gtk_menu_popup_at_pointer(GTK_MENU(GetHandle()), nullptr);
}

void MenuItem::OnActivated(GtkMenuItem*, MenuItem* data)
{
    if (data->onActivated_)
        (*data->onActivated_)(data->ctx_, *data->GetSelfPtr(), {});
}

MenuItem::MenuItem(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, name.empty() ? gtk_menu_item_new() : gtk_menu_item_new_with_label(name.c_str()), name)
{
    g_signal_connect(GetHandle(), "activate", G_CALLBACK(MenuItem::OnActivated), this);
    gtk_widget_show(GetHandle());
}

MenuItem::MenuItem(sa::tl::Context& ctx, GtkWidget* handle, std::string name) :
    Widget(ctx, handle, std::forward<std::string>(name))
{
    g_signal_connect(GetHandle(), "activate", G_CALLBACK(MenuItem::OnActivated), this);
    gtk_widget_show(GetHandle());
}

MenuItem::~MenuItem() = default;

MenuCheckItem::MenuCheckItem(sa::tl::Context& ctx, std::string name) :
    MenuItem(ctx, name.empty() ? gtk_check_menu_item_new() : gtk_check_menu_item_new_with_label(name.c_str()), name)
{ }

MenuCheckItem::~MenuCheckItem() = default;

bool MenuCheckItem::GetActive() const
{
    return gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(GetHandle()));
}

void MenuCheckItem::SetActive(bool value)
{
    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(GetHandle()), value);
}

MenuSeparatorItem::MenuSeparatorItem(sa::tl::Context& ctx, std::string name) :
    MenuItem(ctx, gtk_separator_menu_item_new(), std::forward<std::string>(name))
{ }

MenuSeparatorItem::~MenuSeparatorItem() = default;

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::MenuBar>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::MenuBar, ui::Widget>();
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::Menu>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Menu, ui::Widget>();
    result.AddFunction<ui::Menu, void>("popup", &ui::Menu::Popup);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::MenuItem>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::MenuItem, ui::Widget>();
    result.AddProperty("on_activated", &ui::MenuItem::GetOnActivated, &ui::MenuItem::SetOnActivated);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::MenuCheckItem>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::MenuCheckItem, ui::MenuItem>();
    result.AddProperty("active", &ui::MenuCheckItem::GetActive, &ui::MenuCheckItem::SetActive);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::MenuSeparatorItem>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::MenuSeparatorItem, ui::MenuItem>();
    return result;
}

}
