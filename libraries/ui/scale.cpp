/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "scale.h"
#include <ast.h>

namespace ui {

void Scale::OnValueChanged(GtkScale*, Scale* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

Scale::Scale(sa::tl::Context& ctx, std::string name, bool horiz) :
    Widget(ctx, gtk_scale_new_with_range(horiz ? GTK_ORIENTATION_HORIZONTAL : GTK_ORIENTATION_VERTICAL, 0.0, 100.0, 1), std::forward<std::string>(name))
{
    gtk_scale_set_digits(GTK_SCALE(GetHandle()), 0);
    changedHandler_ = g_signal_connect(GetHandle(), "value-changed", G_CALLBACK(Scale::OnValueChanged), this);
    gtk_widget_show(GetHandle());
}

Scale::~Scale() = default;

sa::tl::Float Scale::GetValue() const
{
    return (sa::tl::Float)gtk_range_get_value(GTK_RANGE(GetHandle()));
}

void Scale::SetValue(sa::tl::Float value)
{
    g_signal_handler_block(GetHandle(), changedHandler_);
    gtk_range_set_value(GTK_RANGE(GetHandle()), (gdouble)value);
    g_signal_handler_unblock(GetHandle(), changedHandler_);
}

sa::tl::TablePtr Scale::GetRange() const
{
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("min", min_);
    result->Add<sa::tl::Float>("max", max_);
    return result;
}

void Scale::SetRange(const sa::tl::Table& value)
{
    gdouble min = 0.0, max = 0.0;
    if (!sa::tl::GetTableValue(ctx_, value, "min", min))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "max", max))
        return;
    min_ = min;
    max_ = max;
    gtk_range_set_range(GTK_RANGE(GetHandle()), min_, max_);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Scale>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Scale, ui::Widget>();
    result
        .AddProperty("range", &ui::Scale::GetRange, &ui::Scale::SetRange)
        .AddProperty("value", &ui::Scale::GetValue, &ui::Scale::SetValue)
        .AddProperty("on_changed", &ui::Scale::GetOnChanged, &ui::Scale::SetOnChanged);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::HorizontalScale>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::HorizontalScale, ui::Scale>();
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::VerticalScale>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::VerticalScale, ui::Scale>();
    return result;
}

}
