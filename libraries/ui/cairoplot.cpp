/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "cairoplot.h"
#include "cairo.h"
#include "cairopattern.h"
#include <cmath>

namespace ui {

CairoPlot::CairoPlot(sa::tl::Context& ctx) :
    Object(ctx)
{
}

CairoPlot::~CairoPlot() = default;

void CairoPlot::AddData(const std::string& graph, sa::tl::TablePtr data)
{
    data_[graph].push_back(std::move(data));
}

int CairoPlot::GetDataCount(const std::string& graph)
{
    auto it = data_.find(graph);
    if (it == data_.end())
        return 0;
    return (int)it->second.size();
}

sa::tl::TablePtr CairoPlot::GetData(const std::string& graph, int index)
{
    auto it = data_.find(graph);
    if (it == data_.end())
        return sa::tl::MakeTable();
    if (index < (int)it->second.size())
        return it->second[index];
    return sa::tl::MakeTable();
}

void CairoPlot::DeleteData(const std::string& graph, int index)
{
    auto it = data_.find(graph);
    if (it == data_.end())
        return;
    if (index < (int)it->second.size())
    {
        it->second.erase(it->second.begin() + index);
    }
}

void CairoPlot::DeleteGraph(const std::string& graph)
{
    data_.erase(graph);
}

void CairoPlot::ClearGraph(const std::string& graph)
{
    auto it = data_.find(graph);
    if (it == data_.end())
        return;
    it->second.clear();
}

void CairoPlot::ClearData()
{
    data_.clear();
}

int CairoPlot::GetGraphCount() const
{
    return (int)data_.size();
}

bool CairoPlot::GraphExists(const std::string& graph)
{
    return data_.find(graph) != data_.end();
}

void CairoPlot::DrawBorder(Cairo& cairo)
{
    cairo.Save();
    if (auto* pattern = borderPattern.GetPtr<CairoPattern, Object>())
    {
        cairo.InternalSetSource(*pattern);
    }
    cairo.MoveTo(cairo.GetLineWidth(), cairo.GetLineWidth());
    cairo.LineTo(width_ - cairo.GetLineWidth(), cairo.GetLineWidth());
    cairo.LineTo(width_ - cairo.GetLineWidth(), height_ - cairo.GetLineWidth());
    cairo.LineTo(cairo.GetLineWidth(), height_ - cairo.GetLineWidth());
    cairo.ClosePath();
    cairo.Stroke();
    cairo.Restore();
}

void CairoPlot::DrawXAxix(Cairo& cairo, const DataLimits& limits, const Rect& plotRect)
{
    cairo.Save();
    if (auto* pattern = xAxis_.pattern.GetPtr<CairoPattern, Object>())
    {
        cairo.InternalSetSource(*pattern);
    }

    auto labelHeight = cairo.GetTextHeight(xAxis_.label);
    if (!xAxis_.label.empty())
    {
        auto labelWidth = cairo.GetTextWidth(xAxis_.label);
        cairo.MoveTo(plotRect.Right() / 2 - labelWidth / 2, plotRect.Bottom() + labelHeight + cairo.GetLineWidth());
        cairo.ShowText(xAxis_.label);
    }
    if (xAxis_.showLimits)
    {
        {
            std::string text = std::format("{}", limits.minX);
            cairo.MoveTo(plotRect.x, plotRect.Bottom() + labelHeight + cairo.GetLineWidth());
            cairo.ShowText(text);
        }
        {
            std::string text = std::format("{}", limits.maxX);
            cairo.MoveTo(plotRect.Right() - cairo.GetTextWidth(text), plotRect.Bottom() + labelHeight + cairo.GetLineWidth());
            cairo.ShowText(text);
        }
    }

    cairo.MoveTo(plotRect.x, plotRect.Bottom() - cairo.GetLineWidth() / 2);
    cairo.LineTo(plotRect.Right(), plotRect.Bottom() - cairo.GetLineWidth() / 2);
    cairo.Stroke();
    cairo.Restore();
}

void CairoPlot::DrawYAxix(Cairo& cairo, const DataLimits& limits, const Rect& plotRect)
{
    cairo.Save();
    if (auto* pattern = yAxis_.pattern.GetPtr<CairoPattern, Object>())
    {
        cairo.InternalSetSource(*pattern);
    }

    if (!yAxis_.label.empty())
    {
        auto labelWidth = cairo.GetTextWidth(yAxis_.label);
        cairo.MoveTo(plotRect.x - cairo.GetLineWidth(), plotRect.YCenter() + labelWidth / 2);
        cairo.Save();
        cairo.Rotate(-M_1_PIf * 5);
        cairo.ShowText(yAxis_.label);
        cairo.Restore();
    }
    if (yAxis_.showLimits)
    {
        {
            std::string text = std::format("{}", limits.minY);
            cairo.MoveTo(plotRect.x - cairo.GetLineWidth(), plotRect.Bottom());
            cairo.Save();
            cairo.Rotate(-M_1_PIf * 5);
            cairo.ShowText(text);
            cairo.Restore();
        }
        {
            std::string text = std::format("{}", limits.maxY);
            auto limitWidth = cairo.GetTextWidth(text);
            cairo.MoveTo(plotRect.x - cairo.GetLineWidth(), plotRect.y + limitWidth);
            cairo.Save();
            cairo.Rotate(-M_1_PIf * 5);
            cairo.ShowText(text);
            cairo.Restore();
        }
    }

    cairo.MoveTo(plotRect.x + cairo.GetLineWidth() / 2, plotRect.y);
    cairo.LineTo(plotRect.x + cairo.GetLineWidth() / 2, plotRect.Bottom());
    cairo.Stroke();
    cairo.Restore();
}

void CairoPlot::DrawLegend(Cairo& cairo, const DataLimits&, const Rect& plotRect)
{
    float maxWidth = 0;
    float height = 0;
    float margin = cairo.GetLineWidth();
    float markerSize = cairo.GetTextHeight("^_") - margin * 2;
    bool haveLegend = false;
    for (const auto& graph : graphOptions_)
    {
        if (!graph.second.legend)
            continue;

        const auto& text = !graph.second.label.empty() ? graph.second.label : graph.first;
        if (text.empty())
            continue;

        maxWidth = std::max(maxWidth, cairo.GetTextWidth(text) + margin * 2.0f);
        height += cairo.GetTextHeight(text) + margin;
        haveLegend = true;
    }
    if (!haveLegend)
        return;

    cairo.Rectangle((plotRect.x + plotRect.width) - maxWidth - markerSize - margin * 2.0f, plotRect.y, maxWidth + markerSize + margin * 4.0f, height + margin * 4.0f);
    cairo.Stroke();
    float y = plotRect.y + margin * 2.0f;
    for (const auto& graph : graphOptions_)
    {
        if (!graph.second.legend)
            continue;

        const auto& text = !graph.second.label.empty() ? graph.second.label : graph.first;
        if (text.empty())
            continue;
        cairo.MoveTo((plotRect.x + plotRect.width) - maxWidth + margin, y + cairo.GetTextHeight(text));
        cairo.ShowText(text);

        cairo.Save();
        cairo.SetSourceColor(graph.second.color.r, graph.second.color.g, graph.second.color.b, graph.second.color.a);
        cairo.Rectangle((plotRect.x + plotRect.width) - maxWidth - markerSize,
            y + margin, markerSize, markerSize);
        cairo.Fill();
        cairo.Restore();

        y += cairo.GetTextHeight(text) + margin;
    }
}

sa::tl::TablePtr CairoPlot::GetXAxis() const
{
    auto result = sa::tl::MakeTable();
    result->Add("label", xAxis_.label);
    if (auto* p = xAxis_.pattern.GetPtr<CairoPattern, Object>())
    {
        const auto* meta = ctx_.GetMetatable(p->GetTypeName());
        if (meta)
            result->Add("pattern", sa::tl::MakeObject(*meta, p));
    }

    result->Add("draw", xAxis_.draw);
    result->Add("show_limits", xAxis_.showLimits);
    result->Add("min", xAxis_.min);
    result->Add("max", xAxis_.max);
    result->Add("key", xAxis_.key);
    return result;
}

void CairoPlot::SetXAxis(const sa::tl::Table& value)
{
    sa::tl::GetTableValue(ctx_, value, "label", xAxis_.label);
    sa::tl::GetTableOptionalValue(ctx_, value, "draw", xAxis_.draw);
    sa::tl::GetTableOptionalValue(ctx_, value, "show_limits", xAxis_.showLimits);
    sa::tl::GetTableOptionalValue(ctx_, value, "min", xAxis_.min);
    sa::tl::GetTableOptionalValue(ctx_, value, "max", xAxis_.max);
    if (value.contains("pattern"))
    {
        sa::tl::ObjectPtr pattern;
        sa::tl::GetTableValue(ctx_, value, "pattern", pattern);
        if (auto* p = sa::tl::MetaTable::GetObject<CairoPattern>(pattern))
        {
            xAxis_.pattern = ctx_.GetGC().GetStrong(p);
        }
    }
    sa::tl::ValuePtr key = value.GetValue("key");
    xAxis_.key = key->ToKey(ctx_);
}

sa::tl::TablePtr CairoPlot::GetYAxis() const
{
    auto result = sa::tl::MakeTable();
    result->Add("label", yAxis_.label);
    if (auto* p = yAxis_.pattern.GetPtr<CairoPattern, Object>())
    {
        const auto* meta = ctx_.GetMetatable(p->GetTypeName());
        if (meta)
            result->Add("pattern", sa::tl::MakeObject(*meta, p));
    }
    result->Add("draw", yAxis_.draw);
    result->Add("show_limits", yAxis_.showLimits);
    result->Add("min", yAxis_.min);
    result->Add("max", yAxis_.max);
    result->Add("key", yAxis_.key);
    return result;

}

void CairoPlot::SetYAxis(const sa::tl::Table& value)
{
    sa::tl::GetTableValue(ctx_, value, "label", yAxis_.label);
    sa::tl::GetTableOptionalValue(ctx_, value, "draw", yAxis_.draw);
    sa::tl::GetTableOptionalValue(ctx_, value, "show_limits", yAxis_.showLimits);
    sa::tl::GetTableOptionalValue(ctx_, value, "min", yAxis_.min);
    sa::tl::GetTableOptionalValue(ctx_, value, "max", yAxis_.max);
    if (value.contains("pattern"))
    {
        sa::tl::ObjectPtr pattern;
        sa::tl::GetTableValue(ctx_, value, "pattern", pattern);
        if (auto* p = sa::tl::MetaTable::GetObject<CairoPattern>(pattern))
        {
            yAxis_.pattern = ctx_.GetGC().GetStrong(p);
        }
    }
    sa::tl::ValuePtr key = value.GetValue("key");
    yAxis_.key = key->ToKey(ctx_);
}

void CairoPlot::SetGraphOptions(const std::string& graph, const sa::tl::Table& value)
{
    GraphOptions& options = graphOptions_[graph];
    sa::tl::GetTableOptionalValue(ctx_, value, "label", options.label);
    sa::tl::GetTableOptionalValue(ctx_, value, "legend", options.legend);
    if (value.contains("color"))
    {
        sa::tl::TablePtr color;
        sa::tl::GetTableValue(ctx_, value, "color", color);
        sa::tl::GetTableValue(ctx_, *color, "r", options.color.r);
        sa::tl::GetTableValue(ctx_, *color, "g", options.color.g);
        sa::tl::GetTableValue(ctx_, *color, "b", options.color.b);
        if (color->contains("a"))
            sa::tl::GetTableValue(ctx_, *color, "a", options.color.a);
        else
            options.color.a = 1.0;
    }
    if (value.contains("pattern"))
    {
        sa::tl::ObjectPtr pattern;
        sa::tl::GetTableValue(ctx_, value, "pattern", pattern);
        options.pattern = ctx_.GetGC().GetStrong(pattern->GetInstance());
    }
}

sa::tl::TablePtr CairoPlot::GetGraphOptions(const std::string& graph)
{
    auto it = graphOptions_.find(graph);
    if (it == graphOptions_.end())
        return sa::tl::MakeTable();
    auto result = sa::tl::MakeTable();
    result->Add("label", it->second.label);
    result->Add("legend", it->second.legend);
    auto color = sa::tl::MakeTable();
    color->Add<sa::tl::Float>("r", it->second.color.r);
    color->Add<sa::tl::Float>("g", it->second.color.g);
    color->Add<sa::tl::Float>("b", it->second.color.b);
    color->Add<sa::tl::Float>("a", it->second.color.a);
    result->Add("color", std::move(color));
    if (auto* pattern = it->second.pattern.GetPtr<CairoPattern, Object>())
    {
        auto* meta = ctx_.GetMetatable<ui::CairoPattern>();
        result->Add("pattern", sa::tl::MakeObject(*meta, pattern));
    }
    else
        result->Add("pattern", nullptr);
    return result;
}

void CairoPlot::SetBorderPattern(const sa::tl::Value& pattern)
{
    if (!pattern.Is<sa::tl::ObjectPtr>())
        return;

    if (auto* p = sa::tl::MetaTable::GetObject<CairoPattern>(pattern.As<sa::tl::ObjectPtr>()))
    {
        borderPattern = ctx_.GetGC().GetStrong(p);
    }
}

sa::tl::Value CairoPlot::GetBorderPattern() const
{
    if (auto* p = borderPattern.GetPtr<CairoPattern, Object>())
    {
        const auto* meta = ctx_.GetMetatable(p->GetTypeName());
        if (meta)
            return sa::tl::MakeObject(*meta, p);
    }
    return nullptr;
}

Rect CairoPlot::GetPlotRect(const Cairo& cairo) const
{
    auto xHeight = cairo.GetTextHeight(xAxis_.label);
    auto yWidth = cairo.GetTextHeight(yAxis_.label);
    Rect result;
    result.x = yWidth + cairo.GetLineWidth() * 4.0f;
    result.y = xHeight + cairo.GetLineWidth() * 4.0f;
    result.width = (width_ - result.x * 2.0f);
    result.height = (height_ - result.y * 2.0f);
    return result;
}

DataLimits CairoPlot::GetDataLimits() const
{
    DataLimits result;
    if (!std::isnan(xAxis_.min))
        result.minX = xAxis_.min;
    if (!std::isnan(xAxis_.max))
        result.maxX = xAxis_.max;
    if (!std::isnan(yAxis_.min))
        result.minY = yAxis_.min;
    if (!std::isnan(yAxis_.max))
        result.maxY = yAxis_.max;
    if (!std::isnan(xAxis_.min) && !std::isnan(xAxis_.max) &&
        !std::isnan(yAxis_.min) && !!std::isnan(yAxis_.max))
        return result;

    for (const auto& graph : data_)
    {
        for (const auto& d : graph.second)
        {
            auto xv = d->GetValue(xAxis_.key);
            if (xv)
            {
                if (std::isnan(xAxis_.min))
                    result.minX = std::min<double>(result.minX, xv->ToFloat());
                if (std::isnan(xAxis_.max))
                    result.maxX = std::max<double>(result.maxX, xv->ToFloat());
            }
            auto yv = d->GetValue(yAxis_.key);
            if (yv)
            {
                if (std::isnan(yAxis_.min))
                    result.minY = std::min<double>(result.minY, yv->ToFloat());
                if (std::isnan(yAxis_.max))
                    result.maxY = std::max<double>(result.maxY, yv->ToFloat());
            }
        }
    }
    return result;
}

CairoBarPlot::CairoBarPlot(sa::tl::Context& ctx) :
    CairoPlot(ctx)
{ }

void CairoBarPlot::Draw(sa::tl::ObjectPtr cairo)
{
    auto* c = sa::tl::MetaTable::GetObject<Cairo>(cairo);
    if (!c)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Cairo object expected");
        return;
    }
    auto plotRect = GetPlotRect(*c);
    auto limits = GetDataLimits();

    DrawBorder(*c);
    if (xAxis_.draw)
        DrawXAxix(*c, limits, plotRect);
    if (yAxis_.draw)
        DrawYAxix(*c, limits, plotRect);
    DrawData(*c, limits, plotRect);
    if (legend_)
        DrawLegend(*c, limits, plotRect);
}

void CairoBarPlot::DrawData(Cairo& cairo, const DataLimits& limits, const Rect& plotRect)
{
    cairo.Save();
    cairo.Translate(plotRect.x, plotRect.y);
    for (const auto& graph : data_)
    {
        DrawGraph(cairo, graph.first, limits, plotRect, graph.second);
    }
    cairo.Restore();
}

void CairoBarPlot::DrawGraph(Cairo& cairo, const std::string& name,
    const DataLimits& limits, const Rect& plotRect, const std::vector<sa::tl::TablePtr>& data)
{
    cairo.Save();
    const GraphOptions& options = graphOptions_[name];

    if (auto* pattern = options.pattern.GetPtr<CairoPattern, Object>())
    {
        cairo.InternalSetSource(*pattern);
    }

    size_t barCount = graphOptions_.size();
    double barWidth = (plotRect.width / (double)barCount) - cairo.GetLineWidth() * 2;

    for (const auto& d : data)
    {
        auto xv = d->GetValue(xAxis_.key);
        if (!xv)
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("Key {} does not exist in data", sa::tl::KeyToString(xAxis_.key)));
            return;
        }
        auto yv = d->GetValue(yAxis_.key);
        if (!yv)
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("Key {} does not exist in data", sa::tl::KeyToString(yAxis_.key)));
            return;
        }

        auto yval = plotRect.height - ((limits.maxY - yv->ToFloat()) / limits.YExcess()) * plotRect.height;

        cairo.Rectangle((xv->ToFloat() - limits.minX) * (barWidth + cairo.GetLineWidth()), plotRect.height - yval,
            barWidth, yval);
        cairo.StrokePreserve();
        cairo.Save();
        cairo.SetSourceColor(options.color.r, options.color.g, options.color.b, options.color.a);
        cairo.Fill();
        cairo.Restore();
    }
    cairo.Restore();
}

CairoScatterPlot::CairoScatterPlot(sa::tl::Context& ctx) :
    CairoPlot(ctx)
{ }

void CairoScatterPlot::Draw(sa::tl::ObjectPtr cairo)
{
    auto* c = sa::tl::MetaTable::GetObject<Cairo>(cairo);
    if (!c)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Cairo object expected");
        return;
    }
    auto plotRect = GetPlotRect(*c);
    auto limits = GetDataLimits();

    DrawBorder(*c);
    if (xAxis_.draw)
        DrawXAxix(*c, limits, plotRect);
    if (yAxis_.draw)
        DrawYAxix(*c, limits, plotRect);
    DrawData(*c, limits, plotRect);
    if (legend_)
        DrawLegend(*c, limits, plotRect);
}

void CairoScatterPlot::DrawGraph(Cairo& cairo, const std::string& name, const DataLimits& limits, const Rect& plotRect, const std::vector<sa::tl::TablePtr>& data)
{
    cairo.Save();
    const GraphOptions& options = graphOptions_[name];
    if (auto* pattern = options.pattern.GetPtr<CairoPattern, Object>())
    {
        cairo.InternalSetSource(*pattern);
    }
    for (const auto& d : data)
    {
        auto xv = d->GetValue(xAxis_.key);
        if (!xv)
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("Key {} does not exist in data", sa::tl::KeyToString(xAxis_.key)));
            return;
        }
        auto yv = d->GetValue(yAxis_.key);
        if (!yv)
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("Key {} does not exist in data", sa::tl::KeyToString(yAxis_.key)));
            return;
        }

        cairo.Arc(((xv->ToFloat() - limits.minX) / limits.XExcess()) * plotRect.width,
            plotRect.height - (((yv->ToFloat() - limits.minY) / limits.YExcess()) * plotRect.height),
            cairo.GetLineWidth(), 0, M_PI * 2);
        cairo.StrokePreserve();
        cairo.Save();
        cairo.SetSourceColor(options.color.r, options.color.g, options.color.b, options.color.a);
        cairo.Fill();
        cairo.Restore();
    }
    cairo.Restore();
}

void CairoScatterPlot::DrawData(Cairo& cairo, const DataLimits& limits, const Rect& plotRect)
{
    cairo.Save();
    cairo.Translate(plotRect.x, plotRect.y);
    for (const auto& graph : data_)
    {
        DrawGraph(cairo, graph.first, limits, plotRect, graph.second);
    }
    cairo.Restore();
}

CairoLinePlot::CairoLinePlot(sa::tl::Context& ctx) :
    CairoPlot(ctx)
{ }

void CairoLinePlot::Draw(sa::tl::ObjectPtr cairo)
{
    auto* c = sa::tl::MetaTable::GetObject<Cairo>(cairo);
    if (!c)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Cairo object expected");
        return;
    }
    auto plotRect = GetPlotRect(*c);
    auto limits = GetDataLimits();

    DrawBorder(*c);
    if (xAxis_.draw)
        DrawXAxix(*c, limits, plotRect);
    if (yAxis_.draw)
        DrawYAxix(*c, limits, plotRect);
    DrawData(*c, limits, plotRect);
    if (legend_)
        DrawLegend(*c, limits, plotRect);
}

void CairoLinePlot::DrawGraph(Cairo& cairo, const std::string& name, const DataLimits& limits, const Rect& plotRect, const std::vector<sa::tl::TablePtr>& data)
{
    cairo.Save();
    bool first = true;
    const GraphOptions& options = graphOptions_[name];
    cairo.SetSourceColor(options.color.r, options.color.g, options.color.b, options.color.a);
    if (auto* pattern = options.pattern.GetPtr<CairoPattern, Object>())
    {
        cairo.InternalSetSource(*pattern);
    }
    for (const auto& d : data)
    {
        auto xv = d->GetValue(xAxis_.key);
        if (!xv)
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("Key {} does not exist in data", sa::tl::KeyToString(xAxis_.key)));
            return;
        }
        auto yv = d->GetValue(yAxis_.key);
        if (!yv)
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("Key {} does not exist in data", sa::tl::KeyToString(yAxis_.key)));
            return;
        }
        if (first)
        {
            cairo.MoveTo(((xv->ToFloat() - limits.minX) / limits.XExcess()) * plotRect.width,
                plotRect.height - (((yv->ToFloat() - limits.minY) / limits.YExcess()) * plotRect.height));
            first = false;
        }
        else
        {
            cairo.LineTo(((xv->ToFloat() - limits.minX) / limits.XExcess()) * plotRect.width,
                plotRect.height - (((yv->ToFloat() - limits.minY) / limits.YExcess()) * plotRect.height));
        }

    }
    cairo.Stroke();
    cairo.Restore();
}

void CairoLinePlot::DrawData(Cairo& cairo, const DataLimits& limits, const Rect& plotRect)
{
    cairo.Save();
    cairo.Translate(plotRect.x, plotRect.y);
    for (const auto& graph : data_)
    {
        DrawGraph(cairo, graph.first, limits, plotRect, graph.second);
    }
    cairo.Restore();
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::CairoPlot>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::CairoPlot, Object>();
    result
        .AddFunction<ui::CairoPlot, void, const std::string&, sa::tl::TablePtr>("add_data", &ui::CairoPlot::AddData)
        .AddFunction<ui::CairoPlot, sa::tl::TablePtr, const std::string&, int>("get_data", &ui::CairoPlot::GetData)
        .AddFunction<ui::CairoPlot, int, const std::string&>("get_data_count", &ui::CairoPlot::GetDataCount)
        .AddFunction<ui::CairoPlot, void, const std::string&, int>("delete_data", &ui::CairoPlot::DeleteData)
        .AddFunction<ui::CairoPlot, void, const std::string&>("delete_graph", &ui::CairoPlot::DeleteGraph)
        .AddFunction<ui::CairoPlot, void, const std::string&>("clear_graph", &ui::CairoPlot::ClearGraph)
        .AddFunction<ui::CairoPlot, bool, const std::string&>("graph_exists", &ui::CairoPlot::GraphExists)
        .AddFunction<ui::CairoPlot, int>("get_graph_count", &ui::CairoPlot::GetGraphCount)
        .AddFunction<ui::CairoPlot, void>("clear_data", &ui::CairoPlot::ClearData)
        .AddFunction<ui::CairoPlot, void, sa::tl::ObjectPtr>("draw", &ui::CairoPlot::Draw)
        .AddFunction<ui::CairoPlot, void, const std::string&, const sa::tl::Table&>("set_graph_options", &ui::CairoPlot::SetGraphOptions)
        .AddFunction<ui::CairoPlot, sa::tl::TablePtr, const std::string&>("get_graph_options", &ui::CairoPlot::GetGraphOptions)
        .AddProperty("x_axis", &ui::CairoPlot::GetXAxis, &ui::CairoPlot::SetXAxis)
        .AddProperty("y_axis", &ui::CairoPlot::GetYAxis, &ui::CairoPlot::SetYAxis)
        .AddProperty("width", &ui::CairoPlot::GetWidth, &ui::CairoPlot::SetWidth)
        .AddProperty("height", &ui::CairoPlot::GetHeight, &ui::CairoPlot::SetHeight)
        .AddProperty("border_pattern", &ui::CairoPlot::GetBorderPattern, &ui::CairoPlot::SetBorderPattern)
        .AddProperty("legend", &ui::CairoPlot::GetLegend, &ui::CairoPlot::SetLegend);

    return result;
}

template<>
sa::tl::MetaTable& Register<ui::CairoBarPlot>(sa::tl::Context& ctx)
{
    return ctx.AddMetatable<ui::CairoBarPlot, ui::CairoPlot>();
}

template<>
sa::tl::MetaTable& Register<ui::CairoScatterPlot>(sa::tl::Context& ctx)
{
    return ctx.AddMetatable<ui::CairoScatterPlot, ui::CairoPlot>();
}

template<>
sa::tl::MetaTable& Register<ui::CairoLinePlot>(sa::tl::Context& ctx)
{
    return ctx.AddMetatable<ui::CairoLinePlot, ui::CairoPlot>();
}

}
