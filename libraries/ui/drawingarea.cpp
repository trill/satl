/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "drawingarea.h"

namespace ui {

DrawingArea::DrawingArea(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_drawing_area_new(), std::forward<std::string>(name))
{
    gtk_widget_show(GetHandle());
}

DrawingArea::~DrawingArea() = default;

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::DrawingArea>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::DrawingArea, ui::Widget>();
    return result;
}

}
