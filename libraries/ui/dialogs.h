/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "window.h"
#include <optional>

namespace ui {

std::optional<std::string> OpenFileDialog(Window* parent);
std::optional<std::string> SaveFileDialog(Window* parent);
int MessageDialog(Window* parent, int type, int buttons, const std::string& message);
void ErrorDialog(Window* parent, const std::string& message);

}
