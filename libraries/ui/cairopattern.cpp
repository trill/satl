/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "cairopattern.h"
#include "cairosurface.h"

namespace ui {

CairoPattern::CairoPattern(sa::tl::Context& ctx) :
    Object(ctx)
{
}

CairoPattern::CairoPattern(sa::tl::Context& ctx, cairo_pattern_t* pattern, bool own) :
    Object(ctx),
    own_(own),
    pattern_(pattern)
{ }

CairoPattern::~CairoPattern()
{
    if (own_ && pattern_)
    {
        cairo_pattern_destroy(pattern_);
        pattern_ = nullptr;
    }
}

bool CairoPattern::CreateColor(const sa::tl::Table& color)
{
    if (color.contains("a"))
    {
        sa::tl::Float r, g, b, a;
        sa::tl::GetTableValue(ctx_, color, "r", r);
        sa::tl::GetTableValue(ctx_, color, "g", g);
        sa::tl::GetTableValue(ctx_, color, "b", b);
        sa::tl::GetTableValue(ctx_, color, "a", a);
        pattern_ = cairo_pattern_create_rgba(r, g, b, a);
        own_ = true;
        return cairo_pattern_status(pattern_) == CAIRO_STATUS_SUCCESS;
    }
    sa::tl::Float r, g, b;
    sa::tl::GetTableValue(ctx_, color, "r", r);
    sa::tl::GetTableValue(ctx_, color, "g", g);
    sa::tl::GetTableValue(ctx_, color, "b", b);
    pattern_ = cairo_pattern_create_rgb(r, g, b);
    own_ = true;
    auto status = cairo_pattern_status(pattern_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoPattern::CreateSurface(const sa::tl::ObjectPtr& surface)
{
    if (const auto* surf = sa::tl::MetaTable::GetObject<CairoSurface>(surface))
    {
        pattern_ = cairo_pattern_create_for_surface(surf->GetHandle());
        own_ = true;
        auto status = cairo_pattern_status(pattern_);
        if (status != CAIRO_STATUS_SUCCESS)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
            return false;
        }
        return true;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "CairoSurface Object expected");
    return false;
}

bool CairoPattern::CreateLinear(sa::tl::Float x0, sa::tl::Float y0, sa::tl::Float x1, sa::tl::Float y1)
{
    pattern_ = cairo_pattern_create_linear(x0, y0, x1, y1);
    own_ = true;
    auto status = cairo_pattern_status(pattern_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoPattern::CreateRadial(sa::tl::Float cx0, sa::tl::Float cy0, sa::tl::Float r0,
    sa::tl::Float cx1, sa::tl::Float cy1, sa::tl::Float r1)
{
    pattern_ = cairo_pattern_create_radial(cx0, cy0, r0, cx1, cy1, r1);
    own_ = true;
    auto status = cairo_pattern_status(pattern_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

bool CairoPattern::CreateMesh()
{
    pattern_ = cairo_pattern_create_mesh();
    own_ = true;
    auto status = cairo_pattern_status(pattern_);
    if (status != CAIRO_STATUS_SUCCESS)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, cairo_status_to_string(status));
        return false;
    }
    return true;
}

sa::tl::TablePtr CairoPattern::GetRGBA() const
{
    auto result = sa::tl::MakeTable();
    if (!pattern_)
        return result;
    double r, g, b, a;
    auto status = cairo_pattern_get_rgba(pattern_, &r, &g, &b, &a);
    if (status != CAIRO_STATUS_SUCCESS)
        return result;
    result->Add<sa::tl::Float>("r", r);
    result->Add<sa::tl::Float>("g", g);
    result->Add<sa::tl::Float>("b", b);
    result->Add<sa::tl::Float>("a", a);
    return result;
}

sa::tl::TablePtr CairoPattern::GetMatrix() const
{
    if (!pattern_)
        return sa::tl::MakeTable();

    auto result = sa::tl::MakeTable();
    cairo_matrix_t m;
    cairo_pattern_get_matrix(pattern_, &m);
    result->Add<sa::tl::Float>("xx", m.xx);
    result->Add<sa::tl::Float>("yx", m.yx);
    result->Add<sa::tl::Float>("xy", m.xy);
    result->Add<sa::tl::Float>("yy", m.yy);
    result->Add<sa::tl::Float>("x0", m.x0);
    result->Add<sa::tl::Float>("y0", m.y0);

    return result;
}

void CairoPattern::SetMatrix(const sa::tl::Table& value)
{
    if (!pattern_)
        return;

    cairo_matrix_t m;
    sa::tl::GetTableValue(ctx_, value, "xx", m.xx);
    sa::tl::GetTableValue(ctx_, value, "yx", m.yx);
    sa::tl::GetTableValue(ctx_, value, "xy", m.xy);
    sa::tl::GetTableValue(ctx_, value, "yy", m.yy);
    sa::tl::GetTableValue(ctx_, value, "x0", m.x0);
    sa::tl::GetTableValue(ctx_, value, "y0", m.y0);
    cairo_pattern_set_matrix(pattern_, &m);
}

int CairoPattern::GetFilter() const
{
    if (!pattern_)
        return -1;
    return cairo_pattern_get_filter(pattern_);
}

void CairoPattern::SetFilter(int value)
{
    if (!pattern_)
        return;
    return cairo_pattern_set_filter(pattern_, (cairo_filter_t)value);
}

void CairoPattern::AddColorStopColor(sa::tl::Float offset, const sa::tl::Table& color)
{
    if (!pattern_)
        return;
    if (color.contains("a"))
    {
        sa::tl::Float r, g, b, a;
        sa::tl::GetTableValue(ctx_, color, "r", r);
        sa::tl::GetTableValue(ctx_, color, "g", g);
        sa::tl::GetTableValue(ctx_, color, "b", b);
        sa::tl::GetTableValue(ctx_, color, "a", a);
        cairo_pattern_add_color_stop_rgba(pattern_, offset, r, g, b, a);
    }
    else
    {
        sa::tl::Float r, g, b;
        sa::tl::GetTableValue(ctx_, color, "r", r);
        sa::tl::GetTableValue(ctx_, color, "g", g);
        sa::tl::GetTableValue(ctx_, color, "b", b);
        cairo_pattern_add_color_stop_rgb(pattern_, offset, r, g, b);
    }
}

int CairoPattern::GetColorStopCount()
{
    if (!pattern_)
        return 0;
    int result = 0;
    auto res = cairo_pattern_get_color_stop_count(pattern_, &result);
    if (res == CAIRO_STATUS_SUCCESS)
        return result;
    return 0;
}

sa::tl::TablePtr CairoPattern::GetColorStopColor(int index)
{
    if (!pattern_)
        return sa::tl::MakeTable();
    double offset, r, g, b, a;
    auto res = cairo_pattern_get_color_stop_rgba(pattern_, index, &offset, &r, &g, &b, &a);
    if (res == CAIRO_STATUS_SUCCESS)
    {
        auto result = sa::tl::MakeTable();
        result->Add<sa::tl::Float>("offset", offset);
        result->Add<sa::tl::Float>("r", r);
        result->Add<sa::tl::Float>("g", g);
        result->Add<sa::tl::Float>("b", b);
        result->Add<sa::tl::Float>("a", a);
        return result;
    }
    return sa::tl::MakeTable();
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::CairoPattern>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::CairoPattern, Object>();
    result
        .AddFunction<ui::CairoPattern, sa::tl::TablePtr>("get_rgba", &ui::CairoPattern::GetRGBA)
        .AddFunction<ui::CairoPattern, void, sa::tl::Float, const sa::tl::Table&>("add_color_stop_color", &ui::CairoPattern::AddColorStopColor)
        .AddFunction<ui::CairoPattern, int>("get_color_stop_count", &ui::CairoPattern::GetColorStopCount)
        .AddFunction<ui::CairoPattern, sa::tl::TablePtr, int>("get_color_stop_color", &ui::CairoPattern::GetColorStopColor)
        .AddProperty("matrix", &ui::CairoPattern::GetMatrix, &ui::CairoPattern::SetMatrix)
        .AddProperty("filter", &ui::CairoPattern::GetFilter, &ui::CairoPattern::SetFilter);

    return result;
}

}
