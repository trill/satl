project (satl_ui CXX)

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTK gtk+-3.0)
pkg_check_modules(WEBKIT webkit2gtk-4.0)

if (GTK_FOUND)
    file(GLOB SOURCES *.cpp *.h)
    add_library(satl_ui SHARED ${SOURCES})
    set_property(TARGET satl_ui PROPERTY POSITION_INDEPENDENT_CODE ON)
    target_link_libraries(satl_ui satl bind pthread)
    target_link_libraries(satl_ui ${GTK_LIBRARIES})
    target_include_directories(satl_ui PUBLIC ${GTK_INCLUDE_DIRS})
    target_compile_options(satl_ui PUBLIC ${GTK_CFLAGS_OTHER})

    if (WEBKIT_FOUND)
        target_compile_definitions(satl_ui PUBLIC HAVE_WEBKIT)
        target_link_libraries(satl_ui ${WEBKIT_LIBRARIES})
        target_include_directories(satl_ui PUBLIC ${WEBKIT_INCLUDE_DIRS})
        target_compile_options(satl_ui PUBLIC ${WEBKIT_CFLAGS_OTHER})
    endif()

    install(TARGETS satl_ui LIBRARY)
endif(GTK_FOUND)
