/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_WEBKIT

#include <object.h>
#include <webkit2/webkit2.h>

namespace ui {

class WebViewDataManager;

class WebViewContext : public sa::tl::bind::Object
{
public:
    explicit WebViewContext(sa::tl::Context& ctx);
    WebViewContext(sa::tl::Context& ctx, WebKitWebContext* handle);
    WebViewContext(sa::tl::Context& ctx, const WebViewDataManager& dataMngr);

    void ClearCache();

    bool IsAutomationAllowed() const;
    void SetAutomationAllowed(bool value);

    sa::tl::Value GetDataManager() const;
    int GetCacheModel() const;
    void SetCacheModel(int value);

    sa::tl::Value GetCookieManager() const;

    [[nodiscard]] sa::tl::CallablePtr GetOnDownload() const { return onDownload_; }
    void SetOnDownload(sa::tl::CallablePtr value) { onDownload_ = std::move(value); }

    WebKitWebContext* GetHandle() const { return handle_; }
private:
    static void OnDownloadStarted(WebKitWebContext* context, WebKitDownload* dl, WebViewContext* data);
    static void OnDownloadResponseReceived(WebKitDownload* dl, GParamSpec* ps, WebViewContext* data);
    bool DoOnDlownload(const std::string& uri);

    WebKitWebContext* handle_{ nullptr };
    sa::tl::CallablePtr onDownload_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::WebViewContext>(sa::tl::Context& ctx);
}

#endif
