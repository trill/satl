/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "glcontext.h"

namespace ui {

GLContext::GLContext(sa::tl::Context& ctx, GdkGLContext* glContext) :
    Object(ctx),
    glContext_(glContext)
{
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::GLContext>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::GLContext, Object>();
    return result;
}

}
