/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "combobox.h"
#include <ast.h>

namespace ui {

void Combobox::OnChanged(GtkComboBox*, Combobox* btn)
{
    if (btn->onChanged_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(btn->ctx_,
                (*btn->onChanged_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(btn->GetValue()));
                }))
            return;
        (*btn->onChanged_)(btn->ctx_, *btn->GetSelfPtr(), args);
    }
}

Combobox::Combobox(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_combo_box_text_new(), std::forward<std::string>(name))
{
    changedHandler_ = g_signal_connect(GetHandle(), "changed", G_CALLBACK(Combobox::OnChanged), this);
    gtk_widget_show(GetHandle());
}

Combobox::Combobox(sa::tl::Context& ctx, GtkWidget* handle, std::string name) :
    Widget(ctx, handle, std::forward<std::string>(name))
{
    changedHandler_ = g_signal_connect(GetHandle(), "changed", G_CALLBACK(Combobox::OnChanged), this);
    gtk_widget_show(GetHandle());
}

Combobox::~Combobox() = default;

void Combobox::AddItem(std::string value)
{
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(GetHandle()), nullptr, value.c_str());
    items_.push_back(std::move(value));
}

void Combobox::RemoveItem(const std::string& value)
{
    auto it = std::find_if(items_.begin(), items_.end(), [&](const auto& current) { return current == value; });
    if (it == items_.end())
        return;
    size_t index = std::distance(items_.begin(), it);
    gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(GetHandle()), index);
    items_.erase(it);
}

sa::tl::TablePtr Combobox::GetItems() const
{
    sa::tl::TablePtr result = sa::tl::MakeTable();
    for (const auto& item : items_)
    {
        result->Add(item);
    }
    return result;
}

void Combobox::SetItems(const sa::tl::TablePtr& value)
{
    items_.clear();
    gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(GetHandle()));
    for (const auto& item : *value)
        AddItem(item.second->ToString());
}

int Combobox::GetSelected() const
{
    return gtk_combo_box_get_active(GTK_COMBO_BOX(GetHandle()));
}

void Combobox::SetSelected(int value)
{
    g_signal_handler_block(GetHandle(), changedHandler_);
    gtk_combo_box_set_active(GTK_COMBO_BOX(GetHandle()), value);
    g_signal_handler_unblock(GetHandle(), changedHandler_);
}

sa::tl::Value Combobox::GetValue() const
{
    int sel = GetSelected();
    if (sel >= 0 && sel < (int)items_.size())
        return sa::tl::MakeString(items_[sel]);
    return nullptr;
}

void Combobox::SetValue(const sa::tl::Value& value)
{
    if (value.IsString())
    {
        const auto& v = *value.As<sa::tl::StringPtr>();
        for (size_t i = 0; i < items_.size(); ++i)
        {
            if (items_[i] == v)
            {
                SetSelected((int)i);
                break;
            }
        }
        return;
    }

    if (value.IsNull())
    {
        SetSelected(-1);
        return;
    }

    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String or null exprected");
}

ComboboxText::ComboboxText(sa::tl::Context& ctx, std::string name) :
    Combobox(ctx, gtk_combo_box_text_new_with_entry(), std::forward<std::string>(name))
{
}

ComboboxText::~ComboboxText() = default;

std::string ComboboxText::GetText() const
{
    return gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(GetHandle()));
}

void ComboboxText::SetText(const std::string& value)
{
    g_signal_handler_block(GetHandle(), changedHandler_);
    gtk_entry_set_text(GTK_ENTRY(GetHandle()), value.c_str());
    g_signal_handler_unblock(GetHandle(), changedHandler_);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Combobox>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Combobox, ui::Widget>();
    result
        .AddFunction<ui::Combobox, void, std::string>("add_item", &ui::Combobox::AddItem)
        .AddFunction<ui::Combobox, void, const std::string&>("remove_item", &ui::Combobox::RemoveItem)

        .AddProperty("selected", &ui::Combobox::GetSelected, &ui::Combobox::SetSelected)
        .AddProperty("value", &ui::Combobox::GetValue, &ui::Combobox::SetValue)
        .AddProperty("items", &ui::Combobox::GetItems, &ui::Combobox::SetItems)
        .AddProperty("on_changed", &ui::Combobox::GetOnChanged, &ui::Combobox::SetOnChanged);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::ComboboxText>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::ComboboxText, ui::Combobox>();
    result
        .AddProperty("text", &ui::ComboboxText::GetText, &ui::ComboboxText::SetText);
    return result;
}

}
