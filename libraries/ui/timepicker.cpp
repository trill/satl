/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "timepicker.h"

namespace ui {

void TimePicker::OnValueChanged(GtkSpinButton*, TimePicker* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), { });
}

static bool FormatValue(GtkSpinButton* spin)
{
    GtkAdjustment *adjustment;
    gchar *text;
    int value;

    adjustment = gtk_spin_button_get_adjustment(spin);
    value = (int)gtk_adjustment_get_value (adjustment);
    text = g_strdup_printf("%02d", value);
    gtk_entry_set_text(GTK_ENTRY(spin), text);
    g_free (text);
    return true;
}

bool TimePicker::OnHourOutput(GtkSpinButton* spin, TimePicker*)
{
    return FormatValue(spin);
}

bool TimePicker::OnMinuteOutput(GtkSpinButton* spin, TimePicker*)
{
    return FormatValue(spin);
}

TimePicker::TimePicker(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0), std::forward<std::string>(name))
{
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_widget_show(GetHandle());

    hours_ = gtk_spin_button_new_with_range(0, 23, 1);
    gtk_orientable_set_orientation(GTK_ORIENTABLE(hours_), GTK_ORIENTATION_VERTICAL);
    gtk_box_pack_start(GTK_BOX(GetHandle()), hours_, true, true, 0);
    changedHandlerHours_ = g_signal_connect(hours_, "value-changed", G_CALLBACK(TimePicker::OnValueChanged), this);
    g_signal_connect(hours_, "output", G_CALLBACK(TimePicker::OnHourOutput), this);

    auto* label = gtk_label_new(":");
    gtk_box_pack_start(GTK_BOX(GetHandle()), label, false, false, 10);

    minutes_ = gtk_spin_button_new_with_range(0, 59, 1);
    gtk_orientable_set_orientation(GTK_ORIENTABLE(minutes_), GTK_ORIENTATION_VERTICAL);
    gtk_box_pack_start(GTK_BOX(GetHandle()), minutes_, true, true, 0);
    changedHandlerMinutes_ = g_signal_connect(minutes_, "value-changed", G_CALLBACK(TimePicker::OnValueChanged), this);
    g_signal_connect(minutes_, "output", G_CALLBACK(TimePicker::OnMinuteOutput), this);

    gtk_widget_show(hours_);
    gtk_widget_set_hexpand(hours_, true);
    gtk_widget_set_vexpand(hours_, true);
    gtk_widget_show(label);
    gtk_widget_show(minutes_);
    gtk_widget_set_hexpand(minutes_, true);
    gtk_widget_set_vexpand(minutes_, true);
}

sa::tl::TablePtr TimePicker::GetValue() const
{

    int hour = (int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(hours_));
    int minute = (int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(minutes_));
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Integer>("hour", hour);
    result->Add<sa::tl::Integer>("min", minute);
    return result;
}

void TimePicker::SetValue(const sa::tl::Table& value)
{
    int hour, minute = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "hour", hour))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "min", minute))
        return;
    if (hour < 0 || hour > 23)
    {
        ctx_.RuntimeError(sa::tl::Error::InvalidArgument, "Hour value must be in trhe range of 0..23");
        return;
    }
    if (minute < 0 || minute > 59)
    {
        ctx_.RuntimeError(sa::tl::Error::InvalidArgument, "Minute value must be in trhe range of 0..23");
        return;
    }

    g_signal_handler_block(hours_, changedHandlerHours_);
    g_signal_handler_block(minutes_, changedHandlerMinutes_);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(hours_), hour);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(minutes_), minute);
    g_signal_handler_unblock(hours_, changedHandlerHours_);
    g_signal_handler_unblock(minutes_, changedHandlerMinutes_);
}

} // namespace ui

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::TimePicker>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::TimePicker, ui::Widget>();
    result
        .AddProperty("value", &ui::TimePicker::GetValue, &ui::TimePicker::SetValue)
        .AddProperty("on_changed", &ui::TimePicker::GetOnChanged, &ui::TimePicker::SetOnChanged);
    return result;
}

}
