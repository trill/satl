/**
 * Copyright 2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "calendar.h"

namespace ui {

void Calendar::OnDaySelected(GtkButton*, Calendar* sender)
{
    if (sender->onDaySelected_)
        (*sender->onDaySelected_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

void Calendar::OnDaySelectedDoubleClick(GtkButton*, Calendar* sender)
{
    if (sender->onDaySelectedDoubleClick_)
        (*sender->onDaySelectedDoubleClick_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

void Calendar::OnMonthChanged(GtkButton*, Calendar* sender)
{
    if (sender->onMonthChanged_)
        (*sender->onMonthChanged_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

void Calendar::OnNextMonth(GtkButton*, Calendar* sender)
{
    if (sender->onNextMonth_)
        (*sender->onNextMonth_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

void Calendar::OnNextYear(GtkButton*, Calendar* sender)
{
    if (sender->onNextYear_)
        (*sender->onNextYear_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

void Calendar::OnPrevMonth(GtkButton*, Calendar* sender)
{
    if (sender->onPrevMonth_)
        (*sender->onPrevMonth_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

void Calendar::OnPrevYear(GtkButton*, Calendar* sender)
{
    if (sender->onPrevYear_)
        (*sender->onPrevYear_)(sender->ctx_, *sender->GetSelfPtr(), {});
}

Calendar::Calendar(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_calendar_new(), std::forward<std::string>(name))
{
    g_signal_connect(GetHandle(), "day-selected", G_CALLBACK(Calendar::OnDaySelected), this);
    g_signal_connect(GetHandle(), "day-selected-double-click", G_CALLBACK(Calendar::OnDaySelectedDoubleClick), this);
    g_signal_connect(GetHandle(), "month-changed", G_CALLBACK(Calendar::OnMonthChanged), this);
    g_signal_connect(GetHandle(), "next-month", G_CALLBACK(Calendar::OnNextMonth), this);
    g_signal_connect(GetHandle(), "next-year", G_CALLBACK(Calendar::OnNextYear), this);
    g_signal_connect(GetHandle(), "prev-month", G_CALLBACK(Calendar::OnPrevMonth), this);
    g_signal_connect(GetHandle(), "prev-year", G_CALLBACK(Calendar::OnPrevYear), this);
    gtk_widget_show(GetHandle());
}

Calendar::~Calendar() = default;

void Calendar::ClearMarks()
{
    gtk_calendar_clear_marks(GTK_CALENDAR(GetHandle()));
}

void Calendar::MarkDay(unsigned day)
{
    gtk_calendar_mark_day(GTK_CALENDAR(GetHandle()), day);
}

void Calendar::UnmarkDay(unsigned day)
{
    gtk_calendar_unmark_day(GTK_CALENDAR(GetHandle()), day);
}

bool Calendar::DayIsMarked(unsigned day)
{
    return gtk_calendar_get_day_is_marked(GTK_CALENDAR(GetHandle()), day);
}

sa::tl::TablePtr Calendar::GetValue() const
{
    unsigned year = 0;
    unsigned month = 0;
    unsigned day = 0;
    gtk_calendar_get_date(GTK_CALENDAR(GetHandle()), &year, &month, &day);
    auto result = sa::tl::MakeTable();
    result->Add("year", year);
    result->Add("month", month);
    result->Add("day", day);

    return result;
}

void Calendar::SetValue(const sa::tl::Table& value)
{
    unsigned year = 0;
    unsigned month = 0;
    unsigned day = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "year", year))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "month", month))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "day", day))
        return;
    gtk_calendar_select_month(GTK_CALENDAR(GetHandle()), month, year);
    gtk_calendar_select_day(GTK_CALENDAR(GetHandle()), day);
}

int Calendar::GetDisplayOptions() const
{
    return static_cast<int>(gtk_calendar_get_display_options(GTK_CALENDAR(GetHandle())));
}

void Calendar::SetDisplayOptions(int value)
{
    gtk_calendar_set_display_options(GTK_CALENDAR(GetHandle()), static_cast<GtkCalendarDisplayOptions>(value));
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Calendar>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Calendar, ui::Widget>();
    result.AddFunction<ui::Calendar, void>("clear_marks", &ui::Calendar::ClearMarks)
        .AddFunction<ui::Calendar, void, unsigned>("mark_day", &ui::Calendar::MarkDay)
        .AddFunction<ui::Calendar, void, unsigned>("unmark_day", &ui::Calendar::UnmarkDay)
        .AddFunction<ui::Calendar, bool, unsigned>("day_is_marked", &ui::Calendar::DayIsMarked)
        .AddProperty("value", &ui::Calendar::GetValue, &ui::Calendar::SetValue)
        .AddProperty("display_options", &ui::Calendar::GetDisplayOptions, &ui::Calendar::SetDisplayOptions)
        .AddProperty("on_day_selected", &ui::Calendar::GetOnDaySelected, &ui::Calendar::SetOnDaySelected)
        .AddProperty("on_day_selected_double_click", &ui::Calendar::GetOnDaySelectedDoubleClick, &ui::Calendar::SetOnDaySelectedDoubleClick)
        .AddProperty("on_month_changed", &ui::Calendar::GetOnMonthChanged, &ui::Calendar::SetOnMonthChanged)
        .AddProperty("on_next_month", &ui::Calendar::GetOnNextMonth, &ui::Calendar::SetOnNextMonth)
        .AddProperty("on_next_year", &ui::Calendar::GetOnNextYear, &ui::Calendar::SetOnNextYear)
        .AddProperty("on_next_month", &ui::Calendar::GetOnPrevMonth, &ui::Calendar::SetOnPrevMonth)
        .AddProperty("on_prev_year", &ui::Calendar::GetOnPrevYear, &ui::Calendar::SetOnPrevYear);
    return result;
}

}
