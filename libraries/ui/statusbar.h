/**
 * Copyright (c) 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

class Statusbar final : public Widget
{
    SATL_NON_COPYABLE(Statusbar);
    SATL_NON_MOVEABLE(Statusbar);
    TYPED_OBJECT(Statusbar);
public:
    Statusbar(sa::tl::Context& ctx, std::string name);
    ~Statusbar() override;

    int GetContextId(const std::string& contextDesciption);
    void Pop(int contextId);
    int Push(int contextId, const std::string& text);
    void Remove(int contextId, int messageId);
    void RemoveAll(int contextId);

    sa::tl::CallablePtr GetOnTextPopped() const { return onTextPopped_; }
    void SetOnTextPopped(sa::tl::CallablePtr value) { onTextPopped_ = std::move(value); }
    sa::tl::CallablePtr GetOnTextPushed() const { return onTextPushed_; }
    void SetOnTextPushed(sa::tl::CallablePtr value) { onTextPushed_ = std::move(value); }
private:
    static void OnTextPopped(GtkStatusbar*, guint context_id, gchar* text, Statusbar* data);
    static void OnTextPushed(GtkStatusbar*, guint context_id, gchar* text, Statusbar* data);

    sa::tl::CallablePtr onTextPopped_;
    sa::tl::CallablePtr onTextPushed_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Statusbar>(sa::tl::Context& ctx);
}
