/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class MultilineEntry final : public Widget
{
    SATL_NON_COPYABLE(MultilineEntry);
    SATL_NON_MOVEABLE(MultilineEntry);
    TYPED_OBJECT(MultilineEntry);
public:
    MultilineEntry(sa::tl::Context& ctx, std::string name);
    ~MultilineEntry() override;

    [[nodiscard]] std::string GetText() const;
    void SetText(const std::string& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
    [[nodiscard]] bool GetReadonly() const;
    void SetReadonly(bool value);
    [[nodiscard]] bool GetMonospace() const;
    void SetMonospace(bool value);
    [[nodiscard]] bool GetAcceptTabs() const;
    void SetAcceptTabs(bool value);
    [[nodiscard]] bool GetOverwrite() const;
    void SetOverwrite(bool value);
    [[nodiscard]] int GetInputPurpose() const;
    void SetInputPurpose(int value);
    [[nodiscard]] int GetWrapMode() const;
    void SetWrapMode(int value);
    bool GetEditable() const;
    void SetEditable(bool value);
private:
    static void OnChanged(GtkEditable*, MultilineEntry* data);
    sa::tl::CallablePtr onChanged_;
    GtkWidget* textView_;
    gulong changedHandler_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::MultilineEntry>(sa::tl::Context& ctx);
}
