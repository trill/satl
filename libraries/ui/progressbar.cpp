/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "progressbar.h"
#include <ast.h>

namespace ui {

Progressbar::Progressbar(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_progress_bar_new(), std::forward<std::string>(name))
{
    gtk_widget_show(GetHandle());
}

Progressbar::~Progressbar() = default;

std::string Progressbar::GetText() const
{
    return gtk_progress_bar_get_text(GTK_PROGRESS_BAR(GetHandle()));
}

void Progressbar::SetText(const std::string& value)
{
    gtk_progress_bar_set_text(GTK_PROGRESS_BAR(GetHandle()), value.c_str());
}

int Progressbar::GetValue() const
{
    return (int)(gtk_progress_bar_get_fraction(GTK_PROGRESS_BAR(GetHandle())) * 100.0);
}

void Progressbar::SetValue(int value)
{
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(GetHandle()), ((gdouble)value) / 100);
}

bool Progressbar::GetShowText() const
{
    return gtk_progress_bar_get_show_text(GTK_PROGRESS_BAR(GetHandle()));
}

void Progressbar::SetShowText(bool value)
{
    gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(GetHandle()), value ? TRUE : FALSE);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Progressbar>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Progressbar, ui::Widget>();
    result
        .AddProperty("text", &ui::Progressbar::GetText, &ui::Progressbar::SetText)
        .AddProperty("value", &ui::Progressbar::GetValue, &ui::Progressbar::SetValue)
        .AddProperty("show_text", &ui::Progressbar::GetShowText, &ui::Progressbar::SetShowText);
    return result;
}

}
