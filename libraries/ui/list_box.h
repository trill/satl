/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class ListBox final : public Widget
{
    SATL_NON_COPYABLE(ListBox);
    SATL_NON_MOVEABLE(ListBox);
    TYPED_OBJECT(ListBox);
public:
    ListBox(sa::tl::Context& ctx, std::string name);
    ~ListBox() override;
    void Add(const sa::tl::Value&) override;

    bool IsSelected(int index);
    [[nodiscard]] int GetSelected() const;
    void SetSelected(int value);
    [[nodiscard]] bool GetMultiselect() const;
    void SetMultiselect(bool value);
    [[nodiscard]] sa::tl::CallablePtr GetOnSelected() const { return onSelected_; }
    void SetOnSelected(sa::tl::CallablePtr value) { onSelected_ = std::move(value); }
private:
    static void OnRowSelected(GtkListBox*, GtkListBoxRow* row, ListBox* data);
    guint selectedEvent_;
    sa::tl::CallablePtr onSelected_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::ListBox>(sa::tl::Context& ctx);
}
