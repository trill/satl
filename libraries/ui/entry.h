/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Entry : public Widget
{
    SATL_NON_COPYABLE(Entry);
    SATL_NON_MOVEABLE(Entry);
    TYPED_OBJECT(Entry);
public:
    Entry(sa::tl::Context& ctx, std::string name);
    ~Entry() override;

    [[nodiscard]] std::string GetText() const;
    void SetText(const std::string& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnActivate() const { return onActivate_; }
    void SetOnActivate(sa::tl::CallablePtr value) { onActivate_ = std::move(value); }
    [[nodiscard]] bool GetVisibility() const;
    void SetVisibility(bool value);
    [[nodiscard]] int GetInputPurpose() const;
    void SetInputPurpose(int value);
    [[nodiscard]] bool GetEditable() const;
    void SetEditable(bool value);
    void SelectRegion(int start, int end);
protected:
    Entry(sa::tl::Context& ctx, std::string name, bool search);
private:
    static void OnChanged(GtkEditable*, Entry* data);
    static void OnActivate(GtkEditable*, Entry* data);
    static void OnSearchChanged(GtkSearchEntry*, Entry* data);
    sa::tl::CallablePtr onChanged_;
    sa::tl::CallablePtr onActivate_;
    gulong changedHandler_;
};

class SearchEntry final : public Entry
{
    TYPED_OBJECT(SearchEntry)
public:
    SearchEntry(sa::tl::Context& ctx, std::string name);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Entry>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::SearchEntry>(sa::tl::Context& ctx);
}
