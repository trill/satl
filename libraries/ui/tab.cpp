/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "tab.h"
#include <ast.h>

namespace ui {

void Tab::OnChanged(GtkNotebook*, GtkWidget*, guint, Tab* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

Tab::Tab(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_notebook_new(), std::forward<std::string>(name))
{
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(GetHandle()), TRUE);
    g_signal_connect(GetHandle(), "switch-page", G_CALLBACK(Tab::OnChanged), this);
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_widget_show(GetHandle());
}

Tab::~Tab() = default;

int Tab::GetPageCount() const
{
    return (int)children_.size();
}

int Tab::GetCurrentPage() const
{
    return gtk_notebook_get_current_page(GTK_NOTEBOOK(GetHandle()));
}

void Tab::SetCurrentPage(int value)
{
    gtk_notebook_set_current_page(GTK_NOTEBOOK(GetHandle()), value);
}

void Tab::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    const auto& o = child.As<sa::tl::ObjectPtr>();
    if (Widget* w = o->GetMetatable().CastTo<Widget>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        gtk_container_add(GTK_CONTAINER(GetHandle()), w->GetHandle());
        if (!w->GetName().empty())
        {
            gtk_notebook_set_tab_label_text(GTK_NOTEBOOK(GetHandle()), w->GetHandle(), w->GetName().c_str());
        }
        InternalAdd(w);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not Widget instance");
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Tab>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Tab, ui::Widget>();
    result
        .AddProperty("page_count", &ui::Tab::GetPageCount)
        .AddProperty("current_page", &ui::Tab::GetCurrentPage, &ui::Tab::SetCurrentPage)
        .AddProperty("on_changed", &ui::Tab::GetOnChanged, &ui::Tab::SetOnChanged);
    return result;
}

}
