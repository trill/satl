/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Frame : public Widget
{
    SATL_NON_COPYABLE(Frame);
    SATL_NON_MOVEABLE(Frame);
    TYPED_OBJECT(Frame);
public:
    Frame(sa::tl::Context& ctx, std::string name);
    ~Frame() override;

    [[nodiscard]] std::string GetLabel() const;
    void SetLabel(const std::string& value);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Frame>(sa::tl::Context& ctx);
}
