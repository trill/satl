/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "datepicker.h"

namespace ui {

void DatePicker::OnSpinChanged(GtkSpinButton*, DatePicker* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), { });
}

void DatePicker::OnComboChanged(GtkComboBox*, DatePicker* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), { });
}

DatePicker::DatePicker(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_grid_new(), std::forward<std::string>(name))
{
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_grid_set_row_spacing(GTK_GRID(GetHandle()), DEF_MARGIN);
    gtk_grid_set_column_spacing(GTK_GRID(GetHandle()), DEF_MARGIN);
    gtk_widget_show(GetHandle());

    auto* yearLabel = gtk_label_new("Year");
    gtk_grid_attach(GTK_GRID(GetHandle()), yearLabel, 0, 0, 1, 1);
    gtk_widget_show(yearLabel);

    years_ = gtk_spin_button_new_with_range(0, 9000, 1);
    gtk_grid_attach(GTK_GRID(GetHandle()), years_, 1, 0, 1, 1);
    changedHandlerYears_ = g_signal_connect(years_, "value-changed", G_CALLBACK(DatePicker::OnSpinChanged), this);
    gtk_widget_show(years_);

    auto* monthLabel = gtk_label_new("Month");
    gtk_grid_attach(GTK_GRID(GetHandle()), monthLabel, 0, 1, 1, 1);
    gtk_widget_show(monthLabel);

    months_ = gtk_combo_box_text_new();
    gtk_grid_attach(GTK_GRID(GetHandle()), months_, 1, 1, 1, 1);
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "January");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "February");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "March");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "April");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "May");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "June");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "July");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "August");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "September");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "October");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "November");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(months_), nullptr, "December");
    changedHandlerMonths_ = g_signal_connect(months_, "changed", G_CALLBACK(DatePicker::OnComboChanged), this);
    gtk_widget_show(months_);

    auto* dayLabel = gtk_label_new("Day");
    gtk_grid_attach(GTK_GRID(GetHandle()), dayLabel, 0, 2, 1, 1);
    gtk_widget_show(dayLabel);

    days_ = gtk_spin_button_new_with_range(1, 31, 1);
    gtk_grid_attach(GTK_GRID(GetHandle()), days_, 1, 2, 1, 1);
    changedHandlerDays_ = g_signal_connect(days_, "value-changed", G_CALLBACK(DatePicker::OnSpinChanged), this);
    gtk_widget_show(days_);
}

sa::tl::TablePtr DatePicker::GetValue() const
{

    int year = (int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(years_));
    int month = (int)gtk_combo_box_get_active(GTK_COMBO_BOX(months_)) + 1;
    int day = (int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(days_));
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Integer>("year", year);
    result->Add<sa::tl::Integer>("month", month);
    result->Add<sa::tl::Integer>("day", day);
    return result;
}

void DatePicker::SetValue(const sa::tl::Table& value)
{
    int year = 0;
    int month = 0;
    int day = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "year", year))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "month", month))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "day", day))
        return;
    if (month < 1 || month > 12)
    {
        ctx_.RuntimeError(sa::tl::Error::InvalidArgument, "Month value must be in trhe range of 1..12");
        return;
    }
    if (day < 1 || day > 31)
    {
        ctx_.RuntimeError(sa::tl::Error::InvalidArgument, "Day value must be in trhe range of 1..31");
        return;
    }
    g_signal_handler_block(years_, changedHandlerYears_);
    g_signal_handler_block(months_, changedHandlerMonths_);
    g_signal_handler_block(days_, changedHandlerDays_);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(years_), (gdouble)year);
    gtk_combo_box_set_active(GTK_COMBO_BOX(months_), month - 1);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(days_), day);
    g_signal_handler_unblock(years_, changedHandlerYears_);
    g_signal_handler_unblock(months_, changedHandlerMonths_);
    g_signal_handler_unblock(days_, changedHandlerDays_);
}

} // namespace ui

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::DatePicker>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::DatePicker, ui::Widget>();
    result
        .AddProperty("value", &ui::DatePicker::GetValue, &ui::DatePicker::SetValue)
        .AddProperty("on_changed", &ui::DatePicker::GetOnChanged, &ui::DatePicker::SetOnChanged);
    return result;
}

}
