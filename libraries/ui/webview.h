/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_WEBKIT

#include  "widget.h"
#include <webkit2/webkit2.h>

namespace ui {

class WebViewContext;

// https://webkitgtk.org/reference/webkit2gtk/unstable/class.WebView.html
class WebView final : public Widget
{
    SATL_NON_COPYABLE(WebView);
    SATL_NON_MOVEABLE(WebView);
    TYPED_OBJECT(WebView);
public:
    WebView(sa::tl::Context& ctx, std::string name);
    WebView(sa::tl::Context& ctx, std::string name, WebView* rv);
    WebView(sa::tl::Context& ctx, std::string name, const WebViewContext& context);
    ~WebView() override;

    void LoadUri(const std::string& uri);
    void LoadHtml(const std::string& content, const std::string& baseUrl);
    void LoadPlainText(const std::string& content);
    void StopLoading();
    void Reload();
    void ReloadBypassCache();
    void GoBack();
    void GoForward();
    [[nodiscard]] bool GetInspectorIsAttached() const;
    void InspectorClose();
    void InspectorShow();
    sa::tl::Value GetContext() const;
    [[nodiscard]] bool GetLoading() const;
    [[nodiscard]] bool GetCanGoBack() const;
    [[nodiscard]] bool GetCanGoForward() const;
    [[nodiscard]] std::string GetTitle() const;
    [[nodiscard]] std::string GetUri() const;
    [[nodiscard]] float GetZoomLevel() const;
    void SetZoomLevel(float value);
    [[nodiscard]] bool GetEditable() const;
    void SetEditable(bool value);
    [[nodiscard]] bool GetMuted() const;
    void SetMuted(bool value);
    [[nodiscard]] sa::tl::TablePtr GetSettings() const;
    void SetSettings(const sa::tl::Table& value);
    sa::tl::Value GetFavicon() const;
    [[nodiscard]] bool GetHttps() const { return isHttps_; }
    [[nodiscard]] std::string GetCustomCharset() const;
    void SetCustomCharset(const std::string& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnTitleChanged() const { return onTitleChanged_; }
    void SetOnTitleChanged(sa::tl::CallablePtr value) { onTitleChanged_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnLoadFailed() const { return onLoadFailed_; }
    void SetOnLoadFailed(sa::tl::CallablePtr value) { onLoadFailed_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnLoadFailedTls() const { return onLoadFailedTls_; }
    void SetOnLoadFailedTls(sa::tl::CallablePtr value) { onLoadFailedTls_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnLoadChanged() const { return onLoadChanged_; }
    void SetOnLoadChanged(sa::tl::CallablePtr value) { onLoadChanged_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnMouseTargetChanged() const { return onMouseTargetChanged_; }
    void SetOnMouseTargetChanged(sa::tl::CallablePtr value) { onMouseTargetChanged_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnPermissionRequested() const { return onPermissionRequested_; }
    void SetOnPermissionRequested(sa::tl::CallablePtr value) { onPermissionRequested_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnResource() const { return onResource_; }
    void SetOnResource(sa::tl::CallablePtr value) { onResource_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnNavigate() const { return onNavigate_; }
    void SetOnNavigate(sa::tl::CallablePtr value) { onNavigate_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnNewWindow() const { return onNewWindow_; }
    void SetOnNewWindow(sa::tl::CallablePtr value) { onNewWindow_ = std::move(value); }
private:
    static void OnTitleChanged(WebKitWebView* webview, GParamSpec* ps, WebView* data);
    static gboolean OnDecidePolicy(WebKitWebView* webview,
        WebKitPolicyDecision* d,
        WebKitPolicyDecisionType dt,
        WebView* data);
    static bool OnLoadFailed(WebKitWebView* webview,
        WebKitLoadEvent load_event,
        gchar* failing_uri,
        gpointer error,
        WebView* data);
    static void OnLoadChanaged(WebKitWebView* webview, WebKitLoadEvent load_event, WebView* data);
    static void OnMouseTargetChanged(WebKitWebView* webview, WebKitHitTestResult* h, guint modifiers, WebView* data);
    static gboolean OnPermissionRequested(WebKitWebView*, WebKitPermissionRequest* request, WebView* data);
    static bool OnLoadFailedTls(WebKitWebView* v, gchar* uri, GTlsCertificate* cert, GTlsCertificateFlags err, WebView* data);

    WebView(sa::tl::Context& ctx, GtkWidget* handle, std::string name);
    void DecideNavigation(WebKitPolicyDecision* d);
    void DecideNewWindow(WebKitPolicyDecision* d);
    void DecideResource(WebKitPolicyDecision* d);

    GTlsCertificate* cert_{ nullptr };
    GTlsCertificate* failedCert_{ nullptr };
    bool isHttps_{ false };
    GTlsCertificateFlags tlsError_{ G_TLS_CERTIFICATE_NO_FLAGS };

    sa::tl::CallablePtr onTitleChanged_;
    sa::tl::CallablePtr onDecidePolicy_;
    sa::tl::CallablePtr onLoadFailed_;
    sa::tl::CallablePtr onLoadFailedTls_;
    sa::tl::CallablePtr onLoadChanged_;
    sa::tl::CallablePtr onMouseTargetChanged_;
    sa::tl::CallablePtr onPermissionRequested_;
    sa::tl::CallablePtr onResource_;
    sa::tl::CallablePtr onNavigate_;
    sa::tl::CallablePtr onNewWindow_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::WebView>(sa::tl::Context& ctx);
}

#endif
