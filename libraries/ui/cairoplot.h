/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <non_copyable.h>
#include <object.h>
#include <value.h>
#include <vector>
#include <limits>
#include <map>

namespace ui {

class Cairo;

struct Color
{
    double r = 0.0;
    double g = 0.0;
    double b = 0.0;
    double a = 1.0;
};

struct Rect
{
    double x = 0.0;
    double y = 0.0;
    double width = 0.0;
    double height = 0.0;
    [[nodiscard]] double XCenter() const
    {
        return (width / 2.0f) + x;
    }
    [[nodiscard]] double YCenter() const
    {
        return (height / 2.0f) + y;
    }
    [[nodiscard]] double Bottom() const
    {
        return height + y;
    }
    [[nodiscard]] double Right() const
    {
        return width + x;
    }
};

struct Axis
{
    std::string label;
    sa::tl::KeyType key;
    sa::tl::StrongObject pattern;
    bool draw = true;
    bool showLimits = false;
    sa::tl::Float min = std::numeric_limits<sa::tl::Float>::quiet_NaN();
    sa::tl::Float max = std::numeric_limits<sa::tl::Float>::quiet_NaN();
};

struct GraphOptions
{
    Color color;
    std::string label;
    bool legend = true;
    sa::tl::StrongObject pattern;
};

struct DataLimits
{
    double minX = std::numeric_limits<double>::max();
    double minY = std::numeric_limits<double>::max();
    double maxX = std::numeric_limits<double>::lowest();
    double maxY = std::numeric_limits<double>::lowest();
    [[nodiscard]] double XExcess() const
    {
        return maxX - minX;
    }
    [[nodiscard]] double YExcess() const
    {
        return maxY - minY;
    }
};

class CairoPlot : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(CairoPlot);
    SATL_NON_MOVEABLE(CairoPlot);
public:
    explicit CairoPlot(sa::tl::Context& ctx);
    ~CairoPlot() override;

    virtual void Draw(sa::tl::ObjectPtr cairo) = 0;
    void AddData(const std::string& graph, sa::tl::TablePtr data);
    int GetDataCount(const std::string& graph);
    sa::tl::TablePtr GetData(const std::string& graph, int index);
    void DeleteData(const std::string& graph, int index);
    void DeleteGraph(const std::string& graph);
    void ClearGraph(const std::string& graph);
    void ClearData();
    [[nodiscard]] int GetGraphCount() const;
    bool GraphExists(const std::string& graph);

    [[nodiscard]] sa::tl::TablePtr GetXAxis() const;
    void SetXAxis(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::TablePtr GetYAxis() const;
    void SetYAxis(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::Float GetWidth() const { return width_; }
    void SetWidth(sa::tl::Float value) { width_ = value; }
    [[nodiscard]] sa::tl::Float GetHeight() const { return height_; }
    void SetHeight(sa::tl::Float value) { height_ = value; }
    [[nodiscard]] bool GetLegend() const { return legend_; }
    void SetLegend(bool value) { legend_ = value; }
    void SetGraphOptions(const std::string& graph, const sa::tl::Table& value);
    sa::tl::TablePtr GetGraphOptions(const std::string& graph);
    void SetBorderPattern(const sa::tl::Value& pattern);
    [[nodiscard]] sa::tl::Value GetBorderPattern() const;

    [[nodiscard]] Rect GetPlotRect(const Cairo& cairo) const;
    [[nodiscard]] DataLimits GetDataLimits() const;
protected:
    void DrawBorder(Cairo& cairo);
    void DrawXAxix(Cairo& cairo, const DataLimits& limits, const Rect& plotRect);
    void DrawYAxix(Cairo& cairo, const DataLimits& limits, const Rect& plotRect);
    void DrawLegend(Cairo& cairo, const DataLimits& limits, const Rect& plotRect);
    Axis xAxis_;
    Axis yAxis_;
    sa::tl::Float width_{ 0.0f };
    sa::tl::Float height_{ 0.0f };
    bool legend_{ true };
    std::map<std::string, GraphOptions> graphOptions_;
    sa::tl::StrongObject borderPattern;
    std::map<std::string, std::vector<sa::tl::TablePtr>> data_;
};

class CairoBarPlot final : public CairoPlot
{
    TYPED_OBJECT(CairoBarPlot);
public:
    explicit CairoBarPlot(sa::tl::Context& ctx);
    void Draw(sa::tl::ObjectPtr cairo) override;
private:
    void DrawData(Cairo& cairo, const DataLimits& limits, const Rect& plotRect);
    void DrawGraph(Cairo& cairo, const std::string& name,
        const DataLimits& limits, const Rect& plotRect, const std::vector<sa::tl::TablePtr>& data);
};

class CairoScatterPlot final : public CairoPlot
{
    TYPED_OBJECT(CairoBarPlot);
public:
    explicit CairoScatterPlot(sa::tl::Context& ctx);
    void Draw(sa::tl::ObjectPtr cairo) override;
private:
    void DrawData(Cairo& cairo, const DataLimits& limits, const Rect& plotRect);
    void DrawGraph(Cairo& cairo, const std::string& name, const DataLimits& limits, const Rect& plotRect, const std::vector<sa::tl::TablePtr>& data);
};

class CairoLinePlot final : public CairoPlot
{
    TYPED_OBJECT(CairoBarPlot);
public:
    explicit CairoLinePlot(sa::tl::Context& ctx);
    void Draw(sa::tl::ObjectPtr cairo) override;
private:
    void DrawData(Cairo& cairo, const DataLimits& limits, const Rect& plotRect);
    void DrawGraph(Cairo& cairo, const std::string& name, const DataLimits& limits, const Rect& plotRect, const std::vector<sa::tl::TablePtr>& data);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::CairoPlot>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::CairoBarPlot>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::CairoScatterPlot>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::CairoLinePlot>(sa::tl::Context& ctx);
}
