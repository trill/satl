/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "separator.h"
#include <ast.h>

namespace ui {

VerticalSeparator::VerticalSeparator(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_separator_new(GTK_ORIENTATION_VERTICAL), std::forward<std::string>(name))
{
    gtk_widget_show(GetHandle());
}

VerticalSeparator::~VerticalSeparator() = default;

HorizontalSeparator::HorizontalSeparator(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_separator_new(GTK_ORIENTATION_HORIZONTAL), std::forward<std::string>(name))
{
    gtk_widget_show(GetHandle());
}

HorizontalSeparator::~HorizontalSeparator() = default;

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::VerticalSeparator>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::VerticalSeparator, ui::Widget>();
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::HorizontalSeparator>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::HorizontalSeparator, ui::Widget>();
    return result;
}

}
