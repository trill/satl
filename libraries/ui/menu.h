/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class MenuBar final : public Widget
{
    SATL_NON_COPYABLE(MenuBar);
    SATL_NON_MOVEABLE(MenuBar);
    TYPED_OBJECT(MenuBar);
public:
    MenuBar(sa::tl::Context& ctx, std::string name);
    ~MenuBar() override;
    void Add(const sa::tl::Value&) override;
};

class Menu final : public Widget
{
    SATL_NON_COPYABLE(Menu);
    SATL_NON_MOVEABLE(Menu);
    TYPED_OBJECT(Menu);
public:
    Menu(sa::tl::Context& ctx, std::string name);
    ~Menu() override;
    void Add(const sa::tl::Value&) override;
    void Popup();
};

class MenuItem : public Widget
{
    SATL_NON_COPYABLE(MenuItem);
    SATL_NON_MOVEABLE(MenuItem);
    TYPED_OBJECT(MenuItem);
public:
    MenuItem(sa::tl::Context& ctx, std::string name);
    ~MenuItem() override;
    [[nodiscard]] sa::tl::CallablePtr GetOnActivated() const { return onActivated_; }
    void SetOnActivated(sa::tl::CallablePtr value) { onActivated_ = std::move(value); }
protected:
    MenuItem(sa::tl::Context& ctx, GtkWidget* handle, std::string name);
private:
    static void OnActivated(GtkMenuItem*, MenuItem* data);
    sa::tl::CallablePtr onActivated_;
};

class MenuCheckItem final : public MenuItem
{
    SATL_NON_COPYABLE(MenuCheckItem);
    SATL_NON_MOVEABLE(MenuCheckItem);
    TYPED_OBJECT(MenuCheckItem);
public:
    MenuCheckItem(sa::tl::Context& ctx, std::string name);
    ~MenuCheckItem() override;
    [[nodiscard]] bool GetActive() const;
    void SetActive(bool value);
};

class MenuSeparatorItem final : public MenuItem
{
    SATL_NON_COPYABLE(MenuSeparatorItem);
    SATL_NON_MOVEABLE(MenuSeparatorItem);
    TYPED_OBJECT(MenuSeparatorItem);
public:
    MenuSeparatorItem(sa::tl::Context& ctx, std::string name);
    ~MenuSeparatorItem() override;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::MenuBar>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::Menu>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::MenuItem>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::MenuCheckItem>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::MenuSeparatorItem>(sa::tl::Context& ctx);
}
