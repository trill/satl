/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "cairo.h"
#include "cairosurface.h"
#include "cairopattern.h"
#include <factory.h>

namespace ui {

Cairo::Cairo(sa::tl::Context& ctx) :
    Object(ctx)
{ }

Cairo::Cairo(sa::tl::Context& ctx, cairo_t* cr) :
    Object(ctx),
    cr_(cr)
{ }

Cairo::Cairo(sa::tl::Context& ctx, const CairoSurface& surface) :
    Object(ctx),
    own_(true),
    cr_(cairo_create(surface.GetHandle()))
{ }

Cairo::~Cairo()
{
    if (own_ && cr_)
    {
        cairo_destroy(cr_);
        cr_ = nullptr;
    }
}

void Cairo::Save()
{
    if (!cr_)
        return;
    cairo_save(cr_);
}

void Cairo::Restore()
{
    if (!cr_)
        return;
    cairo_restore(cr_);
}

void Cairo::ShowPage()
{
    if (!cr_)
        return;
    cairo_show_page(cr_);
}

void Cairo::Translate(sa::tl::Float x, sa::tl::Float y)
{
    if (!cr_)
        return;
    cairo_translate(cr_, x, y);
}

void Cairo::Scale(sa::tl::Float x, sa::tl::Float y)
{
    if (!cr_)
        return;
    cairo_scale(cr_, x, y);
}

void Cairo::Rotate(sa::tl::Float a)
{
    if (!cr_)
        return;
    cairo_rotate(cr_, a);
}

void Cairo::Transform(const sa::tl::Table& matrix)
{
    if (!cr_)
        return;

    cairo_matrix_t m;
    sa::tl::GetTableValue(ctx_, matrix, "xx", m.xx);
    sa::tl::GetTableValue(ctx_, matrix, "yx", m.yx);
    sa::tl::GetTableValue(ctx_, matrix, "xy", m.xy);
    sa::tl::GetTableValue(ctx_, matrix, "yy", m.yy);
    sa::tl::GetTableValue(ctx_, matrix, "x0", m.x0);
    sa::tl::GetTableValue(ctx_, matrix, "y0", m.y0);
    cairo_transform(cr_, &m);
}

void Cairo::IdentityMatrix()
{
    if (!cr_)
        return;
    cairo_identity_matrix(cr_);
}

sa::tl::TablePtr Cairo::GetMatrix() const
{
    if (!cr_)
        return sa::tl::MakeTable();

    auto result = sa::tl::MakeTable();
    cairo_matrix_t m;
    cairo_get_matrix(cr_, &m);
    result->Add<sa::tl::Float>("xx", m.xx);
    result->Add<sa::tl::Float>("yx", m.yx);
    result->Add<sa::tl::Float>("xy", m.xy);
    result->Add<sa::tl::Float>("yy", m.yy);
    result->Add<sa::tl::Float>("x0", m.x0);
    result->Add<sa::tl::Float>("y0", m.y0);

    return result;
}

void Cairo::SetMatrix(const sa::tl::Table& value)
{
    if (!cr_)
        return;

    cairo_matrix_t m;
    sa::tl::GetTableValue(ctx_, value, "xx", m.xx);
    sa::tl::GetTableValue(ctx_, value, "yx", m.yx);
    sa::tl::GetTableValue(ctx_, value, "xy", m.xy);
    sa::tl::GetTableValue(ctx_, value, "yy", m.yy);
    sa::tl::GetTableValue(ctx_, value, "x0", m.x0);
    sa::tl::GetTableValue(ctx_, value, "y0", m.y0);
    cairo_set_matrix(cr_, &m);
}

void Cairo::TagBegin(const std::string& name, const std::string& attributes)
{
    if (!cr_)
        return;
    cairo_tag_begin(cr_, name.c_str(), attributes.c_str());
}

void Cairo::TagEnd(const std::string& name)
{
    if (!cr_)
        return;
    cairo_tag_end(cr_, name.c_str());
}

sa::tl::Value Cairo::PopGroup()
{
    if (!cr_)
        return {};

    auto* meta = ctx_.GetMetatable<CairoPattern>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<CairoPattern>(ctx_, *meta, ctx_, cairo_pop_group(cr_), true);
}

void Cairo::PushGroup()
{
    if (!cr_)
        return;
    cairo_push_group(cr_);
}

sa::tl::Value Cairo::GetSource() const
{
    if (!cr_)
        return {};

    auto* meta = ctx_.GetMetatable<CairoPattern>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<CairoPattern>(ctx_, *meta, ctx_, cairo_get_source(cr_), true);
}

void Cairo::SetSource(const sa::tl::Value& value)
{
    source_.Release();
    if (!cr_)
        return;
    if (auto* patt = sa::tl::MetaTable::GetObject<CairoPattern>(value))
    {
        cairo_set_source(cr_, patt->GetHandle());
        // Do we need to prevent this from being GC'd?
        source_ = ctx_.GetGC().GetStrong(patt);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "CairoPattern Object expected");
}

void Cairo::InternalSetSource(const CairoPattern& source)
{
    if (!cr_)
        return;
    cairo_set_source(cr_, source.GetHandle());
}

void Cairo::MoveTo(sa::tl::Float x, sa::tl::Float y)
{
    if (!cr_)
        return;
    cairo_move_to(cr_, x, y);
}

void Cairo::RelMoveTo(sa::tl::Float x, sa::tl::Float y)
{
    if (!cr_)
        return;
    cairo_move_to(cr_, x, y);
}

void Cairo::LineTo(sa::tl::Float x, sa::tl::Float y)
{
    if (!cr_)
        return;
    cairo_line_to(cr_, x, y);
}

void Cairo::RelLineTo(sa::tl::Float x, sa::tl::Float y)
{
    if (!cr_)
        return;
    cairo_line_to(cr_, x, y);
}

void Cairo::ShowText(const std::string& txt)
{
    if (!cr_)
        return;
    cairo_show_text(cr_, txt.c_str());
}

void Cairo::TextPath(const std::string& txt)
{
    if (!cr_)
        return;
    cairo_text_path(cr_, txt.c_str());
}

sa::tl::Float Cairo::GetTextWidth(const std::string& text) const
{
    if (!cr_)
        return 0.0;
    cairo_text_extents_t extends;
    cairo_text_extents(cr_, text.c_str(), &extends);
    return (sa::tl::Float)extends.width;
}

sa::tl::Float Cairo::GetTextHeight(const std::string& text) const
{
    if (!cr_)
        return 0.0;
    cairo_text_extents_t extends;
    cairo_text_extents(cr_, text.c_str(), &extends);
    return (sa::tl::Float)extends.height;
}

sa::tl::TablePtr Cairo::GetTextExtends(const std::string& text) const
{
    if (!cr_)
        return sa::tl::MakeTable();
    auto result = sa::tl::MakeTable();
    cairo_text_extents_t extends;
    cairo_text_extents(cr_, text.c_str(), &extends);
    result->Add<sa::tl::Float>("x_bearing", extends.x_bearing);
    result->Add<sa::tl::Float>("y_bearing", extends.y_bearing);
    result->Add<sa::tl::Float>("x_advance", extends.x_advance);
    result->Add<sa::tl::Float>("y_advance", extends.y_advance);
    result->Add<sa::tl::Float>("height", extends.height);
    result->Add<sa::tl::Float>("width", extends.width);
    return result;
}

void Cairo::SetFontSize(sa::tl::Float size)
{
    if (!cr_)
        return;
    cairo_set_font_size(cr_, size);
}

void Cairo::SelectFontFace(const std::string& name, int slant, int weight)
{
    if (!cr_)
        return;
    cairo_select_font_face(cr_, name.c_str(), (cairo_font_slant_t)slant, (cairo_font_weight_t)weight);
}

void Cairo::ClosePath()
{
    if (!cr_)
        return;
    cairo_close_path(cr_);
}

void Cairo::NewPath()
{
    if (!cr_)
        return;
    cairo_new_path(cr_);
}

void Cairo::NewSubPath()
{
    if (!cr_)
        return;
    cairo_new_sub_path(cr_);
}

void Cairo::Arc(sa::tl::Float xc, sa::tl::Float yc, sa::tl::Float r, sa::tl::Float a1, sa::tl::Float a2)
{
    if (!cr_)
        return;
    cairo_arc(cr_, xc, yc, r, a1, a2);
}

void Cairo::ArcNegative(sa::tl::Float xc, sa::tl::Float yc, sa::tl::Float r, sa::tl::Float a1, sa::tl::Float a2)
{
    if (!cr_)
        return;
    cairo_arc_negative(cr_, xc, yc, r, a1, a2);
}

void Cairo::CurveTo(sa::tl::Float x1, sa::tl::Float y1, sa::tl::Float x2, sa::tl::Float y2, sa::tl::Float x3, sa::tl::Float y3)
{
    if (!cr_)
        return;
    cairo_curve_to(cr_, x1, y1, x2, y2, x3, y3);
}

void Cairo::RelCurveTo(sa::tl::Float x1, sa::tl::Float y1, sa::tl::Float x2, sa::tl::Float y2, sa::tl::Float x3, sa::tl::Float y3)
{
    if (!cr_)
        return;
    cairo_rel_curve_to(cr_, x1, y1, x2, y2, x3, y3);
}

void Cairo::Rectangle(sa::tl::Float x, sa::tl::Float y, sa::tl::Float width, sa::tl::Float height)
{
    if (!cr_)
        return;
    cairo_rectangle(cr_, x, y, width, height);
}

void Cairo::Stroke()
{
    if (!cr_)
        return;
    cairo_stroke(cr_);
}

void Cairo::StrokePreserve()
{
    if (!cr_)
        return;
    cairo_stroke_preserve(cr_);
}

void Cairo::Fill()
{
    if (!cr_)
        return;
    cairo_fill(cr_);
}

void Cairo::SetSourceColor(const sa::tl::Table& color)
{
    if (!cr_)
        return;
    if (color.contains("a"))
    {
        sa::tl::Float r, g, b, a;
        sa::tl::GetTableValue(ctx_, color, "r", r);
        sa::tl::GetTableValue(ctx_, color, "g", g);
        sa::tl::GetTableValue(ctx_, color, "b", b);
        sa::tl::GetTableValue(ctx_, color, "a", a);
        cairo_set_source_rgba(cr_, r, g, b, a);
    }
    else
    {
        sa::tl::Float r, g, b;
        sa::tl::GetTableValue(ctx_, color, "r", r);
        sa::tl::GetTableValue(ctx_, color, "g", g);
        sa::tl::GetTableValue(ctx_, color, "b", b);
        cairo_set_source_rgb(cr_, r, g, b);
    }
}

void Cairo::SetSourceColor(double r, double g, double b, double a)
{
    if (!cr_)
        return;
    cairo_set_source_rgba(cr_, r, g, b, a);
}

sa::tl::Float Cairo::GetLineWidth() const
{
    if (!cr_)
        return 0.0;
    return (sa::tl::Float)cairo_get_line_width(cr_);
}

void Cairo::SetLineWidth(sa::tl::Float value)
{
    if (!cr_)
        return;
    cairo_set_line_width(cr_, (double)value);
}

sa::tl::TablePtr Cairo::GetCurrentPoint() const
{
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("x", -1.0);
    result->Add<sa::tl::Float>("y", -1.0);
    if (!cr_)
        return result;
    if (!cairo_has_current_point(cr_))
        return result;
    double x = 0.0;
    double y = 0.0;
    cairo_get_current_point(cr_, &x, &y);
    result->Add<sa::tl::Float>("x", x);
    result->Add<sa::tl::Float>("y", y);
    return result;
}

sa::tl::TablePtr Cairo::PathExtends() const
{
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("x1", 0.0);
    result->Add<sa::tl::Float>("y1", 0.0);
    result->Add<sa::tl::Float>("x2", 0.0);
    result->Add<sa::tl::Float>("y2", 0.0);
    if (!cr_)
        return result;
    double x1 = 0.0;
    double y1 = 0.0;
    double x2 = 0.0;
    double y2 = 0.0;
    cairo_path_extents(cr_, &x1, &y1, &x2, &y2);
    result->Add<sa::tl::Float>("x1", x1);
    result->Add<sa::tl::Float>("y1", y1);
    result->Add<sa::tl::Float>("x2", x2);
    result->Add<sa::tl::Float>("y2", y2);
    return result;
}

int Cairo::GetAntialias() const
{
    if (!cr_)
        return CAIRO_ANTIALIAS_NONE;
    return cairo_get_antialias(cr_);
}

void Cairo::SetAnialias(int value)
{
    if (!cr_)
        return;
    return cairo_set_antialias(cr_, (cairo_antialias_t)value);
}

int Cairo::GetOperator() const
{
    if (!cr_)
        return (int)CAIRO_OPERATOR_OVER;
    return (int)cairo_get_operator(cr_);
}

void Cairo::SetOperator(int value)
{
    if (!cr_)
        return;
    cairo_set_operator(cr_, (cairo_operator_t)value);
}

sa::tl::Float Cairo::GetTolerance() const
{
    if (!cr_)
        return 0.0;
    return cairo_get_tolerance(cr_);
}

void Cairo::SetTolerance(sa::tl::Float value)
{
    if (!cr_)
        return;
    cairo_set_tolerance(cr_, value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Cairo>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Cairo, Object>();
    result
        .AddFunction<ui::Cairo, void>("save", &ui::Cairo::Save)
        .AddFunction<ui::Cairo, void>("restore", &ui::Cairo::Restore)
        .AddFunction<ui::Cairo, void>("show_page", &ui::Cairo::ShowPage)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float>("translate", &ui::Cairo::Translate)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float>("scale", &ui::Cairo::Scale)
        .AddFunction<ui::Cairo, void, sa::tl::Float>("rotate", &ui::Cairo::Rotate)
        .AddFunction<ui::Cairo, void, const sa::tl::Table&>("tranform", &ui::Cairo::Transform)
        .AddFunction<ui::Cairo, void>("identity_matrix", &ui::Cairo::IdentityMatrix)
        .AddFunction<ui::Cairo, void, const std::string&, const std::string&>("tab_begin", &ui::Cairo::TagBegin)
        .AddFunction<ui::Cairo, void, const std::string&>("tab_end", &ui::Cairo::TagEnd)
        .AddFunction<ui::Cairo, sa::tl::Value>("pop_group", &ui::Cairo::PopGroup)
        .AddFunction<ui::Cairo, void>("push_group", &ui::Cairo::PushGroup)
        .AddFunction<ui::Cairo, void>("stroke", &ui::Cairo::Stroke)
        .AddFunction<ui::Cairo, void>("stroke_preserve", &ui::Cairo::StrokePreserve)
        .AddFunction<ui::Cairo, void>("fill", &ui::Cairo::Fill)
        .AddFunction<ui::Cairo, void>("close_path", &ui::Cairo::ClosePath)
        .AddFunction<ui::Cairo, void>("new_path", &ui::Cairo::NewPath)
        .AddFunction<ui::Cairo, void>("new_sub_path", &ui::Cairo::NewSubPath)
        .AddFunction<ui::Cairo, void, const std::string&>("show_text", &ui::Cairo::ShowText)
        .AddFunction<ui::Cairo, void, const std::string&>("text_path", &ui::Cairo::TextPath)
        .AddFunction<ui::Cairo, sa::tl::Float, const std::string&>("get_text_width", &ui::Cairo::GetTextWidth)
        .AddFunction<ui::Cairo, sa::tl::Float, const std::string&>("get_text_height", &ui::Cairo::GetTextHeight)
        .AddFunction<ui::Cairo, sa::tl::TablePtr, const std::string&>("get_text_extends", &ui::Cairo::GetTextExtends)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("rectangle", &ui::Cairo::Rectangle)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("arc", &ui::Cairo::Arc)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("arc_negative", &ui::Cairo::ArcNegative)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("curve_to", &ui::Cairo::CurveTo)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("rel_curve_to", &ui::Cairo::RelCurveTo)
        .AddFunction<ui::Cairo, void, sa::tl::Float>("set_font_size", &ui::Cairo::SetFontSize)
        .AddFunction<ui::Cairo, void, const std::string&, int, int>("select_font_face", &ui::Cairo::SelectFontFace)
        .AddFunction<ui::Cairo, void, const sa::tl::Table&>("set_source_color", &ui::Cairo::SetSourceColor)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float>("move_to", &ui::Cairo::MoveTo)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float>("rel_move_to", &ui::Cairo::RelMoveTo)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float>("line_to", &ui::Cairo::LineTo)
        .AddFunction<ui::Cairo, void, sa::tl::Float, sa::tl::Float>("rel_line_to", &ui::Cairo::RelLineTo)
        .AddFunction<ui::Cairo, sa::tl::TablePtr>("path_extends", &ui::Cairo::PathExtends)
        .AddProperty("source", &ui::Cairo::GetSource, &ui::Cairo::SetSource)
        .AddProperty("antialias", &ui::Cairo::GetAntialias, &ui::Cairo::SetAnialias)
        .AddProperty("current_point", &ui::Cairo::GetCurrentPoint)
        .AddProperty("matrix", &ui::Cairo::GetMatrix, &ui::Cairo::SetMatrix)
        .AddProperty("line_width", &ui::Cairo::GetLineWidth, &ui::Cairo::SetLineWidth)
        .AddProperty("tolerance", &ui::Cairo::GetTolerance, &ui::Cairo::SetTolerance)
        .AddProperty("operator", &ui::Cairo::GetOperator, &ui::Cairo::SetOperator);

    return result;
}

}
