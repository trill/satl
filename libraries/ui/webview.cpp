/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifdef HAVE_WEBKIT

#include "webview.h"
#include "cairosurface.h"
#include <factory.h>
#include "webviewcontext.h"

namespace ui {

static std::string GetHost(const std::string& url)
{
    static constexpr std::string_view delim("://");
    auto pos = url.find(delim);
    if (pos == std::string::npos)
        return "";
    auto pos2 = url.find('/', pos + 1 + delim.length());
    if (pos2 == std::string::npos)
        return url.substr(pos + delim.length());
    return url.substr(pos + delim.length(), pos2 - pos - delim.length());
}

void WebView::OnTitleChanged(WebKitWebView*, GParamSpec*, WebView* data)
{
    if (data->onTitleChanged_)
        (*data->onTitleChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

gboolean WebView::OnDecidePolicy(WebKitWebView*,
    WebKitPolicyDecision* d,
    WebKitPolicyDecisionType dt,
    WebView* data)
{
    switch (dt)
    {
    case WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION:
        data->DecideNavigation(d);
        break;
    case WEBKIT_POLICY_DECISION_TYPE_NEW_WINDOW_ACTION:
        data->DecideNewWindow(d);
        break;
    case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
        data->DecideResource(d);
        break;
    default:
        webkit_policy_decision_ignore(d);
        break;
    }
    return true;
}

bool WebView::OnLoadFailed(WebKitWebView*,
    WebKitLoadEvent load_event,
    gchar* failing_uri,
    gpointer,
    WebView* data)
{
    if (data->onLoadFailed_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onLoadFailed_),
                0,
                2,
                [&](int index)
                {
                    switch (index)
                    {
                        case 0:
                            args.push_back(sa::tl::MakeValue(sa::tl::MakeString(failing_uri)));
                            break;
                        case 1:
                            args.push_back(sa::tl::MakeValue(load_event));
                            break;
                    }
                }))
            return false;
        return (*data->onLoadFailed_)(data->ctx_, *data->GetSelfPtr(), args)->ToBool();
    }
    return false;
}

void WebView::OnLoadChanaged(WebKitWebView*, WebKitLoadEvent load_event, WebView* data)
{
    if (load_event == WEBKIT_LOAD_COMMITTED)
    {
        data->isHttps_ = webkit_web_view_get_tls_info(WEBKIT_WEB_VIEW(data->GetHandle()), &data->cert_, &data->tlsError_);
    }

    if (data->onLoadChanged_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onLoadChanged_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(load_event));
                }))
            return;
        (*data->onLoadChanged_)(data->ctx_, *data->GetSelfPtr(), args);
    }
}

void WebView::OnMouseTargetChanged(WebKitWebView*, WebKitHitTestResult* h, guint, WebView* data)
{
    auto hc = (WebKitHitTestResultContext)webkit_hit_test_result_get_context(h);
    std::string target;
    if (hc & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK)
        target = webkit_hit_test_result_get_link_uri(h);
    else if (hc & WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE)
        target = webkit_hit_test_result_get_image_uri(h);
    else if (hc & WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA)
        target = webkit_hit_test_result_get_media_uri(h);
    if (data->onMouseTargetChanged_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onMouseTargetChanged_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("context", hc);
                    data->Add("target", target);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return;
        (*data->onMouseTargetChanged_)(data->ctx_, *data->GetSelfPtr(), args);
    }
}

gboolean WebView::OnPermissionRequested(WebKitWebView*, WebKitPermissionRequest* request, WebView* data)
{
    if (data->onPermissionRequested_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onPermissionRequested_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    bool geoloc = WEBKIT_IS_GEOLOCATION_PERMISSION_REQUEST(request);
                    bool media = WEBKIT_IS_USER_MEDIA_PERMISSION_REQUEST(request);
                    bool audio = false;
                    bool video = false;
                    if (media)
                    {
                        if (webkit_user_media_permission_is_for_audio_device(WEBKIT_USER_MEDIA_PERMISSION_REQUEST(request)))
                            audio = true;
                        if (webkit_user_media_permission_is_for_video_device(WEBKIT_USER_MEDIA_PERMISSION_REQUEST(request)))
                            video = true;
                    }

                    data->Add("geolocation", geoloc);
                    data->Add("media", media);
                    data->Add("audio", audio);
                    data->Add("video", video);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;
        return (*data->onPermissionRequested_)(data->ctx_, *data->GetSelfPtr(), args)->ToBool();
    }
    return false;
}

bool WebView::OnLoadFailedTls(WebKitWebView*, gchar* uri, GTlsCertificate* cert, GTlsCertificateFlags err, WebView* data)
{
    data->tlsError_ = err;
    data->cert_ = (GTlsCertificate*)g_object_ref(cert);

    if (data->onLoadFailedTls_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(data->ctx_,
                (*data->onLoadFailedTls_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("uri", sa::tl::MakeString(uri));
                    data->Add<int>("err", err);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;
        if ((*data->onLoadFailedTls_)(data->ctx_, *data->GetSelfPtr(), args)->ToBool())
        {
            std::string host = GetHost(uri);
            if (host.empty())
                return false;
            // When the handler returns true we accept the certificate
            webkit_web_context_allow_tls_certificate_for_host(webkit_web_view_get_context(WEBKIT_WEB_VIEW(data->GetHandle())), cert, host.c_str());
            return true;
        }
    }
    return false;
}

WebView::WebView(sa::tl::Context& ctx, std::string name) :
    WebView(ctx, webkit_web_view_new(), std::forward<std::string>(name))
{ }

WebView::WebView(sa::tl::Context& ctx, std::string name, WebView* rv) :
    WebView(ctx, webkit_web_view_new_with_related_view(WEBKIT_WEB_VIEW(rv->GetHandle())), std::forward<std::string>(name))
{ }

WebView::WebView(sa::tl::Context& ctx, std::string name, const WebViewContext& context) :
    WebView(ctx, webkit_web_view_new_with_context(context.GetHandle()), std::forward<std::string>(name))
{ }

WebView::WebView(sa::tl::Context& ctx, GtkWidget* handle, std::string name) :
    Widget(ctx, handle, std::forward<std::string>(name))
{
    g_signal_connect(GetHandle(), "notify::title", G_CALLBACK(WebView::OnTitleChanged), this);
    g_signal_connect(GetHandle(), "decide-policy", G_CALLBACK(WebView::OnDecidePolicy), this);
    g_signal_connect(GetHandle(), "load-changed", G_CALLBACK(WebView::OnLoadChanaged), this);
    g_signal_connect(GetHandle(), "load-failed", G_CALLBACK(WebView::OnLoadFailed), this);
    g_signal_connect(GetHandle(), "load-failed-with-tls-errors", G_CALLBACK(WebView::OnLoadFailedTls), this);
    g_signal_connect(GetHandle(), "mouse-target-changed", G_CALLBACK(WebView::OnMouseTargetChanged), this);
    g_signal_connect(GetHandle(), "permission-request", G_CALLBACK(WebView::OnPermissionRequested), this);
    gtk_widget_show(GetHandle());
}

WebView::~WebView() = default;

sa::tl::Value WebView::GetContext() const
{
    auto* meta = ctx_.GetMetatable<WebViewContext>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<WebViewContext>(ctx_, *meta, ctx_, webkit_web_view_get_context(WEBKIT_WEB_VIEW(GetHandle())));
}

void WebView::DecideNavigation(WebKitPolicyDecision* d)
{
    WebKitNavigationAction* a = webkit_navigation_policy_decision_get_navigation_action(WEBKIT_NAVIGATION_POLICY_DECISION(d));
    auto type = webkit_navigation_action_get_navigation_type(a);
    const char* uri = webkit_uri_request_get_uri(webkit_navigation_action_get_request(a));

    bool ignore = false;
    if (onNavigate_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onNavigate_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add<int>("type", type);
                    data->Add("uri", sa::tl::MakeString(uri));
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
        {
            webkit_policy_decision_ignore(d);
            return;
        }
        ignore = (*onNavigate_)(ctx_, *GetSelfPtr(), args)->ToBool();
    }
    // returns whether this is handled by the script and should be ignored by us
    if (ignore)
        webkit_policy_decision_ignore(d);
    else
        webkit_policy_decision_use(d);
}

void WebView::DecideNewWindow(WebKitPolicyDecision* d)
{
    WebKitNavigationAction* a = webkit_navigation_policy_decision_get_navigation_action(WEBKIT_NAVIGATION_POLICY_DECISION(d));
    auto type = webkit_navigation_action_get_navigation_type(a);
    const char* uri = webkit_uri_request_get_uri(webkit_navigation_action_get_request(a));

    bool ignore = false;
    if (onNewWindow_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onNewWindow_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add<int>("type", type);
                    data->Add("uri", sa::tl::MakeString(uri));
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
        {
            webkit_policy_decision_ignore(d);
            return;
        }
        ignore = (*onNewWindow_)(ctx_, *GetSelfPtr(), args)->ToBool();
    }
    // returns whether this is handled by the script and should be ignored by us
    if (ignore)
        webkit_policy_decision_ignore(d);
    else
        webkit_policy_decision_use(d);
}

void WebView::DecideResource(WebKitPolicyDecision* d)
{
    WebKitResponsePolicyDecision* r = WEBKIT_RESPONSE_POLICY_DECISION(d);
    WebKitURIResponse* res = webkit_response_policy_decision_get_response(r);
    const gchar* uri = webkit_uri_response_get_uri(res);

    bool ignore = false;
    if (onResource_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onResource_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(sa::tl::MakeString(uri)));
                }))
        {
            webkit_policy_decision_ignore(d);
            return;
        }
        ignore = (*onResource_)(ctx_, *GetSelfPtr(), args)->ToBool();
    }
    // returns whether this is handled by the script and should be ignored by us
    if (ignore)
        webkit_policy_decision_ignore(d);
    else
        webkit_policy_decision_use(d);
}

void WebView::LoadUri(const std::string& uri)
{
    webkit_web_view_load_uri(WEBKIT_WEB_VIEW(GetHandle()), uri.c_str());
}

void WebView::LoadHtml(const std::string& content, const std::string& baseUrl)
{
    webkit_web_view_load_html(WEBKIT_WEB_VIEW(GetHandle()), content.c_str(), baseUrl.c_str());
}

void WebView::LoadPlainText(const std::string& content)
{
    webkit_web_view_load_plain_text(WEBKIT_WEB_VIEW(GetHandle()), content.c_str());
}

void WebView::StopLoading()
{
    webkit_web_view_stop_loading(WEBKIT_WEB_VIEW(GetHandle()));
}

bool WebView::GetLoading() const
{
    return webkit_web_view_is_loading(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::Reload()
{
    webkit_web_view_reload(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::ReloadBypassCache()
{
    webkit_web_view_reload_bypass_cache(WEBKIT_WEB_VIEW(GetHandle()));
}

float WebView::GetZoomLevel() const
{
    return (float)webkit_web_view_get_zoom_level(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::SetZoomLevel(float value)
{
    webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(GetHandle()), (double)value);
}

bool WebView::GetEditable() const
{
    return webkit_web_view_is_editable(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::SetEditable(bool value)
{
    webkit_web_view_set_editable(WEBKIT_WEB_VIEW(GetHandle()), value);
}

bool WebView::GetMuted() const
{
    return webkit_web_view_get_is_muted(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::SetMuted(bool value)
{
    webkit_web_view_set_is_muted(WEBKIT_WEB_VIEW(GetHandle()), value);
}

void WebView::GoBack()
{
    webkit_web_view_go_back(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::GoForward()
{
    webkit_web_view_go_forward(WEBKIT_WEB_VIEW(GetHandle()));
}

bool WebView::GetCanGoBack() const
{
    return webkit_web_view_can_go_back(WEBKIT_WEB_VIEW(GetHandle()));
}

bool WebView::GetCanGoForward() const
{
    return webkit_web_view_can_go_forward(WEBKIT_WEB_VIEW(GetHandle()));
}

std::string WebView::GetTitle() const
{
    const char* result = webkit_web_view_get_title(WEBKIT_WEB_VIEW(GetHandle()));
    if (!result)
        return "";
    return result;
}

std::string WebView::GetUri() const
{
    const char* result = webkit_web_view_get_uri(WEBKIT_WEB_VIEW(GetHandle()));
    if (!result)
        return "";
    return result;
}

bool WebView::GetInspectorIsAttached() const
{
    auto* inspector = webkit_web_view_get_inspector(WEBKIT_WEB_VIEW(GetHandle()));
    if (inspector)
        return webkit_web_inspector_is_attached(inspector);
    return false;
}

void WebView::InspectorClose()
{
    auto* inspector = webkit_web_view_get_inspector(WEBKIT_WEB_VIEW(GetHandle()));
    if (inspector)
        webkit_web_inspector_close(inspector);
}

void WebView::InspectorShow()
{
    auto* inspector = webkit_web_view_get_inspector(WEBKIT_WEB_VIEW(GetHandle()));
    if (inspector)
        webkit_web_inspector_show(inspector);
}

std::string WebView::GetCustomCharset() const
{
    return webkit_web_view_get_custom_charset(WEBKIT_WEB_VIEW(GetHandle()));
}

void WebView::SetCustomCharset(const std::string& value)
{
    webkit_web_view_set_custom_charset(WEBKIT_WEB_VIEW(GetHandle()), value.c_str());
}

sa::tl::Value WebView::GetFavicon() const
{
    auto* surface = webkit_web_view_get_favicon(WEBKIT_WEB_VIEW(GetHandle()));
    if (!surface)
        return {};
    auto* meta = ctx_.GetMetatable<CairoSurface>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<CairoSurface>(ctx_, *meta, ctx_, surface);
}

sa::tl::TablePtr WebView::GetSettings() const
{
    auto result = sa::tl::MakeTable();
    auto* webkitSettings = webkit_web_view_get_settings(WEBKIT_WEB_VIEW(GetHandle()));
    result->Add<bool>("enable-javascript", webkit_settings_get_enable_javascript(webkitSettings));
    result->Add<bool>("auto-load-images", webkit_settings_get_auto_load_images(webkitSettings));
    result->Add<bool>("enable-html5-local-storage", webkit_settings_get_enable_html5_local_storage(webkitSettings));
    result->Add<bool>("enable-html5-database", webkit_settings_get_enable_html5_database(webkitSettings));
    result->Add<bool>("javascript-can-open-windows-automatically", webkit_settings_get_javascript_can_open_windows_automatically(webkitSettings));
    result->Add<bool>("enable-hyperlink-auditing", webkit_settings_get_enable_hyperlink_auditing(webkitSettings));
    result->Add("default-font-family", sa::tl::MakeString(webkit_settings_get_default_font_family(webkitSettings)));
    result->Add("monospace-font-family", sa::tl::MakeString(webkit_settings_get_monospace_font_family(webkitSettings)));
    result->Add("serif-font-family", sa::tl::MakeString(webkit_settings_get_serif_font_family(webkitSettings)));
    result->Add("sans-serif-font-family", sa::tl::MakeString(webkit_settings_get_sans_serif_font_family(webkitSettings)));
    result->Add("cursive-font-family", sa::tl::MakeString(webkit_settings_get_cursive_font_family(webkitSettings)));
    result->Add("fantasy-font-family", sa::tl::MakeString(webkit_settings_get_fantasy_font_family(webkitSettings)));
    result->Add("pictograph-font-family", sa::tl::MakeString(webkit_settings_get_pictograph_font_family(webkitSettings)));
    result->Add("default-font-size", webkit_settings_get_default_font_size(webkitSettings));
    result->Add("default-monospace-font-size", webkit_settings_get_default_monospace_font_size(webkitSettings));
    result->Add("minimum-font-size", webkit_settings_get_minimum_font_size(webkitSettings));
    result->Add("default-charset", sa::tl::MakeString(webkit_settings_get_default_charset(webkitSettings)));
    result->Add<bool>("enable-developer-extras", webkit_settings_get_enable_developer_extras(webkitSettings));
    result->Add<bool>("enable-resizable-text-areas", webkit_settings_get_enable_resizable_text_areas(webkitSettings));
    result->Add<bool>("enable-tabs-to-links", webkit_settings_get_enable_tabs_to_links(webkitSettings));
    result->Add<bool>("enable-dns-prefetching", webkit_settings_get_enable_dns_prefetching(webkitSettings));
    result->Add<bool>("enable-caret-browsing", webkit_settings_get_enable_caret_browsing(webkitSettings));
    result->Add<bool>("enable-fullscreen", webkit_settings_get_enable_fullscreen(webkitSettings));
    result->Add<bool>("print-backgrounds", webkit_settings_get_print_backgrounds(webkitSettings));
    result->Add<bool>("enable-webaudio", webkit_settings_get_enable_webaudio(webkitSettings));
    result->Add<bool>("enable-webgl", webkit_settings_get_enable_webgl(webkitSettings));
    result->Add<bool>("allow-modal-dialogs", webkit_settings_get_allow_modal_dialogs(webkitSettings));
    result->Add<bool>("zoom-text-only", webkit_settings_get_zoom_text_only(webkitSettings));
    result->Add<bool>("javascript-can-access-clipboard", webkit_settings_get_javascript_can_access_clipboard(webkitSettings));
    result->Add<bool>("media-playback-requires-user-gesture", webkit_settings_get_media_playback_requires_user_gesture(webkitSettings));
    result->Add<bool>("media-playback-allows-inline", webkit_settings_get_media_playback_allows_inline(webkitSettings));
    result->Add<bool>("draw-compositing-indicators", webkit_settings_get_draw_compositing_indicators(webkitSettings));
    result->Add<bool>("enable-site-specific-quirks", webkit_settings_get_enable_site_specific_quirks(webkitSettings));
    result->Add<bool>("enable-page-cache", webkit_settings_get_enable_page_cache(webkitSettings));
    result->Add("user-agent", sa::tl::MakeString(webkit_settings_get_user_agent(webkitSettings)));
    result->Add<bool>("enable-smooth-scrolling", webkit_settings_get_enable_smooth_scrolling(webkitSettings));
    result->Add<bool>("enable-write-console-messages-to-stdout", webkit_settings_get_enable_write_console_messages_to_stdout(webkitSettings));
    result->Add<bool>("enable-media-stream", webkit_settings_get_enable_media_stream(webkitSettings));
    result->Add<bool>("enable-mock-capture-devices", webkit_settings_get_enable_mock_capture_devices(webkitSettings));
    result->Add<bool>("enable-spatial-navigation", webkit_settings_get_enable_spatial_navigation(webkitSettings));
    result->Add<bool>("enable-mediasource", webkit_settings_get_enable_mediasource(webkitSettings));
    result->Add<bool>("enable-encrypted-media", webkit_settings_get_enable_encrypted_media(webkitSettings));
    result->Add<bool>("enable-media-capabilities", webkit_settings_get_enable_media_capabilities(webkitSettings));
    result->Add<bool>("allow-file-access-from-file-urls", webkit_settings_get_allow_file_access_from_file_urls(webkitSettings));
    result->Add<bool>("allow-universal-access-from-file-urls", webkit_settings_get_allow_universal_access_from_file_urls(webkitSettings));
    result->Add<bool>("allow-top-navigation-to-data-urls", webkit_settings_get_allow_top_navigation_to_data_urls(webkitSettings));
    result->Add("hardware-acceleration-policy", webkit_settings_get_hardware_acceleration_policy(webkitSettings));
    result->Add<bool>("enable-back-forward-navigation-gestures", webkit_settings_get_enable_back_forward_navigation_gestures(webkitSettings));
    result->Add<bool>("enable-javascript-markup", webkit_settings_get_enable_javascript_markup(webkitSettings));
    result->Add<bool>("enable-media", webkit_settings_get_enable_media(webkitSettings));
    result->Add("media-content-types-requiring-hardware-support", sa::tl::MakeString(webkit_settings_get_media_content_types_requiring_hardware_support(webkitSettings)));
    result->Add<bool>("enable-webrtc", webkit_settings_get_enable_webrtc(webkitSettings));
    return result;
}

void WebView::SetSettings(const sa::tl::Table& value)
{
    auto* webkitSettings = webkit_web_view_get_settings(WEBKIT_WEB_VIEW(GetHandle()));
    for (const auto& setting : value)
    {
        std::string key = sa::tl::KeyToString(setting.first);
        if (key == "enable-javascript")
            webkit_settings_set_enable_javascript(webkitSettings, setting.second->ToBool());
        else if (key == "auto-load-images")
            webkit_settings_set_auto_load_images(webkitSettings, setting.second->ToBool());
        else if (key == "enable-html5-local-storage")
            webkit_settings_set_enable_html5_local_storage(webkitSettings, setting.second->ToBool());
        else if (key == "enable-html5-database")
            webkit_settings_set_enable_html5_database(webkitSettings, setting.second->ToBool());
        else if (key == "javascript-can-open-windows-automatically")
            webkit_settings_set_javascript_can_open_windows_automatically(webkitSettings, setting.second->ToBool());
        else if (key == "enable-hyperlink-auditing")
            webkit_settings_set_enable_hyperlink_auditing(webkitSettings, setting.second->ToBool());
        else if (key == "default-font-family")
            webkit_settings_set_default_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "monospace-font-family")
            webkit_settings_set_monospace_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "serif-font-family")
            webkit_settings_set_serif_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "sans-serif-font-family")
            webkit_settings_set_sans_serif_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "cursive-font-family")
            webkit_settings_set_cursive_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "fantasy-font-family")
            webkit_settings_set_fantasy_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "pictograph-font-family")
            webkit_settings_set_pictograph_font_family(webkitSettings, setting.second->ToString().c_str());
        else if (key == "default-font-size")
            webkit_settings_set_default_font_size(webkitSettings, setting.second->ToUint());
        else if (key == "default-monospace-font-size")
            webkit_settings_set_default_monospace_font_size(webkitSettings, setting.second->ToUint());
        else if (key == "minimum-font-size")
            webkit_settings_set_minimum_font_size(webkitSettings, setting.second->ToUint());
        else if (key == "default-charset")
            webkit_settings_set_default_charset(webkitSettings, setting.second->ToString().c_str());
        else if (key == "enable-developer-extras")
            webkit_settings_set_enable_developer_extras(webkitSettings, setting.second->ToBool());
        else if (key == "enable-resizable-text-areas")
            webkit_settings_set_enable_resizable_text_areas(webkitSettings, setting.second->ToBool());
        else if (key == "enable-tabs-to-links")
            webkit_settings_set_enable_tabs_to_links(webkitSettings, setting.second->ToBool());
        else if (key == "enable-dns-prefetching")
            webkit_settings_set_enable_dns_prefetching(webkitSettings, setting.second->ToBool());
        else if (key == "enable-caret-browsing")
            webkit_settings_set_enable_caret_browsing(webkitSettings, setting.second->ToBool());
        else if (key == "enable-fullscreen")
            webkit_settings_set_enable_fullscreen(webkitSettings, setting.second->ToBool());
        else if (key == "print-backgrounds")
            webkit_settings_set_print_backgrounds(webkitSettings, setting.second->ToBool());
        else if (key == "enable-webaudio")
            webkit_settings_set_enable_webaudio(webkitSettings, setting.second->ToBool());
        else if (key == "enable-webgl")
            webkit_settings_set_enable_webgl(webkitSettings, setting.second->ToBool());
        else if (key == "allow-modal-dialogs")
            webkit_settings_set_allow_modal_dialogs(webkitSettings, setting.second->ToBool());
        else if (key == "zoom-text-only")
            webkit_settings_set_zoom_text_only(webkitSettings, setting.second->ToBool());
        else if (key == "javascript-can-access-clipboard")
            webkit_settings_set_javascript_can_access_clipboard(webkitSettings, setting.second->ToBool());
        else if (key == "media-playback-requires-user-gesture")
            webkit_settings_set_media_playback_requires_user_gesture(webkitSettings, setting.second->ToBool());
        else if (key == "media-playback-allows-inline")
            webkit_settings_set_media_playback_allows_inline(webkitSettings, setting.second->ToBool());
        else if (key == "draw-compositing-indicators")
            webkit_settings_set_draw_compositing_indicators(webkitSettings, setting.second->ToBool());
        else if (key == "enable-site-specific-quirks")
            webkit_settings_set_enable_site_specific_quirks(webkitSettings, setting.second->ToBool());
        else if (key == "enable-page-cache")
            webkit_settings_set_enable_page_cache(webkitSettings, setting.second->ToBool());
        else if (key == "user-agent")
        {
            if (setting.second->IsNull())
                webkit_settings_set_user_agent(webkitSettings, setting.second->ToString().c_str());
            else
                webkit_settings_set_user_agent(webkitSettings, nullptr);
        }
        else if (key == "enable-smooth-scrolling")
            webkit_settings_set_enable_smooth_scrolling(webkitSettings, setting.second->ToBool());
        else if (key == "enable-write-console-messages-to-stdout")
            webkit_settings_set_enable_write_console_messages_to_stdout(webkitSettings, setting.second->ToBool());
        else if (key == "enable-media-stream")
            webkit_settings_set_enable_media_stream(webkitSettings, setting.second->ToBool());
        else if (key == "enable-mock-capture-devices")
            webkit_settings_set_enable_mock_capture_devices(webkitSettings, setting.second->ToBool());
        else if (key == "enable-spatial-navigation")
            webkit_settings_set_enable_spatial_navigation(webkitSettings, setting.second->ToBool());
        else if (key == "enable-mediasource")
            webkit_settings_set_enable_mediasource(webkitSettings, setting.second->ToBool());
        else if (key == "enable-encrypted-media")
            webkit_settings_set_enable_encrypted_media(webkitSettings, setting.second->ToBool());
        else if (key == "enable-media-capabilities")
            webkit_settings_set_enable_media_capabilities(webkitSettings, setting.second->ToBool());
        else if (key == "allow-file-access-from-file-urls")
            webkit_settings_set_allow_file_access_from_file_urls(webkitSettings, setting.second->ToBool());
        else if (key == "allow-universal-access-from-file-urls")
            webkit_settings_set_allow_universal_access_from_file_urls(webkitSettings, setting.second->ToBool());
        else if (key == "allow-top-navigation-to-data-urls")
            webkit_settings_set_allow_top_navigation_to_data_urls(webkitSettings, setting.second->ToBool());
        else if (key == "hardware-acceleration-policy")
            webkit_settings_set_hardware_acceleration_policy(webkitSettings, (WebKitHardwareAccelerationPolicy)setting.second->ToUint());
        else if (key == "enable-back-forward-navigation-gestures")
            webkit_settings_set_enable_back_forward_navigation_gestures(webkitSettings, setting.second->ToBool());
        else if (key == "enable-javascript-markup")
            webkit_settings_set_enable_javascript_markup(webkitSettings, setting.second->ToBool());
        else if (key == "enable-media")
            webkit_settings_set_enable_media(webkitSettings, setting.second->ToBool());
        else if (key == "enable-media")
            webkit_settings_set_enable_media(webkitSettings, setting.second->ToBool());
        else if (key == "media-content-types-requiring-hardware-support")
            webkit_settings_set_media_content_types_requiring_hardware_support(webkitSettings, setting.second->ToString().c_str());
        else if (key == "enable-webrtc")
            webkit_settings_set_enable_webrtc(webkitSettings, setting.second->ToBool());

        else
        {
            ctx_.RuntimeError(sa::tl::Error::InvalidArgument, "Unknown settings key \"" + key + "\"");
            return;
        }
    }
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::WebView>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::WebView, ui::Widget>();
    result
        .AddFunction<ui::WebView, void, const std::string&>("load_uri", &ui::WebView::LoadUri)
        .AddFunction<ui::WebView, void, const std::string&, const std::string&>("load_html", &ui::WebView::LoadHtml)
        .AddFunction<ui::WebView, void, const std::string&>("load_plain_text", &ui::WebView::LoadPlainText)
        .AddFunction<ui::WebView, void>("stop_loading", &ui::WebView::StopLoading)
        .AddFunction<ui::WebView, void>("reload", &ui::WebView::Reload)
        .AddFunction<ui::WebView, void>("reload_bypass_cache", &ui::WebView::ReloadBypassCache)
        .AddFunction<ui::WebView, void>("go_back", &ui::WebView::GoBack)
        .AddFunction<ui::WebView, void>("go_forward", &ui::WebView::GoForward)
        .AddFunction<ui::WebView, void>("inspector_close", &ui::WebView::InspectorClose)
        .AddFunction<ui::WebView, void>("inspector_show", &ui::WebView::InspectorShow)
        .AddProperty("inspector_is_attached", &ui::WebView::GetInspectorIsAttached)
        .AddProperty("context", &ui::WebView::GetContext)
        .AddProperty("zoom_level", &ui::WebView::GetZoomLevel, &ui::WebView::SetZoomLevel)
        .AddProperty("title", &ui::WebView::GetTitle)
        .AddProperty("uri", &ui::WebView::GetUri)
        .AddProperty("custom_charset", &ui::WebView::GetCustomCharset, &ui::WebView::SetCustomCharset)
        .AddProperty("favicon", &ui::WebView::GetFavicon)
        .AddProperty("https", &ui::WebView::GetHttps)
        .AddProperty("editable", &ui::WebView::GetEditable, &ui::WebView::SetEditable)
        .AddProperty("muted", &ui::WebView::GetMuted, &ui::WebView::SetMuted)
        .AddProperty("loading", &ui::WebView::GetLoading)
        .AddProperty("can_go_back", &ui::WebView::GetCanGoBack)
        .AddProperty("can_go_forward", &ui::WebView::GetCanGoForward)
        .AddProperty("settings", &ui::WebView::GetSettings, &ui::WebView::SetSettings)
        .AddProperty("on_title_changed", &ui::WebView::GetOnTitleChanged, &ui::WebView::SetOnTitleChanged)
        .AddProperty("on_load_changed", &ui::WebView::GetOnLoadChanged, &ui::WebView::SetOnLoadChanged)
        .AddProperty("on_load_failed", &ui::WebView::GetOnLoadFailed, &ui::WebView::SetOnLoadFailed)
        .AddProperty("on_load_failed_tls", &ui::WebView::GetOnLoadFailedTls, &ui::WebView::SetOnLoadFailedTls)
        .AddProperty("on_premission_requested", &ui::WebView::GetOnPermissionRequested, &ui::WebView::SetOnPermissionRequested)
        .AddProperty("on_mouse_target_changed", &ui::WebView::GetOnMouseTargetChanged, &ui::WebView::SetOnMouseTargetChanged)
        .AddProperty("on_resource", &ui::WebView::GetOnResource, &ui::WebView::SetOnResource)
        .AddProperty("on_navigate", &ui::WebView::GetOnNavigate, &ui::WebView::SetOnNavigate)
        .AddProperty("on_new_window", &ui::WebView::GetOnNewWindow, &ui::WebView::SetOnNewWindow);
    return result;
}
}

#endif
