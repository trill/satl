/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "radiobutton.h"

namespace ui {

void Radiobutton::OnToggled(GtkToggleButton*, Radiobutton* data)
{
    if (!data->updating_ && data->onToggled_)
        (*data->onToggled_)(data->ctx_, *data->GetSelfPtr(), {});
}

Radiobutton::Radiobutton(sa::tl::Context& ctx, std::string name, Radiobutton* member) :
    Widget(ctx, gtk_radio_button_new_with_label_from_widget(member ? GTK_RADIO_BUTTON(member->GetHandle()) : nullptr, name.c_str()), name)
{
    g_signal_connect(GetHandle(), "toggled", G_CALLBACK(Radiobutton::OnToggled), this);
    gtk_widget_show(GetHandle());
}

Radiobutton::~Radiobutton() = default;

bool Radiobutton::GetActive() const
{
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GetHandle()));
}

void Radiobutton::SetActive(bool value)
{
    updating_ = true;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GetHandle()), value);
    updating_ = false;
}

void Radiobutton::JoinGroup(const sa::tl::ObjectPtr& member)
{
    if (auto* mem = member->GetMetatable().CastTo<Radiobutton>(member.get()))
    {
        gtk_radio_button_join_group(GTK_RADIO_BUTTON(GetHandle()), GTK_RADIO_BUTTON(mem->GetHandle()));
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Radiobutton instance");
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Radiobutton>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Radiobutton, ui::Widget>();
    result
        .AddFunction<ui::Radiobutton, void, const sa::tl::ObjectPtr&>("join_group", &ui::Radiobutton::JoinGroup)
        .AddProperty("active", &ui::Radiobutton::GetActive, &ui::Radiobutton::SetActive)
        .AddProperty("on_toggled", &ui::Radiobutton::GetOnToggled, &ui::Radiobutton::SetOnToggled);
    return result;
}

}
