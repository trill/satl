/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cairo.h>
#include <object.h>
#include <non_copyable.h>
#include <context.h>

namespace ui {

// https://www.cairographics.org/manual/cairo-Regions.html
class CairoRegion : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(CairoRegion);
    SATL_NON_MOVEABLE(CairoRegion);
    TYPED_OBJECT(CairoRegion);
public:
    explicit CairoRegion(sa::tl::Context& ctx);
    CairoRegion(sa::tl::Context& ctx, cairo_region_t* region, bool own);
    CairoRegion(sa::tl::Context& ctx, int x, int y, int width, int height);
    ~CairoRegion() override;

    sa::tl::Value Copy() const;
    bool Equal(const sa::tl::ObjectPtr& other) const;
    bool Intersect(const sa::tl::ObjectPtr& other);
    bool Substract(const sa::tl::ObjectPtr& other);
    bool Union(const sa::tl::ObjectPtr& other);
    bool Xor(const sa::tl::ObjectPtr& other);
    void Translate(int x, int y);
    bool ContainsPoint(int x, int y);
    bool ContainsRectangle(int x, int y, int width, int height);
    bool IntersectRectangle(int x, int y, int width, int height);
    bool SubstractRectangle(int x, int y, int width, int height);
    bool UnionRectangle(int x, int y, int width, int height);
    bool XorRectangle(int x, int y, int width, int height);
    sa::tl::TablePtr GetRectanlge(int index);

    bool IsEmpty() const;
    sa::tl::TablePtr Extends() const;
    int NumRectangles() const;
private:
    cairo_region_t* region_{ nullptr };
    bool own_{ false };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::CairoRegion>(sa::tl::Context& ctx);
}
