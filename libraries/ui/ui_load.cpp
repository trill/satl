/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ui_load.h"
#include "widget.h"
#include <factory.h>
#include <satl_assert.h>
#include "radiobuttons.h"
#include "combobox.h"
#include "dialog.h"

namespace ui {

static void LoadChildren(sa::tl::Context& ctx, Widget& parent, const sa::tl::TablePtr& table);

static void LoadProperties(sa::tl::Context& ctx, const sa::tl::ObjectPtr& object, const sa::tl::TablePtr& table)
{
    sa::tl::TablePtr children;

    for (const auto& item : *table)
    {
        auto keyString = sa::tl::KeyToString(item.first);
        if (keyString == "name" || keyString == "type")
            continue;

        if (keyString == "children")
        {
            if (!item.second->Is<sa::tl::TablePtr>())
            {
                ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "children member must be a table");
                return;
            }

            children = item.second->As<sa::tl::TablePtr>();
            continue;
        }

        const auto& meta = object->GetMetatable();

        // Ignore special properties that are passed to the constructor
        if (meta.InstanceOf<ui::Dialog>() && (keyString == "buttons"))
            continue;

        // All these are regular properties
        auto prop = meta.GetPropertySetter(keyString);
        if (!prop)
        {
            ctx.RuntimeError(sa::tl::Error::Code::InvalidArgument, std::format("Unknown property \"{}\"", keyString));
            return;
        }
        (*prop)(ctx, object, { item.second });
    }

    if (children)
    {
        // Load children as last. This will make sure all properties are already set.
        auto* objPtr = object->GetMetatable().CastTo<Widget>(object.get());
        if (!objPtr)
        {
            ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not of type Widget");
            return;
        }
        LoadChildren(ctx, *objPtr, children);
    }
}

static sa::tl::CallablePtr GetConstructor(const sa::tl::Context& ctx, const std::string& name)
{
    // Finds the constructor for the type in the ui module.
    // Type and constructor function have the same names.
    auto module = ctx.GetValue("ui", 0);    
    if (!module)
        return {};
    if (!module->IsTable())
        return {};
    auto& tbl = *module->As<sa::tl::TablePtr>();
    if (!tbl.IsModule())
        return {};
    auto value = tbl.GetValue(name);
    if (!value)
        return {};
    if (!value->IsCallable())
        return {};
    return value->As<sa::tl::CallablePtr>();
}

static sa::tl::Value CreateWidget(sa::tl::Context& ctx, const std::string& type, const std::string& name, const sa::tl::TablePtr& table)
{
    if (type == "dialog")
    {
        // Dialog is special, it takes some more arguments.
        ui::Window* p = nullptr;
        auto it = table->find("parent");
        if (it != table->end())
        {
            if (!it->second->IsNull() && !it->second->IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return nullptr;
            }
            if (!it->second->IsNull())
            {
                auto& o = it->second->As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return nullptr;
                }
            }
        }
        int buttons = 0;
        if (!sa::tl::GetTableValue(ctx, *table, "buttons", buttons))
        {
            ctx.RuntimeError(sa::tl::Error::InvalidArgument, "\"button\" member is missing");
            return nullptr;
        }

        auto* meta = ctx.GetMetatable<ui::Dialog>();
        SATL_ASSERT(meta);
        return sa::tl::bind::CreateInstance<ui::Dialog>(ctx, *meta, ctx, name, p, buttons);
    }

    auto ctor = GetConstructor(ctx, type);
    if (!ctor)
    {
        ctx.RuntimeError(sa::tl::Error::UnknownFunction, std::format("Constructor \"{}\" not found", type));
        return nullptr;
    }
    if (ctor->ParamCount() != 1)
    {
        ctx.RuntimeError(sa::tl::Error::ArgumentsMismatch, "Constructor must have 1 parameter");
        return nullptr;
    }
    return *(*ctor)(ctx, { sa::tl::MakeValue(name) });
}

static sa::tl::Value LoadWidget(sa::tl::Context& ctx, const sa::tl::TablePtr& table)
{
    std::string type;
    if (!sa::tl::GetTableValue(ctx, *table, "type", type))
        return nullptr;
    std::string name;
    sa::tl::GetTableOptionalValue(ctx, *table, "name", name);

    sa::tl::Value result = CreateWidget(ctx, type, name, table);
    if (result.IsNull())
        return nullptr;

    auto& object = result.As<sa::tl::ObjectPtr>();
    LoadProperties(ctx, object, table);
    return result;
}

static void LoadChildren(sa::tl::Context& ctx, Widget& parent, const sa::tl::TablePtr& table)
{
    for (const auto& item : *table)
    {
        auto child = LoadWidget(ctx, item.second->As<sa::tl::TablePtr>());
        if (!child.IsNull())
            parent.Add(child);
    }
}

sa::tl::Value Load(sa::tl::Context& ctx, sa::tl::TablePtr table)
{
    return LoadWidget(ctx, table);
}

}
