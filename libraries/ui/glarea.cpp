/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "glarea.h"
#include "glcontext.h"

namespace ui {

bool GLArea::OnRender(GtkGLArea*, GdkGLContext* context, GLArea* area)
{
    return area->DoOnRender(context);
}

void GLArea::OnResize(GtkGLArea*, int width, int height, GLArea* area)
{
    area->DoOnResize(width, height);
}

GLArea::GLArea(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_gl_area_new(), std::forward<std::string>(name))
{
    g_signal_connect(GetHandle(), "render", G_CALLBACK(GLArea::OnRender), this);
    g_signal_connect(GetHandle(), "resize", G_CALLBACK(GLArea::OnResize), this);
    gtk_widget_show(GetHandle());
}

GLArea::~GLArea() = default;

void GLArea::MakeCurrent()
{
    gtk_gl_area_make_current(GTK_GL_AREA(GetHandle()));
}

bool GLArea::DoOnRender(GdkGLContext* context)
{
    if (onRealize_)
    {
        GLContext glContext(ctx_, context);
        auto* meta = ctx_.GetMetatable<GLContext>();
        SATL_ASSERT(meta);
        auto ptr = sa::tl::MakeValue(sa::tl::MakeObject(*meta, &glContext));
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onRealize_),
                0,
                1,
                [&](int)
                {
                    args.push_back(std::move(ptr));
                }))
            return false;

        if ((*onRealize_)(ctx_, *GetSelfPtr(), args)->ToBool())
            return true;
    }
    return false;
}

void GLArea::DoOnResize(int width, int height)
{
    if (onResize_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onRealize_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("width", width);
                    data->Add("height", height);
                    args.push_back(sa::tl::MakeValue(data));
                }))
            return;

        (*onRealize_)(ctx_, *GetSelfPtr(), args);
    }
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::GLArea>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::GLArea, ui::Widget>();
    result
        .AddFunction<ui::GLArea, void>("make_current", &ui::GLArea::MakeCurrent)
        .AddProperty("on_resize", &ui::GLArea::GetOnResize, &ui::GLArea::SetOnResize)
        .AddProperty("on_render", &ui::GLArea::GetOnRender, &ui::GLArea::SetOnRender);
    return result;
}

}
