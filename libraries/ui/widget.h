/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <string>
#include <value.h>
#include <vector>
#include <ast.h>
#include <object.h>

namespace ui {

template<typename Callback>
inline bool AddArguments(sa::tl::Context& ctx, const sa::tl::Callable& function, int min, int max, Callback callback)
{
    if (function.ParamCount() < min || function.ParamCount() > max)
    {
        ctx.RuntimeError(sa::tl::Error::ArgumentsMismatch, std::format("Wrong number of arguments, exprected min {} max {}", min, max));
        return false;
    }

    for (int i = 0; i < function.ParamCount(); ++i)
    {
        callback(i);
    }
    return true;
}

inline constexpr int DEF_MARGIN = 12;

class Menu;

class Widget : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Widget);
    SATL_NON_MOVEABLE(Widget);
    TYPED_OBJECT(Widget);
public:
    enum Modifier : uint32_t
    {
        ModifierCtrl = 1u,
        ModifierAlt = 1u << 1u,
        ModifierShift = 1u << 2u,
        ModifierSuper = 1u << 3u
    };
    ~Widget() override;
    [[nodiscard]] GtkWidget* GetHandle() const { return handle_; }

    void Destroy() override;
    virtual void Show();
    void Hide();
    virtual void Add(const sa::tl::Value&);
    virtual void Add(Widget* child);
    void Remove(Widget* child);
    [[nodiscard]] int GetCount() const { return (int)children_.size(); }
    sa::tl::Value Find(const std::string& name, bool recursive);
    sa::tl::Value GetChild(int index);
    [[nodiscard]] sa::tl::Value GetWindow() const;
    Widget* GetWidgetFromHandle(GtkWidget* handle, bool recursive);
    bool VisitChildren(const sa::tl::Callable& callback);
    [[nodiscard]] sa::tl::CallablePtr GetOnRealize() const { return onRealize_; }
    void SetOnRealize(sa::tl::CallablePtr value) { onRealize_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnResized() const { return onResized_; }
    void SetOnResized(sa::tl::CallablePtr value) { onResized_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnButtonPress() const { return onButtonPress_; }
    void SetOnButtonPress(sa::tl::CallablePtr value) { onButtonPress_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnKeyPress() const { return onKeyPress_; }
    void SetOnKeyPress(sa::tl::CallablePtr value) { onKeyPress_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnEnter() const { return onEnter_; }
    void SetOnEnter(sa::tl::CallablePtr value) { onEnter_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnLeave() const { return onLeave_; }
    void SetOnLeave(sa::tl::CallablePtr value) { onLeave_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnScroll() const { return onScroll_; }
    void SetOnScroll(sa::tl::CallablePtr value) { onScroll_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnDraw() const { return onDraw_; }
    void SetOnDraw(sa::tl::CallablePtr value) { onDraw_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnShow() const { return onShow_; }
    void SetOnShow(sa::tl::CallablePtr value) { onShow_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnHide() const { return onHide_; }
    void SetOnHide(sa::tl::CallablePtr value) { onHide_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnFocusIn() const { return onFocusIn_; }
    void SetOnFocusIn(sa::tl::CallablePtr value) { onFocusIn_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnFocusOut() const { return onFocusOut_; }
    void SetOnFocusOut(sa::tl::CallablePtr value) { onFocusOut_ = std::move(value); }

    [[nodiscard]] std::string GetName() const { return name_; }
    void Focus();
    [[nodiscard]] bool HasFocus() const;
    [[nodiscard]] bool GetCanFocus() const;
    void SetCanFocus(bool value);
    void Redraw();
    void RedrawArea(int x, int y, int width, int height);
    [[nodiscard]] bool GetEnabled() const;
    void SetEnabled(bool value);
    [[nodiscard]] int GetHAlign() const;
    void SetHAlign(int value);
    [[nodiscard]] int GetVAlign() const;
    void SetVAlign(int value);
    [[nodiscard]] bool GetHExpand() const;
    void SetHExpand(bool value);
    [[nodiscard]] bool GetVExpand() const;
    void SetVExpand(bool value);
    [[nodiscard]] int GetMinWidth() const;
    void SetMinWidth(int value);
    [[nodiscard]] int GetMinHeight() const;
    void SetMinHeight(int value);
    [[nodiscard]] sa::tl::TablePtr GetMinSize() const;
    void SetMinSize(const sa::tl::Table& value);
    [[nodiscard]] std::string GetTooltip() const;
    void SetTooltip(const std::string& value);
    [[nodiscard]] bool IsVisible() const { return visible_; }
    void SetVisible(bool value);
    virtual void SetMargins(const sa::tl::Table&);
    [[nodiscard]] virtual sa::tl::TablePtr GetMargins() const;
    [[nodiscard]] virtual sa::tl::TablePtr GetRect() const;
    [[nodiscard]] int GetWidth() const;
    [[nodiscard]] int GetHeight() const;
    [[nodiscard]] sa::tl::Float GetOpacity() const;
    void SetOpacity(sa::tl::Float value);
    void SetMargins(int left, int top, int right, int bottom);
    [[nodiscard]] sa::tl::Value GetParent() const;
    [[nodiscard]] bool HasParent() const;
    [[nodiscard]] sa::tl::Value GetUserData() const;
    void SetUserData(sa::tl::Value value);
    [[nodiscard]] bool GetFocusOnClick() const;
    void SetFocusOnClick(bool value);
protected:
    explicit Widget(sa::tl::Context& ctx, GtkWidget* handle, std::string name);
    [[nodiscard]] virtual GtkWidget* GetContainer() const { return handle_; }
    void InternalAdd(Widget* child);

    virtual void DoOnResized(GdkRectangle*);
    virtual void DoOnRealize();
    virtual bool DoOnButtonPress(GdkEventButton* event);
    virtual bool DoOnPopupMenu();
    virtual void DoOnShow();
    virtual void DoOnHide();
    virtual bool DoOnKeyPress(GdkEventKey* event);
    virtual bool DoOnDraw(cairo_t* cr);
    virtual bool DoOnEnter(GdkEventCrossing* event);
    virtual bool DoOnLeave(GdkEventCrossing* event);
    virtual bool DoOnScroll(GdkEventScroll* event);
    virtual bool DoOnFocusIn(GdkEventFocus* event);
    virtual bool DoOnFocusOut(GdkEventFocus* event);

    std::vector<sa::tl::WeakObject> children_;
    sa::tl::WeakObject popupMenu_;
    bool visible_{ false };
private:
    GtkWidget* handle_;
    std::string name_;
    sa::tl::WeakObject parent_;
    sa::tl::CallablePtr onResized_;
    sa::tl::CallablePtr onRealize_;
    sa::tl::CallablePtr onButtonPress_;
    sa::tl::CallablePtr onKeyPress_;
    sa::tl::CallablePtr onDraw_;
    sa::tl::CallablePtr onEnter_;
    sa::tl::CallablePtr onLeave_;
    sa::tl::CallablePtr onScroll_;
    sa::tl::CallablePtr onShow_;
    sa::tl::CallablePtr onHide_;
    sa::tl::CallablePtr onFocusIn_;
    sa::tl::CallablePtr onFocusOut_;
    sa::tl::Value userData_;
    static uint32_t StateToModifiers(guint state);
    static void OnResized(GtkWidget*, GdkRectangle*, Widget* wnd);
    static void OnRealize(GtkWidget*, Widget* wnd);
    static bool OnPopupMenu(GtkWidget*, Widget* wnd);
    static void OnShow(GtkWidget*, Widget* wnd);
    static void OnHide(GtkWidget*, Widget* wnd);
    static bool OnButtonPress(GtkWidget*, GdkEventButton* event, Widget* wnd);
    static bool OnKeyPress(GtkWidget*, GdkEventKey* event, Widget* wnd);
    static bool OnDraw(GtkWidget*, cairo_t* cr, Widget* wnd);
    static bool OnEnter(GtkWidget*, GdkEventCrossing* event, Widget* wnd);
    static bool OnScroll(GtkWidget*, GdkEventScroll* event, Widget* wnd);
    static bool OnFocusIn(GtkWidget*, GdkEventFocus* event, Widget* wnd);
    static bool OnFocusOut(GtkWidget*, GdkEventFocus* event, Widget* wnd);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Widget>(sa::tl::Context& ctx);
}
