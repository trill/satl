/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Combobox : public Widget
{
    SATL_NON_COPYABLE(Combobox);
    SATL_NON_MOVEABLE(Combobox);
    TYPED_OBJECT(Combobox);
public:
    Combobox(sa::tl::Context& ctx, std::string name);
    ~Combobox() override;

    void AddItem(std::string value);
    void RemoveItem(const std::string& value);
    [[nodiscard]] int GetSelected() const;
    sa::tl::TablePtr GetItems() const;
    void SetItems(const sa::tl::TablePtr& value);
    void SetSelected(int value);
    [[nodiscard]] sa::tl::Value GetValue() const;
    void SetValue(const sa::tl::Value& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
protected:
    Combobox(sa::tl::Context& ctx, GtkWidget* handle, std::string name);
    gulong changedHandler_;
private:
    static void OnChanged(GtkComboBox* button, Combobox* btn);
    sa::tl::CallablePtr onChanged_;
    std::vector<std::string> items_;
};

class ComboboxText : public Combobox
{
    SATL_NON_COPYABLE(ComboboxText);
    SATL_NON_MOVEABLE(ComboboxText);
    TYPED_OBJECT(ComboboxText);
public:
    ComboboxText(sa::tl::Context& ctx, std::string name);
    ~ComboboxText() override;
    [[nodiscard]] std::string GetText() const;
    void SetText(const std::string& value);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Combobox>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::ComboboxText>(sa::tl::Context& ctx);
}
