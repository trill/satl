/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "button.h"
#include <ast.h>

namespace ui {

void Button::OnClicked(GtkButton*, Button* btn)
{
    if (btn->onClicked_)
        (*btn->onClicked_)(btn->ctx_, *btn->GetSelfPtr(), {});
}

Button::Button(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_button_new_with_label(name.c_str()), name)
{
    g_signal_connect(GetHandle(), "clicked", G_CALLBACK(Button::OnClicked), this);
    gtk_widget_show(GetHandle());
}


Button::Button(sa::tl::Context& ctx, GtkWidget* handle, std::string name) :
    Widget(ctx, handle, std::forward<std::string>(name))
{
    g_signal_connect(GetHandle(), "clicked", G_CALLBACK(Button::OnClicked), this);
    gtk_widget_show(GetHandle());
}

Button::~Button() = default;

std::string Button::GetText() const
{
    return gtk_button_get_label(GTK_BUTTON(GetHandle()));
}

void Button::SetText(const std::string& value)
{
    gtk_button_set_label(GTK_BUTTON(GetHandle()), value.c_str());
}

const GdkRGBA ColorButton::black = { 0.0, 0.0, 0.0, 1.0 };

void ColorButton::OnColorSet(GtkButton* button, ColorButton* btn)
{
    if (btn->onColorSet_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(btn->ctx_,
                (*btn->onColorSet_),
                0,
                1,
                [&](int)
                {
                    GdkRGBA rgba;
                    gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(button), &rgba);
                    auto data = sa::tl::MakeTable();
                    data->Add<sa::tl::Float>("r", rgba.red);
                    data->Add<sa::tl::Float>("g", rgba.green);
                    data->Add<sa::tl::Float>("b", rgba.blue);
                    data->Add<sa::tl::Float>("a", rgba.alpha);
                    args.push_back(sa::tl::MakeValue(data));
                }))
            return;

        (*btn->onColorSet_)(btn->ctx_, *btn->GetSelfPtr(), args);
    }
}

ColorButton::ColorButton(sa::tl::Context& ctx, std::string name) :
    Button(ctx, gtk_color_button_new_with_rgba(&black), std::forward<std::string>(name))
{
    g_signal_connect(GTK_COLOR_BUTTON(GetHandle()), "color-set", G_CALLBACK(ColorButton::OnColorSet), this);
}

ColorButton::~ColorButton() = default;

void ColorButton::SetColor(const sa::tl::Table& value)
{
    GdkRGBA rgba = { 0, 0, 0, 0};

    if (!sa::tl::GetTableValue(ctx_, value, "r", rgba.red))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "g", rgba.green))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "b", rgba.blue))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "a", rgba.alpha))
        return;
    gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(GetHandle()), &rgba);
}

sa::tl::TablePtr ColorButton::GetColor() const
{
    GdkRGBA rgba;
    gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(GetHandle()), &rgba);
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("r", rgba.red);
    result->Add<sa::tl::Float>("g", rgba.green);
    result->Add<sa::tl::Float>("b", rgba.blue);
    result->Add<sa::tl::Float>("a", rgba.alpha);

    return result;
}

void FontButton::OnFontSet(GtkFontButton* button, FontButton* btn)
{
    if (btn->onFontSet_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(btn->ctx_,
                (*btn->onFontSet_),
                0,
                1,
                [&](int)
                {
                    PangoFontDescription* pdesc = gtk_font_chooser_get_font_desc(GTK_FONT_CHOOSER(button));
                    auto data = sa::tl::MakeTable();
                    data->Add("family", std::string(pango_font_description_get_family(pdesc)));
                    data->Add("size", pango_font_description_get_size(pdesc));
                    args.push_back(sa::tl::MakeValue(data));
                }))
            return;
        (*btn->onFontSet_)(btn->ctx_, *btn->GetSelfPtr(), args);
    }
}

FontButton::FontButton(sa::tl::Context& ctx, std::string name) :
    Button(ctx, gtk_font_button_new(), std::forward<std::string>(name))
{
    g_signal_connect(GTK_FONT_BUTTON(GetHandle()), "font-set", G_CALLBACK(FontButton::OnFontSet), this);
}

FontButton::~FontButton() = default;

sa::tl::TablePtr FontButton::GetFont() const
{
    PangoFontDescription* pdesc = gtk_font_chooser_get_font_desc(GTK_FONT_CHOOSER(GetHandle()));
    auto result = sa::tl::MakeTable();
    result->Add("family", std::string(pango_font_description_get_family(pdesc)));
    result->Add("size", pango_font_description_get_size(pdesc));
    return result;
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Button>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Button, ui::Widget>();
    result
        .AddProperty("text", &ui::Button::GetText, &ui::Button::SetText)
        .AddProperty("on_clicked", &ui::Button::GetOnClicked, &ui::Button::SetOnClicked);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::ColorButton>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::ColorButton, ui::Button>();
    result
        .AddProperty("color", &ui::ColorButton::GetColor, &ui::ColorButton::SetColor)
        .AddProperty("on_color_set", &ui::ColorButton::GetOnColorSet, &ui::ColorButton::SetOnColorSet);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::FontButton>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::FontButton, ui::Button>();
    result
        .AddProperty("font", &ui::FontButton::GetFont)
        .AddProperty("on_font_set", &ui::FontButton::GetOnFontSet, &ui::FontButton::SetOnFontSet);
    return result;
}

}
