/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cairo.h>
#include <non_copyable.h>
#include <object.h>
#include <context.h>

namespace ui {

// https://www.cairographics.org/manual/cairo-cairo-pattern-t.html
class CairoPattern : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(CairoPattern);
    SATL_NON_MOVEABLE(CairoPattern);
    TYPED_OBJECT(CairoPattern);
public:
    explicit CairoPattern(sa::tl::Context& ctx);
    CairoPattern(sa::tl::Context& ctx, cairo_pattern_t* pattern, bool own);
    ~CairoPattern() override;

    bool CreateColor(const sa::tl::Table& color);
    bool CreateSurface(const sa::tl::ObjectPtr& surface);
    bool CreateLinear(sa::tl::Float x0, sa::tl::Float y0, sa::tl::Float x1, sa::tl::Float y1);
    bool CreateRadial(sa::tl::Float cx0, sa::tl::Float cy0, sa::tl::Float r0,
        sa::tl::Float cx1, sa::tl::Float cy1, sa::tl::Float r1);
    bool CreateMesh();

    void AddColorStopColor(sa::tl::Float offset, const sa::tl::Table& color);
    int GetColorStopCount();
    sa::tl::TablePtr GetColorStopColor(int index);

    [[nodiscard]] sa::tl::TablePtr GetMatrix() const;
    void SetMatrix(const sa::tl::Table& value);
    [[nodiscard]] int GetFilter() const;
    void SetFilter(int value);

    [[nodiscard]] sa::tl::TablePtr GetRGBA() const;
    [[nodiscard]] cairo_pattern_t* GetHandle() const { return pattern_; }
private:
    bool own_{ false };
    cairo_pattern_t* pattern_{ nullptr };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::CairoPattern>(sa::tl::Context& ctx);
}
