/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifdef HAVE_WEBKIT

#include "webviewcookiemanager.h"

namespace ui {

void WebViewCookieManager::OnChanged(WebKitCookieManager*, WebViewCookieManager* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

WebViewCookieManager::WebViewCookieManager(sa::tl::Context& ctx, WebKitCookieManager* handle) :
    Object(ctx),
    handle_(handle)
{
    g_signal_connect(handle_, "changed", G_CALLBACK(WebViewCookieManager::OnChanged), this);
}

void WebViewCookieManager::SetPersistentStorage(const std::string& filename, bool plaintext)
{
    webkit_cookie_manager_set_persistent_storage(handle_, filename.c_str(), plaintext ? WEBKIT_COOKIE_PERSISTENT_STORAGE_TEXT : WEBKIT_COOKIE_PERSISTENT_STORAGE_SQLITE);
}

void WebViewCookieManager::SetAccept(int value)
{
    webkit_cookie_manager_set_accept_policy(handle_, (WebKitCookieAcceptPolicy)value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::WebViewCookieManager>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::WebViewCookieManager, sa::tl::bind::Object>();
    result
        .AddFunction<ui::WebViewCookieManager, void, const std::string&, bool>("set_persistent_storage", &ui::WebViewCookieManager::SetPersistentStorage)
        .AddFunction<ui::WebViewCookieManager, void, int>("set_accept", &ui::WebViewCookieManager::SetAccept)
        .AddProperty("on_changed", &ui::WebViewCookieManager::GetOnChanged, &ui::WebViewCookieManager::SetOnChanged);
    return result;
}

}

#endif
