/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <object.h>
#include <gdk/gdk.h>

namespace ui {

// https://docs.gtk.org/gdk3/class.GLContext.html
class GLContext : public sa::tl::bind::Object
{
public:
    GLContext(sa::tl::Context& ctx, GdkGLContext* glContext);
private:
    GdkGLContext* glContext_{ nullptr };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::GLContext>(sa::tl::Context& ctx);
}
