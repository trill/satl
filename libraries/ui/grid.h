/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Grid : public Widget
{
    SATL_NON_COPYABLE(Grid);
    SATL_NON_MOVEABLE(Grid);
    TYPED_OBJECT(Grid);
public:
    Grid(sa::tl::Context& ctx, std::string name);
    ~Grid() override;
    void Add(const sa::tl::Value&) override;
    void Add(Widget* child) override;
    sa::tl::Value GetChildAt(int left, int top);
    [[nodiscard]] int GetRowSpacing() const;
    void SetRowSpacing(int value);
    [[nodiscard]] int GetColSpacing() const;
    void SetColSpacing(int value);
    [[nodiscard]] int GetNumCols() const { return numCols_; }
    void SetNumCols(int value) { numCols_ = value; }
    [[nodiscard]] bool GetColHomogeneous() const;
    void SetColHomogeneous(bool value);
    [[nodiscard]] bool GetRowHomogeneous() const;
    void SetRowHomogeneous(bool value);
private:
    [[nodiscard]] int GetLeft() const
    {
        return (int)children_.size() % (int)numCols_;
    }
    [[nodiscard]] int GetTop() const
    {
        return (int)children_.size() / (int)numCols_;
    }
    size_t numCols_{ 1 };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Grid>(sa::tl::Context& ctx);
}
