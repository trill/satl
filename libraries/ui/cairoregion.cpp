/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "cairoregion.h"
#include <factory.h>

namespace ui {

CairoRegion::CairoRegion(sa::tl::Context& ctx) :
    Object(ctx)
{
}

CairoRegion::CairoRegion(sa::tl::Context& ctx, cairo_region_t* region, bool own) :
    Object(ctx),
    region_(region),
    own_(own)
{ }

CairoRegion::CairoRegion(sa::tl::Context& ctx, int x, int y, int width, int height) :
    Object(ctx)
{
    const cairo_rectangle_int_t rect = { .x = x, .y = y, .width = width, .height = height };
    region_ = cairo_region_create_rectangle(&rect);
    own_ = true;
}

CairoRegion::~CairoRegion()
{
    if (own_ && region_)
    {
        cairo_region_destroy(region_);
        region_ = nullptr;
    }
}

sa::tl::Value CairoRegion::Copy() const
{
    auto* meta = ctx_.GetMetatable<CairoRegion>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<CairoRegion>(ctx_, *meta, ctx_, cairo_region_copy(region_), true);
}

bool CairoRegion::IsEmpty() const
{
    if (!region_)
        return true;
    return cairo_region_is_empty(region_);
}

sa::tl::TablePtr CairoRegion::Extends() const
{
    auto result = sa::tl::MakeTable();
    if (!region_)
        return result;
    cairo_rectangle_int_t rect = {};
    cairo_region_get_extents(region_, &rect);
    result->Add("x", rect.x);
    result->Add("y", rect.y);
    result->Add("width", rect.width);
    result->Add("height", rect.height);
    return result;
}

int CairoRegion::NumRectangles() const
{
    if (!region_)
        return 0;
    return cairo_region_num_rectangles(region_);
}

sa::tl::TablePtr CairoRegion::GetRectanlge(int index)
{
    auto result = sa::tl::MakeTable();
    if (!region_)
        return result;
    cairo_rectangle_int_t rect = {};
    cairo_region_get_rectangle(region_, index, &rect);
    result->Add("x", rect.x);
    result->Add("y", rect.y);
    result->Add("width", rect.width);
    result->Add("height", rect.height);
    return result;
}

bool CairoRegion::Equal(const sa::tl::ObjectPtr& other) const
{
    if (!region_)
        return false;
    if (!other)
        return false;
    if (auto* ptr = sa::tl::MetaTable::GetObject<CairoRegion>(other))
        return cairo_region_equal(region_, ptr->region_);
    return false;
}

bool CairoRegion::Intersect(const sa::tl::ObjectPtr& other)
{
    if (!region_)
        return false;
    if (!other)
        return false;
    if (auto* ptr = sa::tl::MetaTable::GetObject<CairoRegion>(other))
        return cairo_region_intersect(region_, ptr->region_) == CAIRO_STATUS_SUCCESS;
    return false;
}

bool CairoRegion::Substract(const sa::tl::ObjectPtr& other)
{
    if (!region_)
        return false;
    if (!other)
        return false;
    if (auto* ptr = sa::tl::MetaTable::GetObject<CairoRegion>(other))
        return cairo_region_subtract(region_, ptr->region_) == CAIRO_STATUS_SUCCESS;
    return false;
}

bool CairoRegion::Union(const sa::tl::ObjectPtr& other)
{
    if (!region_)
        return false;
    if (!other)
        return false;
    if (auto* ptr = sa::tl::MetaTable::GetObject<CairoRegion>(other))
        return cairo_region_union(region_, ptr->region_) == CAIRO_STATUS_SUCCESS;
    return false;
}

bool CairoRegion::Xor(const sa::tl::ObjectPtr& other)
{
    if (!region_)
        return false;
    if (!other)
        return false;
    if (auto* ptr = sa::tl::MetaTable::GetObject<CairoRegion>(other))
        return cairo_region_xor(region_, ptr->region_) == CAIRO_STATUS_SUCCESS;
    return false;
}

void CairoRegion::Translate(int x, int y)
{
    if (!region_)
        return;
    cairo_region_translate(region_, x, y);
}

bool CairoRegion::ContainsPoint(int x, int y)
{
    if (!region_)
        return false;
    return cairo_region_contains_point(region_, x, y);
}

bool CairoRegion::ContainsRectangle(int x, int y, int width, int height)
{
    if (!region_)
        return false;
    const cairo_rectangle_int_t rect = { .x = x, .y = y, .width = width, .height = height };
    return cairo_region_contains_rectangle(region_, &rect) == CAIRO_REGION_OVERLAP_IN;
}

bool CairoRegion::IntersectRectangle(int x, int y, int width, int height)
{
    if (!region_)
        return false;
    const cairo_rectangle_int_t rect = { .x = x, .y = y, .width = width, .height = height };
    return cairo_region_intersect_rectangle(region_, &rect) == CAIRO_STATUS_SUCCESS;
}

bool CairoRegion::SubstractRectangle(int x, int y, int width, int height)
{
    if (!region_)
        return false;
    const cairo_rectangle_int_t rect = { .x = x, .y = y, .width = width, .height = height };
    return cairo_region_subtract_rectangle(region_, &rect) == CAIRO_STATUS_SUCCESS;
}

bool CairoRegion::UnionRectangle(int x, int y, int width, int height)
{
    if (!region_)
        return false;
    const cairo_rectangle_int_t rect = { .x = x, .y = y, .width = width, .height = height };
    return cairo_region_union_rectangle(region_, &rect) == CAIRO_STATUS_SUCCESS;
}

bool CairoRegion::XorRectangle(int x, int y, int width, int height)
{
    if (!region_)
        return false;
    const cairo_rectangle_int_t rect = { .x = x, .y = y, .width = width, .height = height };
    return cairo_region_xor_rectangle(region_, &rect) == CAIRO_STATUS_SUCCESS;
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::CairoRegion>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::CairoRegion, Object>();
    result
        .AddFunction<ui::CairoRegion, sa::tl::Value>("copy", &ui::CairoRegion::Copy)
        .AddFunction<ui::CairoRegion, bool, const sa::tl::ObjectPtr&>("equal", &ui::CairoRegion::Equal)
        .AddFunction<ui::CairoRegion, bool, const sa::tl::ObjectPtr&>("intersect", &ui::CairoRegion::Intersect)
        .AddFunction<ui::CairoRegion, bool, const sa::tl::ObjectPtr&>("substract", &ui::CairoRegion::Substract)
        .AddFunction<ui::CairoRegion, bool, const sa::tl::ObjectPtr&>("union", &ui::CairoRegion::Union)
        .AddFunction<ui::CairoRegion, bool, const sa::tl::ObjectPtr&>("xor", &ui::CairoRegion::Xor)
        .AddFunction<ui::CairoRegion, void, int, int>("translate", &ui::CairoRegion::Translate)
        .AddFunction<ui::CairoRegion, bool, int, int>("contains_point", &ui::CairoRegion::ContainsPoint)
        .AddFunction<ui::CairoRegion, bool, int, int, int, int>("contains_rectangle", &ui::CairoRegion::ContainsRectangle)
        .AddFunction<ui::CairoRegion, bool, int, int, int, int>("intersect_rectangle", &ui::CairoRegion::IntersectRectangle)
        .AddFunction<ui::CairoRegion, bool, int, int, int, int>("substract_rectangle", &ui::CairoRegion::SubstractRectangle)
        .AddFunction<ui::CairoRegion, bool, int, int, int, int>("union_rectangle", &ui::CairoRegion::UnionRectangle)
        .AddFunction<ui::CairoRegion, bool, int, int, int, int>("xor_rectangle", &ui::CairoRegion::XorRectangle)
        .AddFunction<ui::CairoRegion, sa::tl::TablePtr, int>("get_rectangle", &ui::CairoRegion::GetRectanlge)
        .AddProperty("extends", &ui::CairoRegion::Extends)
        .AddProperty("num_rectangles", &ui::CairoRegion::NumRectangles)
        .AddProperty("empty", &ui::CairoRegion::IsEmpty);

    return result;
}

}
