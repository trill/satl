/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cairo.h>
#include <non_copyable.h>
#include <object.h>
#include <value.h>

namespace ui {

class CairoSurface;
class CairoPattern;

class Cairo : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Cairo);
    SATL_NON_MOVEABLE(Cairo);
    TYPED_OBJECT(Cairo);
public:
    explicit Cairo(sa::tl::Context& ctx);
    Cairo(sa::tl::Context& ctx, cairo_t* cr);
    Cairo(sa::tl::Context& ctx, const CairoSurface& surface);
    ~Cairo() override;

    [[nodiscard]] cairo_t* GetHandle() const { return cr_; }

    void Save();
    void Restore();
    void ShowPage();

    void Translate(sa::tl::Float x, sa::tl::Float y);
    void Scale(sa::tl::Float x, sa::tl::Float y);
    void Rotate(sa::tl::Float a);
    void Transform(const sa::tl::Table& matrix);
    void IdentityMatrix();
    sa::tl::TablePtr GetMatrix() const;
    void SetMatrix(const sa::tl::Table& value);

    void TagBegin(const std::string& name, const std::string& attributes);
    void TagEnd(const std::string& name);

    sa::tl::Value PopGroup();
    void PushGroup();
    sa::tl::Value GetSource() const;
    void SetSource(const sa::tl::Value& value);
    void InternalSetSource(const CairoPattern& source);

    void RelMoveTo(sa::tl::Float x, sa::tl::Float y);
    void MoveTo(sa::tl::Float x, sa::tl::Float y);
    void LineTo(sa::tl::Float x, sa::tl::Float y);
    void RelLineTo(sa::tl::Float x, sa::tl::Float y);
    void Arc(sa::tl::Float xc, sa::tl::Float yc, sa::tl::Float r, sa::tl::Float a1, sa::tl::Float a2);
    void ArcNegative(sa::tl::Float xc, sa::tl::Float yc, sa::tl::Float r, sa::tl::Float a1, sa::tl::Float a2);
    void CurveTo(sa::tl::Float x1, sa::tl::Float y1, sa::tl::Float x2, sa::tl::Float y2, sa::tl::Float x3, sa::tl::Float y3);
    void RelCurveTo(sa::tl::Float x1, sa::tl::Float y1, sa::tl::Float x2, sa::tl::Float y2, sa::tl::Float x3, sa::tl::Float y3);
    void Rectangle(sa::tl::Float x, sa::tl::Float y, sa::tl::Float width, sa::tl::Float height);
    void Stroke();
    void StrokePreserve();
    void Fill();
    void ClosePath();
    void NewPath();
    void NewSubPath();
    void ShowText(const std::string& txt);
    void TextPath(const std::string& txt);
    sa::tl::Float GetTextWidth(const std::string& text) const;
    sa::tl::Float GetTextHeight(const std::string& text) const;
    sa::tl::TablePtr GetTextExtends(const std::string& text) const;

    void SetFontSize(sa::tl::Float size);
    void SelectFontFace(const std::string& name, int slant, int weight);
    void SetSourceColor(const sa::tl::Table& color);
    void SetSourceColor(double r, double g, double b, double a);
    sa::tl::Float GetLineWidth() const;
    void SetLineWidth(sa::tl::Float value);
    sa::tl::TablePtr GetCurrentPoint() const;
    sa::tl::TablePtr PathExtends() const;
    int GetAntialias() const;
    void SetAnialias(int value);
    int GetOperator() const;
    void SetOperator(int value);
    sa::tl::Float GetTolerance() const;
    void SetTolerance(sa::tl::Float value);
private:
    bool own_{ false };
    cairo_t* cr_{ nullptr };
    sa::tl::StrongObject source_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Cairo>(sa::tl::Context& ctx);
}
