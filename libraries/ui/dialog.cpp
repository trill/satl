/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "dialog.h"
#include "window.h"

namespace ui {

void Dialog::OnResponse(GtkDialog*, int id, Dialog* wnd)
{
    if (wnd->onResponse_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(wnd->ctx_,
                (*wnd->onResponse_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(id));
                }))
            return;
        (*wnd->onResponse_)(wnd->ctx_, *wnd->GetSelfPtr(), args);
    }
}

Dialog::Dialog(sa::tl::Context& ctx, std::string name, Window* parent, int buttons) :
    Widget(ctx, gtk_dialog_new_with_buttons(!name.empty() ? name.c_str() : nullptr,
                    !!parent ? GTK_WINDOW(parent->GetHandle()) : nullptr, FLAGS, nullptr, nullptr), name)
{
    if (buttons == GTK_BUTTONS_OK)
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_OK", GTK_RESPONSE_OK);
    else if (buttons == GTK_BUTTONS_CLOSE)
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_Close", GTK_RESPONSE_CLOSE);
    else if (buttons == GTK_BUTTONS_CANCEL)
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_Cancel", GTK_RESPONSE_CANCEL);
    else if (buttons == GTK_BUTTONS_YES_NO)
    {
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_Yes", GTK_RESPONSE_YES);
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_No", GTK_RESPONSE_NO);
    }
    else if (buttons == GTK_BUTTONS_OK_CANCEL)
    {
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_OK", GTK_RESPONSE_OK);
        gtk_dialog_add_button(GTK_DIALOG(GetHandle()), "_Cancel", GTK_RESPONSE_CANCEL);
    }
    gtk_window_set_type_hint(GTK_WINDOW(GetHandle()), GDK_WINDOW_TYPE_HINT_DIALOG);
    gtk_widget_set_margin_start(GetContainer(), DEF_MARGIN);
    gtk_widget_set_margin_top(GetContainer(), DEF_MARGIN);
    gtk_widget_set_margin_end(GetContainer(), DEF_MARGIN);
    gtk_widget_set_margin_bottom(GetContainer(), DEF_MARGIN);

    gtk_box_set_spacing(GTK_BOX(GetContainer()), DEF_MARGIN);
    g_signal_connect(GTK_WINDOW(GetHandle()), "response", G_CALLBACK(Dialog::OnResponse), this);
}

Dialog::~Dialog() = default;

int Dialog::Run()
{
    auto res = gtk_dialog_run(GTK_DIALOG(GetHandle()));
    gtk_window_close(GTK_WINDOW(GetHandle()));
    return res;
}

void Dialog::Resize(int width, int height)
{
    gtk_window_resize(GTK_WINDOW(GetHandle()), width, height);
}

void Dialog::Move(int x, int y)
{
    gtk_window_move(GTK_WINDOW(GetHandle()), x, y);
}

void Dialog::AddButton(const std::string& text, int response)
{
    gtk_dialog_add_button(GTK_DIALOG(GetHandle()), text.c_str(), response);
}

int Dialog::GetDefaultResponse() const
{
    return defaultResponse_;
}

void Dialog::SetDefaultResponse(int value)
{
    if (defaultResponse_ == value)
        return;
    defaultResponse_ = value;
    gtk_dialog_set_default_response(GTK_DIALOG(GetHandle()), defaultResponse_);
}

sa::tl::TablePtr Dialog::GetPosition() const
{
    int x = 0, y = 0;
    gtk_window_get_position(GTK_WINDOW(GetHandle()), &x, &y);
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add("x", x);
    result->Add("y", y);
    return result;
}

void Dialog::SetPosition(const sa::tl::Table& value)
{
    int x = 0, y = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "x", x))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "y", y))
        return;
    gtk_window_move(GTK_WINDOW(GetHandle()), x, y);
}

sa::tl::TablePtr Dialog::GetSize() const
{
    int x = 0, y = 0;
    gtk_window_get_size(GTK_WINDOW(GetHandle()), &x, &y);
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add("x", x);
    result->Add("y", y);
    return result;
}

void Dialog::SetSize(const sa::tl::Table& value)
{
    int x = 0, y = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "x", x))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "y", y))
        return;
    gtk_window_resize(GTK_WINDOW(GetHandle()), x, y);
}

void Dialog::Response(int id)
{
    gtk_dialog_response(GTK_DIALOG(GetHandle()), id);
}

int Dialog::GetWindowType() const
{
    return gtk_window_get_type_hint(GTK_WINDOW(GetHandle()));
}

void Dialog::SetWindowType(int value)
{
    gtk_window_set_type_hint(GTK_WINDOW(GetHandle()), (GdkWindowTypeHint)value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Dialog>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Dialog, ui::Widget>();
    result
        .AddFunction<ui::Dialog, int>("run", &ui::Dialog::Run)
        .AddFunction<ui::Dialog, void, int>("response", &ui::Dialog::Response)
        .AddFunction<ui::Dialog, void, int, int>("resize", &ui::Dialog::Resize)
        .AddFunction<ui::Dialog, void, int, int>("move", &ui::Dialog::Move)
        .AddFunction<ui::Dialog, void, const std::string&, int>("add_button", &ui::Dialog::AddButton)
        .AddProperty("position", &ui::Dialog::GetPosition, &ui::Dialog::SetPosition)
        .AddProperty("size", &ui::Dialog::GetSize, &ui::Dialog::SetSize)
        .AddProperty("window_type", &ui::Dialog::GetWindowType, &ui::Dialog::SetWindowType)
        .AddProperty("default_response", &ui::Dialog::GetDefaultResponse, &ui::Dialog::SetDefaultResponse)
        .AddProperty("on_response", &ui::Dialog::GetOnResponse, &ui::Dialog::SetOnResponse);
    return result;
}

}
