/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <context.h>

namespace ui {

sa::tl::Value Load(sa::tl::Context& ctx, sa::tl::TablePtr table);

}
