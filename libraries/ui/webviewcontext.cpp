/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifdef HAVE_WEBKIT

#include "widget.h"
#include "webviewcontext.h"
#include "webviewdatamanager.h"
#include "webviewcookiemanager.h"
#include <factory.h>

namespace ui {

void WebViewContext::OnDownloadStarted(WebKitWebContext*, WebKitDownload* dl, WebViewContext* data)
{
    g_signal_connect(dl, "notify::response", G_CALLBACK(WebViewContext::OnDownloadResponseReceived), data);
}

void WebViewContext::OnDownloadResponseReceived(WebKitDownload* dl, GParamSpec*, WebViewContext* data)
{
    std::string uri = webkit_uri_response_get_uri(webkit_download_get_response(dl));
    // When handled by the script cancel it
    if (data->DoOnDlownload(uri))
        webkit_download_cancel(dl);
}

WebViewContext::WebViewContext(sa::tl::Context& ctx, WebKitWebContext* handle) :
    Object(ctx),
    handle_(handle)
{
    g_signal_connect(handle_, "download-started", G_CALLBACK(WebViewContext::OnDownloadStarted), this);
}

WebViewContext::WebViewContext(sa::tl::Context& ctx) :
    WebViewContext(ctx, webkit_web_context_new())
{ }

WebViewContext::WebViewContext(sa::tl::Context& ctx, const WebViewDataManager& dataMngr) :
    WebViewContext(ctx, webkit_web_context_new_with_website_data_manager(dataMngr.GetHandle()))
{ }

sa::tl::Value WebViewContext::GetDataManager() const
{
    auto* meta = ctx_.GetMetatable<WebViewDataManager>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<WebViewDataManager>(ctx_, *meta, ctx_, webkit_web_context_get_website_data_manager(GetHandle()));
}

bool WebViewContext::DoOnDlownload(const std::string& uri)
{
    if (onDownload_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onDownload_),
                0,
                1,
                [&](int)
                {
                    args.push_back(sa::tl::MakeValue(uri));
                }))
            return false;
        return (*onDownload_)(ctx_, *GetSelfPtr(), args)->ToBool();
    }
    return false;
}

bool WebViewContext::IsAutomationAllowed() const
{
    return webkit_web_context_is_automation_allowed(handle_);
}

void WebViewContext::SetAutomationAllowed(bool value)
{
    webkit_web_context_set_automation_allowed(handle_, value);
}

int WebViewContext::GetCacheModel() const
{
    return webkit_web_context_get_cache_model(handle_);
}

void WebViewContext::SetCacheModel(int value)
{
    webkit_web_context_set_cache_model(handle_, (WebKitCacheModel)value);
}

void WebViewContext::ClearCache()
{
    webkit_web_context_clear_cache(handle_);
}

sa::tl::Value WebViewContext::GetCookieManager() const
{
    auto* meta = ctx_.GetMetatable<WebViewCookieManager>();
    SATL_ASSERT(meta);
    return sa::tl::bind::CreateInstance<WebViewCookieManager>(ctx_, *meta, ctx_, webkit_web_context_get_cookie_manager(GetHandle()));
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::WebViewContext>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::WebViewContext, sa::tl::bind::Object>();
    result
        .AddFunction<ui::WebViewContext, void>("clear_cache", &ui::WebViewContext::ClearCache)
        .AddProperty("automation_allowed", &ui::WebViewContext::IsAutomationAllowed, &ui::WebViewContext::SetAutomationAllowed)
        .AddProperty("data_manager", &ui::WebViewContext::GetDataManager)
        .AddProperty("cache_model", &ui::WebViewContext::GetCacheModel, &ui::WebViewContext::SetCacheModel)
        .AddProperty("cookie_manager", &ui::WebViewContext::GetCookieManager)
        .AddProperty("on_download", &ui::WebViewContext::GetOnDownload, &ui::WebViewContext::SetOnDownload);

    return result;
}

}

#endif
