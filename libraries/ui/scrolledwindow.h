/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

// https://docs.gtk.org/gtk3/class.ScrolledWindow.html
class ScrolledWindow : public Widget
{
    SATL_NON_COPYABLE(ScrolledWindow);
    SATL_NON_MOVEABLE(ScrolledWindow);
    TYPED_OBJECT(ScrolledWindow);
public:
    ScrolledWindow(sa::tl::Context& ctx, std::string name);
    ~ScrolledWindow() override;

    void Add(const sa::tl::Value&) override;
    void Add(Widget* child) override;

    [[nodiscard]] int GetMaxContentHeight() const;
    void SetMaxContentHeight(int value);
    [[nodiscard]] int GetMaxContentWidth() const;
    void SetMaxContentWidth(int value);

    [[nodiscard]] int GetMinContentHeight() const;
    void SetMinContentHeight(int value);
    [[nodiscard]] int GetMinContentWidth() const;
    void SetMinContentWidth(int value);

    [[nodiscard]] int GetHPolicy() const;
    void SetHPolicy(int value);
    [[nodiscard]] int GetVPolicy() const;
    void SetVPolicy(int value);

    [[nodiscard]] int GetPlacement() const;
    void SetPlacement(int value);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::ScrolledWindow>(sa::tl::Context& ctx);
}
