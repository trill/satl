/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "label.h"
#include <ast.h>

namespace ui {

Label::Label(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_label_new(name.c_str()), name)
{
    gtk_widget_set_halign(GetHandle(), GTK_ALIGN_START);
    gtk_widget_show(GetHandle());
}

Label::~Label() = default;

std::string Label::GetText() const
{
    return gtk_label_get_text(GTK_LABEL(GetHandle()));
}

void Label::SetText(const std::string& value)
{
    gtk_label_set_text(GTK_LABEL(GetHandle()), value.c_str());
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Label>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Label, ui::Widget>();
    result.AddProperty("text", &ui::Label::GetText, &ui::Label::SetText);
    return result;
}

}
