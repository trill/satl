/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Progressbar final : public Widget
{
    SATL_NON_COPYABLE(Progressbar);
    SATL_NON_MOVEABLE(Progressbar);
    TYPED_OBJECT(Progressbar);
public:
    Progressbar(sa::tl::Context& ctx, std::string name);
    ~Progressbar() override;

    std::string GetText() const;
    void SetText(const std::string& value);
    int GetValue() const;
    void SetValue(int value);
    bool GetShowText() const;
    void SetShowText(bool value);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Progressbar>(sa::tl::Context& ctx);
}
