/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_WEBKIT

#include <object.h>
#include <gc.h>
#include <webkit2/webkit2.h>

namespace ui {

class WebViewDataManager : public sa::tl::bind::Object
{
public:
    explicit WebViewDataManager(sa::tl::Context& ctx);
    WebViewDataManager(sa::tl::Context& ctx, WebKitWebsiteDataManager* handle);

    bool Create(const sa::tl::Table& options);
    bool CreateEphemeral();

    bool IsEphemeral() const;
    std::string GetBaseDataDirectory() const;
    std::string GetBaseCacheDirectory() const;

    sa::tl::Value GetCookieManager() const;

    bool IsItpEnabled() const;
    void SetItpEnabled(bool value);
    bool IsPersistentCredenticalStorageEanbled() const;
    void SetPersistentCredenticalStorageEanbled(bool value);

    int GetTlsErrorPolicy() const;
    void SetTlsErrorPolicy(int value);

    WebKitWebsiteDataManager* GetHandle() const { return handle_; }
private:
    WebKitWebsiteDataManager* handle_{ nullptr };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::WebViewDataManager>(sa::tl::Context& ctx);
}

#endif
