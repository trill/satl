/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "multiline_entry.h"
#include <ast.h>

namespace ui {

void MultilineEntry::OnChanged(GtkEditable*, MultilineEntry* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

MultilineEntry::MultilineEntry(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_scrolled_window_new(nullptr, nullptr), std::forward<std::string>(name)),
    textView_(gtk_text_view_new())
{
    gtk_container_add(GTK_CONTAINER(GetHandle()), textView_);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(GetHandle()), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(GetHandle()), GTK_SHADOW_IN);
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textView_), GTK_WRAP_WORD);
    gtk_text_view_set_left_margin(GTK_TEXT_VIEW(textView_), 6);
    gtk_text_view_set_top_margin(GTK_TEXT_VIEW(textView_), 6);
    gtk_text_view_set_right_margin(GTK_TEXT_VIEW(textView_), 6);
    gtk_text_view_set_bottom_margin(GTK_TEXT_VIEW(textView_), 6);
    changedHandler_ = g_signal_connect(
        gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), "changed", G_CALLBACK(MultilineEntry::OnChanged), this);
    gtk_widget_show(textView_);
    gtk_widget_show(GetHandle());
}

MultilineEntry::~MultilineEntry() = default;

std::string MultilineEntry::GetText() const
{
    GtkTextIter start, end;

    gtk_text_buffer_get_start_iter(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), &start);
    gtk_text_buffer_get_end_iter(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), &end);
    return gtk_text_buffer_get_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), &start, &end, TRUE);
}

void MultilineEntry::SetText(const std::string& value)
{
    g_signal_handler_block(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), changedHandler_);
    gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), value.c_str(), -1);
    g_signal_handler_unblock(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView_)), changedHandler_);
}

bool MultilineEntry::GetReadonly() const
{
    return !gtk_text_view_get_editable(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetReadonly(bool value)
{
    gtk_text_view_set_editable(GTK_TEXT_VIEW(textView_), !value);
}

bool MultilineEntry::GetMonospace() const
{
    return gtk_text_view_get_monospace(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetMonospace(bool value)
{
    gtk_text_view_set_monospace(GTK_TEXT_VIEW(textView_), value);
}

bool MultilineEntry::GetAcceptTabs() const
{
    return gtk_text_view_get_accepts_tab(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetAcceptTabs(bool value)
{
    gtk_text_view_set_accepts_tab(GTK_TEXT_VIEW(textView_), value);
}

bool MultilineEntry::GetOverwrite() const
{
    return gtk_text_view_get_overwrite(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetOverwrite(bool value)
{
    gtk_text_view_set_overwrite(GTK_TEXT_VIEW(textView_), value);
}

int MultilineEntry::GetInputPurpose() const
{
    return (int)gtk_text_view_get_input_purpose(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetInputPurpose(int value)
{
    gtk_text_view_set_input_purpose(GTK_TEXT_VIEW(textView_), (GtkInputPurpose)value);
}

int MultilineEntry::GetWrapMode() const
{
    return (int)gtk_text_view_get_wrap_mode(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetWrapMode(int value)
{
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textView_), (GtkWrapMode)value);
}

bool MultilineEntry::GetEditable() const
{
    return gtk_text_view_get_editable(GTK_TEXT_VIEW(textView_));
}

void MultilineEntry::SetEditable(bool value)
{
    gtk_text_view_set_editable(GTK_TEXT_VIEW(textView_), value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::MultilineEntry>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::MultilineEntry, ui::Widget>();
    result
        .AddProperty("text", &ui::MultilineEntry::GetText, &ui::MultilineEntry::SetText)
        .AddProperty("readonly", &ui::MultilineEntry::GetReadonly, &ui::MultilineEntry::SetReadonly)
        .AddProperty("accept_tabs", &ui::MultilineEntry::GetAcceptTabs, &ui::MultilineEntry::SetAcceptTabs)
        .AddProperty("overwrite", &ui::MultilineEntry::GetOverwrite, &ui::MultilineEntry::SetOverwrite)
        .AddProperty("input_purpose", &ui::MultilineEntry::GetInputPurpose, &ui::MultilineEntry::SetInputPurpose)
        .AddProperty("monospace", &ui::MultilineEntry::GetMonospace, &ui::MultilineEntry::SetMonospace)
        .AddProperty("wrap_mode", &ui::MultilineEntry::GetWrapMode, &ui::MultilineEntry::SetWrapMode)
        .AddProperty("editable", &ui::MultilineEntry::GetEditable, &ui::MultilineEntry::SetEditable)
        .AddProperty("on_changed", &ui::MultilineEntry::GetOnChanged, &ui::MultilineEntry::SetOnChanged);
    return result;
}

}
