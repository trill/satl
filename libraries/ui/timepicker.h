/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

class TimePicker final : public Widget
{
    SATL_NON_COPYABLE(TimePicker);
    SATL_NON_MOVEABLE(TimePicker);
    TYPED_OBJECT(TimePicker);
public:
    TimePicker(sa::tl::Context& ctx, std::string name);
    [[nodiscard]] sa::tl::TablePtr GetValue() const;
    void SetValue(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
private:
    static void OnValueChanged(GtkSpinButton*, TimePicker* data);
    static bool OnHourOutput(GtkSpinButton*, TimePicker* data);
    static bool OnMinuteOutput(GtkSpinButton*, TimePicker* data);
    sa::tl::CallablePtr onChanged_;
    gulong changedHandlerHours_{ 0 };
    gulong changedHandlerMinutes_{ 0 };
    GtkWidget* hours_{ nullptr };
    GtkWidget* minutes_{ nullptr };
};

} // namespace ui

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::TimePicker>(sa::tl::Context& ctx);
}
