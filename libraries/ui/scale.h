/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Scale : public Widget
{
    SATL_NON_COPYABLE(Scale);
    SATL_NON_MOVEABLE(Scale);
    TYPED_OBJECT(Scale);
public:
    ~Scale() override;

    [[nodiscard]] sa::tl::Float GetValue() const;
    void SetValue(sa::tl::Float value);
    [[nodiscard]] sa::tl::TablePtr GetRange() const;
    void SetRange(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
protected:
    Scale(sa::tl::Context& ctx, std::string name, bool horiz);
private:
    static void OnValueChanged(GtkScale*, Scale* data);
    sa::tl::CallablePtr onChanged_;
    gulong changedHandler_;
    gdouble min_{ 0.0 };
    gdouble max_{ 100.0 };
};

class HorizontalScale final : public Scale
{
public:
    HorizontalScale(sa::tl::Context& ctx, std::string name) :
        Scale(ctx, std::forward<std::string>(name), true)
    {}
};

class VerticalScale final : public Scale
{
public:
    VerticalScale(sa::tl::Context& ctx, std::string name) :
        Scale(ctx, std::forward<std::string>(name), true)
    {}
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Scale>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::HorizontalScale>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::VerticalScale>(sa::tl::Context& ctx);
}
