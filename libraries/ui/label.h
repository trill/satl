/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Label final : public Widget
{
    SATL_NON_COPYABLE(Label);
    SATL_NON_MOVEABLE(Label);
    TYPED_OBJECT(Label);
public:
    Label(sa::tl::Context& ctx, std::string name);
    ~Label() override;

    [[nodiscard]] std::string GetText() const;
    void SetText(const std::string& value);
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Label>(sa::tl::Context& ctx);
}
