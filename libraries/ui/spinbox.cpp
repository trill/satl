/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "spinbox.h"
#include <ast.h>

namespace ui {

void Spinbox::OnValueChanged(GtkSpinButton*, Spinbox* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

Spinbox::Spinbox(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_spin_button_new_with_range(0, 100, 1), std::forward<std::string>(name))
{
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(GetHandle()), 0);
    g_signal_connect(GetHandle(), "value-changed", G_CALLBACK(Spinbox::OnValueChanged), this);
    gtk_widget_show(GetHandle());
}

Spinbox::~Spinbox() = default;

int Spinbox::GetValue() const
{
    return (int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(GetHandle()));
}

void Spinbox::SetValue(int value)
{
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(GetHandle()), (gdouble)value);
}

sa::tl::TablePtr Spinbox::GetRange() const
{
    gdouble min = 0.0;
    gdouble max = 0.0;
    gtk_spin_button_get_range(GTK_SPIN_BUTTON(GetHandle()), &min, &max);
    auto result = sa::tl::MakeTable();
    result->Add<sa::tl::Float>("min", min);
    result->Add<sa::tl::Float>("max", max);
    return result;
}

void Spinbox::SetRange(const sa::tl::Table& value)
{
    int min = 0, max = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "min", min))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "max", max))
        return;
    gtk_spin_button_set_range(GTK_SPIN_BUTTON(GetHandle()), (gdouble)min, (gdouble)max);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Spinbox>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Spinbox, ui::Widget>();
    result
        .AddProperty("range", &ui::Spinbox::GetRange, &ui::Spinbox::SetRange)
        .AddProperty("value", &ui::Spinbox::GetValue, &ui::Spinbox::SetValue)
        .AddProperty("on_changed", &ui::Spinbox::GetOnChanged, &ui::Spinbox::SetOnChanged);
    return result;
}

}
