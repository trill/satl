/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "frame.h"
#include <ast.h>
#include "menu.h"

namespace ui {

Frame::Frame(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_frame_new(name.empty() ? nullptr : name.c_str()), name)
{
    SetMargins(DEF_MARGIN, DEF_MARGIN, DEF_MARGIN, DEF_MARGIN);
    gtk_widget_show(GetHandle());
}

Frame::~Frame() = default;

std::string Frame::GetLabel() const
{
    return gtk_frame_get_label(GTK_FRAME(GetHandle()));
}

void Frame::SetLabel(const std::string& value)
{
    gtk_frame_set_label(GTK_FRAME(GetHandle()), value.c_str());
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Frame>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Frame, ui::Widget>();
    result.AddProperty("label", &ui::Frame::GetLabel, &ui::Frame::SetLabel);
    return result;
}

}
