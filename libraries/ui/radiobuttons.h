/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Radiobuttons final : public Widget
{
    SATL_NON_COPYABLE(Radiobuttons);
    SATL_NON_MOVEABLE(Radiobuttons);
    TYPED_OBJECT(Radiobuttons);
public:
    Radiobuttons(sa::tl::Context& ctx, std::string name);
    ~Radiobuttons() override;

    void AddItem(std::string text);
    [[nodiscard]] int GetSelected() const;
    void SetSelected(int value);
    [[nodiscard]] sa::tl::Value GetValue() const;
    void SetValue(const sa::tl::Value& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onToggled_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onToggled_ = std::move(value); }
    sa::tl::TablePtr GetItems() const;
    void SetItems(const sa::tl::TablePtr& value);
private:
    static void OnToggled(GtkToggleButton* button, Radiobuttons* btn);
    sa::tl::CallablePtr onToggled_;
    std::vector<GtkWidget*> buttons_;
    std::vector<std::string> items_;
    bool updating_{ false };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Radiobuttons>(sa::tl::Context& ctx);
}
