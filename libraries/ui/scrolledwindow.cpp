/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "scrolledwindow.h"

namespace ui {

ScrolledWindow::ScrolledWindow(sa::tl::Context& ctx, std::string name) :
    Widget(ctx, gtk_scrolled_window_new(nullptr, nullptr), std::forward<std::string>(name))

{
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(GetHandle()), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(GetHandle()), GTK_SHADOW_IN);
    gtk_widget_show(GetHandle());
}

ScrolledWindow::~ScrolledWindow() = default;

void ScrolledWindow::Add(const sa::tl::Value& child)
{
    if (!children_.empty())
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a child");
        return;
    }
    Widget::Add(child);
}

void ScrolledWindow::Add(Widget* child)
{
    if (!children_.empty())
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a child");
        return;
    }
    Widget::Add(child);
}

int ScrolledWindow::GetMaxContentHeight() const
{
    return gtk_scrolled_window_get_max_content_height(GTK_SCROLLED_WINDOW(GetHandle()));
}

void ScrolledWindow::SetMaxContentHeight(int value)
{

    gtk_scrolled_window_set_max_content_height(GTK_SCROLLED_WINDOW(GetHandle()), value);
}

int ScrolledWindow::GetMaxContentWidth() const
{
    return gtk_scrolled_window_get_max_content_width(GTK_SCROLLED_WINDOW(GetHandle()));
}

void ScrolledWindow::SetMaxContentWidth(int value)
{
    gtk_scrolled_window_set_max_content_width(GTK_SCROLLED_WINDOW(GetHandle()), value);
}

int ScrolledWindow::GetMinContentHeight() const
{
    return gtk_scrolled_window_get_min_content_height(GTK_SCROLLED_WINDOW(GetHandle()));
}

void ScrolledWindow::SetMinContentHeight(int value)
{
    gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(GetHandle()), value);
}

int ScrolledWindow::GetMinContentWidth() const
{
    return gtk_scrolled_window_get_min_content_width(GTK_SCROLLED_WINDOW(GetHandle()));
}

void ScrolledWindow::SetMinContentWidth(int value)
{
    gtk_scrolled_window_set_min_content_width(GTK_SCROLLED_WINDOW(GetHandle()), value);
}

int ScrolledWindow::GetHPolicy() const
{
    GtkPolicyType h = GTK_POLICY_AUTOMATIC;
    GtkPolicyType v = GTK_POLICY_AUTOMATIC;
    gtk_scrolled_window_get_policy(GTK_SCROLLED_WINDOW(GetHandle()), &h, &v);
    return h;
}

void ScrolledWindow::SetHPolicy(int value)
{
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(GetHandle()), (GtkPolicyType)GetHPolicy(), (GtkPolicyType)value);
}

int ScrolledWindow::GetVPolicy() const
{
    GtkPolicyType h = GTK_POLICY_AUTOMATIC;
    GtkPolicyType v = GTK_POLICY_AUTOMATIC;
    gtk_scrolled_window_get_policy(GTK_SCROLLED_WINDOW(GetHandle()), &h, &v);
    return v;
}

void ScrolledWindow::SetVPolicy(int value)
{
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(GetHandle()), (GtkPolicyType)value, (GtkPolicyType)GetVPolicy());
}

int ScrolledWindow::GetPlacement() const
{
    return gtk_scrolled_window_get_placement(GTK_SCROLLED_WINDOW(GetHandle()));
}

void ScrolledWindow::SetPlacement(int value)
{
    gtk_scrolled_window_set_placement(GTK_SCROLLED_WINDOW(GetHandle()), (GtkCornerType)value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::ScrolledWindow>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::ScrolledWindow, ui::Widget>();
    result
        .AddProperty("h_policy", &ui::ScrolledWindow::GetHPolicy, &ui::ScrolledWindow::SetHPolicy)
        .AddProperty("v_policy", &ui::ScrolledWindow::GetVPolicy, &ui::ScrolledWindow::SetVPolicy)
        .AddProperty("placement", &ui::ScrolledWindow::GetPlacement, &ui::ScrolledWindow::SetPlacement)
        .AddProperty("max_content_height", &ui::ScrolledWindow::GetMaxContentHeight, &ui::ScrolledWindow::SetMaxContentHeight)
        .AddProperty("max_content_width", &ui::ScrolledWindow::GetMaxContentWidth, &ui::ScrolledWindow::SetMaxContentWidth)
        .AddProperty("min_content_height", &ui::ScrolledWindow::GetMinContentHeight, &ui::ScrolledWindow::SetMinContentHeight)
        .AddProperty("min_content_width", &ui::ScrolledWindow::GetMinContentWidth, &ui::ScrolledWindow::SetMinContentWidth);

    return result;
}

}
