/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "timer.h"
#include <ast.h>
#include <factory.h>

namespace ui {

gboolean Timer::OnTimer(void* data)
{
    Timer* timer = static_cast<Timer*>(data);
    return timer->DoOnTimer();
}

Timer::Timer(sa::tl::Context& ctx, int intervalMs) :
    Object(ctx),
    intervalMs_(intervalMs)
{ }

Timer::~Timer()
{
    Stop();
}

bool Timer::DoOnTimer()
{
    if (onTimer_)
        (*onTimer_)(ctx_, *GetSelfPtr(), {});

    if (single_)
        running_ = false;
    // Return true otherwise it will stop
    return !single_;
}

void Timer::Start()
{
    if (running_)
        return;

    if (intervalMs_ <= 0)
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "Interval must be greater than zero");
        return;
    }

    if (intervalMs_ >= 1000 && intervalMs_ % 1000 == 0)
        timer_ = g_timeout_add_seconds((unsigned)intervalMs_ / 1000, &Timer::OnTimer, this);
    else
        timer_ = g_timeout_add((unsigned)intervalMs_, &Timer::OnTimer, this);
    running_ = true;
}

void Timer::Stop()
{
    if (!running_)
        return;

    g_source_remove(timer_);
    timer_ = 0;
    running_ = false;
}

void Timer::SetSingle(bool value)
{
    if (running_)
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "Timer is already running");
        return;
    }
    single_ = value;
}

void Timeout(sa::tl::Context& ctx, int inervalMs, sa::tl::CallablePtr callback)
{
    auto* timer = ctx.GetGC().CreateObject<Timer>(ctx, inervalMs);
    timer->SetOnTimer(std::move(callback));
    timer->SetSingle(true);
    timer->Start();
}

sa::tl::Value Interval(sa::tl::Context& ctx, int inervalMs, sa::tl::CallablePtr callback)
{
    const auto* meta = ctx.GetMetatable<Timer>();
    SATL_ASSERT(meta);
    auto* timer = ctx.GetGC().CreateObject<Timer>(ctx, inervalMs);
    timer->SetOnTimer(std::move(callback));
    timer->Start();
    return sa::tl::MakeObject(*meta, timer);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Timer>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Timer, Object>();
    result
        .AddFunction<ui::Timer, void>("start", &ui::Timer::Start)
        .AddFunction<ui::Timer, void>("stop", &ui::Timer::Stop)
        .AddProperty("single", &ui::Timer::IsSingle, &ui::Timer::SetSingle)
        .AddProperty("running", &ui::Timer::IsRunning)
        .AddProperty("on_timer", &ui::Timer::GetOnTimer, &ui::Timer::SetOnTimer);
    return result;
}

}
