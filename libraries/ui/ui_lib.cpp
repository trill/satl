/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <metatable.h>
#include <module.h>
#include <value.h>
#include <ast.h>
#include "button.h"
#include "calendar.h"
#include "checkbox.h"
#include "combobox.h"
#include "dialog.h"
#include "dialogs.h"
#include "entry.h"
#include "frame.h"
#include "grid.h"
#include "label.h"
#include "menu.h"
#include "multiline_entry.h"
#include "progressbar.h"
#include "radiobuttons.h"
#include "radiobutton.h"
#include "scale.h"
#include "separator.h"
#include "spinbox.h"
#include "tab.h"
#include "window.h"
#include "list_box.h"
#include "timer.h"
#include "ui_load.h"
#include <factory.h>
#include "cairo.h"
#include "cairosurface.h"
#include "drawingarea.h"
#include "scrolledwindow.h"
#include "cairoregion.h"
#include "cairopattern.h"
#include "cairoplot.h"
#include "glarea.h"
#include "glcontext.h"
#include "statusbar.h"
#include "timepicker.h"
#include "datepicker.h"
#ifdef HAVE_WEBKIT
#include "webview.h"
#include "webviewcontext.h"
#include "webviewdatamanager.h"
#endif

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("ui"))
        return true;
    auto module = sa::tl::MakeModule();

    module->AddConst<int>("WIDGET_ALIGN_FILL", GTK_ALIGN_FILL);
    module->AddConst<int>("WIDGET_ALIGN_START", GTK_ALIGN_START);
    module->AddConst<int>("WIDGET_ALIGN_END", GTK_ALIGN_END);
    module->AddConst<int>("WIDGET_ALIGN_CENTER", GTK_ALIGN_CENTER);
    module->AddConst<int>("WIDGET_ALIGN_BASELINE", GTK_ALIGN_BASELINE);

    module->AddConst<int>("WINDOW_STATE_NORMAL", (int)ui::Window::WindowState::Normal);
    module->AddConst<int>("WINDOW_STATE_MINIMIZED", (int)ui::Window::WindowState::Minimized);
    module->AddConst<int>("WINDOW_STATE_MAXIMIZED", (int)ui::Window::WindowState::Maximized);
    module->AddConst<int>("WINDOW_STATE_FULLSCREEN", (int)ui::Window::WindowState::Fullscreen);

    // Keyboard modifiers
    module->AddConst<uint32_t>("MODIFIER_CTRL", ui::Widget::ModifierCtrl);
    module->AddConst<uint32_t>("MODIFIER_ALT", ui::Widget::ModifierAlt);
    module->AddConst<uint32_t>("MODIFIER_SHIFT", ui::Widget::ModifierShift);
    module->AddConst<uint32_t>("MODIFIER_SUPER", ui::Widget::ModifierSuper);

    // Mouse buttons
    module->AddConst<uint32_t>("MOUSE_BUTTON_LEFT", 1);
    module->AddConst<uint32_t>("MOUSE_BUTTON_MIDDLE", 2);
    module->AddConst<uint32_t>("MOUSE_BUTTON_RIGHT", 3);
    module->AddConst<uint32_t>("MOUSE_BUTTON_4", 4);
    module->AddConst<uint32_t>("MOUSE_BUTTON_5", 5);
    module->AddConst<uint32_t>("MOUSE_BUTTON_6", 6);
    module->AddConst<uint32_t>("MOUSE_BUTTON_7", 7);
    module->AddConst<uint32_t>("MOUSE_BUTTON_BACK", 8);
    module->AddConst<uint32_t>("MOUSE_BUTTON_FORWARD", 9);

    // Button event types
    module->AddConst<uint32_t>("BUTTON_EVENT_TYPE_PRESS", GDK_BUTTON_PRESS);
    module->AddConst<uint32_t>("BUTTON_EVENT_TYPE_DOUBLE_PRESS", GDK_DOUBLE_BUTTON_PRESS);
    module->AddConst<uint32_t>("BUTTON_EVENT_TYPE_TRIPLE_PRESS", GDK_TRIPLE_BUTTON_PRESS);
    module->AddConst<uint32_t>("BUTTON_EVENT_TYPE_RELEASE", GDK_BUTTON_RELEASE);

    // Key event types
    module->AddConst<uint32_t>("KEY_EVENT_TYPE_PRESS", GDK_KEY_PRESS);
    module->AddConst<uint32_t>("KEY_EVENT_TYPE_RELEASE", GDK_KEY_RELEASE);

    // Dialog types
    module->AddConst<int>("MESSAGE_INFO", GTK_MESSAGE_INFO);
    module->AddConst<int>("MESSAGE_WARNING", GTK_MESSAGE_WARNING);
    module->AddConst<int>("MESSAGE_QUESTION", GTK_MESSAGE_QUESTION);
    module->AddConst<int>("MESSAGE_ERROR", GTK_MESSAGE_ERROR);
    module->AddConst<int>("MESSAGE_OTHER", GTK_MESSAGE_OTHER);

    // Dialog buttons
    module->AddConst<int>("BUTTONS_NONE", GTK_BUTTONS_NONE);
    module->AddConst<int>("BUTTONS_OK", GTK_BUTTONS_OK);
    module->AddConst<int>("BUTTONS_CLOSE", GTK_BUTTONS_CLOSE);
    module->AddConst<int>("BUTTONS_CANCEL", GTK_BUTTONS_CANCEL);
    module->AddConst<int>("BUTTONS_YES_NO", GTK_BUTTONS_YES_NO);
    module->AddConst<int>("BUTTONS_OK_CANCEL", GTK_BUTTONS_OK_CANCEL);

    // Dialog response
    module->AddConst<int>("RESPONSE_NONE", GTK_RESPONSE_NONE);
    module->AddConst<int>("RESPONSE_REJECT", GTK_RESPONSE_REJECT);
    module->AddConst<int>("RESPONSE_ACCEPT", GTK_RESPONSE_ACCEPT);
    module->AddConst<int>("RESPONSE_DELETE_EVENT", GTK_RESPONSE_DELETE_EVENT);
    module->AddConst<int>("RESPONSE_OK", GTK_RESPONSE_OK);
    module->AddConst<int>("RESPONSE_CANCEL", GTK_RESPONSE_CANCEL);
    module->AddConst<int>("RESPONSE_CLOSE", GTK_RESPONSE_CLOSE);
    module->AddConst<int>("RESPONSE_YES", GTK_RESPONSE_YES);
    module->AddConst<int>("RESPONSE_NO", GTK_RESPONSE_NO);
    module->AddConst<int>("RESPONSE_APPLY", GTK_RESPONSE_APPLY);
    module->AddConst<int>("RESPONSE_HELP", GTK_RESPONSE_HELP);

    // Window types
    module->AddConst<int>("WINDOW_TYPE_NORMAL", GDK_WINDOW_TYPE_HINT_NORMAL);
    module->AddConst<int>("WINDOW_TYPE_DIALOG", GDK_WINDOW_TYPE_HINT_DIALOG);
    module->AddConst<int>("WINDOW_TYPE_MENU", GDK_WINDOW_TYPE_HINT_MENU);
    module->AddConst<int>("WINDOW_TYPE_TOOLBAR", GDK_WINDOW_TYPE_HINT_TOOLBAR);
    module->AddConst<int>("WINDOW_TYPE_SPLASHSCREEN", GDK_WINDOW_TYPE_HINT_SPLASHSCREEN);
    module->AddConst<int>("WINDOW_TYPE_UTILITY", GDK_WINDOW_TYPE_HINT_UTILITY);
    module->AddConst<int>("WINDOW_TYPE_DOCK", GDK_WINDOW_TYPE_HINT_DOCK);
    module->AddConst<int>("WINDOW_TYPE_DESKTOP", GDK_WINDOW_TYPE_HINT_DESKTOP);
    module->AddConst<int>("WINDOW_TYPE_DROPDOWN_MENU", GDK_WINDOW_TYPE_HINT_DROPDOWN_MENU);
    module->AddConst<int>("WINDOW_TYPE_POPUP_MENU", GDK_WINDOW_TYPE_HINT_POPUP_MENU);
    module->AddConst<int>("WINDOW_TYPE_TOOLTIP", GDK_WINDOW_TYPE_HINT_TOOLTIP);
    module->AddConst<int>("WINDOW_TYPE_NOTIFICATION", GDK_WINDOW_TYPE_HINT_NOTIFICATION);
    module->AddConst<int>("WINDOW_TYPE_COMBO", GDK_WINDOW_TYPE_HINT_COMBO);
    module->AddConst<int>("WINDOW_TYPE_DND", GDK_WINDOW_TYPE_HINT_DND);

    // Edit input purpose
    module->AddConst<int>("INPUT_PURPOSE_FREE_FORM", GTK_INPUT_PURPOSE_FREE_FORM);
    module->AddConst<int>("INPUT_PURPOSE_ALPHA", GTK_INPUT_PURPOSE_ALPHA);
    module->AddConst<int>("INPUT_PURPOSE_DIGITS", GTK_INPUT_PURPOSE_DIGITS);
    module->AddConst<int>("INPUT_PURPOSE_NUMBER", GTK_INPUT_PURPOSE_NUMBER);
    module->AddConst<int>("INPUT_PURPOSE_PHONE", GTK_INPUT_PURPOSE_PHONE);
    module->AddConst<int>("INPUT_PURPOSE_URL", GTK_INPUT_PURPOSE_URL);
    module->AddConst<int>("INPUT_PURPOSE_EMAIL", GTK_INPUT_PURPOSE_EMAIL);
    module->AddConst<int>("INPUT_PURPOSE_NAME", GTK_INPUT_PURPOSE_NAME);
    module->AddConst<int>("INPUT_PURPOSE_PASSWORD", GTK_INPUT_PURPOSE_PASSWORD);
    module->AddConst<int>("INPUT_PURPOSE_PIN", GTK_INPUT_PURPOSE_PIN);
    module->AddConst<int>("INPUT_PURPOSE_TERMINAL", GTK_INPUT_PURPOSE_TERMINAL);

    // MultilineEntry wrap mode
    module->AddConst<int>("WRAP_NONE", GTK_WRAP_NONE);
    module->AddConst<int>("WRAP_CHAR", GTK_WRAP_CHAR);
    module->AddConst<int>("WRAP_WORD", GTK_WRAP_WORD);
    module->AddConst<int>("WRAP_WORD_CHAR", GTK_WRAP_WORD_CHAR);

    // Scrollbar policy https://docs.gtk.org/gtk3/enum.PolicyType.html
    module->AddConst<int>("POLICY_ALWAYS", GTK_POLICY_ALWAYS);
    module->AddConst<int>("POLICY_AUTOMATIC", GTK_POLICY_AUTOMATIC);
    module->AddConst<int>("POLICY_NEVER", GTK_POLICY_NEVER);
    module->AddConst<int>("POLICY_EXTERNAL", GTK_POLICY_EXTERNAL);
    // https://docs.gtk.org/gtk3/enum.CornerType.html
    module->AddConst<int>("CORNER_TOP_LEFT", GTK_CORNER_TOP_LEFT);
    module->AddConst<int>("CORNER_BOTTOM_LEFT", GTK_CORNER_BOTTOM_LEFT);
    module->AddConst<int>("CORNER_TOP_RIGHT", GTK_CORNER_TOP_RIGHT);
    module->AddConst<int>("CORNER_BOTTOM_RIGHT", GTK_CORNER_BOTTOM_RIGHT);

    // Calendar
    module->AddConst<int>("CALENDAR_SHOW_HEADING", GTK_CALENDAR_SHOW_HEADING);
    module->AddConst<int>("CALENDAR_SHOW_DAY_NAMES", GTK_CALENDAR_SHOW_DAY_NAMES);
    module->AddConst<int>("CALENDAR_NO_MONTH_CHANGE", GTK_CALENDAR_NO_MONTH_CHANGE);
    module->AddConst<int>("CALENDAR_SHOW_WEEK_NUMBERS", GTK_CALENDAR_SHOW_WEEK_NUMBERS);
    module->AddConst<int>("CALENDAR_SHOW_DETAILS", GTK_CALENDAR_SHOW_DETAILS);

    // Cairo
    module->AddConst<int>("CAIRO_ANTIALIAS_DEFAULT", CAIRO_ANTIALIAS_DEFAULT);
    module->AddConst<int>("CAIRO_ANTIALIAS_NONE", CAIRO_ANTIALIAS_NONE);
    module->AddConst<int>("CAIRO_ANTIALIAS_GRAY", CAIRO_ANTIALIAS_GRAY);
    module->AddConst<int>("CAIRO_ANTIALIAS_SUBPIXEL", CAIRO_ANTIALIAS_SUBPIXEL);
    module->AddConst<int>("CAIRO_ANTIALIAS_FAST", CAIRO_ANTIALIAS_FAST);
    module->AddConst<int>("CAIRO_ANTIALIAS_GOOD", CAIRO_ANTIALIAS_GOOD);
    module->AddConst<int>("CAIRO_ANTIALIAS_BEST", CAIRO_ANTIALIAS_BEST);

    module->AddConst<int>("CAIRO_FONT_SLANT_NORMAL", CAIRO_FONT_SLANT_NORMAL);
    module->AddConst<int>("CAIRO_FONT_SLANT_ITALIC", CAIRO_FONT_SLANT_ITALIC);
    module->AddConst<int>("CAIRO_FONT_SLANT_OBLIQUE", CAIRO_FONT_SLANT_OBLIQUE);

    module->AddConst<int>("CAIRO_FONT_WEIGHT_NORMAL", CAIRO_FONT_WEIGHT_NORMAL);
    module->AddConst<int>("CAIRO_FONT_WEIGHT_BOLD", CAIRO_FONT_WEIGHT_BOLD);

    module->AddConst<std::string>("CAIRO_TAG_DEST", CAIRO_TAG_DEST);
    module->AddConst<std::string>("CAIRO_TAG_LINK", CAIRO_TAG_LINK);

    module->AddConst<int>("CAIRO_FORMAT_ARGB32", CAIRO_FORMAT_ARGB32);
    module->AddConst<int>("CAIRO_FORMAT_RGB24", CAIRO_FORMAT_RGB24);
    module->AddConst<int>("CAIRO_FORMAT_A8", CAIRO_FORMAT_A8);
    module->AddConst<int>("CAIRO_FORMAT_A1", CAIRO_FORMAT_A1);
    module->AddConst<int>("CAIRO_FORMAT_RGB16_565", CAIRO_FORMAT_RGB16_565);
    module->AddConst<int>("CAIRO_FORMAT_RGB30", CAIRO_FORMAT_RGB30);
    module->AddConst<int>("CAIRO_FORMAT_RGB96F", CAIRO_FORMAT_RGB96F);
    module->AddConst<int>("CAIRO_FORMAT_RGBA128F", CAIRO_FORMAT_RGBA128F);

    module->AddConst<int>("CAIRO_OPERATOR_CLEAR", CAIRO_OPERATOR_CLEAR);
    module->AddConst<int>("CAIRO_OPERATOR_SOURCE", CAIRO_OPERATOR_SOURCE);
    module->AddConst<int>("CAIRO_OPERATOR_OVER", CAIRO_OPERATOR_OVER);
    module->AddConst<int>("CAIRO_OPERATOR_IN", CAIRO_OPERATOR_IN);
    module->AddConst<int>("CAIRO_OPERATOR_OUT", CAIRO_OPERATOR_OUT);
    module->AddConst<int>("CAIRO_OPERATOR_ATOP", CAIRO_OPERATOR_ATOP);
    module->AddConst<int>("CAIRO_OPERATOR_DEST", CAIRO_OPERATOR_DEST);
    module->AddConst<int>("CAIRO_OPERATOR_DEST_OVER", CAIRO_OPERATOR_DEST_OVER);
    module->AddConst<int>("CAIRO_OPERATOR_DEST_IN", CAIRO_OPERATOR_DEST_IN);
    module->AddConst<int>("CAIRO_OPERATOR_DEST_OUT", CAIRO_OPERATOR_DEST_OUT);
    module->AddConst<int>("CAIRO_OPERATOR_DEST_ATOP", CAIRO_OPERATOR_DEST_ATOP);
    module->AddConst<int>("CAIRO_OPERATOR_XOR", CAIRO_OPERATOR_XOR);
    module->AddConst<int>("CAIRO_OPERATOR_ADD", CAIRO_OPERATOR_ADD);
    module->AddConst<int>("CAIRO_OPERATOR_SATURATE", CAIRO_OPERATOR_SATURATE);
    module->AddConst<int>("CAIRO_OPERATOR_MULTIPLY", CAIRO_OPERATOR_MULTIPLY);
    module->AddConst<int>("CAIRO_OPERATOR_SCREEN", CAIRO_OPERATOR_SCREEN);
    module->AddConst<int>("CAIRO_OPERATOR_OVERLAY", CAIRO_OPERATOR_OVERLAY);
    module->AddConst<int>("CAIRO_OPERATOR_DARKEN", CAIRO_OPERATOR_DARKEN);
    module->AddConst<int>("CAIRO_OPERATOR_LIGHTEN", CAIRO_OPERATOR_LIGHTEN);
    module->AddConst<int>("CAIRO_OPERATOR_COLOR_DODGE", CAIRO_OPERATOR_COLOR_DODGE);
    module->AddConst<int>("CAIRO_OPERATOR_COLOR_BURN", CAIRO_OPERATOR_COLOR_BURN);
    module->AddConst<int>("CAIRO_OPERATOR_HARD_LIGHT", CAIRO_OPERATOR_HARD_LIGHT);
    module->AddConst<int>("CAIRO_OPERATOR_SOFT_LIGHT", CAIRO_OPERATOR_SOFT_LIGHT);
    module->AddConst<int>("CAIRO_OPERATOR_DIFFERENCE", CAIRO_OPERATOR_DIFFERENCE);
    module->AddConst<int>("CAIRO_OPERATOR_EXCLUSION", CAIRO_OPERATOR_EXCLUSION);
    module->AddConst<int>("CAIRO_OPERATOR_HSL_HUE", CAIRO_OPERATOR_HSL_HUE);
    module->AddConst<int>("CAIRO_OPERATOR_HSL_SATURATION", CAIRO_OPERATOR_HSL_SATURATION);
    module->AddConst<int>("CAIRO_OPERATOR_HSL_COLOR", CAIRO_OPERATOR_HSL_COLOR);
    module->AddConst<int>("CAIRO_OPERATOR_HSL_LUMINOSITY", CAIRO_OPERATOR_HSL_LUMINOSITY);

    module->AddConst<int>("CAIRO_CONTENT_COLOR", CAIRO_CONTENT_COLOR);
    module->AddConst<int>("CAIRO_CONTENT_ALPHA", CAIRO_CONTENT_ALPHA);
    module->AddConst<int>("CAIRO_CONTENT_COLOR_ALPHA", CAIRO_CONTENT_COLOR_ALPHA);

    module->AddConst<int>("CAIRO_FILTER_FAST", CAIRO_FILTER_FAST);
    module->AddConst<int>("CAIRO_FILTER_GOOD", CAIRO_FILTER_GOOD);
    module->AddConst<int>("CAIRO_FILTER_BEST", CAIRO_FILTER_BEST);
    module->AddConst<int>("CAIRO_FILTER_NEAREST", CAIRO_FILTER_NEAREST);
    module->AddConst<int>("CAIRO_FILTER_BILINEAR", CAIRO_FILTER_BILINEAR);
    module->AddConst<int>("CAIRO_FILTER_GAUSSIAN", CAIRO_FILTER_GAUSSIAN);
#ifdef HAVE_WEBKIT
    module->AddConst<int>("WEBKIT_LOAD_STARTED", WEBKIT_LOAD_STARTED);
    module->AddConst<int>("WEBKIT_LOAD_REDIRECTED", WEBKIT_LOAD_REDIRECTED);
    module->AddConst<int>("WEBKIT_LOAD_COMMITTED", WEBKIT_LOAD_COMMITTED);
    module->AddConst<int>("WEBKIT_LOAD_FINISHED", WEBKIT_LOAD_FINISHED);

    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_DOCUMENT", WEBKIT_HIT_TEST_RESULT_CONTEXT_DOCUMENT);
    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK", WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK);
    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE", WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE);
    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA", WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA);
    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE", WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE);
    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_SCROLLBAR", WEBKIT_HIT_TEST_RESULT_CONTEXT_SCROLLBAR);
    module->AddConst<int>("WEBKIT_HIT_TEST_RESULT_CONTEXT_SELECTION", WEBKIT_HIT_TEST_RESULT_CONTEXT_SELECTION);
#endif

    [[maybe_unused]] auto& objectMeta = sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    auto& cairoMeta = sa::tl::bind::Register<ui::Cairo>(ctx);
    auto& cairoSurfaceMeta = sa::tl::bind::Register<ui::CairoSurface>(ctx);
    auto& cairoRegionMeta = sa::tl::bind::Register<ui::CairoRegion>(ctx);
    auto& cairoPatternMeta = sa::tl::bind::Register<ui::CairoPattern>(ctx);
    [[maybe_unused]] auto& cairoPlotMeta = sa::tl::bind::Register<ui::CairoPlot>(ctx);
    auto& cairoBarPlotMeta = sa::tl::bind::Register<ui::CairoBarPlot>(ctx);
    auto& cairoScatterPlotMeta = sa::tl::bind::Register<ui::CairoScatterPlot>(ctx);
    auto& cairoLinePlotMeta = sa::tl::bind::Register<ui::CairoLinePlot>(ctx);

    [[maybe_unused]] auto& widgetMeta = sa::tl::bind::Register<ui::Widget>(ctx);
    auto& scrollMeta = sa::tl::bind::Register<ui::ScrolledWindow>(ctx);
    auto& areaMeta = sa::tl::bind::Register<ui::DrawingArea>(ctx);
    auto& glAreaMeta = sa::tl::bind::Register<ui::GLArea>(ctx);
    [[maybe_unused]] auto& glContextMeta = sa::tl::bind::Register<ui::GLContext>(ctx);
    auto& hsepMeta = sa::tl::bind::Register<ui::HorizontalSeparator>(ctx);
    auto& vsepMeta = sa::tl::bind::Register<ui::VerticalSeparator>(ctx);
    auto& lblMeta = sa::tl::bind::Register<ui::Label>(ctx);
    auto& wndMeta = sa::tl::bind::Register<ui::Window>(ctx);
    auto& frameMeta = sa::tl::bind::Register<ui::Frame>(ctx);
    auto& btnMeta = sa::tl::bind::Register<ui::Button>(ctx);
    auto& colorBtnMeta = sa::tl::bind::Register<ui::ColorButton>(ctx);
    auto& fontBtnMeta = sa::tl::bind::Register<ui::FontButton>(ctx);
    auto& chkMeta = sa::tl::bind::Register<ui::Checkbox>(ctx);
    auto& radiosMeta = sa::tl::bind::Register<ui::Radiobuttons>(ctx);
    auto& radioMeta = sa::tl::bind::Register<ui::Radiobutton>(ctx);
    auto& entryMeta = sa::tl::bind::Register<ui::Entry>(ctx);
    auto& searchEntryMeta = sa::tl::bind::Register<ui::SearchEntry>(ctx);
    auto& multiEntryMeta = sa::tl::bind::Register<ui::MultilineEntry>(ctx);
    auto& listBoxMeta = sa::tl::bind::Register<ui::ListBox>(ctx);
    auto& pgrMeta = sa::tl::bind::Register<ui::Progressbar>(ctx);
    auto& spnMeta = sa::tl::bind::Register<ui::Spinbox>(ctx);
    auto& timepickerMeta = sa::tl::bind::Register<ui::TimePicker>(ctx);
    auto& datepickerMeta = sa::tl::bind::Register<ui::DatePicker>(ctx);
    [[maybe_unused]] auto& scaleMeta = sa::tl::bind::Register<ui::Scale>(ctx);
    auto& hscaleMeta = sa::tl::bind::Register<ui::HorizontalScale>(ctx);
    auto& vscaleMeta = sa::tl::bind::Register<ui::VerticalScale>(ctx);
    auto& gridMeta = sa::tl::bind::Register<ui::Grid>(ctx);
    auto& tabMeta = sa::tl::bind::Register<ui::Tab>(ctx);
    auto& cboMeta = sa::tl::bind::Register<ui::Combobox>(ctx);
    auto& cboTextMeta = sa::tl::bind::Register<ui::ComboboxText>(ctx);
    auto& statusbarMeta = sa::tl::bind::Register<ui::Statusbar>(ctx);
    auto& menuBarMeta = sa::tl::bind::Register<ui::MenuBar>(ctx);
    auto& menuMeta = sa::tl::bind::Register<ui::Menu>(ctx);
    auto& menuItemMeta = sa::tl::bind::Register<ui::MenuItem>(ctx);
    auto& menuSepItemMeta = sa::tl::bind::Register<ui::MenuSeparatorItem>(ctx);
    auto& menuCheckItemMeta = sa::tl::bind::Register<ui::MenuCheckItem>(ctx);
    auto& calendarMeta = sa::tl::bind::Register<ui::Calendar>(ctx);
    auto& dialogMeta = sa::tl::bind::Register<ui::Dialog>(ctx);
    auto& timerMeta = sa::tl::bind::Register<ui::Timer>(ctx);
#ifdef HAVE_WEBKIT
    auto& webviewMeta = sa::tl::bind::Register<ui::WebView>(ctx);
    auto& webviewDataMeta = sa::tl::bind::Register<ui::WebViewDataManager>(ctx);
    auto& webviewContextMeta = sa::tl::bind::Register<ui::WebViewContext>(ctx);
#endif

    module->AddFunction<sa::tl::Value, sa::tl::TablePtr>("load",
        [&ctx](sa::tl::TablePtr table) -> sa::tl::Value
        { return ui::Load(ctx, std::move(table)); });
    module->AddFunction<sa::tl::Value, int, int, int>("cairo_image_surface",
        [&ctx, &cairoSurfaceMeta](int format, int width, int height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreateImageSurface((cairo_format_t)format, width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, std::string, sa::tl::Float, sa::tl::Float>("cairo_pdf_surface",
        [&ctx, &cairoSurfaceMeta](std::string filename, sa::tl::Float width, sa::tl::Float height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreatePDFSurface(std::move(filename), width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, std::string, sa::tl::Float, sa::tl::Float>("cairo_ps_surface",
        [&ctx, &cairoSurfaceMeta](std::string filename, sa::tl::Float width, sa::tl::Float height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreatePSSurface(std::move(filename), width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, std::string, sa::tl::Float, sa::tl::Float>("cairo_svg_surface",
        [&ctx, &cairoSurfaceMeta](std::string filename, sa::tl::Float width, sa::tl::Float height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreateSVGSurface(std::move(filename), width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, std::string>("cairo_surface_from_png",
        [&ctx, &cairoSurfaceMeta](std::string filename) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreateFromPNG(std::move(filename)))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, const sa::tl::ObjectPtr&, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("cairo_rectangle_surface",
        [&ctx, &cairoSurfaceMeta](const sa::tl::ObjectPtr& target, sa::tl::Float x, sa::tl::Float y, sa::tl::Float width, sa::tl::Float height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreateForRectangle(target, x, y, width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, const sa::tl::ObjectPtr&, int, sa::tl::Float, sa::tl::Float>("cairo_similar_surface",
        [&ctx, &cairoSurfaceMeta](const sa::tl::ObjectPtr& target, int content, sa::tl::Float width, sa::tl::Float height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreateSimilar(target, content, width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, const sa::tl::ObjectPtr&, int, sa::tl::Float, sa::tl::Float>("cairo_similar_image_surface",
        [&ctx, &cairoSurfaceMeta](const sa::tl::ObjectPtr& target, int format, sa::tl::Float width, sa::tl::Float height) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoSurface>(ctx, cairoSurfaceMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* surface = static_cast<ui::CairoSurface*>(s->GetInstance());
            if (!surface->CreateSimilarImage(target, format, width, height))
            {
                ctx.GetGC().Remove(surface);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value>("cairo_bar_plot",
        [&ctx, &cairoBarPlotMeta]() -> sa::tl::Value
        {
            return sa::tl::bind::CreateInstance<ui::CairoBarPlot>(ctx, cairoBarPlotMeta, ctx);
        });
    module->AddFunction<sa::tl::Value>("cairo_scatter_plot",
        [&ctx, &cairoScatterPlotMeta]() -> sa::tl::Value
        {
            return sa::tl::bind::CreateInstance<ui::CairoScatterPlot>(ctx, cairoScatterPlotMeta, ctx);
        });
    module->AddFunction<sa::tl::Value>("cairo_line_plot",
        [&ctx, &cairoLinePlotMeta]() -> sa::tl::Value
        {
            return sa::tl::bind::CreateInstance<ui::CairoLinePlot>(ctx, cairoLinePlotMeta, ctx);
        });
    module->AddFunction<sa::tl::Value>("cairo_pattern_mesh",
        [&ctx, &cairoPatternMeta]() -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoPattern>(ctx, cairoPatternMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* pattern = static_cast<ui::CairoPattern*>(s->GetInstance());
            pattern->CreateMesh();
            return res;
        });
    module->AddFunction<sa::tl::Value, const sa::tl::Table&>("cairo_pattern_color",
        [&ctx, &cairoPatternMeta](const sa::tl::Table& color) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoPattern>(ctx, cairoPatternMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* pattern = static_cast<ui::CairoPattern*>(s->GetInstance());
            if (!pattern->CreateColor(color))
            {
                ctx.GetGC().Remove(pattern)                ;
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, const sa::tl::ObjectPtr&>("cairo_pattern_surface",
        [&ctx, &cairoPatternMeta](const sa::tl::ObjectPtr& surface) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoPattern>(ctx, cairoPatternMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* pattern = static_cast<ui::CairoPattern*>(s->GetInstance());
            if (!pattern->CreateSurface(surface))
            {
                ctx.GetGC().Remove(pattern)                ;
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("cairo_pattern_linear",
        [&ctx, &cairoPatternMeta](sa::tl::Float x0, sa::tl::Float y0, sa::tl::Float x1, sa::tl::Float y1) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoPattern>(ctx, cairoPatternMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* pattern = static_cast<ui::CairoPattern*>(s->GetInstance());
            if (!pattern->CreateLinear(x0, y0, x1, y1))
            {
                ctx.GetGC().Remove(pattern)                ;
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float, sa::tl::Float>("cairo_pattern_radial",
        [&ctx, &cairoPatternMeta](sa::tl::Float cx0, sa::tl::Float cy0, sa::tl::Float r0,
            sa::tl::Float cx1, sa::tl::Float cy1, sa::tl::Float r1) -> sa::tl::Value
        {
            auto res = sa::tl::bind::CreateInstance<ui::CairoPattern>(ctx, cairoPatternMeta, ctx);
            auto& s = res.As<sa::tl::ObjectPtr>();
            auto* pattern = static_cast<ui::CairoPattern*>(s->GetInstance());
            if (!pattern->CreateRadial(cx0, cy0, r0, cx1, cy1, r1))
            {
                ctx.GetGC().Remove(pattern);
                return {};
            }
            return res;
        });
    module->AddFunction<sa::tl::Value, sa::tl::ObjectPtr>("cairo",
        [&ctx, &cairoMeta](sa::tl::ObjectPtr surface) -> sa::tl::Value // NOLINT(performance-unnecessary-value-param)
        {
            if (auto* surfPtr = sa::tl::MetaTable::GetObject<ui::CairoSurface>(surface))
                return sa::tl::bind::CreateInstance<ui::Cairo>(ctx, cairoMeta, ctx, *surfPtr);
            ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Surface must be a CairoSurface object");
            return {};
        });
    module->AddFunction<sa::tl::Value>("cairo_region",
        [&ctx, &cairoRegionMeta]() -> sa::tl::Value
        {
            return sa::tl::bind::CreateInstance<ui::CairoRegion>(ctx, cairoRegionMeta, ctx);
        });
    module->AddFunction<sa::tl::Value, int, int, int, int>("cairo_region_rectangle",
        [&ctx, &cairoRegionMeta](int x, int y, int width, int height) -> sa::tl::Value
        {
            return sa::tl::bind::CreateInstance<ui::CairoRegion>(ctx, cairoRegionMeta, ctx, x, y, width, height);
        });
    module->AddFunction<sa::tl::Value, int>("timer",
        [&ctx, &timerMeta](int millis) -> sa::tl::Value
        {
            if (millis <= 0)
            {
                ctx.RuntimeError(sa::tl::Error::InvalidArgument, "Interval must be greater than zero");
                return {};
            }
            return sa::tl::bind::CreateInstance<ui::Timer>(ctx, timerMeta, ctx, millis);
        });
    module->AddFunction<void, int, sa::tl::CallablePtr>(
        "timeout", [&ctx](int millis, sa::tl::CallablePtr callback)
        {
            if (millis <= 0)
            {
                ctx.RuntimeError(sa::tl::Error::InvalidArgument, "Interval must be greater than zero");
                return;
            }
            ui::Timeout(ctx, millis, std::move(callback));
        });
    module->AddFunction<void, sa::tl::CallablePtr>(
        "deferred_invoke", [&ctx](sa::tl::CallablePtr callback)
        {
            ui::Timeout(ctx, 1, std::move(callback));
        });
    module->AddFunction<sa::tl::Value, int, sa::tl::CallablePtr>(
        "interval", [&ctx](int millis, sa::tl::CallablePtr callback) -> sa::tl::Value
        {
            if (millis <= 0)
            {
                ctx.RuntimeError(sa::tl::Error::InvalidArgument, "Interval must be greater than zero");
                return {};
            }
            return ui::Interval(ctx, millis, std::move(callback));
        });

    module->AddFunction<sa::tl::Value, std::string>("horizontal_separator",
        [&ctx, &hsepMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::HorizontalSeparator>(ctx, hsepMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("vertical_separator",
        [&ctx, &vsepMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::VerticalSeparator>(ctx, vsepMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("label",
        [&ctx, &lblMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Label>(ctx, lblMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("window",
        [&ctx, &wndMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Window>(ctx, wndMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("scrolled_window",
        [&ctx, &scrollMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::ScrolledWindow>(ctx, scrollMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("drawing_area",
        [&ctx, &areaMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::DrawingArea>(ctx, areaMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("gl_area",
        [&ctx, &glAreaMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::GLArea>(ctx, glAreaMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("frame",
        [&ctx, &frameMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Frame>(ctx, frameMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("button",
        [&ctx, &btnMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Button>(ctx, btnMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("color_button",
        [&ctx, &colorBtnMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::ColorButton>(ctx, colorBtnMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("font_button",
        [&ctx, &fontBtnMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::FontButton>(ctx, fontBtnMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("checkbox",
        [&ctx, &chkMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Checkbox>(ctx, chkMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("radiobuttons",
        [&ctx, &radiosMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Radiobuttons>(ctx, radiosMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("radiobutton",
        [&ctx, &radioMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Radiobutton>(ctx, radioMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string, sa::tl::ObjectPtr>("radiobutton_member",
        [&ctx, &radioMeta](std::string name, sa::tl::ObjectPtr member) -> sa::tl::Value // NOLINT(performance-unnecessary-value-param)
        {
            auto* pmem = radioMeta.CastTo<ui::Radiobutton>(member.get());
            if (!pmem)
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Rediobutton expected");
                return {};
            }
            return sa::tl::bind::CreateInstance<ui::Radiobutton>(ctx, radioMeta, ctx, std::move(name), pmem);
        });
    module->AddFunction<sa::tl::Value, std::string>("entry",
        [&ctx, &entryMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Entry>(ctx, entryMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("multiline_entry",
        [&ctx, &multiEntryMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::MultilineEntry>(ctx, multiEntryMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("search_entry",
        [&ctx, &searchEntryMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::SearchEntry>(ctx, searchEntryMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("progressbar",
        [&ctx, &pgrMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Progressbar>(ctx, pgrMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("spinbox",
        [&ctx, &spnMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Spinbox>(ctx, spnMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("timepicker",
        [&ctx, &timepickerMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::TimePicker>(ctx, timepickerMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("datepicker",
        [&ctx, &datepickerMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::DatePicker>(ctx, datepickerMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("grid",
        [&ctx, &gridMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Grid>(ctx, gridMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("tab",
        [&ctx, &tabMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Tab>(ctx, tabMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("combobox",
        [&ctx, &cboMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Combobox>(ctx, cboMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("combobox_text",
        [&ctx, &cboTextMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::ComboboxText>(ctx, cboTextMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("statusbar",
        [&ctx, &statusbarMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Statusbar>(ctx, statusbarMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("menu_bar",
        [&ctx, &menuBarMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::MenuBar>(ctx, menuBarMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("menu",
        [&ctx, &menuMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Menu>(ctx, menuMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("menu_item",
        [&ctx, &menuItemMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::MenuItem>(ctx, menuItemMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("menu_separator_item",
        [&ctx, &menuSepItemMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::MenuSeparatorItem>(ctx, menuSepItemMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("menu_check_item",
        [&ctx, &menuCheckItemMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::MenuCheckItem>(ctx, menuCheckItemMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("list_box",
        [&ctx, &listBoxMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::ListBox>(ctx, listBoxMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("horizontal_scale",
        [&ctx, &hscaleMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::HorizontalScale>(ctx, hscaleMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("vertical_scale",
        [&ctx, &vscaleMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::VerticalScale>(ctx, vscaleMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string>("calendar",
        [&ctx, &calendarMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::Calendar>(ctx, calendarMeta, ctx, std::move(name)); });
#ifdef HAVE_WEBKIT
    module->AddFunction<sa::tl::Value, std::string>("webview",
        [&ctx, &webviewMeta](std::string name) -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::WebView>(ctx, webviewMeta, ctx, std::move(name)); });
    module->AddFunction<sa::tl::Value, std::string, const sa::tl::ObjectPtr&>("webview_related",
        [&ctx, &webviewMeta](std::string name, const sa::tl::ObjectPtr& rv) -> sa::tl::Value
        {
            if (auto* prv = webviewMeta.CastTo<ui::WebView>(rv.get()))
                return sa::tl::bind::CreateInstance<ui::WebView>(ctx, webviewMeta, ctx, std::move(name), prv);

            ctx.RuntimeError(sa::tl::Error::TypeMismatch, "WebView expected");
            return {};
        });
    module->AddFunction<sa::tl::Value, std::string, const sa::tl::ObjectPtr&>("webview_with_context",
        [&ctx, &webviewMeta](std::string name, const sa::tl::ObjectPtr& wvctx) -> sa::tl::Value
        {
            if (auto* prv = webviewMeta.CastTo<ui::WebViewContext>(wvctx.get()))
                return sa::tl::bind::CreateInstance<ui::WebView>(ctx, webviewMeta, ctx, std::move(name), *prv);

            ctx.RuntimeError(sa::tl::Error::TypeMismatch, "WebViewContext expected");
            return {};
        });
    module->AddFunction<sa::tl::Value>("webview_context",
        [&ctx, &webviewContextMeta]() -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<ui::WebViewContext>(ctx, webviewContextMeta, ctx); });
    module->AddFunction<sa::tl::Value, const sa::tl::Table&>("webview_data_manager",
        [&ctx, &webviewDataMeta](const sa::tl::Table& options) -> sa::tl::Value
        {
            auto result = sa::tl::bind::CreateInstance<ui::WebViewDataManager>(ctx, webviewDataMeta, ctx);

            const auto& s = result.As<sa::tl::ObjectPtr>();
            auto* dataMngr = static_cast<ui::WebViewDataManager*>(s->GetInstance());
            if (!dataMngr->Create(options))
            {
                ctx.GetGC().Remove(dataMngr);
                return {};
            }

            return result;
        });
    module->AddFunction<sa::tl::Value>("webview_data_manager_ephemeral",
        [&ctx, &webviewDataMeta]() -> sa::tl::Value
        {
            auto result = sa::tl::bind::CreateInstance<ui::WebViewDataManager>(ctx, webviewDataMeta, ctx);

            const auto& s = result.As<sa::tl::ObjectPtr>();
            auto* dataMngr = static_cast<ui::WebViewDataManager*>(s->GetInstance());
            if (!dataMngr->CreateEphemeral())
            {
                ctx.GetGC().Remove(dataMngr);
                return {};
            }

            return result;
        });
#endif

    module->AddFunction<sa::tl::Value, sa::tl::Value, std::string, int>("dialog",
        [&ctx, &dialogMeta](sa::tl::Value parent, std::string name, int buttons) -> sa::tl::Value
        {
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return nullptr;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return nullptr;
                }
            }
            return sa::tl::bind::CreateInstance<ui::Dialog>(ctx, dialogMeta, ctx, std::move(name), p, buttons);
        });
    module->AddFunction<sa::tl::Value, sa::tl::Value, std::string>("input_dialog",
        [&ctx](sa::tl::Value parent, std::string message) -> sa::tl::Value
        {
            // Simple dialog with an input text box
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return false;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return false;
                }
            }

            auto dlg = std::make_shared<ui::Dialog>(ctx, "Input Dialog", p, GTK_BUTTONS_OK_CANCEL);
            dlg->SetDefaultResponse(GTK_RESPONSE_OK);
            dlg->SetMinWidth(300);
            dlg->SetWindowType(GDK_WINDOW_TYPE_HINT_DIALOG);
            auto label = std::make_shared<ui::Label>(ctx, std::move(message));
            dlg->Add(label.get());
            auto entry = std::make_shared<ui::Entry>(ctx, "InputDialogEdit");
            dlg->Add(entry.get());
            if (dlg->Run() == GTK_RESPONSE_OK)
            {
                return sa::tl::MakeString(entry->GetText());
            }
            return false;
        });
    module->AddFunction<sa::tl::Value, sa::tl::Value, std::string>("auth_dialog",
        [&ctx](sa::tl::Value parent, std::string message) -> sa::tl::Value // NOLINT(performance-unnecessary-value-param)
        {
            // Simple dialog with an input text box
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return false;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return false;
                }
            }

            auto dlg = std::make_shared<ui::Dialog>(ctx, message, p, GTK_BUTTONS_OK_CANCEL);
            dlg->SetDefaultResponse(GTK_RESPONSE_OK);
            dlg->SetMinWidth(400);
            dlg->SetWindowType(GDK_WINDOW_TYPE_HINT_DIALOG);
            auto grid = std::make_shared<ui::Grid>(ctx, "Container");
            grid->SetNumCols(2);
            grid->SetRowSpacing(ui::DEF_MARGIN);
            grid->SetColSpacing(ui::DEF_MARGIN);
            dlg->Add(grid.get());

            auto userLabel = std::make_shared<ui::Label>(ctx, "Username:");
            grid->Add(userLabel.get());
            auto userEntry = std::make_shared<ui::Entry>(ctx, "InputDialogUser");
            userEntry->SetHExpand(true);
            grid->Add(userEntry.get());

            auto passLabel = std::make_shared<ui::Label>(ctx, "Password:");
            grid->Add(passLabel.get());
            auto passEntry = std::make_shared<ui::Entry>(ctx, "InputDialogPass");
            passEntry->SetVisibility(false);
            passEntry->SetHExpand(true);
            passEntry->SetInputPurpose(GTK_INPUT_PURPOSE_PASSWORD);
            grid->Add(passEntry.get());
            if (dlg->Run() == GTK_RESPONSE_OK)
            {
                auto result = sa::tl::MakeTable();
                result->Add("user", userEntry->GetText());
                result->Add("password", passEntry->GetText());
                return { result };
            }
            return false;
        });
    module->AddFunction<sa::tl::Value, sa::tl::Value>("open_file",
        [&ctx](sa::tl::Value parent) -> sa::tl::Value
        {
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return false;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return false;
                }
            }
            auto res = ui::OpenFileDialog(p);
            if (res.has_value())
                return sa::tl::MakeString(res.value());
            return false;
        });
    module->AddFunction<sa::tl::Value, sa::tl::Value>("save_file",
        [&ctx](sa::tl::Value parent) -> sa::tl::Value
        {
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return false;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return false;
                }
            }
            auto res = ui::SaveFileDialog(p);
            if (res.has_value())
                return sa::tl::MakeString(res.value());
            return false;
        });
    module->AddFunction<int, sa::tl::Value, int, int, std::string>("message_dialog",
        [&ctx](sa::tl::Value parent, int type, int buttons, const std::string& message) -> int
        {
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return false;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return false;
                }
            }
            return ui::MessageDialog(p, type, buttons, message);
        });
    module->AddFunction<void, sa::tl::Value, std::string>("error_dialog",
        [&ctx](sa::tl::Value parent, const std::string& message) -> void
        {
            ui::Window* p = nullptr;

            if (!parent.IsNull() && !parent.IsObject())
            {
                ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
                return;
            }
            if (!parent.IsNull())
            {
                const auto& o = parent.As<sa::tl::ObjectPtr>();
                p = o->GetMetatable().CastTo<ui::Window>(o.get());
                if (!p)
                {
                    ctx.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not a Window instance");
                    return;
                }
            }
            ui::ErrorDialog(p, message);
        });
    module->AddFunction("clipboard",
        [](sa::tl::Context& ctx, const sa::tl::FunctionArguments& args) -> sa::tl::ValuePtr
        {
            // clipboard([new_text]) -> [text]
            GtkClipboard* clip = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
            if (args.empty())
            {
                gchar* text = gtk_clipboard_wait_for_text(clip);
                if (text == nullptr)
                    return sa::tl::MakeValue(std::string(""));
                std::string str(text);
                // A newly-allocated UTF-8 string which must be freed with g_free()
                g_free(text);
                return sa::tl::MakeValue(str);
            }
            if (!args[0]->IsString())
            {
                ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String expected");
                return sa::tl::MakeNull();
            }
            const auto& s = *args[0]->As<sa::tl::StringPtr>();
            gtk_clipboard_set_text(clip, s.data(), (int)s.length());
            return sa::tl::MakeNull();
        });

    module->AddFunction<void>("main", &gtk_main);
    module->AddFunction<void>("quit", &gtk_main_quit);
    ctx.AddConst("ui", std::move(module), sa::tl::SCOPE_LIBRARY);
    int argc = 0;
    gtk_init(&argc, nullptr);

    return true;
}
