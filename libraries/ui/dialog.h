/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Window;

class Dialog final : public Widget
{
    SATL_NON_COPYABLE(Dialog);
    SATL_NON_MOVEABLE(Dialog);
    TYPED_OBJECT(Dialog);
public:
    Dialog(sa::tl::Context& ctx, std::string name, Window* parent, int buttons);
    ~Dialog() override;
    int Run();
    void Response(int id);
    void Resize(int width, int height);
    void Move(int x, int y);
    void AddButton(const std::string& text, int response);
    [[nodiscard]] int GetWindowType() const;
    void SetWindowType(int value);
    [[nodiscard]] sa::tl::TablePtr GetPosition() const;
    void SetPosition(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::TablePtr GetSize() const;
    void SetSize(const sa::tl::Table& value);
    [[nodiscard]] int GetDefaultResponse() const;
    void SetDefaultResponse(int value);
    [[nodiscard]] sa::tl::CallablePtr GetOnResponse() const { return onResponse_; }
    void SetOnResponse(sa::tl::CallablePtr value) { onResponse_ = std::move(value); }
protected:
    [[nodiscard]] GtkWidget* GetContainer() const override { return gtk_dialog_get_content_area(GTK_DIALOG(GetHandle())); }
private:
    static constexpr GtkDialogFlags FLAGS = (GtkDialogFlags)GTK_DIALOG_MODAL;
    static void OnResponse(GtkDialog*, int id, Dialog* wnd);
    int defaultResponse_{ 0 };
    sa::tl::CallablePtr onResponse_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Dialog>(sa::tl::Context& ctx);
}
