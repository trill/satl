/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#ifdef HAVE_WEBKIT

#include <object.h>
#include <webkit2/webkit2.h>

namespace ui {

class WebViewCookieManager : public sa::tl::bind::Object
{
public:
    WebViewCookieManager(sa::tl::Context& ctx, WebKitCookieManager* handle);

    void SetPersistentStorage(const std::string& filename, bool plaintext);
    void SetAccept(int value);

    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }

private:
    static void OnChanged(WebKitCookieManager*, WebViewCookieManager* data);
    WebKitCookieManager* handle_;
    sa::tl::CallablePtr onChanged_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::WebViewCookieManager>(sa::tl::Context& ctx);
}

#endif
