/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"
#include <ast.h>
#include <satl_assert.h>
#include "menu.h"
#include "window.h"
#include "cairo.h"
#include <factory.h>
#include <utf8.h>
#include <utils.h>

namespace ui {

uint32_t Widget::StateToModifiers(guint state)
{
    uint32_t result = 0;
    if ((state & GDK_CONTROL_MASK) != 0)
        result |= ModifierCtrl;
    if ((state & GDK_META_MASK) != 0)
        result |= ModifierAlt;
    if ((state & GDK_MOD1_MASK) != 0)
        result |= ModifierAlt;
    if ((state & GDK_SHIFT_MASK) != 0)
        result |= ModifierShift;
    if ((state & GDK_SUPER_MASK) != 0)
        result |= ModifierSuper;
    return result;
}

void Widget::OnResized(GtkWidget*, GdkRectangle* rect, Widget* wnd)
{
    wnd->DoOnResized(rect);
}

void Widget::OnRealize(GtkWidget*, Widget* wnd)
{
    wnd->DoOnRealize();
}

bool Widget::OnPopupMenu(GtkWidget*, Widget* wnd)
{
    return wnd->DoOnPopupMenu();
}

bool Widget::OnButtonPress(GtkWidget*, GdkEventButton* event, Widget* wnd)
{
    return wnd->DoOnButtonPress(event);
}

bool Widget::OnKeyPress(GtkWidget*, GdkEventKey* event, Widget* wnd)
{
    return wnd->DoOnKeyPress(event);
}

bool Widget::OnDraw(GtkWidget*, cairo_t* cr, Widget* wnd)
{
    return wnd->DoOnDraw(cr);
}

bool Widget::OnEnter(GtkWidget*, GdkEventCrossing* event, Widget* wnd)
{
    if (event->type == GDK_ENTER_NOTIFY)
        return wnd->DoOnEnter(event);
    if (event->type == GDK_LEAVE_NOTIFY)
        return wnd->DoOnLeave(event);
    return false;
}

bool Widget::OnScroll(GtkWidget*, GdkEventScroll* event, Widget* wnd)
{
    return wnd->DoOnScroll(event);
}

void Widget::OnShow(GtkWidget*, Widget* wnd)
{
    return wnd->DoOnShow();
}

void Widget::OnHide(GtkWidget*, Widget* wnd)
{
    return wnd->DoOnHide();
}

bool Widget::OnFocusIn(GtkWidget*, GdkEventFocus* event, Widget* wnd)
{
    return wnd->DoOnFocusIn(event);
}

bool Widget::OnFocusOut(GtkWidget*, GdkEventFocus* event, Widget* wnd)
{
    return wnd->DoOnFocusOut(event);
}

Widget::Widget(sa::tl::Context& ctx, GtkWidget* handle, std::string name) :
    Object(ctx),
    handle_(handle),
    name_(std::move(name))
{
    gtk_widget_add_events(GetHandle(),
        GDK_POINTER_MOTION_MASK |
            GDK_BUTTON_MOTION_MASK |
            GDK_BUTTON_PRESS_MASK |
            GDK_BUTTON_RELEASE_MASK |
            GDK_KEY_PRESS_MASK |
            GDK_KEY_RELEASE_MASK |
            GDK_SCROLL_MASK |
            GDK_ENTER_NOTIFY_MASK |
            GDK_LEAVE_NOTIFY_MASK);
    g_signal_connect(GetHandle(), "realize", G_CALLBACK(Widget::OnRealize), this);
    g_signal_connect(GetHandle(), "size-allocate", G_CALLBACK(Widget::OnResized), this);
    g_signal_connect(GetHandle(), "popup-menu", G_CALLBACK(Widget::OnPopupMenu), this);
    g_signal_connect(GetHandle(), "button-press-event", G_CALLBACK(Widget::OnButtonPress), this);
    g_signal_connect(GetHandle(), "key-press-event", G_CALLBACK(Widget::OnKeyPress), this);
    g_signal_connect(GetHandle(), "draw", G_CALLBACK(Widget::OnDraw), this);
    g_signal_connect(GetHandle(), "enter-notify-event", G_CALLBACK(Widget::OnEnter), this);
    g_signal_connect(GetHandle(), "scroll-event", G_CALLBACK(Widget::OnScroll), this);
    g_signal_connect(GetHandle(), "show", G_CALLBACK(Widget::OnShow), this);
    g_signal_connect(GetHandle(), "focus-in-event", G_CALLBACK(Widget::OnFocusIn), this);
    g_signal_connect(GetHandle(), "focus-out-event", G_CALLBACK(Widget::OnFocusOut), this);
}

Widget::~Widget()
{
    if (auto* p = parent_.GetPtr<Widget, Object>())
    {
        p->Remove(this);
    }
    if (GTK_IS_WIDGET(GetHandle()))
        gtk_widget_destroy(GetHandle());
}

void Widget::Destroy()
{
    for (const auto& c : children_)
    {
        if (auto* cPtr = c.GetPtr<Widget, Object>())
            cPtr->Destroy();
    }
    if (auto* p = parent_.GetPtr<Widget, Object>())
    {
        p->Remove(this);
    }
    Object::Destroy();
}

void Widget::DoOnResized(GdkRectangle* rect)
{
    if (onResized_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onResized_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("x", rect->x);
                    data->Add("y", rect->y);
                    data->Add("width", rect->width);
                    data->Add("height", rect->height);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return;
        (*onResized_)(ctx_, *GetSelfPtr(), args);
    }
}

void Widget::DoOnRealize()
{
    if (onRealize_)
        (*onRealize_)(ctx_, *GetSelfPtr(), {});
}

bool Widget::DoOnButtonPress(GdkEventButton* event)
{
    if (onButtonPress_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onButtonPress_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("type", event->type);
                    data->Add("button", event->button);
                    data->Add("modifiers", StateToModifiers(event->state));
                    data->Add<sa::tl::Float>("x", event->x);
                    data->Add<sa::tl::Float>("y", event->y);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;

        if ((*onButtonPress_)(ctx_, *GetSelfPtr(), args)->ToBool())
            return true;
    }
    if (event->button == 3)
    {
        if (auto* m = popupMenu_.GetPtr<Menu, Object>())
        {
            gtk_menu_popup_at_pointer(GTK_MENU(m->GetHandle()), nullptr);
            return true;
        }
    }
    return false;
}

bool Widget::DoOnPopupMenu()
{
    if (auto* m = popupMenu_.GetPtr<Menu, Object>())
    {
        gtk_menu_popup_at_pointer(GTK_MENU(m->GetHandle()), nullptr);
        return false;
    }
    return true;
}

void Widget::DoOnShow()
{
    if (onShow_)
        (*onShow_)(ctx_, *GetSelfPtr(), {});
}

void Widget::DoOnHide()
{
    if (onHide_)
        (*onHide_)(ctx_, *GetSelfPtr(), {});
}

bool Widget::DoOnKeyPress(GdkEventKey* event)
{
    if (onKeyPress_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onKeyPress_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("type", event->type);
                    data->Add("keyval", event->keyval);
                    data->Add("modifiers", StateToModifiers(event->state));
                    uint32_t unicode = gdk_keyval_to_unicode(event->keyval);
                    std::string string = sa::utf8::Encode({ unicode });
                    data->Add("string", string);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;

        if ((*onKeyPress_)(ctx_, *GetSelfPtr(), args)->ToBool())
            return true;
    }
    return false;
}

bool Widget::DoOnEnter(GdkEventCrossing* event)
{
    if (onEnter_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onEnter_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("type", event->type);
                    data->Add<sa::tl::Float>("x", event->x);
                    data->Add<sa::tl::Float>("y", event->y);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;

        if ((*onEnter_)(ctx_, *GetSelfPtr(), args)->ToBool())
            return true;
    }
    return false;
}

bool Widget::DoOnLeave(GdkEventCrossing* event)
{
    if (onLeave_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onLeave_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("type", event->type);
                    data->Add<sa::tl::Float>("x", event->x);
                    data->Add<sa::tl::Float>("y", event->y);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;

        if ((*onLeave_)(ctx_, *GetSelfPtr(), args)->ToBool())
            return true;
    }
    return false;
}

bool Widget::DoOnScroll(GdkEventScroll* event)
{
    if (onScroll_)
    {
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx_,
                (*onScroll_),
                0,
                1,
                [&](int)
                {
                    auto data = sa::tl::MakeTable();
                    data->Add("type", event->type);
                    data->Add<sa::tl::Float>("x", event->x);
                    data->Add<sa::tl::Float>("y", event->y);
                    data->Add("direction", event->direction);
                    data->Add<sa::tl::Float>("delta_x", event->delta_x);
                    data->Add<sa::tl::Float>("delta_y", event->delta_y);
                    args.push_back(sa::tl::MakeValue(std::move(data)));
                }))
            return false;

        if ((*onScroll_)(ctx_, *GetSelfPtr(), args)->ToBool())
            return true;
    }
    return false;
}

bool Widget::DoOnDraw(cairo_t* cr)
{
    // https://docs.gtk.org/gtk3/signal.Widget.draw.html
    if (onDraw_)
    {
        auto& ctx = ctx_;
        auto* meta = ctx.GetMetatable<Cairo>();
        SATL_ASSERT(meta);
        Cairo cairo(ctx, cr);
        auto ptr = sa::tl::MakeValue(sa::tl::MakeObject(*meta, &cairo));
        sa::tl::FunctionArguments args;
        if (!AddArguments(ctx,
                (*onDraw_),
                0,
                1,
                [&](int)
                {
                    args.push_back(std::move(ptr));
                }))
            return false;

        auto res = (*onDraw_)(ctx, *GetSelfPtr(), args);
        if (res->ToBool())
            return true;
    }

    // TRUE to stop other handlers from being invoked for the event. FALSE to propagate the event further.
    return false;
}

bool Widget::DoOnFocusIn(GdkEventFocus*)
{
    if (onFocusIn_)
    {
        if ((*onFocusIn_)(ctx_, *GetSelfPtr(), {})->ToBool())
            return true;
    }
    return false;
}

bool Widget::DoOnFocusOut(GdkEventFocus*)
{
    if (onFocusOut_)
    {
        if ((*onFocusOut_)(ctx_, *GetSelfPtr(), {})->ToBool())
            return true;
    }
    return false;
}

void Widget::Show()
{
    gtk_widget_show(GetHandle());
    visible_ = true;
}

void Widget::Hide()
{
    gtk_widget_hide(GetHandle());
    visible_ = false;
}

void Widget::Add(const sa::tl::Value& child)
{
    if (!child.IsObject())
    {
        ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not an object instance");
        return;
    }
    auto& o = child.As<sa::tl::ObjectPtr>();
    if (Menu* w = o->GetMetatable().CastTo<Menu>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        popupMenu_ = *w;
        InternalAdd(w);
        return;
    }
    if (Widget* w = o->GetMetatable().CastTo<Widget>(o.get()))
    {
        if (w->HasParent())
        {
            ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
            return;
        }
        auto* container = GetContainer();
        if (GTK_IS_CONTAINER(container))
            gtk_container_add(GTK_CONTAINER(container), w->GetHandle());
        else
            gtk_widget_set_parent(w->GetHandle(), container);
        InternalAdd(w);
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::TypeMismatch, "Value is not Widget instance");
}

void Widget::Add(Widget* child)
{
    SATL_ASSERT(child);
    if (child->HasParent())
    {
        ctx_.RuntimeError(sa::tl::Error::UserDefined, "The Widget has already a parent");
        return;
    }

    if (auto* m = dynamic_cast<Menu*>(child))
    {
        popupMenu_ = *m;
        InternalAdd(m);
        return;
    }

    auto* container = GetContainer();
    if (GTK_IS_CONTAINER(container))
        gtk_container_add(GTK_CONTAINER(container), child->GetHandle());
    else
        gtk_widget_set_parent(child->GetHandle(), container);
    InternalAdd(child);
}

void Widget::InternalAdd(Widget* child)
{
    SATL_ASSERT(child);
    SATL_ASSERT(!child->HasParent());
    children_.push_back(*child);
    child->parent_ = *this;
}

void Widget::Remove(Widget* child)
{
    SATL_ASSERT(child);
    if (auto* p = child->parent_.GetPtr<Widget, Object>())
    {
        if (p != this)
            return;
    }
    sa::tl::DeleteIf(children_,
                     [&](const sa::tl::WeakObject& current)
    {
        auto* currPtr = current.GetPtr<Widget, Object>();
        if (!currPtr)
            return true;
        return currPtr == child;
    });
}

sa::tl::Value Widget::Find(const std::string& name, bool recursive)
{
    if (name.empty())
        return nullptr;

    for (const auto& c : children_)
    {
        if (auto* o = c.GetPtr<Widget, Object>())
        {
            if (o->name_ == name)
            {
                const auto* meta = ctx_.GetMetatable(o->GetTypeName());
                if (meta)
                    return sa::tl::MakeObject(*meta, o);
            }
            if (recursive)
            {
                auto cp = o->Find(name, recursive);
                if (!cp.IsNull())
                    return cp;
            }
        }
    }
    return nullptr;
}

sa::tl::Value Widget::GetChild(int index)
{
    if (index < 0 || index >= GetCount())
        return nullptr;

    if (auto* o = children_[index].GetPtr<Widget, Object>())
    {
        const auto* meta = ctx_.GetMetatable(o->GetTypeName());
        if (meta)
            return sa::tl::MakeObject(*meta, o);
    }
    return nullptr;
}

sa::tl::Value Widget::GetWindow() const
{
    for (Widget* parent = parent_.GetPtr<Widget, Object>(); parent; parent = parent->parent_.GetPtr<Widget, Object>())
    {
        if (auto* p = dynamic_cast<Window*>(parent))
        {
            const auto* meta = ctx_.GetMetatable(p->GetTypeName());
            if (meta)
                return sa::tl::MakeObject(*meta, p);
        }
    }
    return nullptr;
}

Widget* Widget::GetWidgetFromHandle(GtkWidget* handle, bool recursive)
{
    if (!handle)
        return nullptr;
    for (const auto& c : children_)
    {
        if (auto* o = c.GetPtr<Widget, Object>())
        {
            if (o->GetHandle() == handle)
                return o;

            if (recursive)
            {
                auto* cp = o->GetWidgetFromHandle(handle, true);
                if (cp)
                    return cp;
            }
        }
    }
    return nullptr;
}

bool Widget::VisitChildren(const sa::tl::Callable& callback)
{
    using namespace sa::tl;
    if (callback.ParamCount() != 1)
    {
        ctx_.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return false;
    }
    for (int i = 0; i < GetCount(); ++i)
    {
        auto c = GetChild(i);
        auto result = callback(ctx_, { MakeValue(c) });
        if (!result->ToBool())
            return false;
    }
    return true;
}

void Widget::Focus()
{
    gtk_widget_grab_focus(GetHandle());
}

bool Widget::HasFocus() const
{
    return gtk_widget_has_focus(GetHandle());
}

bool Widget::GetFocusOnClick() const
{
    return gtk_widget_get_focus_on_click(GetHandle());
}

void Widget::SetFocusOnClick(bool value)
{
    gtk_widget_set_focus_on_click(GetHandle(), value);
}

bool Widget::GetCanFocus() const
{
    return gtk_widget_get_can_focus(GetHandle());
}

void Widget::SetCanFocus(bool value)
{
    gtk_widget_set_can_focus(GetHandle(), value);
}

void Widget::Redraw()
{
    gtk_widget_queue_draw(GetHandle());
}

void Widget::RedrawArea(int x, int y, int width, int height)
{
    gtk_widget_queue_draw_area(GetHandle(), x, y, width, height);
}

bool Widget::GetEnabled() const
{
    return gtk_widget_get_sensitive(GetHandle());
}

void Widget::SetEnabled(bool value)
{
    gtk_widget_set_sensitive(GetHandle(), value);
}

int Widget::GetHAlign() const
{
    return gtk_widget_get_halign(GetHandle());
}

void Widget::SetHAlign(int value)
{
    gtk_widget_set_halign(GetHandle(), (GtkAlign)value);
}

int Widget::GetVAlign() const
{
    return gtk_widget_get_valign(GetHandle());
}

void Widget::SetVAlign(int value)
{
    gtk_widget_set_valign(GetHandle(), (GtkAlign)value);
}

bool Widget::GetHExpand() const
{
    return gtk_widget_get_hexpand(GetHandle());
}

void Widget::SetHExpand(bool value)
{
    gtk_widget_set_hexpand(GetHandle(), value);
}

bool Widget::GetVExpand() const
{
    return gtk_widget_get_vexpand(GetHandle());
}

void Widget::SetVExpand(bool value)
{
    gtk_widget_set_vexpand(GetHandle(), value);
}

int Widget::GetMinWidth() const
{
    int w = 0, h = 0;
    gtk_widget_get_size_request(GetHandle(), &w, &h);
    return w;
}

void Widget::SetMinWidth(int value)
{
    gtk_widget_set_size_request(GetHandle(), value, GetMinHeight());
}

int Widget::GetMinHeight() const
{
    int w = 0, h = 0;
    gtk_widget_get_size_request(GetHandle(), &w, &h);
    return h;
}

void Widget::SetMinHeight(int value)
{
    gtk_widget_set_size_request(GetHandle(), GetMinWidth(), value);
}

sa::tl::TablePtr Widget::GetMinSize() const
{
    int x = 0, y = 0;
    gtk_widget_get_size_request(GetHandle(), &x, &y);
    sa::tl::TablePtr result = sa::tl::MakeTable();
    result->Add("x", x);
    result->Add("y", y);
    return result;
}

void Widget::SetMinSize(const sa::tl::Table& value)
{
    int x = 0, y = 0;
    if (!sa::tl::GetTableValue(ctx_, value, "x", x))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "y", y))
        return;
    gtk_widget_set_size_request(GetHandle(), x, y);
}

std::string Widget::GetTooltip() const
{
    const char* value = gtk_widget_get_tooltip_text(GetHandle());
    if (!value)
        return "";
    return value;
}

void Widget::SetTooltip(const std::string& value)
{
    gtk_widget_set_tooltip_text(GetHandle(), value.empty() ? nullptr : value.c_str());
}

void Widget::SetVisible(bool value)
{
    if (visible_ == value)
        return;
    if (value)
        Show();
    else
        Hide();
}

void Widget::SetMargins(const sa::tl::Table& value)
{
    int left = 0, top = 0, right = 0, bottom = 0;

    if (!sa::tl::GetTableValue(ctx_, value, "left", left))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "top", top))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "right", right))
        return;
    if (!sa::tl::GetTableValue(ctx_, value, "bottom", bottom))
        return;
    SetMargins(left, top, right, bottom);
}

void Widget::SetMargins(int left, int top, int right, int bottom)
{
    gtk_widget_set_margin_start(GetHandle(), left);
    gtk_widget_set_margin_top(GetHandle(), top);
    gtk_widget_set_margin_end(GetHandle(), right);
    gtk_widget_set_margin_bottom(GetHandle(), bottom);
}

sa::tl::TablePtr Widget::GetMargins() const
{
    auto result = sa::tl::MakeTable();
    result->Add("left", gtk_widget_get_margin_start(GetHandle()));
    result->Add("top", gtk_widget_get_margin_top(GetHandle()));
    result->Add("right", gtk_widget_get_margin_end(GetHandle()));
    result->Add("bottom", gtk_widget_get_margin_bottom(GetHandle()));

    return result;
}

sa::tl::TablePtr Widget::GetRect() const
{
    GtkAllocation alloc;
    gtk_widget_get_allocation(GetHandle(), &alloc);
    auto result = sa::tl::MakeTable();
    result->Add("left", alloc.x);
    result->Add("top", alloc.y);
    result->Add("right", alloc.x + alloc.width);
    result->Add("bottom", alloc.y + alloc.height);

    return result;
}

int Widget::GetWidth() const
{
    return gtk_widget_get_allocated_width(GetHandle());
}

int Widget::GetHeight() const
{
    return gtk_widget_get_allocated_height(GetHandle());
}

sa::tl::Float Widget::GetOpacity() const
{
    return (sa::tl::Float)gtk_widget_get_opacity(GetHandle());
}

void Widget::SetOpacity(sa::tl::Float value)
{
    gtk_widget_set_opacity(GetHandle(), (gdouble)value);
}

sa::tl::Value Widget::GetParent() const
{
    auto* p = parent_.GetPtr<Widget, Object>();
    if (!p)
        return nullptr;
    const auto* meta = ctx_.GetMetatable(p->GetTypeName());
    if (meta)
        return sa::tl::MakeObject(*meta, p);
    return nullptr;
}

bool Widget::HasParent() const
{
    return !!parent_.GetPtr<Widget, Object>();
}

sa::tl::Value Widget::GetUserData() const
{
    return userData_;
}

void Widget::SetUserData(sa::tl::Value value)
{
    userData_ = std::move(value);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Widget>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Widget, Object>();
    result
        // Add child ui::Widget
        .AddFunction<ui::Widget, void, const sa::tl::Value&>("add", &ui::Widget::Add)
        // Find child ui::Widget by name: ui::Widget.find(name, recursive) -> ui::Widget
        .AddFunction<ui::Widget, sa::tl::Value, const std::string&, bool>("find", &ui::Widget::Find)
        .AddFunction<ui::Widget, void>("show", &ui::Widget::Show)
        .AddFunction<ui::Widget, void>("hide", &ui::Widget::Hide)
        // Focus this ui::Widget
        .AddFunction<ui::Widget, void>("focus", &ui::Widget::Focus)
        .AddFunction<ui::Widget, bool>("has_focus", &ui::Widget::HasFocus)
        .AddFunction<ui::Widget, sa::tl::Value, int>("get_child", &ui::Widget::GetChild)
        // Iterates children calling a callback function. Callback function must return true to continue.
        // ui::Widget.visit_children(function(ui::Widget) { return true; });
        .AddFunction<ui::Widget, bool, const sa::tl::Callable&>("visit_children", &ui::Widget::VisitChildren)
        .AddFunction<ui::Widget, void>("redraw", &ui::Widget::Redraw)
        .AddFunction<ui::Widget, void, int, int, int, int>("redraw_area", &ui::Widget::RedrawArea)

        // Get parent ui::Widget or null
        .AddProperty("parent", &ui::Widget::GetParent)
        // The name of the ui::Widget. Can be used for the find() function
        .AddProperty("name", &ui::Widget::GetName)
        // Number of children
        .AddProperty("count", &ui::Widget::GetCount)
        .AddProperty("enabled", &ui::Widget::GetEnabled, &ui::Widget::SetEnabled)
        .AddProperty("can_focus", &ui::Widget::GetCanFocus, &ui::Widget::SetCanFocus)
        .AddProperty("focus_on_click", &ui::Widget::GetFocusOnClick, &ui::Widget::SetFocusOnClick)
        // Horizontal alignment
        .AddProperty("h_align", &ui::Widget::GetHAlign, &ui::Widget::SetHAlign)
        // Vertical alignment
        .AddProperty("v_align", &ui::Widget::GetVAlign, &ui::Widget::SetVAlign)
        // Expand to fill available horizontal space
        .AddProperty("h_expand", &ui::Widget::GetHExpand, &ui::Widget::SetHExpand)
        // Expand to fill available vertical space
        .AddProperty("v_expand", &ui::Widget::GetVExpand, &ui::Widget::SetVExpand)
        .AddProperty("margins", &ui::Widget::GetMargins, &ui::Widget::SetMargins)
        .AddProperty("opacity", &ui::Widget::GetOpacity, &ui::Widget::SetOpacity)
        .AddProperty("min_width", &ui::Widget::GetMinWidth, &ui::Widget::SetMinWidth)
        .AddProperty("min_height", &ui::Widget::GetMinHeight, &ui::Widget::SetMinHeight)
        .AddProperty("min_size", &ui::Widget::GetMinSize, &ui::Widget::SetMinSize)
        .AddProperty("tooltip", &ui::Widget::GetTooltip, &ui::Widget::SetTooltip)
        .AddProperty("rect", &ui::Widget::GetRect)
        .AddProperty("visible", &ui::Widget::IsVisible, &ui::Widget::SetVisible)
        .AddProperty("window", &ui::Widget::GetWindow)
        .AddProperty("user_data", &ui::Widget::GetUserData, &ui::Widget::SetUserData)
        .AddProperty("width", &ui::Widget::GetWidth)
        .AddProperty("height", &ui::Widget::GetHeight)

        .AddProperty("on_button_press", &ui::Widget::GetOnButtonPress, &ui::Widget::SetOnButtonPress)
        .AddProperty("on_key_press", &ui::Widget::GetOnKeyPress, &ui::Widget::SetOnKeyPress)
        .AddProperty("on_realize", &ui::Widget::GetOnRealize, &ui::Widget::SetOnRealize)
        .AddProperty("on_resized", &ui::Widget::GetOnResized, &ui::Widget::SetOnResized)
        .AddProperty("on_draw", &ui::Widget::GetOnDraw, &ui::Widget::SetOnDraw)
        .AddProperty("on_scroll", &ui::Widget::GetOnScroll, &ui::Widget::SetOnScroll)
        .AddProperty("on_show", &ui::Widget::GetOnShow, &ui::Widget::SetOnShow)
        .AddProperty("on_hide", &ui::Widget::GetOnHide, &ui::Widget::SetOnHide)
        .AddProperty("on_enter", &ui::Widget::GetOnEnter, &ui::Widget::SetOnEnter)
        .AddProperty("on_leave", &ui::Widget::GetOnLeave, &ui::Widget::SetOnLeave)
        .AddProperty("on_focus_in", &ui::Widget::GetOnFocusIn, &ui::Widget::SetOnFocusIn)
        .AddProperty("on_focus_out", &ui::Widget::GetOnFocusOut, &ui::Widget::SetOnFocusOut);

    return result;
}

}
