/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "dialogs.h"
#include <gtk/gtk.h>

namespace ui {

int MessageDialog(Window* parent, int type, int buttons, const std::string& message)
{
    auto* dlg = gtk_message_dialog_new(parent ? GTK_WINDOW(parent->GetHandle()) : nullptr,
        GTK_DIALOG_MODAL, (GtkMessageType)type, (GtkButtonsType)buttons, "%s", message.c_str());
    auto response = gtk_dialog_run(GTK_DIALOG(dlg));
    gtk_widget_destroy(dlg);
    return response;
}

void ErrorDialog(Window* parent, const std::string& message)
{
    MessageDialog(parent, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, message);
}

static std::optional<std::string> FileDialog(GtkWindow* parent, GtkFileChooserAction mode, const gchar* confirm)
{
    GtkWidget* fcd = nullptr;
    GtkFileChooser* fc = nullptr;
    gint response = 0;

    fcd = gtk_file_chooser_dialog_new(
        nullptr, parent, mode, "_Cancel", GTK_RESPONSE_CANCEL, confirm, GTK_RESPONSE_ACCEPT, NULL);
    fc = GTK_FILE_CHOOSER(fcd);
    gtk_file_chooser_set_local_only(fc, FALSE);
    gtk_file_chooser_set_select_multiple(fc, FALSE);
    gtk_file_chooser_set_show_hidden(fc, TRUE);
    gtk_file_chooser_set_do_overwrite_confirmation(fc, TRUE);
    gtk_file_chooser_set_create_folders(fc, TRUE);
    response = gtk_dialog_run(GTK_DIALOG(fcd));
    if (response != GTK_RESPONSE_ACCEPT)
    {
        gtk_widget_destroy(fcd);
        return {};
    }
    std::string filename = gtk_file_chooser_get_filename(fc);
    gtk_widget_destroy(fcd);
    return filename;
}

std::optional<std::string> OpenFileDialog(Window* parent)
{
    return FileDialog(parent ? GTK_WINDOW(parent->GetHandle()) : nullptr, GTK_FILE_CHOOSER_ACTION_OPEN, "_Open");
}
std::optional<std::string> SaveFileDialog(Window* parent)
{
    return FileDialog(parent ? GTK_WINDOW(parent->GetHandle()) : nullptr, GTK_FILE_CHOOSER_ACTION_SAVE, "_Save");
}

}
