/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Tab final : public Widget
{
    SATL_NON_COPYABLE(Tab);
    SATL_NON_MOVEABLE(Tab);
    TYPED_OBJECT(Tab);
public:
    Tab(sa::tl::Context& ctx, std::string name);
    ~Tab() override;
    void Add(const sa::tl::Value&) override;
    [[nodiscard]] int GetPageCount() const;
    [[nodiscard]] sa::tl::CallablePtr GetOnChanged() const { return onChanged_; }
    void SetOnChanged(sa::tl::CallablePtr value) { onChanged_ = std::move(value); }
    [[nodiscard]] int GetCurrentPage() const;
    void SetCurrentPage(int value);
private:
    static void OnChanged(GtkNotebook*, GtkWidget*, guint, Tab* data);
    sa::tl::CallablePtr onChanged_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Tab>(sa::tl::Context& ctx);
}
