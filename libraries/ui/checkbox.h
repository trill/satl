/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class Checkbox final : public Widget
{
    SATL_NON_COPYABLE(Checkbox);
    SATL_NON_MOVEABLE(Checkbox);
    TYPED_OBJECT(Checkbox);
public:
    Checkbox(sa::tl::Context& ctx, std::string name);
    ~Checkbox() override;

    [[nodiscard]] std::string GetText() const;
    void SetText(const std::string& value);
    [[nodiscard]] sa::tl::CallablePtr GetOnToggled() const { return onToggled_; }
    void SetOnToggled(sa::tl::CallablePtr value) { onToggled_ = std::move(value); }
private:
    static void OnToggled(GtkToggleButton* button, Checkbox* btn);
    sa::tl::CallablePtr onToggled_;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Checkbox>(sa::tl::Context& ctx);
}
