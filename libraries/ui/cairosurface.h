/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cairo.h>
#include <non_copyable.h>
#include <object.h>
#include <context.h>

namespace ui {

// https://cairographics.org/manual/cairo-cairo-surface-t.html
class CairoSurface : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(CairoSurface);
    SATL_NON_MOVEABLE(CairoSurface);
    TYPED_OBJECT(CairoSurface);
public:
    explicit CairoSurface(sa::tl::Context& ctx);
    CairoSurface(sa::tl::Context& ctx, cairo_surface_t* surface);
    ~CairoSurface() override;

    void Finish();
    void Flush();

    bool CreateImageSurface(cairo_format_t format, int width, int height);
    bool CreatePDFSurface(std::string filename, sa::tl::Float width, sa::tl::Float height);
    bool CreatePSSurface(std::string filename, sa::tl::Float width, sa::tl::Float height);
    bool CreateSVGSurface(std::string filename, sa::tl::Float width, sa::tl::Float height);
    bool CreateForRectangle(const sa::tl::ObjectPtr& target, sa::tl::Float x, sa::tl::Float y, sa::tl::Float width, sa::tl::Float height);
    bool CreateSimilar(const sa::tl::ObjectPtr& other, int content, sa::tl::Float width, sa::tl::Float height);
    bool CreateSimilarImage(const sa::tl::ObjectPtr& other, int format, sa::tl::Float width, sa::tl::Float height);
    bool CreateFromPNG(std::string filename);

    void ShowPage();
    void CopyPage();
    void MarkDirty();
    void MarkDirtyRectangle(int x, int y, int width, int height);
    bool SetSize(const sa::tl::Value& width, const sa::tl::Value& height);
    void SetEPS(bool value);
    [[nodiscard]] bool GetEPS() const;
    bool WriteToPNG(const std::string& filename);

    [[nodiscard]] int GetContent() const;
    [[nodiscard]] sa::tl::TablePtr GetDeviceScale() const;
    void SetDeviceScale(const sa::tl::Table& value);

    [[nodiscard]] cairo_surface_t* GetHandle() const { return surface_; }
private:
    bool own_{ false };
    cairo_surface_t* surface_{ nullptr };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::CairoSurface>(sa::tl::Context& ctx);
}
