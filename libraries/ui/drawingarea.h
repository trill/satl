/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

// Essentially a blank widget you can draw on.
class DrawingArea : public Widget
{
    SATL_NON_COPYABLE(DrawingArea);
    SATL_NON_MOVEABLE(DrawingArea);
    TYPED_OBJECT(DrawingArea);
public:
    DrawingArea(sa::tl::Context& ctx, std::string name);
    ~DrawingArea() override;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::DrawingArea>(sa::tl::Context& ctx);
}
