/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "widget.h"

namespace ui {

class VerticalSeparator final : public Widget
{
    SATL_NON_COPYABLE(VerticalSeparator);
    SATL_NON_MOVEABLE(VerticalSeparator);
    TYPED_OBJECT(VerticalSeparator);
public:
    VerticalSeparator(sa::tl::Context& ctx, std::string name);
    ~VerticalSeparator() override;
};

class HorizontalSeparator final : public Widget
{
    SATL_NON_COPYABLE(HorizontalSeparator);
    SATL_NON_MOVEABLE(HorizontalSeparator);
    TYPED_OBJECT(HorizontalSeparator);
public:
    HorizontalSeparator(sa::tl::Context& ctx, std::string name);
    ~HorizontalSeparator() override;
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::VerticalSeparator>(sa::tl::Context& ctx);
template<> sa::tl::MetaTable& Register<ui::HorizontalSeparator>(sa::tl::Context& ctx);
}
