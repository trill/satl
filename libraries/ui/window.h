/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "widget.h"

namespace ui {

class MenuBar;

class Window final : public Widget
{
    SATL_NON_COPYABLE(Window);
    SATL_NON_MOVEABLE(Window);
    TYPED_OBJECT(Window);
public:
    enum class WindowState
    {
        Normal,
        Minimized,
        Maximized,
        Fullscreen
    };
    Window(sa::tl::Context& ctx, std::string name);
    ~Window() override;

    void Show() override;
    void Close();
    void Resize(int width, int height);
    void Move(int x, int y);
    void Add(const sa::tl::Value&) override;
    [[nodiscard]] std::string GetTitle() const;
    void SetTitle(const std::string& value);
    [[nodiscard]] sa::tl::TablePtr GetPosition() const;
    void SetPosition(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::TablePtr GetSize() const;
    void SetSize(const sa::tl::Table& value);
    [[nodiscard]] sa::tl::TablePtr GetDefaultSize() const;
    void SetDefaultSize(const sa::tl::Table& value);
    [[nodiscard]] bool GetResizeable() const;
    void SetResizeable(bool value);
    [[nodiscard]] int GetWindowType() const;
    void SetWindowType(int value);
    [[nodiscard]] sa::tl::CallablePtr GetOnClosing() const { return onClosing_; }
    void SetOnClosing(sa::tl::CallablePtr value) { onClosing_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnShow() const { return onShow_; }
    void SetOnShow(sa::tl::CallablePtr value) { onShow_ = std::move(value); }
    [[nodiscard]] sa::tl::CallablePtr GetOnWindowState() const { return onWindowState_; }
    void SetOnWindowState(sa::tl::CallablePtr value) { onWindowState_ = std::move(value); }
    [[nodiscard]] int GetWindowState() const { return (int)windowState_; }
    void Maximize();
    void Minimize();
    void Unminimize();
    void Fullscreen();
    void SetMargins(const sa::tl::Table&) override;
    [[nodiscard]] sa::tl::TablePtr GetMargins() const override;
    [[nodiscard]] sa::tl::TablePtr GetRect() const override;
protected:
    [[nodiscard]] GtkWidget* GetContainer() const override { return GTK_WIDGET(container_); }
private:
    GtkBox* container_;
    static gboolean OnClosing(GtkWidget*, GdkEvent*, Window* wnd);
    static void OnDestroy(GtkWidget*, Widget* wnd);
    static gboolean OnWindowState(GtkWidget*, GdkEventWindowState* event, Window* wnd);
    sa::tl::CallablePtr onClosing_;
    sa::tl::CallablePtr onShow_;
    sa::tl::CallablePtr onWindowState_;
    sa::tl::WeakObject menubar_;
    WindowState windowState_{ WindowState::Normal };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<ui::Window>(sa::tl::Context& ctx);
}
