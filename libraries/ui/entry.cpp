/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "entry.h"
#include <ast.h>

namespace ui {

void Entry::OnChanged(GtkEditable*, Entry* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

void Entry::OnActivate(GtkEditable*, Entry* data)
{
    if (data->onActivate_)
        (*data->onActivate_)(data->ctx_, *data->GetSelfPtr(), {});
}

void Entry::OnSearchChanged(GtkSearchEntry*, Entry* data)
{
    if (data->onChanged_)
        (*data->onChanged_)(data->ctx_, *data->GetSelfPtr(), {});
}

Entry::Entry(sa::tl::Context& ctx, std::string name, bool search) :
    Widget(ctx, search ? gtk_search_entry_new() : gtk_entry_new(), std::forward<std::string>(name))
{
    if (!search)
        changedHandler_ = g_signal_connect(GetHandle(), "changed", G_CALLBACK(Entry::OnChanged), this);
    else
        changedHandler_ = g_signal_connect(GetHandle(), "search-changed", G_CALLBACK(Entry::OnSearchChanged), this);
    g_signal_connect(GetHandle(), "activate", G_CALLBACK(Entry::OnActivate), this);
    gtk_widget_show(GetHandle());
}

Entry::Entry(sa::tl::Context& ctx, std::string name) :
    Entry(ctx, std::forward<std::string>(name), false)
{ }

Entry::~Entry() = default;

std::string Entry::GetText() const
{
    return gtk_entry_get_text(GTK_ENTRY(GetHandle()));
}

void Entry::SetText(const std::string& value)
{
    g_signal_handler_block(GetHandle(), changedHandler_);
    gtk_entry_set_text(GTK_ENTRY(GetHandle()), value.c_str());
    g_signal_handler_unblock(GetHandle(), changedHandler_);
}

bool Entry::GetVisibility() const
{
    return gtk_entry_get_visibility(GTK_ENTRY(GetHandle()));
}

void Entry::SetVisibility(bool value)
{
    return gtk_entry_set_visibility(GTK_ENTRY(GetHandle()), value);
}

int Entry::GetInputPurpose() const
{
    return (int)gtk_entry_get_input_purpose(GTK_ENTRY(GetHandle()));
}

void Entry::SetInputPurpose(int value)
{
    gtk_entry_set_input_purpose(GTK_ENTRY(GetHandle()), (GtkInputPurpose)value);
}

bool Entry::GetEditable() const
{
    return gtk_editable_get_editable(GTK_EDITABLE(GetHandle()));
}

void Entry::SetEditable(bool value)
{
    gtk_editable_set_editable(GTK_EDITABLE(GetHandle()), value);
}

void Entry::SelectRegion(int start, int end)
{
    gtk_editable_select_region(GTK_EDITABLE(GetHandle()), start, end);
}

SearchEntry::SearchEntry(sa::tl::Context& ctx, std::string name) :
    Entry(ctx, std::forward<std::string>(name), true)
{ }

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<ui::Entry>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::Entry, ui::Widget>();
    result
        .AddFunction<ui::Entry, void, int, int>("select_region", &ui::Entry::SelectRegion)
        .AddProperty("text", &ui::Entry::GetText, &ui::Entry::SetText)
        .AddProperty("visibility", &ui::Entry::GetVisibility, &ui::Entry::SetVisibility)
        .AddProperty("editable", &ui::Entry::GetEditable, &ui::Entry::SetEditable)
        .AddProperty("input_purpose", &ui::Entry::GetInputPurpose, &ui::Entry::SetInputPurpose)
        .AddProperty("on_activate", &ui::Entry::GetOnActivate, &ui::Entry::SetOnActivate)
        .AddProperty("on_changed", &ui::Entry::GetOnChanged, &ui::Entry::SetOnChanged);
    return result;
}

template<>
sa::tl::MetaTable& Register<ui::SearchEntry>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<ui::SearchEntry, ui::Entry>();
    return result;
}

}
