/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <object.h>
#include <pugixml.hpp>
#include <memory.h>
#include "node.h"

namespace xml {

class Document final : public Node
{
    SATL_NON_COPYABLE(Document);
    SATL_NON_MOVEABLE(Document);
    TYPED_OBJECT(Document);
public:
    Document(sa::tl::Context& ctx);
    void LoadFile(const std::string& filename);
    bool SaveFile(const std::string& filename);
    void LoadString(const std::string& string);
    std::string SaveString();
private:
    pugi::xml_document document_;
};

} // namespace xml

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<xml::Document>(sa::tl::Context& ctx);
}
