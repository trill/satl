/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "node.h"
#include <factory.h>
#include "attribute.h"

namespace xml {

Node::Node(sa::tl::Context& ctx, pugi::xml_node node) :
    Object(ctx),
    node_(node)
{
}

const sa::tl::MetaTable& Node::GetNodeMeta() const
{
    if (!nodeMeta_)
        nodeMeta_ = ctx_.GetMetatable<Node>();
    SATL_ASSERT(nodeMeta_);
    return *nodeMeta_;
}

const sa::tl::MetaTable& Node::GetAttrMeta() const
{
    if (!attrMeta_)
        attrMeta_ = ctx_.GetMetatable<Attribute>();
    SATL_ASSERT(attrMeta_);
    return *attrMeta_;
}

sa::tl::Value Node::GetFirstChild()
{
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.first_child());
}

sa::tl::Value Node::GetLastChild()
{
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.last_child());
}

sa::tl::Value Node::GetChild(const std::string& name)
{
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.child(name.c_str()));
}

sa::tl::TablePtr Node::GetChildren(const std::string& name)
{
    if (name.empty())
    {
        auto nodes = node_.children();
        auto result = sa::tl::MakeTable();
        for (const auto& nd : nodes)
            result->Add(sa::tl::bind::CreateInstanceValuePtr<Node>(ctx_, GetNodeMeta(), ctx_, nd));
        return result;
    }

    auto nodes = node_.children(name.c_str());
    auto result = sa::tl::MakeTable();
    for (const auto& nd : nodes)
        result->Add(sa::tl::bind::CreateInstanceValuePtr<Node>(ctx_, GetNodeMeta(), ctx_, nd));
    return result;
}

sa::tl::Value Node::GetNextSibling(const std::string& name)
{
    if (name.empty())
        return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.next_sibling());
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.next_sibling(name.c_str()));
}

sa::tl::Value Node::GetPreviousSibling(const std::string& name)
{
    if (name.empty())
        return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.previous_sibling());
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.previous_sibling(name.c_str()));
}

bool Node::Traverse(sa::tl::CallablePtr pred)
{
    class SimpleWalker final : public pugi::xml_tree_walker
    {
    public:
        SimpleWalker(sa::tl::Context& ctx, sa::tl::CallablePtr callback, const sa::tl::MetaTable& nodeMeta) :
            ctx_(ctx),
            callback_(std::move(callback)),
            nodeMeta_(nodeMeta)
        {
        }
        bool for_each(pugi::xml_node& node) override
        {
            auto current = sa::tl::bind::CreateInstance<Node>(ctx_, nodeMeta_, ctx_, node);
            sa::tl::bind::ObjectDestroyer resDest(*static_cast<Object*>(current.As<sa::tl::ObjectPtr>()->GetInstance()));
            return (*callback_)(ctx_, { sa::tl::MakeValue(current), sa::tl::MakeValue(depth()) })->ToBool();
        }
    private:
        sa::tl::Context& ctx_;
        sa::tl::CallablePtr callback_;
        const sa::tl::MetaTable& nodeMeta_;
    };
    SimpleWalker walker(ctx_, std::move(pred), GetNodeMeta());
    return node_.traverse(walker);
}

sa::tl::Value Node::FindNode(sa::tl::CallablePtr pred)
{
    auto node = node_.find_node([&](const pugi::xml_node& curr) -> bool {
        auto current = sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, curr);
        sa::tl::bind::ObjectDestroyer resDest(*static_cast<Object*>(current.As<sa::tl::ObjectPtr>()->GetInstance()));
        return (*pred)(ctx_, { sa::tl::MakeValue(current) })->ToBool();
    });

    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node);
}

sa::tl::Value Node::SelectNode(const std::string& selector)
{
    auto node = node_.select_node(selector.c_str());
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node.node());
}

sa::tl::TablePtr Node::SelectNodes(const std::string& selector)
{
    auto nodes = node_.select_nodes(selector.c_str());
    auto result = sa::tl::MakeTable();
    for (const auto& nd : nodes)
        result->Add(sa::tl::bind::CreateInstanceValuePtr<Node>(ctx_, GetNodeMeta(), ctx_, nd.node()));
    return result;
}

std::string Node::GetText() const
{
    return node_.text().as_string();
}

void Node::SetText(const std::string& value)
{
    node_.text().set(value.c_str());
}

std::string Node::GetName() const
{
    return node_.name();
}

void Node::SetName(const std::string& value)
{
    node_.set_name(value.c_str());
}

std::string Node::GetValue() const
{
    return node_.value();
}

void Node::SetValue(const std::string& value)
{
    node_.set_value(value.c_str());
}

sa::tl::Value Node::GetParent() const
{
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.parent());
}

sa::tl::Value Node::GetRoot() const
{
    return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node_.root());
}

sa::tl::Value Node::GetAttribute(const std::string& name)
{
    return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetAttrMeta(), ctx_, node_.attribute(name.c_str()));
}

sa::tl::TablePtr Node::GetAttributes()
{
    auto result = sa::tl::MakeTable();
    auto attrs = node_.attributes();
    for (const auto& attr : attrs)
    {
        result->Add(sa::tl::bind::CreateInstanceValuePtr<Attribute>(ctx_, GetAttrMeta(), ctx_, attr));
    }
    return result;
}

sa::tl::Value Node::FindAttribute(sa::tl::CallablePtr pred)
{
    auto result = node_.find_attribute([&](const pugi::xml_attribute& curr) -> bool {
        auto current = sa::tl::bind::CreateInstance<Attribute>(ctx_, GetAttrMeta(), ctx_, curr);
        sa::tl::bind::ObjectDestroyer resDest(*static_cast<Object*>(current.As<sa::tl::ObjectPtr>()->GetInstance()));
        return (*pred)(ctx_, { sa::tl::MakeValue(current) })->ToBool();
    });

    return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetAttrMeta(), ctx_, result);
}

sa::tl::Value Node::AppendAttribute(const sa::tl::Value& value)
{
    if (value.IsString())
    {
        auto a = node_.append_attribute(value.ToString().c_str());
        return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetAttrMeta(), ctx_, a);
    }
    if (value.IsObject())
    {
        const auto& obj = value.As<sa::tl::ObjectPtr>();
        if (auto* attr = GetAttrMeta().CastTo<Attribute>(obj.get()))
        {
            auto a = node_.append_attribute(attr->GetName().c_str());
            if (!a.empty())
                a.set_value(attr->GetValue().c_str());
            return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetAttrMeta(), ctx_, a);
        }
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not an instance of Attribute");
        return nullptr;
    }
    if (value.IsTable())
    {
        std::string name;
        std::string _value;
        const sa::tl::TablePtr& tbl = value.As<sa::tl::TablePtr>();
        if (!sa::tl::GetTableValue(ctx_, *tbl, "name", name))
            return nullptr;
        if (!sa::tl::GetTableValue(ctx_, *tbl, "value", _value))
            return nullptr;
        auto a = node_.append_attribute(name.c_str());
        if (!a.empty())
            a.set_value(_value.c_str());
        return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetAttrMeta(), ctx_, a);
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Attribute or Table expected");
    return nullptr;
}

bool Node::RemoveAttribute(const sa::tl::Value& value)
{
    if (value.IsObject())
    {
        const auto& obj = value.As<sa::tl::ObjectPtr>();
        if (auto* attr = GetAttrMeta().CastTo<Attribute>(obj.get()))
        {
            return node_.remove_attribute(attr->attr_);
        }
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not an instance of Attribute");
        return false;
    }
    if (value.IsString())
    {
        return node_.remove_attribute(value.ToString().c_str());
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String or Attribute object expected");
    return false;
}

bool Node::RemoveAttributes()
{
    return node_.remove_attributes();
}

sa::tl::Value Node::AppendChild(const sa::tl::Value& value)
{
    if (value.IsInt())
    {
        auto node = node_.append_child(static_cast<pugi::xml_node_type>(value.ToInt()));
        return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node);
    }
    if (value.IsString())
    {
        auto node = node_.append_child(value.ToString().c_str());
        return sa::tl::bind::CreateInstance<Node>(ctx_, GetNodeMeta(), ctx_, node);
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String or int expected");
    return nullptr;
}

bool Node::RemoveChild(const sa::tl::Value& child)
{
    if (child.IsObject())
    {
        const auto& obj = child.As<sa::tl::ObjectPtr>();
        if (auto* nd = GetNodeMeta().CastTo<Node>(obj.get()))
        {
            return node_.remove_child(nd->node_);
        }
        ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not an instance of Node");
        return false;
    }
    if (child.IsString())
    {
        return node_.remove_child(child.ToString().c_str());
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "String or Node object expected");
    return false;
}

bool Node::RemoveChildren()
{
    return node_.remove_children();
}

void Node::AppendBuffer(const std::string& value)
{
    auto xmlResult = node_.append_buffer(value.c_str(), value.size());
    if (xmlResult.status != pugi::status_ok)
        ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("XML parsing error offset {}: {}", xmlResult.offset, xmlResult.description()));
}

bool Node::GetEmpty() const
{
    return node_.empty();
}

sa::tl::Value Node::OpNot() const
{
    return node_.empty();
}

sa::tl::Value Node::OpCountOf() const
{
    if (node_.empty())
        return 0;
    auto children = node_.children();
    return static_cast<sa::tl::Integer>(std::distance(children.begin(), children.end()));
}

int Node::GetType() const
{
    return (int)node_.type();
}

std::string Node::GetPath() const
{
    return node_.path();
}

} // namespace xml

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<xml::Node>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<xml::Node, sa::tl::bind::Object>();
    result
        .AddFunction<xml::Node, sa::tl::Value>("get_first_child", &xml::Node::GetFirstChild)
        .AddFunction<xml::Node, sa::tl::Value>("get_last_child", &xml::Node::GetLastChild)
        .AddFunction<xml::Node, sa::tl::Value, const std::string&>("get_child", &xml::Node::GetChild)
        .AddFunction<xml::Node, sa::tl::TablePtr, const std::string&>("get_children", &xml::Node::GetChildren)
        .AddFunction<xml::Node, sa::tl::Value, const std::string&>("get_next_sibling", &xml::Node::GetNextSibling)
        .AddFunction<xml::Node, sa::tl::Value, const std::string&>("get_previous_sibling", &xml::Node::GetPreviousSibling)
        .AddFunction<xml::Node, sa::tl::Value, sa::tl::CallablePtr>("find_node", &xml::Node::FindNode)
        .AddFunction<xml::Node, bool, sa::tl::CallablePtr>("traverse", &xml::Node::Traverse)
        .AddFunction<xml::Node, sa::tl::Value, const std::string&>("select_node", &xml::Node::SelectNode)
        .AddFunction<xml::Node, sa::tl::TablePtr, const std::string&>("select_nodes", &xml::Node::SelectNodes)
        .AddFunction<xml::Node, sa::tl::Value, const std::string&>("get_attribute", &xml::Node::GetAttribute)
        .AddFunction<xml::Node, sa::tl::Value, sa::tl::CallablePtr>("find_attribute", &xml::Node::FindAttribute)
        .AddFunction<xml::Node, sa::tl::TablePtr>("get_attributes", &xml::Node::GetAttributes)
        .AddFunction<xml::Node, sa::tl::Value, const sa::tl::Value&>("append_attribute", &xml::Node::AppendAttribute)
        .AddFunction<xml::Node, bool, const sa::tl::Value&>("remove_attribute", &xml::Node::RemoveAttribute)
        .AddFunction<xml::Node, bool>("remove_attributes", &xml::Node::RemoveAttributes)
        .AddFunction<xml::Node, sa::tl::Value, const sa::tl::Value&>("append_child", &xml::Node::AppendChild)
        .AddFunction<xml::Node, bool, const sa::tl::Value&>("remove_child", &xml::Node::RemoveChild)
        .AddFunction<xml::Node, bool>("remove_children", &xml::Node::RemoveChildren)
        .AddFunction<xml::Node, void, const std::string&>("append_buffer", &xml::Node::AppendBuffer)

        .AddProperty("parent", &xml::Node::GetParent)
        .AddProperty("root", &xml::Node::GetRoot)
        .AddProperty("empty", &xml::Node::GetEmpty)
        .AddProperty("type", &xml::Node::GetType)
        .AddProperty("name", &xml::Node::GetName, &xml::Node::SetName)
        .AddProperty("value", &xml::Node::GetValue, &xml::Node::SetValue)
        .AddProperty("text", &xml::Node::GetText, &xml::Node::SetText)
        .AddProperty("path", &xml::Node::GetPath)

        .AddOperator("!operator", &xml::Node::OpNot)
        .AddOperator("#operator", &xml::Node::OpCountOf);

    return result;
}

}
