/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "document.h"
#include <factory.h>
#include <satl_assert.h>

namespace xml {

Document::Document(sa::tl::Context& ctx) :
    Node(ctx, {})
{
    node_ = document_.root();
}

void Document::LoadFile(const std::string& filename)
{
    document_.reset();
    const pugi::xml_parse_result xmlResult = document_.load_file(filename.c_str());
    if (xmlResult.status != pugi::status_ok)
        ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("XML parsing error offset {}: {}", xmlResult.offset, xmlResult.description()));
    node_ = document_.document_element();
}

bool Document::SaveFile(const std::string& filename)
{
    if (document_.empty())
        return false;
    return document_.save_file(filename.c_str());
}

void Document::LoadString(const std::string& string)
{
    document_.reset();
    const pugi::xml_parse_result xmlResult = document_.load_string(string.c_str());
    if (xmlResult.status != pugi::status_ok)
        ctx_.RuntimeError(sa::tl::Error::UserDefined, std::format("XML parsing error offset {}: {}", xmlResult.offset, xmlResult.description()));
    node_ = document_.document_element();
}

std::string Document::SaveString()
{
    if (document_.empty())
        return "";

    std::stringstream ss;
    document_.save(ss);
    return ss.str();
}

} // namespace xml

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<xml::Document>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<xml::Document, xml::Node>();
    result
        .AddFunction<xml::Document, void, const std::string&>("load_file", &xml::Document::LoadFile)
        .AddFunction<xml::Document, void, const std::string&>("load_string", &xml::Document::LoadString)
        .AddFunction<xml::Document, bool, const std::string&>("save_file", &xml::Document::SaveFile)
        .AddFunction<xml::Document, std::string>("save_string", &xml::Document::SaveString);
    return result;
}

}
