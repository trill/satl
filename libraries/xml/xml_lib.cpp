/**
 * Copyright (c) 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <module.h>
#include "document.h"
#include "node.h"
#include "attribute.h"
#include <pugixml.hpp>
#include <factory.h>

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("xml"))
        return true;

    auto module = sa::tl::MakeModule();

    module->AddConst<int>("node_null", (int)pugi::node_null);
    module->AddConst<int>("node_document", (int)pugi::node_document);
    module->AddConst<int>("node_element", (int)pugi::node_element);
    module->AddConst<int>("node_pcdata", (int)pugi::node_pcdata);
    module->AddConst<int>("node_cdata", (int)pugi::node_cdata);
    module->AddConst<int>("node_comment", (int)pugi::node_comment);
    module->AddConst<int>("node_pi", (int)pugi::node_pi);
    module->AddConst<int>("node_declaration", (int)pugi::node_declaration);
    module->AddConst<int>("node_doctype", (int)pugi::node_doctype);

    sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    sa::tl::bind::Register<xml::Node>(ctx);
    sa::tl::bind::Register<xml::Attribute>(ctx);
    sa::tl::MetaTable& docMeta = sa::tl::bind::Register<xml::Document>(ctx);

    module->AddFunction<sa::tl::Value>("document",
        [&ctx, &docMeta]() -> sa::tl::Value
        {
            return sa::tl::bind::CreateInstance<xml::Document>(ctx, docMeta, ctx);
        });

    ctx.AddConst("xml", std::move(module), sa::tl::SCOPE_LIBRARY);
    return true;
}
