/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <object.h>
#include <pugixml.hpp>

namespace xml {

class Node : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Node);
    SATL_NON_MOVEABLE(Node);
    TYPED_OBJECT(Node);
public:
    Node(sa::tl::Context& ctx, pugi::xml_node node);

    sa::tl::Value GetFirstChild();
    sa::tl::Value GetLastChild();
    sa::tl::Value GetChild(const std::string& name);
    sa::tl::TablePtr GetChildren(const std::string& name);
    sa::tl::Value GetNextSibling(const std::string& name);
    sa::tl::Value GetPreviousSibling(const std::string& name);
    sa::tl::Value FindNode(sa::tl::CallablePtr pred);
    bool Traverse(sa::tl::CallablePtr pred);
    sa::tl::Value SelectNode(const std::string& selector);
    sa::tl::TablePtr SelectNodes(const std::string& selector);
    sa::tl::Value GetChildren();
    std::string GetText() const;
    void SetText(const std::string& value);
    std::string GetName() const;
    void SetName(const std::string& value);
    std::string GetValue() const;
    void SetValue(const std::string& value);
    sa::tl::Value GetParent() const;
    sa::tl::Value GetRoot() const;
    sa::tl::Value GetAttribute(const std::string& name);
    sa::tl::TablePtr GetAttributes();
    sa::tl::Value FindAttribute(sa::tl::CallablePtr pred);
    sa::tl::Value AppendAttribute(const sa::tl::Value& value);
    bool RemoveAttribute(const sa::tl::Value& value);
    bool RemoveAttributes();
    sa::tl::Value AppendChild(const sa::tl::Value& value);
    bool RemoveChild(const sa::tl::Value& child);
    bool RemoveChildren();
    void AppendBuffer(const std::string& value);
    bool GetEmpty() const;
    int GetType() const;
    std::string GetPath() const;
    sa::tl::Value OpNot() const;
    sa::tl::Value OpCountOf() const;
private:
    const sa::tl::MetaTable& GetNodeMeta() const;
    const sa::tl::MetaTable& GetAttrMeta() const;
    mutable const sa::tl::MetaTable* nodeMeta_{ nullptr };
    mutable const sa::tl::MetaTable* attrMeta_{ nullptr };
protected:
    pugi::xml_node node_;
};

} // namespace xml

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<xml::Node>(sa::tl::Context& ctx);
}
