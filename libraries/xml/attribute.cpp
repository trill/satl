/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "attribute.h"
#include <factory.h>

namespace xml {

Attribute::Attribute(sa::tl::Context& ctx, pugi::xml_attribute attr) :
    Object(ctx),
    attr_(attr)
{
}

const sa::tl::MetaTable& Attribute::GetMeta() const
{
    if (!meta_)
        meta_ = ctx_.GetMetatable<Attribute>();
    SATL_ASSERT(meta_);
    return *meta_;
}

sa::tl::Value Attribute::GetNextAttribute()
{
    return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetMeta(), ctx_, attr_.next_attribute());
}

sa::tl::Value Attribute::GetPreviousAttribute()
{
    return sa::tl::bind::CreateInstance<Attribute>(ctx_, GetMeta(), ctx_, attr_.previous_attribute());
}

std::string Attribute::GetName() const
{
    return attr_.name();
}

void Attribute::SetName(const std::string& value)
{
    attr_.set_name(value.c_str());
}

std::string Attribute::GetValue() const
{
    return attr_.value();
}

void Attribute::SetValue(const std::string& value)
{
    attr_.set_value(value.c_str());
}

bool Attribute::GetEmpty() const
{
    return attr_.empty();
}

sa::tl::Value Attribute::OpNot() const
{
    return attr_.empty();
}

} // namespace xml

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<xml::Attribute>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<xml::Attribute, sa::tl::bind::Object>();
    result
        .AddFunction<xml::Attribute, sa::tl::Value>("get_next_attribute", &xml::Attribute::GetNextAttribute)
        .AddFunction<xml::Attribute, sa::tl::Value>("get_previous_attribute", &xml::Attribute::GetPreviousAttribute)

        .AddProperty("name", &xml::Attribute::GetName, &xml::Attribute::SetName)
        .AddProperty("value", &xml::Attribute::GetValue, &xml::Attribute::SetValue)
        .AddProperty("empty", &xml::Attribute::GetEmpty)

        .AddOperator("!operator", &xml::Attribute::OpNot);
    return result;
}

}
