/**
 * Copyright 2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <object.h>
#include <pugixml.hpp>

namespace xml {

class Attribute final : public sa::tl::bind::Object
{
    friend class Node;
    SATL_NON_COPYABLE(Attribute);
    SATL_NON_MOVEABLE(Attribute);
    TYPED_OBJECT(Attribute);
public:
    Attribute(sa::tl::Context& ctx, pugi::xml_attribute attr);
    sa::tl::Value GetNextAttribute();
    sa::tl::Value GetPreviousAttribute();
    std::string GetName() const;
    void SetName(const std::string& value);
    std::string GetValue() const;
    void SetValue(const std::string& value);
    bool GetEmpty() const;
    sa::tl::Value OpNot() const;
private:
    const sa::tl::MetaTable& GetMeta() const;
    mutable const sa::tl::MetaTable* meta_{ nullptr };
    pugi::xml_attribute attr_;
};

} // namespace xml

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<xml::Attribute>(sa::tl::Context& ctx);
}
