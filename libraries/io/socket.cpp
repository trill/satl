/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "socket.h"
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/un.h>
#include <cstring>
#include <ast.h>
#include <satl_assert.h>

namespace io {

struct Addr
{
    std::string name;
    int port = 0;
};

static Addr GetAddr(struct sockaddr* a, socklen_t len)
{
    if (len == sizeof(sockaddr_in))
    {
        Addr result;
        struct sockaddr_in* ai = (struct sockaddr_in*)a;
        result.name = inet_ntoa(ai->sin_addr);
        result.port = ai->sin_port;
        return result;
    }
    if (len == sizeof(sockaddr_un))
    {
        Addr result;
        struct sockaddr_un* ai = (struct sockaddr_un*)a;
        result.name = ai->sun_path;
        return result;
    }
    return {};
}

static std::string GetError()
{
    return std::strerror(errno);
}

Socket::Socket(sa::tl::Context& ctx) :
    ctx_(ctx)
{ }

Socket::~Socket()
{
    if (fd_)
        Shutdown();
}

void Socket::SetSockOpt(int level, int name, const sa::tl::Value& value)
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return;
    }
    if (value.IsInt())
    {
        int v = (int)value.ToInt();
        if (setsockopt(fd_, level, name, &v, sizeof(v)) != 0)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
        }
        return;
    }
    if (value.IsBool())
    {
        int v = value.ToBool() ? 1 : 0;
        if (setsockopt(fd_, level, name, &v, sizeof(v)) != 0)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
        }
        return;
    }
    ctx_.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Wrong type");
}

sa::tl::Value Socket::GetSockOpt(int level, int name)
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return {};
    }

    int val = 0;
    socklen_t size = sizeof(val);
    if (getsockopt(fd_, level, name, &val, &size) < 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
        return {};
    }
    return { val };
}

void Socket::Shutdown()
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return;
    }
    if (shutdown(fd_, SHUT_RDWR) == -1)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
        return;
    }
    close(fd_);
    fd_ = 0;
}

bool Socket::Create(int domain, int type, int protocol)
{
    domain_ = domain;
    type_ = type;
    fd_ = socket(domain, type, protocol);
    if (fd_ == 0)
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());

    return fd_ != 0;
}

void Socket::Bind(const std::string& address, int port)
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return;
    }

    struct sockaddr_in addIn = {};
    struct sockaddr_un addUn = {};
    struct sockaddr* addr = nullptr;
    size_t len = 0;
    address_ = address;
    port_ = port;

    if (domain_ == AF_INET)
    {
        addr = (struct sockaddr*)&addIn;
        addIn.sin_family = domain_;
        if (address.empty())
            addIn.sin_addr.s_addr = INADDR_ANY;
        else
            addIn.sin_addr.s_addr = inet_addr(address.c_str());
        addIn.sin_port = htons(port);
        len = sizeof(addIn);
    }
    else if (domain_ == AF_UNIX)
    {
        if (address.empty())
        {
            ctx_.RuntimeError(sa::tl::Error::Code::InvalidArgument, "Address can not be empty for a UNIX socket");
            return;
        }

        addr = (struct sockaddr*)&addUn;
        addUn.sun_family = domain_;
        strncpy(addUn.sun_path, address.c_str(), address.length());
        len = sizeof(addUn);
    }
    else
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Unknown domain {}", domain_));
        return;
    }

    if (bind(fd_, addr, len) < 0)
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
}

sa::tl::Value Socket::RecvFrom()
{
    struct sockaddr_in cliAddIn = {};
    struct sockaddr_un cliAaddUn = {};
    struct sockaddr* cliAddr = nullptr;
    socklen_t cliAddlen = 0;
    if (domain_ == AF_INET)
    {
        cliAddr = (struct sockaddr*)&cliAddIn;
        cliAddlen = sizeof(cliAddIn);
    }
    else
    {
        cliAddr = (struct sockaddr*)&cliAaddUn;
        cliAddlen = sizeof(cliAaddUn);
    }

    char buffer[1024];
    ssize_t recsize = recvfrom(fd_, (void*)buffer, sizeof(buffer), 0, cliAddr, &cliAddlen);
    if (recsize > -1)
    {
        auto result = sa::tl::MakeTable();
        Addr ca = GetAddr(cliAddr, cliAddlen);
        result->Add("address", sa::tl::MakeValue(ca.name));
        result->Add("port", sa::tl::MakeValue(ca.port));
        result->Add("content", sa::tl::MakeValue(sa::tl::MakeString(buffer, (size_t)recsize)));
        return result;
    }

    ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
    return {};
}

sa::tl::Value Socket::Accept()
{
    const auto* sockMeta = ctx_.GetMetatable<io::Socket>();
    SATL_ASSERT(sockMeta);

    struct sockaddr_in cliAddIn = {};
    struct sockaddr_un cliAaddUn = {};
    struct sockaddr* cliAddr = nullptr;
    socklen_t cliAddlen = 0;
    if (domain_ == AF_INET)
    {
        cliAddr = (struct sockaddr*)&cliAddIn;
        cliAddlen = sizeof(cliAddIn);
    }
    else
    {
        cliAddr = (struct sockaddr*)&cliAaddUn;
        cliAddlen = sizeof(cliAaddUn);
    }

    int newSockFd = accept(fd_, cliAddr, &cliAddlen);
    if (newSockFd > -1)
    {
        auto* sock = ctx_.GetGC().CreateObject<io::Socket>(ctx_, newSockFd);
        Addr a = GetAddr(cliAddr, cliAddlen);
        sock->address_ = a.name;
        sock->port_ = a.port;
        sa::tl::FunctionArguments args;
        return sa::tl::MakeObject(*sockMeta, sock);
    }
    ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
    return {};
}

void Socket::Listen()
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return;
    }

    if (listen(fd_, 0) < 0)
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
}

void Socket::Connect(const std::string& address, int port)
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return;
    }
    struct sockaddr_in addIn = {};
    struct sockaddr_un addUn = {};
    struct sockaddr* addr = nullptr;
    size_t len = 0;

    address_ = address;
    port_ = port;
    if (domain_ == AF_INET)
    {
        addr = (struct sockaddr*)&addIn;
        addIn.sin_family = domain_;
        if (address.empty())
            addIn.sin_addr.s_addr = INADDR_ANY;
        else
            addIn.sin_addr.s_addr = inet_addr(address.c_str());
        addIn.sin_port = htons(port);

        if (inet_pton(domain_, address.c_str(), &addIn.sin_addr) <= 0)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
            return;
        }
        len = sizeof(addIn);
    }
    else if (domain_ == AF_UNIX)
    {
        if (address.empty())
        {
            ctx_.RuntimeError(sa::tl::Error::Code::InvalidArgument, "Address can not be empty for a UNIX socket");
            return;
        }

        addr = (struct sockaddr*)&addUn;
        addUn.sun_family = domain_;
        strncpy(addUn.sun_path, address.c_str(), address.length());
        len = sizeof(addUn);
    }
    else
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "Unknown domain");
        return;
    }

    if (connect(fd_, addr, len) < 0)
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
}

int Socket::Send(const sa::tl::Value& value) const
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return -1;
    }
    std::string v = value.ToString();
    auto res = send(fd_, v.data(), v.length(), 0);
    if (res < 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
        return -1;
    }
    return (int)res;
}

int Socket::SendTo(const std::string& address, int port, const sa::tl::Value& value)
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return -1;
    }
    if (type_ != SOCK_DGRAM)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "This is not an UDP socket");
        return -1;
    }
    if (address.empty())
    {
        ctx_.RuntimeError(sa::tl::Error::Code::InvalidArgument, "Address can not be empty");
        return -1;
    }

    struct sockaddr_in addIn = {};
    struct sockaddr_un addUn = {};
    struct sockaddr* addr = nullptr;
    size_t len = 0;

    if (domain_ == AF_INET)
    {
        addr = (struct sockaddr*)&addIn;
        addIn.sin_family = domain_;
        if (address.empty())
            addIn.sin_addr.s_addr = INADDR_ANY;
        else
            addIn.sin_addr.s_addr = inet_addr(address.c_str());
        addIn.sin_port = htons(port);

        if (inet_pton(domain_, address.c_str(), &addIn.sin_addr) <= 0)
        {
            ctx_.RuntimeError(sa::tl::Error::Code::OSError, GetError());
            return -1;
        }
        len = sizeof(addIn);
    }
    else if (domain_ == AF_UNIX)
    {
        addr = (struct sockaddr*)&addUn;
        addUn.sun_family = domain_;
        strncpy(addUn.sun_path, address.c_str(), address.length());
        len = sizeof(addUn);
    }
    else
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Unknown domain ", domain_));
        return -1;
    }

    std::string v = value.ToString();
    ssize_t bytesSent = sendto(fd_, v.data(), v.length(), 0, addr, len);
    return (int)bytesSent;
}

sa::tl::Integer Socket::Read(const sa::tl::StringPtr& result, sa::tl::Integer size) const
{
    if (fd_ == 0)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "File descriptor is 0");
        return -1;
    }
    bool autosize = false;
    if (result->size() < static_cast<size_t>(size))
    {
        autosize = true;
        result->resize(static_cast<size_t>(size), '\0');
    }
    ssize_t bytesRead = read(fd_, result->data(), size);
    if (autosize)
        result->resize(bytesRead);
    return (int)bytesRead;
}

} // namespace io
