/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "../io_utils.h"

TEST_CASE("ParseIP", "[io][ip]")
{
    auto ip = io::ParseIP("127.0.0.1");
    REQUIRE(ip == 0x7f000001);
    auto ip2 = io::ParseIP("1.2.3.4");
    REQUIRE(ip2 == 0x01020304);
    auto ip3 = io::ParseIP("");
    REQUIRE(ip3 == -1);
}

TEST_CASE("FormatIP", "[io][ip]")
{
    auto ip = io::FormatIP(0x7f000001);
    REQUIRE(ip == "127.0.0.1");
    auto ip2 = io::FormatIP(0x01020304);
    REQUIRE(ip2 == "1.2.3.4");
}

TEST_CASE("gethostbyname", "[io]")
{
    auto host = io::GetHostByName("localhost");
    REQUIRE(host == "127.0.0.1");
}
