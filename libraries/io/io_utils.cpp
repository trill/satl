/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "io_utils.h"
#include <arpa/inet.h>
#include <netdb.h>

namespace io {

std::string FormatIP(sa::tl::Integer ip)
{
    uint32_t _ip = htonl(ip);
    char res[16] = {};
    inet_ntop(AF_INET, &_ip, res, sizeof(res));
    return res;
}

sa::tl::Integer ParseIP(const std::string& ip)
{
    // https://github.com/jart/cosmopolitan/blob/398f0c16fbdcd356bdfc14111434b0f0f8de7de5/net/http/parseip.c
    if (ip.empty())
        return -1;
    sa::tl::Integer result = 0;
    int b = 0, c = 0;
    for (char i : ip)
    {
        c = i & 255;
        if (isdigit(c))
        {
            b *= 10;
            b += c - '0';
        }
        else if (c == '.')
        {
            if (b > 255)
            {
                return -1;
            }
            result <<= 8;
            result |= b;
            b = 0;
        }
        else
        {
            return -1;
        }
    }
    result <<= 8;
    result |= b;
    return result;
}

std::string GetHostByName(const std::string& name)
{
    struct hostent *host_entry = gethostbyname(name.c_str());
    if (host_entry == nullptr)
        return "";
    return { inet_ntoa(*((struct in_addr*)host_entry->h_addr_list[0])) };
}

}
