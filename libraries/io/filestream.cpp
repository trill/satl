/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "filestream.h"

namespace io {

FileStream::~FileStream()
{
    if (stream_.is_open())
    {
        stream_.flush();
        stream_.close();
    }
}

bool FileStream::IsGood() const
{
    return stream_.good();
}

sa::tl::Integer FileStream::GetSize() const
{
    auto pos = stream_.tellg();
    stream_.seekg(std::fstream::end);
    auto result = stream_.tellg();
    stream_.seekg(pos);
    return result;
}

sa::tl::Integer FileStream::Write(const sa::tl::Value& content)
{
    std::string v = content.ToString();
    size_t length = v.length();
    auto pos = GetPos();
    stream_.write(v.data(), (std::streamsize)length);
    return GetPos() - pos;
}

sa::tl::Integer FileStream::Read(const sa::tl::StringPtr& result, sa::tl::Integer length)
{
    bool autosize = false;
    if (result->size() < static_cast<size_t>(length))
    {
        autosize = true;
        result->resize(static_cast<size_t>(length), '\0');
    }
    stream_.read(result->data(), length);
    auto read = stream_.gcount();
    if (autosize)
        result->resize(read);
    return read;
}

std::string FileStream::ReadAll()
{
    std::string result;
    auto size = GetSize();
    stream_.seekg(std::fstream::beg);
    result.resize(size);
    stream_.read(result.data(), size);
    return result;
}

std::string FileStream::GetLine()
{
    std::string result;
    std::getline(stream_, result);
    return result;
}

void FileStream::PutLine(const sa::tl::Value& value)
{
    stream_ << value << std::endl;
}

void FileStream::PutChar(sa::tl::Integer value)
{
    char c = static_cast<char>(std::clamp<sa::tl::Integer>(value, 0, 255));
    stream_.write(&c, 1);
}

sa::tl::Integer FileStream::GetChar()
{
    if (Eof())
        return 0;
    char value = 0;
    stream_.read(&value, 1);
    return value;
}

sa::tl::Integer FileStream::GetPos() const
{
    return stream_.tellg();
}

void FileStream::SetPos(sa::tl::Integer pos)
{
    stream_.seekp(pos);
}

bool FileStream::Eof() const
{
    return stream_.eof();
}

void FileStream::Flush()
{
    stream_.flush();
}

FileStream* FileStream::OpShl(const sa::tl::Value& rhs)
{
    stream_ << rhs.Stringify(ctx_);
    return this;
}

} // namespace io
