/**
 * Copyright 2022-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <fstream>
#include <string>
#include <context.h>
#include <non_copyable.h>
#include <value.h>

namespace io {

class FileStream
{
    SATL_NON_COPYABLE(FileStream);
    SATL_NON_MOVEABLE(FileStream);
public:
    FileStream(sa::tl::Context& ctx, const std::string& name, sa::tl::Integer mode) :
        ctx_(ctx),
        stream_(name, static_cast<std::ios_base::openmode>(mode))
    {}
    ~FileStream();
    bool IsGood() const;
    sa::tl::Integer GetSize() const;
    sa::tl::Integer Write(const sa::tl::Value& content);
    sa::tl::Integer Read(const sa::tl::StringPtr& result, sa::tl::Integer length);
    std::string ReadAll();
    std::string GetLine();
    void PutLine(const sa::tl::Value& value);
    void PutChar(sa::tl::Integer value);
    sa::tl::Integer GetChar();
    sa::tl::Integer GetPos() const;
    void Flush();
    void SetPos(sa::tl::Integer pos);
    bool Eof() const;
    FileStream* OpShl(const sa::tl::Value& rhs);
private:
    sa::tl::Context& ctx_;
    mutable std::fstream stream_;
};

} // namespace io
