/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <iostream>
#include <arpa/inet.h>
#include <context.h>
#include <module.h>
#include <sstream>
#include <netdb.h>
#include <cstring>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include "filestream.h"
#include "socket.h"
#include "io_utils.h"

using namespace sa::tl;
extern "C" bool Init(Context& ctx);

class StdOStream
{
public:
    explicit StdOStream(Context& ctx, std::ostream& stream) :
        ctx_(ctx),
        stream_(stream)
    {}
    Integer Write(const Value& content)
    {
        std::string v = content.ToString();
        size_t length = v.length();
        stream_.write(v.data(), (std::streamsize)length);
        return (Integer)length;
    }
    void PutLine(const Value& value)
    {
        stream_ << value << std::endl;
    }
    void Flush()
    {
        stream_.flush();
    }
    StdOStream* OpShl(const Value& rhs)
    {
        stream_ << rhs.Stringify(ctx_);
        return this;
    }
private:
    Context& ctx_;
    std::ostream& stream_;
};
class StdIStream
{
public:
    explicit StdIStream(std::istream& stream) : stream_(stream) {}
    Integer Read(StringPtr result, Integer length)
    {
        if (result->size() < static_cast<size_t>(length))
            result->resize(static_cast<size_t>(length));
        stream_.read(result->data(), length);
        return stream_.gcount();
    }
    std::string ReadAll()
    {
        std::string line;
        std::stringstream ss;
        while (std::getline(stream_, line))
        {
            ss << line << '\n';
        }
        return ss.str();
    }
    std::string GetLine()
    {
        std::string result;
        std::getline(stream_, result);
        return result;
    }
private:
    std::istream& stream_;
};

extern "C" bool Init(Context& ctx)
{
    if (ctx.HasValue("io"))
        return true;
    if (ctx.HasMetatable<io::FileStream>())
        return false;

    sa::tl::MetaTable& osMeta = ctx.AddMetatable<StdOStream>();
    osMeta
        .AddFunction<StdOStream, Integer, const Value&>("write", &StdOStream::Write)
        .AddFunction<StdOStream, void, const Value&>("put_line", &StdOStream::PutLine)
        .AddFunction<StdOStream, void>("flush", &StdOStream::Flush)
        .AddOperator("operator<<", &StdOStream::OpShl);

    sa::tl::MetaTable& isMeta = ctx.AddMetatable<StdIStream>();
    isMeta
        .AddFunction<StdIStream, Integer, StringPtr, Integer>("read", &StdIStream::Read)
        .AddFunction<StdIStream, std::string>("read_all", &StdIStream::ReadAll)
        .AddFunction<StdIStream, std::string>("get_line", &StdIStream::GetLine);

    sa::tl::MetaTable& fileMeta = ctx.AddMetatable<io::FileStream>();
    fileMeta
        .AddFunction<io::FileStream, Integer, const Value&>("write", &io::FileStream::Write, false)
        .AddFunction<io::FileStream, Integer, const StringPtr&, Integer>("read", &io::FileStream::Read)
        .AddFunction<io::FileStream, std::string>("read_all", &io::FileStream::ReadAll)
        .AddFunction<io::FileStream, std::string>("get_line", &io::FileStream::GetLine)
        .AddFunction<io::FileStream, void, const Value&>("put_line", &io::FileStream::PutLine, false)
        .AddFunction<io::FileStream, void, Integer>("put_char", &io::FileStream::PutChar, false)
        .AddFunction<io::FileStream, Integer>("get_char", &io::FileStream::GetChar)
        .AddFunction<io::FileStream, void>("flush", &io::FileStream::Flush, false)
        .AddProperty("size", &io::FileStream::GetSize)
        .AddProperty("pos", &io::FileStream::GetPos, &io::FileStream::SetPos)
        .AddProperty("eof", &io::FileStream::Eof)
        .AddOperator("operator<<", &io::FileStream::OpShl, false);

    sa::tl::MetaTable& socketMeta = ctx.AddMetatable<io::Socket>();
    socketMeta
        .AddFunction<io::Socket, void>("listen", &io::Socket::Listen, false)
        .AddFunction<io::Socket, void, const std::string&, int>("bind", &io::Socket::Bind, false)
        .AddFunction<io::Socket, void, const std::string&, int>("connect", &io::Socket::Connect, false)
        .AddFunction<io::Socket, sa::tl::Value>("accept", &io::Socket::Accept, false)
        .AddFunction<io::Socket, int, const sa::tl::Value&>("send", &io::Socket::Send, false)
        .AddFunction<io::Socket, int, const std::string&, int, const sa::tl::Value&>("sendto", &io::Socket::SendTo, false)
        .AddFunction<io::Socket, sa::tl::Integer, const sa::tl::StringPtr&, sa::tl::Integer>("read", &io::Socket::Read, false)
        .AddFunction<io::Socket, sa::tl::Value>("recvfrom", &io::Socket::RecvFrom, false)
        .AddFunction<io::Socket, void>("shutdown", &io::Socket::Shutdown, false)
        .AddFunction<io::Socket, void, int, int, const sa::tl::Value&>("setsockopt", &io::Socket::SetSockOpt, false)
        .AddFunction<io::Socket, sa::tl::Value, int, int>("getsockopt", &io::Socket::GetSockOpt)
        .AddProperty("domain", &io::Socket::GetDomain)
        .AddProperty("type", &io::Socket::GetType)
        .AddProperty("address", &io::Socket::GetAddress)
        .AddProperty("port", &io::Socket::GetPort)
        .AddProperty("fd", &io::Socket::GetFd);

    static StdOStream outStream(ctx, std::cout);
    static StdOStream errStream(ctx, std::cerr);
    static StdIStream inStream(std::cin);

    auto module = MakeModule();

    module->SetInstance<StdOStream>(osMeta, "stdout", outStream);
    module->SetInstance<StdOStream>(osMeta, "stderr", errStream);
    module->SetInstance<StdIStream>(isMeta, "stdin", inStream);

    module->AddConst<Integer>("MODE_APP", std::ios_base::app);
    module->AddConst<Integer>("MODE_BINARY", std::ios_base::binary);
    module->AddConst<Integer>("MODE_IN", std::ios_base::in);
    module->AddConst<Integer>("MODE_OUT", std::ios_base::out);
    module->AddConst<Integer>("MODE_ATE", std::ios_base::ate);
    module->AddConst<Integer>("MODE_TRUNC", std::ios_base::trunc);

    module->AddConst<Integer>("AF_INET", AF_INET);
    module->AddConst<Integer>("AF_INET6", AF_INET6);
    module->AddConst<Integer>("AF_UNIX", AF_UNIX);
    module->AddConst<Integer>("AF_PACKET", AF_PACKET);

    module->AddConst<Integer>("IPPROTO_TCP", IPPROTO_TCP);
    module->AddConst<Integer>("IPPROTO_UDP", IPPROTO_UDP);
    module->AddConst<Integer>("IPPROTO_SCTP", IPPROTO_SCTP);

    module->AddConst<Integer>("SO_BROADCAST", SO_BROADCAST);
    module->AddConst<Integer>("SO_KEEPALIVE", SO_KEEPALIVE);
    module->AddConst<Integer>("SO_REUSEADDR", SO_REUSEADDR);
    module->AddConst<Integer>("TCP_NODELAY", TCP_NODELAY);

    module->AddConst<Integer>("SOCK_STREAM", SOCK_STREAM);
    module->AddConst<Integer>("SOCK_DGRAM", SOCK_DGRAM);
    module->AddConst<Integer>("SOCK_SEQPACKET", SOCK_SEQPACKET);
    module->AddConst<Integer>("SOCK_RAW", SOCK_RAW);

    // setsockopt/getsockopt level
    module->AddConst<Integer>("SOL_SOCKET", SOL_SOCKET);
    module->AddConst<Integer>("IPPROTO_TCP", IPPROTO_TCP);

    module->AddFunction<Value, std::string, Integer>("open",
        [&ctx, &fileMeta](const std::string& nameV, Integer modeV) -> Value
        {
            // open(name, mode) -> file
            auto* file = ctx.GetGC().CreateObject<io::FileStream>(ctx, nameV, modeV);
            if (!file->IsGood())
            {
                ctx.RuntimeError(Error::Code::UserDefined, std::format("Could not open file \"{}\"", nameV));
                ctx.GetGC().Remove(file);
                return {};
            }
            return MakeObject(fileMeta, file);
        });
    module->AddFunction<void, sa::tl::ObjectPtr>("close",
        [&ctx, &fileMeta, &socketMeta](const sa::tl::ObjectPtr& fileObj) -> void
        {
            // close(file)
            if (auto* file = fileMeta.CastTo<io::FileStream>(fileObj.get()))
                ctx.GetGC().Remove(file);
            else if (auto* sock = socketMeta.CastTo<io::Socket>(fileObj.get()))
                ctx.GetGC().Remove(sock);
            else
            {
                ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not an instance of FileStream or Socket");
                return;
            }

            ctx.ResetObject(fileObj->GetInstance());
        });
    module->AddFunction<Value, Integer, Integer, Integer>("socket",
        [&ctx, &socketMeta](Integer domain, Integer type, Integer proto) -> Value
        {
            // socket(domain, type) -> socket
            auto* sock = ctx.GetGC().CreateObject<io::Socket>(ctx);
            if (!sock->Create((int)domain, (int)type, (int)proto))
            {
                ctx.GetGC().Remove(sock);
                return {};
            }
            return MakeObject(socketMeta, sock);
        }, false);
    module->AddFunction<std::string, std::string>("gethostbyname",
        [&ctx](const std::string& hostname) -> std::string
        {
            std::string result = io::GetHostByName(hostname);
            if (result.empty())
                ctx.RuntimeError(sa::tl::Error::Code::OSError, "gethostbyname() failed");
            return result;
        }, false);
    module->AddFunction<std::string, Integer>("format_ip", &io::FormatIP);
    module->AddFunction<Integer, std::string>("parse_ip",
        [&ctx](const std::string& ip) -> Integer
        {
            Integer result = io::ParseIP(ip);
            if (result == -1)
                ctx.RuntimeError(Error::Code::InvalidArgument, "Malformed IP address");
            return result;
        });

    ctx.AddConst("io", std::move(module), SCOPE_LIBRARY);
    return true;
}
