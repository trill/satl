/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <value.h>
#include <string>

namespace io {

std::string FormatIP(sa::tl::Integer ip);
sa::tl::Integer ParseIP(const std::string& ip);
std::string GetHostByName(const std::string& name);

}
