/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sys/socket.h>
#include <netinet/in.h>
#include <non_copyable.h>
#include <string>
#include <node.h>
#include <context.h>

namespace io {

class Socket
{
    SATL_NON_COPYABLE(Socket);
    SATL_NON_MOVEABLE(Socket);
public:
    explicit Socket(sa::tl::Context& ctx);
    Socket(sa::tl::Context& ctx, int fd) :
        ctx_(ctx),
        fd_(fd)
    {}
    ~Socket();
    void Shutdown();

    bool Create(int domain, int type, int protocol);
    void SetSockOpt(int level, int name, const sa::tl::Value& value);
    sa::tl::Value GetSockOpt(int level, int name);
    void Listen();
    void Bind(const std::string& address, int port);
    sa::tl::Value Accept();

    void Connect(const std::string& address, int port);

    [[nodiscard]] int Send(const sa::tl::Value& value) const;
    int SendTo(const std::string& address, int port, const sa::tl::Value& value);
    [[nodiscard]] sa::tl::Integer Read(const sa::tl::StringPtr& result, sa::tl::Integer size) const;
    sa::tl::Value RecvFrom();

    [[nodiscard]] int GetDomain() const { return domain_; }
    [[nodiscard]] int GetType() const { return type_; }
    [[nodiscard]] std::string GetAddress() const { return address_; }
    [[nodiscard]] int GetPort() const { return port_; }
    [[nodiscard]] int GetFd() const { return fd_; }

    std::string address_;
    int port_{ 0 };
private:
    sa::tl::Context& ctx_;
    int fd_{ 0 };
    int domain_{ 0 };
    int type_{ 0 };
};

} // namespace io
