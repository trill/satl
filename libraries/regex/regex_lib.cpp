/**
 * Copyright (c) 2021-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <regex>
#include <context.h>
#include <module.h>

using namespace sa::tl;

extern "C" bool Init(Context& ctx);
extern "C" bool Init(Context& ctx)
{
    if (ctx.HasValue("regex"))
        return true;
    auto module = MakeModule();
    module->AddFunction("match",
        [](Context& ctx, const FunctionArguments& args) -> ValuePtr
        {
            // regex_match(regex, subject[, exact = false]) -> bool | int
            // return false if not matched or position of match
            if (args.size() < 2)
            {
                ctx.RuntimeError(Error::Code::ArgumentsMismatch, "Wrong number of arguments");
                return MakeNull();
            }
            if (!args[0]->IsString() || !args[1]->IsString())
            {
                ctx.RuntimeError(Error::Code::TypeMismatch, "String expected");
                return MakeNull();
            }

            const auto regexV = args[0]->ToString();
            const auto subjectV = args[1]->ToString();
            bool exact = args.size() > 2 ? args[2]->ToBool() : false;
            try
            {
                std::regex re(regexV);
                auto match = std::smatch{};
                if (exact)
                {
                    bool result = std::regex_match(subjectV, match, re);
                    if (!result)
                        return MakeValue(false);
                    return MakeValue(match.prefix().length());
                }
                bool result = std::regex_search(subjectV, match, re);
                if (!result)
                    return MakeValue(false);
                return MakeValue(match.prefix().length());
            }
            catch (const std::regex_error& ex)
            {
                ctx.RuntimeError(ex.code(), ex.what());
                return MakeNull();
            }
        });
    module->AddFunction<Value, std::string, std::string>("matches",
        [&ctx](const std::string& regex, const std::string& subject) -> Value
        {
            // regex_matches(regex, subject) -> table
            // Get a list of matches
            try
            {
                std::regex re(regex);

                auto result = MakeTable();
                Integer i = 0;
                for (auto it = std::sregex_iterator(subject.begin(), subject.end(), re); it != std::sregex_iterator();
                     ++it)
                {
                    auto item = MakeTable();
                    item->Add("str", MakeValue(it->str()));
                    item->Add("pos", MakeValue<Integer>(it->position()));
                    item->Add("length", MakeValue<Integer>(it->length()));
                    result->Add(i, MakeValue(std::move(item)));
                    ++i;
                }
                return result;
            }
            catch (const std::regex_error& ex)
            {
                ctx.RuntimeError(ex.code(), ex.what());
                return {};
            }
        });
    module->AddFunction<Value, std::string, std::string, std::string>("replace",
        [&ctx](const std::string& regex, const std::string& subject, const std::string& format) -> Value
        {
            // regex_replace(regex, subject, format) -> string
            // https://en.cppreference.com/w/cpp/regex/regex_replace
            try
            {
                std::regex re(regex);
                return MakeString(std::regex_replace(subject, re, format));
            }
            catch (const std::regex_error& ex)
            {
                ctx.RuntimeError(ex.code(), ex.what());
                return {};
            }
        });
    ctx.AddConst("regex", std::move(module), SCOPE_LIBRARY);
    return true;
}
