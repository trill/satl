project (satl_regex CXX)

file(GLOB SOURCES *.cpp *.h)

add_library(satl_regex SHARED ${SOURCES})
set_property(TARGET satl_regex PROPERTY POSITION_INDEPENDENT_CODE ON)
target_link_libraries(satl_regex satl)
install(TARGETS satl_regex LIBRARY)
