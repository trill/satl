/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <httplib.h>
#include <memory>
#include <object.h>

namespace http {

class Server : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Server);
    SATL_NON_MOVEABLE(Server);
    TYPED_OBJECT(Server);
public:
    explicit Server(sa::tl::Context& ctx);
    ~Server() override = default;
    void Listen(std::string host, uint16_t port);
    void Stop();
    [[nodiscard]] bool IsRunning() const
    {
        if (!server_)
            return false;
        return server_->is_running();
    }
    void Get(const std::string& route, sa::tl::CallablePtr handler);
    void Post(const std::string& route, sa::tl::CallablePtr handler);
    void Put(const std::string& route, sa::tl::CallablePtr handler);
    void Patch(const std::string& route, sa::tl::CallablePtr handler);
    void Delete(const std::string& route, sa::tl::CallablePtr handler);
    void Options(const std::string& route, sa::tl::CallablePtr handler);
    void SetErrorHandler(sa::tl::CallablePtr handler);
    void SetExceptionHandler(sa::tl::CallablePtr handler);
    void SetLoggerHandler(sa::tl::CallablePtr handler);
    [[nodiscard]] std::string GetCertPath() const { return certPath_; }
    void SetCertPath(std::string value) { certPath_ = std::move(value); }
    [[nodiscard]] std::string GetPrivateKeyPath() const { return privateKey_; }
    void SetPrivateKeyPath(std::string value) { privateKey_ = std::move(value); }
    [[nodiscard]] std::string GetHost() const { return host_; }
    [[nodiscard]] uint16_t GetPort() const { return port_; }
    void SetMountPoint(const std::string& mount, const std::string& path);
    void RemoveMountPoint(const std::string& mount);
    [[nodiscard]] sa::tl::TablePtr GetDefaultHeader() const;
    void SetDefaultHeaders(sa::tl::TablePtr headers);
    [[nodiscard]] bool IsSSL() const { return ssl_; }
    void SetSSL(bool value);
private:
    enum class Handler
    {
        Get,
        Post,
        Put,
        Patch,
        Delete,
        Options,
        Error,
    };
    void SetHandler(Handler type, const std::string& route, sa::tl::CallablePtr handler);
    [[nodiscard]] httplib::Server* GetServer();
    bool ssl_{ false };
    std::string certPath_;
    std::string privateKey_;
    std::unique_ptr<httplib::Server> server_;
    std::string host_;
    uint16_t port_{ 0 };
    sa::tl::TablePtr defaultHeader_;
};

} // namespace http

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<http::Server>(sa::tl::Context& ctx);
}
