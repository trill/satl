/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <object.h>
#include <httplib.h>

namespace http {

class Request : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Request);
    SATL_NON_MOVEABLE(Request);
    TYPED_OBJECT(Request);
public:
    Request(sa::tl::Context& ctx, const httplib::Request& request);
    ~Request() override = default;
    [[nodiscard]] bool HasHeader(const std::string& name) const;
    [[nodiscard]] std::string GetHeader(const std::string& name) const;
    [[nodiscard]] bool HasParam(const std::string& name) const;
    [[nodiscard]] std::string GetParam(const std::string& name) const;
    [[nodiscard]] std::string GetPath() const;
    [[nodiscard]] std::string GetMethod() const;
    [[nodiscard]] bool HasFile(const std::string& name) const;
    [[nodiscard]] std::string GetFileName(const std::string& name) const;
    [[nodiscard]] std::string GetFileContent(const std::string& name) const;
    [[nodiscard]] std::string GetFileFilename(const std::string& name) const;
    [[nodiscard]] std::string GetFileContentType(const std::string& name) const;
private:
    const httplib::Request& request_;
};

} // namespace http

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<http::Request>(sa::tl::Context& ctx);
}
