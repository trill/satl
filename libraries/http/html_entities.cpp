/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "html_entities.h"
#include <string_view>
#include <sstream>
#include <map>
#include <utf8.h>

namespace http {

using std::literals::string_view_literals::operator""sv;

// All named HTML entities according to https://www.freeformatter.com/html-entities.html
static const std::map<std::string_view, uint32_t> LookupTable = {
    // ASCII Characters
    { "amp"sv,     '&' },
    { "lt"sv,      '<' },
    { "gt"sv,      '>' },
    { "quot"sv,    '"' },
    // ISO-8859-1 Characters
    { "Agrave"sv, u'À' },
    { "Aacute"sv, u'Á' },
    { "Acirc"sv,  u'Â' },
    { "Atilde"sv, u'Ã' },
    { "Auml"sv,   u'Ä' },
    { "Aring"sv,  u'Å' },
    { "AElig"sv,  u'Æ' },
    { "Ccedil"sv, u'Ç' },
    { "Egrave"sv, u'È' },
    { "Eacute"sv, u'É' },
    { "Ecirc"sv,  u'Ê' },
    { "Euml"sv,   u'Ë' },
    { "Igrave"sv, u'Ì' },
    { "Iacute"sv, u'Í' },
    { "Icirc"sv,  u'Î' },
    { "Iuml"sv,   u'Ï' },
    { "ETH"sv,    u'Ð' },
    { "Ntilde"sv, u'Ñ' },
    { "Ograve"sv, u'Ò' },
    { "Oacute"sv, u'Ó' },
    { "Ocirc"sv,  u'Ô' },
    { "Otilde"sv, u'Õ' },
    { "Ouml"sv,   u'Ö' },
    { "Oslash"sv, u'Ø' },
    { "Ugrave"sv, u'Ù' },
    { "Uacute"sv, u'Ú' },
    { "Ucirc"sv,  u'Û' },
    { "Uuml"sv,   u'Ü' },
    { "Yacute"sv, u'Ý' },
    { "THORN"sv,  u'Þ' },
    { "szlig"sv,  u'ß' },
    { "agrave"sv, u'à' },
    { "aacute"sv, u'á' },
    { "acirc"sv,  u'â' },
    { "atilde"sv, u'ã' },
    { "auml"sv,   u'ä' },
    { "aring"sv,  u'å' },
    { "aelig"sv,  u'æ' },
    { "ccedil"sv, u'ç' },
    { "egrave"sv, u'è' },
    { "eacute"sv, u'é' },
    { "ecirc"sv,  u'ê' },
    { "euml"sv,   u'ë' },
    { "igrave"sv, u'ì' },
    { "iacute"sv, u'í' },
    { "icirc"sv,  u'î' },
    { "iuml"sv,   u'ï' },
    { "eth"sv,    u'ð' },
    { "ntilde"sv, u'ñ' },
    { "ograve"sv, u'ò' },
    { "oacute"sv, u'ó' },
    { "ocirc"sv,  u'ô' },
    { "otilde"sv, u'õ' },
    { "ouml"sv,   u'ö' },
    { "oslash"sv, u'ø' },
    { "ugrave"sv, u'ù' },
    { "uacute"sv, u'ú' },
    { "ucirc"sv,  u'û' },
    { "uuml"sv,   u'ü' },
    { "yacute"sv, u'ý' },
    { "thorn"sv,  u'þ' },
    { "yuml"sv,   u'ÿ' },
    // ISO-8859-1 Symbols
    { "nbsp"sv,   u' ' },
    { "iexcl"sv,  u'¡' },
    { "cent"sv,   u'¢' },
    { "pound"sv,  u'£' },
    { "curren"sv, u'¤' },
    { "yen"sv,    u'¥' },
    { "brvbar"sv, u'¦' },
    { "sect"sv,   u'§' },
    { "uml"sv,    u'¨' },
    { "copy"sv,   u'©' },
    { "ordf"sv,   u'ª' },
    { "laquo"sv,  u'«' },
    { "not"sv,    u'¬' },
    { "reg"sv,    u'®' },
    { "macr"sv,   u'¯' },
    { "deg"sv,    u'°' },
    { "plusmn"sv, u'±' },
    { "sup2"sv,   u'²' },
    { "sup3"sv,   u'³' },
    { "acute"sv,  u'´' },
    { "micro"sv,  u'µ' },
    { "para"sv,   u'¶' },
    { "cedil"sv,  u'¸' },
    { "sup1"sv,   u'¹' },
    { "ordm"sv,   u'º' },
    { "raquo"sv,  u'»' },
    { "frac14"sv, u'¼' },
    { "frac12"sv, u'½' },
    { "frac34"sv, u'¾' },
    { "iquest"sv, u'¿' },
    { "times"sv,  u'×' },
    { "divide"sv, u'÷' },
    // Math Symbols
    { "forall"sv, u'∀' },
    { "part"sv,   u'∂' },
    { "exist"sv,  u'∃' },
    { "empty"sv,  u'∅' },
    { "nabla"sv,  u'∇' },
    { "isin"sv,   u'∈' },
    { "notin"sv,  u'∉' },
    { "ni"sv,     u'∋' },
    { "prod"sv,   u'∏' },
    { "sum"sv,    u'∑' },
    { "minus"sv,  u'−' },
    { "lowast"sv, u'∗' },
    { "radic"sv,  u'√' },
    { "prop"sv,   u'∝' },
    { "infin"sv,  u'∞' },
    { "ang"sv,    u'∠' },
    { "and"sv,    u'∧' },
    { "or"sv,     u'∨' },
    { "cap"sv,    u'∩' },
    { "cup"sv,    u'∪' },
    { "int"sv,    u'∫' },
    { "there4"sv, u'∴' },
    { "sim"sv,    u'∼' },
    { "cong"sv,   u'≅' },
    { "asymp"sv,  u'≈' },
    { "ne"sv,     u'≠' },
    { "equiv"sv,  u'≡' },
    { "le"sv,     u'≤' },
    { "ge"sv,     u'≥' },
    { "sub"sv,    u'⊂' },
    { "sup"sv,    u'⊃' },
    { "nsub"sv,   u'⊄' },
    { "sube"sv,   u'⊆' },
    { "supe"sv,   u'⊇' },
    { "oplus"sv,  u'⊕' },
    { "otimes"sv, u'⊗' },
    { "perp"sv,   u'⊥' },
    { "sdot"sv,   u'⋅' },
    // Greek Letters
    { "Alpha"sv,  u'Α' },
    { "Beta"sv,   u'Β' },
    { "Gamma"sv,  u'Γ' },
    { "Delta"sv,  u'Δ' },
    { "Epsilon"sv,u'Ε' },
    { "Zeta"sv,   u'Ζ' },
    { "Eta"sv,    u'Η' },
    { "Theta"sv,  u'Θ' },
    { "Iota"sv,   u'Ι' },
    { "Kappa"sv,  u'Κ' },
    { "Lambda"sv, u'Λ' },
    { "Mu"sv,     u'Μ' },
    { "Nu"sv,     u'Ν' },
    { "Xi"sv,     u'Ξ' },
    { "Omicron"sv,u'Ο' },
    { "Pi"sv,     u'Π' },
    { "Rho"sv,    u'Ρ' },
    { "Sigma"sv,  u'Σ' },
    { "Tau"sv,    u'Τ' },
    { "Upsilon"sv,u'Υ' },
    { "Phi"sv,    u'Φ' },
    { "Chi"sv,    u'Χ' },
    { "Psi"sv,    u'Ψ' },
    { "Omega"sv,  u'Ω' },
    { "alpha"sv,  u'α' },
    { "beta"sv,   u'β' },
    { "gamma"sv,  u'γ' },
    { "delta"sv,  u'δ' },
    { "epsilon"sv,u'ε' },
    { "zeta"sv,   u'ζ' },
    { "eta"sv,    u'η' },
    { "theta"sv,  u'θ' },
    { "iota"sv,   u'ι' },
    { "kappa"sv,  u'κ' },
    { "lambda"sv, u'λ' },
    { "mu"sv,     u'μ' },
    { "nu"sv,     u'ν' },
    { "xi"sv,     u'ξ' },
    { "omicron"sv,u'ο' },
    { "pi"sv,     u'π' },
    { "rho"sv,    u'ρ' },
    { "sigmaf"sv, u'ς' },
    { "sigma"sv,  u'σ' },
    { "tau"sv,    u'τ' },
    { "upsilon"sv,u'υ' },
    { "phi"sv,    u'φ' },
    { "chi"sv,    u'χ' },
    { "psi"sv,    u'ψ' },
    { "omega"sv,  u'ω' },
    { "thetasym"sv,u'ϑ' },
    { "upsih"sv,  u'ϒ' },
    { "piv"sv,    u'ϖ' },
    // Miscellaneous HTML entities
    { "OElig"sv,  u'Œ' },
    { "oelig"sv,  u'œ' },
    { "Scaron"sv, u'Š' },
    { "scaron"sv, u'š' },
    { "Yuml"sv,   u'Ÿ' },
    { "fnof"sv,   u'ƒ' },
    { "circ"sv,   u'ˆ' },
    { "tilde"sv,  u'˜' },
    { "ensp"sv,   u' ' },
    { "emsp"sv,   u' ' },
    { "thinsp"sv, u' ' },
    { "zwnj"sv,   u'\u200c' },
    { "zwj"sv,    u'\u200d' },
    { "lrm"sv,    u'\u200e' },
    { "rlm"sv,    u'\u200f' },
    { "ndash"sv,  u'–' },
    { "mdash"sv,  u'—' },
    { "lsquo"sv,  u'‘' },
    { "rsquo"sv,  u'’' },
    { "sbquo"sv,  u'‚' },
    { "ldquo"sv,  u'“' },
    { "rdquo"sv,  u'”' },
    { "bdquo"sv,  u'„' },
    { "dagger"sv, u'†' },
    { "Dagger"sv, u'‡' },
    { "bull"sv,   u'•' },
    { "hellip"sv, u'…' },
    { "permil"sv, u'‰' },
    { "prime"sv,  u'′' },
    { "Prime"sv,  u'″' },
    { "lsaquo"sv, u'‹' },
    { "rsaquo"sv, u'›' },
    { "oline"sv,  u'‾' },
    { "euro"sv,   u'€' },
    { "trade"sv,  u'™' },
    { "larr"sv,   u'←' },
    { "uarr"sv,   u'↑' },
    { "rarr"sv,   u'→' },
    { "darr"sv,   u'↓' },
    { "harr"sv,   u'↔' },
    { "crarr"sv,  u'↵' },
    { "lceil"sv,  u'⌈' },
    { "rceil"sv,  u'⌉' },
    { "lfloor"sv, u'⌊' },
    { "rfloor"sv, u'⌋' },
    { "loz"sv,    u'◊' },
    { "spades"sv, u'♠' },
    { "clubs"sv,  u'♣' },
    { "hearts"sv, u'♥' },
    { "diams"sv,  u'♦' },
};

static const std::map<uint32_t, std::string_view>& ReverseTable()
{
    static std::map<uint32_t, std::string_view> revTable;
    if (revTable.empty())
    {
        for (const auto& e : LookupTable)
            revTable.emplace(e.second, e.first);
    }
    return revTable;
}

static std::string_view FindEntitiy(uint32_t cp)
{
    const auto& rt = ReverseTable();
    auto it = rt.find(cp);
    if (it != rt.end())
        return it->second;
    return ""sv;
}

template<bool Entities>
static std::string Encode(const std::string& value)
{
    std::stringstream ss;
    auto codepoints = sa::utf8::Decode(value);
    for (auto c : codepoints)
    {
        if (c > 127 || c == '&' || c == '<' || c == '>')
        {
            if constexpr(Entities)
            {
                auto ent = FindEntitiy(c);
                if (!ent.empty())
                    ss << "&" << ent << ";";
                else
                    ss << "&#" << c << ";";
            }
            else
                ss << "&#" << c << ";";
        }
        else
            ss << (char)c;
    }
    return ss.str();
}

template<bool Entities>
static std::string Decode(const std::string& value)
{
    std::stringstream ss;

    auto isDigit = [](char c, bool hex) -> bool
    {
        if (hex)
            return isdigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
        return isdigit(c);
    };

    auto isnumber = [&](const std::string& v, bool hex)
    {
        for (auto c : v)
        {
            if (!isDigit(c, hex))
                return false;
        }
        return true;
    };

    auto translate = [&](std::string v) -> std::string
    {
        bool n = v.starts_with('#');
        if (n)
            v.erase(0, 1);
        int base = 10;
        // Either dec #nnn or hex #xnnn
        if (n && v.starts_with('x'))
        {
            base = 16;
            v.erase(0, 1);
        }
        if (n && isnumber(v, base == 16))
        {
            auto ival = std::stoi(v, nullptr, base);
            return sa::utf8::Encode({ static_cast<uint32_t>(ival) });
        }
        if constexpr (Entities)
        {
            auto it = LookupTable.find(v);
            if (it != LookupTable.end())
                return sa::utf8::Encode({ static_cast<uint32_t>(it->second) });
        }
        return "";
    };

    for (size_t i = 0; i < value.length(); ++i)
    {
        char c = value[i];
        if (c == '&')
        {
            auto pos = value.find(';', i + 1);
            if (pos != std::string::npos)
            {
                std::string ent = value.substr(i + 1, pos - i - 1);
                auto c = translate(ent);
                if (!c.empty())
                    ss << c;
                else
                    // Unknown
                    ss << "&" << ent << ";";
                i += ent.length() + 1;
            }
            else
                ss << c;
        }
        else
            ss << c;
    }
    return ss.str();
}

std::string XMLEncode(const std::string& value)
{
    return Encode<false>(value);
}

std::string HTMLEncode(const std::string& value)
{
    return Encode<true>(value);
}

std::string XMLDecode(const std::string& value)
{
    return Decode<false>(value);
}

std::string HTMLDecode(const std::string& value)
{
    return Decode<true>(value);
}

}
