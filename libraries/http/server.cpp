/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "server.h"
#include <ast.h>
#include "request.h"
#include "response.h"
#include <factory.h>

namespace http {

Server::Server(sa::tl::Context& ctx) :
    Object(ctx),
    defaultHeader_(sa::tl::MakeTable())
{
}

void Server::SetSSL(bool value)
{
    if (value == ssl_)
        return;
    if (server_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "Server already created");
        return;
    }
    ssl_ = value;
}

httplib::Server* Server::GetServer()
{
    if (!server_)
    {
        if (ssl_)
        {
            if (certPath_.empty())
            {
                ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "SSL Server needs a certificate path");
                return nullptr;
            }
            if (privateKey_.empty())
            {
                ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "SSL Server needs a private key path");
                return nullptr;
            }
            server_ = std::make_unique<httplib::SSLServer>(certPath_.c_str(), privateKey_.c_str());
        }
        else
            server_ = std::make_unique<httplib::Server>();
    }
    return server_.get();
}

void Server::Get(const std::string& route, sa::tl::CallablePtr handler)
{
    try
    {
        SetHandler(Handler::Get, route, std::move(handler));
    }
    catch (const std::regex_error& ex)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, ex.what());
    }
}

void Server::Post(const std::string& route, sa::tl::CallablePtr handler)
{
    try
    {
        SetHandler(Handler::Post, route, std::move(handler));
    }
    catch (const std::regex_error& ex)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, ex.what());
    }
}

void Server::Put(const std::string& route, sa::tl::CallablePtr handler)
{
    try
    {
        SetHandler(Handler::Put, route, std::move(handler));
    }
    catch (const std::regex_error& ex)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, ex.what());
    }
}

void Server::Patch(const std::string& route, sa::tl::CallablePtr handler)
{
    try
    {
        SetHandler(Handler::Patch, route, std::move(handler));
    }
    catch (const std::regex_error& ex)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, ex.what());
    }
}

void Server::Delete(const std::string& route, sa::tl::CallablePtr handler)
{
    try
    {
        SetHandler(Handler::Delete, route, std::move(handler));
    }
    catch (const std::regex_error& ex)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, ex.what());
    }
}

void Server::Options(const std::string& route, sa::tl::CallablePtr handler)
{
    try
    {
        SetHandler(Handler::Options, route, std::move(handler));
    }
    catch (const std::regex_error& ex)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, ex.what());
    }
}

void Server::SetErrorHandler(sa::tl::CallablePtr handler)
{
    SetHandler(Handler::Error, "", std::move(handler));
}

void Server::SetExceptionHandler(sa::tl::CallablePtr handler)
{
    if (handler->ParamCount() != 3)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return;
    }

    auto* server = GetServer();
    if (!server)
        return;

    auto handlerFunc = [handler = std::move(handler), this]
        (const httplib::Request& req, httplib::Response& res, const std::exception_ptr& e)
    {
        if (ctx_.stop_)
            Stop();

        auto* reqMeta = ctx_.GetMetatable<Request>();
        SATL_ASSERT(reqMeta);
        auto request = sa::tl::bind::CreateInstance<http::Request>(ctx_, *reqMeta, ctx_, req);
        sa::tl::bind::ObjectDestroyer reqDest(*static_cast<Object*>(request.As<sa::tl::ObjectPtr>()->GetInstance()));

        auto* resMeta = ctx_.GetMetatable<Response>();
        SATL_ASSERT(resMeta);
        auto response = sa::tl::bind::CreateInstance<http::Response>(ctx_, *resMeta, ctx_, res);
        sa::tl::bind::ObjectDestroyer resDest(*static_cast<Object*>(response.As<sa::tl::ObjectPtr>()->GetInstance()));

        if (defaultHeader_)
        {
            for (const auto& h : *defaultHeader_)
                res.set_header(sa::tl::KeyToString(h.first), h.second->ToString());
        }

        auto what = [](const std::exception_ptr& eptr) -> std::string
        {
            if (!eptr) { throw std::bad_exception(); }

            try { std::rethrow_exception(eptr); }
            catch (const std::exception& e) { return e.what() ; }
            catch (const std::string&    e) { return e        ; }
            catch (const char*           e) { return e        ; }
            catch (...)                     { return "Unknown"; }
        };

        (*handler)(ctx_, { sa::tl::MakeValue(request), sa::tl::MakeValue(response), sa::tl::MakeValue(sa::tl::MakeString(what(e))) });
    };
    server->set_exception_handler(handlerFunc);
}

void Server::SetLoggerHandler(sa::tl::CallablePtr handler)
{
    if (handler->ParamCount() != 1)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return;
    }

    auto* server = GetServer();
    if (!server)
        return;
    auto handlerLogger = [handler = std::move(handler), this](const httplib::Request& req, const httplib::Response&)
    {
        if (ctx_.stop_)
            Stop();

        auto* reqMeta = ctx_.GetMetatable<Request>();
        SATL_ASSERT(reqMeta);
        auto request = sa::tl::bind::CreateInstance<http::Request>(ctx_, *reqMeta, ctx_, req);
        sa::tl::bind::ObjectDestroyer reqDest(*static_cast<Object*>(request.As<sa::tl::ObjectPtr>()->GetInstance()));

        (*handler)(ctx_, { sa::tl::MakeValue(request) });
    };
    server->set_logger(handlerLogger);
}

void Server::SetHandler(Handler type, const std::string& route, sa::tl::CallablePtr handler)
{
    if (handler->ParamCount() != 2)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return;
    }

    auto* server = GetServer();
    if (!server)
        return;

    auto handlerFunc = [handler = std::move(handler), this](const httplib::Request& req, httplib::Response& res)
    {
        if (ctx_.stop_)
            Stop();

        auto* reqMeta = ctx_.GetMetatable<Request>();
        SATL_ASSERT(reqMeta);
        auto request = sa::tl::bind::CreateInstance<http::Request>(ctx_, *reqMeta, ctx_, req);
        sa::tl::bind::ObjectDestroyer reqDest(*static_cast<Object*>(request.As<sa::tl::ObjectPtr>()->GetInstance()));

        auto* resMeta = ctx_.GetMetatable<Response>();
        SATL_ASSERT(resMeta);
        auto response = sa::tl::bind::CreateInstance<http::Response>(ctx_, *resMeta, ctx_, res);
        sa::tl::bind::ObjectDestroyer resDest(*static_cast<Object*>(response.As<sa::tl::ObjectPtr>()->GetInstance()));

        if (defaultHeader_)
        {
            for (const auto& h : *defaultHeader_)
                res.set_header(sa::tl::KeyToString(h.first), h.second->ToString());
        }

        (*handler)(ctx_, { sa::tl::MakeValue(request), sa::tl::MakeValue(response) });
    };
    switch (type)
    {
    case Handler::Get:
        server->Get(route, handlerFunc);
        break;
    case Handler::Post:
        server->Post(route, handlerFunc);
        break;
    case Handler::Put:
        server->Put(route, handlerFunc);
        break;
    case Handler::Patch:
        server->Patch(route, handlerFunc);
        break;
    case Handler::Delete:
        server->Delete(route, handlerFunc);
        break;
    case Handler::Options:
        server->Options(route, handlerFunc);
        break;
    case Handler::Error:
        server->set_error_handler(handlerFunc);
        break;
    default:
        break;
    }
}

void Server::Listen(std::string host, uint16_t port)
{
    host_ = std::move(host);
    port_ = port;
    auto* server = GetServer();
    if (!server)
        return;
    server->listen(host_, port);
}

void Server::Stop()
{
    if (IsRunning())
        server_->stop();
}

void Server::SetMountPoint(const std::string& mount, const std::string& path)
{
    auto* server = GetServer();
    if (!server)
        return;
    server->set_mount_point(mount, path);
}

void Server::RemoveMountPoint(const std::string& mount)
{
    auto* server = GetServer();
    if (!server)
        return;
    server->remove_mount_point(mount);
}

sa::tl::TablePtr Server::GetDefaultHeader() const
{
    return defaultHeader_;
}

void Server::SetDefaultHeaders(sa::tl::TablePtr headers)
{
    defaultHeader_ = std::move(headers);
}

} // namespace http

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<http::Server>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<http::Server, sa::tl::bind::Object>();
    result
        .AddFunction<http::Server, void, std::string, uint16_t>("listen", &http::Server::Listen, false)
        .AddFunction<http::Server, void>("stop", &http::Server::Stop)
        .AddFunction<http::Server, void, const std::string&, sa::tl::CallablePtr>("get", &http::Server::Get)
        .AddFunction<http::Server, void, const std::string&, sa::tl::CallablePtr>("post", &http::Server::Post)
        .AddFunction<http::Server, void, const std::string&, sa::tl::CallablePtr>("put", &http::Server::Put)
        .AddFunction<http::Server, void, const std::string&, sa::tl::CallablePtr>("patch", &http::Server::Patch)
        .AddFunction<http::Server, void, const std::string&, sa::tl::CallablePtr>("delete", &http::Server::Delete)
        .AddFunction<http::Server, void, const std::string&, sa::tl::CallablePtr>("options", &http::Server::Options)
        .AddFunction<http::Server, void, sa::tl::CallablePtr>("set_error_handler", &http::Server::SetErrorHandler)
        .AddFunction<http::Server, void, sa::tl::CallablePtr>("set_exception_handler", &http::Server::SetExceptionHandler)
        .AddFunction<http::Server, void, sa::tl::CallablePtr>("set_logger_handler", &http::Server::SetLoggerHandler)
        .AddFunction<http::Server, void, const std::string&, const std::string&>("set_mount_point", &http::Server::SetMountPoint)
        .AddFunction<http::Server, void, const std::string&>("remove_mount_point", &http::Server::RemoveMountPoint)
        .AddProperty("ssl", &http::Server::IsSSL, &http::Server::SetSSL)
        .AddProperty("cert_path", &http::Server::GetCertPath, &http::Server::SetCertPath)
        .AddProperty("private_key_path", &http::Server::GetPrivateKeyPath, &http::Server::SetPrivateKeyPath)
        .AddProperty("default_headers", &http::Server::GetDefaultHeader, &http::Server::SetDefaultHeaders)
        .AddProperty("host", &http::Server::GetHost)
        .AddProperty("port", &http::Server::GetPort)
        .AddProperty("running", &http::Server::IsRunning);
    return result;
}

}
