/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "client.h"
#include <ast.h>

namespace http {

Client::Client(sa::tl::Context& ctx) :
    Object(ctx)
{
}

sa::tl::TablePtr Client::GetHeaders() const
{
    return headers_;
}

void Client::SetHeaders(sa::tl::TablePtr value)
{
    headers_ = std::move(value);
}

sa::tl::TablePtr Client::GetResponseHeaders() const
{
    return responseHeaders_;
}

std::unique_ptr<httplib::Client> Client::GetClient(const URL& remote) const
{
    auto result = std::make_unique<httplib::Client>(remote.SchemeHostPort());
    result->enable_server_certificate_verification(verifySSL_);
    InternalSetAuth(*result);
    return result;
}

void Client::InternalSetAuth(httplib::Client& client) const
{
    switch (auth_)
    {
    case Auth::Basic:
        client.set_basic_auth(user_, pass_);
        break;
    case Auth::Digest:
        client.set_digest_auth(user_, pass_);
        break;
    default:
        break;
    }
}

httplib::Headers Client::InternalGetHeaders() const
{
    httplib::Headers result;
    if (headers_)
    {
        for (const auto& h : *headers_)
            result.emplace(sa::tl::KeyToString(h.first), h.second->ToString());
    }
    return result;
}

int Client::Get(const std::string& remote, const sa::tl::Callable& callback)
{
    if (callback.ParamCount() != 1)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return 0;
    }

    URL url(remote);
    auto client = GetClient(url);

    responseHeaders_.reset();

    auto res = client->Get(
        url.PathQuery(),
        InternalGetHeaders(),
        [&](const httplib::Response& response)
        {
            responseHeaders_ = sa::tl::MakeTable();
            for (const auto& h : response.headers)
                responseHeaders_->Add(h.first, sa::tl::MakeString(h.second));
            return true;
        },
        [&](const char* data, uint64_t size) -> bool
        {
            if (ctx_.stop_)
                return false;
            std::string content = { data, size };
            auto ret = callback(ctx_, { sa::tl::MakeValue(content) });
            return ret->ToBool();
        });
    if (!res)
        return (int)res.error();
    return res->status;
}

int Client::Post(const std::string& remote, const std::string& body, const std::string& contentType)
{
    URL url(remote);
    auto client = GetClient(url);

    responseHeaders_.reset();
    auto res = client->Post(url.PathQuery(), InternalGetHeaders(), body, contentType);
    if (!res)
        return (int)res.error();
    return res->status;
}

int Client::Put(const std::string& remote, const std::string& body, const std::string& contentType)
{
    URL url(remote);
    auto client = GetClient(url);

    responseHeaders_.reset();
    auto res = client->Put(url.PathQuery(), InternalGetHeaders(), body, contentType);
    if (!res)
        return (int)res.error();
    return res->status;
}

int Client::Patch(const std::string& remote, const std::string& body, const std::string& contentType)
{
    URL url(remote);
    auto client = GetClient(url);

    responseHeaders_.reset();
    auto res = client->Patch(url.PathQuery(), InternalGetHeaders(), body, contentType);
    if (!res)
        return (int)res.error();
    return res->status;
}

int Client::Delete(const std::string& remote)
{
    URL url(remote);
    auto client = GetClient(url);

    responseHeaders_.reset();
    auto res = client->Delete(url.PathQuery(), InternalGetHeaders());
    if (!res)
        return (int)res.error();
    return res->status;
}

sa::tl::Value Client::Options(const std::string& remote)
{
    URL url(remote);
    auto client = GetClient(url);

    responseHeaders_.reset();
    auto res = client->Options(url.PathQuery(), InternalGetHeaders());
    if (!res)
        return (int)res.error();
    auto result = sa::tl::MakeTable();
    for (const auto& h : res->headers)
    {
        result->Add(h.first, sa::tl::MakeString(h.second));
    }
    return result;
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<http::Client>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<http::Client, sa::tl::bind::Object>();
    result
        .AddFunction<http::Client, int, const std::string&, const sa::tl::Callable&>("get", &http::Client::Get)
        .AddFunction<http::Client, int, const std::string&, const std::string&, const std::string&>("post", &http::Client::Post)
        .AddFunction<http::Client, int, const std::string&, const std::string&, const std::string&>("put", &http::Client::Put)
        .AddFunction<http::Client, int, const std::string&, const std::string&, const std::string&>("patch", &http::Client::Patch)
        .AddFunction<http::Client, int, const std::string&>("delete", &http::Client::Delete)
        .AddFunction<http::Client, sa::tl::Value, const std::string&>("options", &http::Client::Options)
        .AddProperty("verify_ssl", &http::Client::GetVerifySSL, &http::Client::SetVerifySSL)
        .AddProperty("headers", &http::Client::GetHeaders, &http::Client::SetHeaders)
        .AddProperty("response_headers", &http::Client::GetResponseHeaders)
        .AddProperty("user", &http::Client::GetUser, &http::Client::SetUser)
        .AddProperty("pass", &http::Client::GetPass, &http::Client::SetPass)
        .AddProperty("auth", &http::Client::GetAuth, &http::Client::SetAuth);
    return result;
}

}
