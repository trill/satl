/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "response.h"

namespace http {

Response::Response(sa::tl::Context& ctx, httplib::Response& response) :
    Object(ctx),
    response_(response)
{
}

void Response::SetContent(const std::string& content, const std::string& contentType)
{
    response_.set_content(content, contentType);
}

void Response::SetHeader(const std::string& name, const std::string& value)
{
    response_.set_header(name, value);
}

std::string Response::GetHeader(const std::string& name)
{
    if (!response_.has_header(name))
        return "";
    return response_.get_header_value(name);
}

bool Response::HasHeader(const std::string& name)
{
    return response_.has_header(name);
}

void Response::Redirect(const std::string& location)
{
    response_.set_redirect(location);
}

int Response::GetStatus() const
{
    return response_.status;
}

void Response::SetStatus(int value)
{
    response_.status = value;
}

std::string Response::GetBody() const
{
    return response_.body;
}

void Response::SetBody(std::string value)
{
    response_.body = std::move(value);
}

void Response::Error(int status, const std::string& message)
{
    response_.status = status;
    response_.set_content(message, "text/plain");
}

sa::tl::Integer Response::GetContentLength() const
{
    return (sa::tl::Integer)response_.content_length_;
}

void Response::SetContentLength(sa::tl::Integer value)
{
    response_.content_length_ = value;
}

} // namespace http

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<http::Response>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<http::Response, sa::tl::bind::Object>();
    result
        .AddFunction<http::Response, void, const std::string&, const std::string&>("set_content", &http::Response::SetContent)
        .AddFunction<http::Response, void, const std::string&, const std::string&>("set_header", &http::Response::SetHeader)
        .AddFunction<http::Response, std::string, const std::string&>("get_header", &http::Response::GetHeader)
        .AddFunction<http::Response, bool, const std::string&>("has_header", &http::Response::HasHeader)
        .AddFunction<http::Response, void, const std::string&>("redirect", &http::Response::Redirect)
        .AddFunction<http::Response, void, int, const std::string&>("error", &http::Response::Error)
        .AddProperty("status", &http::Response::GetStatus, &http::Response::SetStatus)
        .AddProperty("body", &http::Response::GetBody, &http::Response::SetBody)
        .AddProperty("content_length", &http::Response::GetContentLength, &http::Response::SetContentLength);
    return result;
}

}
