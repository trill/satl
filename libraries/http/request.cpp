/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "request.h"

namespace http {

Request::Request(sa::tl::Context& ctx, const httplib::Request& request) :
    Object(ctx),
    request_(request)
{
}

bool Request::HasHeader(const std::string& name) const
{
    return request_.has_header(name);
}

std::string Request::GetHeader(const std::string& name) const
{
    if (!request_.has_header(name))
        return "";
    return request_.get_header_value(name);
}

bool Request::HasParam(const std::string& name) const
{
    return request_.has_param(name);
}

std::string Request::GetParam(const std::string& name) const
{
    if (!request_.has_param(name))
        return "";
    return request_.get_param_value(name);
}

std::string Request::GetPath() const
{
    return request_.path;
}

std::string Request::GetMethod() const
{
    return request_.method;
}

bool Request::HasFile(const std::string& name) const
{
    return request_.has_file(name);
}

std::string Request::GetFileName(const std::string& name) const
{
    if (!request_.has_file(name))
        return "";
    return request_.get_file_value(name).name;
}

std::string Request::GetFileContent(const std::string& name) const
{
    if (!request_.has_file(name))
        return "";
    return request_.get_file_value(name).content;
}

std::string Request::GetFileFilename(const std::string& name) const
{
    if (!request_.has_file(name))
        return "";
    return request_.get_file_value(name).filename;
}

std::string Request::GetFileContentType(const std::string& name) const
{
    if (!request_.has_file(name))
        return "";
    return request_.get_file_value(name).content_type;
}

} // namespace http

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<http::Request>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<http::Request, sa::tl::bind::Object>();
    result
        .AddFunction<http::Request, bool, const std::string&>("has_header", &http::Request::HasHeader)
        .AddFunction<http::Request, std::string, const std::string&>("get_header", &http::Request::GetHeader)
        .AddFunction<http::Request, bool, const std::string&>("has_param", &http::Request::HasParam)
        .AddFunction<http::Request, std::string, const std::string&>("get_param", &http::Request::GetParam)
        .AddFunction<http::Request, bool, const std::string&>("has_file", &http::Request::HasFile)
        .AddFunction<http::Request, std::string, const std::string&>("get_file_name", &http::Request::GetFileName)
        .AddFunction<http::Request, std::string, const std::string&>("get_file_content", &http::Request::GetFileContent)
        .AddFunction<http::Request, std::string, const std::string&>("get_file_filename", &http::Request::GetFileFilename)
        .AddFunction<http::Request, std::string, const std::string&>("get_file_content_type", &http::Request::GetFileContentType)
        .AddProperty("path", &http::Request::GetPath)
        .AddProperty("method", &http::Request::GetMethod);
    return result;
}

}
