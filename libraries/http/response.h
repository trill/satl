/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <object.h>
#include <httplib.h>

namespace http {

class Response : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Response);
    SATL_NON_MOVEABLE(Response);
    TYPED_OBJECT(Response);
public:
    Response(sa::tl::Context& ctx, httplib::Response& response);
    ~Response() override = default;
    void SetContent(const std::string& content, const std::string& contentType);
    void SetHeader(const std::string& name, const std::string& value);
    std::string GetHeader(const std::string& name);
    bool HasHeader(const std::string& name);
    void Redirect(const std::string& location);
    [[nodiscard]] sa::tl::Integer GetContentLength() const;
    void SetContentLength(sa::tl::Integer value);
    [[nodiscard]] int GetStatus() const;
    void SetStatus(int value);
    [[nodiscard]] std::string GetBody() const;
    void SetBody(std::string value);
    void Error(int status, const std::string& message);
private:
    httplib::Response& response_;
};

} // namespace http

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<http::Response>(sa::tl::Context& ctx);
}
