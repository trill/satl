# http_lib

HTTP client and server library using [cpp-httplib](https://github.com/yhirose/cpp-httplib). This library requires the cpp-httplib library to be installed. On Arch it can be installed from [AUR](https://aur.archlinux.org/packages/cpp-httplib):

~~~sh
$ yay -S cpp-httplib
~~~

## Client

Example:

~~~
load_lib("libsatl_http.so");

client = http.client();
client.get("https://example.com/index.html?a=b", function(content) {
    print(content);
    return true;
});
~~~

## Server

Example:

~~~
load_lib("libsatl_http.so");

server = http.server();
// Headers that are sent with every response
server.default_header = { "Server" = "sa::tl HTTP Server" };

server.get("/hi", function(request, response)
{
    response.set_content("Hello there!", "text/plain");
});
server.get("/stop", function(request, response)
{
    server.stop();
});
server.get("(/(.*))", function(request, response)
{
    // Match everything else
    response.set_content("Path " + request.path, "text/plain");
});

server.listen("localhost", 8080);
~~~

## API

See the Manual for the API documentation.
