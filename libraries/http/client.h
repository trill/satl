/**
 * Copyright 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <object.h>
#include <value.h>
#include "url.h"
#include <httplib.h>

namespace httplib {
class Client;
}

namespace http {

class Client : public sa::tl::bind::Object
{
    SATL_NON_COPYABLE(Client);
    SATL_NON_MOVEABLE(Client);
    TYPED_OBJECT(Client);
public:
    enum class Auth : int
    {
        None,
        Basic,
        Digest,
    };
    explicit Client(sa::tl::Context& ctx);
    ~Client() override = default;
    int Get(const std::string& remote, const sa::tl::Callable& callback);
    int Post(const std::string& remote, const std::string& body, const std::string& contentType);
    int Put(const std::string& remote, const std::string& body, const std::string& contentType);
    int Patch(const std::string& remote, const std::string& body, const std::string& contentType);
    int Delete(const std::string& remote);
    sa::tl::Value Options(const std::string& remote);
    [[nodiscard]] bool GetVerifySSL() const { return verifySSL_; }
    void SetVerifySSL(bool value) { verifySSL_ = value; }
    [[nodiscard]] sa::tl::TablePtr GetHeaders() const;
    [[nodiscard]] sa::tl::TablePtr GetResponseHeaders() const;
    void SetHeaders(sa::tl::TablePtr value);
    [[nodiscard]] std::string GetUser() const { return user_; }
    void SetUser(std::string value) { user_ = std::move(value); }
    [[nodiscard]] std::string GetPass() const { return pass_; }
    void SetPass(std::string value) { pass_ = std::move(value); }
    [[nodiscard]] int GetAuth() const { return (int)auth_; }
    void SetAuth(int value) { auth_ = (Auth)value; }
private:
    [[nodiscard]] std::unique_ptr<httplib::Client> GetClient(const URL& remote) const;
    void InternalSetAuth(httplib::Client& client) const;
    [[nodiscard]] httplib::Headers InternalGetHeaders() const;
    bool verifySSL_{ true };
    sa::tl::TablePtr headers_;
    sa::tl::TablePtr responseHeaders_;
    std::string user_;
    std::string pass_;
    Auth auth_{ Auth::None };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<http::Client>(sa::tl::Context& ctx);
}
