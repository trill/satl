/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "../html_entities.h"
#include "../url.h"

TEST_CASE("HTML Entities decode", "[http][entities]")
{
    auto enc = http::HTMLDecode("&#352;");
    REQUIRE(enc == "Š");
    auto enc2 = http::HTMLDecode("&#x2013;");
    REQUIRE(enc2 == "–");
    auto enc3 = http::HTMLDecode("&copy;");
    REQUIRE(enc3 == "©");
}

TEST_CASE("HTML Entities encode", "[http][entities]")
{
    auto enc = http::HTMLEncode("©");
    REQUIRE(enc == "&copy;");
}

TEST_CASE("XML encode", "[http][xml]")
{
    auto enc = http::XMLEncode("©");
    REQUIRE(enc == "&#169;");
}

TEST_CASE("XML encode 2", "[http][xml]")
{
    auto enc = http::XMLEncode("&<>");
    REQUIRE(enc == "&#38;&#60;&#62;");
}

TEST_CASE("XML dedode", "[http][xml]")
{
    auto enc = http::XMLDecode("&#169;");
    REQUIRE(enc == "©");
    auto enc2 = http::XMLDecode("&#xa9;");
    REQUIRE(enc2 == "©");
}

TEST_CASE("XML dedode 2", "[http][xml]")
{
    auto enc = http::XMLDecode("&#38;&#60;&#62;");
    REQUIRE(enc == "&<>");
}

TEST_CASE("XML dedode 3", "[http][cml]")
{
    auto enc = http::XMLDecode("&amp;&lt;&gt;");
    // Fails no entities
    REQUIRE(enc == "&amp;&lt;&gt;");
}

TEST_CASE("URL encode", "[http][url]")
{
    auto enc = http::URLEncode("this is a question");
    REQUIRE(enc == "this%20is%20a%20question");
}

TEST_CASE("URL decode", "[http][url]")
{
    auto enc = http::URLDecode("this%20is%20a%20question");
    REQUIRE(enc == "this is a question");
}
