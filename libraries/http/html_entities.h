/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace http {

std::string HTMLEncode(const std::string& value);
std::string XMLEncode(const std::string& value);
std::string HTMLDecode(const std::string& value);
std::string XMLDecode(const std::string& value);

}
