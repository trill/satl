/**
 * Copyright (c) 2022-2023, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <context.h>
#include <module.h>
#include "client.h"
#include "server.h"
#include "request.h"
#include "response.h"
#include "url.h"
#include <fstream>
#include "html_entities.h"
#include <factory.h>

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("http"))
        return true;
    if (ctx.HasMetatable<http::Client>())
        return false;
    if (ctx.HasMetatable<http::Server>())
        return false;

    sa::tl::bind::Register<sa::tl::bind::Object>(ctx);
    auto& clientMeta = sa::tl::bind::Register<http::Client>(ctx);
    auto& serverMeta = sa::tl::bind::Register<http::Server>(ctx);
    sa::tl::bind::Register<http::Request>(ctx);
    sa::tl::bind::Register<http::Response>(ctx);

    auto module = sa::tl::MakeModule();
    module->AddConst<int>("AUTH_NONE", (int)http::Client::Auth::None);
    module->AddConst<int>("AUTH_BASIC", (int)http::Client::Auth::Basic);
    module->AddConst<int>("AUTH_DIGEST", (int)http::Client::Auth::Digest);

    module->AddConst<int>("ERROR_SUCCESS", (int)httplib::Error::Success);
    module->AddConst<int>("ERROR_UNKNOWN", (int)httplib::Error::Unknown);
    module->AddConst<int>("ERROR_CONNECTION", (int)httplib::Error::Connection);
    module->AddConst<int>("ERROR_BIND_IP_ADDRESS", (int)httplib::Error::BindIPAddress);
    module->AddConst<int>("ERROR_READ", (int)httplib::Error::Read);
    module->AddConst<int>("ERROR_WRITE", (int)httplib::Error::Write);
    module->AddConst<int>("ERROR_EXCEEDED_REDIRECTION_COUNT", (int)httplib::Error::ExceedRedirectCount);
    module->AddConst<int>("ERROR_CANCELED", (int)httplib::Error::Canceled);
    module->AddConst<int>("ERROR_SSL_CONNECTION", (int)httplib::Error::SSLConnection);
    module->AddConst<int>("ERROR_SSL_LOADING_CERT", (int)httplib::Error::SSLLoadingCerts);
    module->AddConst<int>("ERROR_SSL_SERVER_VERIFICATION", (int)httplib::Error::SSLServerVerification);
    module->AddConst<int>("ERROR_UNSUPPORTED_MULTIPART", (int)httplib::Error::UnsupportedMultipartBoundaryChars);
    module->AddConst<int>("ERROR_COMPRESSION", (int)httplib::Error::Compression);
    module->AddConst<int>("ERROR_CONNECTION_TIMEOUT", (int)httplib::Error::ConnectionTimeout);

    module->AddFunction<sa::tl::Value>("client",
        [&ctx, &clientMeta]() -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<http::Client>(ctx, clientMeta, ctx); });
    module->AddFunction<sa::tl::Value>("server",
        [&ctx, &serverMeta]() -> sa::tl::Value
        { return sa::tl::bind::CreateInstance<http::Server>(ctx, serverMeta, ctx); });
    module->AddFunction<sa::tl::TablePtr, std::string>("parse_url", [](const std::string& _url) -> sa::tl::TablePtr {
        http::URL url(_url);
        sa::tl::TablePtr result = sa::tl::MakeTable();
        result->Add("scheme", url.Scheme());
        result->Add("host", url.Host());
        result->Add("port", url.Port());
        result->Add("path", url.Path());
        result->Add("query", url.Query());
        return result;
    });
    module->AddFunction<bool, std::string, std::string>("download", [&ctx](const std::string& _url, const std::string& dest) -> bool {
        http::URL url(_url);
        std::ofstream out(dest);
        if (!out.good())
        {
            ctx.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Could not open file \"{}\"", dest));
            return false;
        }
        httplib::Client client(url.SchemeHostPort());
        auto res = client.Get(url.PathQuery(),
            [&](const char* data, uint64_t size) -> bool
            {
                out.write(data, (std::streamsize)size);
                return true;
            });
        return !!res;
    });
    module->AddFunction<std::string, std::string>("fetch", [&ctx](const std::string& _url) -> std::string {
        http::URL url(_url);
        std::stringstream out;
        httplib::Client client(url.SchemeHostPort());
        auto res = client.Get(url.PathQuery(),
            [&](const char* data, uint64_t size) -> bool
            {
                out.write(data, (std::streamsize)size);
                return true;
            });
        if (res)
            return out.str();
        ctx.RuntimeError(sa::tl::Error::UserDefined, httplib::to_string(res.error()));
        return "";
    });
    module->AddFunction<std::string, std::string>("url_encode", &http::URLEncode);
    module->AddFunction<std::string, std::string>("url_decode", &http::URLDecode);
    module->AddFunction<std::string, std::string>("html_encode", &http::HTMLEncode);
    module->AddFunction<std::string, std::string>("html_decode", &http::HTMLDecode);
    module->AddFunction<std::string, std::string>("xml_encode", &http::XMLEncode);
    module->AddFunction<std::string, std::string>("xml_decode", &http::XMLDecode);

    ctx.AddConst("http", std::move(module), sa::tl::SCOPE_LIBRARY);
    return true;
}
