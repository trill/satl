project (satl_pg CXX)

find_package(PostgreSQL)

if (PostgreSQL_FOUND)
    file(GLOB SOURCES *.cpp *.h)

    add_library(satl_pg SHARED ${SOURCES})
    set_property(TARGET satl_pg PROPERTY POSITION_INDEPENDENT_CODE ON)

    target_include_directories(satl_pg PRIVATE ${PostgreSQL_INCLUDE_DIRS})
    target_link_libraries(satl_pg satl bind ${PostgreSQL_LIBRARIES})
    install(TARGETS satl_pg LIBRARY)
endif(PostgreSQL_FOUND)
