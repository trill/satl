# pg_lib

Extension library to connect to a PostgreSQL server.

## Usage

Load the extension:

~~~cpp
sa::tl::Context ctx;
ctx.LoadExtLibrary("/path/to/libsatl_pg.so");
~~~

~~~
// 5432 = port
conn = pg.connect("host.name", "database_name", "user_name", "password", 5432);
// Returns a table of rows
res = conn.query("SELECT * FROM my_table");
foreach (row : res)
{
    foreach (col, value in row)
    {
        // col = column name
        // value = column value
        println(col, value);
    }
}

// Using a callback function
conn.query_callback("SELECT * FROM my_table", function(row)
{
    foreach (col, value in row)
    {
        // col = column name
        // value = column value
        println(col, value);
    }
    return true;
});

// Close the connection
pg.close(conn);
~~~

## API

See the Manual for the API documentation.
