/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libpq-fe.h>
#include <string>
#include <value.h>
#include <context.h>
#include <node.h>
#include <non_copyable.h>
#include <object.h>

namespace pg {

class Result;

class Connection
{
    SATL_NON_COPYABLE(Connection);
    SATL_NON_MOVEABLE(Connection);
public:
    Connection(sa::tl::Context& ctx, std::string host, std::string db, std::string user, std::string pass, uint16_t port);
    ~Connection();
    bool Connect(int numTries = 1);
    bool CheckConnection();
    std::string EscapeString(const std::string& s);
    sa::tl::Integer GetLastInsertId();
    [[nodiscard]] bool IsConnected() const { return connected_; }
    void ExecuteQuery(const std::string& query);
    sa::tl::TablePtr StoreQuery(const std::string& query);
    void StoreQueryCallback(const std::string& query, const sa::tl::Callable& callback);
    [[nodiscard]] std::string GetDsn() const { return dsn_; }
    [[nodiscard]] std::string GetHost() const { return host_; }
    [[nodiscard]] std::string GetDb() const { return db_; }
    [[nodiscard]] std::string GetUser() const { return user_; }
    [[nodiscard]] std::string GetPass() const { return pass_; }
    [[nodiscard]] sa::tl::Integer GetPort() const { return port_; }
private:
    void InternalQuery(const std::string& query);
    std::shared_ptr<Result> InternalSelectQuery(const std::string& query);
    std::shared_ptr<Result> VerifyResult(std::shared_ptr<Result> result);
    void FreeResult(Result* res);
    sa::tl::Context& ctx_;
    std::string host_;
    std::string db_;
    std::string user_;
    std::string pass_;
    uint16_t port_{ 0 };
    std::string dsn_;
    PGconn* handle_{ nullptr };
    bool connected_{ false };
};

}

namespace sa::tl::bind {
template<> sa::tl::MetaTable& Register<pg::Connection>(sa::tl::Context& ctx);
}
