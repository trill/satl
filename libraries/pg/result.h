/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <libpq-fe.h>
#include "connection.h"
#include <memory>
#include <value.h>

namespace pg {

class Result : public std::enable_shared_from_this<Result>
{
    friend class Connection;
public:
    ~Result();
    std::shared_ptr<Result> Next();
    int32_t GetColumns() const { return columns_; }
    std::string GetColumnName(int32_t column);
    uint32_t GetColumnType(int32_t column);
    int32_t GetInt(int32_t column);
    int64_t GetLong(int32_t column);
    double GetFloat(int32_t column);
    std::string GetString(int32_t column);
    sa::tl::ValuePtr GetValue(int32_t column);
protected:
    explicit Result(PGresult* res);
private:
    PGresult* handle_;
    int32_t rows_, cursor_;
    int32_t columns_;
};

} // namespace pg

