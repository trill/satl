/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <metatable.h>
#include <module.h>
#include <node.h>
#include "connection.h"

extern "C" bool Init(sa::tl::Context& ctx);
extern "C" bool Init(sa::tl::Context& ctx)
{
    if (ctx.HasValue("pg"))
        return true;
    
    sa::tl::MetaTable& connMeta = sa::tl::bind::Register<pg::Connection>(ctx);

    auto module = sa::tl::MakeModule();
    module->AddFunction<sa::tl::Value, std::string, std::string, std::string, std::string, sa::tl::Integer>(
        "connect",
        [&ctx, &connMeta](const std::string& host,
            const std::string& db,
            const std::string& user,
            const std::string& pass,
            sa::tl::Integer port) -> sa::tl::Value
        {
            auto* connection = ctx.GetGC().CreateObject<pg::Connection>(ctx, host, db, user, pass, port);
            if (connection->Connect())
            {
                sa::tl::Value v = sa::tl::MakeObject(connMeta, connection);
                return v;
            }
            ctx.GetGC().Remove(connection);
            ctx.RuntimeError(sa::tl::Error::Code::UserDefined, std::format("Unable to connect to database server at {}:{}", host, port));
            return {};
        });
    module->AddFunction<void, sa::tl::ObjectPtr>("close",
        [&ctx, &connMeta](const sa::tl::ObjectPtr& connObj) -> void
        {
            if (auto* conn = connMeta.CastTo<pg::Connection>(connObj.get()))
            {
                ctx.GetGC().Remove(conn);
                return;
            }

            ctx.RuntimeError(sa::tl::Error::Code::TypeMismatch, "Object is not an instance of Connection");
        });

    ctx.AddConst("pg", std::move(module), sa::tl::SCOPE_LIBRARY);

    return true;
}
