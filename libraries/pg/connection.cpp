/**
 * Copyright (c) 2021-2024, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "connection.h"
#include <ast.h>
#include <scope_guard.h>
#include <chrono>
#include <sstream>
#include <thread>
#include "result.h"

namespace pg {

#define PG_OK(stat) (stat == PGRES_COMMAND_OK || stat == PGRES_TUPLES_OK)

Connection::Connection(sa::tl::Context& ctx,
    std::string host,
    std::string db,
    std::string user,
    std::string pass,
    uint16_t port) :
    ctx_(ctx),
    host_(std::move(host)),
    db_(std::move(db)),
    user_(std::move(user)),
    pass_(std::move(pass)),
    port_(port)
{
    std::stringstream dsn;
    dsn << "host='" << host_ << "' dbname='" << db_ << "' user='" << user_ << "' password='" << pass_ << "' port='"
        << port_ << "'";
    dsn_ = dsn.str();
}

Connection::~Connection()
{
    if (handle_)
        PQfinish(handle_);
}

bool Connection::Connect(int numTries)
{
    int tr = 0;
    std::string lastError = "Unknown error";
    while (!connected_ && tr < numTries)
    {
        int remaingTries = tr - numTries;
        PGPing ping = PQping(dsn_.c_str());
        switch (ping)
        {
        case PQPING_REJECT:
            lastError = "Server rejected connection";
            break;
        case PQPING_NO_RESPONSE:
            lastError = "Server didn't respond";
            break;
        case PQPING_NO_ATTEMPT:
            lastError = "Bad connection parameters";
            return false;
        default:
            break;
        }

        if (ping == PQPING_OK)
        {
            // When ping OK then try to connect
            handle_ = PQconnectdb(dsn_.c_str());
            connected_ = PQstatus(handle_) == CONNECTION_OK;
        }
        ++tr;
        if (!connected_ && remaingTries > 0)
            std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    if (!connected_)
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, lastError);

    return connected_;
}

bool Connection::CheckConnection()
{
    if (dsn_.empty())
        return false;

    PGresult* res = PQexec(handle_, "SELECT 1");
    ExecStatusType stat = PQresultStatus(res);
    sa::tl::ScopeGuard deleteGguard([res]() { PQclear(res); });

    if (PG_OK(stat))
        // OK
        return true;

    connected_ = false;
    PGPing ping = PQping(dsn_.c_str());
    if (ping == PQPING_OK)
        return Connect();
    return false;
}

std::string Connection::EscapeString(const std::string& s)
{
    if (s.empty())
        return "''";

    int32_t error = 0;
    std::string output;
    output.resize(s.length() * 2 + 1);

    size_t len = PQescapeStringConn(handle_, output.data(), s.c_str(), s.length(), &error);
    std::stringstream ss;
    output.resize(len);
    ss << "'" << output << "'";
    return ss.str();
}

sa::tl::Integer Connection::GetLastInsertId()
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No connection");
        return 0;
    }

    PGresult* res = PQexec(handle_, "SELECT LASTVAL() as last;");
    ExecStatusType stat = PQresultStatus(res);
    sa::tl::ScopeGuard deleteGguard([res]() { PQclear(res); });

    if (!PG_OK(stat))
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, PQresultErrorMessage(res));
        return 0;
    }

    return static_cast<sa::tl::Integer>(strtoull(PQgetvalue(res, 0, PQfnumber(res, "last")), nullptr, 0));
}

std::shared_ptr<Result> Connection::VerifyResult(std::shared_ptr<Result> result)
{
    if (!result->Next())
        return {};
    return result;
}

void Connection::FreeResult(Result* res)
{
    delete res;
}

void Connection::ExecuteQuery(const std::string& query)
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No connection");
        return;
    }
    InternalQuery(query);
}

void Connection::InternalQuery(const std::string& query)
{
    PGresult* res = PQexec(handle_, query.c_str());
    ExecStatusType stat = PQresultStatus(res);

    if (!PG_OK(stat))
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, PQresultErrorMessage(res));
        PQclear(res);
        return;
    }

    PQclear(res);
}

sa::tl::TablePtr Connection::StoreQuery(const std::string& query)
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No connection");
        return {};
    }
    auto result = sa::tl::MakeTable();
    sa::tl::Integer rows = 0;
    for (auto res = InternalSelectQuery(query); res; res = res->Next())
    {
        auto row = sa::tl::MakeTable();
        for (int32_t column = 0; column < res->GetColumns(); ++column)
        {
            const auto columnName = res->GetColumnName(column);
            row->Add(columnName, res->GetValue(column));
        }
        result->Add(rows, sa::tl::MakeValue(row));
        ++rows;
    }
    return result;
}

void Connection::StoreQueryCallback(const std::string& query, const sa::tl::Callable& callback)
{
    if (!connected_)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, "No connection");
        return;
    }
    if (callback.ParamCount() != 1)
    {
        ctx_.RuntimeError(sa::tl::Error::Code::ArgumentsMismatch, "Wrong number of arguments of callback function");
        return;
    }

    for (auto res = InternalSelectQuery(query); res; res = res->Next())
    {
        if (ctx_.stop_)
            break;
        auto row = sa::tl::MakeTable();
        for (int32_t column = 0; column < res->GetColumns(); ++column)
        {
            const auto columnName = res->GetColumnName(column);
            row->Add(columnName, res->GetValue(column));
        }
        auto ret = callback(ctx_, { MakeValue(row) });
        if (!ret->ToBool())
            break;
    }
}

std::shared_ptr<Result> Connection::InternalSelectQuery(const std::string& query)
{
    PGresult* res = PQexec(handle_, query.c_str());
    ExecStatusType stat = PQresultStatus(res);

    if (!PG_OK(stat))
    {
        ctx_.RuntimeError(sa::tl::Error::Code::UserDefined, PQresultErrorMessage(res));
        PQclear(res);

        return {};
    }

    std::shared_ptr<Result> results(
        new Result(res), std::bind(&Connection::FreeResult, this, std::placeholders::_1));
    return VerifyResult(results);
}

}

namespace sa::tl::bind {

template<>
sa::tl::MetaTable& Register<pg::Connection>(sa::tl::Context& ctx)
{
    auto& result = ctx.AddMetatable<pg::Connection>();
    result
        .AddFunction<pg::Connection, std::string, const std::string&>("escape", &pg::Connection::EscapeString)
        .AddFunction<pg::Connection, sa::tl::Integer>("last_id", &pg::Connection::GetLastInsertId)
        .AddFunction<pg::Connection, void, const std::string&>("execute", &pg::Connection::ExecuteQuery)
        .AddFunction<pg::Connection, sa::tl::TablePtr, const std::string&>("query", &pg::Connection::StoreQuery)
        .AddFunction<pg::Connection, void, const std::string&, const sa::tl::Callable&>(
            "query_callback", &pg::Connection::StoreQueryCallback)
        .AddFunction<pg::Connection, bool>("check_connection", &pg::Connection::CheckConnection)

        .AddProperty("dsn", &pg::Connection::GetDsn)
        .AddProperty("host", &pg::Connection::GetHost)
        .AddProperty("db", &pg::Connection::GetDb)
        .AddProperty("user", &pg::Connection::GetUser)
        .AddProperty("pass", &pg::Connection::GetPass)
        .AddProperty("port", &pg::Connection::GetPort);

    return result;
}

}
