/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "result.h"

namespace pg {

#define BOOLOID 16
#define INT8OID 20
#define INT2OID 21
#define INT4OID 23
#define FLOAT4OID 700
#define FLOAT8OID 701

Result::Result(PGresult* res) :
    handle_(res),
    cursor_(-1)
{
    rows_ = PQntuples(handle_) - 1;
    columns_ = PQnfields(handle_);
}

Result::~Result()
{
    PQclear(handle_);
}

std::shared_ptr<Result> Result::Next()
{
    if (cursor_ >= rows_)
        return {};

    ++cursor_;
    return shared_from_this();
}

std::string Result::GetColumnName(int32_t column)
{
    if (column >= columns_)
        return "";
    return PQfname(handle_, column);
}

uint32_t Result::GetColumnType(int32_t column)
{
    return PQftype(handle_, column);
}

int32_t Result::GetInt(int32_t column)
{
    return static_cast<int32_t>(strtol(PQgetvalue(handle_, cursor_, column), nullptr, 10));
}

int64_t Result::GetLong(int32_t column)
{
    return strtoll(PQgetvalue(handle_, cursor_, column), nullptr, 10);
}

double Result::GetFloat(int32_t column)
{
    return atof(PQgetvalue(handle_, cursor_, column));
}

std::string Result::GetString(int32_t column)
{
    size_t size = PQgetlength(handle_, cursor_, column);
    return { PQgetvalue(handle_, cursor_, column), size };
}

sa::tl::ValuePtr Result::GetValue(int32_t column)
{
    switch (GetColumnType(column))
    {
    case INT2OID:
    case INT4OID:
        return sa::tl::MakeValue(GetInt(column));
    case INT8OID:
        return sa::tl::MakeValue(GetLong(column));
    case FLOAT4OID:
    case FLOAT8OID:
        return sa::tl::MakeValue((sa::tl::Float)GetFloat(column));
    default:
        return sa::tl::MakeValue(GetString(column));
    }
}

} // namespace pg
